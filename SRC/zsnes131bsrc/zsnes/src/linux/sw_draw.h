#ifndef SW_DRAW_h
#define SW_DRAW_h 1

int sw_start(int width, int height, int req_depth, int FullScreen);
void sw_end();
void sw_clearwin();
void sw_drawwin();

#endif
