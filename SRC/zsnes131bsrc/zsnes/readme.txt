If you need help to compile ZSNES, read install.txt.
If you need help to browse the sources, read srcinfo.txt.

For more information about ZSNES development, check http://zsnes.sourceforge.net
The official website is at http://www.zsnes.com
You can also reach us on irc on #zsnes : 
EFNet for general discussion about ZSNES and lot of other stuff
irc.openprojects.org if you want to chat with the developers
