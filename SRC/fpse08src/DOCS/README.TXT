
                FPSE: Free PlayStation Emulator - Win32 Version
             -----------------------------------------------------


1. Introduction
2. Installation
3. Command Line options
4. Using script files
5. Using debugger
6. Notes to developers
7. Team
8. Final credits


1. Introduction
===============
FPSE is a software that emulate the Sony PlayStation (PSX).
If you use FPSE you agree with the conditions in TERMS.TXT

Features:
* Good compatibility:
   - all R3000 and many GTE opcodes.
   - Nice graphic and sound output
   - MDEC support.
   - CDROM can run commercial games.
   - I/O emulation.
* Fast because it can use dinamic recompilation.
* Easy plugin technology: some good plugins are already present.
* Memory cards and Rs232 emulation.
* Dual shock emulation.
* Accepts PS_EXE, CPE and COFF images as executables.
* And it's open-source.

Win32 version requires:
* 486DX/33
* 8 MB Ram
* A Video card and a keyboard (I hope you have them...)
* Microsoft Windows 95/98/NT4/2000

Optional:
* A 3D Video accelerator
* Sound card (it's better if it has an hardware voice mixer)
* A gamepad with force-feedback.
* A mouse
* A CDRom at least 2X
* DirectX 5.0 or higher.


2. Installation
===============
* Unzip the binary package in a directory. Be sure that the original tree
  structure is mantained.
* Run 'FpseCfg.exe', configure the options and select some plugins.
  Click on 'SAVE' button when you finish.
* Put an ORIGINAL Psx cdrom in your drive and run FPSE with 'RUN' button.

If you get new plugins, copy them in 'PlugIn' subdirectory and launch again
'FpseCfg.exe'.

The configurator is a new tool added in version 0.08 for managing FPSE.
It's very easy to understand, so I'll give you only some advices:
* If you want to use new option you must alway click on "SAVE": this act
  will write them in FPSE.INI file.
* If you run again FPSECFG, the plugin selected in combo boxes are not the
  same in the FPSE main dir (gpurend, spurend, etc), because NO LoadLibrary()
  function is used for an easy porting.
* When you click on "SAVE" button, plugin are not substituited if the
  read-only attribute is on. So, be sure that this flag is clear before
  mail to me.


3. Command Line options
=======================
You can override default settings in FPSE.INI with some command line options.

Syntax:
        FPSE [-options] [EXE_FILE]

Exe_File    executable to run.
-aFLASH     load 'FLASH' with A/R. Run '-a' only will load 'AR.ROM'.
-bBIOS      Load an original bios image. If 'BIOS' is not present in the
            current directory, it will be searched in BIOS subdirectory.
            if no bios file is specified, bios emulation will be turned on.
-c          Use compiler cpu engine.
-d          Show disassembled PSX code at runtime.
-e          Run debugger.
-i          Use interpreter cpu engine.
-sSCRIPT    Load a script file.
-v          Be verbose, show all read/write accesses to hardware registers.
-?          Display an help screen.


4. Using script files
=====================

In the command line write:
        FPSE -sStart.bat
if you want to run "Start.bat"

Script files can contain the following commands:
* LOAD
    Syntax: LOAD <filename> <address>
  Load <filename> in the system ram at the specified address.
  If <address> is not present, <filename> must be an EXE file.
* RUN
    Syntax: RUN <filename>
  Similar to LOAD command, also it setup PC and SP.
  Filename must be an EXE file.

With Script files you can run more demo than other emu's; the load order is:
1) the BIOS (if not emulated)
2) LIBPS.exe (it is always loaded if present in the current path)
3) Script file
3) Executable written in the command line


5. Using debugger
=================
FPSE is also a good system for development and testing of PSX apps.
For that purpose an integrated debugger is included and actived with
the '-e' option.
There is a portable debugger based on a CUI interface; commands available are:
    r <x> <val> : watch regs (x[0..3] return COPx, else return GPR
    b <hexadr>  : set breakpoint at hexadr (if no hexadr break is disabled)
    g           : go
    s           : step
    n           : next
    <ENTER>     : repeat last command
    u <hexadr>  : unassemble at hexadr (if none PC reg is used)
    d <hexadr>  : dump data at hexadr (if none PC reg is used)
    p <hexadr>  : print portname at hexadr
    w <x,hexadr>: write data of type x at hexadr
    h           : display this help
    q           : quit debugger


6. Notes to developers
======================
FPSE SDK is designed to give you a nice enviroment for making new plugins.
Refer to FPSE SDK manual for more informations.

Some other notes:
* If you use portable libraries, make the code so that porting will be easy.
* If you write a document, please make it a simple text file.
* NEVER use the registry for saving defaults (Win32 version).


7. Team
=======

BERO:   CODER ....................................... bero@geocities.co.jp

LDChen: CODER ....................................... LDChen@fpse.emuforce.com

Mr.Fog: WEB DESIGNER ................................ alex_mrfog@hotmail.com


The latest version of FPSE is at http://fpse.emuforce.com


8. Final credits
================
most info by Blackbag
http://www.blackbag.org/psx/playstation/playstation.html  (DOWN)

more info from rw-pslib source & headers by Rob Withey
http://napalm.intelinet.com/_utils.htm

GPU info by k-comm.
http://www3.airnet.ne.jp/kcomm/

more GPU, CD-ROM, SPU & GTE info by Doomed/Padua
http://psx.rules.org/psxrul2.shtml

more SPU info by bITmASTER
http://members.xoom.com/_bITmASTER_/

cygwin32 gcc compiler by GNU/FSF and Cygnus Solutions
http://sourceware.cygnus.com/cygwin/

DJGPP compiler by DJ Delorie
http://www.delorie.com

RSXNTDJ by Rainer Schnitker eXtensions
http://www.mathematik.uni-bielefeld.de/~rainer/

fast inverse-DCT by Independent JPEG Group
http://www.ijg.com

SEAL audio Library
http://www.egator.com/seal/

ASPI SDK from Adaptec
ftp://ftp.adaptec.com/pub/BBS/developer/

Info on pad & memory cards at:
http://www.blackthunder.demon.nl/

Info on RS232, IRQ logic & parallel port discovered by me (LDChen)

Many demos from Hitmen
http://www.hitmen-console.org

Thanks to all people at PSXDEV group
http://www.egroups.com/group/psxdev/

