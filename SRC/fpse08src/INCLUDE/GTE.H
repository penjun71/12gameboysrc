#ifndef GTE_H
#define GTE_H

#include "type.h"

typedef void (*cop2func)(UINT32 *r);

int cop2(int code,UINT32 *r);
void cop2read(int regno,UINT32 *r);
void cop2write(int regno,UINT32 *r);

cop2func cop2funcptr(int code);
cop2func cop2readfuncptr(int regno);
cop2func cop2writefuncptr(int regno);

#endif
