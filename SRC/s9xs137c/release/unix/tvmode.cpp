void TVMode (uint8 *srcPtr, uint32 srcPitch, uint8 *deltaPtr,
	     uint8 *dstPtr, uint32 dstPitch, int width, int height)
{
    uint8 *nextLine, *finish;
    uint32 colorMask = ~(RGB_LOW_BITS_MASK | (RGB_LOW_BITS_MASK << 16));
    uint32 lowPixelMask = RGB_LOW_BITS_MASK;

    nextLine = dstPtr + dstPitch;

    if (width == 256)
    {
	do
	{
	    uint32 *bP = (uint32 *) srcPtr;
	    uint32 *xP = (uint32 *) deltaPtr;
	    uint32 *dP = (uint32 *) dstPtr;
	    uint32 *nL = (uint32 *) nextLine;
	    uint32 currentPixel;
	    uint32 nextPixel;
	    uint32 currentDelta;
	    uint32 nextDelta;

	    finish = (uint8 *) bP + ((width + 2) << 1);
	    nextPixel = *bP++;
	    nextDelta = *xP++;

	    do
	    {
		currentPixel = nextPixel;
		currentDelta = nextDelta;
		nextPixel = *bP++;
		nextDelta = *xP++;

		if ((nextPixel != nextDelta) || (currentPixel != currentDelta))
		{
		    uint32 colorA, colorB, product, darkened;

		    *(xP - 2) = currentPixel;
#ifdef LSB_FIRST
		    colorA = currentPixel & 0xffff;
#else
		    colorA = (currentPixel & 0xffff0000) >> 16;
#endif

#ifdef LSB_FIRST
		    colorB = (currentPixel & 0xffff0000) >> 16;
		    *(dP) = product = colorA |
				      ((((colorA & colorMask) >> 1) +
					((colorB & colorMask) >> 1) +
					(colorA & colorB & lowPixelMask)) << 16);
#else
		    colorB = currentPixel & 0xffff;
		    *(dP) = product = (colorA << 16) | 
				      (((colorA & colorMask) >> 1) +
				       ((colorB & colorMask) >> 1) +
				       (colorA & colorB & lowPixelMask));
#endif
		    darkened = (product = ((product & colorMask) >> 1));
		    darkened += (product = ((product & colorMask) >> 1));
		    darkened += (product & colorMask) >> 1;
		    *(nL) = darkened;

#ifdef LSB_FIRST
		    colorA = nextPixel & 0xffff;
		    *(dP + 1) = product = colorB |
					  ((((colorA & colorMask) >> 1) +
					    ((colorB & colorMask) >> 1) +
					    (colorA & colorB & lowPixelMask)) << 16);
#else
		    colorA = (nextPixel & 0xffff0000) >> 16;
		    *(dP + 1) = product = (colorB << 16) | 
					   (((colorA & colorMask) >> 1) +
					    ((colorB & colorMask) >> 1) + 
					    (colorA & colorB & lowPixelMask));
#endif
		    darkened = (product = ((product & colorMask) >> 1));
		    darkened += (product = ((product & colorMask) >> 1));
		    darkened += (product & colorMask) >> 1;
		    *(nL + 1) = darkened;
		}

		dP += 2;
		nL += 2;
	    }
	    while ((uint8 *) bP < finish);

	    deltaPtr += srcPitch;
	    srcPtr += srcPitch;
	    dstPtr += dstPitch * 2;
	    nextLine += dstPitch * 2;
	}
	while (--height);
    }
    else
    {
	do
	{
	    uint32 *bP = (uint32 *) srcPtr;
	    uint32 *xP = (uint32 *) deltaPtr;
	    uint32 *dP = (uint32 *) dstPtr;
	    uint32 currentPixel;

	    finish = (uint8 *) bP + ((width + 2) << 1);

	    do
	    {
		currentPixel = *bP++;

		if (currentPixel != *xP++)
		{
		    uint32 product, darkened;

		    *(xP - 1) = currentPixel;
		    *dP = currentPixel;
		    darkened = (product = ((currentPixel & colorMask) >> 1));
		    darkened += (product = ((product & colorMask) >> 1));
		    darkened += (product & colorMask) >> 1;
		    *(uint32 *) ((uint8 *) dP + dstPitch) = darkened;
		}

		dP++;
	    }
	    while ((uint8 *) bP < finish);

	    deltaPtr += srcPitch;
	    srcPtr += srcPitch;
	    dstPtr += dstPitch * 2;
	}
	while (--height);
    }
}
