#ifndef SNES9X_GUI_H
#define SNES9X_GUI_H

#include <qmainwindow.h>

class QToolBar;
class QPopupMenu;

class Snes9xGUI: public QMainWindow
{
    Q_OBJECT
public:
    Snes9xGUI();
    virtual ~Snes9xGUI();
    
private slots:
    void newDoc();
    void load();
    void load( const char *fileName );
    void save();
    void closeDoc();
    void reset ();

    void toggleMenuBar();
    void toggleStatusBar();
    void toggleToolBar();

private:
    QToolBar *fileTools;
    QPopupMenu *controls;
    int mb, tb, sb;
};


#endif
