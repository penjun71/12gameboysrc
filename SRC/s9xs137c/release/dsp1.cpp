/*
 * Snes9x - Portable Super Nintendo Entertainment System (TM) emulator.
 *
 * (c) Copyright 1996 - 2001 Gary Henderson (gary@daniver.demon.co.uk) and
 *                           Jerremy Koot (jkoot@snes9x.com)
 *
 * Super FX C emulator code 
 * (c) Copyright 1997 - 1999 Ivar (Ivar@snes9x.com) and
 *                           Gary Henderson.
 * Super FX assembler emulator code (c) Copyright 1998 zsKnight and _Demo_.
 *
 * DSP1 emulator code (c) Copyright 1998 Ivar, _Demo_ and Gary Henderson.
 * C4 asm and some C emulation code (c) Copyright 2000 zsKnight and _Demo_.
 * C4 C code (c) Copyright 2001 Gary Henderson (gary@daniver.demon.co.uk).
 *
 * DOS port code contains the works of other authors. See headers in
 * individual files.
 *
 * Snes9x homepage: www.snes9x.com
 *
 * Permission to use, copy, modify and distribute Snes9x in both binary and
 * source form, for non-commercial purposes, is hereby granted without fee,
 * providing that this license information and copyright notice appear with
 * all copies and any derived work.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event shall the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Snes9x is freeware for PERSONAL USE only. Commercial users should
 * seek permission of the copyright holders first. Commercial use includes
 * charging money for Snes9x or software derived from Snes9x.
 *
 * The copyright holders request that bug fixes and improvements to the code
 * should be forwarded to them so everyone can benefit from the modifications
 * in future versions.
 *
 * Super NES and Super Nintendo Entertainment System are trademarks of
 * Nintendo Co., Limited and its subsidiary companies.
 */
#include "snes9x.h"
#include "dsp1.h"
#include "missing.h"
#include "memmap.h"
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265359
#endif

#define CLIPU8(v) if(v < 0) v = 0; else if(v > 255) v = 255
#define CLIP8(v) if(v < -128) v = -128; else if(v > 127) v = 127
#define CLIP16(v) if(v < -32768) v = -32768; else if(v > 32767) v = 32767
#define DEG(d) (M_PI * 2 * d / 360.0)

int CosTable2[256]={65536,65516,65457,65358,65220,65043,64826,64571,64276,63943,63571,63161,62713,62227,61704,61144,60546,59912,59243,58537,57796,
57020,56210,55367,54489,53579,52637,51663,50658,49622,48556,47461,46338,45187,44008,42803,41572,
40316,39036,37732,36406,35057,33688,32298,30889,29461,28015,26553,25074,23581,22073,20552,19018,17473,15918,14353,12779,11198,9610,8016,6417,4814,
3209,1601,-6,-1615,-3222,-4828,-6430,-8029,-9623,-11211,-12792,-14366,-15931,-17486,-19031,-20565,-22086,-23593,-25087,-26565,-28028,-29473,
-30901,-32310,-33700,-35069,-36417,-37743,-39047,-40327,-41583,-42813,-44018,-45197,-46348,-47471,-48565,
-49631,-50666,-51671,-52645,-53587,-54497,-55374,-56217,-57027,-57802,-58543,-59248,-59918,-60551,-61148,
-61709,-62232,-62717,-63165,-63575,-63946,-64279,-64573,-64828,-65044,-65221,-65359,-65457,-65516,-65535,
-65515,-65456,-65357,-65219,-65041,-64824,-64568,-64273,-63940,-63568,-63158,-62709,-62223,-61699,-61139,
-60541,-59907,-59237,-58531,-57790,-57014,-56203,-55359,-54482,-53571,-52629,-51654,-50649,-49613,-48547,
-47452,-46328,-45177,-43998,-42793,-41562,-40306,-39025,-37721,-36395,-35046,-33676,-32286,-30877,-29449,
-28003,-26540,-25062,-23568,-22060,-20539,-19005,-17460,-15905,-14340,-12766,-11184,-9596,-8002,-6403,
-4801,-3195,-1588,20,1628,3236,4841,6444,8043,9636,11224,12806,14379,15944,17500,19044,20578,22099,
23606,25099,26578,28040,29485,30913,32322,33711,35080,36428,37754,39058,40338,41593,42824,44028,
45206,46357,47480,48575,49640,50675,51680,52653,53595,54504,55381,56224,57034,57809,58549,59254,
59923,60557,61153,61713,62236,62721,63168,63578,63949,64281,64575,64830,65046,65223,65360,65458,65516};

int SinTable2[256]={0,1608,3215,4821,6424,8022,9616,11204,12786,14359,15924,17480,19025,20558,22079,
23587,25081,26559,28021,29467,30895,32304,33694,35063,36411,37738,39041,40322,41577,42808,44013,
45192,46343,47466,48561,49626,50662,51667,52641,53583,54493,55370,56214,57024,57799,58540,59245,
59915,60549,61146,61706,62229,62715,63163,63573,63944,64277,64572,64827,65043,65221,65358,65457,
65516,65535,65516,65456,65357,65219,65042,64825,64569,64275,63941,63570,63159,62711,62225,61702,
61141,60544,59910,59240,58534,57793,57017,56207,55363,54486,53575,52633,51659,50653,49617,48552,
47457,46333,45182,44003,42798,41567,40311,39031,37727,36400,35052,33682,32292,30883,29455,28009,
26547,25068,23574,22067,20545,19012,17467,15911,14346,12772,11191,9603,8009,6410,4807,3202,1594,
-13,-1622,-3229,-4834,-6437,-8036,-9630,-11218,-12799,-14373,-15938,-17493,-19038,-20571,-22092,
-23600,-25093,-26571,-28034,-29479,-30907,-32316,-33705,-35075,-36423,-37749,-39052,-40332,-41588,-42818,-44023,-45201,-46352,-47476,-48570,-49635,-50671,-51675,-52649,-53591,-54501,-55377,-56221,-57030,-57806,-58546,-59251,-59921,-60554,-61151,-61711,
-62234,-62719,-63167,-63576,-63947,-64280,-64574,-64829,-65045,-65222,-65359,-65458,-65516,-65535,
-65515,-65456,-65356,-65218,-65040,-64823,-64567,-64272,-63938,-63566,-63156,-62707,-62221,-61697,
-61136,-60538,-59904,-59234,-58528,-57786,-57010,-56200,-55356,-54478,-53567,-52625,-51650,-50645,
-49609,-48543,-47447,-46324,-45172,-43993,-42788,-41556,-40300,-39020,-37716,-36389,-35040,-33670,
-32280,-30871,-29443,-27997,-26534,-25056,-23562,-22054,-20532,-18999,-17454,-15898,-14333,-12759,
-11178,-9589,-7995,-6397,-4794,-3188,-1581};

int16 Op02FX = 0;
int16 Op02FY = 0;
int16 Op02FZ = 0;
int16 Op02LFE = 0;
int16 Op02LES = 0;
int16 Op02AZS = 0;
int16 Op02AAS = 0;

int16 Op02CX = 0;
int16 Op02CY = 0;
int16 Op02CXF = 0;
int16 Op02CYF = 0;
int16 Op02VOF = 0;
int16 Op02VVA = 0;

int16 Op0AVS = 0;

int16 Op0AA = 0;
int16 Op0AB = 0;
int16 Op0AC = 0;
int16 Op0AD = 0;

uint16 Op1CAZ = 0;
uint16 Op1CAX = 0;
uint16 Op1CAY = 0;
int16 Op1CX = 0;
int16 Op1CY = 0;
int16 Op1CZ = 0;
int16 Op1CX1 = 0;
int16 Op1CY1 = 0;
int16 Op1CZ1 = 0;
int16 Op1CX2 = 0;
int16 Op1CY2 = 0;
int16 Op1CZ2 = 0;
int16 Op1CX3 = 0;
int16 Op1CY3 = 0;
int16 Op1CZ3 = 0;

int32 Op03F = 0;
int32 Op03L = 0;
int32 Op03U = 0;
int16 Op03X = 0;
int16 Op03Y = 0;
int16 Op03Z = 0;

int16 Op01m = 0;
int16 Op01Zr = 0;
int16 Op01Xr = 0;
int16 Op01Yr = 0;

int16 Op0DX = 0;
int16 Op0DY = 0;
int16 Op0DZ = 0;
int32 Op0DF = 0;
int32 Op0DL = 0;
int32 Op0DU = 0;

int16 Op14Zr = 0;
int16 Op14Xr = 0;
int16 Op14Yr = 0;
int16 Op14U = 0;
int16 Op14F = 0;
int16 Op14L = 0;
int16 Op14Zrr = 0;
int16 Op14Xrr = 0;
int16 Op14Yrr = 0;

double matrix[4][4] = {0};
double smat[4][4] = {0};
double tmat[4][4] = {0};
double xmat[4][4] = {0};
double ymat[4][4] = {0};
double zmat[4][4] = {0};
double matrix0[4][4] = {0};
double matrix1[4][4] = {0};
double matrix2[4][4] = {0};
double matrixI0[4][4] = {0};
double matrixI1[4][4] = {0};
double matrixI2[4][4] = {0};

int16 DSPOp00 (int16 K, int16 I);
void DSPOp02 ();
void DSPOp0A ();
void DSPOp1C ();
void DSPOp14 ();

void DSPOp01 ();
void DSPOp11 ();
void DSPOp21 ();
void DSPOp0D ();
void DSPOp1D ();
void DSPOp2D ();
void DSPOp03 ();
void DSPOp13 ();
void DSPOp23 ();

//#define REVERSE_MATRIX_ROTATION

// Screen coordinate base (number of units pr pixel)
#define SCREEN_BASE_X 1.0
#define SCREEN_BASE_Y 1.0
//#define SCREEN_BASE_X 128.0
//#define SCREEN_BASE_Y 128.0

// Screen center X and Y (number of pixels)
#define SCREEN_CENTER_X 0.0
#define SCREEN_CENTER_Y 0.0

// Screen width and height (number of pixels)
#define SCREEN_WIDTH 256.0
#define SCREEN_HEIGHT 224.0

// Resolution of mode7 matrix
// (Number of mode7 units pr pixels in mode7 layer)
#define MODE7_RESOLUTION 256.0

double Length(VECTOR & v)
{
    return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

// These are just a couple of debug macros that I use, since
// I have limited debug facilities at the moment.
// Please don't remove them yet.
// - Lestat
#if 0
int debug_count = 0;
#define SHOW_FUNC_START() printf("%04d - Start: %s(%d):%s\n",++debug_count,__FILE__,__LINE__,__FUNCTION__)
#define SHOW_FUNC_END() printf("%04d -   End: %s(%d):%s\n",debug_count--,__FILE__,__LINE__,__FUNCTION__)
#else
#define SHOW_FUNC_START()
#define SHOW_FUNC_END()
#endif

void S9xResetDSP1 ()
{
    SHOW_FUNC_START();
    DSP1.waiting4command = TRUE;
    DSP1.in_count = 0;
    DSP1.out_count = 0;
    DSP1.in_index = 0;
    DSP1.out_index = 0;
    DSP1.first_parameter = TRUE;
    SHOW_FUNC_END();
}

uint8 S9xGetDSP (uint16 address)
{
    uint8 t;

#ifdef DEBUGGER
    if (Settings.TraceDSP)
    {
	sprintf (String, "DSP read: 0x%04X", address);
	S9xMessage (S9X_TRACE, S9X_TRACE_DSP1, String);
    }
#endif
    if ((address & 0xf000) == 0x6000 ||
	(address >= 0x8000 && address < 0xc000))
    {
	if (DSP1.out_count)
	{
	    if ((address & 1) == 0)
		t = (uint8) DSP1.output [DSP1.out_index];
	    else
	    {
		t = (uint8) (DSP1.output [DSP1.out_index] >> 8);
		DSP1.out_index++;
		if (--DSP1.out_count == 0)
		{
		    if (DSP1.command == 0x1a || DSP1.command == 0x0a)
		    {
			DSPOp0A ();
			DSP1.out_count = 4;
			DSP1.out_index = 0;
			DSP1.output [0] = Op0AA;
			DSP1.output [1] = Op0AB;
			DSP1.output [2] = Op0AC;
			DSP1.output [3] = Op0AD;
		    }
		}
		DSP1.waiting4command = TRUE;
	    }
	}
	else
	{
	    // Top Gear 3000 requires this value....
	    t = 0xff;
	}
    }
    else
	t = 0x80;

    return (t);
}

void S9xSetDSP (uint8 byte, uint16 address)
{
    SHOW_FUNC_START();
#ifdef DEBUGGER
    missing.unknowndsp_write = address;
    if (Settings.TraceDSP)
    {
	sprintf (String, "DSP write: 0x%04X=0x%02X", address, byte);
	S9xMessage (S9X_TRACE, S9X_TRACE_DSP1, String);
    }
#endif
    if ((address & 0xf000) == 0x6000 ||
	(address >= 0x8000 && address < 0xc000))
    {
	if ((address & 1) == 0)
	{
	    if (DSP1.waiting4command)
	    {
		DSP1.command = byte;
		DSP1.in_index = 0;
		DSP1.waiting4command = FALSE;
		DSP1.first_parameter = TRUE;

		// Mario Kart uses 0x00, 0x02, 0x06, 0x0c, 0x28, 0x0a
		switch (byte)
		{
		    case 0x00: DSP1.in_count = 2;	break;
		    case 0x10: DSP1.in_count = 2;	break;
		    case 0x04: DSP1.in_count = 2;	break;
		    case 0x08: DSP1.in_count = 3;	break;
		    case 0x18: DSP1.in_count = 4;	break;
		    case 0x28: DSP1.in_count = 3;	break;
		    case 0x0c: DSP1.in_count = 3;	break;
		    case 0x1c: DSP1.in_count = 6;	break;
		    case 0x02: DSP1.in_count = 7;	break;
		    case 0x0a: DSP1.in_count = 1;	break;
		    case 0x1a: DSP1.in_count = 1;	break;
		    case 0x06: DSP1.in_count = 3;	break;
		    case 0x0e: DSP1.in_count = 2;	break;
		    case 0x01: DSP1.in_count = 4;	break;
		    case 0x11: DSP1.in_count = 4;	break;
		    case 0x21: DSP1.in_count = 4;	break;
		    case 0x0d: DSP1.in_count = 3;	break;
		    case 0x1d: DSP1.in_count = 3;	break;
		    case 0x2d: DSP1.in_count = 3;	break;
		    case 0x03: DSP1.in_count = 3;	break;
		    case 0x13: DSP1.in_count = 3;	break;
		    case 0x23: DSP1.in_count = 3;	break;
		    case 0x0b: DSP1.in_count = 3;	break;
		    case 0x1b: DSP1.in_count = 3;	break;
		    case 0x2b: DSP1.in_count = 3;	break;
		    case 0x14: DSP1.in_count = 6;	break;
//		    case 0x80: DSP1.in_count = 2;	break;

		    default:
		    case 0x80:
			DSP1.in_count = 0;
			DSP1.waiting4command = TRUE;
			DSP1.first_parameter = TRUE;
			break;
		}
	    }
	    else
	    {
		DSP1.parameters [DSP1.in_index] = byte;
		DSP1.first_parameter = FALSE;
	    }
	}
	else
	{
	    if (DSP1.waiting4command ||
		(DSP1.first_parameter && byte == 0x80))
	    {
		DSP1.waiting4command = TRUE;
		DSP1.first_parameter = FALSE;
	    }
	    else
	    if (DSP1.first_parameter)
	    {
	    }
	    else
	    {
		if (DSP1.in_count)
		{
		    DSP1.parameters [DSP1.in_index] |= (byte << 8);
		    if (--DSP1.in_count == 0)
		    {
			// Actually execute the command
			DSP1.waiting4command = TRUE;
			DSP1.out_index = 0;
			switch (DSP1.command)
			{
			    case 0x00:	// Multiple
				DSP1.output [0] = DSPOp00 (
						  (int16) DSP1.parameters [0],
						  (int16) DSP1.parameters [1]);
				DSP1.out_count = 1;
				break;

			    case 0x10:	// Inverse
			    {
				int16 a = (int16) DSP1.parameters [0];
				int16 b = (int16) DSP1.parameters [1];
				double m = ldexp (double (a) / 32768.0, b);
				m = 1.0 / m;
				int c;
				m = frexp (m, &c);
				
				if (m == 1.0)
				{
				    c++;
				    m = 0.5;
				}
			        m *= 32768.0;
				DSP1.output [0] = (uint16) (int16) m;
				DSP1.output [1] = (uint16) (int16) c;
				DSP1.out_count = 2;
				break;
			    }

			    case 0x04:	// Sin and Cos of angle
			    {
				DSP1_Triangle triangle ((int16) DSP1.parameters [0],
							(int16) DSP1.parameters [1]);

				DSP1.out_count = 2;
				DSP1.output [0] = (uint16) triangle.S;
				DSP1.output [1] = (uint16) triangle.C;
				break;
			    }

			    case 0x08:	// Radius
			    {
				int32 x = (int16) DSP1.parameters [0];
				int32 y = (int16) DSP1.parameters [1];
				int32 z = (int16) DSP1.parameters [2];
				int32 s = (x * x + y * y + z * z) * 2;
				DSP1.output [0] = (int16) s; 
				DSP1.output [1] = (int16) (s >> 16);
				DSP1.out_count = 2;
				break;
			    }

			    case 0x18:	// Range
			    {
				int32 x = (int16) DSP1.parameters [0];
				int32 y = (int16) DSP1.parameters [1];
				int32 z = (int16) DSP1.parameters [2];
				int32 r = (int16) DSP1.parameters [3];
				int32 s = ((x * x + y * y + z * z - r * r) * 2) / 65536;
				DSP1.output [0] = (int16) s; 
				DSP1.out_count = 1;
				break;
			    }

			    case 0x28:	// Distance (vector length)
			    {
				int32 x = (int16) DSP1.parameters [0];
				int32 y = (int16) DSP1.parameters [1];
				int32 z = (int16) DSP1.parameters [2];
				DSP1.output [0] = (uint16) sqrt (double(x * x + y * y + z * z));
				DSP1.out_count = 1;
				break;
			    }

			    case 0x0c:	// Rotate (2D rotate)
			    {
				DSP1_Rotate rotate ((int16) DSP1.parameters [0],
						    (int16) DSP1.parameters [1],
						    (int16) DSP1.parameters [2]);
				DSP1.out_count = 2;
				DSP1.output [0] = (uint16) rotate.x2;
				DSP1.output [1] = (uint16) rotate.y2;
				break;
			    }

			    case 0x1c:	// Polar (3D rotate)
			    {
				Op1CAZ = DSP1.parameters [0];
				Op1CAY = DSP1.parameters [1];
				Op1CAX = DSP1.parameters [2];
				Op1CX1 = DSP1.parameters [3];
				Op1CY1 = DSP1.parameters [4];
				Op1CZ1 = DSP1.parameters [5];
				DSPOp1C ();
				DSP1.out_count = 3;
				DSP1.output [0] = (uint16) Op1CX3;
				DSP1.output [1] = (uint16) Op1CY3;
				DSP1.output [2] = (uint16) Op1CZ3;
				break;
			    }

			    case 0x02:	// Parameter (Projection)
				    Op02FX = DSP1.parameters [0];
				    Op02FY = DSP1.parameters [1];
				    Op02FZ = DSP1.parameters [2];
				    Op02LFE = DSP1.parameters [3];
				    Op02LES = DSP1.parameters [4];
				    Op02AAS = DSP1.parameters [5];
				    Op02AZS = DSP1.parameters [6];

				    DSPOp02 ();
				    DSP1.out_count = 4;
				    DSP1.output [0] = Op02VOF;
				    DSP1.output [1] = Op02VVA;
				    DSP1.output [2] = Op02CX;
				    DSP1.output [3] = Op02CY;
				    break;

			    case 0x1a:	// Raster mode 7 matrix data
			    case 0x0a:
				    Op0AVS = DSP1.parameters [0];
				    DSPOp0A ();
				    DSP1.out_count = 4;
				    DSP1.output [0] = Op0AA;
				    DSP1.output [1] = Op0AB;
				    DSP1.output [2] = Op0AC;
				    DSP1.output [3] = Op0AD;
				    break;

			    case 0x06:	// Project object
				{
				    DSP1_Project Object ((int16) DSP1.parameters [0],
							 (int16) DSP1.parameters [1],
							 (int16) DSP1.parameters [2]);
			
				    DSP1.out_count = 3;
				    DSP1.output [0] = Object.H;
				    DSP1.output [1] = Object.V;
				    DSP1.output [2] = Object.M;
				    break;
				}
			    
			    case 0x0e:	// Target
				// XXX:
				DSP1.output [0] = 0;
				DSP1.output [1] = 0;
				DSP1.out_count = 2;
				break;

			    // Extra commands used by Pilot Wings
			    case 0x01: // Set attitude matrix A
				Op01m = (int16) DSP1.parameters [0];
				Op01Zr = (int16) DSP1.parameters [1];
				Op01Xr = (int16) DSP1.parameters [2];
				Op01Yr = (int16) DSP1.parameters [3];
				DSPOp01 ();
				break;

			    case 0x11:	// Set attitude matrix B
				Op01m = (int16) DSP1.parameters [0];
				Op01Zr = (int16) DSP1.parameters [1];
				Op01Xr = (int16) DSP1.parameters [2];
				Op01Yr = (int16) DSP1.parameters [3];
				DSPOp11 ();
				break;

			    case 0x21:	// Set attitude matrix C
				Op01m = (int16) DSP1.parameters [0];
				Op01Zr = (int16) DSP1.parameters [1];
				Op01Xr = (int16) DSP1.parameters [2];
				Op01Yr = (int16) DSP1.parameters [3];
				DSPOp21 ();
				break;

			    case 0x0d:	// Objective matrix A
				Op0DX = (int16) DSP1.parameters [0];
				Op0DY = (int16) DSP1.parameters [1];
				Op0DZ = (int16) DSP1.parameters [2];
				DSPOp0D ();
				DSP1.output [0] = (uint16) Op0DF;
				DSP1.output [1] = (uint16) Op0DL;
				DSP1.output [2] = (uint16) Op0DU;
				DSP1.out_count = 3;
				break;

			    case 0x1d:	// Objective matrix B
				Op0DX = (int16) DSP1.parameters [0];
				Op0DY = (int16) DSP1.parameters [1];
				Op0DZ = (int16) DSP1.parameters [2];
				DSPOp1D ();
				DSP1.output [0] = (uint16) Op0DF;
				DSP1.output [1] = (uint16) Op0DL;
				DSP1.output [2] = (uint16) Op0DU;
				DSP1.out_count = 3;
				break;

			    case 0x2d:	// Objective matrix C
				Op0DX = (int16) DSP1.parameters [0];
				Op0DY = (int16) DSP1.parameters [1];
				Op0DZ = (int16) DSP1.parameters [2];
				DSPOp2D ();
				DSP1.output [0] = (uint16) Op0DF;
				DSP1.output [1] = (uint16) Op0DL;
				DSP1.output [2] = (uint16) Op0DU;
				DSP1.out_count = 3;
				break;

			    case 0x03:	// Subjective matrix A
				Op03F = ((int16) DSP1.parameters [0] * 2);
				Op03L = ((int16) DSP1.parameters [1] * 2);
				Op03U = ((int16) DSP1.parameters [2] * 2);
				DSPOp03 ();
				DSP1.output [0] = (uint16) Op03X;
				DSP1.output [1] = (uint16) Op03Y;
				DSP1.output [2] = (uint16) Op03Z;
				DSP1.out_count = 3;
				break;

			    case 0x13:	// Subjective matrix B
				Op03F = ((int16) DSP1.parameters [0] * 2);
				Op03L = ((int16) DSP1.parameters [1] * 2);
				Op03U = ((int16) DSP1.parameters [2] * 2);
				DSPOp13 ();
				DSP1.output [0] = (uint16) Op03X;
				DSP1.output [1] = (uint16) Op03Y;
				DSP1.output [2] = (uint16) Op03Z;
				DSP1.out_count = 3;
				break;

			    case 0x23:	// Subjective matrix C
				Op03F = ((int16) DSP1.parameters [0] * 2);
				Op03L = ((int16) DSP1.parameters [1] * 2);
				Op03U = ((int16) DSP1.parameters [2] * 2);
				DSPOp23 ();
				DSP1.output [0] = (uint16) Op03X;
				DSP1.output [1] = (uint16) Op03Y;
				DSP1.output [2] = (uint16) Op03Z;
				DSP1.out_count = 3;
				break;

			    case 0x0b:
				// XXX
			    case 0x1b:
			    case 0x2b:
				DSP1.out_count = 1;
				break;

			    case 0x14:	// Gyrate
#if 0
				Op14Zr = (int16) DSP1.parameters [0];
				Op14Xr = (int16) DSP1.parameters [1];
				Op14Yr = (int16) DSP1.parameters [2];
				Op14U = (int16) DSP1.parameters [3];
				Op14F = (int16) DSP1.parameters [4];
				Op14L = (int16) DSP1.parameters [5];
				DSPOp14 ();
				DSP1.output [0] = (uint16) Op14Zrr;
				DSP1.output [1] = (uint16) Op14Xrr;
				DSP1.output [2] = (uint16) Op14Yrr;
				DSP1.out_count = 3;
				break;
#endif
#if 1
{
#define AXES_3D 3
    double		Angle[AXES_3D];
    double		Delta[AXES_3D];
    double		Result;
    int			j;

    for( j = 0; j < AXES_3D; j++ )
    {
	    Angle[j] = M_PI * (((double)(int16) DSP1.parameters [j]) / 32768.0);
    }
    for( j = 0; j < AXES_3D; j++ )
    {
	    Delta[j] = M_PI * (((double)(int16) DSP1.parameters [j + 3]) / 32768.0);
    }
    Result = Angle[0];
    Result += (1.0 / cos(Angle[1])) *
	    ((Delta[0] * cos(Angle[2])) - (Delta[1] * sin(Angle[2])));
    DSP1.output [0] = (uint16) (Result * M_PI / 32768.0);
    Result = Angle[1];
    Result += (Delta[0] * sin(Angle[2])) + (Delta[1] * cos(Angle[2]));
    DSP1.output [1] = (uint16) (Result * M_PI / 32768.0);
    Result = Angle[2];
    Result -= tan(Angle[1]) *
	    ((Delta[0] * cos(Angle[2])) + (Delta[1] * sin(Angle[2])));
    DSP1.output [2] = (uint16) (Result * M_PI / 32768.0);
    DSP1.out_count = 3;
    break;
}
#endif
#if 0
			    {
				DSP1_Gyrate gyrate (DSP1.parameters [0] >> 8,
						    DSP1.parameters [1] >> 8,
						    DSP1.parameters [2] >> 8,
						    DSP1.parameters [3] >> 8,
						    DSP1.parameters [4] >> 8,
						    DSP1.parameters [5] >> 8);
				DSP1.output [0] = gyrate.Z0 << 8;
				DSP1.output [1] = gyrate.X0 << 8;
				DSP1.output [2] = gyrate.Y0 << 8;
				DSP1.out_count = 3;
				break;
			    }
#endif

			    default:
			        break;
			}
		    }
		    else
			DSP1.in_index++;
		}
	    }
	}
    }
    SHOW_FUNC_END();
}

///////////////////////// DSP1 Calculation code /////////////////////////

// Convert DSP1 type angle to a radian
#define ANGLE_TO_RAD(angle) (double(angle) * M_PI / 128.0)

// Create a unit matrix
void UnitMatrix(MATRIX &m, double scale = 1.0)
{
    SHOW_FUNC_START();
    m[0][0] = scale; m[0][1] =   0.0; m[0][2] =   0.0;
    m[1][0] =   0.0; m[1][1] = scale; m[1][2] =   0.0;
    m[2][0] =   0.0; m[2][1] =   0.0; m[2][2] = scale;
    SHOW_FUNC_END();
}

// Rotate matrix around X axis with 'a' radians
// anti-clockwise, in a right handed coordinate system
void RotateX(MATRIX &m, double a)
{
    SHOW_FUNC_START();
#ifdef REVERSE_MATRIX_ROTATION
    double cx = double(cos(-a));
    double sx = double(sin(-a));
#else
    double cx = double(cos(a));
    double sx = double(sin(a));
#endif
    double m01 = m[0][1];
    double m11 = m[1][1];
    double m21 = m[2][1];
    m[0][1] =	    m01 * cx	+ m[0][2] * sx;
    m[0][2] =	m[0][2] * cx	-     m01 * sx;
    m[1][1] =	    m11 * cx	+ m[1][2] * sx;
    m[1][2] =	m[1][2] * cx	-     m11 * sx;
    m[2][1] =	    m21 * cx	+ m[2][2] * sx;
    m[2][2] =	m[2][2] * cx	-     m21 * sx;
    SHOW_FUNC_END();
}

// Rotate matrix around Y axis with 'a' radians
// anti-clockwise, in a right handed coordinate system
void RotateY(MATRIX &m, double a)
{
    SHOW_FUNC_START();
#ifdef REVERSE_MATRIX_ROTATION
    double cy = double(cos(-a));
    double sy = double(sin(-a));
#else
    double cy = double(cos(a));
    double sy = double(sin(a));
#endif
    double m00 = m[0][0];
    double m10 = m[1][0];
    double m20 = m[2][0];
    m[0][0] =	m00 * cy	- m[0][2] * sy;
    m[0][2] =	m00 * sy	+ m[0][2] * cy;
    m[1][0] =	m10 * cy	- m[1][2] * sy;
    m[1][2] =	m10 * sy	+ m[1][2] * cy;
    m[2][0] =	m20 * cy	- m[2][2] * sy;
    m[2][2] =	m20 * sy	+ m[2][2] * cy;
    SHOW_FUNC_END();
}

// Rotate matrix around Y axis with 'a' radians
// anti-clockwise, in a right handed coordinate system
void RotateZ(MATRIX &m, double a)
{
    SHOW_FUNC_START();
#ifdef REVERSE_MATRIX_ROTATION
    double cz = double(cos(-a));
    double sz = double(sin(-a));
#else
    double cz = double(cos(a));
    double sz = double(sin(a));
#endif
    double m00 = m[0][0];
    double m10 = m[1][0];
    double m20 = m[2][0];
    m[0][0] =	    m00 * cz	+ m[0][1] * sz;
    m[0][1] =	m[0][1] * cz	-     m00 * sz;
    m[1][0] =	    m10 * cz	+ m[1][1] * sz;
    m[1][1] =	m[1][1] * cz	-     m10 * sz;
    m[2][0] =	    m20 * cz	+ m[2][1] * sz;
    m[2][1] =	m[2][1] * cz	-     m20 * sz;
    SHOW_FUNC_END();
}

void RotateVector(VECTOR &v, MATRIX &m)
{
    SHOW_FUNC_START();
    double x = v[0];
    double y = v[1];
    double z = v[2];
    v[0] = x * m[0][0] + y * m[0][1] + z * m[0][2];
    v[1] = x * m[1][0] + y * m[1][1] + z * m[1][2];
    v[2] = x * m[2][0] + y * m[2][1] + z * m[2][2];
    SHOW_FUNC_END();
}

void TransposeRotateVector(VECTOR &v, MATRIX &m)
{
    SHOW_FUNC_START();
    double x = v[0];
    double y = v[1];
    double z = v[2];
    v[0] = x * m[0][0] + y * m[1][0] + z * m[2][0];
    v[1] = x * m[0][1] + y * m[1][1] + z * m[2][1];
    v[2] = x * m[0][2] + y * m[1][2] + z * m[2][2];
    SHOW_FUNC_END();
}

void Normalize(VECTOR &v)
{
    SHOW_FUNC_START();
    double l = Length(v);
    if(l != 0.0)
    {
	v[0] /= l;
	v[1] /= l;
	v[2] /= l;
    }
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////////////////////

// This will not work if the screen coordinate specified is above the horizon
void SDSP1::ScreenToGround(VECTOR &v, double X2d, double Y2d)
{
    SHOW_FUNC_START();
    // Calculate screen ray vector
    VECTOR vRay;
    vRay[0] = X2d;
    vRay[1] = Y2d;
    vRay[2] = vFov;
    Normalize(vRay);

    // Calculate the distance from the view point to the ground at specified screen coordinate
    // (If dist value is negative, screen coordinate is above the horizon)
    double div = vRay[0]*vM[0][2] + vRay[1]*vM[1][2] + vRay[2]*vM[2][2];
    double dist = (div != 0.0) ? (vPlaneD / div) : 0.0;
    
    // Calculate ground coordinate
    v[0] = vM[2][0] * dist + vT[0];
    v[1] = vM[2][1] * dist + vT[1];
    v[2] = vM[2][2] * dist + vT[2];
    SHOW_FUNC_END();
}

MATRIX &SDSP1::GetMatrix( AttitudeMatrix Matrix )
{
    SHOW_FUNC_START();
    switch(Matrix)
    {
	default:
	case MatrixA:
	    SHOW_FUNC_END();
	    return vMa;
	case MatrixB:
	    SHOW_FUNC_END();
	    return vMb;
	case MatrixC:
	    SHOW_FUNC_END();
	    return vMc;
    }
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

DSP1_Parameter::DSP1_Parameter( int16 Fx, int16 Fy, int16 Fz,
				uint16 Lfe, uint16 Les,
				int8 Aas, int8 Azs )
{
    SHOW_FUNC_START();
    MATRIX &vM = DSP1.vM;
    VECTOR &vT = DSP1.vT;

    // Set focal distance
    DSP1.vFov = double(Les);
    
    // Create matrix.
    // (This can be done a lot faster,
    // but I just want it to work first)
    UnitMatrix(vM);
    // Rotate matrix around the Z axis using Azimuth angle (Aas)
    RotateZ(vM,ANGLE_TO_RAD(Aas));
    // Rotate matrix around the Y axis using Zenith angle (Azs)
    RotateY(vM,ANGLE_TO_RAD(Azs));

    // Calculate translation.
    double lfe = double(Lfe);
    vT[0] = (-vM[2][0] * lfe) + double(Fx);
    vT[1] = (-vM[2][1] * lfe) + double(Fy);
    vT[2] = (-vM[2][2] * lfe) + double(Fz);

    // Calculate DSP1.vPlaneD
    DSP1.vPlaneD = -(vT[0]*vT[0] + vT[1]*vT[1] + vT[2]*vT[2]);
    
    // Calculate DSP1.vHorizon
    DSP1.vHorizon = vM[1][2] != 0.0 ? ((-vM[2][2] * DSP1.vFov) / vM[1][2]) : 0.0;

    /////////////////////////////
    // Calculate output values //
    /////////////////////////////

    // Sky background X-scroll position
    Vof = int16(uint8(Aas));	// We should do some more checking on this

    // Horizon
    // (Sky background bottom position)
    double h = DSP1.vHorizon + SCREEN_CENTER_Y + 1.0;
    h *= SCREEN_BASE_Y;

    CLIP16(h);
    Vva = int16(h);

    // Start coordinate of mode7 layer
    // at left screen border, and horizon position
    VECTOR vCxy;
    DSP1.ScreenToGround(vCxy, -(SCREEN_WIDTH/2.0), DSP1.vHorizon + 1.0);

    vCxy[0] += SCREEN_CENTER_X;
    vCxy[0] *= SCREEN_BASE_X;
    vCxy[1] += SCREEN_CENTER_Y;
    vCxy[1] *= SCREEN_BASE_Y;
    
    Cx = int16(vCxy[0]);
    Cy = int16(vCxy[1]);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

DSP1_Raster::DSP1_Raster( int16 Vs )
{
    SHOW_FUNC_START();

    double vs = (double(Vs) / SCREEN_BASE_Y) - SCREEN_CENTER_X;

    VECTOR vL,vR,vN;
    DSP1.ScreenToGround(vL, -(SCREEN_WIDTH/2.0), vs);
    DSP1.ScreenToGround(vR, SCREEN_WIDTH/2.0, vs);
    DSP1.ScreenToGround(vN, -(SCREEN_WIDTH/2.0), vs + 1.0);
    double a = (vR[0] - vL[0]) * -MODE7_RESOLUTION;
    double b = (vR[1] - vL[1]) * -MODE7_RESOLUTION;
    double c = (vN[0] - vL[0]) * -MODE7_RESOLUTION;
    double d = (vN[1] - vL[1]) * -MODE7_RESOLUTION;
    CLIP16(a);
    CLIP16(b);
    CLIP16(c);
    CLIP16(d);
    An = int16(a);
    Bn = int16(b);
    Cn = int16(c);
    Dn = int16(d);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

DSP1_Target::DSP1_Target( int16 h, int16 v )
{
    SHOW_FUNC_START();
    double H = (double(h) / SCREEN_BASE_X) - SCREEN_CENTER_X;
    double V = (double(v) / SCREEN_BASE_Y) - SCREEN_CENTER_Y;
    VECTOR xy;
    DSP1.ScreenToGround(xy, H, V);
    xy[0] *= MODE7_RESOLUTION;
    xy[1] *= MODE7_RESOLUTION;
    CLIP16(xy[0]);
    CLIP16(xy[1]);
    X = int16(xy[0]);
    Y = int16(xy[1]);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

DSP1_Triangle::DSP1_Triangle (int16 A, int16 r)
{
    A = (A / 256) & 255;
    S = SinTable2 [A] * r / 65536;
    C = CosTable2 [A] * r / 65536;
}

////////////////////////////////////////////////////////////////////

DSP1_Radius::DSP1_Radius( int16 x, int16 y, int16 z )
{
    SHOW_FUNC_START();
    int32 l = int32(x)*int32(x) + int32(y)*int32(y) + int32(z)*int32(z);
    Ll = int16(l);
    Lh = int16(l>>16);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

int16 DSP1_Range( int16 x, int16 y, int16 z, int16 r )
{
    SHOW_FUNC_START();
    int32 l = int32(x)*int32(x) + int32(y)*int32(y) + int32(z)*int32(z) - int32(r)*int32(r);
    SHOW_FUNC_END();
    return int16(l);
    //return int16(l>>16); // Should it be like this instead???
}

////////////////////////////////////////////////////////////////////

int16 DSP1_Distance( int16 x, int16 y, int16 z )
{
    SHOW_FUNC_START();
    int32 l = int32(x)*int32(x) + int32(y)*int32(y) + int32(z)*int32(z);
    l = int32(sqrt(l));
    CLIP16(l);
    SHOW_FUNC_END();
    return int16(l);
}

////////////////////////////////////////////////////////////////////

DSP1_Rotate::DSP1_Rotate (int16 A, int16 x1, int16 y1)
{
    A = (A / 256) & 255;

    x2 = (x1 *  CosTable2 [A] + y1 * SinTable2 [A]) / 65536;
    y2 = (x1 * -SinTable2 [A] + y1 * CosTable2 [A]) / 65536;

#if 0
    SHOW_FUNC_START();
    double a = ANGLE_TO_RAD(A);
    double s = sin(a);
    double c = cos(a);
    double x = c * double(x1) - s * double(y1);
    double y = s * double(x1) + c * double(y1);
    CLIP16(x);
    CLIP16(y);
    x2 = int16(x);
    y2 = int16(y);
    SHOW_FUNC_END();
#endif
}

////////////////////////////////////////////////////////////////////

DSP1_Polar::DSP1_Polar( int8 Za, int8 Xa, int8 Ya, int16 x, int16 y, int16 z )
{
    SHOW_FUNC_START();
    VECTOR v;
    v[0] = double(x);
    v[1] = double(y);
    v[2] = double(z);

    MATRIX m;
    UnitMatrix(m);
    RotateY(m,ANGLE_TO_RAD(Ya));
    RotateX(m,ANGLE_TO_RAD(Xa));
    RotateZ(m,ANGLE_TO_RAD(Za));

    RotateVector(v,m);

    CLIP16(v[0]);
    CLIP16(v[1]);
    CLIP16(v[2]);

    X = int16(v[0]);
    Y = int16(v[1]);
    Z = int16(v[2]);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

void DSP1_Attitude( int16 m, int8 Za, int8 Xa, int8 Ya, AttitudeMatrix Matrix )
{
    SHOW_FUNC_START();
    MATRIX &mat = DSP1.GetMatrix(Matrix);
    UnitMatrix( mat, double(m) / 32768.0 );
    RotateZ( mat, ANGLE_TO_RAD(Za) );
    RotateX( mat, ANGLE_TO_RAD(Xa) );
    RotateY( mat, ANGLE_TO_RAD(Ya) );
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

DSP1_Objective::DSP1_Objective( int16 x, int16 y, int16 z, AttitudeMatrix Matrix )
{
    SHOW_FUNC_START();
    MATRIX &mat = DSP1.GetMatrix(Matrix);
    VECTOR v;
    v[0] = double(x);
    v[1] = double(y);
    v[2] = double(z);
    TransposeRotateVector(v,mat);
    CLIP16(v[0]);
    CLIP16(v[1]);
    CLIP16(v[2]);
    F = int16(v[0]);
    L = int16(v[1]);
    U = int16(v[2]);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

DSP1_Subjective::DSP1_Subjective( int16 F, int16 L, int16 U, AttitudeMatrix Matrix )
{
    SHOW_FUNC_START();
    MATRIX &mat = DSP1.GetMatrix(Matrix);
    VECTOR v;
    v[0] = double(F);
    v[1] = double(L);
    v[2] = double(U);
    RotateVector(v,mat);
    CLIP16(v[0]);
    CLIP16(v[1]);
    CLIP16(v[2]);
    X = int16(v[0]);
    Y = int16(v[1]);
    Z = int16(v[2]);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

int16 DSP1_Scalar( int16 x, int16 y, int16 z, AttitudeMatrix Matrix )
{
    SHOW_FUNC_START();
    MATRIX &mat = DSP1.GetMatrix(Matrix);
    double s = mat[0][0] * double(x) + mat[0][1] * double(y) + mat[0][2] * double(z);
    CLIP16(s);
    SHOW_FUNC_END();
    return int16(s);
}

////////////////////////////////////////////////////////////////////

DSP1_Gyrate::DSP1_Gyrate( int8 Zi, int8 Xi, int8 Yi, int8 dU, int8 dF, int8 dL )
{
    SHOW_FUNC_START();
    double zi = ANGLE_TO_RAD(Zi);
    double xi = ANGLE_TO_RAD(Xi);
    double yi = ANGLE_TO_RAD(Yi);
    double du = ANGLE_TO_RAD(dU);
    double df = ANGLE_TO_RAD(dF);
    double dl = ANGLE_TO_RAD(dL);

    double sxi = 1 / cos(xi); // sec(xi) ????
    double cyi = cos(yi);
    double syi = sin(yi);
    double txi = tan(xi);

    double z0 = zi + sxi * (du * cyi - df * syi);
    double x0 = xi + (du * syi + df * cyi);
    double y0 = yi - txi * (du * cyi + df * syi) + dl;

    Z0 = int8(z0);
    X0 = int8(x0);
    Y0 = int8(y0);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

int16 DSP1_Multiply( int16 k, int16 I )
{
    SHOW_FUNC_START();
    int32 v = (int32(k) * int32(I)) >> 15;
    if(v < -32768) v = -32768;
    else if(v > 32767) v = 32767;
    SHOW_FUNC_END();
    return int16(v);
}

////////////////////////////////////////////////////////////////////

DSP1_Inverse::DSP1_Inverse( int16 a, int16 b )
{
    SHOW_FUNC_START();
    double m =double(ldexp(double(a) / 32768.0, b));
    m = 1.0 / m;
    int c;
    m = double(frexp(m,&c));
    if(m == 1.0)
    {
	c++;
	m = 0.5;
    }
    m *= 32768.0;
    CLIP16(m);
    A = int16(m);
    B = int16(c);
    SHOW_FUNC_END();
}

////////////////////////////////////////////////////////////////////

int CosTable[256]={
 65536,  65516,  65457,  65358,  65220,  65043,  64826,  64571, 
 64276,  63943,  63571,  63161,  62713,  62227,  61704,  61144, 
 60546,  59912,  59243,  58537,  57796,  57020,  56210,  55367, 
 54489,  53579,  52637,  51663,  50658,  49622,  48556,  47461, 
 46338,  45187,  44008,  42803,  41572,  40316,  39036,  37732, 
 36406,  35057,  33688,  32298,  30889,  29461,  28015,  26553, 
 25074,  23581,  22073,  20552,  19018,  17473,  15918,  14353, 
 12779,  11198,   9610,   8016,   6417,   4814,   3209,   1601, 
    -6,  -1615,  -3222,  -4828,  -6430,  -8029,  -9623, -11211, 
-12792, -14366, -15931, -17486, -19031, -20565, -22086, -23593, 
-25087, -26565, -28028, -29473, -30901, -32310, -33700, -35069, 
-36417, -37743, -39047, -40327, -41583, -42813, -44018, -45197, 
-46348, -47471, -48565, -49631, -50666, -51671, -52645, -53587, 
-54497, -55374, -56217, -57027, -57802, -58543, -59248, -59918, 
-60551, -61148, -61709, -62232, -62717, -63165, -63575, -63946, 
-64279, -64573, -64828, -65044, -65221, -65359, -65457, -65516, 
-65535, -65515, -65456, -65357, -65219, -65041, -64824, -64568, 
-64273, -63940, -63568, -63158, -62709, -62223, -61699, -61139, 
-60541, -59907, -59237, -58531, -57790, -57014, -56203, -55359, 
-54482, -53571, -52629, -51654, -50649, -49613, -48547, -47452, 
-46328, -45177, -43998, -42793, -41562, -40306, -39025, -37721, 
-36395, -35046, -33676, -32286, -30877, -29449, -28003, -26540, 
-25062, -23568, -22060, -20539, -19005, -17460, -15905, -14340, 
-12766, -11184,  -9596,  -8002,  -6403,  -4801,  -3195,  -1588, 
    20,   1628,   3236,   4841,   6444,   8043,   9636,  11224, 
 12806,  14379,  15944,  17500,  19044,  20578,  22099,  23606, 
 25099,  26578,  28040,  29485,  30913,  32322,  33711,  35080, 
 36428,  37754,  39058,  40338,  41593,  42824,  44028,  45206, 
 46357,  47480,  48575,  49640,  50675,  51680,  52653,  53595, 
 54504,  55381,  56224,  57034,  57809,  58549,  59254,  59923, 
 60557,  61153,  61713,  62236,  62721,  63168,  63578,  63949, 
 64281,  64575,  64830,  65046,  65223,  65360,  65458,  65516, 
};

int SinTable[256]={
     0,   1608,   3215,   4821,   6424,   8022,   9616,  11204, 
 12786,  14359,  15924,  17480,  19025,  20558,  22079,  23587, 
 25081,  26559,  28021,  29467,  30895,  32304,  33694,  35063, 
 36411,  37738,  39041,  40322,  41577,  42808,  44013,  45192, 
 46343,  47466,  48561,  49626,  50662,  51667,  52641,  53583, 
 54493,  55370,  56214,  57024,  57799,  58540,  59245,  59915, 
 60549,  61146,  61706,  62229,  62715,  63163,  63573,  63944, 
 64277,  64572,  64827,  65043,  65221,  65358,  65457,  65516, 
 65535,  65516,  65456,  65357,  65219,  65042,  64825,  64569, 
 64275,  63941,  63570,  63159,  62711,  62225,  61702,  61141, 
 60544,  59910,  59240,  58534,  57793,  57017,  56207,  55363, 
 54486,  53575,  52633,  51659,  50653,  49617,  48552,  47457, 
 46333,  45182,  44003,  42798,  41567,  40311,  39031,  37727, 
 36400,  35052,  33682,  32292,  30883,  29455,  28009,  26547, 
 25068,  23574,  22067,  20545,  19012,  17467,  15911,  14346, 
 12772,  11191,   9603,   8009,   6410,   4807,   3202,   1594, 
   -13,  -1622,  -3229,  -4834,  -6437,  -8036,  -9630, -11218, 
-12799, -14373, -15938, -17493, -19038, -20571, -22092, -23600, 
-25093, -26571, -28034, -29479, -30907, -32316, -33705, -35075, 
-36423, -37749, -39052, -40332, -41588, -42818, -44023, -45201, 
-46352, -47476, -48570, -49635, -50671, -51675, -52649, -53591, 
-54501, -55377, -56221, -57030, -57806, -58546, -59251, -59921, 
-60554, -61151, -61711, -62234, -62719, -63167, -63576, -63947, 
-64280, -64574, -64829, -65045, -65222, -65359, -65458, -65516, 
-65535, -65515, -65456, -65356, -65218, -65040, -64823, -64567, 
-64272, -63938, -63566, -63156, -62707, -62221, -61697, -61136, 
-60538, -59904, -59234, -58528, -57786, -57010, -56200, -55356, 
-54478, -53567, -52625, -51650, -50645, -49609, -48543, -47447, 
-46324, -45172, -43993, -42788, -41556, -40300, -39020, -37716, 
-36389, -35040, -33670, -32280, -30871, -29443, -27997, -26534, 
-25056, -23562, -22054, -20532, -18999, -17454, -15898, -14333, 
-12759, -11178,  -9589,  -7995,  -6397,  -4794,  -3188,  -1581, 
};

int16 DSPOp00 (int16 K, int16 I)
{
    return ((int32) K * I / 32768);
}

double ScreenLX1 = 0;
double ScreenLY1 = 0;
double ScreenLZ1 = 0;

double RasterRX = 0;
double RasterLX = 0;
double RasterRY = 0;
double RasterLY = 0;
double RasterRZ = 0;
double RasterLZ = 0;
double Distance = 0;
double RasterLSlopeX = 0;
double RasterLSlopeY = 0;
double RasterLSlopeZ = 0;
double RasterRSlopeX = 0;
double RasterRSlopeY = 0;
double RasterRSlopeZ = 0;
double GroundLX = 0;
double GroundLY = 0;
double GroundRX = 0;
double GroundRY = 0;

int32 ViewerAX = 0;
int32 ViewerAY = 0;
int32 ViewerAZ = 0;

double ViewerX0 = 0;
double ViewerX1 = 0;
double ViewerY1 = 0;
double ViewerZ1 = 0;

double ViewerX = 0;
double ViewerY = 0;
double ViewerZ = 0;

double ScreenX = 0;
double ScreenY = 0;
double ScreenZ = 0;
double NumberOfSlope = 0;

void DSPOp02 ()
{
    double azs = Op02AZS * 6.2832 / 65536.0;
    double aas = Op02AAS * 6.2832 / 65536.0;
    ViewerZ1 = -cos (azs);
    ViewerX1 = sin (azs) * sin (aas);
    ViewerY1 = sin (azs) * cos (-aas);

    ViewerX = Op02FX - ViewerX1 * Op02LFE;
    ViewerY = Op02FY - ViewerY1 * Op02LFE;
    ViewerZ = Op02FZ - ViewerZ1 * Op02LFE;

    ScreenX = Op02FX + ViewerX1 * (Op02LES - Op02LFE);
    ScreenY = Op02FY + ViewerY1 * (Op02LES - Op02LFE);
    ScreenZ = Op02FZ + ViewerZ1 * (Op02LES - Op02LFE);

    if (ViewerZ1 == 0)
	ViewerZ1++;

    NumberOfSlope = ViewerZ / -ViewerZ1;

    Op02CX = int16(ViewerX + ViewerX1 * NumberOfSlope);
    Op02CY = int16(ViewerY + ViewerY1 * NumberOfSlope);

    Op02CXF = int16(ViewerX + ViewerX1 * NumberOfSlope);
    Op02CYF = int16(ViewerY + ViewerY1 * NumberOfSlope);

    Op02VOF = 0x0000;

    if (Op02LFE == 0x2200)
	Op02VVA = (int16) 0xFECD;
    else
	Op02VVA = (int16) 0xFFB2;
}

#if 0
void DSPOp0A ()
{
    if (Op0AVS == 0)
	Op0AVS++;

// The view angle with a rotation of 3/4 (pointing up screen i think)
    ViewerAY = ((128 + (Op02AZS / 256) + 192)) & 255;	// X/Z
    ViewerAZ = ((128 + (Op02AAS / 256))) & 255;		// X/Y

    ViewerX0 = (-SinTable[(int32) ViewerAY]);
    ScreenLX1 = (ViewerX0 * -SinTable[ViewerAZ]) / 65536;
    ScreenLY1 = (ViewerX0 * CosTable[ViewerAZ]) / 65536;
    ScreenLZ1 = (CosTable[ViewerAY]);

// The left/right points after the up/down move
    RasterRX = RasterLX = ScreenX + (Op0AVS) * ScreenLX1 / 65536;
    RasterRY = RasterLY = ScreenY + (Op0AVS) * ScreenLY1 / 65536;
    RasterRZ = RasterLZ = ScreenZ + (Op0AVS) * ScreenLZ1 / 65536;

// The angle to move right
    ViewerAZ = (128 + (Op02AAS / 256) + 64) & 255;	// X/Y

    ScreenLX1 = (-SinTable[ViewerAZ]);
    ScreenLY1 = (CosTable[ViewerAZ]);

// (move left, -112 in the right direction)
    RasterLX = RasterLX - 112 * ScreenLX1 / 65536;
    RasterLY = RasterLY - 112 * ScreenLY1 / 65536;

// move right, 112 in the right direction)
    RasterRX = RasterRX + 112 * ScreenLX1 / 65536;
    RasterRY = RasterRY + 112 * ScreenLY1 / 65536;

// The left/right point on the screen after going right/left
    Distance = Op02LFE;
    if (Distance == 0)
	Distance = 1;

// The slope to go to the ground (the R/L were on the screen)
    RasterLSlopeX = 65536 * (RasterLX - ViewerX) / Distance;
    RasterLSlopeY = 65536 * (RasterLY - ViewerY) / Distance;
    RasterLSlopeZ = 65536 * (RasterLZ - ViewerZ) / Distance;
    RasterRSlopeX = 65536 * (RasterRX - ViewerX) / Distance;
    RasterRSlopeY = 65536 * (RasterRY - ViewerY) / Distance;
    RasterRSlopeZ = 65536 * (RasterRZ - ViewerZ) / Distance;

    if (RasterLSlopeZ == 0)
	RasterLSlopeZ++;

    NumberOfSlope = ViewerZ * 65536 / -RasterLSlopeZ;

    GroundLX = ViewerX * 65536 + RasterLSlopeX * NumberOfSlope;
    GroundLY = ViewerY * 65536 + RasterLSlopeY * NumberOfSlope;

    if (RasterRSlopeZ == 0)
	RasterRSlopeZ++;

    NumberOfSlope = ViewerZ * 65536 / -RasterRSlopeZ;

// The position of the left/right point on the ground
    GroundRX = ViewerX * 65536 + RasterRSlopeX * NumberOfSlope;
    GroundRY = ViewerY * 65536 + RasterRSlopeY * NumberOfSlope;

    Op0AA = (int16) ((GroundRX - GroundLX) / 65536); // / 2
    Op0AB = (int16) ((GroundRY - GroundLY) / 65536 / 2);
    Op0AC = (int16) ((((Op02CXF * 65536) - (GroundRX + GroundLX) / 2)) / (-(Op0AVS) * 256));
    Op0AD = (int16) ((((Op02CYF * 65536) - (GroundRY + GroundLY) / 2)) / (-(Op0AVS) * 256));

    Op0AVS += 1;
}
#else
void DSPOp0A ()
{
    if (Op0AVS == 0)
	Op0AVS = 1;

    ScreenLZ1 = -cos ((Op02AZS - 16384.0) * 6.2832 / 65536.0);
    ScreenLX1 = sin ((Op02AZS - 16384.0) * 6.2832 / 65536.0) *
	       -sin (Op02AAS * 6.2832 / 65536.0);
    ScreenLY1 = -sin ((Op02AZS - 16384.0) * 6.2832 / 65536.0) *
	        -cos (-Op02AAS * 6.2832 / 65536.0);
    RasterRX = RasterLX = ScreenX + Op0AVS * ScreenLX1;
    RasterRY = RasterLY = ScreenY + Op0AVS * ScreenLY1;
    RasterRZ = RasterLZ = ScreenZ + Op0AVS * ScreenLZ1;
    ScreenLX1 = sin ((Op02AAS + 16384.0) * 6.2832 / 65536);
    ScreenLY1 = cos (-(Op02AAS + 16384.0) * 6.2832 / 65536);
    RasterLX = RasterLX - 128 * ScreenLX1;
    RasterLY = RasterLY - 128 * ScreenLY1;
    RasterRX = RasterRX + 128 * ScreenLX1;
    RasterRY = RasterRY + 128 * ScreenLY1;
    Distance = Op02LFE;

    if (Distance == 0)
	Distance = 1;

    RasterLSlopeX = (RasterLX - ViewerX) / Distance;
    RasterLSlopeY = (RasterLY - ViewerY) / Distance;
    RasterLSlopeZ = (RasterLZ - ViewerZ) / Distance;
    RasterRSlopeX = (RasterRX - ViewerX) / Distance;
    RasterRSlopeY = (RasterRY - ViewerY) / Distance;
    RasterRSlopeZ = (RasterRZ - ViewerZ) / Distance;

    if (RasterLSlopeZ == 0)
	RasterLSlopeZ++;

    NumberOfSlope = ViewerZ / -RasterLSlopeZ;
    GroundLX = ViewerX + RasterLSlopeX * NumberOfSlope;
    GroundLY = ViewerY + RasterLSlopeY * NumberOfSlope;

    if (RasterRSlopeZ == 0)
	RasterRSlopeZ++;

    NumberOfSlope = ViewerZ / -RasterRSlopeZ;
    GroundRX = ViewerX + RasterRSlopeX * NumberOfSlope;
    GroundRY = ViewerY + RasterRSlopeY * NumberOfSlope;

    if (Op02LES == 0)
	Op02LES = 1;

    Op0AA = int16(GroundRX - GroundLX);
    Op0AB = int16(GroundRY - GroundLY);

    if (Op0AVS != 0)
    {
	Op0AC = int16((((Op02CXF - ((GroundRX + GroundLX) / 2.0))) / Op0AVS) * 256.0);
	Op0AD = int16((((Op02CYF - ((GroundRY + GroundLY) / 2.0))) / Op0AVS) * 256.0);
    }
    else
    {
	Op0AC = 256;
	Op0AD = 256;
    }
    Op0AVS += 1;
}
#endif

DSP1_Project::DSP1_Project (int16 x, int16 y, int16 z)
{
    double X = double(x) - Op02CXF;
    double Y = double(y) - Op02CYF;
    double Z = double(z);

    double X1;
    double Y1;
    double Z1;
    double X2;
    double Y2;
    double Z2;
    double tempA; 
    double cx;
    double sx;

    // AAS rotate around Z
    tempA = (-Op02AAS + 32768) / 65536.0 * 6.2832;
    cx = cos (tempA);
    sx = sin (tempA);

    X1 = (X * cx + Y * -sx);
    Y1 = (X * sx + Y * cx);
    Z1 = Z;

    tempA = (-Op02AZS) / 65536.0 * 6.2832;
    cx = cos (tempA);
    sx = sin (tempA);

    // AZS rotate around X
    X2 = X1;
    Y2 = (Y1 * cx +Z1 * -sx);
    Z2 = (Y1 * sx +Z1 * cx);

    Z2 = Z2 - Op02LFE;

    if (Z2<0)
    {
	H = int16(-X2 * Op02LES / -(Z2));
	V = int16(-Y2 * Op02LES / -(Z2));
	M = int32(256.0 * Op02LES / -Z2);
    }
    else
    {
	H = 0;
	V = 224;
    } 
}

void DSPOp1C()
{
    // rotate around Y
    uint32 a = Op1CAY >> 8;
    Op1CX1 = (Op1CX * CosTable2 [a] + Op1CZ * SinTable2 [a]) / 65536;
    Op1CY1 = Op1CY;
    Op1CZ1 = (Op1CX * -SinTable2 [a] + Op1CZ * CosTable2 [a]) / 65536;
    // rotate around X
    a = Op1CAX >> 8;
    Op1CX2 = Op1CX1;
    Op1CY2 = (Op1CY1 * CosTable2 [a] + Op1CZ1 * -SinTable2 [a]) / 65536;
    Op1CZ2 = (Op1CY1 * SinTable2 [a] + Op1CZ1 * CosTable2[a]) / 65536;
    // rotate around Z
    a = Op1CAZ >> 8;
    Op1CX3 = (Op1CX2 * CosTable2 [a] + Op1CY2 * -SinTable2 [a]) / 65536;
    Op1CY3 = (Op1CX2 * SinTable2 [a] + Op1CY2 * CosTable2 [a]) / 65536;
    Op1CZ3 = Op1CZ2;
}

static void InitMatrix ()
{
    matrix[0][0]=1; matrix[0][1]=0; matrix[0][2]=0; matrix[0][3]=0;
    matrix[1][0]=0; matrix[1][1]=1; matrix[1][2]=0; matrix[1][3]=0;
    matrix[2][0]=0; matrix[2][1]=0; matrix[2][2]=1; matrix[2][3]=0;
    matrix[3][0]=0; matrix[3][1]=0; matrix[3][2]=0; matrix[3][3]=1;
}

static void MultMatrix(double result[4][4],double mat1[4][4],double mat2[4][4])
{
    result[0][0]=0;
    result[0][0]+=(mat1[0][0]*mat2[0][0]+mat1[0][1]*mat2[1][0]+mat1[0][2]*mat2[2][0]+mat1[0][3]*mat2[3][0]);
    result[0][1]=0;
    result[0][1]+=(mat1[0][0]*mat2[0][1]+mat1[0][1]*mat2[1][1]+mat1[0][2]*mat2[2][1]+mat1[0][3]*mat2[3][1]);
    result[0][2]=0;
    result[0][2]+=(mat1[0][0]*mat2[0][2]+mat1[0][1]*mat2[1][2]+mat1[0][2]*mat2[2][2]+mat1[0][3]*mat2[3][2]);
    result[0][3]=0;
    result[0][3]+=(mat1[0][0]*mat2[0][3]+mat1[0][1]*mat2[1][3]+mat1[0][2]*mat2[2][3]+mat1[0][3]*mat2[3][3]);

    result[1][0]=0;
    result[1][0]+=(mat1[1][0]*mat2[0][0]+mat1[1][1]*mat2[1][0]+mat1[1][2]*mat2[2][0]+mat1[1][3]*mat2[3][0]);
    result[1][1]=0;
    result[1][1]+=(mat1[1][0]*mat2[0][1]+mat1[1][1]*mat2[1][1]+mat1[1][2]*mat2[2][1]+mat1[1][3]*mat2[3][1]);
    result[1][2]=0;
    result[1][2]+=(mat1[1][0]*mat2[0][2]+mat1[1][1]*mat2[1][2]+mat1[1][2]*mat2[2][2]+mat1[1][3]*mat2[3][2]);
    result[1][3]=0;
    result[1][3]+=(mat1[1][0]*mat2[0][3]+mat1[1][1]*mat2[1][3]+mat1[1][2]*mat2[2][3]+mat1[1][3]*mat2[3][3]);

    result[2][0]=0;
    result[2][0]+=(mat1[2][0]*mat2[0][0]+mat1[2][1]*mat2[1][0]+mat1[2][2]*mat2[2][0]+mat1[2][3]*mat2[3][0]);
    result[2][1]=0;
    result[2][1]+=(mat1[2][0]*mat2[0][1]+mat1[2][1]*mat2[1][1]+mat1[2][2]*mat2[2][1]+mat1[2][3]*mat2[3][1]);
    result[2][2]=0;
    result[2][2]+=(mat1[2][0]*mat2[0][2]+mat1[2][1]*mat2[1][2]+mat1[2][2]*mat2[2][2]+mat1[2][3]*mat2[3][2]);
    result[2][3]=0;                    
    result[2][3]+=(mat1[2][0]*mat2[0][3]+mat1[2][1]*mat2[1][3]+mat1[2][2]*mat2[2][3]+mat1[2][3]*mat2[3][3]);

    result[3][0]=0;
    result[3][0]+=(mat1[3][0]*mat2[0][0]+mat1[3][1]*mat2[1][0]+mat1[3][2]*mat2[2][0]+mat1[3][3]*mat2[3][0]);
    result[3][1]=0;
    result[3][1]+=(mat1[3][0]*mat2[0][1]+mat1[3][1]*mat2[1][1]+mat1[3][2]*mat2[2][1]+mat1[3][3]*mat2[3][1]);
    result[3][2]=0;
    result[3][2]+=(mat1[3][0]*mat2[0][2]+mat1[3][1]*mat2[1][2]+mat1[3][2]*mat2[2][2]+mat1[3][3]*mat2[3][2]);
    result[3][3]=0;
    result[3][3]+=(mat1[3][0]*mat2[0][3]+mat1[3][1]*mat2[1][3]+mat1[3][2]*mat2[2][3]+mat1[3][3]*mat2[3][3]);
}

static void CopyMatrix(double dest[4][4],double source[4][4])
{
    dest[0][0]=source[0][0];
    dest[0][1]=source[0][1];
    dest[0][2]=source[0][2];
    dest[0][3]=source[0][3];
    dest[1][0]=source[1][0];
    dest[1][1]=source[1][1];
    dest[1][2]=source[1][2];
    dest[1][3]=source[1][3];
    dest[2][0]=source[2][0];
    dest[2][1]=source[2][1];
    dest[2][2]=source[2][2];
    dest[2][3]=source[2][3];
    dest[3][0]=source[3][0];
    dest[3][1]=source[3][1];
    dest[3][2]=source[3][2];
    dest[3][3]=source[3][3];
}

static void scale(double sf)
{
    double mat[4][4];
    smat[0][0]=sf; smat[0][1]=0; smat[0][2]=0; smat[0][3]=0;
    smat[1][0]=0; smat[1][1]=sf; smat[1][2]=0; smat[1][3]=0;
    smat[2][0]=0; smat[2][1]=0; smat[2][2]=sf; smat[2][3]=0;
    smat[3][0]=0; smat[3][1]=0; smat[3][2]=0; smat[3][3]=1;
    MultMatrix(mat,smat,matrix);
    CopyMatrix(matrix,mat);
}

static void translate(double xt,double yt,double zt)
{
    double mat[4][4];
    tmat[0][0]=1; tmat[0][1]=0; tmat[0][2]=0; tmat[0][3]=0;
    tmat[1][0]=0; tmat[1][1]=1; tmat[1][2]=0; tmat[1][3]=0;
    tmat[2][0]=0; tmat[2][1]=0; tmat[2][2]=1; tmat[2][3]=0;
    tmat[3][0]=xt; tmat[3][1]=yt; tmat[3][2]=zt; tmat[3][3]=1;
    MultMatrix(mat,matrix,tmat);
    CopyMatrix(matrix,mat);
}

static void rotate(double ax,double ay,double az)
{
    double mat1[4][4];
    double mat2[4][4];
    xmat[0][0]=1; xmat[0][1]=0; xmat[0][2]=0; xmat[0][3]=0;
    xmat[1][0]=0; xmat[1][1]=cos(ax); xmat[1][2]=sin(ax); xmat[1][3]=0;
    xmat[2][0]=0; xmat[2][1]=-(sin(ax)); xmat[2][2]=cos(ax); xmat[2][3]=0;
    xmat[3][0]=0; xmat[3][1]=0; xmat[3][2]=0; xmat[3][3]=1;
    MultMatrix(mat1,xmat,matrix);
    ymat[0][0]=cos(ay); ymat[0][1]=0; ymat[0][2]=-(sin(ay)); ymat[0][3]=0;
    ymat[1][0]=0; ymat[1][1]=1; ymat[1][2]=0; ymat[1][3]=0;
    ymat[2][0]=sin(ay); ymat[2][1]=0; ymat[2][2]=cos(ay); ymat[2][3]=0;
    ymat[3][0]=0; ymat[3][1]=0; ymat[3][2]=0; ymat[3][3]=1;
    MultMatrix(mat2,ymat,mat1);
    zmat[0][0]=cos(az); zmat[0][1]=sin(az); zmat[0][2]=0; zmat[0][3]=0;
    zmat[1][0]=-(sin(az)); zmat[1][1]=cos(az); zmat[1][2]=0; zmat[1][3]=0;
    zmat[2][0]=0; zmat[2][1]=0; zmat[2][2]=1; zmat[2][3]=0;
    zmat[3][0]=0; zmat[3][1]=0; zmat[3][2]=0; zmat[3][3]=1;
    MultMatrix(matrix,zmat,mat2);
}

void DSPOp01()
{
    InitMatrix();
    rotate(Op01Xr/65536.0*6.2832,Op01Yr/65536.0*6.2832,Op01Zr/65536.0*6.2832);
    CopyMatrix(matrix0,matrix);
    InitMatrix();
    rotate(0,0,Op01Zr/65536.0*6.2832);
    rotate(Op01Xr/65536.0*6.2832,0,0);
    rotate(0,Op01Yr/65536.0*6.2832,0);
    CopyMatrix(matrixI0,matrix);
}

void DSPOp11 ()
{
    InitMatrix();
    rotate(Op01Xr/65536.0*6.2832,Op01Yr/65536.0*6.2832,Op01Zr/65536.0*6.2832);
    CopyMatrix(matrix1,matrix);
    InitMatrix();
    rotate(0,0,Op01Zr/65536.0*6.2832);
    rotate(Op01Xr/65536.0*6.2832,0,0);
    rotate(0,Op01Yr/65536.0*6.2832,0);
    CopyMatrix(matrixI1,matrix);
}

void DSPOp21 ()
{
    InitMatrix();
    rotate(Op01Xr/65536.0*6.2832,Op01Yr/65536.0*6.2832,Op01Zr/65536.0*6.2832);
    CopyMatrix(matrix2,matrix);
    InitMatrix();
    rotate(0,0,Op01Zr/65536.0*6.2832);
    rotate(Op01Xr/65536.0*6.2832,0,0);
    rotate(0,Op01Yr/65536.0*6.2832,0);
    CopyMatrix(matrixI2,matrix);
}

void DSPOp0D ()
{
    Op0DF=(int32)((Op0DX*matrixI0[0][0]+Op0DY*matrixI0[1][0]+Op0DZ*matrixI0[2][0]+matrixI0[3][0]) / 2);
    Op0DL=(int32)((Op0DX*matrixI0[0][1]+Op0DY*matrixI0[1][1]+Op0DZ*matrixI0[2][1]+matrixI0[3][1]) / 2);
    Op0DU=(int32)((Op0DX*matrixI0[0][2]+Op0DY*matrixI0[1][2]+Op0DZ*matrixI0[2][2]+matrixI0[3][2]) / 2);
}

void DSPOp1D ()
{
    Op0DF=(int32)((Op0DX*matrixI1[0][0]+Op0DY*matrixI1[1][0]+Op0DZ*matrixI1[2][0]+matrixI1[3][0]) / 2);
    Op0DL=(int32)((Op0DX*matrixI1[0][1]+Op0DY*matrixI1[1][1]+Op0DZ*matrixI1[2][1]+matrixI1[3][1]) / 2);
    Op0DU=(int32)((Op0DX*matrixI1[0][2]+Op0DY*matrixI1[1][2]+Op0DZ*matrixI1[2][2]+matrixI1[3][2]) / 2);
}

void DSPOp2D ()
{
    Op0DF=(int32)((Op0DX*matrixI2[0][0]+Op0DY*matrixI2[1][0]+Op0DZ*matrixI2[2][0]+matrixI2[3][0]) / 2);
    Op0DL=(int32)((Op0DX*matrixI2[0][1]+Op0DY*matrixI2[1][1]+Op0DZ*matrixI2[2][1]+matrixI2[3][1]) / 2);
    Op0DU=(int32)((Op0DX*matrixI2[0][2]+Op0DY*matrixI2[1][2]+Op0DZ*matrixI2[2][2]+matrixI2[3][2]) / 2);
}

void DSPOp03 ()
{
    Op03X=(int32)(Op03F*matrix0[0][0]+Op03L*matrix0[1][0]+Op03U*matrix0[2][0]+matrix0[3][0]);
    Op03Y=(int32)(Op03F*matrix0[0][1]+Op03L*matrix0[1][1]+Op03U*matrix0[2][1]+matrix0[3][1]);
    Op03Z=(int32)(Op03F*matrix0[0][2]+Op03L*matrix0[1][2]+Op03U*matrix0[2][2]+matrix0[3][2]);
}

void DSPOp13 ()
{
    Op03X=(int32)(Op03F*matrix1[0][0]+Op03L*matrix1[1][0]+Op03U*matrix1[2][0]+matrix1[3][0]);
    Op03Y=(int32)(Op03F*matrix1[0][1]+Op03L*matrix1[1][1]+Op03U*matrix1[2][1]+matrix1[3][1]);
    Op03Z=(int32)(Op03F*matrix1[0][2]+Op03L*matrix1[1][2]+Op03U*matrix1[2][2]+matrix1[3][2]);
}

void DSPOp23 ()
{
    Op03X=(int32)(Op03F*matrix2[0][0]+Op03L*matrix2[1][0]+Op03U*matrix2[2][0]+matrix2[3][0]);
    Op03Y=(int32)(Op03F*matrix2[0][1]+Op03L*matrix2[1][1]+Op03U*matrix2[2][1]+matrix2[3][1]);
    Op03Z=(int32)(Op03F*matrix2[0][2]+Op03L*matrix2[1][2]+Op03U*matrix2[2][2]+matrix2[3][2]);
}

void DSPOp14 ()
{
    double tmp;

    tmp = (Op14Zr * 6.2832 / 65536.0) + 
	   (1 / cos (Op14Xr * 6.2832 / 65536.0)) * 
	  ((Op14U * 6.2832 / 65536.0) * 
	   cos (Op14Yr * 6.2832 / 65536.0) - 
	   (Op14F * 6.2832 / 65536.0) *
	   sin (Op14Yr * 6.2832 / 65536.0));
    Op14Zrr = (int32) (tmp*65536.0/6.2832);
    tmp = (Op14Xr * 6.2832 / 65536.0) + 
	  ((Op14U * 6.2832 /65536.0) * sin (Op14Yr * 6.2832 / 65536.0) +
	   (Op14F * 6.2832 / 65536.0) * cos (Op14Yr * 6.2832 / 65536.0));
    Op14Xrr = (int32) (tmp * 65536.0 / 6.2832);
    tmp = (Op14Yr * 6.2832 / 65536.0) - 
		tan (Op14Xr * 6.2832 / 65536.0) * 
		((Op14U * 6.2832 / 65536.0) * 
		 cos (Op14Yr * 6.2832 / 65536.0) +
		 (Op14F * 6.2832 / 65536.0) *
		 sin (Op14Yr * 6.2832 / 65536.0)) + (Op14L * 6.2832 / 65536.0);
    Op14Yrr = (int32) (tmp * 65536.0 / 6.2832);
}
