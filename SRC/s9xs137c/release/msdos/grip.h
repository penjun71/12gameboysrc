/************************************************************************\
**                                                                      **
**                GrIP Prototype API Interface Library                  **
**                                for                                   **
**                               DJGPP                                  **
**                                                                      **
**                           Revision 1.00                              **
**                                                                      **
**  COPYRIGHT:                                                          **
**                                                                      **
**    (C) Copyright Advanced Gravis Computer Technology Ltd 1995.       **
**        All Rights Reserved.                                          **
**                                                                      **
**  DISCLAIMER OF WARRANTIES:                                           **
**                                                                      **
**    The following [enclosed] code is provided to you "AS IS",         **
**    without warranty of any kind.  You have a royalty-free right to   **
**    use, modify, reproduce and distribute the following code (and/or  **
**    any modified version) provided that you agree that Advanced       **
**    Gravis has no warranty obligations and shall not be liable for    **
**    any damages arising out of your use of this code, even if they    **
**    have been advised of the possibility of such damages.  This       **
**    Copyright statement and Disclaimer of Warranties may not be       **
**    removed.                                                          **
**                                                                      **
**  HISTORY:                                                            **
**                                                                      **
**    0.102   Jul 12 95   David Bollo     Initial public release on     **
**                                          GrIP hardware               **
**    0.200   Aug 10 95   David Bollo     Added Gravis Loadable Library **
**                                          support                     **
**    0.201   Aug 11 95   David Bollo     Removed Borland C++ support   **
**                                          for maintenance reasons     **
**    1.00    Nov  1 95   David Bollo     First official release as     **
**                                          part of GrIP SDK            **
**                                                                      **
\************************************************************************/

#ifndef GRIP_H
#define GRIP_H

#ifdef __cplusplus
extern "C"
  {
#endif

/* 2. Type Definitions */
typedef unsigned char         GRIP_SLOT;
typedef unsigned char         GRIP_CLASS;
typedef unsigned char         GRIP_INDEX;
typedef unsigned short        GRIP_VALUE;
typedef unsigned long         GRIP_BITFIELD;
typedef unsigned short        GRIP_bool8;
typedef char *                GRIP_STRING;
typedef void *                GRIP_BUF;
typedef unsigned char *       GRIP_BUF_C;
typedef unsigned short *      GRIP_BUF_S;
typedef unsigned long *       GRIP_BUF_L;

/* Standard Classes */
#define GRIP_CLASS_BUTTON                 1
#define GRIP_CLASS_AXIS                   2
#define GRIP_CLASS_POV_HAT                3
#define GRIP_CLASS_VELOCITY               4

/* Refresh Flags */
#define GRIP_REFRESH_COMPLETE             0           /* Default */
#define GRIP_REFRESH_PARTIAL              2
#define GRIP_REFRESH_TRANSMIT             0           /* Default */
#define GRIP_REFRESH_NOTRANSMIT           4

/* 3.1 System API Calls */
GRIP_bool8 GrInitialize(void);
void GrShutdown(void);
GRIP_BITFIELD GrRefresh(GRIP_BITFIELD flags);

/* 3.2 Configuration API Calls */
GRIP_BITFIELD GrGetSlotMap(void);
GRIP_BITFIELD GrGetClassMap(GRIP_SLOT s);
GRIP_BITFIELD GrGetOEMClassMap(GRIP_SLOT s);
GRIP_INDEX GrGetMaxIndex(GRIP_SLOT s, GRIP_CLASS c);
GRIP_VALUE GrGetMaxValue(GRIP_SLOT s, GRIP_CLASS c);

/* 3.3 Data API Calls */
GRIP_VALUE GrGetValue(GRIP_SLOT s, GRIP_CLASS c, GRIP_INDEX i);
GRIP_BITFIELD GrGetPackedValues(GRIP_SLOT s, GRIP_CLASS c, GRIP_INDEX start, GRIP_INDEX end);
void GrSetValue(GRIP_SLOT s, GRIP_CLASS c, GRIP_INDEX i, GRIP_VALUE v);

/* 3.4 OEM Information API Calls */
void GrGetVendorName(GRIP_SLOT s, GRIP_STRING name);
GRIP_VALUE GrGetProductName(GRIP_SLOT s, GRIP_STRING name);
void GrGetControlName(GRIP_SLOT s, GRIP_CLASS c, GRIP_INDEX i, GRIP_STRING name);
GRIP_BITFIELD GrGetCaps(GRIP_SLOT s, GRIP_CLASS c, GRIP_INDEX i);

/* 3.5 Library Management Calls */
GRIP_bool8 GrLink(GRIP_BUF image, GRIP_VALUE size);
GRIP_bool8 GrUnlink(void);

GRIP_bool8 Gr__Link(void);
void Gr__Unlink(void);

/* Diagnostic Information Calls */
GRIP_VALUE GrGetSWVer(void);
GRIP_VALUE GrGetHWVer(void);
GRIP_VALUE GrGetDiagCnt(void);
unsigned long GrGetDiagReg(GRIP_INDEX reg);

/* Standard String Constants for Loadable Library */
extern char GrLibName[];
extern char GrLibEnv[];
extern char GrLibDir[];

/* API Call Thunk */
typedef unsigned char GRIP_THUNK[14];
extern GRIP_THUNK GRIP_Thunk;

#ifdef __cplusplus
  }
#endif

#endif /* GRIP_H */
