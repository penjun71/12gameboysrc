/*
 * Snes9x - Portable Super Nintendo Entertainment System (TM) emulator.
 *
 * (c) Copyright 1996 - 2001 Gary Henderson (gary@daniver.demon.co.uk) and
 *                           Jerremy Koot (jkoot@snes9x.com)
 *
 * Super FX C emulator code 
 * (c) Copyright 1997 - 1999 Ivar (Ivar@snes9x.com) and
 *                           Gary Henderson.
 * Super FX assembler emulator code (c) Copyright 1998 zsKnight and _Demo_.
 *
 * DSP1 emulator code (c) Copyright 1998 Ivar, _Demo_ and Gary Henderson.
 * C4 asm and some C emulation code (c) Copyright 2000 zsKnight and _Demo_.
 * C4 C code (c) Copyright 2001 Gary Henderson (gary@daniver.demon.co.uk).
 *
 * DOS port code contains the works of other authors. See headers in
 * individual files.
 *
 * Snes9x homepage: www.snes9x.com
 *
 * Permission to use, copy, modify and distribute Snes9x in both binary and
 * source form, for non-commercial purposes, is hereby granted without fee,
 * providing that this license information and copyright notice appear with
 * all copies and any derived work.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event shall the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Snes9x is freeware for PERSONAL USE only. Commercial users should
 * seek permission of the copyright holders first. Commercial use includes
 * charging money for Snes9x or software derived from Snes9x.
 *
 * The copyright holders request that bug fixes and improvements to the code
 * should be forwarded to them so everyone can benefit from the modifications
 * in future versions.
 *
 * Super NES and Super Nintendo Entertainment System are trademarks of
 * Nintendo Co., Limited and its subsidiary companies.
 */
#include <allegro.h>
#undef TRUE
#undef FALSE

#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>

#include <dpmi.h>
#include <go32.h>

#include "snes9x.h"
#include "memmap.h"
#include "debug.h"
#include "ppu.h"
#include "snapshot.h"
#include "gfx.h"
#include "display.h"
#include "apu.h"
#include "soundux.h"

extern BITMAP *off_screen;
extern uint32 last_rendered_width;
extern uint32 last_rendered_height;

void SaveScreenshot ()
{
    char FilePCX [512];
    char FileNAME [255], FileDIR [255], FileDRIVE [_MAX_DRIVE], FileEXT [255];
    int i, numpos;
    char HexDig [17] = "0123456789ABCDEF";
    BITMAP *bmp;

    _splitpath (Memory.ROMFilename, FileDRIVE, FileDIR, FileNAME, FileEXT);

    if (strlen (FileNAME) <= 8)
    {
	for (i = strlen (FileNAME); i < 8 ; i++)
	    FileNAME [i] = '_';
	FileNAME [8] = 0;  // We extended the filename... Truncate it at 8.
	numpos = 6;
    }
    else
    {
	numpos = strlen (FileNAME);
			 // Make sure our extending terminates properly.
	FileNAME [numpos + 1] = FileNAME [numpos + 2]=0;
    }

    i = 0;
    while (i <= 0xff)    // Find an open slot. 0-FF.
    {
	FileNAME [numpos] = HexDig [i >> 4];
	FileNAME [numpos + 1] = HexDig [i & 0xf];
	_makepath (FilePCX, FileDRIVE, FileDIR, FileNAME, "pcx");

	if (!exists (FilePCX))
	   break;
	else
	   i++;
    }

    // There.  We have a decent filename, now save the damned thing!
    uint16 Brightness = IPPU.MaxBrightness * 138;
    PALLETE p;
    for (int i = 0; i < 256; i++)
    {
	p[i].r = (((PPU.CGDATA [i] >> 0) & 0x1F) * Brightness) >> 10;
	p[i].g = (((PPU.CGDATA [i] >> 5) & 0x1F) * Brightness) >> 10;
	p[i].b = (((PPU.CGDATA [i] >> 10) & 0x1F) * Brightness) >> 10;
    }
    bmp = create_sub_bitmap (off_screen, 0, 0, last_rendered_width,
			     last_rendered_height);
    save_bitmap (FilePCX, bmp, p);
    destroy_bitmap (bmp);
}
