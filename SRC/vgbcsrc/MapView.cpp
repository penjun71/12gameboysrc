//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <stdio.h>
#include "rom.h"

#include "MapView.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMapWnd *MapWnd;
int mapBase=0x1800;
//---------------------------------------------------------------------------
__fastcall TMapWnd::TMapWnd(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMapWnd::PaintBox1Paint(TObject *Sender)
{
    DoUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMapWnd::DoUpdate()
{
    if ((hiRam[0x40]&0x48)==0)
    {
        TabControl1->Tabs->Strings[0]="$9800 (BG/Wnd)";
        TabControl1->Tabs->Strings[1]="$9C00";
    }
    else if ((hiRam[0x40]&0x48)==8)
    {
        TabControl1->Tabs->Strings[0]="$9800 (Wnd)";
        TabControl1->Tabs->Strings[1]="$9C00 (BG)";
    }
    else if ((hiRam[0x40]&0x48)==0x40)
    {
        TabControl1->Tabs->Strings[0]="$9800 (BG)";
        TabControl1->Tabs->Strings[1]="$9C00 (Wnd)";
    }
    else if ((hiRam[0x40]&0x48)==0x48)
    {
        TabControl1->Tabs->Strings[0]="$9800";
        TabControl1->Tabs->Strings[1]="$9C00 (BG/Wnd)";
    }
    Graphics::TBitmap *bmp=new Graphics::TBitmap;
    bmp->Width=8;
    bmp->Height=8;
    bmp->PixelFormat=pf24bit;
    for (int y=0;y<32;y++)
    {
        for (int x=0;x<32;x++)
        {
            int n;
            int vflip=0;
            int hflip=0;
            if (hiRam[0x40]&16)
                n=vidRam[mapBase+y*32+x];
            else
                n=256+(int)((char)vidRam[mapBase+y*32+x]);
            if (cgb)
            {
                n+=(vidRam[mapBase+y*32+x+0x2000]&8)?512:0;
                vflip=vidRam[mapBase+y*32+x+0x2000]&0x40;
                hflip=vidRam[mapBase+y*32+x+0x2000]&0x20;
            }
            int pal=vidRam[mapBase+y*32+x+0x2000]&7;
            for (int j=0;j<8;j++)
            {
                char* tilePtr=(n>=512)?(&vidRam[0x2000+(n-512)*16+(vflip?(7-j):j)*2]):(&vidRam[n*16+(vflip?(7-j):j)*2]);
                char* bmpPtr0=(char*)bmp->ScanLine[j];//*2];
//                char* bmpPtr1=(char*)bmp->ScanLine[j*2+1];
                for (int i=0,bit=hflip?1:128;i<8;i++,hflip?bit<<=1:bit>>=1)
                {
                    int b0=(tilePtr[0]&bit)?1:0;
                    int b1=(tilePtr[1]&bit)?2:0;
                    if (!cgb)
                    {
                        int c=b1|b0;
                        switch (c)
                        {
                            case 0: c=3-hiRam[0x47]&3; break;
                            case 1: c=3-(hiRam[0x47]>>2)&3; break;
                            case 2: c=3-(hiRam[0x47]>>4)&3; break;
                            case 3: c=3-(hiRam[0x47]>>6)&3; break;
                        }
                        c*=255;
                        c/=3;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
//                        *(bmpPtr0++)=c;
//                        *(bmpPtr0++)=c;
//                        *(bmpPtr0++)=c;
/*                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;*/
                    }
                    else
                    {
                        int c=b1|b0;
                        *(bmpPtr0++)=(bgPal[(pal<<2)+c]&31)<<3;
                        *(bmpPtr0++)=((bgPal[(pal<<2)+c]>>5)&31)<<3;
                        *(bmpPtr0++)=((bgPal[(pal<<2)+c]>>10)&31)<<3;
//                            *(bmpPtr0++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
//                            *(bmpPtr0++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
//                            *(bmpPtr0++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;
/*                            *(bmpPtr1++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;
                            *(bmpPtr1++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;*/
                    }
                }
            }
            PaintBox1->Canvas->Pen->Color=clSilver;
            PaintBox1->Canvas->Brush->Color=clSilver;
            PaintBox1->Canvas->Rectangle(x*9,y*9,x*9+9,y*9+9);
            PaintBox1->Canvas->Draw(x*9,y*9,bmp);
        }
    }
}
void __fastcall TMapWnd::TabControl1Change(TObject *Sender)
{
    mapBase=(TabControl1->TabIndex==0)?0x1800:0x1c00;
    DoUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMapWnd::PaintBox1MouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
    int x=X/9;
    int y=Y/9;
    int n;
    int vflip=0;
    int hflip=0;
    if (hiRam[0x40]&16)
        n=vidRam[mapBase+y*32+x];
    else
        n=256+(int)((char)vidRam[mapBase+y*32+x]);
    if (cgb)
    {
        n+=(vidRam[mapBase+y*32+x+0x2000]&8)?512:0;
        vflip=vidRam[mapBase+y*32+x+0x2000]&0x40;
        hflip=vidRam[mapBase+y*32+x+0x2000]&0x20;
    }
    int p=vidRam[mapBase+y*32+x+0x2000]&7;
    char str[32];
    sprintf(str,"Tile number = $%2.2X",vidRam[mapBase+y*32+x]);
    Number->Caption=str;
    if (n<512)
        sprintf(str,"Tile address = 0:$%4.4X",n*16+0x8000);
    else
        sprintf(str,"Tile address = 1:$%4.4X",(n-512)*16+0x8000);
    Address->Caption=str;
    sprintf(str,"HFlip=%s",hflip?"Yes":"No");
    HFlip->Caption=str;
    sprintf(str,"VFlip=%s",vflip?"Yes":"No");
    VFlip->Caption=str;
    for (int j=0;j<8;j++)
    {
        char* tilePtr=(n>=512)?(&vidRam[0x2000+(n-512)*16+(vflip?(7-j):j)*2]):(&vidRam[n*16+(vflip?(7-j):j)*2]);
        for (int i=0,bit=hflip?1:128;i<8;i++,hflip?bit<<=1:bit>>=1)
        {
            int b0=(tilePtr[0]&bit)?1:0;
            int b1=(tilePtr[1]&bit)?2:0;
            if (!cgb)
            {
                int c=b1|b0;
                switch (c)
                {
                    case 0: c=3-hiRam[0x47]&3; break;
                    case 1: c=3-(hiRam[0x47]>>2)&3; break;
                    case 2: c=3-(hiRam[0x47]>>4)&3; break;
                    case 3: c=3-(hiRam[0x47]>>6)&3; break;
                }
                Shape1->Brush->Color=(TColor)((((3-hiRam[0x47]&3)*255/3)<<16)|(((3-hiRam[0x47]&3)*255/3)<<8)|((3-hiRam[0x47]&3)*255/3));
                Shape2->Brush->Color=(TColor)((((3-(hiRam[0x47]>>2)&3)*255/3)<<16)|(((3-(hiRam[0x47]>>2)&3)*255/3)<<8)|((3-(hiRam[0x47]>>2)&3)*255/3));
                Shape3->Brush->Color=(TColor)((((3-(hiRam[0x47]>>4)&3)*255/3)<<16)|(((3-(hiRam[0x47]>>4)&3)*255/3)<<8)|((3-(hiRam[0x47]>>4)&3)*255/3));
                Shape4->Brush->Color=(TColor)((((3-(hiRam[0x47]>>6)&3)*255/3)<<16)|(((3-(hiRam[0x47]>>6)&3)*255/3)<<8)|((3-(hiRam[0x47]>>6)&3)*255/3));
                char str[8];
                sprintf(str,"R=%d",((3-hiRam[0x47]&3)*31/3));
                R0->Caption=str;
                sprintf(str,"G=%d",((3-hiRam[0x47]&3)*31/3));
                G0->Caption=str;
                sprintf(str,"B=%d",((3-hiRam[0x47]&3)*31/3));
                B0->Caption=str;
                sprintf(str,"R=%d",((3-(hiRam[0x47]>>2)&3)*31/3));
                R1->Caption=str;
                sprintf(str,"G=%d",((3-(hiRam[0x47]>>2)&3)*31/3));
                G1->Caption=str;
                sprintf(str,"B=%d",((3-(hiRam[0x47]>>2)&3)*31/3));
                B1->Caption=str;
                sprintf(str,"R=%d",((3-(hiRam[0x47]>>4)&3)*31/3));
                R2->Caption=str;
                sprintf(str,"G=%d",((3-(hiRam[0x47]>>4)&3)*31/3));
                G2->Caption=str;
                sprintf(str,"B=%d",((3-(hiRam[0x47]>>4)&3)*31/3));
                B2->Caption=str;
                sprintf(str,"R=%d",((3-(hiRam[0x47]>>6)&3)*31/3));
                R3->Caption=str;
                sprintf(str,"G=%d",((3-(hiRam[0x47]>>6)&3)*31/3));
                G3->Caption=str;
                sprintf(str,"B=%d",((3-(hiRam[0x47]>>6)&3)*31/3));
                B3->Caption=str;
                c*=255;
                c/=3;
                PaintBox2->Canvas->Brush->Color=(TColor)((c<<16)|(c<<8)|c);
                TRect r;
                r.Left=16*i; r.Right=16*i+15;
                r.Top=16*j; r.Bottom=16*j+15;
                PaintBox2->Canvas->FillRect(r);
            }
            else
            {
                int c=b1|b0;
                Shape1->Brush->Color=(TColor)(((bgPal[(p<<2)+0]&31)<<19)|(((bgPal[(p<<2)+0]>>5)&31)<<11)|(((bgPal[(p<<2)+0]>>10)&31)<<3));
                Shape2->Brush->Color=(TColor)(((bgPal[(p<<2)+1]&31)<<19)|(((bgPal[(p<<2)+1]>>5)&31)<<11)|(((bgPal[(p<<2)+1]>>10)&31)<<3));
                Shape3->Brush->Color=(TColor)(((bgPal[(p<<2)+2]&31)<<19)|(((bgPal[(p<<2)+2]>>5)&31)<<11)|(((bgPal[(p<<2)+2]>>10)&31)<<3));
                Shape4->Brush->Color=(TColor)(((bgPal[(p<<2)+3]&31)<<19)|(((bgPal[(p<<2)+3]>>5)&31)<<11)|(((bgPal[(p<<2)+3]>>10)&31)<<3));
                char str[8];
                sprintf(str,"B=%d",((bgPal[(p<<2)+0]&31)));
                B0->Caption=str;
                sprintf(str,"B=%d",((bgPal[(p<<2)+1]&31)));
                B1->Caption=str;
                sprintf(str,"B=%d",((bgPal[(p<<2)+2]&31)));
                B2->Caption=str;
                sprintf(str,"B=%d",((bgPal[(p<<2)+3]&31)));
                B3->Caption=str;
                sprintf(str,"G=%d",((bgPal[(p<<2)+0]>>5)&31));
                G0->Caption=str;
                sprintf(str,"G=%d",((bgPal[(p<<2)+1]>>5)&31));
                G1->Caption=str;
                sprintf(str,"G=%d",((bgPal[(p<<2)+2]>>5)&31));
                G2->Caption=str;
                sprintf(str,"G=%d",((bgPal[(p<<2)+3]>>5)&31));
                G3->Caption=str;
                sprintf(str,"R=%d",((bgPal[(p<<2)+0]>>10)&31));
                R0->Caption=str;
                sprintf(str,"R=%d",((bgPal[(p<<2)+1]>>10)&31));
                R1->Caption=str;
                sprintf(str,"R=%d",((bgPal[(p<<2)+2]>>10)&31));
                R2->Caption=str;
                sprintf(str,"R=%d",((bgPal[(p<<2)+3]>>10)&31));
                R3->Caption=str;
                int r=(bgPal[(p<<2)+c]&31)<<3;
                int g=((bgPal[(p<<2)+c]>>5)&31)<<3;
                int b=((bgPal[(p<<2)+c]>>10)&31)<<3;
                PaintBox2->Canvas->Brush->Color=(TColor)((r<<16)|(g<<8)|b);
                TRect rect;
                rect.Left=16*i; rect.Right=16*i+15;
                rect.Top=16*j; rect.Bottom=16*j+15;
                PaintBox2->Canvas->FillRect(rect);
            }
        }
    }
}
//---------------------------------------------------------------------------

