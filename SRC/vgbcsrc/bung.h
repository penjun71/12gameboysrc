//---------------------------------------------------------------------------
#ifndef bungH
#define bungH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TBungSendWnd : public TForm
{
__published:	// IDE-managed Components
    TProgressBar *ProgressBar1;
    TLabel *Label1;
private:	// User declarations
public:		// User declarations
    __fastcall TBungSendWnd(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBungSendWnd *BungSendWnd;

int SendToBung(char *filename);
//---------------------------------------------------------------------------
#endif
