//----------------------------------------------------------------------------
#ifndef __DBGWARN_H__
#define __DBGWARN_H__
//----------------------------------------------------------------------------
#include <System.hpp>
#include <Windows.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <checklst.hpp>
//----------------------------------------------------------------------------
class TDebugWarnDlg : public TForm
{
__published:
	TButton *OKBtn;
    TButton *CancelBtn;
    TLabel *Label1;
    TCheckListBox *List;
    TButton *Button1;
    TButton *Button2;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall CancelBtnClick(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    
    void __fastcall FormCreate(TObject *Sender);
private:
public:
	virtual __fastcall TDebugWarnDlg(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TDebugWarnDlg *DebugWarnDlg;
//----------------------------------------------------------------------------
#endif
