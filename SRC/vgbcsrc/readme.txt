Marat Fayzullin wanted the name of this emulator changed since it is too
similar to the name of his emulator.  Thus, this Gameboy Color emulator no longer
has a name.  The full source code is released along with a compiled binary.
This program is no longer being supported or developed at all, and you can do
whatever you please with the source.  Here are the button arrangements:

Z = B button
X = A button
S = Select
Enter = Start


-Rusty Wagner

