//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "TileView.h"
#include "rom.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTileWnd *TileWnd;
//---------------------------------------------------------------------------
__fastcall TTileWnd::TTileWnd(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TTileWnd::DoUpdate()
{
    char pal[768];
    if (cgb)
    {
        memset(pal,0xff,768);
        if (UsePalette->Checked)
        {
            for (int k=0;k<0x800;k++)
            {
                int t=vidRam[0x1800+k];
                if (!(hiRam[0x40]&16))
                {
                    if (t>=0x80)
                        t=-(0x100-t);
                    t+=256;
                }
                if (vidRam[0x3800+k]&8)
                    t+=384;
                pal[t]=vidRam[0x3800+k]&7;
            }
        }
    }
    if (!cgb)
    {
        PaintBox1->Width=480;
        PaintBox1->Height=320;
        Tiles->Width=497;
        Tiles->Height=343;
        ZoomView->Left=520;
        Shape1->Left=517;
        Shape2->Left=557;
        Shape3->Left=597;
        Shape4->Left=637;
        Address->Left=520;
        R0->Left=520;
        G0->Left=520;
        B0->Left=520;
        R1->Left=560;
        G1->Left=560;
        B1->Left=560;
        R2->Left=600;
        G2->Left=600;
        B2->Left=600;
        R3->Left=640;
        G3->Left=640;
        B3->Left=640;
        UsePalette->Left=520;
        ClientWidth=676;
        ClientHeight=362;
    }
    else
    {
        PaintBox1->Width=640;
        PaintBox1->Height=480;
        Tiles->Width=657;
        Tiles->Height=505;
        ZoomView->Left=680;
        Shape1->Left=677;
        Shape2->Left=717;
        Shape3->Left=757;
        Shape4->Left=797;
        Address->Left=680;
        R0->Left=680;
        G0->Left=680;
        B0->Left=680;
        R1->Left=720;
        G1->Left=720;
        B1->Left=720;
        R2->Left=760;
        G2->Left=760;
        B2->Left=760;
        R3->Left=800;
        G3->Left=800;
        B3->Left=800;
        UsePalette->Left=680;
        ClientWidth=836;
        ClientHeight=522;
    }
    Graphics::TBitmap *bmp=new Graphics::TBitmap;
    bmp->Width=16;
    bmp->Height=16;
    bmp->PixelFormat=pf24bit;
    for (int y=0;y<(cgb?24:16);y++)
    {
        for (int x=0;x<(cgb?32:24);x++)
        {
            int n=x*(cgb?24:16)+y;
            for (int j=0;j<8;j++)
            {
                char* tilePtr=(n>=384)?(&vidRam[0x2000+(n-384)*16+j*2]):(&vidRam[n*16+j*2]);
                char* bmpPtr0=(char*)bmp->ScanLine[j*2];
                char* bmpPtr1=(char*)bmp->ScanLine[j*2+1];
                for (int i=0,bit=128;i<8;i++,bit>>=1)
                {
                    int b0=(tilePtr[0]&bit)?1:0;
                    int b1=(tilePtr[1]&bit)?2:0;
                    if (!cgb)
                    {
                        int c=b1|b0;
                        switch (c)
                        {
                            case 0: c=3-hiRam[0x47]&3; break;
                            case 1: c=3-(hiRam[0x47]>>2)&3; break;
                            case 2: c=3-(hiRam[0x47]>>4)&3; break;
                            case 3: c=3-(hiRam[0x47]>>6)&3; break;
                        }
                        c*=255;
                        c/=3;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
                        *(bmpPtr0++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                        *(bmpPtr1++)=c;
                    }
                    else
                    {
                        int c=b1|b0;
                        if (pal[n]==-1)
                        {
                            c*=255;
                            c/=3;
                            *(bmpPtr0++)=c;
                            *(bmpPtr0++)=c;
                            *(bmpPtr0++)=c;
                            *(bmpPtr0++)=c;
                            *(bmpPtr0++)=c;
                            *(bmpPtr0++)=c;
                            *(bmpPtr1++)=c;
                            *(bmpPtr1++)=c;
                            *(bmpPtr1++)=c;
                            *(bmpPtr1++)=c;
                            *(bmpPtr1++)=c;
                            *(bmpPtr1++)=c;
                        }
                        else
                        {
                            *(bmpPtr0++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
                            *(bmpPtr0++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
                            *(bmpPtr0++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;
                            *(bmpPtr0++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
                            *(bmpPtr0++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
                            *(bmpPtr0++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;
                            *(bmpPtr1++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;
                            *(bmpPtr1++)=(bgPal[(pal[n]<<2)+c]&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>5)&31)<<3;
                            *(bmpPtr1++)=((bgPal[(pal[n]<<2)+c]>>10)&31)<<3;
                        }
                    }
                }
            }
            PaintBox1->Canvas->Draw(x*20,y*20,bmp);
        }
    }
}
void __fastcall TTileWnd::PaintBox1Paint(TObject *Sender)
{
    DoUpdate();
}
//---------------------------------------------------------------------------
void __fastcall TTileWnd::PaintBox1MouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
    int x=X/20;
    int y=Y/20;
    int n=x*(cgb?24:16)+y;
    char str[32];
    if (n<384)
        sprintf(str,"Tile address = 0:$%4.4X",n*16+0x8000);
    else
        sprintf(str,"Tile address = 1:$%4.4X",(n-384)*16+0x8000);
    Address->Caption=str;
    for (int j=0;j<8;j++)
    {
        char* tilePtr=(n>=384)?(&vidRam[0x2000+(n-384)*16+j*2]):(&vidRam[n*16+j*2]);
        for (int i=0,bit=128;i<8;i++,bit>>=1)
        {
            int b0=(tilePtr[0]&bit)?1:0;
            int b1=(tilePtr[1]&bit)?2:0;
            if (!cgb)
            {
                int c=b1|b0;
                switch (c)
                {
                    case 0: c=3-hiRam[0x47]&3; break;
                    case 1: c=3-(hiRam[0x47]>>2)&3; break;
                    case 2: c=3-(hiRam[0x47]>>4)&3; break;
                    case 3: c=3-(hiRam[0x47]>>6)&3; break;
                }
                Shape1->Brush->Color=(TColor)((((3-hiRam[0x47]&3)*255/3)<<16)|(((3-hiRam[0x47]&3)*255/3)<<8)|((3-hiRam[0x47]&3)*255/3));
                Shape2->Brush->Color=(TColor)((((3-(hiRam[0x47]>>2)&3)*255/3)<<16)|(((3-(hiRam[0x47]>>2)&3)*255/3)<<8)|((3-(hiRam[0x47]>>2)&3)*255/3));
                Shape3->Brush->Color=(TColor)((((3-(hiRam[0x47]>>4)&3)*255/3)<<16)|(((3-(hiRam[0x47]>>4)&3)*255/3)<<8)|((3-(hiRam[0x47]>>4)&3)*255/3));
                Shape4->Brush->Color=(TColor)((((3-(hiRam[0x47]>>6)&3)*255/3)<<16)|(((3-(hiRam[0x47]>>6)&3)*255/3)<<8)|((3-(hiRam[0x47]>>6)&3)*255/3));
                char str[8];
                sprintf(str,"R=%d",((3-hiRam[0x47]&3)*31/3));
                R0->Caption=str;
                sprintf(str,"G=%d",((3-hiRam[0x47]&3)*31/3));
                G0->Caption=str;
                sprintf(str,"B=%d",((3-hiRam[0x47]&3)*31/3));
                B0->Caption=str;
                sprintf(str,"R=%d",((3-(hiRam[0x47]>>2)&3)*31/3));
                R1->Caption=str;
                sprintf(str,"G=%d",((3-(hiRam[0x47]>>2)&3)*31/3));
                G1->Caption=str;
                sprintf(str,"B=%d",((3-(hiRam[0x47]>>2)&3)*31/3));
                B1->Caption=str;
                sprintf(str,"R=%d",((3-(hiRam[0x47]>>4)&3)*31/3));
                R2->Caption=str;
                sprintf(str,"G=%d",((3-(hiRam[0x47]>>4)&3)*31/3));
                G2->Caption=str;
                sprintf(str,"B=%d",((3-(hiRam[0x47]>>4)&3)*31/3));
                B2->Caption=str;
                sprintf(str,"R=%d",((3-(hiRam[0x47]>>6)&3)*31/3));
                R3->Caption=str;
                sprintf(str,"G=%d",((3-(hiRam[0x47]>>6)&3)*31/3));
                G3->Caption=str;
                sprintf(str,"B=%d",((3-(hiRam[0x47]>>6)&3)*31/3));
                B3->Caption=str;
                c*=255;
                c/=3;
                PaintBox2->Canvas->Brush->Color=(TColor)((c<<16)|(c<<8)|c);
                TRect r;
                r.Left=16*i; r.Right=16*i+15;
                r.Top=16*j; r.Bottom=16*j+15;
                PaintBox2->Canvas->FillRect(r);
            }
            else
            {
                int p=-1;
                if (UsePalette->Checked)
                {
                    for (int k=0;k<0x800;k++)
                    {
                        int t=vidRam[0x1800+k];
                        if (!(hiRam[0x40]&16))
                        {
                            if (t>=0x80)
                                t=-(0x100-t);
                            t+=256;
                        }
                        if (vidRam[0x3800+k]&8)
                            t+=384;
                        if (t==n)
                            p=vidRam[0x3800+k]&7;
                    }
                }
                int c=b1|b0;
                if (p==-1)
                {
                    Shape1->Brush->Color=(TColor)0;
                    Shape2->Brush->Color=(TColor)((85<<16)|(85<<8)|85);
                    Shape3->Brush->Color=(TColor)((170<<16)|(170<<8)|170);
                    Shape4->Brush->Color=(TColor)0xffffff;
                    R0->Caption="R=0"; G0->Caption="G=0"; B0->Caption="B=0";
                    R1->Caption="R=10"; G1->Caption="G=10"; B1->Caption="B=10";
                    R2->Caption="R=21"; G2->Caption="G=21"; B2->Caption="B=21";
                    R3->Caption="R=31"; G3->Caption="G=31"; B3->Caption="B=31";
                    c*=255;
                    c/=3;
                    PaintBox2->Canvas->Brush->Color=(TColor)((c<<16)|(c<<8)|c);
                }
                else
                {
                    Shape1->Brush->Color=(TColor)(((bgPal[(p<<2)+0]&31)<<19)|(((bgPal[(p<<2)+0]>>5)&31)<<11)|(((bgPal[(p<<2)+0]>>10)&31)<<3));
                    Shape2->Brush->Color=(TColor)(((bgPal[(p<<2)+1]&31)<<19)|(((bgPal[(p<<2)+1]>>5)&31)<<11)|(((bgPal[(p<<2)+1]>>10)&31)<<3));
                    Shape3->Brush->Color=(TColor)(((bgPal[(p<<2)+2]&31)<<19)|(((bgPal[(p<<2)+2]>>5)&31)<<11)|(((bgPal[(p<<2)+2]>>10)&31)<<3));
                    Shape4->Brush->Color=(TColor)(((bgPal[(p<<2)+3]&31)<<19)|(((bgPal[(p<<2)+3]>>5)&31)<<11)|(((bgPal[(p<<2)+3]>>10)&31)<<3));
                    char str[8];
                    sprintf(str,"B=%d",((bgPal[(p<<2)+0]&31)));
                    B0->Caption=str;
                    sprintf(str,"B=%d",((bgPal[(p<<2)+1]&31)));
                    B1->Caption=str;
                    sprintf(str,"B=%d",((bgPal[(p<<2)+2]&31)));
                    B2->Caption=str;
                    sprintf(str,"B=%d",((bgPal[(p<<2)+3]&31)));
                    B3->Caption=str;
                    sprintf(str,"G=%d",((bgPal[(p<<2)+0]>>5)&31));
                    G0->Caption=str;
                    sprintf(str,"G=%d",((bgPal[(p<<2)+1]>>5)&31));
                    G1->Caption=str;
                    sprintf(str,"G=%d",((bgPal[(p<<2)+2]>>5)&31));
                    G2->Caption=str;
                    sprintf(str,"G=%d",((bgPal[(p<<2)+3]>>5)&31));
                    G3->Caption=str;
                    sprintf(str,"R=%d",((bgPal[(p<<2)+0]>>10)&31));
                    R0->Caption=str;
                    sprintf(str,"R=%d",((bgPal[(p<<2)+1]>>10)&31));
                    R1->Caption=str;
                    sprintf(str,"R=%d",((bgPal[(p<<2)+2]>>10)&31));
                    R2->Caption=str;
                    sprintf(str,"R=%d",((bgPal[(p<<2)+3]>>10)&31));
                    R3->Caption=str;
                    int r=(bgPal[(p<<2)+c]&31)<<3;
                    int g=((bgPal[(p<<2)+c]>>5)&31)<<3;
                    int b=((bgPal[(p<<2)+c]>>10)&31)<<3;
                    PaintBox2->Canvas->Brush->Color=(TColor)((r<<16)|(g<<8)|b);
                }
                TRect r;
                r.Left=16*i; r.Right=16*i+15;
                r.Top=16*j; r.Bottom=16*j+15;
                PaintBox2->Canvas->FillRect(r);
            }
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TTileWnd::UsePaletteClick(TObject *Sender)
{
    DoUpdate();
}
//---------------------------------------------------------------------------


