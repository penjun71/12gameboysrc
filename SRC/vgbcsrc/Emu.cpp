//---------------------------------------------------------------------------
#include <vcl.h>
#include <vcl\registry.hpp>
#pragma hdrstop
#include <stdio.h>
#include <dir.h>

#include "Emu.h"
#include <clipbrd.hpp>
#include "gbcemu.h"
#include "bung.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TEmuWnd *EmuWnd;

int startInDebug=0;
int showSpeed=0;
int scaleFact=2;
int paused=0;

extern int cmdLineValid;
extern char initialROM[256];

char currentROM[256];

POINT gbPoly[]={{3,9},{8,5},{278,5},{283,8},{283,466},{279,476},{273,482},
    {268,485},{243,492},{222,495},{192,499},{169,501},{119,501},{94,499},
    {72,496},{49,492},{33,489},{22,486},{17,485},{13,483},{10,480},
    {5,473},{3,468}};
int gbPolyPointCount=23;

static int clicked=0;
static int clickX,clickY;

void PauseStart()
{
/*    pauseLevel++;
    if (pauseLevel>1)
        return;
    int startTime=GetTickCount();
    while (!pauseExit)
    {
        Sleep(0);
        if ((GetTickCount()-startTime)>3000)
            break;
    }
    pauseExit=false;
    pause=true;
    SetEvent(emuEvent);
    while (!pauseReady)
    {
        Sleep(0);
        if ((GetTickCount()-startTime)>3000)
            break;
    }*/
}

void PauseEnd()
{
/*    pauseLevel--;
    if (pauseLevel)
        return;
    pause=false;
    pauseReady=false;
    SetEvent(emuEvent);
    int startTime=GetTickCount();
    while (!pauseExit)
    {
        Sleep(0);
        if ((GetTickCount()-startTime)>3000)
            break;
    }*/
}

//---------------------------------------------------------------------------
int lcdOnly=0;

__fastcall TEmuWnd::TEmuWnd(TComponent* Owner)
    : TForm(Owner)
{
    enforceRatio=0;
}
//---------------------------------------------------------------------------
extern char *ScreenLine[288];

extern HINSTANCE hInst;
bool minimizeOnDeactivate=true;
int showSkin=1;

void __fastcall TEmuWnd::Make()
{
    minimizeOnDeactivate=false;
    enforceRatio=0;
    Caption="";
    Application->Title=Caption;
    if (!Fullscreenview1->Checked)
    {
//        BorderStyle=bsSizeable;
        FormStyle=(Stayontop1->Checked)?fsStayOnTop:fsNormal;
        AutoScroll=false;
        Application->ProcessMessages();
//        int sf1w=(currentSkin==-1)?0:skinImageSm->Width;
//        int sf1h=(currentSkin==-1)?0:skinImageSm->Height;
//        int sf2w=(currentSkin==-1)?0:skinImageLg->Width;
//        int sf2h=(currentSkin==-1)?0:skinImageLg->Height;
//        if (currentSkin==-1)
        if (showSkin)
        {
            Normal1xview1->Checked=(scaleFact==1);
            Large2xview1->Checked=(scaleFact==2);
            ClientWidth=288*scaleFact;
            ClientHeight=507*scaleFact;
            gb->Width=288*scaleFact;
            gb->Height=507*scaleFact;
            batt->Left=24*scaleFact;
            batt->Top=88*scaleFact;
            batt->Width=12*scaleFact;
            batt->Height=13*scaleFact;
            LCD->Left=64*scaleFact;
            LCD->Top=54*scaleFact;
            LCD->ClientWidth=160*scaleFact;
            LCD->ClientHeight=144*scaleFact;
            GBC_LCDPos(LCD->Left,LCD->Top);
        }
        else
        {
            ClientWidth=160*scaleFact;
            ClientHeight=144*scaleFact;
            LCD->Left=0;
            LCD->Top=0;
            GBC_LCDPos(0,0);
            batt->Left=350;
        }
//        else if ((skin[currentSkin].defW)&&
//                 (skin[currentSkin].defH))
//        {
//            ClientWidth=skin[currentSkin].defW;
//            ClientHeight=skin[currentSkin].defH;
//            Image->Width=skin[currentSkin].defW;
//            Image->Height=skin[currentSkin].defH;
//            skin[currentSkin].defW=0;
//            skin[currentSkin].defH=0;
//            Normal1xview1->Checked=false;
//            Large2xview1->Checked=false;
//            if (((abs((Image->Width-sf1w)))<5)&&
//                ((abs((Image->Height-sf1h)))<5))
//            {
//                Normal1xview1->Checked=true;
//                scaleFact=1;
//                ClientWidth=skinImageSm->Width;
//                ClientHeight=skinImageSm->Height;
//                Image->Width=skinImageSm->Width;
//                Image->Height=skinImageSm->Height;
//                LCD->Left=skinLcd.left/(3-scaleFact);
//                LCD->Top=skinLcd.top/(3-scaleFact);
//                LCD->ClientWidth=160*scaleFact;
//                LCD->ClientHeight=144*scaleFact;
//            }
//            else if (((abs((Image->Width-sf2w)))<5)&&
//                     ((abs((Image->Height-sf2h)))<5))
//            {
//                Large2xview1->Checked=true;
//                scaleFact=2;
//                ClientWidth=skinImageLg->Width;
//                ClientHeight=skinImageLg->Height;
//                Image->Width=skinImageLg->Width;
//                Image->Height=skinImageLg->Height;
//                LCD->Left=skinLcd.left/(3-scaleFact);
//                LCD->Top=skinLcd.top/(3-scaleFact);
//                LCD->ClientWidth=160*scaleFact;
//                LCD->ClientHeight=144*scaleFact;
//            }
//            else
//            {
//                if (LCD->Width>160)
//                    scaleFact=2;
//                else
//                    scaleFact=1;
//                int imgW=(skinImageLg)?skinImageLg->Width:(160<<1);
//                int imgH=(skinImageLg)?skinImageLg->Height:(144<<1);
//                LCD->Left=skinLcd.left*Image->Width/imgW;
//                LCD->Width=(skinLcd.right-skinLcd.left)*Image->Width/imgW;
//                LCD->Top=skinLcd.top*Image->Height/imgH;
//                LCD->Height=(skinLcd.bottom-skinLcd.top)*Image->Height/imgH;
//            }
//        }
//        else if (scaleFact==1)
//        {
//            Normal1xview1->Checked=true;
//            Large2xview1->Checked=false;
//            ClientWidth=skinImageSm->Width;
//            ClientHeight=skinImageSm->Height;
//            Image->Width=skinImageSm->Width;
//            Image->Height=skinImageSm->Height;
//            LCD->Left=skinLcd.left/(3-scaleFact);
//            LCD->Top=skinLcd.top/(3-scaleFact);
//            LCD->ClientWidth=160*scaleFact;
//            LCD->ClientHeight=144*scaleFact;
//        }
//        else
//        {
//            Normal1xview1->Checked=false;
//            Large2xview1->Checked=true;
//            ClientWidth=skinImageLg->Width;
//            ClientHeight=skinImageLg->Height;
//            Image->Width=skinImageLg->Width;
//            Image->Height=skinImageLg->Height;
//            LCD->Left=skinLcd.left/(3-scaleFact);
//            LCD->Top=skinLcd.top/(3-scaleFact);
//            LCD->ClientWidth=160*scaleFact;
//            LCD->ClientHeight=144*scaleFact;
//        }
//        calcImage=skinImageLg;
//        None1->Checked=(currentSkin==-1);
//        while (Currentskin1->Count>1)
//            Currentskin1->Delete(1);
//        for (int i=0;i<skinCount;i++)
//        {
//            TMenuItem *itm=new TMenuItem(Currentskin1);
//            itm->Caption=skin[i].name;
//            itm->Checked=(i==currentSkin);
//            itm->OnClick=OnSkinSelect;
//            Currentskin1->Add(itm);
//        }
    }
    else
    {
        BorderStyle=bsNone;
        FormStyle=fsStayOnTop;
        AutoScroll=false;
        Application->ProcessMessages();
        Left=0; Top=0;
        Width=GetDeviceCaps(Canvas->Handle,HORZRES);
        Height=GetDeviceCaps(Canvas->Handle,VERTRES);
        LCD->Left=0; LCD->Width=Width;
        LCD->Top=0; LCD->Height=Height;
    }
    enforceRatio=1;
    minimizeOnDeactivate=true;
    POINT *p=new POINT[gbPolyPointCount];
    for (int i=0;i<gbPolyPointCount;i++)
    {
        p[i].x=gbPoly[i].x*scaleFact;
        p[i].y=gbPoly[i].y*scaleFact;
    }
    SetWindowRgn(Handle,CreatePolygonRgn(p,gbPolyPointCount,WINDING),true);
    delete[] p;
    GBC_SetWindow(Handle);
    Visible=true;
}

int lcdUpdate;
void __fastcall TEmuWnd::FormCreate(TObject *Sender)
{
    lcdUpdate=0;

    if (!GBC_Init(Handle))
        ExitProcess(0);
    GBC_LCDScaleFact(1);
    GBC_LCDPos(LCD->Left,LCD->Top);
    scaleFact=1;

    if (cmdLineValid)
    {
        GBC_LoadROM(initialROM);
        strcpy(currentROM,initialROM);
        batt->Visible=true;
    }

    Application->OnIdle=TEmuWnd::OnIdle;
//    Application->OnActivate=FormActivate;
    Application->OnDeactivate=FormDeactivate;
    Application->OnMinimize=OnMinimize;
    Application->OnRestore=OnRestore;

    TRegistry *reg=new TRegistry;
    reg->RootKey=HKEY_LOCAL_MACHINE;
    reg->OpenKey("\\Software\\gbc",true);
    if (reg->ValueExists("StayOnTop"))
    {
        if (reg->ReadInteger("StayOnTop"))
            Stayontop1->Checked=true;
        else
            Stayontop1->Checked=false;
    }
    if (reg->ValueExists("FullScreen"))
    {
        if (reg->ReadInteger("FullScreen"))
        {
            Fullscreenview1->Checked=true;
            Normal1xview1->Checked=false;
            Large2xview1->Checked=false;
            Enterdebugger1->Enabled=false;
            Showpercentageofactualspeed1->Enabled=false;
            Stayontop1->Enabled=false;
        }
        else
            Fullscreenview1->Checked=false;
    }
    reg->CloseKey();
    delete reg;

//    emuThread=new EmuThread(false);

    Make();

//    EnableSound();
}
//---------------------------------------------------------------------------
extern int needPaint;
extern HWND hWnd;
void __fastcall TEmuWnd::OnIdle(TObject *,bool& done)
{
/*    if (needPaint)
    {
        HDC dc=GetDC(hWnd);
        BitBlt(dc,0,0,EmuWnd->gba->Width,EmuWnd->gba->Height,
            EmuWnd->gba->Picture->Bitmap->Canvas->Handle,0,0,SRCAND);
        BitBlt(dc,0,0,EmuWnd->gb->Width,EmuWnd->gb->Height,
            EmuWnd->gb->Picture->Bitmap->Canvas->Handle,0,0,SRCPAINT);
        ReleaseDC(hWnd,dc);
        needPaint=0;
    }*/
    done=false;
    if (!paused)
    {
        GBC_Run();
        static int oldPercent=0;
        int percent=GBC_GetPercentSpeed();
        if (showSpeed&&(oldPercent!=percent))
        {
            char str[64];
            sprintf(str,"%d%%",percent);
            Speed->Caption=str;
            oldPercent=percent;
        }
    }
    else
        done=true;
}


void __fastcall TEmuWnd::LCDPaint(TObject *Sender)
{
    GBC_PaintLCD();
}
//---------------------------------------------------------------------------


//extern int TransferFile(char *fn);
void __fastcall TEmuWnd::SendFile1Click(TObject *Sender)
{
//    PauseStart();
    TOpenDialog *dlog=new TOpenDialog(this);
    dlog->DefaultExt="gb";
    dlog->FileName="*.gb;*.gbc";
    dlog->FilterIndex=1;
    dlog->Options.Clear();
    dlog->Options << ofFileMustExist << ofPathMustExist << ofAllowMultiSelect;
    dlog->Filter="Gameboy ROMs (*.gb;*.gbc)|*.gb;*.gbc";
    char cwd[256];
    getcwd(cwd,256);
    dlog->InitialDir=cwd;
    if (dlog->Execute())
    {
        Screen->Cursor=crHourGlass;
        strcpy(currentROM,dlog->FileName.c_str());
        SendtoBungXChanger1->Enabled=true;
        GBC_LoadROM(dlog->FileName.c_str());
        batt->Visible=true;
        if (startInDebug)
            GBC_EnterDebug();
        Screen->Cursor=crDefault;
    }
    delete dlog;
//    PauseEnd();
}
//---------------------------------------------------------------------------

//extern void ResetEmu();
void __fastcall TEmuWnd::Reset1Click(TObject *Sender)
{
//    PauseStart();
    GBC_Reset();
    if (startInDebug)
        GBC_EnterDebug();
//    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::FormDestroy(TObject *Sender)
{
//    DisableSound();
//    while (exitEmu)
//        Sleep(0);
//    CloseSkin();
    GBC_Close();
    TRegistry *reg=new TRegistry;
    reg->RootKey=HKEY_LOCAL_MACHINE;
    reg->OpenKey("\\Software\\gbc",true);
    reg->WriteInteger("LCDOnly",lcdOnly);
    reg->WriteInteger("StayOnTop",Stayontop1->Checked?1:0);
    reg->WriteInteger("FullScreen",Fullscreenview1->Checked?1:0);
    reg->CloseKey();
    delete reg;
}

void __fastcall TEmuWnd::FormDeactivate(TObject *Sender)
{
    if ((Fullscreenview1->Checked)&&(minimizeOnDeactivate))
        Application->Minimize();
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Enterdebugmode1Click(TObject *Sender)
{
/*    debug=1; run=0;
    debugStartPC=regPC;//R.PC.D;
    DebugWnd->Show();
    DebugWnd->Update();*/
    GBC_EnterDebug();
}

void __fastcall TEmuWnd::Savestateimage1Click(TObject *Sender)
{
/*    PauseStart();
    TSaveDialog *dlog=new TSaveDialog(this);
    dlog->DefaultExt="sav";
    char str[256];
    strcpy(str,romImage[currentROM].file);
    str[strlen(str)-3]='s';
    str[strlen(str)-2]='a';
    str[strlen(str)-1]='v';
    dlog->FileName=str;
    dlog->Options.Clear();
    dlog->Filter="State files (*.sav)|*.sav";
    dlog->FilterIndex=1;
    char cwd[256];
    getcwd(cwd,256);
    dlog->InitialDir=cwd;
    if (dlog->Execute())
        SaveState(dlog->FileName.c_str());
    delete dlog;
    PauseEnd();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Loadstateimage1Click(TObject *Sender)
{
//    PauseStart();
    TOpenDialog *dlog=new TOpenDialog(this);
    dlog->DefaultExt="sav";
    dlog->FileName="*.sav";
    dlog->Options.Clear();
    dlog->Options << ofFileMustExist << ofPathMustExist;
    dlog->Filter="State files (*.sav)|*.sav";
    dlog->FilterIndex=1;
    char cwd[256];
    getcwd(cwd,256);
    dlog->InitialDir=cwd;
    if (dlog->Execute())
    {
/*        FILE *fp=fopen(dlog->FileName.c_str(),"rb");
        char str[9],romName[56];
        fread(str,8,1,fp);
        fread(romName,56,1,fp);
        fclose(fp);
        str[8]=0;
        if (strcmp(str,"VTIv2.0 "))
        {
            MessageBox(NULL,"Unrecognized file format","State Load Error",MB_OK|MB_ICONSTOP);
            PauseEnd();
            chdir(old);
            return;
        }
        if (!strcmp(romName,romImage[currentROM].name))
            hw->LoadState(dlog->FileName.c_str());
        else
        {
            SaveState();
            int newROM=-1;
            for (int i=0;i<romImageCount;i++)
            {
                if (!strcmp(romImage[i].name,romName))
                {
                    newROM=i;
                    break;
                }
            }
            if (newROM==-1)
            {
                MessageBox(NULL,"Required ROM not present","State Load Error",MB_OK|MB_ICONSTOP);
                PauseEnd();
                chdir(old);
                return;
            }
            CloseSkin();
            CloseEmu();
            InitEmu(newROM);
            TRegistry *reg=new TRegistry;
            reg->RootKey=HKEY_LOCAL_MACHINE;
            reg->OpenKey("\\Software\\ACZ\\Virtual TI\\Skins",true);
            char keyName[24];
            switch (calc)
            {
                case 73: strcpy(keyName,"DefaultSkin73"); break;
                case 82: strcpy(keyName,"DefaultSkin82"); break;
                case 83: strcpy(keyName,"DefaultSkin83"); break;
                case 84: strcpy(keyName,"DefaultSkin83Plus"); break;
                case 85: strcpy(keyName,"DefaultSkin85"); break;
                case 86: strcpy(keyName,"DefaultSkin86"); break;
                case 89: strcpy(keyName,"DefaultSkin89"); break;
                case 92: case 93: strcpy(keyName,"DefaultSkin92"); break;
                case 94: strcpy(keyName,"DefaultSkin92Plus"); break;
            }
            if (reg->ValueExists(keyName))
            {
                char skinName[32];
                strcpy(skinName,reg->ReadString(keyName).c_str());
                int found=-1;
                for (int i=0;i<skinCount;i++)
                {
                    if (!strcmp(skinName,skin[i].file))
                    {
                        found=i;
                        break;
                    }
                }
                if (found!=-1)
                    LoadSkin(found);
            }
            else
            {
                for (int i=0;i<skinCount;i++)
                {
                    if ((calc==skin[i].calc)||
                        ((calc>=92)&&(skin[i].calc>=92)))
                    {
                        LoadSkin(i);
                        break;
                    }
                }
            }
            reg->CloseKey();
            delete reg;
            hw->LoadState(dlog->FileName.c_str());
            Make();
            AboutDlg->UpdateText();
        }*/
    }
    delete dlog;
//    PauseEnd();
}

void __fastcall TEmuWnd::FormKeyDown(TMessage &Msg)
{
    int Key;
    Key=Msg.WParam;
    if ((Key==VK_F11)&&(Enterdebugger1->Enabled))
        Enterdebugmode1Click(NULL);
    else if (Key==VK_F10)
        SendFile1Click(NULL);
    else if (Key==VK_F12)
        EnableDebugWarnings1Click(NULL);
    else if (Key=='Z')
        GBC_KeyDown(KEY_B);
    else if (Key=='X')
        GBC_KeyDown(KEY_A);
    else if (Key==13)
        GBC_KeyDown(KEY_START);
    else if (Key=='S')
        GBC_KeyDown(KEY_SELECT);
    else if (Key==VK_LEFT)
        GBC_KeyDown(KEY_LEFT);
    else if (Key==VK_RIGHT)
        GBC_KeyDown(KEY_RIGHT);
    else if (Key==VK_UP)
        GBC_KeyDown(KEY_UP);
    else if (Key==VK_DOWN)
        GBC_KeyDown(KEY_DOWN);
    Msg.Result=0;
}
//---------------------------------------------------------------------------void __fastcall TEmuWnd::FormKeyUp(TObject *Sender, WORD &Key,
void __fastcall TEmuWnd::FormKeyUp(TMessage &Msg)
{
    int Key;
    Key=Msg.WParam;
    if (Key=='Z')
        GBC_KeyUp(KEY_B);
    else if (Key=='X')
        GBC_KeyUp(KEY_A);
    else if (Key==13)
        GBC_KeyUp(KEY_START);
    else if (Key=='S')
        GBC_KeyUp(KEY_SELECT);
    else if (Key==VK_LEFT)
        GBC_KeyUp(KEY_LEFT);
    else if (Key==VK_RIGHT)
        GBC_KeyUp(KEY_RIGHT);
    else if (Key==VK_UP)
        GBC_KeyUp(KEY_UP);
    else if (Key==VK_DOWN)
        GBC_KeyUp(KEY_DOWN);
}

void __fastcall TEmuWnd::Large2xcalculatorimage1Click(TObject *Sender)
{
    if (Fullscreenview1->Checked)
    {
        Fullscreenview1->Checked=false;
        Top=5; Left=100;
        Enterdebugger1->Enabled=true;
        Showpercentageofactualspeed1->Enabled=true;
        Stayontop1->Enabled=true;
        FormStyle=(Stayontop1->Checked)?fsStayOnTop:fsNormal;
    }
    Normal1xview1->Checked=false;
    Large2xview1->Checked=true;
    scaleFact=2;
    GBC_LCDScaleFact(2);
    Make();
    Speed->Left=442-Speed->Width;
    Speed->Top=90-Speed->Height;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Showpercentageofactualspeed1Click(
      TObject *Sender)
{
    showSpeed=!showSpeed;
    Speed->Visible=showSpeed;
    Showpercentageofactualspeed1->Checked=showSpeed;
    Caption="";
    Application->Title=Caption;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::LCDonlyblackwhiteBMP1Click(TObject *Sender)
{
/*//    PauseStart();
    TSaveDialog *dlog=new TSaveDialog(this);
    dlog->DefaultExt="bmp";
    dlog->FileName="screen.bmp";
    dlog->Options.Clear();
    dlog->Options << ofOverwritePrompt << ofPathMustExist
        << ofNoChangeDir;
    dlog->Filter="Bitmap files (*.bmp)|*.bmp";
    dlog->FilterIndex=1;
    char cwd[256];
    getcwd(cwd,256);
    dlog->InitialDir=cwd;
    if (dlog->Execute())
    {
        Graphics::TBitmap *bmp=GetLCD_BW();
        bmp->SaveToStream(&TFileStream(dlog->FileName,
            fmCreate));
        delete bmp;
    }
    delete dlog;
//    PauseEnd();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::LCDonlytruecolorsBMP1Click(TObject *Sender)
{
/*//    PauseStart();
    TSaveDialog *dlog=new TSaveDialog(this);
    dlog->DefaultExt="bmp";
    dlog->FileName="screen.bmp";
    dlog->Options.Clear();
    dlog->Options << ofOverwritePrompt << ofPathMustExist
        << ofNoChangeDir;
    dlog->Filter="Bitmap files (*.bmp)|*.bmp";
    dlog->FilterIndex=1;
    char cwd[256];
    getcwd(cwd,256);
    dlog->InitialDir=cwd;
    if (dlog->Execute())
    {
        Graphics::TBitmap *bmp=GetLCD_True();
        bmp->SaveToStream(&TFileStream(dlog->FileName,
            fmCreate));
        delete bmp;
    }
    delete dlog;
//    PauseEnd();*/
}
//---------------------------------------------------------------------------


void __fastcall TEmuWnd::LCDonlyblackwhiteClipboard1Click(
      TObject *Sender)
{
/*//    PauseStart();
    Graphics::TBitmap *bmp=GetLCD_BW();
    int DataHandle;
    HPALETTE APalette;
    unsigned short format;
    bmp->SaveToClipboardFormat(format,DataHandle,APalette);
    Clipboard()->SetAsHandle(format,DataHandle);
    delete bmp;
//    PauseEnd();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::LCDonlytruecolorsClipboard1Click(
      TObject *Sender)
{
/*//    PauseStart();
    Graphics::TBitmap *bmp=GetLCD_True();
    int DataHandle;
    HPALETTE APalette;
    unsigned short format;
    bmp->SaveToClipboardFormat(format,DataHandle,APalette);
    Clipboard()->SetAsHandle(format,DataHandle);
    delete bmp;
//    PauseEnd();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::CalculatorimageBMP1Click(TObject *Sender)
{
/*//    PauseStart();
    TSaveDialog *dlog=new TSaveDialog(this);
    dlog->DefaultExt="bmp";
    dlog->FileName="screen.bmp";
    dlog->Options.Clear();
    dlog->Options << ofOverwritePrompt << ofPathMustExist
        << ofNoChangeDir;
    dlog->Filter="Bitmap files (*.bmp)|*.bmp";
    dlog->FilterIndex=1;
    char cwd[256];
    getcwd(cwd,256);
    dlog->InitialDir=cwd;
    if (dlog->Execute())
    {
        Graphics::TBitmap *bmp=GetLCD_Calc(calcImage);
        bmp->SaveToStream(&TFileStream(dlog->FileName,
            fmCreate));
        delete bmp;
    }
    delete dlog;
//    PauseEnd();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::CalculatorimageClipboard1Click(TObject *Sender)
{
/*//    PauseStart();
    Graphics::TBitmap *bmp=GetLCD_Calc(calcImage);
    int DataHandle;
    HPALETTE APalette;
    unsigned short format;
    bmp->SaveToClipboardFormat(format,DataHandle,APalette);
    Clipboard()->SetAsHandle(format,DataHandle);
    delete bmp;
//    PauseEnd();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Normal1xview1Click(TObject *Sender)
{
    if (Fullscreenview1->Checked)
    {
        Fullscreenview1->Checked=false;
        Top=5; Left=100;
        Enterdebugger1->Enabled=true;
        Showpercentageofactualspeed1->Enabled=true;
        Stayontop1->Enabled=true;
        FormStyle=(Stayontop1->Checked)?fsStayOnTop:fsNormal;
    }
    Normal1xview1->Checked=true;
    Large2xview1->Checked=false;
    scaleFact=1;
    GBC_LCDScaleFact(1);
    Make();
    Speed->Left=221-Speed->Width;
    Speed->Top=45-Speed->Height;
}

void __fastcall TEmuWnd::OnKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    TMessage msg;
    msg.WParam=Key;
    msg.LParam=0;
    FormKeyDown(msg);
    Key=0;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::OnKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    TMessage msg;
    msg.WParam=Key;
    msg.LParam=0;
    FormKeyUp(msg);
    Key=0;
}
//---------------------------------------------------------------------------
void __fastcall TEmuWnd::OnSkinSelect(TObject *Sender)
{
/*    int indx=((TMenuItem*)Sender)->Parent->
        IndexOf((TMenuItem*)Sender);
    for (int i=0;i<skinCount;i++)
    {
        if ((calc==skin[i].calc)||((calc>=92)&&
            (skin[i].calc>=92)))
        {
            indx--;
            if (!indx)
            {
                PauseStart();
                LoadSkin(i);
                PauseEnd();
                None1->Checked=false;
                Make();
                break;
            }
        }
    }*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::ImagePaint(TObject *Sender)
{
/*    if (!skinImageSm) return;
    if (!skinImageLg) return;
    TRect r;
    r.Left=0; r.Right=Image->Width;
    r.Top=0; r.Bottom=Image->Height;
    if (Image->Width>skinImageSm->Width)
        Image->Canvas->StretchDraw(r,skinImageLg);
    else
        Image->Canvas->StretchDraw(r,skinImageSm);*/
}
//---------------------------------------------------------------------------
void __fastcall TEmuWnd::WMSizing(TMessage &Msg)
{
/*    RECT *r=(LPRECT)Msg.LParam;
    if (!enforceRatio)
    {
        Msg.Result=false;
        return;
    }
    Normal1xview1->Checked=false;
    Large2xview1->Checked=false;
    int cliW=r->right-r->left-(Width-ClientWidth);
    int cliH=r->bottom-r->top-(Height-ClientHeight);
    int imgW=(skinImageLg)?skinImageLg->Width:(160<<1);
    int imgH=(skinImageLg)?skinImageLg->Height:(144<<1);
    int newW=imgW*cliH/imgH;
    int newH=imgH*cliW/imgW;
    newW+=(Width-ClientWidth);
    newH+=(Height-ClientHeight);
    int dx=abs(newW-(r->right-r->left));
    int dy=abs(newH-(r->bottom-r->top));
    if (((dx>=dy)||(Msg.WParam==WMSZ_LEFT)||(Msg.WParam==WMSZ_RIGHT))&&
        ((Msg.WParam!=WMSZ_TOP)&&(Msg.WParam!=WMSZ_BOTTOM)))
    {
        if (newH<GetSystemMetrics(SM_CYMIN))
            GetWindowRect(Handle,r);
        else
        {
            if ((Msg.WParam==WMSZ_TOP)||
                (Msg.WParam==WMSZ_TOPLEFT)||
                (Msg.WParam==WMSZ_TOPRIGHT))
                r->top=r->bottom-newH;
            else
                r->bottom=r->top+newH;
        }
    }
    else
    {
        if (newW<GetSystemMetrics(SM_CXMIN))
            GetWindowRect(Handle,r);
        else
        {
            if ((Msg.WParam==WMSZ_BOTTOMLEFT)||
                (Msg.WParam==WMSZ_LEFT)||
                (Msg.WParam==WMSZ_TOPLEFT))
                r->left=r->right-newW;
            else
                r->right=r->left+newW;
        }
    }
    Image->Width=r->right-r->left-(Width-ClientWidth);
    Image->Height=r->bottom-r->top-(Height-ClientHeight);
    LCD->Left=skinLcd.left*Image->Width/imgW;
    LCD->Width=(skinLcd.right-skinLcd.left)*Image->Width/imgW;
    LCD->Top=skinLcd.top*Image->Height/imgH;
    LCD->Height=(skinLcd.bottom-skinLcd.top)*Image->Height/imgH;
    if ((LCD->Width>160)&&(scaleFact==1))
    {
//        PauseStart();
        scaleFact=2;
        bmp->Width=160*scaleFact;
        bmp->Height=144*scaleFact;
        bmp->PixelFormat=pf15bit;
        for (int i=0;i<(144*scaleFact);i++)
            ScreenLine[i]=(char *)bmp->ScanLine[i];
//        PauseEnd();
    }
    else if ((LCD->Width<=160)&&(scaleFact==2))
    {
//        PauseStart();
        scaleFact=1;
        bmp->Width=160*scaleFact;
        bmp->Height=144*scaleFact;
        bmp->PixelFormat=pf15bit;
        for (int i=0;i<(144*scaleFact);i++)
            ScreenLine[i]=(char *)bmp->ScanLine[i];
//        PauseEnd();
    }
    Msg.Result=true;*/
}

void __fastcall TEmuWnd::Fullscreenview1Click(TObject *Sender)
{
/*    if (Fullscreenview1->Checked)
    {
        Fullscreenview1->Checked=false;
        Top=5; Left=100;
        Enterdebugger1->Enabled=true;
        Currentskin1->Enabled=true;
        Showpercentageofactualspeed1->Enabled=true;
        Stayontop1->Enabled=true;
        FormStyle=(Stayontop1->Checked)?fsStayOnTop:fsNormal;
        Make();
        return;
    }
    if (currentSkin!=-1)
    {
        skin[currentSkin].defW=Image->Width;
        skin[currentSkin].defH=Image->Height;
    }
    Fullscreenview1->Checked=true;
    Normal1xview1->Checked=false;
    Large2xview1->Checked=false;
    Enterdebugger1->Enabled=false;
    Currentskin1->Enabled=false;
    Showpercentageofactualspeed1->Enabled=false;
    Stayontop1->Enabled=false;
    DebugWnd->Clearallbreakpoints1Click(Sender);
    Make();*/
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Exitemulator1Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Stayontop1Click(TObject *Sender)
{
    Stayontop1->Checked=!Stayontop1->Checked;
    FormStyle=(Stayontop1->Checked)?fsStayOnTop:fsNormal;
    GBC_SetWindow(Handle);
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::ImageMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    TPoint p;
    if (Button==mbLeft)
    {
/*        if (keyHeld!=-1) return;
        int imgW=(skinImageLg)?skinImageLg->Width:(160<<1);
        int imgH=(skinImageLg)?skinImageLg->Height:(144<<1);
        int key=GetKeyAt(X*imgW/Image->Width,
            Y*imgH/Image->Height);
        if (key!=-1)
        {
            kdown(key);
            keyHeld=key;
        }*/
    }
}

void __fastcall TEmuWnd::Enablesound1Click(TObject *Sender)
{
    Enablesound1->Checked=!Enablesound1->Checked;
//    PauseStart();
    if (Enablesound1->Checked)
        GBC_EnableSound();
    else
        GBC_DisableSound();
//    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::ImageMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    if (Button==mbLeft)
    {
//        if (keyHeld==-1) return;
//        kup(keyHeld);
//        keyHeld=-1;
    }
}
//---------------------------------------------------------------------------


void __fastcall TEmuWnd::Noframeskip1Click(TObject *Sender)
{
    GBC_SetLCDUpdateFreq(0);
    Noframeskip1->Checked=true;
    Skip1frame1->Checked=false;
    Skip2frames1->Checked=false;
    Skip3frames1->Checked=false;
    Skip8frames1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Skip1frame1Click(TObject *Sender)
{
    GBC_SetLCDUpdateFreq(1);
    Noframeskip1->Checked=false;
    Skip1frame1->Checked=true;
    Skip2frames1->Checked=false;
    Skip3frames1->Checked=false;
    Skip8frames1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Skip2frames1Click(TObject *Sender)
{
    GBC_SetLCDUpdateFreq(2);
    Noframeskip1->Checked=false;
    Skip1frame1->Checked=false;
    Skip2frames1->Checked=true;
    Skip3frames1->Checked=false;
    Skip8frames1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Skip3frames1Click(TObject *Sender)
{
    GBC_SetLCDUpdateFreq(4);
    Noframeskip1->Checked=false;
    Skip1frame1->Checked=false;
    Skip2frames1->Checked=false;
    Skip3frames1->Checked=true;
    Skip8frames1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Lowqualitysound1Click(TObject *Sender)
{
    GBC_SetSoundQuality(1);
    Lowqualitysound1->Checked=true;
    Mediumqualitysound1->Checked=false;
    Highqualitysound1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Mediumqualitysound1Click(TObject *Sender)
{
    GBC_SetSoundQuality(4);
    Lowqualitysound1->Checked=false;
    Mediumqualitysound1->Checked=true;
    Highqualitysound1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Highqualitysound1Click(TObject *Sender)
{
    GBC_SetSoundQuality(16);
    Lowqualitysound1->Checked=false;
    Mediumqualitysound1->Checked=false;
    Highqualitysound1->Checked=true;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Startindebugmode1Click(TObject *Sender)
{
    startInDebug=!startInDebug;
    Startindebugmode1->Checked=startInDebug;    
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::EnableDebugWarnings1Click(TObject *Sender)
{
/*    int oldRun=run;
    run=0;
    if (DebugWarnDlg->ShowModal()==2)
    {
        int enable=0;
        for (int i=0;i<DebugWarnDlg->List->Items->Count;i++)
        {
            debugWarn[i]=DebugWarnDlg->List->Checked[i];
            if (debugWarn[i]) enable=1;
        }
        EnableDebugWarnings1->Checked=enable;
    }
    run=oldRun;*/
    Enabledebugwarnings1->Checked=GBC_DebugWarnDlg();
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Runatmaximumpossiblespeed1Click(TObject *Sender)
{
    Runatmaximumpossiblespeed1->Checked=!Runatmaximumpossiblespeed1->Checked;
    GBC_SetSpeedRestrict(!Runatmaximumpossiblespeed1->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Skip8frames1Click(TObject *Sender)
{
    GBC_SetLCDUpdateFreq(8);
    Noframeskip1->Checked=false;
    Skip1frame1->Checked=false;
    Skip2frames1->Checked=false;
    Skip3frames1->Checked=false;
    Skip8frames1->Checked=true;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Showskin1Click(TObject *Sender)
{
    Showskin1->Checked=!Showskin1->Checked;
    showSkin=Showskin1->Checked;
    Make();
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::OnMinimize(TObject *Sender)
{
    paused=1;
}

void __fastcall TEmuWnd::OnRestore(TObject *Sender)
{
    paused=0;
}
void __fastcall TEmuWnd::CloseROM1Click(TObject *Sender)
{
    GBC_CloseROM();
    batt->Visible=false;
    SendtoBungXChanger1->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::SendtoBungXChanger1Click(TObject *Sender)
{
    SendToBung(currentROM);
}
//---------------------------------------------------------------------------


void __fastcall TEmuWnd::Emulateforaccuracy1Click(TObject *Sender)
{
    GBC_SetMode(EMUMODE_ACCURACY);
    Emulateforaccuracy1->Checked=true;
    Emulateforspeed1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::Emulateforspeed1Click(TObject *Sender)
{
    GBC_SetMode(EMUMODE_SPEED);
    Emulateforaccuracy1->Checked=false;
    Emulateforspeed1->Checked=true;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::gbMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    if ((X>=48)&&(X<=73)&&(Y>=297)&&(Y<=321))
        GBC_KeyDown(KEY_UP);
    else if ((X>=26)&&(X<=49)&&(Y>=320)&&(Y<=345))
        GBC_KeyDown(KEY_LEFT);
    else if ((X>=73)&&(X<=95)&&(Y>=320)&&(Y<=345))
        GBC_KeyDown(KEY_RIGHT);
    else if ((X>=48)&&(X<=73)&&(Y>=345)&&(Y<=368))
        GBC_KeyDown(KEY_DOWN);
    else if ((X>=175)&&(X<=209)&&(Y>=325)&&(Y<=359))
        GBC_KeyDown(KEY_B);
    else if ((X>=227)&&(X<=262)&&(Y>=308)&&(Y<=342))
        GBC_KeyDown(KEY_A);
    else if ((X>=151)&&(X<=178)&&(Y>=409)&&(Y<=424))
        GBC_KeyDown(KEY_START);
    else if ((X>=107)&&(X<=135)&&(Y>=409)&&(Y<=424))
        GBC_KeyDown(KEY_SELECT);
    else
    {
        clicked=1;
        clickX=X;
        clickY=Y;
    }
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::gbMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    GBC_KeyUp(KEY_UP);
    GBC_KeyUp(KEY_LEFT);
    GBC_KeyUp(KEY_RIGHT);
    GBC_KeyUp(KEY_DOWN);
    GBC_KeyUp(KEY_B);
    GBC_KeyUp(KEY_A);
    GBC_KeyUp(KEY_START);
    GBC_KeyUp(KEY_SELECT);
    clicked=0;
}
//---------------------------------------------------------------------------

void __fastcall TEmuWnd::gbMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
    if (clicked)
    {
        Left+=X-clickX;
        Top+=Y-clickY;
    }
}
//---------------------------------------------------------------------------

