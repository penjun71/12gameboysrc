�
 TDEBUGWND 0�)  TPF0	TDebugWndDebugWndLeft� Top^BorderIconsbiSystemMenu
biMinimize BorderStylebsSingleCaptionDebuggerClientHeighttClientWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Menu	MainMenu1ShowHint	OnCloseQueryFormCloseQueryOnCreate
FormCreatePixelsPerInch`
TextHeight 	TGroupBox	GroupBox1LeftTopWidthMHeight� CaptionCodeTabOrder  TListBoxCodeLeftTopWidth=Height� Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeight
ParentFont	PopupMenuCodePopupMenu1StylelbOwnerDrawFixedTabOrder 
OnDrawItemCodeDrawItem	OnKeyDownCodeKeyDown   	TGroupBox	GroupBox2LeftXTopWidth� Height� Caption	RegistersTabOrder TLabelAFLeftTopWidth1HeightCaptionAF=0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickAFClick  TLabelBCLeftTopWidth1HeightCaptionBC=0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickBCClick  TLabelDELeftTop(Width1HeightCaptionDE=0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickDEClick  TLabelHLLeftTop4Width1HeightCaptionHL=0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickHLClick  TLabelSPLeftTop@Width1HeightCaptionSP=0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickSPClick  TLabelPCLeftTopXWidth1HeightCaptionPC=0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickPCClick  TLabelIMELeftHTopWidth#HeightCaptionIME=0Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickIMEClick  TLabelROMPgLeftHTop(WidthFHeightCaption
ROM PG=000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClick
ROMPgClick  TLabelRAMPgLeftHTop4Width8HeightCaptionRAM PG=0Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClick
RAMPgClick  TLabel	RAMEnableLeftHTop@WidthTHeightCaptionRAM ENABLE=0Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickRAMEnableClick  TLabelCPULeftHTopXWidthbHeightCaptionCPU SPEED=SLOWFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickCPUClick  TLabelVBlankLeftToppWidth� HeightCaptionVBLANK INT=DISABLEDFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickVBlankClick  TLabelLCDCLeftTop|Width� HeightCaptionLCDC INT=  DISABLEDFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClick	LCDCClick  TLabelTimerLeftTop� Width� HeightCaptionTIMER INT= DISABLEDFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClick
TimerClick  TLabelSerialLeftTop� Width� HeightCaptionSERIAL INT=DISABLEDFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickSerialClick  TLabelButtonLeftTop� Width� HeightCaptionBUTTON INT=DISABLEDFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickButtonClick  TLabelLYLeftTop� Width#HeightCaptionLY=00Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickLYClick  TLabelLYCLeftHTop� Width*HeightCaptionLYC=00Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontOnClickLYCClick  TLabelLCDModeLeftTop� WidthiHeightCaptionLCD MODE=VBLANKFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont  TLabelLCDClkLeftTop� Width� HeightCaption0000 CLK UNTIL OAM SRCHFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont   	TGroupBox	GroupBox3LeftXTop� Width� Height}CaptionStackTabOrder TListBoxStackLeftTopWidth� HeighteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeight
ParentFontTabOrder    	TGroupBox	GroupBox4LeftTop� WidthMHeightqCaptionMemoryTabOrder TListBoxMemoryLeftTopWidth=HeightYEnabledFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeight
ParentFontTabOrder   TListBoxMemAddrLeft
TopWidth7HeightUBorderStylebsNoneCtl3DEnabledFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFontTabOrder  TListBoxMemCol0LeftBTopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol0Click
OnDblClickMemCol0DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol1LeftZTopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol1Click
OnDblClickMemCol1DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol2LeftrTopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol2Click
OnDblClickMemCol2DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol3Left� TopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol3Click
OnDblClickMemCol3DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol4Left� TopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol4Click
OnDblClickMemCol4DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol5Left� TopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol5Click
OnDblClickMemCol5DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol6Left� TopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrderOnClickMemCol6Click
OnDblClickMemCol6DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemCol7Left� TopWidthHeightUBorderStylebsNoneCtl3DFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFont	PopupMenuMemoryPopupMenuTabOrder	OnClickMemCol7Click
OnDblClickMemCol7DblClick	OnKeyDownMemCol0KeyDown  TListBoxMemTextLeftTopWidth=HeightUBorderStylebsNoneCtl3DEnabledFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeightParentCtl3D
ParentFontTabOrder
   	TMainMenu	MainMenu1Left�TopL 	TMenuItemDebug1Caption&Debug 	TMenuItemRun1Caption&RunShortCuttOnClick	Run1Click  	TMenuItemStep1Caption&StepShortCutvOnClick
Step1Click  	TMenuItem	Stepover1Caption
Step &overShortCutwOnClickStepover1Click  	TMenuItemRuntocursor1CaptionRun to &cursorShortCutsOnClickRuntocursor1Click  	TMenuItemBreak1Caption&BreakShortCutzOnClickBreak1Click   	TMenuItemView1Caption&View 	TMenuItem	Palettes1Caption	&PalettesOnClickPalettes1Click  	TMenuItemTiles1Caption&TilesOnClickTiles1Click  	TMenuItemMap1Caption&MapOnClick	Map1Click  	TMenuItemSprite1Caption&SpriteEnabled   	TMenuItemBreakpoints1Caption&Breakpoints 	TMenuItemSetbreakpoint1CaptionSet &breakpoint at selectionShortCutqOnClickSetbreakpoint1Click  	TMenuItemSetdatabreakpoint1CaptionSet &data breakpoint...EnabledOnClickSetdatabreakpoint1Click  	TMenuItemN2Caption-  	TMenuItemClearallbreakpoints1CaptionClear &all breakpointsOnClickClearallbreakpoints1Click   	TMenuItemLog1Caption&LogEnabled 	TMenuItemViewlog1Caption&View log...  	TMenuItemEditloggedaddresses1Caption&Edit logged addresses...  	TMenuItemLoglinkportsend1CaptionLog link port &send  	TMenuItemLoglinkportreceive1CaptionLog link port &receive  	TMenuItem	Clearall1Caption
&Clear all  	TMenuItemN1Caption-  	TMenuItemEnablelogging1CaptionEnable &logging    
TPopupMenuCodePopupMenu1Left(Top�  	TMenuItemGotoaddress1Caption&Go to address...ShortCutGOnClickGotoaddress1Click  	TMenuItemGotoPC1Caption	Go to &PCOnClickGotoPC1Click  	TMenuItemSetbreakpoint2CaptionSet &breakpointShortCutqOnClickSetbreakpoint1Click  	TMenuItemSetPC1Caption&Set PC to selectionOnClickSetPC1Click   
TPopupMenuMemoryPopupMenuLeft$Top@ 	TMenuItemMemGoToCaptionGo to &address...ShortCutGOnClickMemGotoClick  	TMenuItemSetbreakpointonwrite1CaptionSet data &breakpoint...  	TMenuItemSearchforvalue1Caption&Search for value...ShortCutSOnClickSearchforvalue1Click   
TPopupMenuHandlesPopupMenuLeft�Top@ 	TMenuItemViewmemory1CaptionView &memory  	TMenuItemViewdisassembly1CaptionView &disassembly    