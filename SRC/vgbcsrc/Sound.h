//---------------------------------------------------------------------------
#ifndef SoundH
#define SoundH

void EnableSound(HWND hWnd);
void DisableSound();
void SoundUpdate(int cycles);
void SoundSetCycles(int n);

extern snd1Enable,snd2Enable,snd3Enable,snd4Enable;
//---------------------------------------------------------------------------
#endif
