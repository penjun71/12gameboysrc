//---------------------------------------------------------------------------
#ifndef regH
#define regH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TRegCode : public TForm
{
__published:	// IDE-managed Components
    TLabel *Label1;
    TEdit *Edit1;
    TButton *Button1;
    void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TRegCode(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRegCode *RegCode;
//---------------------------------------------------------------------------
#endif
