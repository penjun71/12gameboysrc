//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <registry.hpp>
#include <stdio.h>
#include "rom.h"
#include "debug.h"
#include "gbcemu.h"
#include "gotodialog.h"
#include "search.h"
#include "pal.h"
#include "value.h"
#include "dbgwarn.h"
#include "tileview.h"
#include "sound.h"
#include "dbgwarn.h"
#include "debug.h"
#include "z80.h"
#include "reg.h"
#include "MapView.h"
//---------------------------------------------------------------------------
//   Important note about DLL memory management when your DLL uses the
//   static version of the RunTime Library:
//
//   If your DLL exports any functions that pass String objects (or structs/
//   classes containing nested Strings) as parameter or function results,
//   you will need to add the library MEMMGR.LIB to both the DLL project and
//   any other projects that use the DLL.  You will also need to use MEMMGR.LIB
//   if any other projects which use the DLL will be perfomring new or delete
//   operations on any non-TObject-derived classes which are exported from the
//   DLL. Adding MEMMGR.LIB to your project will change the DLL and its calling
//   EXE's to use the BORLNDMM.DLL as their memory manager.  In these cases,
//   the file BORLNDMM.DLL should be deployed along with your DLL.
//
//   To avoid using BORLNDMM.DLL, pass string information using "char *" or
//   ShortString parameters.
//
//   If your DLL uses the dynamic version of the RTL, you do not need to
//   explicitly add MEMMGR.LIB as this will be done implicitly for you
//---------------------------------------------------------------------------
USEFORM("Debug.cpp", DebugWnd);
USEFORM("Gotodialog.cpp", GoToDlg);
USEFORM("Search.cpp", SearchDlg);
USEUNIT("rom.cpp");
USEUNIT("Sound.cpp");
USEFORM("Pal.cpp", PalView);
USEFORM("About.cpp", AboutDlg);
USEFORM("Value.cpp", NewValueDlg);
USEFORM("DbgWarn.cpp", DebugWarnDlg);
USEFORM("TileView.cpp", TileWnd);
USEASM("AsmZ80.asm");
USEFORM("reg.cpp", RegCode);
USEFORM("MapView.cpp", MapWnd);
USELIB("..\..\CBuilder3\Lib\dxguid.lib");
//---------------------------------------------------------------------------
Graphics::TBitmap *bmp;
unsigned long perfFreq;
volatile bool showDebugWnd=false;
extern int debug,run,debugStartPC;
int percentChange=0;
int percent=0;
int enableDebugWarnings=0;
int lcdXOfs,lcdYOfs;
int emuMode=EMUMODE_ACCURACY;
int init=0;
int instrCount=0;

//extern Z80_Regs R;

HWND hWnd;
TCanvas *LCDCanvas;

char* debugErrorText[]=
    {"","","Two consecutive HALT instructions have caused the CPU to hang"};

char* debugWarnText[]=
    {"An attempt was made to access a disabled RAM bank",
     "An attempt was made to access an invalid memory location",
     "Non-high memory was accessed during a DMA operation",
     "DMA was enabled from code outside of the high memory area",
     "Video/OAM memory was accessed during an LCD refresh",
     "An instruction was executed that may trigger the OAM memory bug",
     "The HALT instruction was executed with interrupts disabled and is not followed by a NOP",
     "The STOP instruction was executed without the CPU speed switch set",
     "An attempt was made to switch to a RAM/ROM page that is out of range",
     "The stack is set to ROM area; what were you thinking, you moron?!",
     "GDMA was enabled outside of VBlank"};

//---------------------------------------------------------------------------
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*)
{
    return 1;
}
//---------------------------------------------------------------------------

int _export __cdecl GBC_Init(HWND _hWnd)
{
    hWnd=_hWnd;
    bmp=new Graphics::TBitmap;
    bmp->Width=320;
    bmp->Height=288;
    bmp->PixelFormat=pf15bit;
    for (int i=0;i<288;i++)
        ScreenLine[i]=(char *)bmp->ScanLine[i];
    GenFilter();
    int color=Filter[0x7fff];
    color=((color>>10)<<19)|(((color>>5)&31)<<11)|((color&31)<<3)+0x70707;
    bmp->Canvas->Brush->Color=(TColor)color;
    bmp->Canvas->Pen->Color=(TColor)color;
    bmp->Canvas->Rectangle(0,0,160*scaleFact,144*scaleFact);
    LARGE_INTEGER li;
    QueryPerformanceFrequency(&li);
    perfFreq=li.u.LowPart/32;
    LCDCanvas=new TCanvas;
    LCDCanvas->Handle=GetDC(hWnd);
    for (int i=0;i<DBW_COUNT;i++)
        debugWarn[i]=0;
    DebugWnd=new TDebugWnd(NULL);
    GoToDlg=new TGoToDlg(NULL);
    SearchDlg=new TSearchDlg(NULL);
    NewValueDlg=new TNewValueDlg(NULL);
    PalView=new TPalView(NULL);
    DebugWarnDlg=new TDebugWarnDlg(NULL);
    TileWnd=new TTileWnd(NULL);
    MapWnd=new TMapWnd(NULL);
    EnableSound(hWnd);
    instrCount=0;
    init=1;
    return 1;
}

void _export __cdecl GBC_SetWindow(HWND _hWnd)
{
    if (init==0) return;
    hWnd=_hWnd;
    LCDCanvas->Handle=GetDC(hWnd);
}

void _export __cdecl GBC_Close()
{
    if (init==0) return;
    DisableSound();
    ReleaseDC(hWnd,LCDCanvas->Handle);
    delete LCDCanvas;
    CloseEmu();
    delete bmp;
}

void _export __cdecl GBC_LoadROM(char *filename)
{
    if (init==0) return;
    CloseEmu();
    InitEmu(filename);
    Reset();
}

void _export __cdecl GBC_CloseROM()
{
    if (init==0) return;
    CloseEmu();
    int color=Filter[0x7fff];
    color=((color>>10)<<19)|(((color>>5)&31)<<11)|((color&31)<<3)+0x70707;
    bmp->Canvas->Brush->Color=(TColor)color;
    bmp->Canvas->Pen->Color=(TColor)color;
    bmp->Canvas->Rectangle(0,0,160*scaleFact,144*scaleFact);
    LCDCanvas->Draw(lcdXOfs,lcdYOfs,bmp);
}

void _export __cdecl GBC_Run()
{
    if (init==0) return;
    if (!romLoaded)
    {
        Sleep(50);
        return;
    }
    if (showDebugWnd)
    {
        DebugWnd->Show();
        DebugWnd->Update();
        showDebugWnd=false;
        return;
    }
    if ((!debug)||(run))
    {
        int i;
        for (i=0;i<=1;i++)
        {
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
            Execute();
        }
        if ((!soundEnable)&&(restrictSpeed))
        {
            static int lastTime=0;
            LARGE_INTEGER li;
            if (!lastTime)
            {
                QueryPerformanceCounter(&li);
                lastTime=li.u.LowPart;
            }
            QueryPerformanceCounter(&li);
            int curTime=li.u.LowPart;
            int time=curTime-lastTime;
            if (time<perfFreq)
            {
                while (time<perfFreq)
                {
                    Sleep(10);
                    QueryPerformanceCounter(&li);
                    curTime=li.u.LowPart;
                    time=curTime-lastTime;
                }
                lastTime+=perfFreq;
            }
            else
                lastTime=curTime;
        }
        if (run)
        {
            static int ticks=0; ticks+=133;
            static int lastTime=GetTickCount();
            int curTime=GetTickCount();
            if ((curTime-lastTime)>=1000)
            {
                percentChange++;
                percent=(ticks*100)/(4194*(curTime-lastTime)/1000);
                lastTime=curTime;
                ticks=0;
            }
        }
    }
    else
        Sleep(10);
    if (debugWarnMsg!=-1)
    {
        if (debugWarnMsg>=0)
        {
            MessageBox(DebugWnd->Handle,debugWarnText[
                debugWarnMsg],"Debug Warning",
                MB_OK|MB_ICONWARNING);
        }
        else
        {
            MessageBox(DebugWnd->Handle,debugErrorText[
                -debugWarnMsg],"Error",MB_OK|MB_ICONSTOP);
        }
        debugWarnMsg=-1;
    }
}

void _export __cdecl GBC_PaintLCD()
{
    if (init==0) return;
    LCDCanvas->Draw(lcdXOfs,lcdYOfs,bmp);
}

void _export __cdecl GBC_Reset()
{
    if (init==0) return;
    Reset();
}

void _export __cdecl GBC_SetLCDUpdateFreq(int freq)
{
    if (init==0) return;
    lcdUpdateFreq=freq;
}

void _export __cdecl GBC_SetSoundQuality(int quality)
{
    if (init==0) return;
    sndQuality=quality;
}

void _export __cdecl GBC_EnableSound()
{
    if (init==0) return;
    EnableSound(hWnd);
}

void _export __cdecl GBC_DisableSound()
{
    if (init==0) return;
    DisableSound();
}

void _export __cdecl GBC_KeyDown(int key)
{
    if (init==0) return;
    keys&=~key;
    intReq|=INT_KEY;
}

void _export __cdecl GBC_KeyUp(int key)
{
    if (init==0) return;
    keys|=key;
    intReq|=INT_KEY;
}

void _export __cdecl GBC_SetSpeedRestrict(int restrict)
{
    if (init==0) return;
    restrictSpeed=restrict;
}

int _export __cdecl GBC_GetPercentSpeed()
{
    if (init==0) return 0;
    return percent;
}

void _export __cdecl GBC_EnterDebug()
{
    if (init==0) return;
    debug=1; run=0;
    debugStartPC=regPC;
    DebugWnd->Show();
    DebugWnd->Update();
}

int _export __cdecl GBC_DebugWarnDlg()
{
    if (init==0) return enableDebugWarnings;
    int oldRun=run;
    run=0;
    if (DebugWarnDlg->ShowModal()==2)
    {
        int enable=0;
        for (int i=0;i<DebugWarnDlg->List->Items->Count;i++)
        {
            debugWarn[i]=DebugWarnDlg->List->Checked[i];
            if (debugWarn[i]) enable=1;
        }
        enableDebugWarnings=enable;
    }
    run=oldRun;
    return enableDebugWarnings;
}

void _export __cdecl GBC_LCDScaleFact(int fact)
{
    if (init==0) return;
    scaleFact=fact;
    bmp->Width=160*scaleFact;
    bmp->Height=144*scaleFact;
    bmp->PixelFormat=pf15bit;
    for (int i=0;i<(144*scaleFact);i++)
        ScreenLine[i]=(char *)bmp->ScanLine[i];
}

void _export __cdecl GBC_LCDPos(int x,int y)
{
    if (init==0) return;
    lcdXOfs=x;
    lcdYOfs=y;
}

void _export __cdecl GBC_SetMode(int mode)
{
    if (init==0) return;
    emuMode=mode;
    if (emuMode==EMUMODE_ACCURACY)
    {
        getmemfunc=getmem;
        putmemfunc=putmem;
    }
    else
    {
        getmemfunc=getmem_direct;
        putmemfunc=putmem_direct;
    }
}

