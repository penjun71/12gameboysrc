//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Debug.h"
#include "Pal.h"
#define NOTYPEREDEF
#include "gotodialog.h"
#include "Value.h"
//#include "Entry.h"
//#include "Data.h"
#include "Search.h"
//#include "Log.h"
//#include "EditLog.h"
//#include "AddLog.h"
#include "rom.h"
#include "z80.h"
#include "z80dasm.h"
#include "TileView.h"
#include "MapView.h"

#define GETBYTE(x) (getmem_direct(x)&0xff)
#define GETWORD(x) ((getmem_direct(x)|(getmem_direct(x+1)<<8))&0xffff)
#define GETDWORD(x) (GETWORD(x)|(GETWORD(x+2)<<16))

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDebugWnd *DebugWnd;
int debug=0;
int run=1;
int debugStartPC,debugStartMem;
int debugCheckEnable;
int debugEnforceAddr=0;
int debugFirstInstrucLen;
int debugCodeBreakCount,debugCodeBreak[256];
int debugDataBreakCount,debugDataBreakType[256];
int debugDataBreakLow[256],debugDataBreakHigh[256];
int debugDataBreak;
int debugProgBreak,debugProgBreakOfs,debugProgLen;
int debugBeginStepOver,debugEndStepOver;
int debugStepOver,debugStepGoal;
int debugStepA7;
int debugForceUpdate;
int debugLogEnable;
int debugLogCount;
int debugLogClass[256];
int debugLogType[256];
int debugLogLow[256];
int debugLogHigh[256];
int debugLogCheck[256];
int debugLogLast[256];
int debugLogState[256];
int debugLogAny[256];
int debugLogLinkSend,debugLogLinkRecv;
int debugException=0;
int debugRunToCursor=0;
int debugCursorAddr;
int exceptBPCount,exceptBP[48];
int endStepOver=0,ignoreStack=0;
//extern int readmem24(int a);
//extern int readmem24_word(int a);
//extern int readmem24_dword(int a);
//extern int writemem24(int a,int v);
//extern int writemem24_word(int a,int v);
//extern int writemem24_dword(int a,int v);
//extern void UseDebugReadFuncs();
//extern void RestoreReadFuncs();
extern void PauseStart();
extern void PauseEnd();
//extern Z80_Regs R;
volatile extern int emuRunning;
extern int lcdClk;
extern int enableDebugWarnings;

static char tmpStr[64];

//---------------------------------------------------------------------------
__fastcall TDebugWnd::TDebugWnd(TComponent* Owner)
    : TForm(Owner)
{
    debugStartMem=0;
    debugProgBreak=0;
    debugCodeBreakCount=0;
    debugDataBreakCount=0;
    debugDataBreak=0;
    debugStepOver=0;
    debugBeginStepOver=0;
    debugForceUpdate=1;
    debugLogEnable=0;
    debugLogCount=0;
    debugLogLinkSend=0;
    debugLogLinkRecv=0;
}

//---------------------------------------------------------------------------
void __fastcall TDebugWnd::Update()
{
    char str[256],str2[256];
    int useOldPC=0;

    if (!romLoaded) return;
//    PauseStart();
    if ((!run)&&debugStepOver)
        debugStepOver=0;
    if ((!run)&&debugDataBreak)
    {
        debugDataBreak=0;
        useOldPC=1;
    }
    int chg,i,j,k,max;
//    sprintf(str,"AF=%4.4X",R.AF.D); AF->Caption=str; AF->Update();
//    sprintf(str,"BC=%4.4X",R.BC.D); BC->Caption=str; BC->Update();
//    sprintf(str,"DE=%4.4X",R.DE.D); DE->Caption=str; DE->Update();
//    sprintf(str,"HL=%4.4X",R.HL.D); HL->Caption=str; HL->Update();
//    sprintf(str,"SP=%4.4X",R.SP.D); SP->Caption=str; SP->Update();
    #ifdef C_CORE
        sprintf(str,"AF=%4.4X",regAF); AF->Caption=str; AF->Update();
    #else
        int z=regAF&0x40?0x80:0;
        int n=nFlag?0x40:0;
        int h=regAF&0x10?0x20:0;
        int c=regAF&1?0x10:0;
        sprintf(str,"AF=%4.4X",(regAF&0xff00)|z|n|h|c); AF->Caption=str; AF->Update();
    #endif
    sprintf(str,"BC=%4.4X",regBC); BC->Caption=str; BC->Update();
    sprintf(str,"DE=%4.4X",regDE); DE->Caption=str; DE->Update();
    sprintf(str,"HL=%4.4X",regHL); HL->Caption=str; HL->Update();
    sprintf(str,"SP=%4.4X",regSP); SP->Caption=str; SP->Update();
//        MakeSR();
//    sprintf(str,"PC=%4.4X",R.PC.D); PC->Caption=str; PC->Update();
    sprintf(str,"PC=%4.4X",regPC); PC->Caption=str; PC->Update();
//    sprintf(str,"IME=%X",R.IFF1?1:0); IME->Caption=str; IME->Update();
    sprintf(str,"IME=%X",imeFlag); IME->Caption=str; IME->Update();
    sprintf(str,"ROM PG=%3.3X",romPage); ROMPg->Caption=str; ROMPg->Update();
    sprintf(str,"RAM PG=%X",ramPage); RAMPg->Caption=str; RAMPg->Update();
    sprintf(str,"RAM ENABLE=%X",ramEnable&1); RAMEnable->Caption=str; RAMEnable->Update();
    sprintf(str,"CPU SPEED=%s",(clkMult==1)?"SLOW":"FAST"); CPU->Caption=str; CPU->Update();
    sprintf(str,"VBLANK INT=%s",(hiRam[0xff]&1)?"ENABLED":"DISABLED"); VBlank->Caption=str; VBlank->Update();
    sprintf(str,"LCDC INT=  %s",(hiRam[0xff]&2)?"ENABLED":"DISABLED"); LCDC->Caption=str; LCDC->Update();
    sprintf(str,"TIMER INT= %s",(hiRam[0xff]&4)?"ENABLED":"DISABLED"); Timer->Caption=str; Timer->Update();
    sprintf(str,"SERIAL INT=%s",(hiRam[0xff]&8)?"ENABLED":"DISABLED"); Serial->Caption=str; Serial->Update();
    sprintf(str,"BUTTON INT=%s",(hiRam[0xff]&16)?"ENABLED":"DISABLED"); Button->Caption=str; Button->Update();
    sprintf(str,"LY=%2.2X",lcdY); LY->Caption=str; LY->Update();
    sprintf(str,"LYC=%2.2X",hiRam[0x45]&0xff); LYC->Caption=str; LYC->Update();
    sprintf(str,"LCD MODE=%s",(lcdMode==0)?"HBLANK":(lcdMode==1)?"VBLANK":(lcdMode==2)?"OAM SRCH":"TRANSFER"); LCDMode->Caption=str; LCDMode->Update();
//    sprintf(str,"%4.4d CLK UNTIL %s",(lcdMode==0)?(203*clkMult)-lcdClk:(lcdMode==1)?((153-lcdY)*(456*clkMult))+((456*clkMult)-lcdClk):(lcdMode==2)?(83*clkMult)-lcdClk:(175*clkMult)-lcdClk,(lcdMode==0)?"OAM SRCH":(lcdMode==1)?"OAM SRCH":(lcdMode==2)?"TRANSFER":(lcdY>=143)?"VBLANK":"HBLANK"); LCDClk->Caption=str; LCDClk->Update();
    sprintf(str,"%4.4d CLK UNTIL %s",lcdClk,(lcdMode==0)?"OAM SRCH":(lcdMode==1)?"OAM SRCH":(lcdMode==2)?"TRANSFER":(lcdY>=143)?"VBLANK":"HBLANK"); LCDClk->Caption=str; LCDClk->Update();
    int updateCode=1;
    int pc=regPC;//R.PC.D;
    if ((Code->Items->Count)&&(!debugEnforceAddr))
    {
        for (i=0,j=-1,k=-1;i<15;i++)
        {
            int addr;
            if (Code->Items->Strings[i].c_str()[0]!='@')
            {
                sscanf(Code->Items->Strings[i].c_str(),"%x",&addr);
                if (addr==pc)
                {
                    j=i;
                    if (!useOldPC) k=i;
                }
                if ((useOldPC)&&(addr==debugStartPC))
                    k=i;
            }
        }
        if ((j!=-1)&&(k==-1)) k=j;
        if (j!=-1)
        {
            if (j>=11)
            {
                if (Code->Items->Strings[j-10].c_str()[0]!='@')
                {
                    sscanf(Code->Items->Strings[j-10].c_str(),
                        "%x",&debugStartPC);
                }
                else
                {
                    sscanf(Code->Items->Strings[j-9].c_str(),
                        "%x",&debugStartPC);
                }
            }
            else
            {
                if (Code->Items->Strings[0].c_str()[0]!='@')
                {
                    sscanf(Code->Items->Strings[0].c_str(),
                        "%x",&debugStartPC);
                }
                else
                {
                    sscanf(Code->Items->Strings[1].c_str(),
                        "%x",&debugStartPC);
                }
                if (!debugForceUpdate)
                    updateCode=0;
                for (i=0;i<15;i++)
                {
                    if (i==j)
                    {
                        strcpy(str,Code->Items->Strings[i].c_str());
                        str[4]='-'; str[5]='>';
                        Code->Items->Strings[i]=str;
                    }
                    else
                    {
                        strcpy(str,Code->Items->Strings[i].c_str());
                        if (str[0]!='@')
                        {
                            str[4]=':'; str[5]=' ';
                            Code->Items->Strings[i]=str;
                        }
                    }
                }
                Code->ItemIndex=k;
            }
        }
    }
    debugEnforceAddr=0;
    if (updateCode)
    {
        unsigned short buf[256];
        for (i=0;i<256;i++)
            buf[i]=GETWORD(debugStartPC+(i<<1));
        Code->Clear();
        int first=1;
        for (i=0,j=0,k=-1;i<15;i++)
        {
            int oj=j;
//            if (IsROMAddr(debugStartPC+j))
//            {
//                sprintf(str2,"@%s:",ROMAddrName(
//                    debugStartPC+j));
//                Code->Items->Add(str2);
//                i++; if (i>=15) break;
//            }
            j+=DasmZ80(str,debugStartPC+j);
            if (first)
            {
                debugFirstInstrucLen=j;
                first=0;
            }
            if ((debugStartPC+oj)!=pc)
                sprintf(str2,"%4.4X: %s",(debugStartPC+oj)&0xffffff,str);
            else
            {
                sprintf(str2,"%4.4X->%s",(debugStartPC+oj)&0xffffff,str);
                k=i;
            }
            Code->Items->Add(str2);
        }
        Code->ItemIndex=k;
    }
    Code->Update();
    Stack->Clear();
    for (i=0;i<16;i++)
    {
//        sprintf(str,"%4.4X: %4.4X",R.SP.D+(i*2),GETWORD(
//            R.SP.D+(i*2)));
        sprintf(str,"%4.4X: %4.4X",regSP+(i*2),GETWORD(
            regSP+(i*2)));
        Stack->Items->Add(str);
    }
    Stack->Update();
    UpdateMem();
    if (PalView->Visible) PalView->DoUpdate();
    if (TileWnd->Visible) TileWnd->DoUpdate();
    debugForceUpdate=0;
//    PauseEnd();
}

void __fastcall TDebugWnd::UpdateMem()
{
    if (!romLoaded) return;
    int i,j;
    char str[32];
    TListBox *memCol[8]={MemCol0,MemCol1,MemCol2,MemCol3,
        MemCol4,MemCol5,MemCol6,MemCol7};
//    PauseStart();
    MemAddr->Clear(); MemCol0->Clear(); MemCol1->Clear();
    MemCol2->Clear(); MemCol3->Clear(); MemCol4->Clear();
    MemCol5->Clear(); MemCol6->Clear(); MemCol7->Clear();
    MemText->Clear();
    for (i=0;i<0x30;)
    {
        sprintf(str,"%6.6X",(debugStartMem+i)&0xffffff);
        MemAddr->Items->Add(str);
        for (j=0;j<8;j++)
        {
            sprintf(str,"%2.2X",getmem_direct(debugStartMem+i+j));
            memCol[j]->Items->Add(str);
        }
        str[8]=0;
        for (j=0;j<8;j++,i++)
        {
            int c=getmem_direct(debugStartMem+i)&0xff;
            if ((c>=32)&&(c<128))
                str[j]=c;
            else
                str[j]='.';
        }
        MemText->Items->Add(str);
    }
    MemAddr->Repaint(); MemCol0->Repaint(); MemCol1->Repaint();
    MemCol2->Repaint(); MemCol3->Repaint(); MemCol4->Repaint();
    MemCol5->Repaint(); MemCol6->Repaint(); MemCol7->Repaint();
    MemText->Repaint();
//    PauseEnd();
}

void __fastcall TDebugWnd::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    debugStepOver=0;
    debug=0; run=1;
    CanClose=false;
    Hide();
    debugForceUpdate=1;
    debugException=0;
}
//---------------------------------------------------------------------------

//void DoOneInstruction();

void __fastcall TDebugWnd::Step1Click(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    debugException=0;
//    PauseStart();
    run=1;
    OneInstruction();
    run=0;
    debugStartPC=regPC;//R.PC.D;
    Update();
//    PauseEnd();
    #endif
}
//---------------------------------------------------------------------------


void __fastcall TDebugWnd::Run1Click(TObject *Sender)
{
    debugException=0;
    debugStepOver=0;
    debug=0; run=1;
    Hide();
    debugForceUpdate=1;
}
//---------------------------------------------------------------------------


void __fastcall TDebugWnd::CodeKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if (!romLoaded) return;
    if (Key==VK_PRIOR)
    {
        debugStartPC--;
        debugEnforceAddr=1;
        Update();
        Application->ProcessMessages();
    }
    else if (Key==VK_NEXT)
    {
        debugStartPC+=debugFirstInstrucLen;
        debugEnforceAddr=1;
        Update();
        Application->ProcessMessages();
    }
    else if (Key==VK_UP)
    {
        if (Code->ItemIndex==0)
        {
            debugStartPC--;
            debugEnforceAddr=1;
            Update();
            Application->ProcessMessages();
            Code->ItemIndex=0;
        }
    }
    else if (Key==VK_DOWN)
    {
        if (Code->ItemIndex==14)
        {
            debugStartPC+=debugFirstInstrucLen;
            debugEnforceAddr=1;
            Update();
            Application->ProcessMessages();
            Code->ItemIndex=14;
        }
    }
    else if (Key=='G')
        Gotoaddress1Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Gotoaddress1Click(TObject *Sender)
{
    if (!romLoaded) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    GoToDlg->Address->Text="";
    if (GoToDlg->ShowModal()==1)
    {
        sscanf(GoToDlg->Address->Text.c_str(),"%x",&debugStartPC);
        debugEnforceAddr=1;
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::SetPC1Click(TObject *Sender)
{
    if (!romLoaded) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    debugException=0;
    if (Code->ItemIndex==-1) return;
    sscanf(Code->Items->Strings[Code->ItemIndex].c_str(),
        "%x",&regPC);//R.PC.D);
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Break1Click(TObject *Sender)
{
    if (!romLoaded) return;
    debug=1; run=0;
//    while (emuRunning)
//        Sleep(0);
    debugStartPC=regPC;//R.PC.D;
    Update();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol0KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if (!romLoaded) return;
    if (Key=='G')
        MemGotoClick(Sender);
    else if (Key=='S')
        Searchforvalue1Click(Sender);
    else if (Key==VK_PRIOR)
    {
        int row=((TListBox*)Sender)->ItemIndex;
        debugStartMem-=0x30;
        UpdateMem();
        ((TListBox*)Sender)->ItemIndex=row;
        Key=0;
    }
    else if (Key==VK_NEXT)
    {
        int row=((TListBox*)Sender)->ItemIndex;
        debugStartMem+=0x30;
        UpdateMem();
        ((TListBox*)Sender)->ItemIndex=row;
        Key=0;
    }
    else if (Key==VK_DOWN)
    {
        if (((TListBox *)Sender)->ItemIndex==5)
        {
            debugStartMem+=8;
            UpdateMem();
            ((TListBox *)Sender)->ItemIndex=5;
        }
    }
    else if (Key==VK_UP)
    {
        if (((TListBox *)Sender)->ItemIndex==0)
        {
            debugStartMem-=8;
            UpdateMem();
            ((TListBox *)Sender)->ItemIndex=0;
        }
    }
    else if (Key==VK_LEFT)
    {
        if (Sender==MemCol1)
        {
            MemCol0->ItemIndex=MemCol1->ItemIndex;
            MemCol1->ItemIndex=-1;
            ActiveControl=MemCol0;
        }
        else if (Sender==MemCol2)
        {
            MemCol1->ItemIndex=MemCol2->ItemIndex;
            MemCol2->ItemIndex=-1;
            ActiveControl=MemCol1;
        }
        else if (Sender==MemCol3)
        {
            MemCol2->ItemIndex=MemCol3->ItemIndex;
            MemCol3->ItemIndex=-1;
            ActiveControl=MemCol2;
        }
        else if (Sender==MemCol4)
        {
            MemCol3->ItemIndex=MemCol4->ItemIndex;
            MemCol4->ItemIndex=-1;
            ActiveControl=MemCol3;
        }
        else if (Sender==MemCol5)
        {
            MemCol4->ItemIndex=MemCol5->ItemIndex;
            MemCol5->ItemIndex=-1;
            ActiveControl=MemCol4;
        }
        else if (Sender==MemCol6)
        {
            MemCol5->ItemIndex=MemCol6->ItemIndex;
            MemCol6->ItemIndex=-1;
            ActiveControl=MemCol5;
        }
        else if (Sender==MemCol7)
        {
            MemCol6->ItemIndex=MemCol7->ItemIndex;
            MemCol7->ItemIndex=-1;
            ActiveControl=MemCol6;
        }
        Key=0;
    }
    else if (Key==VK_RIGHT)
    {
        if (Sender==MemCol0)
        {
            MemCol1->ItemIndex=MemCol0->ItemIndex;
            MemCol0->ItemIndex=-1;
            ActiveControl=MemCol1;
        }
        else if (Sender==MemCol1)
        {
            MemCol2->ItemIndex=MemCol1->ItemIndex;
            MemCol1->ItemIndex=-1;
            ActiveControl=MemCol2;
        }
        else if (Sender==MemCol2)
        {
            MemCol3->ItemIndex=MemCol2->ItemIndex;
            MemCol2->ItemIndex=-1;
            ActiveControl=MemCol3;
        }
        else if (Sender==MemCol3)
        {
            MemCol4->ItemIndex=MemCol3->ItemIndex;
            MemCol3->ItemIndex=-1;
            ActiveControl=MemCol4;
        }
        else if (Sender==MemCol4)
        {
            MemCol5->ItemIndex=MemCol4->ItemIndex;
            MemCol4->ItemIndex=-1;
            ActiveControl=MemCol5;
        }
        else if (Sender==MemCol5)
        {
            MemCol6->ItemIndex=MemCol5->ItemIndex;
            MemCol5->ItemIndex=-1;
            ActiveControl=MemCol6;
        }
        else if (Sender==MemCol6)
        {
            MemCol7->ItemIndex=MemCol6->ItemIndex;
            MemCol6->ItemIndex=-1;
            ActiveControl=MemCol7;
        }
        Key=0;
    }
    else if (Key==VK_RETURN)
    {
        if (Sender==MemCol0)
            MemDblClick(0,MemCol0->ItemIndex);
        else if (Sender==MemCol1)
            MemDblClick(1,MemCol1->ItemIndex);
        else if (Sender==MemCol2)
            MemDblClick(2,MemCol2->ItemIndex);
        else if (Sender==MemCol3)
            MemDblClick(3,MemCol3->ItemIndex);
        else if (Sender==MemCol4)
            MemDblClick(4,MemCol4->ItemIndex);
        else if (Sender==MemCol5)
            MemDblClick(5,MemCol5->ItemIndex);
        else if (Sender==MemCol6)
            MemDblClick(6,MemCol6->ItemIndex);
        else if (Sender==MemCol7)
            MemDblClick(7,MemCol7->ItemIndex);
    }
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemGotoClick(TObject *Sender)
{
    if (!romLoaded) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    GoToDlg->Address->Text="";
    if (GoToDlg->ShowModal()==1)
    {
        sscanf(GoToDlg->Address->Text.c_str(),"%x",&debugStartMem);
        UpdateMem();
    }
    #endif
}

void __fastcall TDebugWnd::MemCol0Click(TObject *Sender)
{
    MemCol1->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol1Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol2Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol1->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol3Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol1->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol4Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol1->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol5Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol1->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol6Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol1->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol7->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol7Click(TObject *Sender)
{
    MemCol0->ItemIndex=-1;
    MemCol1->ItemIndex=-1;
    MemCol2->ItemIndex=-1;
    MemCol3->ItemIndex=-1;
    MemCol4->ItemIndex=-1;
    MemCol5->ItemIndex=-1;
    MemCol6->ItemIndex=-1;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemDblClick(int col,int selindx)
{
    if (!romLoaded) return;
    if (selindx==-1) return;
    #ifdef DEMO
    MessageBox(NULL,"Memory editing is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    int addr=debugStartMem+col+(selindx<<3);
    char str[4];
    sprintf(str,"%2.2X",getmem_direct(addr)&0xff);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
        int v;
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&v);
        putmem_direct(addr&0xffffff,v);
        int row=selindx;
        UpdateMem();
        switch(col)
        {
            case 0: MemCol0->ItemIndex=row; break;
            case 1: MemCol1->ItemIndex=row; break;
            case 2: MemCol2->ItemIndex=row; break;
            case 3: MemCol3->ItemIndex=row; break;
            case 4: MemCol4->ItemIndex=row; break;
            case 5: MemCol5->ItemIndex=row; break;
            case 6: MemCol6->ItemIndex=row; break;
            case 7: MemCol7->ItemIndex=row; break;
        }
    }
    #endif
}

void __fastcall TDebugWnd::MemCol0DblClick(TObject *Sender)
{
    MemDblClick(0,MemCol0->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol1DblClick(TObject *Sender)
{
    MemDblClick(1,MemCol1->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol2DblClick(TObject *Sender)
{
    MemDblClick(2,MemCol2->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol3DblClick(TObject *Sender)
{
    MemDblClick(3,MemCol3->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol4DblClick(TObject *Sender)
{
    MemDblClick(4,MemCol4->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol5DblClick(TObject *Sender)
{
    MemDblClick(5,MemCol5->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol6DblClick(TObject *Sender)
{
    MemDblClick(6,MemCol6->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::MemCol7DblClick(TObject *Sender)
{
    MemDblClick(7,MemCol7->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Setbreakpoint1Click(TObject *Sender)
{
    if (!romLoaded) return;
    if (Code->ItemIndex==-1) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    int pc,i,j;
//    PauseStart();
    sscanf(Code->Items->Strings[Code->ItemIndex].c_str(),
        "%x",&pc);
    for (i=0;i<debugCodeBreakCount;i++)
    {
        if (debugCodeBreak[i]==pc)
        {
            for (j=i;j<(debugCodeBreakCount-1);j++)
                debugCodeBreak[j]=debugCodeBreak[j+1];
            debugCodeBreakCount--;
            Code->Repaint();
//            PauseEnd();
            return;
        }
    }
    debugCodeBreak[debugCodeBreakCount++]=pc;
    Code->Repaint();
//    PauseEnd();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::CodeDrawItem(TWinControl *Control, int Index,
      TRect &Rect, TOwnerDrawState State)
{
    if (!romLoaded) return;
    TBrush *brush=new TBrush;
    TBrush *oldBrush=Code->Canvas->Brush;
    brush->Style=bsSolid;
    TPen *pen=new TPen;
    TPen *oldPen=Code->Canvas->Pen;
    int pos,bp=0;
    int txt=0;
    if (Code->Items->Strings[Index].c_str()[0]!='@')
    {
        sscanf(Code->Items->Strings[Index].c_str(),
            "%X",&pos);
        for (int i=0;i<debugCodeBreakCount;i++)
        {
            if (pos==debugCodeBreak[i])
                bp=1;
        }
    }
    else
        txt=1;
    if (State.Contains(odSelected))
    {
        if ((!txt)&&(Code->Items->Strings[Index].c_str()[4]=='-'))
        {
            if (bp)
            {
                brush->Color=clDkGray;
                pen->Color=clWhite;
            }
            else
            {
                brush->Color=clTeal;
                pen->Color=clWhite;
            }
        }
        else
        {
            if (bp)
            {
                brush->Color=clPurple;
                pen->Color=clWhite;
            }
            else
            {
                brush->Color=clNavy;
                pen->Color=clWhite;
            }
        }
    }
    else
    {
        if ((!txt)&&(Code->Items->Strings[Index].c_str()[4]=='-'))
        {
            if (bp)
            {
                brush->Color=clOlive;
                pen->Color=clWhite;
            }
            else
            {
                brush->Color=clGreen;
                pen->Color=clWhite;
            }
        }
        else
        {
            if (bp)
            {
                brush->Color=clMaroon;
                pen->Color=clWhite;
            }
            else
            {
                brush->Color=clWhite;
                pen->Color=clBlack;
            }
        }
    }
    Code->Canvas->Brush=brush;
    Code->Canvas->Pen=pen;
    Code->Canvas->FillRect(Rect);
    Code->Canvas->Font->Color=pen->Color;
    HFONT fnt;
    TFont *oldFont;
    if (txt)
    {
        fnt=CreateFont(-MulDiv(8,GetDeviceCaps(
            Code->Canvas->Handle, LOGPIXELSY),72),0,0,0,
            FW_BOLD,0,0,0,ANSI_CHARSET,OUT_DEFAULT_PRECIS,
            CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
            FF_DONTCARE,"Courier New");
        oldFont=Code->Canvas->Font;
        Code->Canvas->Font->Handle=fnt;
        Code->Canvas->TextOut(Rect.Left,Rect.Top,&(Code->Items->
            Strings[Index].c_str()[1]));
        Code->Canvas->Font=oldFont;
        DeleteObject(fnt);
    }
    else
    {
        Code->Canvas->TextOut(Rect.Left,Rect.Top,Code->Items->
            Strings[Index]);
    }
    Code->Canvas->Brush=oldBrush;
    Code->Canvas->Pen=oldPen;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::GotoPC1Click(TObject *Sender)
{
    debugStartPC=regPC;//R.PC.D;
    Update();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Clearallbreakpoints1Click(TObject *Sender)
{
//    PauseStart();
    debugCodeBreakCount=0;
    debugDataBreakCount=0;
    debugProgBreak=0;
    Code->Repaint();
//    PauseEnd();
}
//---------------------------------------------------------------------------
void __fastcall TDebugWnd::Setdatabreakpoint1Click(TObject *Sender)
{
    if (!romLoaded) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #endif
//    PauseStart();
//    DataBreakDlg->UpdateList();
//    DataBreakDlg->ShowModal();
//    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Searchforvalue1Click(TObject *Sender)
{
    if (!romLoaded) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    SearchDlg->ActiveControl=SearchDlg->Value;
    if (SearchDlg->ShowModal()==1)
    {
        int v,addr;
        if (SearchDlg->Size->ItemIndex<3)
        {
            sscanf(SearchDlg->Value->Text.c_str(),"%x",&v);
            if (SearchDlg->Size->ItemIndex==0) v&=0xff;
            else if (SearchDlg->Size->ItemIndex==1) v&=0xffff;
            addr=(debugStartMem+2)&0xffffff;
            int size=SearchDlg->Size->ItemIndex;
            if (size!=0) addr&=~1;
            for (;addr<0x10000;addr+=(size==0)?1:2)
            {
                if (size==0)
                {
                    if ((getmem_direct(addr)&0xff)==v)
                    {
                        debugStartMem=addr;
                        UpdateMem();
                        break;
                    }
                }
                else if (size==1)
                {
                    if ((GETWORD(addr)&0xffff)==v)
                    {
                        debugStartMem=addr;
                        UpdateMem();
                        break;
                    }
                }
                else
                {
                    if (GETDWORD(addr)==v)
                    {
                        debugStartMem=addr;
                        UpdateMem();
                        break;
                    }
                }
            }
        }
        else
        {
            char str[256];
            int indx;

            addr=(debugStartMem+2)&0xffffff;
            strcpy(str,SearchDlg->Value->Text.c_str());
            for (indx=0;addr<0x10000;addr++)
            {
                if (!str[indx])
                {
                    addr-=strlen(str);
                    debugStartMem=addr;
                    UpdateMem();
                    break;
                }
                if ((getmem_direct(addr)&0xff)!=(str[indx]&0xff))
                    indx=0;
                else
                    indx++;
            }
        }
        if (addr>=0x10000)
            MessageBox(Handle,"No matches found","Search error",MB_OK|MB_TASKMODAL);
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Stepover1Click(TObject *Sender)
{
    if (!romLoaded) return;
    debugException=0;
    if (debugStepOver) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    unsigned short buf[16];
    char str[64];
    int addr,i;
//    PauseStart();
//    for (i=0;i<16;i++)
//        buf[i]=GETWORD(R.PC.D+(i<<1));
    for (i=0;i<16;i++)
        buf[i]=GETWORD(regPC+(i<<1));
//    addr=R.PC.D+DasmZ80(str,R.PC.D);
    addr=regPC+DasmZ80(str,regPC);
    debugStepOver=1; debugStepGoal=addr;
    debugBeginStepOver=1; debugEndStepOver=0;
    debugStepA7=regSP;//R.SP.D;
    run=1;
    OneInstruction();
//    PauseEnd();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::AFClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%4.4X",R.AF.D&0xffff);
    sprintf(str,"%4.4X",regAF&0xffff);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.AF.D);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&regAF);
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::BCClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%4.4X",R.BC.D&0xffff);
    sprintf(str,"%4.4X",regBC&0xffff);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.BC.D);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&regBC);
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::DEClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%4.4X",R.DE.D&0xffff);
    sprintf(str,"%4.4X",regDE&0xffff);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.DE.D);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&regDE);
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::HLClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%4.4X",R.HL.D&0xffff);
    sprintf(str,"%4.4X",regHL&0xffff);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.HL.D);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&regHL);
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::SPClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%4.4X",R.SP.D&0xffff);
    sprintf(str,"%4.4X",regSP&0xffff);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.SP.D);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&regSP);
        Update();
    }
    #endif
}

void __fastcall TDebugWnd::PCClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%4.4X",R.PC.D&0xffff);
    sprintf(str,"%4.4X",regPC);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.PC.D);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&regPC);
        Update();
    }
    #endif
}

/*void __fastcall TDebugWnd::Viewlog1Click(TObject *Sender)
{
    PauseStart();
    LogDlg->ShowModal();
    Enablelogging1->Checked=debugLogEnable;
    if ((debugLogEnable&&debugLogCount)||debugDataBreakCount)
        UseDebugReadFuncs();
    else
        RestoreReadFuncs();
    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Editloggedaddresses1Click(TObject *Sender)
{
    PauseStart();
    EditLogDlg->UpdateList();
    EditLogDlg->ShowModal();
    Enablelogging1->Checked=debugLogEnable;
    if ((debugLogEnable&&debugLogCount)||debugDataBreakCount)
        UseDebugReadFuncs();
    else
        RestoreReadFuncs();
    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Enablelogging1Click(TObject *Sender)
{
    PauseStart();
    debugLogEnable=!debugLogEnable;
    Enablelogging1->Checked=debugLogEnable;
    if ((debugLogEnable&&debugLogCount)||debugDataBreakCount)
        UseDebugReadFuncs();
    else
        RestoreReadFuncs();
    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Clearall1Click(TObject *Sender)
{
    PauseStart();
    debugLogEnable=0;
    Enablelogging1->Checked=false;
    debugLogCount=0;
    if ((debugLogEnable&&debugLogCount)||debugDataBreakCount)
        UseDebugReadFuncs();
    else
        RestoreReadFuncs();
    PauseEnd();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Loglinkportsend1Click(TObject *Sender)
{
    Loglinkportsend1->Checked=!Loglinkportsend1->Checked;
    debugLogLinkSend=Loglinkportsend1->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Loglinkportreceive1Click(TObject *Sender)
{
    Loglinkportreceive1->Checked=!Loglinkportreceive1->Checked;
    debugLogLinkRecv=Loglinkportreceive1->Checked;
}*/
//---------------------------------------------------------------------------

int CheckAddr(int addr,int size,int type,int v)
{
    int i;
    for (i=0;i<debugDataBreakCount;i++)
    {
        if (!(debugDataBreakType[i]&type)) continue;
        if ((addr>(debugDataBreakLow[i]-size))&&
            (addr<=debugDataBreakHigh[i]))
            return 1;
    }
/*    int pc=R.PC.D;
    for (i=0;i<debugLogCount;i++)
    {
        if (debugLogClass[i]==LOG_VALUE)
        {
            if (!(debugLogType[i]&type)) continue;
            if ((addr>(debugLogLow[i]-size))&&
                (addr<=debugLogHigh[i]))
            {
                char str[64];
                if (type==DATABREAK_READ)
                {
                    if (size==1)
                        sprintf(str,"Read byte %2.2X from %X at PC=%X",getmem(addr)&0xff,addr,pc&0xffffff);
                    else if (size==2)
                        sprintf(str,"Read word %4.4X from %X at PC=%X",getmem_word(addr)&0xffff,addr,pc&0xffffff);
                    else
                        sprintf(str,"Read long %8.8X from %X at PC=%X",getmem_dword(addr),addr,pc&0xffffff);
                }
                else
                {
                    if (size==1)
                        sprintf(str,"Write byte %2.2X to %X at PC=%X",v&0xff,addr,pc&0xffffff);
                    else if (size==2)
                        sprintf(str,"Write word %4.4X to %X at PC=%X",v&0xffff,addr,pc&0xffffff);
                    else
                        sprintf(str,"Write long %8.8X to %X at PC=%X",v,addr,pc&0xffffff);
                }
                LogDlg->List->Items->Add(str);
            }
        }
        else
        {
            if (type==DATABREAK_READ) continue;
            if (!((addr>(debugLogLow[i]-size))&&
                  (addr<=debugLogLow[i])))
                continue;
            int j,b,nv;
            char str[64];
            nv=v&0xff;
            if (size==2)
            {
                if ((debugLogLow[i]-addr)==0)
                    nv=(v>>8)&0xff;
                else
                    nv=v&0xff;
            }
            else if (size==4)
            {
                if ((debugLogLow[i]-addr)==0)
                    nv=(v>>24)&0xff;
                else if ((debugLogLow[i]-addr)==1)
                    nv=(v>>16)&0xff;
                else if ((debugLogLow[i]-addr)==2)
                    nv=(v>>8)&0xff;
                else if ((debugLogLow[i]-addr)==3)
                    nv=v&0xff;
            }
            for (j=0,b=1;j<8;j++,b<<=1)
            {
                if (!debugLogCheck[i]&b) continue;
                if (debugLogAny[i]&b)
                {
                    if ((debugLogLast[i]&b)!=(nv&b))
                    {
                        if ((z80)&&((debugLogLow[i]>>20)==6))
                        {
                            if (nv&b)
                                sprintf(str,"Set bit %d of I/O port %X at PC=%X",j,debugLogLow[i]&0xff,pc&0xffffff);
                            else
                                sprintf(str,"Clear bit %d of I/O port %X at PC=%X",j,debugLogLow[i]&0xff,pc&0xffffff);
                        }
                        else
                        {
                            if (nv&b)
                                sprintf(str,"Set bit %d of %X at PC=%X",j,debugLogLow[i],pc&0xffffff);
                            else
                                sprintf(str,"Clear bit %d of %X at PC=%X",j,debugLogLow[i],pc&0xffffff);
                        }
                        LogDlg->List->Items->Add(str);
                    }
                }
                else
                {
                    if ((debugLogLast[i]&b)==(nv&b)) continue;
                    if ((debugLogState[i]&b)==(nv&b))
                    {
                        if ((z80)&&((debugLogLow[i]>>20)==6))
                        {
                            if (nv&b)
                                sprintf(str,"Set bit %d of I/O port %X at PC=%X",j,debugLogLow[i]&0xff,pc&0xffffff);
                            else
                                sprintf(str,"Clear bit %d of I/O port %X at PC=%X",j,debugLogLow[i]&0xff,pc&0xffffff);
                        }
                        else
                        {
                            if (nv&b)
                                sprintf(str,"Set bit %d of %X at PC=%X",j,debugLogLow[i],pc&0xffffff);
                            else
                                sprintf(str,"Clear bit %d of %X at PC=%X",j,debugLogLow[i],pc&0xffffff);
                        }
                        LogDlg->List->Items->Add(str);
                    }
                }
            }
            debugLogLast[i]=v;
        }
    }*/
    return 0;
}

void DataBreak()
{
    debugDataBreak=1;
}

void UpdateDebugCheckEnable()
{
    if (debugCodeBreakCount||debugDataBreakCount||
        debugProgBreak||debugStepOver||debugLogEnable||
        exceptBPCount||debugRunToCursor||DebugWnd->Visible||
        enableDebugWarnings)
        debugCheckEnable=1;
    else
        debugCheckEnable=0;
/*    if (debugDataBreakCount||debugLogEnable||enableDebugWarnings)
    {
        _readmem=debug_readmem;
        _readmem_word=debug_readmem_word;
        _readmem_dword=debug_readmem_dword;
        _writemem=debug_writemem;
        _writemem_word=debug_writemem_word;
        _writemem_dword=debug_writemem_dword;
    }
    else
    {
        _readmem=asm_readmem;
        _readmem_word=asm_readmem_word;
        _readmem_dword=asm_readmem_dword;
        _writemem=asm_writemem;
        _writemem_word=asm_writemem_word;
        _writemem_dword=asm_writemem_dword;
    }*/
}

void __fastcall TDebugWnd::FormCreate(TObject *Sender)
{
    Code->ItemHeight=MulDiv(10,GetDeviceCaps(Code->Canvas->
        Handle, LOGPIXELSY),96)+4;
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Runtocursor1Click(TObject *Sender)
{
    if (!romLoaded) return;
    if (Code->ItemIndex==-1) return;
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    int pc,i,j;
//    PauseStart();
    debugRunToCursor=1;
    sscanf(Code->Items->Strings[Code->ItemIndex].c_str(),
        "%x",&pc);
    debugCursorAddr=pc;
    debugStepOver=0;
    run=1;
    Hide();
    OneInstruction();
//    PauseEnd();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Palettes1Click(TObject *Sender)
{
    PalView->DoUpdate();
    PalView->Show();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::IMEClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
//    sprintf(str,"%X",R.IFF1&1);
    sprintf(str,"%X",imeFlag);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
//        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&R.IFF1);
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&imeFlag);
//        R.IFF2=R.IFF1;
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::ROMPgClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
    sprintf(str,"%3.3X",romPage);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&romPage);
        while ((romPage<<14)>=romSize)
        {
            if (debugWarn[DBW_PAGERANGE])
                DebugWarn(DBW_PAGERANGE);
            romPage-=romSize>>14;
        }
        mem[4]=&rom[romPage<<14];
        mem[5]=&rom[(romPage<<14)+0x1000];
        mem[6]=&rom[(romPage<<14)+0x2000];
        mem[7]=&rom[(romPage<<14)+0x3000];
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::RAMPgClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
    sprintf(str,"%X",ramPage);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
        if (ram)
        {
            sscanf(NewValueDlg->Address->Text.c_str(),"%x",&ramPage);
            while ((ramPage<<13)>=ramSize)
            {
                if (debugWarn[DBW_PAGERANGE])
                    DebugWarn(DBW_PAGERANGE);
                ramPage-=romSize>>13;
            }
            if (ramEnable)
            {
                mem[0xa]=&ram[ramPage<<13];
                mem[0xb]=&ram[(ramPage<<13)+0x1000];
            }
            Update();
        }
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::RAMEnableClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
    sprintf(str,"%X",ramEnable);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
        if (ram)
        {
            sscanf(NewValueDlg->Address->Text.c_str(),"%x",&ramEnable);
            if (ramEnable)
            {
                mem[0xa]=&ram[ramPage<<13];
                mem[0xb]=&ram[(ramPage<<13)+0x1000];
            }
            else
            {
                mem[0xa]=NULL;
                mem[0xb]=NULL;
            }
            Update();
        }
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::CPUClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    clkMult=3-clkMult;
    hiRam[0x4d]=(hiRam[0x4d]&1)|((clkMult==1)?0:0x80);
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::VBlankClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    hiRam[0xff]^=1;
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::LCDCClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    hiRam[0xff]^=2;
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::TimerClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    hiRam[0xff]^=4;
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::SerialClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    hiRam[0xff]^=8;
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::ButtonClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    hiRam[0xff]^=16;
    Update();
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::LYClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
    sprintf(str,"%2.2X",lcdY);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&lcdY);
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::LYCClick(TObject *Sender)
{
    #ifdef DEMO
    MessageBox(NULL,"This feature is disabled in the demo version.","VGBC",MB_OK|MB_TASKMODAL);
    return;
    #else
    char str[4];
    sprintf(str,"%2.2X",hiRam[0x45]);
    NewValueDlg->Address->Text=str;
    NewValueDlg->Address->SelectAll();
    NewValueDlg->ActiveControl=NewValueDlg->Address;
    if (NewValueDlg->ShowModal()==1)
    {
        sscanf(NewValueDlg->Address->Text.c_str(),"%x",&hiRam[0x45]);
        Update();
    }
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Tiles1Click(TObject *Sender)
{
    TileWnd->Show();
    TileWnd->DoUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TDebugWnd::Map1Click(TObject *Sender)
{
    MapWnd->Show();
    MapWnd->DoUpdate();
}
//---------------------------------------------------------------------------

