#include <vcl\vcl.h>
#include <vcl\registry.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dir.h>
#include <io.h>
#include <dos.h>
#include "rom.h"
#include "Debug.h"
#include "Z80.h"
#include "sound.h"

#define PALCNVT(x) ((((x)&31)<<10)|((x)&0x3e0)|(((x)>>10)&31))

FILE *execTrack;

//extern Z80_Regs R;
//extern unsigned short regPC,regAF,regBC,regDE,regHL,regSP;

UBYTE *ram,*rom;
UBYTE vidRam[16384];
UBYTE oam[0xa0];
UBYTE hiRam[0x100];
UBYTE intRam[0x8000];
UBYTE* mem[16];
int romLoaded=0;
int clkMult=1;
int romType,romSize,ramSize,mbc,batt,mbc1Mode;
int romPage,ramPage,ramEnable,vidPage;
int bgPal[32],objPal[32];
int bgPalGB[32],objPalGB[32];
int keys=0xff;
int timerClkCount=0;
int intReq=0;
extern int intReq2,intReq3;
int initialPC;
int lcdMode=0;
int lcdY=0;
int lcdRefresh;
int lcdUpdateFreq=0;
int lcdUpdateNeeded=0;
char *ScreenLine[288];
int scaleFact=2;
int soundEnable=0,sndQuality=2;
int cgb=0,debugWarn[DBW_COUNT],debugWarnMsg=-1;
int restrictSpeed=1;
char romFileName[256];
int previouspc,dmaClk=0,dmaType,gdmaClk=0;
extern int debug,run,debugStartPC;
#ifndef C_CORE
extern short minTime;
#endif
int sndRegChange=1;
int displayEnabled=1;
extern int emuMode;

unsigned char (*getmemfunc)(unsigned short addr)=getmem;
void (*putmemfunc)(unsigned short addr,unsigned char v)=putmem;

SkinDesc skin[64];
int skinCount=0;
int currentSkin=-1;
Graphics::TBitmap *skinImageLg=NULL;
Graphics::TBitmap *skinImageSm=NULL;
RECT skinLcd;
RECT skinKey[8];

int irvC[2]={0,255};
int igvC[2]={0,255};
int ibvC[2]={0,255};

int grayVal[4]={(31<<10)+(31<<5)+31,(21<<10)+(21<<5)+21,(10<<10)+(10<<5)+10,0};

unsigned short Filter[32768];

extern TCanvas *LCDCanvas;
extern Graphics::TBitmap *bmp;

int GetValue(int min,int max,int v)
{
    return min+(float)(max-min)*(2.0*(v/31.0)-(v/31.0)*(v/31.0));
}

void GenFilter()
{
    for (int r=0;r<32;r++)
    {
        for (int g=0;g<32;g++)
        {
            for (int b=0;b<32;b++)
            {
                int nr=GetValue(GetValue(4,14,g),GetValue(24,29,g),r)-4;
                int ng=GetValue(GetValue(4+GetValue(0,5,r),14+GetValue(0,3,r),b),
                    GetValue(24+GetValue(0,3,r),29+GetValue(0,1,r),b),g)-4;
                int nb=GetValue(GetValue(4+GetValue(0,5,r),14+GetValue(0,3,r),g),
                    GetValue(24+GetValue(0,3,r),29+GetValue(0,1,r),g),b)-4;
                Filter[(b<<10)|(g<<5)|r]=(nr<<10)|(ng<<5)|nb;
            }
        }
    }
}

void InitEmu(char *name)
{
    strcpy(romFileName,name);
    FILE *fp=fopen(name,"rb");
    fseek(fp,0x147,SEEK_SET);
    romType=fgetc(fp);
    switch(fgetc(fp))
    {
        case 0: romSize=32*1024; break;
        case 1: romSize=64*1024; break;
        case 2: romSize=128*1024; break;
        case 3: romSize=256*1024; break;
        case 4: romSize=512*1024; break;
        case 5: romSize=1024*1024; break;
        case 6: romSize=2048*1024; break;
        case 7: romSize=4096*1024; break;
        case 8: romSize=8192*1024; break;
        case 0x52: romSize=(1024+128)*1024; break;
        case 0x53: romSize=(1024+256)*1024; break;
        case 0x54: romSize=(1024+512)*1024; break;
        default:
            MessageBox(NULL,"Unsupported ROM size",
                "Error",MB_OK|MB_ICONSTOP);
            return;
    }
    fseek(fp,0,SEEK_SET);
    rom=new UBYTE[romSize];
    fread(rom,1,romSize,fp);
    fclose(fp);
    for (int i=0;i<0x8000;i++)
        intRam[i]=rand()&0xff;
    switch (rom[0x149])
    {
        case 0: ramSize=0; break;
        case 1: ramSize=2*1024; break;
        case 2: ramSize=8*1024; break;
        case 3: ramSize=32*1024; break;
        case 4: ramSize=128*1024; break;
        case 5: ramSize=256*1024; break;
        case 6: ramSize=512*1024; break;
        case 7: ramSize=1024*1024; break;
        default:
            MessageBox(NULL,"Unsupported RAM size",
                "Error",MB_OK|MB_ICONSTOP);
            return;
    }
    if (ramSize)
    {
        ram=new UBYTE[ramSize];
        for (int i=0;i<ramSize;i++)
            ram[i]=rand()&0xff;
    }
    else
        ram=NULL;
    switch (romType)
    {
        case 0: mbc=0; batt=0; break;
        case 1: mbc=1; batt=0; break;
        case 2: mbc=1; batt=0; break;
        case 3: mbc=1; batt=1; break;
        case 5: mbc=2; batt=0; break;
        case 6: mbc=2; batt=1; break;
        case 8: mbc=0; batt=0; break;
        case 9: mbc=0; batt=1; break;
        case 0xb: case 0xc: case 0xd:
            MessageBox(NULL,"MMM01 cartridges are not supported",
                "Error",MB_OK|MB_ICONSTOP);
            return;
        case 0xf: mbc=3; batt=1; break;
        case 0x10: mbc=3; batt=1; break;
        case 0x11: mbc=3; batt=0; break;
        case 0x12: mbc=3; batt=0; break;
        case 0x13: mbc=3; batt=1; break;
        case 0x19: mbc=5; batt=0; break;
        case 0x1a: mbc=5; batt=0; break;
        case 0x1b: mbc=5; batt=1; break;
        case 0x1c: mbc=6; batt=0; break;
        case 0x1d: mbc=6; batt=0; break;
        case 0x1e: mbc=6; batt=1; break;
        case 0x1f:
            MessageBox(NULL,"Pocket Camera is not supported",
                "Error",MB_OK|MB_ICONSTOP);
            return;
        case 0xfd:
            MessageBox(NULL,"Bandai TAMA5 cartridges are not supported",
                "Error",MB_OK|MB_ICONSTOP);
            return;
        case 0xfe: case 0xff:
            MessageBox(NULL,"Hudson HuC cartridges are not supported",
                "Error",MB_OK|MB_ICONSTOP);
            return;
        default:
            MessageBox(NULL,"Unsupported cartridge type",
                "Error",MB_OK|MB_ICONSTOP);
            return;
    }
    romLoaded=1;
    Reset();
    if (batt)
    {
        char str[256],drive[3],dir[256],name[256];
        fnsplit(romFileName,drive,dir,name,NULL);
        fnmerge(str,drive,dir,name,".sav");
        FILE *fp=fopen(str,"rb");
        if (fp)
        {
            fread(ram,1,ramSize,fp);
            fclose(fp);
        }
    }
}

void CloseEmu()
{
    if (!romLoaded) return;
    if (batt)
    {
        char str[256],drive[3],dir[256],name[256];
        fnsplit(romFileName,drive,dir,name,NULL);
        fnmerge(str,drive,dir,name,".sav");
        FILE *fp=fopen(str,"wb");
        if (fp)
        {
            fwrite(ram,1,ramSize,fp);
            fclose(fp);
        }
    }
    romLoaded=0;
    delete[] rom; rom=NULL;
    if (ram) { delete[] ram; ram=NULL; }
}

void SaveState(char *name)
{
}

void LoadState(char *name)
{
}

Graphics::TBitmap* GetLCD_BW()
{
/*	int x,y,i,j,b,v0,v1,v2,v3,v4,v5,v,ofs,lofs;
	unsigned long *sptr,*sptr1,*sptr2;
    unsigned long rgbv[7];
    unsigned long* ScreenLine[256];
    int cl,w,h;
    Graphics::TBitmap *bmp;

    for (x=0;x<7;x++)
    {
        y=(255*x)/6;
        rgbv[x]=(y<<16)|(y<<8)|y;
    }
    {
        bmp=new Graphics::TBitmap;
        bmp->Width=lcdWidth;
        bmp->Height=lcdHeight;
        bmp->PixelFormat=pf32bit;
        for (int i=0;i<lcdHeight;i++)
            ScreenLine[i]=(unsigned long *)bmp->ScanLine[i];
    	for (y=0,lofs=0;y<lcdHeight;y++,lofs+=lcdLineBytes)
    	{
	    	ofs=lofs;
    		for (x=0,sptr=(unsigned long*)ScreenLine[y];x<lcdWidth;x+=8,ofs++)
    		{
	    		v0=oldScreen[0][ofs];
		    	v1=oldScreen[1][ofs];
    			v2=oldScreen[2][ofs];
       			v3=oldScreen[3][ofs];
   	    		v4=oldScreen[4][ofs];
   		    	v5=oldScreen[5][ofs];
       			for (i=7,b=0x80;i>=0;i--,b>>=1)
       			{
                    j=6-(((v0&b)+(v1&b)+(v2&b)+(v3&b)+(v4&b)+(v5&b))>>i);
                    *(sptr++)=rgbv[j];
   	    		}
   		    }
        }
  	}
    return bmp;*/
    return NULL;
}

Graphics::TBitmap* GetLCD_True()
{
/*	int x,y,i,j,b,v0,v1,v2,v3,v4,v5,v,ofs,lofs;
	unsigned long *sptr,*sptr1,*sptr2;
    int rv[7],gv[7],bv[7];
    unsigned long rgbv[7];
    unsigned long* ScreenLine[256];
    int cl,w,h;
    Graphics::TBitmap *bmp;

    irv[0]=irvC[0]; igv[0]=igvC[0]; ibv[0]=ibvC[0];
    irv[6]=irvC[1]; igv[6]=igvC[1]; ibv[6]=ibvC[1];
    if (calc==89)
        cl=contrast;
    else if (calc>=92)
        cl=31-contrast;
    else if ((calc==82)||(calc==85))
        cl=contrast+((calc==85)?4:0);
    else if (calc==83)
        cl=((contrast-12)*32)/52;
    else
        cl=contrast;
    if (cpuCompleteStop) cl+=6;
    if (romErase)
    {
        if (calc!=89)
            cl+=contrastDelta[romErasePhase>>3];
        romErasePhase++;
        if (romErasePhase==64)
        {
            romErasePhase=0;
            romErase=0;
        }
    }
    if (cl>31) cl=31;
    if (cl<0) cl=0;
    if (cl>16)
    {
        rv[0]=irv[0]; rv[6]=irv[0]+((irv[6]-irv[0])*(32-cl))/16;
        gv[0]=igv[0]; gv[6]=igv[0]+((igv[6]-igv[0])*(32-cl))/16;
        bv[0]=ibv[0]; bv[6]=ibv[0]+((ibv[6]-ibv[0])*(32-cl))/16;
        rv[1]=rv[0]+(rv[6]-rv[0])/6;
        rv[2]=rv[0]+((rv[6]-rv[0])*2)/6;
        rv[3]=rv[0]+((rv[6]-rv[0])*3)/6;
        rv[4]=rv[0]+((rv[6]-rv[0])*4)/6;
        rv[5]=rv[0]+((rv[6]-rv[0])*5)/6;
        gv[1]=gv[0]+(gv[6]-gv[0])/6;
        gv[2]=gv[0]+((gv[6]-gv[0])*2)/6;
        gv[3]=gv[0]+((gv[6]-gv[0])*3)/6;
        gv[4]=gv[0]+((gv[6]-gv[0])*4)/6;
        gv[5]=gv[0]+((gv[6]-gv[0])*5)/6;
        bv[1]=bv[0]+(bv[6]-bv[0])/6;
        bv[2]=bv[0]+((bv[6]-bv[0])*2)/6;
        bv[3]=bv[0]+((bv[6]-bv[0])*3)/6;
        bv[4]=bv[0]+((bv[6]-bv[0])*4)/6;
        bv[5]=bv[0]+((bv[6]-bv[0])*5)/6;
    }
    else
    {
        rv[6]=irv[6]; rv[0]=irv[6]-((irv[6]-irv[0])*cl)/16;
        gv[6]=igv[6]; gv[0]=igv[6]-((igv[6]-igv[0])*cl)/16;
        bv[6]=ibv[6]; bv[0]=ibv[6]-((ibv[6]-ibv[0])*cl)/16;
        rv[1]=rv[0]+(rv[6]-rv[0])/6;
        rv[2]=rv[0]+((rv[6]-rv[0])*2)/6;
        rv[3]=rv[0]+((rv[6]-rv[0])*3)/6;
        rv[4]=rv[0]+((rv[6]-rv[0])*4)/6;
        rv[5]=rv[0]+((rv[6]-rv[0])*5)/6;
        gv[1]=gv[0]+(gv[6]-gv[0])/6;
        gv[2]=gv[0]+((gv[6]-gv[0])*2)/6;
        gv[3]=gv[0]+((gv[6]-gv[0])*3)/6;
        gv[4]=gv[0]+((gv[6]-gv[0])*4)/6;
        gv[5]=gv[0]+((gv[6]-gv[0])*5)/6;
        bv[1]=bv[0]+(bv[6]-bv[0])/6;
        bv[2]=bv[0]+((bv[6]-bv[0])*2)/6;
        bv[3]=bv[0]+((bv[6]-bv[0])*3)/6;
        bv[4]=bv[0]+((bv[6]-bv[0])*4)/6;
        bv[5]=bv[0]+((bv[6]-bv[0])*5)/6;
    }
    for (j=0;j<7;j++)
        rgbv[j]=bv[j]|(gv[j]<<8)|(rv[j]<<16);
    {
        bmp=new Graphics::TBitmap;
        bmp->Width=lcdWidth;
        bmp->Height=lcdHeight;
        bmp->PixelFormat=pf32bit;
        for (int i=0;i<lcdHeight;i++)
            ScreenLine[i]=(unsigned long *)bmp->ScanLine[i];
    	for (y=0,lofs=0;y<lcdHeight;y++,lofs+=lcdLineBytes)
    	{
	    	ofs=lofs;
    		for (x=0,sptr=(unsigned long*)ScreenLine[y];x<lcdWidth;x+=8,ofs++)
    		{
	    		v0=oldScreen[0][ofs];
		    	v1=oldScreen[1][ofs];
    			v2=oldScreen[2][ofs];
       			v3=oldScreen[3][ofs];
   	    		v4=oldScreen[4][ofs];
   		    	v5=oldScreen[5][ofs];
       			for (i=7,b=0x80;i>=0;i--,b>>=1)
       			{
                    j=6-(((v0&b)+(v1&b)+(v2&b)+(v3&b)+(v4&b)+(v5&b))>>i);
                    *(sptr++)=rgbv[j];
   	    		}
   		    }
        }
  	}
    return bmp;*/
    return NULL;
}

Graphics::TBitmap* GetLCD_Calc(Graphics::TBitmap *calcImage)
{
/*	int x,y,i,j,b,v0,v1,v2,v3,v4,v5,v,ofs,lofs;
	unsigned long *sptr,*sptr1,*sptr2;
    int rv[7],gv[7],bv[7];
    unsigned long rgbv[7];
    unsigned long* ScreenLine[256];
    int cl,w,h;
    Graphics::TBitmap *bmp,*lcd;

    irv[0]=irvC[0]; igv[0]=igvC[0]; ibv[0]=ibvC[0];
    irv[6]=irvC[1]; igv[6]=igvC[1]; ibv[6]=ibvC[1];
    if (calc==89)
        cl=contrast;
    else if (calc>=92)
        cl=31-contrast;
    else if ((calc==82)||(calc==85))
        cl=contrast+((calc==85)?4:0);
    else if (calc==83)
        cl=((contrast-12)*32)/52;
    else
        cl=contrast;
    if (cpuCompleteStop) cl+=6;
    if (romErase)
    {
        if (calc!=89)
            cl+=contrastDelta[romErasePhase>>3];
        romErasePhase++;
        if (romErasePhase==64)
        {
            romErasePhase=0;
            romErase=0;
        }
    }
    if (cl>31) cl=31;
    if (cl<0) cl=0;
    if (cl>16)
    {
        rv[0]=irv[0]; rv[6]=irv[0]+((irv[6]-irv[0])*(32-cl))/16;
        gv[0]=igv[0]; gv[6]=igv[0]+((igv[6]-igv[0])*(32-cl))/16;
        bv[0]=ibv[0]; bv[6]=ibv[0]+((ibv[6]-ibv[0])*(32-cl))/16;
        rv[1]=rv[0]+(rv[6]-rv[0])/6;
        rv[2]=rv[0]+((rv[6]-rv[0])*2)/6;
        rv[3]=rv[0]+((rv[6]-rv[0])*3)/6;
        rv[4]=rv[0]+((rv[6]-rv[0])*4)/6;
        rv[5]=rv[0]+((rv[6]-rv[0])*5)/6;
        gv[1]=gv[0]+(gv[6]-gv[0])/6;
        gv[2]=gv[0]+((gv[6]-gv[0])*2)/6;
        gv[3]=gv[0]+((gv[6]-gv[0])*3)/6;
        gv[4]=gv[0]+((gv[6]-gv[0])*4)/6;
        gv[5]=gv[0]+((gv[6]-gv[0])*5)/6;
        bv[1]=bv[0]+(bv[6]-bv[0])/6;
        bv[2]=bv[0]+((bv[6]-bv[0])*2)/6;
        bv[3]=bv[0]+((bv[6]-bv[0])*3)/6;
        bv[4]=bv[0]+((bv[6]-bv[0])*4)/6;
        bv[5]=bv[0]+((bv[6]-bv[0])*5)/6;
    }
    else
    {
        rv[6]=irv[6]; rv[0]=irv[6]-((irv[6]-irv[0])*cl)/16;
        gv[6]=igv[6]; gv[0]=igv[6]-((igv[6]-igv[0])*cl)/16;
        bv[6]=ibv[6]; bv[0]=ibv[6]-((ibv[6]-ibv[0])*cl)/16;
        rv[1]=rv[0]+(rv[6]-rv[0])/6;
        rv[2]=rv[0]+((rv[6]-rv[0])*2)/6;
        rv[3]=rv[0]+((rv[6]-rv[0])*3)/6;
        rv[4]=rv[0]+((rv[6]-rv[0])*4)/6;
        rv[5]=rv[0]+((rv[6]-rv[0])*5)/6;
        gv[1]=gv[0]+(gv[6]-gv[0])/6;
        gv[2]=gv[0]+((gv[6]-gv[0])*2)/6;
        gv[3]=gv[0]+((gv[6]-gv[0])*3)/6;
        gv[4]=gv[0]+((gv[6]-gv[0])*4)/6;
        gv[5]=gv[0]+((gv[6]-gv[0])*5)/6;
        bv[1]=bv[0]+(bv[6]-bv[0])/6;
        bv[2]=bv[0]+((bv[6]-bv[0])*2)/6;
        bv[3]=bv[0]+((bv[6]-bv[0])*3)/6;
        bv[4]=bv[0]+((bv[6]-bv[0])*4)/6;
        bv[5]=bv[0]+((bv[6]-bv[0])*5)/6;
    }
    for (j=0;j<7;j++)
        rgbv[j]=bv[j]|(gv[j]<<8)|(rv[j]<<16);
    {
        bmp=new Graphics::TBitmap;
        bmp->Width=calcImage->Width;
        bmp->Height=calcImage->Height;
        bmp->PixelFormat=pf32bit;
        lcd=new Graphics::TBitmap;
        lcd->Width=lcdWidth<<1;
        lcd->Height=lcdHeight<<1;
        lcd->PixelFormat=pf32bit;
        for (int i=0;i<(lcdHeight<<1);i++)
            ScreenLine[i]=(unsigned long *)lcd->ScanLine[i];
    	for (y=0,lofs=0;y<lcdHeight;y++,lofs+=lcdLineBytes)
    	{
  	    	ofs=lofs;
      		for (x=0,sptr1=(unsigned long*)ScreenLine[y<<1],sptr2=
                (unsigned long*)ScreenLine[(y<<1)+1];x<lcdWidth;x+=8,
                ofs++)
     		{
 	    		v0=oldScreen[0][ofs];
  		    	v1=oldScreen[1][ofs];
      			v2=oldScreen[2][ofs];
      			v3=oldScreen[3][ofs];
  	    		v4=oldScreen[4][ofs];
   		    	v5=oldScreen[5][ofs];
       			for (i=7,b=0x80;i>=0;i--,b>>=1)
       			{
                    j=6-(((v0&b)+(v1&b)+(v2&b)+(v3&b)+(v4&b)+(v5&b))>>i);
                    unsigned long v=rgbv[j];
                    *(sptr1++)=v; *(sptr1++)=v;
                    *(sptr2++)=v; *(sptr2++)=v;
   	    		}
   		    }
        }
        if (calcImage)
            bmp->Canvas->Draw(0,0,calcImage);
        bmp->Canvas->Draw(skinLcd.left,skinLcd.top,lcd);
        delete lcd;
    }
    return bmp;*/
    return NULL;
}

void AddSkin(char *name,int size)
{
/*    FILE *fp=fopen(name,"rb");
    if (!fp) return;
    char str[9];
    fread(str,8,1,fp);
    str[8]=0;
    if (strcmp(str,"VTIv2.1 "))
    {
        fclose(fp);
        return;
    }
    strcpy(skin[skinCount].file,name);
    fread(skin[skinCount].name,64,1,fp);
    fread(&skin[skinCount].calc,4,1,fp);
    fseek(fp,0,SEEK_SET);
    skin[skinCount].csum=0;
    for (int i=0;i<size;i++)
        skin[skinCount].csum+=fgetc(fp)&0xff;
    TRegistry *reg=new TRegistry;
    reg->RootKey=HKEY_LOCAL_MACHINE;
    reg->OpenKey("\\Software\\ACZ\\Virtual TI\\Skins",true);
    char keyName[96];
    sprintf(keyName,"%s",name);
    skin[skinCount].defW=0;
    skin[skinCount].defH=0;
    if (reg->OpenKey(keyName,false))
    {
        if (reg->ValueExists("Checksum"))
        {
            if (reg->ReadInteger("Checksum")==
                skin[skinCount].csum)
            {
                if ((reg->ValueExists("Width"))&&
                    (reg->ValueExists("Height")))
                {
                    skin[skinCount].defW=reg->ReadInteger("Width");
                    skin[skinCount].defH=reg->ReadInteger("Height");
                }
            }
        }
    }
    reg->CloseKey();
    delete reg;
    fclose(fp);
    skinCount++;*/
}

void AnalyzeSkins()
{
    ffblk ff;
    int done;

    skinCount=0;
/*    done=findfirst("*.skn",&ff,FA_RDONLY|FA_ARCH);
    while (!done)
    {
        AddSkin(ff.ff_name,ff.ff_fsize);
        done=findnext(&ff);
    }*/
}

void LoadSkin(int n)
{
/*    char filename[256];
    if (initPath[0])
    {
        sprintf(filename,"%s%s%s",initPath,(initPath[strlen(
            initPath)-1]=='\\')?"":"\\",skin[n].file);
    }
    else
        strcpy(filename,skin[n].file);
    FILE *fp=fopen(filename,"rb");
    if (!fp) return;
    fseek(fp,80,SEEK_SET);
    TColor white,black;
    fread(&white,4,1,fp);
    fread(&black,4,1,fp);
    irvC[0]=((int)black)&0xff; irvC[1]=((int)white)&0xff;
    igvC[0]=(((int)black)>>8)&0xff; igvC[1]=(((int)white)>>8)&0xff;
    ibvC[0]=(((int)black)>>16)&0xff; ibvC[1]=(((int)white)>>16)&0xff;
    fread(&skinLcd,sizeof(RECT),1,fp);
    fread(skinKey,sizeof(RECT),80,fp);
    Screen->Cursor=crHourGlass;
    if (!skinImageLg) skinImageLg=new Graphics::TBitmap;
    skinImageLg->PixelFormat=pf24bit;
    ReadJPEG(fp,skinImageLg);
    if (!skinImageSm) skinImageSm=new Graphics::TBitmap;
    skinImageSm->Width=skinImageLg->Width>>1;
    skinImageSm->Height=skinImageLg->Height>>1;
    skinImageSm->PixelFormat=pf24bit;
    TRect r;
    r.Left=0; r.Right=skinImageSm->Width;
    r.Top=0; r.Bottom=skinImageSm->Height;
    skinImageSm->Canvas->StretchDraw(r,skinImageLg);
    Screen->Cursor=crDefault;
    fclose(fp);
    currentSkin=n;*/
}

void CloseSkin(int saveSkin)
{
/*    if (saveSkin)
    {
        TRegistry *reg=new TRegistry;
        reg->RootKey=HKEY_LOCAL_MACHINE;
        if ((currentSkin!=-1)&&(!EmuWnd->Fullscreenview1->Checked))
        {
            reg->OpenKey("\\Software\\ACZ\\Virtual TI\\Skins",true);
            char keyName[96];
            sprintf(keyName,"%s",skin[currentSkin].file);
            reg->OpenKey(keyName,true);
            reg->WriteInteger("Checksum",(int)((unsigned long)
                skin[currentSkin].csum));
            reg->WriteInteger("Width",EmuWnd->Image->Width);
            reg->WriteInteger("Height",EmuWnd->Image->Height);
            skin[currentSkin].defW=EmuWnd->Image->Width;
            skin[currentSkin].defH=EmuWnd->Image->Height;
            reg->CloseKey();
        }
        reg->OpenKey("\\Software\\ACZ\\Virtual TI\\Skins",true);
        char keyName[24];
        switch (calc)
        {
            case 73: strcpy(keyName,"DefaultSkin73"); break;
            case 82: strcpy(keyName,"DefaultSkin82"); break;
            case 83: strcpy(keyName,"DefaultSkin83"); break;
            case 84: strcpy(keyName,"DefaultSkin83Plus"); break;
            case 85: strcpy(keyName,"DefaultSkin85"); break;
            case 86: strcpy(keyName,"DefaultSkin86"); break;
            case 89: strcpy(keyName,"DefaultSkin89"); break;
            case 92: case 93: strcpy(keyName,"DefaultSkin92"); break;
            case 94: strcpy(keyName,"DefaultSkin92Plus"); break;
        }
        if (currentSkin!=-1)
            reg->WriteString(keyName,skin[currentSkin].file);
        else
            reg->WriteString(keyName,"");
        reg->CloseKey();
        delete reg;
    }
    if (skinImageLg) { delete skinImageLg; skinImageLg=NULL; }
    if (skinImageSm) { delete skinImageSm; skinImageSm=NULL; }
    skinLcd.left=0; skinLcd.right=lcdWidth<<1;
    skinLcd.top=0; skinLcd.bottom=lcdHeight<<1;
    currentSkin=-1;
    irvC[0]=0; irvC[1]=255;
    igvC[0]=0; igvC[1]=255;
    ibvC[0]=0; ibvC[1]=255;*/
}

unsigned char getmem_direct(unsigned short addr)
{
    if (addr<0xf000)
    {
        if (!mem[addr>>12])
            return 0;
        return mem[addr>>12][addr&0xfff];
    }
    return getmem(addr);
}

unsigned char getmem(unsigned short addr)
{
    if ((dmaClk)&&(addr<0xff00))
    {
        if (addr<0x8000)
        {
            if (dmaType==0)
            {
                if (debugWarn[DBW_DMA])
                    DebugWarn(DBW_DMA);
                return 0;
            }
        }
        else if (addr<0xa000)
        {
            if (dmaType==1)
            {
                if (debugWarn[DBW_DMA])
                    DebugWarn(DBW_DMA);
                return 0;
            }
        }
        else
        {
            if (dmaType==2)
            {
                if (debugWarn[DBW_DMA])
                    DebugWarn(DBW_DMA);
                return 0;
            }
        }
    }
    if (addr<0x8000)
    {
        if (!mem[addr>>12])
        {
            if (debugWarn[DBW_INVALIDMEM])
                DebugWarn(DBW_INVALIDMEM);
            return 0;
        }
        return mem[addr>>12][addr&0xfff];
    }
    else if (addr<0xa000)
    {
        if ((lcdMode==3)&&(hiRam[0x40]&0x80))
        {
            if (debugWarn[DBW_VIDMEM])
                DebugWarn(DBW_VIDMEM);
            return 0;
        }
        return vidRam[(vidPage<<13)+(addr&0x1fff)];
    }
    else if (addr<0xf000)
    {
        if (!mem[addr>>12])
        {
            if ((addr<0xc000)&&(!ramEnable)&&
                (debugWarn[DBW_RAMDISABLED]))
                DebugWarn(DBW_RAMDISABLED);
            else if (debugWarn[DBW_INVALIDMEM])
                DebugWarn(DBW_INVALIDMEM);
            return 0;
        }
        return mem[addr>>12][addr&0xfff];
    }
    else if (addr<0xfe00)
        return intRam[(hiRam[0x70]<<12)+(addr&0xfff)];
    if (addr<0xfea0)
    {
        if ((lcdMode>=2)&&(hiRam[0x40]&2)&&(hiRam[0x40]&0x80))
        {
            if (debugWarn[DBW_VIDMEM])
                DebugWarn(DBW_VIDMEM);
            return 0;
        }
        return oam[addr&0xff];
    }
    else if (addr<0xff00)
        return 0;
    else if (addr==0xff00)
    {
        hiRam[0]=(hiRam[0]&0xf0)|(((hiRam[0]&0x10)?15:(keys&0xf))&
            ((hiRam[0]&0x20)?15:((keys>>4)&0xf)));
        return hiRam[0];
    }
    else if (addr==0xff0f)
    {
        hiRam[0xf]=intReq;
        return hiRam[0xf];
    }
    else if (addr==0xff26)
    {
        hiRam[0x26]=(hiRam[0x26]&0xf0)|(snd4Enable<<3)|
            (snd3Enable<<2)|(snd2Enable<<1)|snd1Enable;
        return hiRam[0x26];
    }
    else if ((addr>=0xff30)&&(addr<0xff40))
    {
//        if ((!(hiRam[0x26]&0x80))||(!(hiRam[0x1a]&0x80)))
            return hiRam[addr&0xff];
//        return rand()&0xff;
    }
    else if (addr==0xff41)
    {
        hiRam[0x41]=(hiRam[0x41]&0xf8)|((lcdY==
            hiRam[0x45])?4:0)|lcdMode;
//        hiRam[0x41]=(hiRam[0x41]&0xf8)|(hiRam[0x41]&4)|lcdMode;
        int v=hiRam[0x41];
//        if (lcdY!=hiRam[0x45])
//            hiRam[0x41]&=~4;
        return v;
    }
    else if (addr==0xff44)
    {
        hiRam[0x44]=lcdY;
        return hiRam[0x44];
    }
    else if (addr==0xff69)
    {
        unsigned char v;
        if (hiRam[0x68]&1)
            v=(bgPalGB[(hiRam[0x68]>>1)&31]>>8)&0xff;
        else
            v=bgPalGB[(hiRam[0x68]>>1)&31]&0xff;
        if (hiRam[0x68]&0x80)
            hiRam[0x68]=((hiRam[0x68]+1)&63)|0x80;
        return v;
    }
    else if (addr==0xff6b)
    {
        unsigned char v;
        if (hiRam[0x6a]&1)
            v=(objPalGB[(hiRam[0x6a]>>1)&31]>>8)&0xff;
        else
            v=objPalGB[(hiRam[0x6a]>>1)&31]&0xff;
        if (hiRam[0x6a]&0x80)
            hiRam[0x6a]=((hiRam[0x6a]+1)&63)|0x80;
        return v;
    }
    else
        return hiRam[addr&0xff];
}

unsigned short getmem_word(unsigned short addr)
{
    return getmem(addr)+(getmem(addr+1)<<8);
}

unsigned long getmem_dword(unsigned short addr)
{
    return getmem(addr)+(getmem(addr+1)<<8)+
        (getmem(addr+2)<<16)+(getmem(addr+3)<<24);
}

void putmem_direct(unsigned short addr,unsigned char v)
{
    if (addr<0x8000)
        putmem(addr,v);
    else if (addr<0xf000)
    {
        if (!mem[addr>>12])
            return;
        mem[addr>>12][addr&0xfff]=v;
    }
    else if (addr<0xfe00)
        intRam[(hiRam[0x70]<<12)+(addr&0xfff)]=v;
    else if (addr<0xfea0)
        oam[addr&0xff]=v;
    else if (addr<0xff00)
        return;
    else if ((addr>=0xff00)&&(addr<0xff80))
        putmem(addr,v);
    else
        hiRam[addr&0xff]=v;
}

void putmem(unsigned short addr,unsigned char v)
{
    if ((dmaClk)&&(addr<0xff00))
    {
        if (addr<0x8000)
        {
            if (dmaType==0)
            {
                if (debugWarn[DBW_DMA])
                    DebugWarn(DBW_DMA);
                return;
            }
        }
        else if (addr<0xa000)
        {
            if (dmaType==1)
            {
                if (debugWarn[DBW_DMA])
                    DebugWarn(DBW_DMA);
                return;
            }
        }
        else
        {
            if (dmaType==2)
            {
                if (debugWarn[DBW_DMA])
                    DebugWarn(DBW_DMA);
                return;
            }
        }
    }
    if (addr<0x8000)
    {
        switch(mbc)
        {
            case 0: return;
            case 1:
                if (addr<=0x1fff)
                {
                    if (((v&0xf)==0xa)&&(ram))
                    {
                        ramEnable=1;
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                    else
                    {
                        ramEnable=0;
                        mem[0xa]=NULL; mem[0xb]=NULL;
                    }
                }
                else if ((addr>=0x2000)&&(addr<=0x3fff))
                {
                    romPage=(mbc1Mode)?(v&0x1f):(v&0x7f);
                    if (romPage==0) romPage=1;
                    while ((romPage<<14)>=romSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        romPage-=romSize>>14;
                    }
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                else if ((addr>=0x4000)&&(addr<=0x5fff))
                {
                    if (mbc1Mode==0)
                    {
                        romPage=(romPage&0x1f)|((v&3)<<5);
                        if (romPage==0) romPage=1;
                        while ((romPage<<14)>=romSize)
                        {
                            if (debugWarn[DBW_PAGERANGE])
                                DebugWarn(DBW_PAGERANGE);
                            romPage-=romSize>>14;
                        }
                        mem[4]=&rom[romPage<<14];
                        mem[5]=&rom[(romPage<<14)+0x1000];
                        mem[6]=&rom[(romPage<<14)+0x2000];
                        mem[7]=&rom[(romPage<<14)+0x3000];
                    }
                    else if (ramSize)
                    {
                        ramPage=v&3;
                        while ((ramPage<<13)>=ramSize)
                        {
                            if (debugWarn[DBW_PAGERANGE])
                                DebugWarn(DBW_PAGERANGE);
                            ramPage-=ramSize>>13;
                        }
                        if (ramEnable)
                        {
                            mem[0xa]=&ram[ramPage<<13];
                            mem[0xb]=&ram[(ramPage<<13)+0x1000];
                        }
                    }
                }
                else if ((addr>=0x6000)&&(addr<=0x7fff))
                {
                    mbc1Mode=v&1;
                    if (mbc1Mode==1) romPage&=0x1f;
                }
                break;
            case 2:
                if ((addr<=0x1fff)/*&&(!(addr&0x100))*/)
                {
                    if (((v&0xf)==0xa)&&(ram))
                    {
                        ramEnable=1;
                        mem[0xa]=ram;
                        mem[0xb]=&ram[0x1000];
                    }
                    else
                    {
                        ramEnable=0;
                        mem[0xa]=NULL; mem[0xb]=NULL;
                    }
                }
                else if ((addr>=0x2000)&&(addr<=0x3fff)/*&&(addr&0x100)*/)
                {
                    romPage=v&0xf;
                    if (romPage==0) romPage=1;
                    while ((romPage<<14)>=romSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        romPage-=romSize>>14;
                    }
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                break;
            case 3:
                if (addr<=0x1fff)
                {
                    if (((v&0xf)==0xa)&&(ram))
                    {
                        ramEnable=1;
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                    else
                    {
                        ramEnable=0;
                        mem[0xa]=NULL; mem[0xb]=NULL;
                    }
                }
                else if ((addr>=0x2000)&&(addr<=0x3fff))
                {
                    romPage=v&0x7f;
                    if (romPage==0) romPage=1;
                    while ((romPage<<14)>=romSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        romPage-=romSize>>14;
                    }
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                else if ((addr>=0x4000)&&(addr<=0x5fff)&&(ramSize))
                {
                    ramPage=v&3;
                    while ((ramPage<<13)>=ramSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        ramPage-=ramSize>>13;
                    }
                    if (ramEnable)
                    {
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                }
                break;
            case 5:
                if (addr<=0x1fff)
                {
                    if (((v&0xf)==0xa)&&(ram))
                    {
                        ramEnable=1;
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                    else
                    {
                        ramEnable=0;
                        mem[0xa]=NULL; mem[0xb]=NULL;
                    }
                }
                else if ((addr>=0x2000)&&(addr<=0x2fff))
                {
                    romPage=(romPage&0x100)|(int)(v&0xff);
                    while ((romPage<<14)>=romSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        romPage-=romSize>>14;
                    }
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                else if ((addr>=0x3000)&&(addr<=0x3fff))
                {
                    romPage=(romPage&0xff)|(((int)(v&1))<<8);
                    while ((romPage<<14)>=romSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        romPage-=romSize>>14;
                    }
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                else if ((addr>=0x4000)&&(addr<=0x5fff)&&(ramSize))
                {
                    ramPage=v&0xf;
                    while ((ramPage<<13)>=ramSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        ramPage-=ramSize>>13;
                    }
                    if (ramEnable)
                    {
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                }
                break;
            case 6:
                if (addr<=0x1fff)
                {
                    if (((v&0xf)==0xa)&&(ram))
                    {
                        ramEnable=1;
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                    else
                    {
                        ramEnable=0;
                        mem[0xa]=NULL; mem[0xb]=NULL;
                    }
                }
                else if ((addr>=0x2000)&&(addr<=0x2fff))
                {
                    romPage=(romPage&0x100)|(int)(v&0xff);
                    while ((romPage<<14)>=romSize)
                        romPage-=romSize>>14;
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                else if ((addr>=0x3000)&&(addr<=0x3fff))
                {
                    romPage=(romPage&0xff)|(((int)(v&1))<<8);
                    while ((romPage<<14)>=romSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        romPage-=romSize>>14;
                    }
                    mem[4]=&rom[romPage<<14];
                    mem[5]=&rom[(romPage<<14)+0x1000];
                    mem[6]=&rom[(romPage<<14)+0x2000];
                    mem[7]=&rom[(romPage<<14)+0x3000];
                }
                else if ((addr>=0x4000)&&(addr<=0x5fff)&&(ramSize))
                {
                    ramPage=v&0x7;
                    while ((ramPage<<13)>=ramSize)
                    {
                        if (debugWarn[DBW_PAGERANGE])
                            DebugWarn(DBW_PAGERANGE);
                        ramPage-=ramSize>>13;
                    }
                    if (ramEnable)
                    {
                        mem[0xa]=&ram[ramPage<<13];
                        mem[0xb]=&ram[(ramPage<<13)+0x1000];
                    }
                }
                break;
        }
    }
    else if (addr<0xa000)
    {
        if ((lcdMode==3)&&(hiRam[0x40]&0x80))
        {
            if (debugWarn[DBW_VIDMEM])
                DebugWarn(DBW_VIDMEM);
            return;
        }
        vidRam[(vidPage<<13)+(addr&0x1fff)]=v;
    }
    else if (addr<0xf000)
    {
        if (!mem[addr>>12])
        {
            if ((addr<0xc000)&&(!ramEnable)&&
                (debugWarn[DBW_RAMDISABLED]))
                DebugWarn(DBW_RAMDISABLED);
            else if (debugWarn[DBW_INVALIDMEM])
                DebugWarn(DBW_INVALIDMEM);
            return;
        }
        mem[addr>>12][addr&0xfff]=v;
    }
    else if (addr<0xfe00)
        intRam[(hiRam[0x70]<<12)+(addr&0xfff)]=v;
    else if (addr<0xfea0)
    {
        if ((lcdMode>=2)&&(hiRam[0x40]&2)&&(hiRam[0x40]&0x80))
        {
            if (debugWarn[DBW_VIDMEM])
                DebugWarn(DBW_VIDMEM);
            return;
        }
        oam[addr&0xff]=v;
    }
    else if (addr<0xff00)
        return;
    else if (addr==0xff04)
        hiRam[4]=0;
    else if (addr==0xff07)
    {
        hiRam[7]=v;
        if (v&4)
        {
            switch(v&3)
            {
                case 0: timerClkCount=1024; break;
                case 1: timerClkCount=16; break;
                case 2: timerClkCount=64; break;
                case 3: timerClkCount=256; break;
            }
        }
        else
            timerClkCount=0;
        #ifndef C_CORE
            minTime=0;
        #endif
    }
#ifndef EXECTRACK
    else if (addr==0xff0f)
        intReq=hiRam[0xf]=v;
#endif
    else if (addr==0xff26)
    {
        hiRam[0x26]=v;
        if (!(v&0x80))
        {
            snd1Enable=0;
            snd2Enable=0;
            snd3Enable=0;
            snd4Enable=0;
        }
        sndRegChange=1;
    }
    else if ((addr>=0xff10)&&(addr<0xff30))
    {
        sndRegChange=1;
        hiRam[addr&0xff]=v;
    }
    else if ((addr>=0xff30)&&(addr<0xff40))
    {
//        if ((!(hiRam[0x26]&0x80))||(!(hiRam[0x1a]&0x80)))
            hiRam[addr&0xff]=v;
    }
    else if (addr==0xff40)
    {
        if (!(v&0x80))
            displayEnabled=0;
        if ((!(hiRam[0x40]&0x80))&&(v&0x80))
        {
            lcdY=0;
            lcdMode=0;
        }
        hiRam[0x40]=v;
    }
    else if (addr==0xff44)
    {
        lcdY=0; lcdMode=0;
        hiRam[0x44]=0;
    #ifndef EXECTRACK
        if ((hiRam[0x41]&0x40)&&(lcdY==hiRam[0x45]))
            intReq|=INT_LCDC;
    #endif
    #ifndef C_CORE
        minTime=0;
    #endif
    }
    else if (addr==0xff45)
    {
        hiRam[0x45]=v;
    #ifndef EXECTRACK
        if ((v==lcdY)&&(hiRam[0x41]&0x40))
            intReq|=INT_LCDC;
    #endif
    #ifndef C_CORE
        minTime=0;
    #endif
    }
    else if (addr==0xff46)
    {
//        if ((R.PC.D<0xff80)&&(debugWarn[DBW_DMA]))
        if ((regPC<0xff80)&&(debugWarn[DBW_DMA]))
            DebugWarn(DBW_DMA);
        dmaClk=640;
    #ifndef C_CORE
        minTime=0;
    #endif
        if (v>0xf1) return;
        if (v<0x80)
        {
            dmaType=0;
            if (!mem[v>>4]) return;
            memcpy(oam,&mem[v>>4][(v&0xf)<<8],0xa0);
        }
        else if (v<0xa0)
        {
            dmaType=1;
            memcpy(oam,&vidRam[(vidPage<<13)|(v&0x1f)<<8],0xa0);
        }
        else if (v<0xf0)
        {
            dmaType=2;
            if (!mem[v>>4]) return;
            memcpy(oam,&mem[v>>4][(v&0xf)<<8],0xa0);
        }
        else
        {
            dmaType=2;
            memcpy(oam,&intRam[(hiRam[0x70]<<12)|(v&0x1f)<<8],0xa0);
        }
    }
    else if (addr==0xff47)
    {
        hiRam[0x47]=v;
        if (!cgb)
        {
            bgPal[0]=bgPalGB[0]=grayVal[v&3];
            bgPal[1]=bgPalGB[1]=grayVal[(v>>2)&3];
            bgPal[2]=bgPalGB[2]=grayVal[(v>>4)&3];
            bgPal[3]=bgPalGB[3]=grayVal[(v>>6)&3];
        }
    }
    else if (addr==0xff48)
    {
        hiRam[0x48]=v;
        if (!cgb)
        {
            objPal[0]=objPalGB[0]=grayVal[v&3];
            objPal[1]=objPalGB[1]=grayVal[(v>>2)&3];
            objPal[2]=objPalGB[2]=grayVal[(v>>4)&3];
            objPal[3]=objPalGB[3]=grayVal[(v>>6)&3];
        }
    }
    else if (addr==0xff49)
    {
        hiRam[0x49]=v;
        if (!cgb)
        {
            objPal[4]=objPalGB[4]=grayVal[v&3];
            objPal[5]=objPalGB[5]=grayVal[(v>>2)&3];
            objPal[6]=objPalGB[6]=grayVal[(v>>4)&3];
            objPal[7]=objPalGB[7]=grayVal[(v>>6)&3];
        }
    }
    else if (addr==0xff4d)
        hiRam[0x4d]=(hiRam[0x4d]&0xfe)|(v&1);
    else if (addr==0xff4f)
    {
        hiRam[0x4f]=v;
        vidPage=v&1;
        mem[8]=&vidRam[vidPage<<13];
        mem[9]=&vidRam[(vidPage<<13)+0x1000];
    }
    else if (addr==0xff55)
    {
        hiRam[0x55]=v;
//        if (((v&0x80))&&(lcdMode!=1))
//        {
//            if (debugWarn[DBW_GDMA])
//                DebugWarn(DBW_GDMA);
//        }
//        else
        {
            int src=((hiRam[0x51]<<4)|(hiRam[0x52]>>4))<<4;
            int dest=((((hiRam[0x53]&31)|0x80)<<4)|(hiRam[0x54]>>4))<<4;
            if ((dest>=0x8000)&&(dest<=0x9fff)&&(mem[src>>12]))
            {
                int len=((v&127)+1)<<4;
                for (int i=0;i<len;i++,dest++,src++)
                    vidRam[(vidPage<<13)+(dest&0x1fff)]=mem[src>>12][src&0xfff];
            }
//            hiRam[0x55]&=0x7f;
//            hiRam[0x55]|=0x80;
            if (!(v&0x80))
                gdmaClk=(210+((727*(v&0x7f))/100)*clkMult)*4;
            hiRam[0x55]=0xff;
        }
    }
    else if (addr==0xff69)
    {
        cgb=1;
        if (hiRam[0x68]&1)
        {
            bgPalGB[(hiRam[0x68]>>1)&31]=(bgPalGB[(hiRam[0x68]>>1)&31]&0xff)|(v<<8);
            bgPal[(hiRam[0x68]>>1)&31]=Filter[bgPalGB[(hiRam[0x68]>>1)&31]&0x7fff];
        }
        else
        {
            bgPalGB[(hiRam[0x68]>>1)&31]=(bgPalGB[(hiRam[0x68]>>1)&31]&0xff00)|v;
            bgPal[(hiRam[0x68]>>1)&31]=Filter[bgPalGB[(hiRam[0x68]>>1)&31]&0x7fff];
        }
        if (hiRam[0x68]&0x80)
            hiRam[0x68]=((hiRam[0x68]+1)&63)|0x80;
    }
    else if (addr==0xff6b)
    {
        cgb=1;
        if (hiRam[0x6a]&1)
        {
            objPalGB[(hiRam[0x6a]>>1)&31]=(objPalGB[(hiRam[0x6a]>>1)&31]&0xff)|(v<<8);
            objPal[(hiRam[0x6a]>>1)&31]=Filter[objPalGB[(hiRam[0x6a]>>1)&31]&0x7fff];
        }
        else
        {
            objPalGB[(hiRam[0x6a]>>1)&31]=(objPalGB[(hiRam[0x6a]>>1)&31]&0xff00)|v;
            objPal[(hiRam[0x6a]>>1)&31]=Filter[objPalGB[(hiRam[0x6a]>>1)&31]&0x7fff];
        }
        if (hiRam[0x6a]&0x80)
            hiRam[0x6a]=((hiRam[0x6a]+1)&63)|0x80;
    }
    else if (addr==0xff70)
    {
        hiRam[0x70]=((v&7)==0)?1:(v&7);
        mem[0xd]=&intRam[hiRam[0x70]<<12];
    }
    else
        hiRam[addr&0xff]=v;
}

void putmem_word(unsigned short addr,unsigned short v)
{
    putmem(addr,v&0xff);
    putmem(addr+1,(v>>8)&0xff);
}

void putmem_dword(unsigned short addr,unsigned long v)
{
    putmem(addr,v&0xff);
    putmem(addr+1,(v>>8)&0xff);
    putmem(addr+2,(v>>16)&0xff);
    putmem(addr+3,(v>>24)&0xff);
}

#ifndef C_CORE
extern "C" int AsmZ80_Execute(int cycles);
#endif
void OneInstruction()
{
    if (!romLoaded) return;
    UpdateDebugCheckEnable();
    #ifdef C_CORE
        Z80_Execute(1);
    #else
        AsmZ80_Execute(1);
    #endif
}

void Execute()
{
    if (!romLoaded) return;
    if (!run) return;
#ifdef EXECTRACK
#ifdef C_CORE
    int cycles=Z80_Execute(1);
    SoundUpdate(cycles*(3-clkMult));
    fwrite(&regAF,2,1,execTrack);
    fwrite(&regBC,2,1,execTrack);
    fwrite(&regDE,2,1,execTrack);
    fwrite(&regHL,2,1,execTrack);
    fwrite(&regSP,2,1,execTrack);
    fwrite(&regPC,2,1,execTrack);
    hiRam[0x44]=lcdY;
    fwrite(hiRam,0xff,1,execTrack);
    fwrite(&intReq,4,1,execTrack);
    fwrite(&lcdMode,4,1,execTrack);
#else
    WORD prevPC=regPC;
    int cycles=AsmZ80_Execute(1);
    SoundUpdate(cycles*(3-clkMult));
    WORD cAF,cBC,cDE,cHL,cSP,cPC;
    fread(&cAF,2,1,execTrack);
    fread(&cBC,2,1,execTrack);
    fread(&cDE,2,1,execTrack);
    fread(&cHL,2,1,execTrack);
    fread(&cSP,2,1,execTrack);
    fread(&cPC,2,1,execTrack);
    fread(hiRam,0x80,1,execTrack);
    unsigned char wantHiRam[0x80];
    fread(wantHiRam,0x7f,1,execTrack);
    lcdY=hiRam[0x44];
    fread(&intReq,4,1,execTrack);
    fread(&lcdMode,4,1,execTrack);
    int oldCAF=cAF;
    int z=cAF&0x40?0x80:0;
    int n=cAF&0x2?0x40:0;
    int h=cAF&0x10?0x20:0;
    int c=cAF&1?0x10:0;
    cAF&=0xff00;
    cAF|=z|n|h|c;
    if (((regPC-prevPC)==1)&&(((getmem(prevPC)&(~0x30))==9)||
        ((getmem(prevPC)&(~0x18))==7)))
    {
        regAF=(regAF&0xff00)|(z?0x40:0)|(h?0x10:0)|(c?1:0);
        nFlag=n?1:0;
    }
    z=regAF&0x40?0x80:0;
    n=nFlag?0x40:0;
    h=regAF&0x10?0x20:0;
    c=regAF&1?0x10:0;
    int badAddr=-1;
    for (int i=0;i<0x7f;i++)
    {
        if (hiRam[i+0x80]!=wantHiRam[i])
        {
            if ((((0xff80+i)==regSP)||((0xff80+i)==(regSP+1)))&&(getmem(prevPC)==0xf5))
            {
                hiRam[i+0x80]=wantHiRam[i];
                continue;
            }
            badAddr=0xff80+i;
            break;
        }
    }
    if (((cAF&0xfff0)!=(regAF&0xff00|z|n|h|c))||(cBC!=regBC)||(cDE!=regDE)||(cHL!=regHL)||
        (cSP!=regSP)||(cPC!=regPC)||(badAddr!=-1))
    {
        debug=1;
        run=0;
        debugStartPC=regPC;
        DebugWnd->Show();
        DebugWnd->Update();
        char str[256];
        if (badAddr!=-1)
            sprintf(str,"Should be:\nAF=%4.4X BC=%4.4X DE=%4.4X HL=%4.4X SP=%4.4X PC=%4.4X\n"
                "Bad addr=%4.4X, should be=%2.2X  Prev PC=%4.4X",cAF,cBC,cDE,cHL,cSP,cPC,(badAddr!=-1)?badAddr:0,wantHiRam[badAddr-0xff80],prevPC);
        else
            sprintf(str,"Should be:\nAF=%4.4X BC=%4.4X DE=%4.4X HL=%4.4X SP=%4.4X PC=%4.4X\nPrev PC=%4.4X",
                cAF,cBC,cDE,cHL,cSP,cPC,prevPC);
        MessageBox(NULL,str,"",MB_OK);
    }
#endif
    return;
#endif
    UpdateDebugCheckEnable();
    static int leftOver=0;
    if (soundEnable)
    {
        for (int i=0;i<(sndQuality*16);i++)
        {
            #ifdef C_CORE
                int cycles=Z80_Execute(((256/sndQuality)*clkMult)-leftOver);
            #else
              int cycles=AsmZ80_Execute(((256/sndQuality)*clkMult)-leftOver);
            #endif
            SoundUpdate(cycles*(3-clkMult));
            leftOver=cycles-(((256/sndQuality)*clkMult)-leftOver);
        }
    }
    else
    {
        #ifdef C_CORE
            int cycles=Z80_Execute((4096*clkMult)-leftOver);
        #else
            int cycles=AsmZ80_Execute((4096*clkMult)-leftOver);
        #endif
        leftOver=cycles-((4096*clkMult)-leftOver);
    }
}

void Reset()
{
    if (!romLoaded) return;
    #ifdef C_CORE
        Z80_Reset();
    #endif
    regPC=0x100;
    regBC=0x13;
    regDE=0xd8;
    regHL=0x14d;
    regSP=0xfffe;
    #ifdef C_CORE
        regAF=0x11b0;
        #ifdef EXECTRACK
            execTrack=fopen("ExecTrack.dat","wb");
        #endif
    #else
        regAF=0x1110;//0x11b0;
        #ifdef EXECTRACK
            execTrack=fopen("ExecTrack.dat","rb");
        #endif
    #endif
    hiRam[5]=0; hiRam[6]=0; hiRam[7]=0; hiRam[0xf]=0; hiRam[0x10]=0x80;
    hiRam[0x11]=0xbf; hiRam[0x12]=0xf3; hiRam[0x14]=0x3f;
    hiRam[0x16]=0x3f; hiRam[0x17]=0; hiRam[0x19]=0x3f;
    hiRam[0x1a]=0x7f; hiRam[0x1b]=0xff; hiRam[0x1c]=0x9f;
    hiRam[0x1e]=0xbf; hiRam[0x20]=0xff; hiRam[0x21]=0;
    hiRam[0x22]=0; hiRam[0x23]=0x3f; hiRam[0x24]=0x77;
    hiRam[0x25]=0; hiRam[0x26]=0xf1; hiRam[0x40]=0x91;
    hiRam[0x42]=0; hiRam[0x43]=0; hiRam[0x45]=0;
    hiRam[0x47]=0xfc; hiRam[0x48]=0xff; hiRam[0x49]=0xff;
    hiRam[0x4a]=0; hiRam[0x4b]=0; hiRam[0xff]=0;
    hiRam[0x4d]=0; hiRam[0x4f]=0; hiRam[0x68]=0;
    hiRam[0x70]=1;
    mbc1Mode=0; romPage=1; ramPage=0; ramEnable=0;
    mem[0]=rom; mem[1]=&rom[0x1000];
    mem[2]=&rom[0x2000]; mem[3]=&rom[0x3000];
    mem[4]=&rom[0x4000]; mem[5]=&rom[0x5000];
    mem[6]=&rom[0x6000]; mem[7]=&rom[0x7000];
    mem[8]=vidRam; mem[9]=&vidRam[0x1000];
    mem[0xa]=NULL; mem[0xb]=NULL;
    mem[0xc]=intRam; mem[0xd]=&intRam[0x1000];
    mem[0xe]=intRam; mem[0xf]=NULL;
    intReq=0; intReq2=0; intReq3=0; timerClkCount=0;
    lcdMode=1; lcdY=144;
    clkMult=1; vidPage=0;
    memset(vidRam,0,0x4000);
    for (int i=0;i<8;i++)
    {
        bgPal[i<<2]=bgPalGB[i<<2]=(31<<10)|(31<<5)|31;
        bgPal[(i<<2)+1]=bgPalGB[(i<<2)+1]=(21<<10)|(21<<5)|21;
        bgPal[(i<<2)+2]=bgPalGB[(i<<2)+2]=(10<<10)|(10<<5)|10;
        bgPal[(i<<2)+3]=bgPalGB[(i<<2)+3]=0;
        objPal[i<<2]=objPalGB[i<<2]=(31<<10)|(31<<5)|31;
        objPal[(i<<2)+1]=objPalGB[(i<<2)+1]=(21<<10)|(21<<5)|21;
        objPal[(i<<2)+2]=objPalGB[(i<<2)+2]=(10<<10)|(10<<5)|10;
        objPal[(i<<2)+3]=objPalGB[(i<<2)+3]=0;
    }
    hiRam[0x68]=0x80; hiRam[0x6a]=0x80;
    hiRam[0x69]=bgPal[0]&0xff;
    hiRam[0x6b]=objPal[0]&0xff;
    snd1Enable=snd2Enable=snd3Enable=snd4Enable=0;
    sndRegChange=1;
    displayEnabled=1;
    cgb=0;
    memset(vidRam,0,0x4000);
    memset(oam,0,0xa0);
}

/*int _USERENTRY sprcmp(const void *a,const void *b)
{
    asm mov eax,a
    asm mov ebx,b
    asm mov cl,[eax+4]
    asm cmp cl,[ebx+4]
    asm jne sprcmp_notEqu
    asm mov cl,[eax+1]
    asm cmp cl,[ebx+1]
    asm jl sprcmp_less
    asm jg sprcmp_greater
    asm xor eax,eax
    asm jmp sprcmp_ret
sprcmp_notEqu:
    asm jl sprcmp_greater
sprcmp_less:
    asm mov eax,-1
    asm jmp sprcmp_ret
sprcmp_greater:
    asm mov eax,1
sprcmp_ret:
}*/

void RenderLine()
{
    if (lcdY>=144) return;
    if (!displayEnabled)
    {
        if (scaleFact==1)
        {
            char *ptr0=ScreenLine[lcdY];
            int value=Filter[0x7fff]|(Filter[0x7fff]<<16);
            asm mov edi,ptr0
            asm mov eax,value
            asm mov ecx,80
            asm rep stosd
        }
        else
        {
            char *ptr0=ScreenLine[lcdY<<1];
            char *ptr1=ScreenLine[(lcdY<<1)+1];
            int value=Filter[0x7fff]|(Filter[0x7fff]<<16);
            asm mov edi,ptr0
            asm mov eax,value
            asm mov ecx,160
            asm rep stosd
            asm mov edi,ptr1
            asm mov eax,value
            asm mov ecx,160
            asm rep stosd
        }
        return;
    }
    unsigned short v[160],bc[160];
    char bpri[160];
//    memset(v,0,320);
//    memset(bc,0,320);
//    if (hiRam[0x40]&1)
    {
        int bkty=((hiRam[0x42]+lcdY)&0xff)>>3;
        int bkyo=(hiRam[0x42]+lcdY)&7;
        int bktx=hiRam[0x43]>>3;
        int bkxo=hiRam[0x43]&7;
        int vidRamBase=((hiRam[0x40]&8)?0x1c00:0x1800)+(bkty<<5);
        int tileBase=((hiRam[0x40]&16)?0:0x1000);
        for (int i=0;i<160;)
        {
            int ti=vidRam[vidRamBase+bktx];
            int pn=vidRam[vidRamBase+0x2000+bktx];
            int attr=pn&0xf8;
            pn&=7;
            if ((!(hiRam[0x40]&16))&&(ti>=0x80))
                ti=-(0x100-ti);
            int taddr=tileBase+(ti*16);
            if (attr&8) taddr+=0x2000;
            int v0=vidRam[taddr+(((attr&0x40)?(7-bkyo):bkyo)<<1)];
            int v1=vidRam[taddr+(((attr&0x40)?(7-bkyo):bkyo)<<1)+1];
            for (;(bkxo<8)&&(i<160);)
            {
                int c;
                if (attr&0x20)
                {
                    c=(v0&(0x80>>(7-bkxo)))?1:0;
                    c|=(v1&(0x80>>(7-bkxo)))?2:0;
                }
                else
                {
                    c=(v0&(0x80>>bkxo))?1:0;
                    c|=(v1&(0x80>>bkxo))?2:0;
                }
                bpri[i]=((attr&0x80)&&(c));
                bc[i]=c;
                v[i]=bgPal[(pn<<2)+c];
//                r[i]=(bgPal[(pn<<2)+c]&31)<<3;
//                g[i]=((bgPal[(pn<<2)+c]>>5)&31)<<3;
//                b[i]=((bgPal[(pn<<2)+c]>>10)&31)<<3;
                bkxo++; i++;
            }
            bkxo=0; bktx=(bktx+1)&31;
        }
    }
    static int wy,wyr;
    if (lcdY==0) { wy=hiRam[0x4a]; wyr=0; }
    if ((wy<=lcdY)&&(hiRam[0x4b]<=166)&&
        (hiRam[0x40]&0x20))
    {
        int wty=wyr>>3;
        int wyo=wyr&7;
        wyr++;
        int wtx=0;
        int wxo=0;
        for (int i=(hiRam[0x4b]-7);i<160;)
        {
            if (i<0)
            {
                wxo-=i;
                i=0;
                continue;
            }
            int ti=vidRam[((hiRam[0x40]&0x40)?0x1c00:0x1800)+
                (wty<<5)+wtx];
            int pn=vidRam[((hiRam[0x40]&0x40)?0x3c00:0x3800)+
                (wty<<5)+wtx];
            int attr=pn&0xf8;
            pn&=7;
            if ((!(hiRam[0x40]&16))&&(ti>=0x80))
                ti=-(0x100-ti);
            int taddr=((hiRam[0x40]&16)?0:0x1000)+(ti*16);
            if (attr&8) taddr+=0x2000;
            int v0=vidRam[taddr+(((attr&0x40)?(7-wyo):wyo)<<1)];
            int v1=vidRam[taddr+(((attr&0x40)?(7-wyo):wyo)<<1)+1];
            for (;(wxo<8)&&(i<160);)
            {
                int c;
                if (attr&0x20)
                {
                    c=(v0&(0x80>>(7-wxo)))?1:0;
                    c|=(v1&(0x80>>(7-wxo)))?2:0;
                }
                else
                {
                    c=(v0&(0x80>>wxo))?1:0;
                    c|=(v1&(0x80>>wxo))?2:0;
                }
                bc[i]=c;
                v[i]=bgPal[(pn<<2)+c];
//                r[i]=(bgPal[(pn<<2)+c]&31)<<3;
//                g[i]=((bgPal[(pn<<2)+c]>>5)&31)<<3;
//                b[i]=((bgPal[(pn<<2)+c]>>10)&31)<<3;
                wxo++; i++;
            }
            wxo=0; wtx++;
        }
    }
    if (hiRam[0x40]&2)
    {
        char s[40];
/*        asm lea eax,s
        asm lea ebx,oam
        asm mov ecx,40
       sprCopyLoop:
        asm mov edx,[ebx]
        asm mov [eax],edx
        asm mov [eax+4],cl
        asm add ebx,4
        asm add eax,8
        asm loop sprCopyLoop
        qsort(s,40,8,sprcmp);*/
        if (hiRam[0x40]&4)
        {
            int i,j,k,bit;
            for (i=0,j=0;i<40;i++)
            {
                if ((oam[i<<2]-16)>lcdY)
                    s[i]=0;
                else if ((oam[i<<2]-16+15)<lcdY)
                    s[i]=0;
                else if (oam[(i<<2)+1]>=168)
                {
                    j++;
                    s[i]=0;
                }
                else if (j>=10)
                    s[i]=0;
                else
                {
                    s[i]=1;
                    j++;
                }
            }
            for (i=39;i>=0;i--)
            {
                if (!s[i])
                    continue;
                int saddr=(oam[(i<<2)+2]&(~1))<<4;
                if (oam[(i<<2)+3]&0x40)
                    saddr+=(15+((oam[i<<2]-16)-lcdY))<<1;
                else
                    saddr+=(lcdY-(oam[i<<2]-16))<<1;
                if (oam[(i<<2)+3]&8)
                    saddr+=0x2000;
                int v0=vidRam[saddr];
                int v1=vidRam[saddr+1];
                int pn=(oam[(i<<2)+3]&7)|(cgb?0:((oam[(i<<2)+3]>>4)&1));
                bit=(oam[(i<<2)+3]&0x20)?0:7;
                for (j=oam[(i<<2)+1]-8,k=0;(j<160)&&(k<8);j++,k++,bit+=(oam[(i<<2)+3]&0x20)?1:-1)
                {
                    if (j<0) continue;
                    if (bpri[j]) continue;
                    if ((oam[(i<<2)+3]&0x80)&&(bc[j])) continue;
                    int c=(v0&(1<<bit))?1:0;
                    c|=(v1&(1<<bit))?2:0;
                    if (c==0) continue;
                    v[j]=objPal[(pn<<2)+c];
//                    r[j]=(objPal[(pn<<2)+c]&31)<<3;
//                    g[j]=((objPal[(pn<<2)+c]>>5)&31)<<3;
//                    b[j]=((objPal[(pn<<2)+c]>>10)&31)<<3;
                }
            }
        }
        else
        {
            int i,j,k,bit;
            for (i=0,j=0;i<40;i++)
            {
                if ((oam[i<<2]-16)>lcdY)
                    s[i]=0;
                else if ((oam[i<<2]-16+7)<lcdY)
                    s[i]=0;
                else if (oam[(i<<2)+1]>=168)
                {
                    j++;
                    s[i]=0;
                }
                else if (j>=10)
                    s[i]=0;
                else
                {
                    s[i]=1;
                    j++;
                }
            }
            for (i=39;i>=0;i--)
            {
                if (!s[i])
                    continue;
                int saddr=oam[(i<<2)+2]<<4;
                if (oam[(i<<2)+3]&0x40)
                    saddr+=(7+((oam[i<<2]-16)-lcdY))<<1;
                else
                    saddr+=(lcdY-(oam[i<<2]-16))<<1;
                if (oam[(i<<2)+3]&8)
                    saddr+=0x2000;
                int v0=vidRam[saddr];
                int v1=vidRam[saddr+1];
                int pn=(oam[(i<<2)+3]&7)|(cgb?0:((oam[(i<<2)+3]>>4)&1));
                bit=(oam[(i<<2)+3]&0x20)?0:7;
                for (j=oam[(i<<2)+1]-8,k=0;(j<160)&&(k<8);j++,k++,bit+=(oam[(i<<2)+3]&0x20)?1:-1)
                {
                    if (j<0) continue;
                    if (bpri[j]) continue;
                    if ((oam[(i<<2)+3]&0x80)&&(bc[j])) continue;
                    int c=(v0&(1<<bit))?1:0;
                    c|=(v1&(1<<bit))?2:0;
                    if (c==0) continue;
                    v[j]=objPal[(pn<<2)+c];
//                    r[j]=(objPal[(pn<<2)+c]&31)<<3;
//                    g[j]=((objPal[(pn<<2)+c]>>5)&31)<<3;
//                    b[j]=((objPal[(pn<<2)+c]>>10)&31)<<3;
                }
            }
        }
    }
    if (scaleFact==1)
    {
        char *ptr0=ScreenLine[lcdY];
        asm mov edi,ptr0
        asm lea esi,v
        asm mov ecx,80
        asm rep movsd
    }
    else
    {
        char *ptr0=ScreenLine[lcdY<<1];
        char *ptr1=ScreenLine[(lcdY<<1)+1];
        asm mov edi,ptr0
        asm mov edx,ptr1
        asm lea esi,v
        asm mov ecx,160
       copyLoop:
        asm mov bx,[esi]
        asm mov ax,bx
        asm shl eax,16
        asm mov ax,bx
        asm add esi,2
        asm mov [edi],eax
        asm add edi,4
        asm mov [edx],eax
        asm add edx,4
        asm loop copyLoop
    }
/*    for (int i=0;i<160;i++)
    {
        if (scaleFact==1)
        {
            ScreenLine[lcdY][i<<1]=r[i]&0xff;
            ScreenLine[lcdY][(i<<1)+1]=r[i]>>8;
//            ScreenLine[lcdY][(i<<2)+2]=r[i];
        }
        else
        {
            ScreenLine[lcdY<<1][i<<3]=b[i];
            ScreenLine[lcdY<<1][(i<<3)+1]=g[i];
            ScreenLine[lcdY<<1][(i<<3)+2]=r[i];
            ScreenLine[(lcdY<<1)+1][i<<3]=b[i];
            ScreenLine[(lcdY<<1)+1][(i<<3)+1]=g[i];
            ScreenLine[(lcdY<<1)+1][(i<<3)+2]=r[i];
            ScreenLine[lcdY<<1][(i<<3)+4]=b[i];
            ScreenLine[lcdY<<1][(i<<3)+5]=g[i];
            ScreenLine[lcdY<<1][(i<<3)+6]=r[i];
            ScreenLine[(lcdY<<1)+1][(i<<3)+4]=b[i];
            ScreenLine[(lcdY<<1)+1][(i<<3)+5]=g[i];
            ScreenLine[(lcdY<<1)+1][(i<<3)+6]=r[i];
        }
    }*/
}

extern int debugStartPC,debug;
void DebugWarn(int wn)
{
    debugStartPC=previouspc;
    run=0;
    debug=1;
    DebugWnd->Show();
    DebugWnd->Update();
    debugWarnMsg=wn;
}

extern int lcdXOfs,lcdYOfs;
void RedrawLCD()
{
    LCDCanvas->Draw(lcdXOfs,lcdYOfs,bmp);
}

extern int debugRunToCursor,debugCursorAddr;
extern int debugCodeBreakCount,debugCodeBreak[256];
extern int debugDataBreak;
extern int debugStepOver,debugBeginStepOver,endStepOver;
extern int debugStepGoal,debugStepA7,debugEndStepOver;
extern int ignoreStack; 
extern unsigned short oldPC;
void DebugChecks()
{
    int bp,i;
    if (debugRunToCursor)
    {
        if (regPC==debugCursorAddr)
        {
            run=0;
            debug=1;
            debugStartPC=regPC;
            debugRunToCursor=0;
            DebugWnd->Show();
            DebugWnd->Update();
            return;
        }
    }
    for (i=0,bp=0;i<debugCodeBreakCount;i++)
    {
        if (regPC==debugCodeBreak[i])
            bp=1;
    }
    if (bp)
    {
        run=0;
        debug=1;
        debugStartPC=regPC;
        DebugWnd->Show();
        DebugWnd->Update();
        return;
    }
    if (debugDataBreak)
    {
        run=0;
        debug=1;
        debugStartPC=oldPC;
        DebugWnd->Show();
        DebugWnd->Update();
        return;
    }
    if (debugStepOver)
    {
        if (debugBeginStepOver)
        {
            debugBeginStepOver=0;
            if (endStepOver)
            {
                run=0;
                debug=1;
                debugStartPC=regPC;
                debugEndStepOver=1;
                DebugWnd->Show();
                DebugWnd->Update();
                return;
            }
        }
        if (debugStepOver&&(regPC==debugStepGoal)&&
            ((regSP==debugStepA7)||(ignoreStack)))
        {
            run=0;
            debug=1;
            debugStartPC=regPC;
            DebugWnd->Show();
            DebugWnd->Update();
            debugEndStepOver=1;
            return;
        }
    }
}

