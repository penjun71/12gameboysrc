//---------------------------------------------------------------------------
#ifndef TileViewH
#define TileViewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TTileWnd : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *Tiles;
    TPaintBox *PaintBox1;
    TGroupBox *ZoomView;
    TPaintBox *PaintBox2;
    TShape *Shape1;
    TShape *Shape2;
    TShape *Shape3;
    TShape *Shape4;
    TLabel *Address;
    TLabel *R0;
    TLabel *G0;
    TLabel *B0;
    TLabel *R1;
    TLabel *G1;
    TLabel *B1;
    TLabel *R2;
    TLabel *G2;
    TLabel *B2;
    TLabel *R3;
    TLabel *G3;
    TLabel *B3;
    TCheckBox *UsePalette;
    void __fastcall PaintBox1Paint(TObject *Sender);
    void __fastcall PaintBox1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
    
    
    void __fastcall UsePaletteClick(TObject *Sender);
    
private:	// User declarations
public:		// User declarations
    __fastcall TTileWnd(TComponent* Owner);
    void DoUpdate();
};
//---------------------------------------------------------------------------
extern PACKAGE TTileWnd *TileWnd;
//---------------------------------------------------------------------------
#endif
