#define VERSION_STRING "1.0"
//---------------------------------------------------------------------------
#include <vcl.h>
#include <vcl\registry.hpp>
#include <stdio.h>
#pragma hdrstop
USEFORM("Emu.cpp", EmuWnd);
USELIB("gbcemu.lib");
USEFORM("bung.cpp", BungSendWnd);
USERES("gbc.res");
//---------------------------------------------------------------------------
#include "Debug.h"
#include "emu.h"
#include "rom.h"
#include "gbcemu.h"

HINSTANCE hInst;
int needPaint=0;
HWND hWnd;

int cmdLineValid=0;
char initialROM[256];

WINAPI WinMain(HINSTANCE inst, HINSTANCE, LPSTR cmdLine, int)
{
    hInst=inst;
    try
    {
        Application->Initialize();
        Application->Title = "";
        Application->ShowMainForm=false;
        Application->ShowHint=true;
        if (cmdLine[0])
        {
            cmdLineValid=1;
            strcpy(initialROM,cmdLine);
            if (initialROM[0]=='\"')
            {
                strcpy(initialROM,&cmdLine[1]);
                initialROM[strlen(initialROM)-1]=0;
            }
        }
        Application->CreateForm(__classid(TEmuWnd), &EmuWnd);
         Application->CreateForm(__classid(TBungSendWnd), &BungSendWnd);
         {
            TRegistry *reg=new TRegistry;
            reg->RootKey=HKEY_LOCAL_MACHINE;
            reg->OpenKey("\\Software\\gbc",true);
            reg->WriteString("LastEmuVersion",VERSION_STRING);
            reg->CloseKey();
            delete reg;
        }
        Application->Run();
    }
    catch (Exception &exception)
    {
        Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
