//---------------------------------------------------------------------------
#ifndef MapViewH
#define MapViewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TMapWnd : public TForm
{
__published:	// IDE-managed Components
    TShape *Shape1;
    TShape *Shape2;
    TShape *Shape3;
    TShape *Shape4;
    TLabel *Address;
    TLabel *R0;
    TLabel *G0;
    TLabel *B0;
    TLabel *R1;
    TLabel *G1;
    TLabel *B1;
    TLabel *R2;
    TLabel *G2;
    TLabel *B2;
    TLabel *R3;
    TLabel *G3;
    TLabel *B3;
    TGroupBox *ZoomView;
    TPaintBox *PaintBox2;
    TTabControl *TabControl1;
    TGroupBox *Map;
    TPaintBox *PaintBox1;
    TLabel *Number;
    TLabel *HFlip;
    TLabel *VFlip;
    void __fastcall PaintBox1Paint(TObject *Sender);
    void __fastcall TabControl1Change(TObject *Sender);
    void __fastcall PaintBox1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
private:	// User declarations
public:		// User declarations
    __fastcall TMapWnd(TComponent* Owner);
    void __fastcall DoUpdate();
};
//---------------------------------------------------------------------------
extern PACKAGE TMapWnd *MapWnd;
//---------------------------------------------------------------------------
#endif
