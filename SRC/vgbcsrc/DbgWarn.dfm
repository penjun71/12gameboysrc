�
 TDEBUGWARNDLG 0�  TPF0TDebugWarnDlgDebugWarnDlgLeftTop�BorderStylebsDialogCaptionDebug WarningsClientHeight� ClientWidthV
ParentFont	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth� HeightCaption Select debug warnings to enable:  TButtonOKBtnLeft� Top� Width:HeightCaptionOKDefault	ModalResultTabOrder OnClick
OKBtnClick  TButton	CancelBtnLeftTop� Width:HeightCancel	CaptionCancelModalResultTabOrderOnClickCancelBtnClick  TCheckListBoxListLeftTopWidthAHeight� 
ItemHeightItems.Strings#Attempt to access disabled RAM bank)Attempt to access invalid memory locationMemory access during DMA)DMA enabled from code outside high memory&Video memory access during LCD refresh$Instruction may cause OAM memory bug=HALT instruction with interrupts disabled not followed by NOP-STOP instruction without CPU speed switch setRAM/ROM page out of rangeStack in ROMGDMA enabled outside of VBlank TabOrder  TButtonButton1LeftTop� WidthKHeightCaption
Enable AllTabOrderOnClickButton1Click  TButtonButton2Left`Top� WidthKHeightCaptionDisable AllTabOrderOnClickButton2Click   