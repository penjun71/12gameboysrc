//---------------------------------------------------------------------------
#ifndef PalH
#define PalH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TPalView : public TForm
{
__published:	// IDE-managed Components
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TLabel *Label7;
    TLabel *Label8;
    TShape *Bg00;
    TShape *Bg01;
    TShape *Bg02;
    TShape *Bg03;
    TShape *Bg10;
    TShape *Bg11;
    TShape *Bg12;
    TShape *Bg13;
    TShape *Bg20;
    TShape *Bg21;
    TShape *Bg22;
    TShape *Bg23;
    TShape *Bg30;
    TShape *Bg31;
    TShape *Bg32;
    TShape *Bg33;
    TShape *Bg40;
    TShape *Bg41;
    TShape *Bg42;
    TShape *Bg43;
    TShape *Bg50;
    TShape *Bg51;
    TShape *Bg52;
    TShape *Bg53;
    TShape *Bg60;
    TShape *Bg61;
    TShape *Bg62;
    TShape *Bg63;
    TShape *Bg70;
    TShape *Bg71;
    TShape *Bg72;
    TShape *Bg73;
    TLabel *Label9;
    TLabel *Label10;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label13;
    TLabel *Label14;
    TLabel *Label15;
    TLabel *Label16;
    TShape *Obj00;
    TShape *Obj01;
    TShape *Obj02;
    TShape *Obj03;
    TShape *Obj10;
    TShape *Obj11;
    TShape *Obj12;
    TShape *Obj13;
    TShape *Obj20;
    TShape *Obj21;
    TShape *Obj22;
    TShape *Obj23;
    TShape *Obj30;
    TShape *Obj31;
    TShape *Obj32;
    TShape *Obj33;
    TShape *Obj40;
    TShape *Obj41;
    TShape *Obj42;
    TShape *Obj43;
    TShape *Obj50;
    TShape *Obj51;
    TShape *Obj52;
    TShape *Obj53;
    TShape *Obj60;
    TShape *Obj61;
    TShape *Obj62;
    TShape *Obj63;
    TShape *Obj70;
    TShape *Obj71;
    TShape *Obj72;
    TShape *Obj73;
    TLabel *Label17;
    TLabel *Label18;
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    
    
private:	// User declarations
public:		// User declarations
    __fastcall TPalView(TComponent* Owner);
    void __fastcall DoUpdate();
};
//---------------------------------------------------------------------------
extern PACKAGE TPalView *PalView;
//---------------------------------------------------------------------------
#endif
