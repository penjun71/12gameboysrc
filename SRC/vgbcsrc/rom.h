//---------------------------------------------------------------------------
#ifndef romH
#define romH

//#define C_CORE

#define INT_KEY 16
#define INT_SERIAL 8
#define INT_TIMER 4
#define INT_LCDC 2
#define INT_VBLANK 1

#define DBW_COUNT 11
#define DBW_HANG -2
#define DBW_RAMDISABLED 0
#define DBW_INVALIDMEM 1
#define DBW_DMA 2
#define DBW_DMAENABLE 3
#define DBW_VIDMEM 4
#define DBW_OAMBUG 5
#define DBW_HALT 6
#define DBW_STOP 7
#define DBW_PAGERANGE 8
#define DBW_STACK 9
#define DBW_GDMA 10

struct SkinDesc
{
    char file[32];
    char name[64];
    int calc;
    unsigned short csum;
    int defW,defH;
};
extern SkinDesc skin[64];
extern int skinCount,currentSkin;
extern Graphics::TBitmap *skinImageLg;
extern Graphics::TBitmap *skinImageSm;
extern RECT skinLcd;
extern RECT skinKey[8];

typedef unsigned char UBYTE;
extern UBYTE *ram,*rom;
extern UBYTE vidRam[16384];
extern UBYTE oam[0xa0];
extern UBYTE hiRam[0x100];
extern UBYTE intRam[0x8000];
extern UBYTE *mem[16];
extern int romLoaded,clkMult;
extern int romType,romSize,ramSize,mbc,batt;
extern int keys,timerClkCount,intReq;
extern int mbc1Mode,romPage,ramPage,ramEnable,vidPage;
extern int bgPal[32],objPal[32];
extern int bgPalGB[32],objPalGB[32];
extern int initialPC;
extern int lcdMode,lcdY;
extern int lcdRefresh,lcdUpdateNeeded;
extern int lcdUpdateFreq,run;
extern char *ScreenLine[288];
extern int scaleFact;
extern int soundEnable,sndQuality;
extern int cgb,debugWarn[DBW_COUNT],debugWarnMsg;
extern int restrictSpeed;
extern char romFileName[256];
extern int previouspc,dmaClk;

extern void InitEmu(char *name);
extern void CloseEmu();
extern void SaveState();
extern void LoadState();
extern void AnalyzeSkins();
extern void LoadSkin(int n);
extern void CloseSkin(int saveSkin=1);
extern int GetKeyAt(int x,int y);

extern Graphics::TBitmap* GetLCD_BW();
extern Graphics::TBitmap* GetLCD_True();
extern Graphics::TBitmap* GetLCD_Calc(Graphics::TBitmap* calc);

extern unsigned char (*getmemfunc)(unsigned short addr);
extern void (*putmemfunc)(unsigned short addr,unsigned char v);
extern "C" unsigned char getmem_direct(unsigned short addr);
extern "C" unsigned char getmem(unsigned short addr);
extern "C" unsigned short getmem_word(unsigned short addr);
extern "C" unsigned long getmem_dword(unsigned short addr);
extern "C" void putmem_direct(unsigned short addr,unsigned char v);
extern "C" void putmem(unsigned short addr,unsigned char v);
extern "C" void putmem_word(unsigned short addr,unsigned short v);
extern "C" void putmem_dword(unsigned short addr,unsigned long v);
extern void OneInstruction();
extern void Execute();
extern void Reset();
extern "C" void RenderLine();
extern void DebugWarn(int wn);
extern "C" void RedrawLCD();
extern "C" void DebugChecks();
extern void GenFilter();

extern unsigned short Filter[32768];
//---------------------------------------------------------------------------
#endif
