//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "DbgWarn.h"
//--------------------------------------------------------------------- 
#pragma resource "*.dfm"
TDebugWarnDlg *DebugWarnDlg;
//---------------------------------------------------------------------
__fastcall TDebugWarnDlg::TDebugWarnDlg(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void __fastcall TDebugWarnDlg::OKBtnClick(TObject *Sender)
{
    ModalResult=2;    
}
//---------------------------------------------------------------------------

void __fastcall TDebugWarnDlg::CancelBtnClick(TObject *Sender)
{
    ModalResult=1;    
}
//---------------------------------------------------------------------------

void __fastcall TDebugWarnDlg::Button1Click(TObject *Sender)
{
    for (int i=0;i<List->Items->Count;i++)
        List->Checked[i]=true;    
}
//---------------------------------------------------------------------------

void __fastcall TDebugWarnDlg::Button2Click(TObject *Sender)
{
    for (int i=0;i<List->Items->Count;i++)
        List->Checked[i]=false;    
}
//---------------------------------------------------------------------------


void __fastcall TDebugWarnDlg::FormCreate(TObject *Sender)
{
    #ifdef DEMO
    List->Enabled=false;
    Button1->Enabled=false;
    Button2->Enabled=false;
    #endif
}
//---------------------------------------------------------------------------

