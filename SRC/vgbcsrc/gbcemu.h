#ifndef __GBCEMU_H__
#define __GBCEMU_H__

#define KEY_RIGHT 1
#define KEY_LEFT 2
#define KEY_UP 4
#define KEY_DOWN 8
#define KEY_A 0x10
#define KEY_B 0x20
#define KEY_SELECT 0x40
#define KEY_START 0x80

#define EMUMODE_ACCURACY 0
#define EMUMODE_SPEED 1

extern "C" __cdecl int GBC_Init(HWND hWnd);
extern "C" __cdecl void GBC_SetWindow(HWND hWnd);
extern "C" __cdecl void GBC_Close();
extern "C" __cdecl void GBC_LoadROM(char *filename);
extern "C" __cdecl void GBC_CloseROM();
extern "C" __cdecl void GBC_Run();
extern "C" __cdecl void GBC_Reset();
extern "C" __cdecl void GBC_PaintLCD();
extern "C" __cdecl void GBC_SetLCDUpdateFreq(int freq);
extern "C" __cdecl void GBC_SetSoundQuality(int quality);
extern "C" __cdecl void GBC_EnableSound();
extern "C" __cdecl void GBC_DisableSound();
extern "C" __cdecl void GBC_KeyDown(int key);
extern "C" __cdecl void GBC_KeyUp(int key);
extern "C" __cdecl void GBC_SetSpeedRestrict(int restrict);
extern "C" __cdecl int GBC_GetPercentSpeed();
extern "C" __cdecl void GBC_EnterDebug();
extern "C" __cdecl int GBC_DebugWarnDlg();
extern "C" __cdecl void GBC_LCDScaleFact(int fact);
extern "C" __cdecl void GBC_LCDPos(int x,int y);
extern "C" __cdecl void GBC_SetMode(int mode);

#endif

