//---------------------------------------------------------------------------
#ifndef EmuH
#define EmuH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Graphics.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
class TEmuWnd : public TForm
{
__published:	// IDE-managed Components
    TPaintBox *LCD;
    TImage *gb;
    TImage *batt;
    TPopupMenu *PopupMenu1;
    TMenuItem *File1;
    TMenuItem *Exitemulator1;
    TMenuItem *N1;
    TMenuItem *Takescreenshot1;
    TMenuItem *CalculatorimageClipboard1;
    TMenuItem *LCDonlytruecolorsClipboard1;
    TMenuItem *LCDonlygrayscaleClipboard1;
    TMenuItem *N2;
    TMenuItem *CalculatorimageBMP1;
    TMenuItem *LCDonlytruecolorsBMP1;
    TMenuItem *LCDonlygrayscaleBMP1;
    TMenuItem *Resetgame1;
    TMenuItem *Savestateimage1;
    TMenuItem *Loadstateimage1;
    TMenuItem *N4;
    TMenuItem *Emulateforspeed1;
    TMenuItem *Emulateforaccuracy1;
    TMenuItem *Startindebugmode1;
    TMenuItem *Enabledebugwarnings1;
    TMenuItem *Enterdebugger1;
    TMenuItem *SendtoBungXChanger1;
    TMenuItem *CloseROM1;
    TMenuItem *LoadROM1;
    TMenuItem *View1;
    TMenuItem *Skip8frames1;
    TMenuItem *Skip3frames1;
    TMenuItem *Skip2frames1;
    TMenuItem *Skip1frame1;
    TMenuItem *Noframeskip1;
    TMenuItem *N3;
    TMenuItem *Showskin1;
    TMenuItem *Fullscreenview1;
    TMenuItem *Large2xview1;
    TMenuItem *Normal1xview1;
    TMenuItem *N6;
    TMenuItem *Runatmaximumpossiblespeed1;
    TMenuItem *Stayontop1;
    TMenuItem *Showpercentageofactualspeed1;
    TMenuItem *Sound1;
    TMenuItem *Highqualitysound1;
    TMenuItem *Mediumqualitysound1;
    TMenuItem *Lowqualitysound1;
    TMenuItem *N7;
    TMenuItem *Enablesound1;
    TMenuItem *N8;
    TLabel *Speed;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall LCDPaint(TObject *Sender);

    void __fastcall SendFile1Click(TObject *Sender);
    void __fastcall Reset1Click(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall FormDeactivate(TObject *Sender);
    void __fastcall Enterdebugmode1Click(TObject *Sender);
    void __fastcall Savestateimage1Click(TObject *Sender);
    void __fastcall Loadstateimage1Click(TObject *Sender);
    void __fastcall Large2xcalculatorimage1Click(TObject *Sender);
    void __fastcall Showpercentageofactualspeed1Click(TObject *Sender);
    void __fastcall LCDonlyblackwhiteBMP1Click(TObject *Sender);
    void __fastcall LCDonlytruecolorsBMP1Click(TObject *Sender);
    void __fastcall LCDonlyblackwhiteClipboard1Click(TObject *Sender);
    void __fastcall LCDonlytruecolorsClipboard1Click(TObject *Sender);
    void __fastcall CalculatorimageBMP1Click(TObject *Sender);
    void __fastcall CalculatorimageClipboard1Click(TObject *Sender);
    void __fastcall Normal1xview1Click(TObject *Sender);
    void __fastcall OnKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall OnKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
    void __fastcall ImagePaint(TObject *Sender);
    void __fastcall Fullscreenview1Click(TObject *Sender);
    void __fastcall Exitemulator1Click(TObject *Sender);
    void __fastcall Stayontop1Click(TObject *Sender);
    void __fastcall ImageMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall Enablesound1Click(TObject *Sender);
    void __fastcall ImageMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    
    void __fastcall Noframeskip1Click(TObject *Sender);
    void __fastcall Skip1frame1Click(TObject *Sender);
    void __fastcall Skip2frames1Click(TObject *Sender);
    void __fastcall Skip3frames1Click(TObject *Sender);
    void __fastcall Lowqualitysound1Click(TObject *Sender);
    void __fastcall Mediumqualitysound1Click(TObject *Sender);
    void __fastcall Highqualitysound1Click(TObject *Sender);
    void __fastcall Startindebugmode1Click(TObject *Sender);
    void __fastcall EnableDebugWarnings1Click(TObject *Sender);
    void __fastcall Runatmaximumpossiblespeed1Click(TObject *Sender);
    void __fastcall Skip8frames1Click(TObject *Sender);
    
    void __fastcall Showskin1Click(TObject *Sender);
    void __fastcall CloseROM1Click(TObject *Sender);
    void __fastcall SendtoBungXChanger1Click(TObject *Sender);
    
    void __fastcall Emulateforaccuracy1Click(TObject *Sender);
    void __fastcall Emulateforspeed1Click(TObject *Sender);
    
    void __fastcall gbMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall gbMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall gbMouseMove(TObject *Sender, TShiftState Shift, int X,
          int Y);
private:	// User declarations
    int keyHeld,genTab,enforceRatio,nonFullSkin;
    Graphics::TBitmap *calcImage;
    unsigned long perfFreq;
public:		// User declarations
    Graphics::TBitmap *bmp;
    __fastcall TEmuWnd(TComponent* Owner);
    void __fastcall OnIdle(TObject *,bool&);
    void __fastcall Make();
    void __fastcall OnSkinSelect(TObject *Sender);
    void __fastcall OnMinimize(TObject *Sender);
    void __fastcall OnRestore(TObject *Sender);
protected:
    void __fastcall FormKeyDown(TMessage &Msg);
    void __fastcall FormKeyUp(TMessage &Msg);
    void __fastcall WMSizing(TMessage &Msg);
BEGIN_MESSAGE_MAP
    MESSAGE_HANDLER(WM_KEYDOWN,TMessage,FormKeyDown)
    MESSAGE_HANDLER(WM_KEYUP,TMessage,FormKeyUp)
    MESSAGE_HANDLER(WM_SIZING,TMessage,WMSizing)
END_MESSAGE_MAP(TForm)
};
//---------------------------------------------------------------------------
extern PACKAGE TEmuWnd *EmuWnd;
extern HWND otherWnd;
//---------------------------------------------------------------------------
#endif
