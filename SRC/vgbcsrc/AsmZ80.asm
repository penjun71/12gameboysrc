	.486p
	model flat

_DATA segment dword public use32 'DATA'
 _regAF dw 0
 _regBC dw 0
 _regDE dw 0
 _regHL dw 0
 _regSP dw 0
 _regPC dw 0
 memVal dw 0
 _nFlag db 0
 _imeFlag db 0
 clkLeft dw 0
 divClk dw 0
 timerClk dw 0
 _lcdClk dw 0
 _lcdUpdateCount dw 0
 _oldPC dw 0
 _halted db 0
 _minTime dw 0
 _intReq2 dw 0
 _intReq3 dw 0
_DATA ends

_TEXT segment dword public use32 'CODE'
    extern _getmemfunc
    extern _putmemfunc
    extern @RenderLine
    extern @RedrawLCD
    extern @DebugChecks
    extern _mem
    extern _oam
    extern _hiRam
    extern _dmaClk
    extern _timerClkCount
    extern _intReq
    extern _lcdMode
    extern _lcdY
    extern _lcdUpdateFreq
    extern _clkMult
    extern _debugCheckEnable
    extern _run
    extern _endStepOver
    extern _ignoreStack
    extern _gdmaClk
    extern _displayEnabled

get_opbyte:
;    cmp [_regPC],0fe00h
;    jae gob_noDirectRead
;    cmp byte ptr [_lcdMode],3
;    jz gob_callmem
;    cmp byte ptr [_dmaClk],0
;    jne gob_callmem
    movzx esi,[_regPC]
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz gob_callmem
    push ax
    mov ah,[esi+ebp]
    mov byte ptr [memVal],ah
    pop ax
gob_end:
    inc [_regPC]
    ret
gob_noDirectRead:
    cmp [_regPC],0fea0h
    jb gob_oam
    cmp [_regPC],0ff80h
    jae gob_hi
gob_callmem:
    pusha
    mov ax,[_regPC]
    mov esi,[_getmemfunc]
    db 0ffh,0d6h ;call esi
    mov byte ptr [memVal],al
    popa
    inc [_regPC]
    ret
gob_oam:
    mov si,[_regPC]
    and esi,0ffh
    push ax
    mov ah,[esi+offset _oam]
    mov byte ptr [memVal],ah
    pop ax
    inc [_regPC]
    ret
gob_hi:
    mov si,[_regPC]
    and esi,0ffh
    push ax
    mov ah,[esi+offset _hiRam]
    mov byte ptr [memVal],ah
    pop ax
    inc [_regPC]
    ret

get_opword:
    inc [_regPC]
    call get_opbyte
    shl [memVal],8
    sub [_regPC],2
    call get_opbyte
    inc [_regPC]
    ret

get_bc:
;    cmp bx,0fe00h
;    jae gbc_noDirectRead
;    cmp byte ptr [_lcdMode],3
;    jz gbc_callmem
;    cmp byte ptr [_dmaClk],0
;    jne gbc_callmem
    movzx esi,bx
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz gbc_callmem
    push ax
    mov ah,[esi+ebp]
    mov byte ptr [memVal],ah
    pop ax
gbc_end:
    ret
gbc_noDirectRead:
    cmp bx,0fea0h
    jb gbc_oam
    cmp bx,0ff80h
    jae gbc_hi
gbc_callmem:
    pusha
    mov ax,bx
    mov esi,[_getmemfunc]
    db 0ffh,0d6h ;call esi
    mov byte ptr [memVal],al
    popa
    ret
gbc_oam:
    mov si,bx
    and esi,0ffh
    push ax
    mov ah,[esi+offset _oam]
    mov byte ptr [memVal],ah
    pop ax
    ret
gbc_hi:
    mov si,bx
    and esi,0ffh
    push ax
    mov ah,[esi+offset _hiRam]
    mov byte ptr [memVal],ah
    pop ax
    ret

get_de:
;    cmp cx,0fe00h
;    jae gde_noDirectRead
;    cmp byte ptr [_lcdMode],3
;    jz gde_callmem
;    cmp byte ptr [_dmaClk],0
;    jne gde_callmem
    movzx esi,cx
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz gde_callmem
    push ax
    mov ah,[esi+ebp]
    mov byte ptr [memVal],ah
    pop ax
gde_end:
    ret
gde_noDirectRead:
    cmp cx,0fea0h
    jb gde_oam
    cmp cx,0ff80h
    jae gde_hi
gde_callmem:
    pusha
    mov ax,cx
    mov esi,[_getmemfunc]
    db 0ffh,0d6h ;call esi
    mov byte ptr [memVal],al
    popa
    ret
gde_oam:
    mov si,cx
    and esi,0ffh
    push ax
    mov ah,[esi+offset _oam]
    mov byte ptr [memVal],ah
    pop ax
    ret
gde_hi:
    mov si,cx
    and esi,0ffh
    push ax
    mov ah,[esi+offset _hiRam]
    mov byte ptr [memVal],ah
    pop ax
    ret

get_hl:
;    cmp dx,0fe00h
;    jae ghl_noDirectRead
;    cmp byte ptr [_lcdMode],3
;    jz ghl_callmem
;    cmp byte ptr [_dmaClk],0
;    jne ghl_callmem
    movzx esi,dx
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz ghl_callmem
    push ax
    mov ah,[esi+ebp]
    mov byte ptr [memVal],ah
    pop ax
ghl_end:
    ret
ghl_noDirectRead:
    cmp dx,0fea0h
    jb ghl_oam
    cmp dx,0ff80h
    jae ghl_hi
ghl_callmem:
    pusha
    mov ax,dx
    mov esi,[_getmemfunc]
    db 0ffh,0d6h ;call esi
    mov byte ptr [memVal],al
    popa
    ret
ghl_oam:
    mov si,dx
    and esi,0ffh
    push ax
    mov ah,[esi+offset _oam]
    mov byte ptr [memVal],ah
    pop ax
    ret
ghl_hi:
    mov si,dx
    and esi,0ffh
    push ax
    mov ah,[esi+offset _hiRam]
    mov byte ptr [memVal],ah
    pop ax
    ret

get_si:
;    cmp si,0fe00h
;    jae gsi_noDirectRead
;    cmp byte ptr [_lcdMode],3
;    jz gsi_callmem
;    cmp byte ptr [_dmaClk],0
;    jne gsi_callmem
    push esi
    movzx esi,si
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz gsi_end
    push ax
    mov ah,[esi+ebp]
    mov byte ptr [memVal],ah
    pop ax
    pop esi
    ret
gsi_end:
    pop esi
    jmp gsi_callmem
gsi_noDirectRead:
    cmp si,0fea0h
    jb gsi_oam
    cmp si,0ff80h
    jae gsi_hi
gsi_callmem:
    pusha
    mov ax,si
    mov esi,[_getmemfunc]
    db 0ffh,0d6h ;call esi
    mov byte ptr [memVal],al
    popa
    ret
gsi_oam:
    and esi,0ffh
    push ax
    mov ah,[esi+offset _oam]
    mov byte ptr [memVal],ah
    pop ax
    ret
gsi_hi:
    and esi,0ffh
    push ax
    mov ah,[esi+offset _hiRam]
    mov byte ptr [memVal],ah
    pop ax
    ret

get_spword:
    lea esi,[edi+1]
    call get_si
    shl [memVal],8
    mov esi,edi
    call get_si
    ret

put_bc:
    cmp bx,8000h
    jb pbc_noDirectWrite
    cmp bx,0fe00h
    jae pbc_noDirectWrite
    cmp byte ptr [_lcdMode],3
    jz pbc_noDirectWrite
    cmp byte ptr [_dmaClk],0
    jne pbc_noDirectWrite
    movzx esi,bx
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz pbc_end
    push ax
    mov ah,byte ptr [memVal]
    mov [esi+ebp],ah
    pop ax
pbc_end:
    ret
pbc_noDirectWrite:
    pusha
    mov ax,bx
    mov dl,byte ptr [memVal]
    mov esi,[_putmemfunc]
    db 0ffh,0d6h ;call esi
    popa
    ret

put_de:
    cmp cx,8000h
    jb pde_noDirectWrite
    cmp cx,0fe00h
    jae pde_noDirectWrite
    cmp byte ptr [_lcdMode],3
    jz pde_noDirectWrite
    cmp byte ptr [_dmaClk],0
    jne pde_noDirectWrite
    movzx esi,cx
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz pde_end
    push ax
    mov ah,byte ptr [memVal]
    mov [esi+ebp],ah
    pop ax
pde_end:
    ret
pde_noDirectWrite:
    pusha
    mov ax,cx
    mov dl,byte ptr [memVal]
    mov esi,[_putmemfunc]
    db 0ffh,0d6h ;call esi
    popa
    ret

put_hl:
    cmp dx,8000h
    jb phl_noDirectWrite
    cmp dx,0fe00h
    jae phl_noDirectWrite
    cmp byte ptr [_lcdMode],3
    jz phl_noDirectWrite
    cmp byte ptr [_dmaClk],0
    jne phl_noDirectWrite
    movzx esi,dx
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz phl_end
    push ax
    mov ah,byte ptr [memVal]
    mov [esi+ebp],ah
    pop ax
phl_end:
    ret
phl_noDirectWrite:
    pusha
    mov ax,dx
    mov dl,byte ptr [memVal]
    mov esi,[_putmemfunc]
    db 0ffh,0d6h ;call esi
    popa
    ret

put_si:
    cmp si,8000h
    jb psi_noDirectWrite
    cmp si,0fe00h
    jae psi_noDirectWrite
    cmp byte ptr [_lcdMode],3
    jz psi_noDirectWrite
    cmp byte ptr [_dmaClk],0
    jne psi_noDirectWrite
    movzx esi,si
    mov ebp,esi
    shr esi,12
    and ebp,0fffh
    mov esi,[esi*4+offset _mem]
    or esi,esi
    jz psi_end
    push ax
    mov ah,byte ptr [memVal]
    mov [esi+ebp],ah
    pop ax
psi_end:
    ret
psi_noDirectWrite:
    pusha
    mov ax,si
    mov dl,byte ptr [memVal]
    mov esi,[_putmemfunc]
    db 0ffh,0d6h ;call esi
    popa
    ret

put_spword:
    mov si,di
    call put_si
    lea esi,[edi+1]
    shr [memVal],8
    call put_si
    ret

nop:
    jmp next

adc_a_b:
    sahf
    adc al,bh
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_c:
    sahf
    adc al,bl
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_d:
    sahf
    adc al,ch
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_e:
    sahf
    adc al,cl
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_h:
    sahf
    adc al,dh
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_l:
    sahf
    adc al,dl
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_xhl:
    call get_hl
    sahf
    adc al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],0
    jmp next
adc_a_a:
    sahf
    adc al,al
    lahf
    mov byte ptr [_nFlag],0
    jmp next

adc_a_byte:
    call get_opbyte
    sahf
    adc al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],0
    jmp next

add_hl_bc:
    add dx,bx
    push bx
    mov bl,ah
    lahf
    and bl,40h
    and ah,0bfh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next

add_hl_de:
    add dx,cx
    push bx
    mov bl,ah
    lahf
    and bl,40h
    and ah,0bfh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next

add_hl_hl:
    add dx,dx
    push bx
    mov bl,ah
    lahf
    and bl,40h
    and ah,0bfh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next

add_hl_sp:
    add dx,di
    push bx
    mov bl,ah
    lahf
    and bl,40h
    and ah,0bfh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next

add_a_b:
    add al,bh
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_c:
    add al,bl
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_d:
    add al,ch
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_e:
    add al,cl
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_h:
    add al,dh
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_l:
    add al,dl
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_xhl:
    call get_hl
    add al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],0
    jmp next
add_a_a:
    add al,al
    lahf
    mov byte ptr [_nFlag],0
    jmp next

add_a_byte:
    call get_opbyte
    add al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],0
    jmp next

add_sp_disp:
    call get_opbyte
    movsx si,byte ptr [memVal]
    add di,si
    lahf
    and ah,0bfh
    mov byte ptr [_nFlag],0
    jmp next

and_a_b:
    and al,bh
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_c:
    and al,bl
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_d:
    and al,ch
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_e:
    and al,cl
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_h:
    and al,dh
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_l:
    and al,dl
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_xhl:
    call get_hl
    and al,byte ptr [memVal]
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next
and_a_a:
    and al,al
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next

and_byte:
    call get_opbyte
    and al,byte ptr [memVal]
    lahf
    and ah,40h
    or ah,10h
    mov byte ptr [_nFlag],0
    jmp next

bit_0_b:
    test bh,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_0_c:
    test bl,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_0_d:
    test ch,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_0_e:
    test cl,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_0_h:
    test dh,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_0_l:
    test dl,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_0_xhl:
    call get_hl
    test byte ptr [memVal],1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_0_a:
    test al,1
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_b:
    test bh,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_c:
    test bl,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_d:
    test ch,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_e:
    test cl,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_h:
    test dh,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_l:
    test dl,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_1_xhl:
    call get_hl
    test byte ptr [memVal],2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_1_a:
    test al,2
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_b:
    test bh,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_c:
    test bl,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_d:
    test ch,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_e:
    test cl,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_h:
    test dh,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_l:
    test dl,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_2_xhl:
    call get_hl
    test byte ptr [memVal],4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_2_a:
    test al,4
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_b:
    test bh,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_c:
    test bl,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_d:
    test ch,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_e:
    test cl,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_h:
    test dh,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_l:
    test dl,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_3_xhl:
    call get_hl
    test byte ptr [memVal],8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_3_a:
    test al,8
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_b:
    test bh,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_c:
    test bl,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_d:
    test ch,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_e:
    test cl,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_h:
    test dh,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_l:
    test dl,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_4_xhl:
    call get_hl
    test byte ptr [memVal],16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_4_a:
    test al,16
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_b:
    test bh,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_c:
    test bl,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_d:
    test ch,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_e:
    test cl,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_h:
    test dh,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_l:
    test dl,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_5_xhl:
    call get_hl
    test byte ptr [memVal],32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_5_a:
    test al,32
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_b:
    test bh,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_c:
    test bl,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_d:
    test ch,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_e:
    test cl,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_h:
    test dh,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_l:
    test dl,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_6_xhl:
    call get_hl
    test byte ptr [memVal],64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_6_a:
    test al,64
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_b:
    test bh,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_c:
    test bl,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_d:
    test ch,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_e:
    test cl,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_h:
    test dh,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_l:
    test dl,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
bit_7_xhl:
    call get_hl
    test byte ptr [memVal],128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
bit_7_a:
    test al,128
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,040h
    or ah,bl
    or ah,10h
    pop bx
    mov byte ptr [_nFlag],0
    jmp next

call_nz_word:
    call get_opword
    sahf
    jz next
    mov si,[memVal]
    push si
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    pop si
    mov [_regPC],si
    sub [clkLeft],7
    mov byte ptr [_ignoreStack],0
    jmp next

call_z_word:
    call get_opword
    sahf
    jnz next
    mov si,[memVal]
    push si
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    pop si
    mov [_regPC],si
    sub [clkLeft],7
    mov byte ptr [_ignoreStack],0
    jmp next

call_word:
    mov si,[_regPC]
    add si,2
    mov [memVal],si
    sub di,2
    call put_spword
    call get_opword
    mov si,[memVal]
    mov [_regPC],si
    sub [clkLeft],7
    mov byte ptr [_ignoreStack],0
    jmp next

call_nc_word:
    call get_opword
    sahf
    jc next
    mov si,[memVal]
    push si
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    pop si
    mov [_regPC],si
    sub [clkLeft],7
    mov byte ptr [_ignoreStack],0
    jmp next

call_c_word:
    call get_opword
    sahf
    jnc next
    mov si,[memVal]
    push si
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    pop si
    mov [_regPC],si
    sub [clkLeft],7
    mov byte ptr [_ignoreStack],0
    jmp next

ccf:
    and ah,0efh
    xor ah,1
    mov byte ptr [_nFlag],0
    jmp next

cp_a_b:
    cmp al,bh
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_c:
    cmp al,bl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_d:
    cmp al,ch
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_e:
    cmp al,cl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_h:
    cmp al,dh
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_l:
    cmp al,dl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_xhl:
    call get_hl
    cmp al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],1
    jmp next
cp_a_a:
    cmp al,al
    lahf
    mov byte ptr [_nFlag],1
    jmp next

cp_byte:
    call get_opbyte
    cmp al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],1
    jmp next

cpl:
    not al
    or ah,10h
    mov byte ptr [_nFlag],1
    jmp next

daa:
    cmp byte ptr [_nFlag],0
    jnz das
    sahf
    daa
    lahf
    jmp next
das:
    sahf
    das
    lahf
    jmp next

dec_b:
    dec bh
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next
dec_bc:
    dec bx
    jmp next
dec_c:
    dec bl
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next
dec_d:
    dec ch
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next
dec_de:
    dec cx
    jmp next
dec_e:
    dec cl
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next
dec_h:
    dec dh
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next
dec_hl:
    dec dx
    jmp next
dec_l:
    dec dl
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next
dec_xhl:
    call get_hl
    dec byte ptr [memVal]
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    call put_hl
    jmp next
dec_sp:
    dec di
    jmp next
dec_a:
    dec al
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],1
    jmp next

di_:
    mov byte ptr [_imeFlag],0
    jmp next

ei:
    cmp byte ptr [_imeFlag],1
    jz next
    pusha
    mov byte ptr [_imeFlag],1
    mov word ptr [_intReq],0
    popa
    jmp execloop2

halt:
    cmp byte ptr [_imeFlag],0
    jz next
    mov byte ptr [_halted],1
    dec word ptr [_regPC]
    jmp next

inc_bc:
    inc bx
    jmp next
inc_b:
    inc bh
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
inc_c:
    inc bl
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
inc_de:
    inc cx
    jmp next
inc_d:
    inc ch
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
inc_e:
    inc cl
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
inc_hl:
    inc dx
    jmp next
inc_h:
    inc dh
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
inc_l:
    inc dl
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next
inc_sp:
    inc di
    jmp next
inc_xhl:
    call get_hl
    inc byte ptr [memVal]
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
inc_a:
    inc al
    push bx
    mov bl,ah
    lahf
    and bl,1
    and ah,0feh
    or ah,bl
    pop bx
    mov byte ptr [_nFlag],0
    jmp next

jp_nz_word:
    call get_opword
    sahf
    jz next
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_endStepOver],1
    jmp next

jp_word:
    call get_opword
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_endStepOver],1
    jmp next

jp_z_word:
    call get_opword
    sahf
    jnz next
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_endStepOver],1
    jmp next

jp_nc_word:
    call get_opword
    sahf
    jc next
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_endStepOver],1
    jmp next

jp_c_word:
    call get_opword
    sahf
    jnc next
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_endStepOver],1
    jmp next

jp_hl:
    mov [_regPC],dx
    mov byte ptr [_endStepOver],1
    jmp next

jr_disp:
    call get_opbyte
    movsx si,byte ptr [memVal]
    add [_regPC],si
    sub [clkLeft],5
    mov byte ptr [_endStepOver],1
    jmp next

jr_nz_disp:
    call get_opbyte
    sahf
    jz next
    movsx si,byte ptr [memVal]
    add [_regPC],si
    sub [clkLeft],5
    mov byte ptr [_endStepOver],1
    jmp next

jr_z_disp:
    call get_opbyte
    sahf
    jnz next
    movsx si,byte ptr [memVal]
    add [_regPC],si
    sub [clkLeft],5
    mov byte ptr [_endStepOver],1
    jmp next

jr_nc_disp:
    call get_opbyte
    sahf
    jc next
    movsx si,byte ptr [memVal]
    add [_regPC],si
    sub [clkLeft],5
    mov byte ptr [_endStepOver],1
    jmp next

jr_c_disp:
    call get_opbyte
    sahf
    jnc next
    movsx si,byte ptr [memVal]
    add [_regPC],si
    sub [clkLeft],5
    mov byte ptr [_endStepOver],1
    jmp next

ld_bc_word:
    call get_opword
    mov bx,[memVal]
    jmp next

ld_xbc_a:
    mov byte ptr [memVal],al
    call put_bc
    jmp next

ld_b_byte:
    call get_opbyte
    mov bh,byte ptr [memVal]
    jmp next

ld_xword_sp:
    call get_opword
    mov si,[memVal]
    mov [memVal],di
    push si
    call put_si
    shr word ptr [memVal],8
    pop si
    inc si
    call put_si
    jmp next

ld_a_xbc:
    call get_bc
    mov al,byte ptr [memVal]
    jmp next

ld_c_byte:
    call get_opbyte
    mov bl,byte ptr [memVal]
    jmp next

ld_de_word:
    call get_opword
    mov cx,[memVal]
    jmp next

ld_xde_a:
    mov byte ptr [memVal],al
    call put_de
    jmp next

ld_d_byte:
    call get_opbyte
    mov ch,byte ptr [memVal]
    jmp next

ld_a_xde:
    call get_de
    mov al,byte ptr [memVal]
    jmp next

ld_e_byte:
    call get_opbyte
    mov cl,byte ptr [memVal]
    jmp next

ld_hl_word:
    call get_opword
    mov dx,[memVal]
    jmp next

ldi_xhl_a:
    mov byte ptr [memVal],al
    call put_hl
    inc dx
    jmp next

ld_h_byte:
    call get_opbyte
    mov dh,byte ptr [memVal]
    jmp next

ldi_a_xhl:
    call get_hl
    mov al,byte ptr [memVal]
    inc dx
    jmp next

ld_l_byte:
    call get_opbyte
    mov dl,byte ptr [memVal]
    jmp next

ld_sp_word:
    call get_opword
    mov di,[memVal]
    jmp next

ldd_xhl_a:
    mov byte ptr [memVal],al
    call put_hl
    dec dx
    jmp next

ld_xhl_byte:
    call get_opbyte
    call put_hl
    jmp next

ldd_a_xhl:
    call get_hl
    mov al,byte ptr [memVal]
    dec dx
    jmp next

ld_a_byte:
    call get_opbyte
    mov al,byte ptr [memVal]
    jmp next

ld_b_b:
    jmp next
ld_b_c:
    mov bh,bl
    jmp next
ld_b_d:
    mov bh,ch
    jmp next
ld_b_e:
    mov bh,cl
    jmp next
ld_b_h:
    mov bh,dh
    jmp next
ld_b_l:
    mov bh,dl
    jmp next
ld_b_xhl:
    call get_hl
    mov bh,byte ptr [memVal]
    jmp next
ld_b_a:
    mov bh,al
    jmp next

ld_c_b:
    mov bl,bh
    jmp next
ld_c_c:
    jmp next
ld_c_d:
    mov bl,ch
    jmp next
ld_c_e:
    mov bl,cl
    jmp next
ld_c_h:
    mov bl,dh
    jmp next
ld_c_l:
    mov bl,dl
    jmp next
ld_c_xhl:
    call get_hl
    mov bl,byte ptr [memVal]
    jmp next
ld_c_a:
    mov bl,al
    jmp next

ld_d_b:
    mov ch,bh
    jmp next
ld_d_c:
    mov ch,bl
    jmp next
ld_d_d:
    jmp next
ld_d_e:
    mov ch,cl
    jmp next
ld_d_h:
    mov ch,dh
    jmp next
ld_d_l:
    mov ch,dl
    jmp next
ld_d_xhl:
    call get_hl
    mov ch,byte ptr [memVal]
    jmp next
ld_d_a:
    mov ch,al
    jmp next

ld_e_b:
    mov cl,bh
    jmp next
ld_e_c:
    mov cl,bl
    jmp next
ld_e_d:
    mov cl,ch
    jmp next
ld_e_e:
    jmp next
ld_e_h:
    mov cl,dh
    jmp next
ld_e_l:
    mov cl,dl
    jmp next
ld_e_xhl:
    call get_hl
    mov cl,byte ptr [memVal]
    jmp next
ld_e_a:
    mov cl,al
    jmp next

ld_h_b:
    mov dh,bh
    jmp next
ld_h_c:
    mov dh,bl
    jmp next
ld_h_d:
    mov dh,ch
    jmp next
ld_h_e:
    mov dh,cl
    jmp next
ld_h_h:
    jmp next
ld_h_l:
    mov dh,dl
    jmp next
ld_h_xhl:
    call get_hl
    mov dh,byte ptr [memVal]
    jmp next
ld_h_a:
    mov dh,al
    jmp next

ld_l_b:
    mov dl,bh
    jmp next
ld_l_c:
    mov dl,bl
    jmp next
ld_l_d:
    mov dl,ch
    jmp next
ld_l_e:
    mov dl,cl
    jmp next
ld_l_h:
    mov dl,dh
    jmp next
ld_l_l:
    jmp next
ld_l_xhl:
    call get_hl
    mov dl,byte ptr [memVal]
    jmp next
ld_l_a:
    mov dl,al
    jmp next

ld_xhl_b:
    mov byte ptr [memVal],bh
    call put_hl
    jmp next
ld_xhl_c:
    mov byte ptr [memVal],bl
    call put_hl
    jmp next
ld_xhl_d:
    mov byte ptr [memVal],ch
    call put_hl
    jmp next
ld_xhl_e:
    mov byte ptr [memVal],cl
    call put_hl
    jmp next
ld_xhl_h:
    mov byte ptr [memVal],dh
    call put_hl
    jmp next
ld_xhl_l:
    mov byte ptr [memVal],dl
    call put_hl
    jmp next
ld_xhl_a:
    mov byte ptr [memVal],al
    call put_hl
    jmp next

ld_a_b:
    mov al,bh
    jmp next
ld_a_c:
    mov al,bl
    jmp next
ld_a_d:
    mov al,ch
    jmp next
ld_a_e:
    mov al,cl
    jmp next
ld_a_h:
    mov al,dh
    jmp next
ld_a_l:
    mov al,dl
    jmp next
ld_a_xhl:
    call get_hl
    mov al,byte ptr [memVal]
    jmp next
ld_a_a:
    jmp next

ld_xword_a:
    call get_opword
    mov si,[memVal]
    mov byte ptr [memVal],al
    call put_si
    jmp next

ld_hl_spdisp:
    call get_opbyte
    movsx dx,byte ptr [memVal]
    add dx,di
    lahf
    and ah,11h
    mov byte ptr [_nFlag],0
    jmp next

ld_sp_hl:
    mov di,dx
    jmp next

ld_a_xword:
    call get_opword
    mov si,[memVal]
    call get_si
    mov al,byte ptr [memVal]
    jmp next

ldh_xbyte_a:
    call get_opbyte
    movzx si,byte ptr [memVal]
    add si,0ff00h
    mov byte ptr [memVal],al
    call put_si
    jmp next

ldh_xc_a:
    movzx si,bl
    add si,0ff00h
    mov byte ptr [memVal],al
    call put_si
    jmp next

ldh_a_xbyte:
    call get_opbyte
    movzx si,byte ptr [memVal]
    add si,0ff00h
    call get_si
    mov al,byte ptr [memVal]
    jmp next

ldh_a_xc:
    movzx si,bl
    add si,0ff00h
    call get_si
    mov al,byte ptr [memVal]
    jmp next

or_a_b:
    or al,bh
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_c:
    or al,bl
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_d:
    or al,ch
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_e:
    or al,cl
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_h:
    or al,dh
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_l:
    or al,dl
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_xhl:
    call get_hl
    or al,byte ptr [memVal]
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
or_a_a:
    or al,al
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next

or_byte:
    call get_opbyte
    or al,byte ptr [memVal]
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next

pop_af:
    call get_spword
    add di,2
    mov ah,byte ptr [memVal]
    mov al,ah
    shr ah,1
    shr al,4
    and al,1
    and ah,070h
    or ah,al
    mov al,ah
    shr al,5
    and al,1
;    and al,2
;    shr al,1
    mov byte ptr [_nFlag],al
    mov al,byte ptr [memVal+1]
    jmp next

pop_bc:
    call get_spword
    add di,2
    mov bx,[memVal]
    jmp next

pop_de:
    call get_spword
    add di,2
    mov cx,[memVal]
    jmp next

pop_hl:
    call get_spword
    add di,2
    mov dx,[memVal]
    jmp next

push_bc:
    mov [memVal],bx
    sub di,2
    call put_spword
    jmp next

push_de:
    mov [memVal],cx
    sub di,2
    call put_spword
    jmp next

push_hl:
    mov [memVal],dx
    sub di,2
    call put_spword
    jmp next

push_af:
    push ax
    mov byte ptr [memVal+1],al
    mov al,ah
    shl ah,1
    and ah,0a0h
    shl al,4
    and al,10h
    or ah,al
    mov al,byte ptr [_nFlag]
    shl al,6
    or ah,al
    mov byte ptr [memVal],ah
    pop ax
    sub di,2
    call put_spword
    jmp next

res_0_b:
    btr bx,8
    jmp next
res_0_c:
    btr bx,0
    jmp next
res_0_d:
    btr cx,8
    jmp next
res_0_e:
    btr cx,0
    jmp next
res_0_h:
    btr dx,8
    jmp next
res_0_l:
    btr dx,0
    jmp next
res_0_xhl:
    call get_hl
    btr [memVal],0
    call put_hl
    jmp next
res_0_a:
    btr ax,0
    jmp next
res_1_b:
    btr bx,9
    jmp next
res_1_c:
    btr bx,1
    jmp next
res_1_d:
    btr cx,9
    jmp next
res_1_e:
    btr cx,1
    jmp next
res_1_h:
    btr dx,9
    jmp next
res_1_l:
    btr dx,1
    jmp next
res_1_xhl:
    call get_hl
    btr [memVal],1
    call put_hl
    jmp next
res_1_a:
    btr ax,1
    jmp next
res_2_b:
    btr bx,10
    jmp next
res_2_c:
    btr bx,2
    jmp next
res_2_d:
    btr cx,10
    jmp next
res_2_e:
    btr cx,2
    jmp next
res_2_h:
    btr dx,10
    jmp next
res_2_l:
    btr dx,2
    jmp next
res_2_xhl:
    call get_hl
    btr [memVal],2
    call put_hl
    jmp next
res_2_a:
    btr ax,2
    jmp next
res_3_b:
    btr bx,11
    jmp next
res_3_c:
    btr bx,3
    jmp next
res_3_d:
    btr cx,11
    jmp next
res_3_e:
    btr cx,3
    jmp next
res_3_h:
    btr dx,11
    jmp next
res_3_l:
    btr dx,3
    jmp next
res_3_xhl:
    call get_hl
    btr [memVal],3
    call put_hl
    jmp next
res_3_a:
    btr ax,3
    jmp next
res_4_b:
    btr bx,12
    jmp next
res_4_c:
    btr bx,4
    jmp next
res_4_d:
    btr cx,12
    jmp next
res_4_e:
    btr cx,4
    jmp next
res_4_h:
    btr dx,12
    jmp next
res_4_l:
    btr dx,4
    jmp next
res_4_xhl:
    call get_hl
    btr [memVal],4
    call put_hl
    jmp next
res_4_a:
    btr ax,4
    jmp next
res_5_b:
    btr bx,13
    jmp next
res_5_c:
    btr bx,5
    jmp next
res_5_d:
    btr cx,13
    jmp next
res_5_e:
    btr cx,5
    jmp next
res_5_h:
    btr dx,13
    jmp next
res_5_l:
    btr dx,5
    jmp next
res_5_xhl:
    call get_hl
    btr [memVal],5
    call put_hl
    jmp next
res_5_a:
    btr ax,5
    jmp next
res_6_b:
    btr bx,14
    jmp next
res_6_c:
    btr bx,6
    jmp next
res_6_d:
    btr cx,14
    jmp next
res_6_e:
    btr cx,6
    jmp next
res_6_h:
    btr dx,14
    jmp next
res_6_l:
    btr dx,6
    jmp next
res_6_xhl:
    call get_hl
    btr [memVal],6
    call put_hl
    jmp next
res_6_a:
    btr ax,6
    jmp next
res_7_b:
    btr bx,15
    jmp next
res_7_c:
    btr bx,7
    jmp next
res_7_d:
    btr cx,15
    jmp next
res_7_e:
    btr cx,7
    jmp next
res_7_h:
    btr dx,15
    jmp next
res_7_l:
    btr dx,7
    jmp next
res_7_xhl:
    call get_hl
    btr [memVal],7
    call put_hl
    jmp next
res_7_a:
    btr ax,7
    jmp next

ret:
    call get_spword
    add di,2
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_endStepOver],1
    jmp next

ret_nz:
    sahf
    jz next
    call get_spword
    add di,2
    mov si,[memVal]
    mov [_regPC],si
    jmp next

ret_z:
    sahf
    jnz next
    call get_spword
    add di,2
    mov si,[memVal]
    mov [_regPC],si
    jmp next

ret_nc:
    sahf
    jc next
    call get_spword
    add di,2
    mov si,[memVal]
    mov [_regPC],si
    jmp next

ret_c:
    sahf
    jnc next
    call get_spword
    add di,2
    mov si,[memVal]
    mov [_regPC],si
    jmp next

reti:
    call get_spword
    add di,2
    mov si,[memVal]
    mov [_regPC],si
    mov byte ptr [_imeFlag],1
    mov byte ptr [_endStepOver],1
    jmp next

rla:
    sahf
    rcl al,1
    lahf
    and ah,1
    mov byte ptr [_nFlag],0
    jmp next

rra:
    sahf
    rcr al,1
    lahf
    and ah,1
    mov byte ptr [_nFlag],0
    jmp next

rlca:
    rol al,1
    lahf
    and ah,1
    mov byte ptr [_nFlag],0
    jmp next

rrca:
    ror al,1
    lahf
    and ah,1
    mov byte ptr [_nFlag],0
    jmp next

rl_b:
    sahf
    rcl bh,1
    pushf
    cmp bh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rl_c:
    sahf
    rcl bl,1
    pushf
    cmp bl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rl_d:
    sahf
    rcl ch,1
    pushf
    cmp ch,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rl_e:
    sahf
    rcl cl,1
    pushf
    cmp cl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rl_h:
    sahf
    rcl dh,1
    pushf
    cmp dh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rl_l:
    sahf
    rcl dl,1
    pushf
    cmp dl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rl_xhl:
    call get_hl
    sahf
    rcl byte ptr [memVal],1
    pushf
    cmp byte ptr [memVal],0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
rl_a:
    sahf
    rcl al,1
    pushf
    cmp al,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next

rr_b:
    sahf
    rcr bh,1
    pushf
    cmp bh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rr_c:
    sahf
    rcr bl,1
    pushf
    cmp bl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rr_d:
    sahf
    rcr ch,1
    pushf
    cmp ch,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rr_e:
    sahf
    rcr cl,1
    pushf
    cmp cl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rr_h:
    sahf
    rcr dh,1
    pushf
    cmp dh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rr_l:
    sahf
    rcr dl,1
    pushf
    cmp dl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rr_xhl:
    call get_hl
    sahf
    rcr byte ptr [memVal],1
    pushf
    cmp byte ptr [memVal],0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
rr_a:
    sahf
    rcr al,1
    pushf
    cmp al,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next

rlc_b:
    rol bh,1
    pushf
    cmp bh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rlc_c:
    rol bl,1
    pushf
    cmp bl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rlc_d:
    rol ch,1
    pushf
    cmp ch,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rlc_e:
    rol cl,1
    pushf
    cmp cl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rlc_h:
    rol dh,1
    pushf
    cmp dh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rlc_l:
    rol dl,1
    pushf
    cmp dl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rlc_xhl:
    call get_hl
    rol byte ptr [memVal],1
    pushf
    cmp byte ptr [memVal],0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
rlc_a:
    rol al,1
    pushf
    cmp al,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next

rrc_b:
    ror bh,1
    pushf
    cmp bh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rrc_c:
    ror bl,1
    pushf
    cmp bl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rrc_d:
    ror ch,1
    pushf
    cmp ch,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rrc_e:
    ror cl,1
    pushf
    cmp cl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rrc_h:
    ror dh,1
    pushf
    cmp dh,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rrc_l:
    ror dl,1
    pushf
    cmp dl,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next
rrc_xhl:
    call get_hl
    ror byte ptr [memVal],1
    pushf
    cmp byte ptr [memVal],0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
rrc_a:
    ror al,1
    pushf
    cmp al,0
    lahf
    and ah,40h
    popf
    adc ah,0
    mov byte ptr [_nFlag],0
    jmp next

rst_00:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],0
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_08:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],8
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_10:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],10h
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_18:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],18h
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_20:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],20h
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_28:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],28h
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_30:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],30h
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

rst_38:
    mov si,[_regPC]
    mov [memVal],si
    sub di,2
    call put_spword
    mov word ptr [_regPC],38h
    mov byte ptr [_endStepOver],1
    mov byte ptr [_ignoreStack],0
    jmp next

sbc_a_b:
    sahf
    sbb al,bh
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_c:
    sahf
    sbb al,bl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_d:
    sahf
    sbb al,ch
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_e:
    sahf
    sbb al,cl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_h:
    sahf
    sbb al,dh
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_l:
    sahf
    sbb al,dl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_xhl:
    call get_hl
    sahf
    sbb al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sbc_a_a:
    sahf
    sbb al,al
    lahf
    mov byte ptr [_nFlag],1
    jmp next

sbc_a_byte:
    call get_opbyte
    sahf
    sbb al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],1
    jmp next

scf:
    and ah,0efh
    or ah,1
    mov byte ptr [_nFlag],0
    jmp next

set_0_b:
    bts bx,8
    jmp next
set_0_c:
    bts bx,0
    jmp next
set_0_d:
    bts cx,8
    jmp next
set_0_e:
    bts cx,0
    jmp next
set_0_h:
    bts dx,8
    jmp next
set_0_l:
    bts dx,0
    jmp next
set_0_xhl:
    call get_hl
    bts [memVal],0
    call put_hl
    jmp next
set_0_a:
    bts ax,0
    jmp next
set_1_b:
    bts bx,9
    jmp next
set_1_c:
    bts bx,1
    jmp next
set_1_d:
    bts cx,9
    jmp next
set_1_e:
    bts cx,1
    jmp next
set_1_h:
    bts dx,9
    jmp next
set_1_l:
    bts dx,1
    jmp next
set_1_xhl:
    call get_hl
    bts [memVal],1
    call put_hl
    jmp next
set_1_a:
    bts ax,1
    jmp next
set_2_b:
    bts bx,10
    jmp next
set_2_c:
    bts bx,2
    jmp next
set_2_d:
    bts cx,10
    jmp next
set_2_e:
    bts cx,2
    jmp next
set_2_h:
    bts dx,10
    jmp next
set_2_l:
    bts dx,2
    jmp next
set_2_xhl:
    call get_hl
    bts [memVal],2
    call put_hl
    jmp next
set_2_a:
    bts ax,2
    jmp next
set_3_b:
    bts bx,11
    jmp next
set_3_c:
    bts bx,3
    jmp next
set_3_d:
    bts cx,11
    jmp next
set_3_e:
    bts cx,3
    jmp next
set_3_h:
    bts dx,11
    jmp next
set_3_l:
    bts dx,3
    jmp next
set_3_xhl:
    call get_hl
    bts [memVal],3
    call put_hl
    jmp next
set_3_a:
    bts ax,3
    jmp next
set_4_b:
    bts bx,12
    jmp next
set_4_c:
    bts bx,4
    jmp next
set_4_d:
    bts cx,12
    jmp next
set_4_e:
    bts cx,4
    jmp next
set_4_h:
    bts dx,12
    jmp next
set_4_l:
    bts dx,4
    jmp next
set_4_xhl:
    call get_hl
    bts [memVal],4
    call put_hl
    jmp next
set_4_a:
    bts ax,4
    jmp next
set_5_b:
    bts bx,13
    jmp next
set_5_c:
    bts bx,5
    jmp next
set_5_d:
    bts cx,13
    jmp next
set_5_e:
    bts cx,5
    jmp next
set_5_h:
    bts dx,13
    jmp next
set_5_l:
    bts dx,5
    jmp next
set_5_xhl:
    call get_hl
    bts [memVal],5
    call put_hl
    jmp next
set_5_a:
    bts ax,5
    jmp next
set_6_b:
    bts bx,14
    jmp next
set_6_c:
    bts bx,6
    jmp next
set_6_d:
    bts cx,14
    jmp next
set_6_e:
    bts cx,6
    jmp next
set_6_h:
    bts dx,14
    jmp next
set_6_l:
    bts dx,6
    jmp next
set_6_xhl:
    call get_hl
    bts [memVal],6
    call put_hl
    jmp next
set_6_a:
    bts ax,6
    jmp next
set_7_b:
    bts bx,15
    jmp next
set_7_c:
    bts bx,7
    jmp next
set_7_d:
    bts cx,15
    jmp next
set_7_e:
    bts cx,7
    jmp next
set_7_h:
    bts dx,15
    jmp next
set_7_l:
    bts dx,7
    jmp next
set_7_xhl:
    call get_hl
    bts [memVal],7
    call put_hl
    jmp next
set_7_a:
    bts ax,7
    jmp next

sla_b:
    shl bh,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sla_c:
    shl bl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sla_d:
    shl ch,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sla_e:
    shl cl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sla_h:
    shl dh,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sla_l:
    shl dl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sla_xhl:
    call get_hl
    shl byte ptr [memVal],1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
sla_a:
    shl al,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next

sra_b:
    sar bh,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sra_c:
    sar bl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sra_d:
    sar ch,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sra_e:
    sar cl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sra_h:
    sar dh,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sra_l:
    sar dl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
sra_xhl:
    call get_hl
    sar byte ptr [memVal],1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
sra_a:
    sar al,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next

srl_b:
    shr bh,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
srl_c:
    shr bl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
srl_d:
    shr ch,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
srl_e:
    shr cl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
srl_h:
    shr dh,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
srl_l:
    shr dl,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next
srl_xhl:
    call get_hl
    shr byte ptr [memVal],1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
srl_a:
    shr al,1
    lahf
    and ah,0efh
    mov byte ptr [_nFlag],0
    jmp next

stop:
    call get_opbyte
    test byte ptr [_hiRam+4dh],1
    jz next
    xor byte ptr [_hiRam+4dh],80h
    and byte ptr [_hiRam+4dh],80h
    test byte ptr [_hiRam+4dh],80h
    jz stop_SlowMode
    mov byte ptr [_clkMult],2
    jmp next
stop_SlowMode:
    mov byte ptr [_clkMult],1
    jmp next

sub_a_b:
    sub al,bh
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_c:
    sub al,bl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_d:
    sub al,ch
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_e:
    sub al,cl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_h:
    sub al,dh
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_l:
    sub al,dl
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_xhl:
    call get_hl
    sub al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],1
    jmp next
sub_a_a:
    sub al,al
    lahf
    mov byte ptr [_nFlag],1
    jmp next

sub_byte:
    call get_opbyte
    sub al,byte ptr [memVal]
    lahf
    mov byte ptr [_nFlag],1
    jmp next

swap_b:
    rol bh,4
    cmp bh,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
swap_c:
    rol bl,4
    cmp bl,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
swap_d:
    rol ch,4
    cmp ch,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
swap_e:
    rol cl,4
    cmp cl,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
swap_h:
    rol dh,4
    cmp dh,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
swap_l:
    rol dl,4
    cmp dl,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
swap_xhl:
    call get_hl
    rol byte ptr [memVal],4
    cmp byte ptr [memVal],0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    call put_hl
    jmp next
swap_a:
    rol al,4
    cmp al,0
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next

xor_a_b:
    xor al,bh
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_c:
    xor al,bl
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_d:
    xor al,ch
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_e:
    xor al,cl
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_h:
    xor al,dh
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_l:
    xor al,dl
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_xhl:
    call get_hl
    xor al,byte ptr [memVal]
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next
xor_a_a:
    xor al,al
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next

xor_byte:
    call get_opbyte
    xor al,byte ptr [memVal]
    lahf
    and ah,40h
    mov byte ptr [_nFlag],0
    jmp next

cb_cycle_table:
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,16,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8
 db 8,8,8,8,8,8,12,8

cb_table:
 dd rlc_b,rlc_c,rlc_d,rlc_e,rlc_h,rlc_l,rlc_xhl,rlc_a
 dd rrc_b,rrc_c,rrc_d,rrc_e,rrc_h,rrc_l,rrc_xhl,rrc_a
 dd rl_b,rl_c,rl_d,rl_e,rl_h,rl_l,rl_xhl,rl_a
 dd rr_b,rr_c,rr_d,rr_e,rr_h,rr_l,rr_xhl,rr_a
 dd sla_b,sla_c,sla_d,sla_e,sla_h,sla_l,sla_xhl,sla_a
 dd sra_b,sra_c,sra_d,sra_e,sra_h,sra_l,sra_xhl,sra_a
 dd swap_b,swap_c,swap_d,swap_e,swap_h,swap_l,swap_xhl,swap_a
 dd srl_b,srl_c,srl_d,srl_e,srl_h,srl_l,srl_xhl,srl_a
 dd bit_0_b,bit_0_c,bit_0_d,bit_0_e,bit_0_h,bit_0_l,bit_0_xhl,bit_0_a
 dd bit_1_b,bit_1_c,bit_1_d,bit_1_e,bit_1_h,bit_1_l,bit_1_xhl,bit_1_a
 dd bit_2_b,bit_2_c,bit_2_d,bit_2_e,bit_2_h,bit_2_l,bit_2_xhl,bit_2_a
 dd bit_3_b,bit_3_c,bit_3_d,bit_3_e,bit_3_h,bit_3_l,bit_3_xhl,bit_3_a
 dd bit_4_b,bit_4_c,bit_4_d,bit_4_e,bit_4_h,bit_4_l,bit_4_xhl,bit_4_a
 dd bit_5_b,bit_5_c,bit_5_d,bit_5_e,bit_5_h,bit_5_l,bit_5_xhl,bit_5_a
 dd bit_6_b,bit_6_c,bit_6_d,bit_6_e,bit_6_h,bit_6_l,bit_6_xhl,bit_6_a
 dd bit_7_b,bit_7_c,bit_7_d,bit_7_e,bit_7_h,bit_7_l,bit_7_xhl,bit_7_a
 dd res_0_b,res_0_c,res_0_d,res_0_e,res_0_h,res_0_l,res_0_xhl,res_0_a
 dd res_1_b,res_1_c,res_1_d,res_1_e,res_1_h,res_1_l,res_1_xhl,res_1_a
 dd res_2_b,res_2_c,res_2_d,res_2_e,res_2_h,res_2_l,res_2_xhl,res_2_a
 dd res_3_b,res_3_c,res_3_d,res_3_e,res_3_h,res_3_l,res_3_xhl,res_3_a
 dd res_4_b,res_4_c,res_4_d,res_4_e,res_4_h,res_4_l,res_4_xhl,res_4_a
 dd res_5_b,res_5_c,res_5_d,res_5_e,res_5_h,res_5_l,res_5_xhl,res_5_a
 dd res_6_b,res_6_c,res_6_d,res_6_e,res_6_h,res_6_l,res_6_xhl,res_6_a
 dd res_7_b,res_7_c,res_7_d,res_7_e,res_7_h,res_7_l,res_7_xhl,res_7_a
 dd set_0_b,set_0_c,set_0_d,set_0_e,set_0_h,set_0_l,set_0_xhl,set_0_a
 dd set_1_b,set_1_c,set_1_d,set_1_e,set_1_h,set_1_l,set_1_xhl,set_1_a
 dd set_2_b,set_2_c,set_2_d,set_2_e,set_2_h,set_2_l,set_2_xhl,set_2_a
 dd set_3_b,set_3_c,set_3_d,set_3_e,set_3_h,set_3_l,set_3_xhl,set_3_a
 dd set_4_b,set_4_c,set_4_d,set_4_e,set_4_h,set_4_l,set_4_xhl,set_4_a
 dd set_5_b,set_5_c,set_5_d,set_5_e,set_5_h,set_5_l,set_5_xhl,set_5_a
 dd set_6_b,set_6_c,set_6_d,set_6_e,set_6_h,set_6_l,set_6_xhl,set_6_a
 dd set_7_b,set_7_c,set_7_d,set_7_e,set_7_h,set_7_l,set_7_xhl,set_7_a

cb_prefix:
    call get_opbyte
    xor esi,esi
    movzx si,byte ptr [memVal]
    movzx bp,byte ptr [offset cb_cycle_table+esi]
    sub [clkLeft],bp
    sub word ptr [_minTime],bp
    jmp [offset cb_table+esi*4]

invalid:
    jmp next

cycle_table:
 db 4,12,8,8,4,4,8,4
 db 4,12,8,8,4,4,8,4
 db 8,12,8,8,4,4,8,4
 db 8,12,8,8,4,4,8,4
 db 8,12,8,8,4,4,8,4
 db 8,12,8,8,4,4,8,4
 db 8,12,8,8,12,12,12,4
 db 8,12,8,8,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 8,8,8,8,8,8,4,8
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 4,4,4,4,4,4,8,4
 db 16,12,16,16,24,16,8,16
 db 16,16,16,0,24,24,8,16
 db 16,10,16,16,24,16,8,16
 db 16,16,16,16,24,24,8,16
 db 12,12,8,12,12,16,8,16
 db 16,4,16,12,12,12,8,16
 db 12,12,8,4,12,16,8,16
 db 12,8,16,4,12,12,8,16

op_table:
 dd nop,ld_bc_word,ld_xbc_a,inc_bc,inc_b,dec_b,ld_b_byte,rlca
 dd ld_xword_sp,add_hl_bc,ld_a_xbc,dec_bc,inc_c,dec_c,ld_c_byte,rrca
 dd stop,ld_de_word,ld_xde_a,inc_de,inc_d,dec_d,ld_d_byte,rla
 dd jr_disp,add_hl_de,ld_a_xde,dec_de,inc_e,dec_e,ld_e_byte,rra
 dd jr_nz_disp,ld_hl_word,ldi_xhl_a,inc_hl,inc_h,dec_h,ld_h_byte,daa
 dd jr_z_disp,add_hl_hl,ldi_a_xhl,dec_hl,inc_l,dec_l,ld_l_byte,cpl
 dd jr_nc_disp,ld_sp_word,ldd_xhl_a,inc_sp,inc_xhl,dec_xhl,ld_xhl_byte,scf
 dd jr_c_disp,add_hl_sp,ldd_a_xhl,dec_sp,inc_a,dec_a,ld_a_byte,ccf
 dd ld_b_b,ld_b_c,ld_b_d,ld_b_e,ld_b_h,ld_b_l,ld_b_xhl,ld_b_a
 dd ld_c_b,ld_c_c,ld_c_d,ld_c_e,ld_c_h,ld_c_l,ld_c_xhl,ld_c_a
 dd ld_d_b,ld_d_c,ld_d_d,ld_d_e,ld_d_h,ld_d_l,ld_d_xhl,ld_d_a
 dd ld_e_b,ld_e_c,ld_e_d,ld_e_e,ld_e_h,ld_e_l,ld_e_xhl,ld_e_a
 dd ld_h_b,ld_h_c,ld_h_d,ld_h_e,ld_h_h,ld_h_l,ld_h_xhl,ld_h_a
 dd ld_l_b,ld_l_c,ld_l_d,ld_l_e,ld_l_h,ld_l_l,ld_l_xhl,ld_l_a
 dd ld_xhl_b,ld_xhl_c,ld_xhl_d,ld_xhl_e,ld_xhl_h,ld_xhl_l,halt,ld_xhl_a
 dd ld_a_b,ld_a_c,ld_a_d,ld_a_e,ld_a_h,ld_a_l,ld_a_xhl,ld_a_a
 dd add_a_b,add_a_c,add_a_d,add_a_e,add_a_h,add_a_l,add_a_xhl,add_a_a
 dd adc_a_b,adc_a_c,adc_a_d,adc_a_e,adc_a_h,adc_a_l,adc_a_xhl,adc_a_a
 dd sub_a_b,sub_a_c,sub_a_d,sub_a_e,sub_a_h,sub_a_l,sub_a_xhl,sub_a_a
 dd sbc_a_b,sbc_a_c,sbc_a_d,sbc_a_e,sbc_a_h,sbc_a_l,sbc_a_xhl,sbc_a_a
 dd and_a_b,and_a_c,and_a_d,and_a_e,and_a_h,and_a_l,and_a_xhl,and_a_a
 dd xor_a_b,xor_a_c,xor_a_d,xor_a_e,xor_a_h,xor_a_l,xor_a_xhl,xor_a_a
 dd or_a_b,or_a_c,or_a_d,or_a_e,or_a_h,or_a_l,or_a_xhl,or_a_a
 dd cp_a_b,cp_a_c,cp_a_d,cp_a_e,cp_a_h,cp_a_l,cp_a_xhl,cp_a_a
 dd ret_nz,pop_bc,jp_nz_word,jp_word,call_nz_word,push_bc,add_a_byte,rst_00
 dd ret_z,ret,jp_z_word,cb_prefix,call_z_word,call_word,adc_a_byte,rst_08
 dd ret_nc,pop_de,jp_nc_word,invalid,call_nc_word,push_de,sub_byte,rst_10
 dd ret_c,reti,jp_c_word,invalid,call_c_word,invalid,sbc_a_byte,rst_18
 dd ldh_xbyte_a,pop_hl,ldh_xc_a,invalid,invalid,push_hl,and_byte,rst_20
 dd add_sp_disp,jp_hl,ld_xword_a,invalid,invalid,invalid,xor_byte,rst_28
 dd ldh_a_xbyte,pop_af,ldh_a_xc,di_,invalid,push_af,or_byte,rst_30
 dd ld_hl_spdisp,ld_sp_hl,ld_a_xword,ei,invalid,invalid,cp_byte,rst_38

@AsmZ80_Execute proc near
    cmp [_run],0
    jz execret
    push eax
    mov [clkLeft],ax
    pusha
    mov al,byte ptr [_regAF+1]
    mov ah,byte ptr [_regAF]
    mov bx,[_regBC]
    mov cx,[_regDE]
    mov dx,[_regHL]
    mov di,[_regSP]
    pusha
    jmp execloop
execloop2:
    cmp word ptr [_gdmaClk],0
    jnz inGDMA
    mov si,[_regPC]
    mov [_oldPC],si
    call get_opbyte
    xor esi,esi
    movzx si,byte ptr [memVal]
    movzx bp,byte ptr [offset cycle_table+esi]
    sub [clkLeft],bp
    sub [_minTime],bp
    jmp [offset op_table+esi*4]
inGDMA:
    mov bp,4
    sub word ptr [clkLeft],bp
    sub word ptr [_minTime],bp
    sub word ptr [_gdmaClk],bp
    jnc next
    mov word ptr [_gdmaClk],0
next:
    cmp word ptr [_minTime],0
    jg execloop2
    jmp afterexecloop
execloop:
    cmp [_debugCheckEnable],0
    jz ecNoDebug
    mov [_minTime],1
    jmp ecNotNeg2
ecNoDebug:
    mov [_minTime],16
    mov bp,[clkLeft]
    cmp bp,[_minTime]
    jg ecGo
    mov [_minTime],bp
ecGo:
    cmp word ptr [_dmaClk],0
    je ecNoDmaClk
    mov bp,word ptr [_dmaClk]
    cmp bp,[_minTime]
    jg ecNoDmaClk
    mov [_minTime],bp
ecNoDmaClk:
    mov bp,[divClk]
    cmp bp,[_minTime]
    jg ecNoDivClk
    mov [_minTime],bp
ecNoDivClk:
    cmp word ptr [_timerClkCount],0
    je ecNoTimerClk
    mov bp,[timerClk]
    cmp bp,[_minTime]
    jg ecNoTimerClk
    mov [_minTime],bp
ecNoTimerClk:
    test byte ptr [_hiRam+40h],80h
    jz ecNoLCD
    mov bp,[_lcdClk]
    cmp bp,[_minTime]
    jg ecNoLCD
    mov [_minTime],bp
ecNoLCD:
    test word ptr [_minTime],8000h
    jz ecNotNeg2
    mov word ptr [_minTime],0
ecNotNeg2:
    popa
    cmp byte ptr [_imeFlag],0
    je noInts
    movzx bp,byte ptr [_hiRam+0ffh]
    and bp,word ptr [_intReq3]
    jz noInts
    cmp byte ptr [_halted],1
    jnz notHalted
    mov byte ptr [_halted],0
    inc word ptr [_regPC]
notHalted:
    mov byte ptr [_imeFlag],0
    push ebp
    sub di,2
    mov si,[_regPC]
    mov [memVal],si
    call put_spword
    pop ebp
    test bp,1
    jz notInt1
    and byte ptr [_intReq],not 1
    and byte ptr [_intReq2],not 1
    mov [_regPC],40h
;    pusha
;    jmp execEnd
    jmp noInts
notInt1:
    test bp,2
    jz notInt2
    and byte ptr [_intReq],not 2
    and byte ptr [_intReq2],not 2
    mov [_regPC],48h
;    pusha
;    jmp execEnd
    jmp noInts
notInt2:
    test bp,4
    jz notInt3
    and byte ptr [_intReq],not 4
    and byte ptr [_intReq2],not 4
    mov [_regPC],50h
;    pusha
;    jmp execEnd
    jmp noInts
notInt3:
    test bp,8
    jz notInt4
    and byte ptr [_intReq],not 8
    and byte ptr [_intReq2],not 8
    mov [_regPC],58h
;    pusha
;    jmp execEnd
    jmp noInts
notInt4:
    test bp,16
    jz noInts
    and byte ptr [_intReq],not 16
    and byte ptr [_intReq2],not 16
    mov [_regPC],60h
;    pusha
;    jmp execEnd
noInts:
    mov si,[_intReq2]
    mov [_intReq3],si
    mov si,word ptr [_intReq]
    mov [_intReq2],si
    mov si,[clkLeft]
    push esi
    jmp execloop2
afterexecloop:
    pop esi
    pusha
    sub si,[clkLeft]
    cmp word ptr [_dmaClk],0
    je noDmaClk
    sub word ptr [_dmaClk],si
    jnc noDmaClk
    mov word ptr [_dmaClk],0
noDmaClk:
    sub [divClk],si
    jnc noDivClk
    inc byte ptr [_hiRam+4]
    add [divClk],256
    cmp byte ptr [_clkMult],1
    je noDivClk
    add [divClk],256
noDivClk:
    cmp word ptr [_timerClkCount],0
    je noTimerClk
    sub [timerClk],si
    jnc noTimerClk
    inc byte ptr [_hiRam+5]
    jnz noTimerOvr
    sahf
    mov ah,byte ptr [_hiRam+6]
    mov byte ptr [_hiRam+5],ah
    lahf
    or byte ptr [_intReq],4
noTimerOvr:
    mov bp,word ptr [_timerClkCount]
    add [timerClk],bp
noTimerClk:
    sub [_lcdClk],si
    jnc lcdEnd
    mov ebp,[_lcdMode]
    jmp [offset lcd_jump_table+ebp*4]
lcd_jump_table dd lcd0,lcd1,lcd2,lcd3
lcd0:
    mov bp,83
    mov dword ptr [_lcdMode],2
    inc dword ptr [_lcdY]
    cmp word ptr [_lcdY],144
    jl notVBlank
    mov byte ptr [_lcdMode],1
    test byte ptr [_hiRam+40h],80h
    jz lcd0NoInt2
    or byte ptr [_intReq],1
    test byte ptr [_hiRam+41h],10h
    jz lcd0NoInt2
    or byte ptr [_intReq],2
lcd0NoInt2:
    test byte ptr [_hiRam+40h],80h
    jz lcd0NoInt3
    test byte ptr [_hiRam+41h],40h
    jz lcd0NoInt3
    movzx bp,byte ptr [_hiRam+45h]
    cmp word ptr [_lcdY],bp
    jne lcd0NoInt3
    or byte ptr [_intReq],2
lcd0NoInt3:
    inc [_lcdUpdateCount]
    mov bp,word ptr [_lcdUpdateFreq]
    cmp [_lcdUpdateCount],bp
    jle lcd3NoRedraw
    mov [_lcdUpdateCount],0
    pusha
    mov esi,offset @RedrawLCD
    db 0ffh,0d6h ;call esi
    popa
lcd3NoRedraw:
    mov bp,456
    jmp lcdAddClk
notVBlank:
    test byte ptr [_hiRam+40h],80h
    jz lcdAddClk
    test byte ptr [_hiRam+41h],20h
    jz lcdAddClk
    or byte ptr [_intReq],2
    jmp lcdAddClk
lcd1:
    inc dword ptr [_lcdY] 
    test byte ptr [_hiRam+40h],80h
    jz lcd1NoInt
    test byte ptr [_hiRam+41h],40h
    jz lcd1NoInt
    movzx bp,byte ptr [_hiRam+45h]
    cmp word ptr [_lcdY],bp
    jne lcd1NoInt
    or byte ptr [_intReq],2
    or byte ptr [_hiRam+41h],4
lcd1NoInt:
    mov bp,456
    cmp word ptr [_lcdY],153
    jle lcdAddClk
    mov dword ptr [_lcdY],0
    mov byte ptr [_lcdMode],2
    mov bp,203
    test byte ptr [_hiRam+40h],80h
    jz lcdNotEnabled
    mov byte ptr [_displayEnabled],1
lcdNotEnabled:
    jmp lcdAddClk
lcd2:
    mov byte ptr [_lcdMode],3
    mov bp,[_lcdUpdateCount]
    cmp word ptr [_lcdUpdateFreq],bp
    mov bp,175
    jnz lcdAddClk
    pusha
    mov esi,offset @RenderLine
    db 0ffh,0d6h ;call esi
    popa
    jmp lcdAddClk
lcd3:
    test byte ptr [_hiRam+40h],80h
    jz lcd3NoInt
    test byte ptr [_hiRam+41h],40h
    jz lcd3NoInt
    movzx bp,byte ptr [_hiRam+45h]
    cmp word ptr [_lcdY],bp
    jne lcd3NoInt
    or byte ptr [_intReq],2
lcd3NoInt:
    mov byte ptr [_lcdMode],0
    mov bp,203
    test byte ptr [_hiRam+40h],80h
    jz lcdAddClk
    test byte ptr [_hiRam+41h],8
    jz lcdAddClk
    or byte ptr [_intReq],2
lcdAddClk:
    add [_lcdClk],bp
    cmp byte ptr [_clkMult],1
    je lcdEnd
    add [_lcdClk],bp
lcdEnd:
    movzx bp,byte ptr [_hiRam+45h]
    cmp word ptr [_lcdY],bp
    jne noLCD
    or byte ptr [_hiRam+41h],4
;    test byte ptr [_hiRam+41h],40h
;    jz noLCD
;    or byte ptr [_intReq],2
noLCD:
    cmp [_debugCheckEnable],0
    jz clkCheck
    cmp [_run],0
    jz execend
    pusha
    mov byte ptr [_regAF+1],al
    mov byte ptr [_regAF],ah
    mov [_regBC],bx
    mov [_regDE],cx
    mov [_regHL],dx
    mov [_regSP],di
    mov esi,offset @DebugChecks
    db 0ffh,0d6h ;call esi
    popa
    cmp [_run],0
    jz execend
clkCheck:
    cmp [clkLeft],0
    jg execloop
execend:
    popa
    mov byte ptr [_regAF+1],al
    mov byte ptr [_regAF],ah
    mov [_regBC],bx
    mov [_regDE],cx
    mov [_regHL],dx
    mov [_regSP],di
    popa
    pop eax
    sub ax,[clkLeft]
    movsx eax,ax
execret:
    ret
@AsmZ80_Execute endp

_TEXT ends
    public @AsmZ80_Execute
    public _regAF
    public _regBC
    public _regDE
    public _regHL
    public _regSP
    public _regPC
    public _nFlag
    public _imeFlag
    public _lcdClk
    public _oldPC
    public _halted
    public _minTime
    public _intReq2
    public _intReq3
    public _lcdUpdateCount
    end

