/* IbmAsm.S
   This file is part of the VGB-DOS project
   Copyright (C) Marcel de Kogel (m.dekogel@student.utwente.nl), 1996
   You may not use this file for commercial purposes
   Please notify me if you make any changes to this file */

.file   "IbmAsm.s"

.data
.align 4
TimerCount:     .long 0
TimerIntCount:  .long 16
OldInt8:        .long 0
OldInt8Selector:.word 0
                .globl  _XBuf
                .globl  _DosSelector
                .globl  _cs_alias


.text
.globl _AllocStack
.globl _FreeStack

.globl _nofunc
_nofunc:
        ret

.globl ___enable
___enable:
        sti
        ret

.globl ___disable
___disable:
        cli
        ret

.globl _JoyGetPos
_JoyGetPos:
        pushl   %ebx
        pushl   %ecx
        pushl   %edx
        pushl   %esi
        pushl   %edi
        movw    $0x201,%dx
        movl    $10000,%ecx
        subl    %esi,%esi
        subl    %edi,%edi
        cli
        outb    %al,%dx
        jmp     _JoyGetPos_Loop
_JoyGetPos_Loop:
        inb     %dx,%al
        movl    %eax,%ebx
        shrl    $1,%ebx
        adcl    $0,%esi
        shrl    $1,%ebx
        adcl    $0,%edi
        testb   $3,%al
        loopnz  _JoyGetPos_Loop
        sti
        movl    %edi,%eax
        shll    $16,%eax
        movw    %si,%ax
        popl    %edi
        popl    %esi
        popl    %edx
        popl    %ecx
        popl    %ebx
        ret

.globl _PutImage_Standard
_PutImage_Standard:
        pushal
        pushw   %es
        movl    _XBuf,%esi
        addl    $8,%esi
        movl    $0xA1450,%edi
        movw    _DosSelector,%es
        movl    $144,%edx
        movl    $40,%ebx
_PutImage_Standard_Loop:
        movl    %ebx,%ecx
        rep
        movsl
        addl    $160,%edi
        addl    $16,%esi
        decl    %edx
        jnz     _PutImage_Standard_Loop
        popw    %es
        popal
        ret

.globl _PutImage_FullScreen
_PutImage_FullScreen:
        pushal
        pushw   %es
        movl    _XBuf,%esi
        addl    $8,%esi
        movl    $0xA0014,%edi
        movw    _DosSelector,%es
        movl    $144,%ebp
_PutFullScreen_LoopY:
        movl    $40,%ecx
        subl    %edx,%edx
_PutFullScreen_LoopX:
        lodsl
        movl    %eax,%ebx
        movb    %bh,%al
        shll    $16,%eax
        movb    %bl,%al
        movb    %bl,%ah
        shll    $4,%edx
        orl     %edx,%eax
        movb    %ah,%dl
        shll    $20,%edx
        orl     %edx,%eax
        stosl
        movzbl  %bh,%edx
        shrl    $16,%ebx
        movb    %bh,%al
        movb    %bh,%ah
        shll    $16,%eax
        movb    %bl,%al
        movb    %bl,%ah
        shll    $4,%edx
        orl     %edx,%eax
        movb    %ah,%dl
        shll    $20,%edx
        orl     %edx,%eax
        stosl
        movzbl  %bh,%edx
        loop    _PutFullScreen_LoopX
        addl    $16,%esi
        addl    $40,%edi
        decl    %ebp
        jnz     _PutFullScreen_LoopY
        popw    %es
        popal
        ret

.globl _ReadTimer
_ReadTimer:
        movl    TimerCount,%eax
        ret

.globl _timer_interrupt
_timer_interrupt_asm:
        pushal
        pushw   %gs
        pushw   %fs
        pushw   %es
        pushw   %ds
.byte 0x2e
        movw    _cs_alias,%ax
        movw    %ax,%ds
        movw    %ax,%es
        movw    %ax,%fs
        movw    %ax,%gs
        call    _AllocStack
        decl    TimerIntCount
        jnz     _timer_int_1
        pushfl
        lcall   (OldInt8)
        movl    $16,TimerIntCount
_timer_int_1:
        addl    $4096,TimerCount
        call    _timer_interrupt
        call    _FreeStack
        popw    %ds
        popw    %es
        popw    %fs
        popw    %gs
        popal
        iret

.globl _install_timer_int
_install_timer_int:
        pushal
        movl    $0x204,%eax
        movb    $8,%bl
        int     $0x31
        movl    %edx,OldInt8
        movw    %cx,OldInt8Selector
        movw    %cs,%cx
        leal    _timer_interrupt_asm,%edx
        movb    $8,%bl
        movl    $0x205,%eax
        int     $0x31
        cli
        movb    $0x36,%al
        outb    %al,$0x43
        call    _nofunc
        movb    $0,%al
        outb    %al,$0x40
        call    _nofunc
        movb    $16,%al
        outb    %al,$0x40
        popal
        sti
        ret

.globl _restore_timer_int
_restore_timer_int:
        pushal
        cli
        movb    $0x36,%al
        outb    %al,$0x43
        call    _nofunc
        movb    $0,%al
        outb    %al,$0x40
        call    _nofunc
        outb    %al,$0x40
        call    _nofunc
        sti
        movl    OldInt8,%edx
        movw    OldInt8Selector,%cx
        movb    $8,%bl
        movl    $0x205,%eax
        int     $0x31
        popal
        ret

.globl _keyb_interrupt
.globl _keyb_interrupt_asm
_keyb_interrupt_asm:
        pushal
        pushw   %gs
        pushw   %fs
        pushw   %es
        pushw   %ds
.byte 0x2e
        movw    _cs_alias,%ax
        movw    %ax,%ds
        movw    %ax,%es
        movw    %ax,%fs
        movw    %ax,%gs
        call    _AllocStack
        call    _keyb_interrupt
        call    _FreeStack
        popw    %ds
        popw    %es
        popw    %fs
        popw    %gs
        popal
        iret

.end

