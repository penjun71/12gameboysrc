#ifndef GBAEMU_HEADER
#define GBAEMU_HEADER

typedef unsigned char  byte;
typedef unsigned short hword;
typedef unsigned long  word;

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned long  u32;

typedef signed char  s8;
typedef signed short s16;
typedef signed long  s32;

#define USER_MODE		0x10
#define FIQ_MODE		0x11
#define IRQ_MODE		0x12
#define SUPERVISOR_MODE	0x13
#define ABORT_MODE		0x17
#define UNDEFINED_MODE	0x1B
#define SYSTEM_MODE		0x1F

#define THUMB_BIT		0x20
#define FIQ_BIT			0x40
#define IRQ_BIT			0x80

#define N_BIT		0x80000000
#define Z_BIT		0x40000000
#define C_BIT		0x20000000
#define V_BIT		0x10000000
#define T_BIT		0x00000020

#define ZFLAG		arm->z_flag
#define NFLAG		arm->n_flag
#define CFLAG		arm->c_flag
#define VFLAG		arm->v_flag

#define DISPCNT		io_ram_u32[0x0]
#define DISPSTAT	io_ram_u16[0x2]
#define BG0CNT		io_ram_u16[0x4]
#define BG1CNT		io_ram_u16[0x5]
#define BG2CNT		io_ram_u16[0x6]
#define BG3CNT		io_ram_u16[0x7]
#define VCOUNT		io_ram_u16[0x3]
#define DM3SAD		io_ram_u32[0x35]
#define DM3SAD_L	io_ram_u16[0x6A]
#define DM3SAD_H	io_ram_u16[0x6B]
#define DM3DAD		io_ram_u32[0x36]
#define DM3DAD_L	io_ram_u16[0x6C]
#define DM3DAD_H	io_ram_u16[0x6D]
#define DM3CNT		io_ram_u32[0x37]
#define DM3CNT_L	io_ram_u16[0x6E]
#define DM3CNT_H	io_ram_u16[0x6F]

#define OPCODE			arm->op
#define OPCODE_T		arm->op_t
#define CPSR			arm->cpsr
#define CONDITION_MASK	(OPCODE>>28)&0xF
#define OPCODE_MASK		((OPCODE&0xFF00000)>>16)|((OPCODE&0xF0)>>4)
#define OPCODE_MASK_T	(OPCODE_T>>6)

#define N_FLAG_SET		(arm->cpsr&0x80000000)
#define Z_FLAG_SET		(arm->cpsr&0x40000000)
#define C_FLAG_SET		(arm->cpsr&0x20000000)
#define V_FLAG_SET		(arm->cpsr&0x10000000)
#define N_FLAG_CLEAR	(!(arm->cpsr&0x80000000))
#define Z_FLAG_CLEAR	(!(arm->cpsr&0x40000000))
#define C_FLAG_CLEAR	(!(arm->cpsr&0x20000000))
#define V_FLAG_CLEAR	(!(arm->cpsr&0x10000000))
#define N_EQU_V_FLAG	(!(N_FLAG_SET^(V_FLAG_SET<<3)))
#define N_NEQ_V_FLAG	(N_FLAG_SET^(V_FLAG_SET<<3))

#define LSL 0
#define LSR 1
#define ASR 2
#define ROR 3

#define POS(i) ( (~(i)) >> 31 )
#define NEG(i) ( (i) >> 31 )

typedef struct BINARY_IMAGE {
	u32 rom_size_u8;
	u32 rom_size_u16;
	u32 rom_size_u32;

}BINARY_IMAGE;

typedef struct ARM7TDMI {
	u32 gp_reg[16];
	u32 cpsr;
	u32 op;
	u16 op_t;
	u32 spsr [0x20];
	u32 state;

	u32 z_flag;
	u32 n_flag;
	u32 c_flag;
	u32 v_flag;

}ARM7TDMI;

BINARY_IMAGE bin;
ARM7TDMI *arm;

u8  *wram_int_u8;	u16 *wram_int_u16;	u32 *wram_int_u32;
u8  *wram_ext_u8;	u16 *wram_ext_u16;	u32 *wram_ext_u32;
u8  *pal_ram_u8;	u16 *pal_ram_u16;	u32 *pal_ram_u32; 
u8  *vram_u8;		u16 *vram_u16;		u32 *vram_u32;
u8  *oam_u8;		u16 *oam_u16;		u32 *oam_u32;
u8  *io_ram_u8;		u16 *io_ram_u16;	u32 *io_ram_u32;
u8  *zero_page_u8;  u16 *zero_page_u16; u32 *zero_page_u32;

u32 *screen;

char *debug_string;
char *debug_str;
char *message_string;
char *operand_string;

u32 new_message;
u32 blit_res_x;
u32 blit_res_y;

u32 (*exec)(void);

u32  (*code_execution   [0xF])(void);
u32  (*opcode_handles   [0x1000])(void);
u32  (*opcode_handles_t [0x400])(void);

void (*io_write_handles	[0x3FF])(void);
void (*debug_handles [0x1000])(u32 op, u32 adress, char *dest);
void (*render_mode [0x8])(void);

char *opcode_strings	[0x1000];
char *opcode_strings_t  [0x400];

static char *condition_strings[] = {"eq", "ne", "cs", "cc", "mi", "pl", "vs", "vc", "hi",
									"ls", "ge", "lt", "gt", "le", "", ""};

static char *register_strings[]  = {"r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8",
						 			"r9", "r10", "r11", "r12", "sp", "lr", "pc"}; 

static char *shift_strings[]     = {"LSL", "LSR", "ASR", "ROR"};

char *cpu_mode_strings [0x20];  

u8  *rom_pages_u8  [0x200];
u16 *rom_pages_u16 [0x200];
u32 *rom_pages_u32 [0x200];

char *dummy_mem; 

u16 *virtual_screen;
u32 translated_palette [0x200];
u32 sprite_sizes_x [0xF];
u32 sprite_sizes_y [0xF];

__declspec(dllexport) int init_gbaemu (void);
__declspec(dllexport) void reset_gbaemu (word pc, char *debug_destination, char *operand_destination);
__declspec(dllexport) int load_bin (char *filename);
__declspec(dllexport) void exec_step ();
__declspec(dllexport) void run_breakpoint (u32 breakpoint);
__declspec(dllexport) void run_frame (void);
__declspec(dllexport) void clean_up (void);

void setup_tables (void);
u8   read_byte (u32 adress);
u16  read_hword (u32 adress);
u32  read_word (u32 adress);
u16  read_aligned_hword (u32 adress);
u32  read_aligned_word (u32 adress);
void write_byte (word adress, byte data);
void write_hword (u32 adress, u16 data);
void write_word (u32 adress, u32 data);

void exec_dma3 (void);

void render_mode0_frame (void);
void render_mode1_frame (void);
void render_mode2_frame (void);
void render_mode3_frame (void);
void render_mode4_frame (void);
void render_mode5_frame (void);
void draw_sprites (u16 priority);
void draw_sprite (u16 a0, u16 a1, u16 a2);

__inline void fill_instruction_pipe (void)  
{
	OPCODE = read_aligned_word (arm->gp_reg [15]);	
	arm->gp_reg [15] += 8;									 			
}   
  
__inline void advance_instruction_pipe (void)
{
	OPCODE = read_aligned_word (arm->gp_reg [15] - 4); 
	arm->gp_reg [15] += 4; 
}

__inline void tfill_instruction_pipe (void)
{
	OPCODE_T = read_hword (arm->gp_reg [15]);
	arm->gp_reg [15] += 4;
}

__inline void tadvance_instruction_pipe (void)
{
	OPCODE_T = read_hword (arm->gp_reg [15] - 2); 
	arm->gp_reg [15] += 2; 
}

#endif
