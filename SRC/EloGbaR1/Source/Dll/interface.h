#ifndef GBAEMU_INTERFACE
#define GBAEMU_INTERFACE

typedef unsigned char  byte;
typedef unsigned short hword;
typedef unsigned long  word;

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned long  u32;
 
typedef signed char  s8;
typedef signed short s16;
typedef signed long  s32;


HBITMAP hBitmap;
BITMAPINFO BmpInfo;
DWORD *BmpBits;
HDC hDC;

//Prototypes of the interface functions
__declspec(dllexport) u32 get_arm_gpreg (u32 i);
__declspec(dllexport) u32 get_arm_cpsr (void);
__declspec(dllexport) void set_arm_gpreg (u32 index, u32 value);
__declspec(dllexport) void set_arm_cpsr (u32 value);
__declspec(dllexport) u32 get_bin_size (void);
__declspec(dllexport) u32 get_instruction_pipe (u32 num);
__declspec(dllexport) u32 get_new_message (char *message_destination);
__declspec(dllexport) void output_message (char *message);
__declspec(dllexport) u16 get_io_reg (u32 index);
__declspec(dllexport) void set_io_reg (u32 index, u16 value);
__declspec(dllexport) u32 get_rom (u32 offset);
__declspec(dllexport) u32 get_rom_size_u32 (void);
__declspec(dllexport) void decode_opcode (u32 op, u32 adress, char *destination);
__declspec(dllexport) void dump_vram (void);
__declspec(dllexport) void blit_frame (void);
__declspec(dllexport) void setup_graphics (HWND handle);
__declspec(dllexport) void set_keyfield (u16 keyfield);
__declspec(dllexport) void set_blit_res (u32 x, u32 y);
__declspec(dllexport) u16 get_rom_u16 (u32 offset);
__declspec(dllexport) void decode_opcode_thumb (u16 op, u32 adress, char *destination);

#endif
