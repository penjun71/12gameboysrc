#include <windows.h>
#include <stdlib.h>
#include <stdio.h> 
#include "interface.h"
#include "gbaemu.h"

u32 get_arm_gpreg (u32 i)                
{ 
	return arm->gp_reg [i];
}

void set_arm_gpreg (u32 index, u32 value)
{ 
	arm->gp_reg [index] = value;
}

u32 get_arm_cpsr (void)					 
{ 
	if (CFLAG) CPSR |= C_BIT; else CPSR &= ~C_BIT; 
	if (VFLAG) CPSR |= V_BIT; else CPSR &= ~V_BIT;
	if (NFLAG) CPSR |= N_BIT; else CPSR &= ~N_BIT;
	if (ZFLAG) CPSR |= Z_BIT; else CPSR &= ~Z_BIT;

	return arm->cpsr;
}

void set_arm_cpsr (u32 value)			 
{ 
	arm->cpsr = value;
}

u32 get_bin_size (void)
{
	return bin.rom_size_u32;
}

u32 get_instruction_pipe (u32 num)
{
	return arm->op;
}

u32 get_new_message (char *message_destination)
{
	message_string = message_destination;
	return new_message;
}

u16 get_io_reg (u32 index)
{
	return io_ram_u16 [index];
}

void set_io_reg (u32 index, u16 value)
{
	io_ram_u16 [index] = value;
}

void output_message (char *message)
{
	new_message = 1;
	if (message_string)
		strcpy (message_string, message);
}

u32 get_rom (u32 offset)
{
	return read_word (offset);
	//return rom_pages_u32 [(offset>>16)&0x1FF] [(offset>>2)&0x3FFF];
}

u32 get_rom_size_u32 (void)
{
	return bin.rom_size_u32;
}

void dump_vram (void)
{
	FILE *fp;

	fp = fopen ("vram.bin", "wb");

	fwrite (vram_u8, 0x17FFF, 1, fp);

	fclose (fp);
}

void setup_graphics (HWND handle)
{
	int i;

	hDC = GetDC(handle);

	for(i=0;i<sizeof(BmpInfo);i++)
		((byte*)&BmpInfo)[i] = 0;

	BmpInfo.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	BmpInfo.bmiHeader.biBitCount    = 32; 
	BmpInfo.bmiHeader.biWidth       = 240;
	BmpInfo.bmiHeader.biHeight      = -160;
	BmpInfo.bmiHeader.biPlanes      = 1;	
	BmpInfo.bmiHeader.biCompression = BI_RGB;
	BmpInfo.bmiHeader.biSizeImage   = 0;

	hBitmap = CreateDIBSection(NULL,&BmpInfo,DIB_RGB_COLORS,(VOID**)&screen,NULL,0);
}

void blit_frame (void)
{
	StretchDIBits(hDC,0,0, 240, 160,0,0,240, 160,screen,&BmpInfo,DIB_RGB_COLORS,SRCCOPY);
}

void decode_opcode (u32 op, u32 adress, char *destination)
{
	debug_handles [((op&0xFF00000)>>16)|((op&0xF0)>>4)](op, adress, destination);
}

void set_keyfield (u16 keyfield)
{
	io_ram_u16 [0x98] = keyfield;
}

void set_blit_res (u32 x, u32 y)
{
	blit_res_x = x;
	blit_res_y = y;
}

void decode_opcode_thumb (u16 op, u32 adress, char *destination)
{
	strcpy (destination, opcode_strings_t [(op>>6)]);
}

u16 get_rom_u16 (u32 offset)
{
	return read_hword (offset);
}
