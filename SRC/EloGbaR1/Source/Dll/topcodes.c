#include "gbaemu.h"

#define Rd_t		arm->gp_reg [(OPCODE_T&0x7)]
#define Rs_t		arm->gp_reg [((OPCODE_T>>3)&0x7)]
#define Rn_t		arm->gp_reg [((OPCODE_T>>6)&0x7)]
#define Rd_imm_t	arm->gp_reg [((OPCODE_T>>8)&0x7)]
#define Hd_t		arm->gp_reg [(OPCODE_T&0x7)+8]
#define Hs_t		arm->gp_reg [((OPCODE_T>>3)&0x7)+8]

int tins_unknown (void)
{
	return 1;
}

int tins_mov_lo_hi (void)
{
	Rd_t = Hs_t;

	tadvance_instruction_pipe();
	return 1;
}

int tins_mov_hi_lo (void)
{
	Hd_t = Rs_t;

	tadvance_instruction_pipe();
	return 1;
}

int tins_mov_hi_hi (void)
{
	Hd_t = Hs_t;

	tadvance_instruction_pipe();
	return 1;
}

int tins_add_lo_hi (void)
{
	Rd_t += Hs_t;

	tadvance_instruction_pipe();
	return 1;
}

int tins_add_hi_lo (void)
{
	Hd_t += Rs_t;

	tadvance_instruction_pipe();
	return 1;
}

int tins_add_hi_hi (void)
{
	Hd_t += Hs_t;

	tadvance_instruction_pipe();
	return 1;
}

int tins_add_sp (void)
{
	if (OPCODE_T & 0x80)
		arm->gp_reg [13] -= (OPCODE_T&0x7F)<<2;
	else
		arm->gp_reg [13] += (OPCODE_T&0x7F)<<2;

	tadvance_instruction_pipe();
	return 1;
}

int tins_mov_imm (void)
{
	Rd_imm_t = OPCODE_T&0xFF;

	SET_DP_LOG_FLAGS(Rd_imm_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_sp_rel_str (void)
{
	write_word (arm->gp_reg[13] + ((OPCODE_T&0xFF)<<2), Rd_imm_t);

	tadvance_instruction_pipe();
	return 1;
}

int tins_pc_rel_ldr (void)
{
	Rd_imm_t = read_word ((arm->gp_reg[15]&~0x2) + ((OPCODE_T&0xFF)<<2));
	
	tadvance_instruction_pipe();
	return 1;
}

int tins_str_imm (void)
{
	write_word (Rs_t + (((OPCODE_T>>6)&0x1F)<<2), Rd_t);

	tadvance_instruction_pipe();
	return 1;
}

int tins_strb_imm (void)
{
	write_byte (Rs_t + (((OPCODE_T>>6)&0x1F)<<2), (u8)Rd_t);

	tadvance_instruction_pipe();
	return 1;
}

int tins_ldr_imm (void)
{
	Rd_t = read_word(Rs_t + (((OPCODE_T>>6)&0x1F)<<2));

	tadvance_instruction_pipe();
	return 1;
}

int tins_ldrb_imm (void)
{
	Rd_t = read_byte(Rs_t + (((OPCODE_T>>6)&0x1F)<<2));

	tadvance_instruction_pipe();
	return 1;
}

int tins_lsl (void) 
{
	Rd_t = Rs_t << ((OPCODE_T>>6)&0x1F);

	SET_DP_LOG_FLAGS(Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_lsr (void) 
{
	Rd_t = Rs_t >> ((OPCODE_T>>6)&0x1F);

	SET_DP_LOG_FLAGS(Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_asr (void)
{
	u32 shift_amount = (OPCODE_T>>6)&0x1F;

	if (Rs_t&0x80000000)
		Rd_t = ((0xFFFFFFFF<<(32-shift_amount))|(OP_REG>>shift_amount)); 
	else
		Rd_t = (Rs_t >> shift_amount);

	SET_DP_LOG_FLAGS(Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_add (void)
{
	Rd_t = Rs_t + Rn_t;

	SET_ADD_FLAGS (Rs_t, Rn_t, Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_add_imm (void)
{
	u32 temp1 = Rd_imm_t;
	u32 temp2 = (OPCODE_T&0xFF);
	Rd_imm_t = temp1 + temp2;

	SET_ADD_FLAGS (temp1, temp2, Rd_imm_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_cmp_imm (void)
{
	u32 temp2 = (OPCODE_T&0xFF);
	u32 temp  = Rd_imm_t - temp2;

	SET_SUB_FLAGS (Rd_imm_t, temp2, temp);
	tadvance_instruction_pipe();
	return 1;
}

int tins_ble (void)
{
	if (Z_FLAG_SET || N_NEQ_V_FLAG) {
		
		if (OPCODE_T & 0x80)
			arm->gp_reg [15] += (((OPCODE_T&0xFF)<<1)-0x200);
		else
			arm->gp_reg [15] += ((OPCODE_T&0x7F)<<1);

		tfill_instruction_pipe();
		return 1;
	}	

	tadvance_instruction_pipe();
	return 1;
}

int tins_strh (void)
{
	write_hword (Rs_t + (((OPCODE_T>>6)&0x1F)<<1), (u16)Rd_t);

	tadvance_instruction_pipe();
	return 1;
}

int tins_ldrh (void)
{
	Rd_t = read_hword (Rs_t + (((OPCODE_T>>6)&0x1F)<<1));

	tadvance_instruction_pipe();
	return 1;
}

int tins_and (void)
{
	Rd_t &= Rs_t;

	SET_DP_LOG_FLAGS (Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_orr (void)
{
	Rd_t |= Rs_t;

	SET_DP_LOG_FLAGS (Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_bic (void)
{
	Rd_t &= ~Rs_t;

	SET_DP_LOG_FLAGS (Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_mvn (void)
{
	Rd_t = ~Rs_t;

	SET_DP_LOG_FLAGS (Rd_t);
	tadvance_instruction_pipe();
	return 1;

}

int tins_add_short_imm (void)
{
	u32 op = ((OPCODE_T>>6)&0x7);

	Rd_t = Rs_t + op;

	SET_ADD_FLAGS (Rs_t, op, Rd_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_sub_imm (void)
{
	u32 temp1 = Rd_imm_t;
	u32 temp2 = (OPCODE_T&0xFF);
	Rd_imm_t = temp1 - temp2;

	SET_SUB_FLAGS (temp1, temp2, Rd_imm_t);
	tadvance_instruction_pipe();
	return 1;
}

int tins_push (void)
{
	int i;

	for (i=0; i<8; i++) {
		
		if (OPCODE_T & (1<<i)) {
			arm->gp_reg [13] -= 4;
			write_word (arm->gp_reg [13], arm->gp_reg [i]);
		}
	}

	if (OPCODE_T & 0x100) {

		arm->gp_reg [13] -= 4;
		write_word (arm->gp_reg [13], arm->gp_reg [14]);
	}
			
	tadvance_instruction_pipe();
	return 1;
}

int tins_pop (void)
{
	int i;

	for (i=0; i<8; i++) {
		
		if (OPCODE_T & (1<<i)) {
			arm->gp_reg [i] = read_word (arm->gp_reg [13]);
			arm->gp_reg [13] += 4;
		}
	}

	if (OPCODE_T & 0x100) {
		arm->gp_reg [15] = read_word (arm->gp_reg [13]);
		arm->gp_reg [13] += 4;
		tfill_instruction_pipe();
		return 1;
	}
			
	tadvance_instruction_pipe();
	return 1;
}

int tins_bl (void)
{
	u32 temp;

	if (OPCODE_T & 0x800) {
		temp = arm->gp_reg [14] + ((OPCODE_T&0x7FF)<<1);
		arm->gp_reg [14] = arm->gp_reg [15] - 2;
		arm->gp_reg [15] = temp;
		tfill_instruction_pipe();
		return 1;
	}
	else {
		arm->gp_reg [14] = arm->gp_reg [15] + ((OPCODE_T&0x7FF)<<12);
		tadvance_instruction_pipe();
		return 1;
	}
}