u32 arm_exec (void)
{
	switch (CONDITION_MASK) {
	
		case 0:
				if (ZFLAG) 
					return opcode_handles[OPCODE_MASK]();
				break;
		case 1:
				if (!ZFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 2:
				if (CFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 3:
				if (!CFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 4:
				if (NFLAG)				
					return opcode_handles[OPCODE_MASK]();
				break;
		case 5:
				if (!NFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 6:
				if (VFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 7:
				if (!VFLAG)				
					return opcode_handles[OPCODE_MASK]();
				break;
		case 8:
				if (CFLAG && !ZFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 9:
				if (!CFLAG || ZFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 10:
				if (NFLAG == VFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 11:
				if (NFLAG != VFLAG)
					return opcode_handles[OPCODE_MASK]();
				break;
		case 12:
				if (!ZFLAG && (NFLAG == VFLAG))
					return opcode_handles[OPCODE_MASK]();
				break;
		case 13:
				if (ZFLAG || (NFLAG != VFLAG))
					return opcode_handles[OPCODE_MASK]();
				break;
		default:
				return opcode_handles[OPCODE_MASK]();
	}
		
	advance_instruction_pipe();
	return 1;
}

u32 thumb_exec (void)
{
	return opcode_handles_t [OPCODE_MASK_T]();
}