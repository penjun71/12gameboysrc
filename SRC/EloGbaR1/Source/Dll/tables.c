void setup_hwdt_handles (u32 base, void *handle)
{	
	opcode_handles [base] = handle;
	opcode_handles [base|0x20] = handle;
	opcode_handles [base|0x80] = handle;
	opcode_handles [base|0x80|0x20] = handle;
	opcode_handles [base|0x100] = handle;
	opcode_handles [base|0x100|0x20] = handle;
	opcode_handles [base|0x100|0x80] = handle;
	opcode_handles [base|0x100|0x80|0x20] = handle;
}

void setup_hwdt_handles2 (u32 base, void *handle, void *handle2)
{	
	opcode_handles [base] = handle;
	opcode_handles [base|0x20] = handle;
	opcode_handles [base|0x80] = handle;
	opcode_handles [base|0x80|0x20] = handle;
	opcode_handles [base|0x100] = handle2;
	opcode_handles [base|0x100|0x20] = handle2;
	opcode_handles [base|0x100|0x80] = handle2;
	opcode_handles [base|0x100|0x80|0x20] = handle2;
}

void setup_sdt_handles2 (u32 base, void *handle, void *handle2, void *handle3, void *handle4)
{
	int i;

	for (i=0; i<0x10; i++) {
		opcode_handles [base|i]					= handle;
		opcode_handles [base|0x20|i]			= handle;
		opcode_handles [base|0x80|i]			= handle2;
		opcode_handles [base|0x80|0x20|i]		= handle2;
		opcode_handles [base|0x100|i]			= handle3;
		opcode_handles [base|0x100|0x80|i]		= handle4;
		opcode_handles [base|0x100|0x20|i]		= handle3;
		opcode_handles [base|0x100|0x80|0x20|i]	= handle4;
	}
}

void setup_sdt_strings (u32 base, char *string)
{
	int i;

	for (i=0; i<0x10; i++) {
		opcode_strings [base|i]					= string;
		opcode_strings [base|0x20|i]			= string;
		opcode_strings [base|0x80|i]			= string;
		opcode_strings [base|0x80|0x20|i]		= string;
		opcode_strings [base|0x100|i]			= string;
		opcode_strings [base|0x100|0x80|i]		= string;
		opcode_strings [base|0x100|0x20|i]		= string;
		opcode_strings [base|0x100|0x80|0x20|i] = string;
	}
}

void setup_dp_handle (u32 base, void *ins, void *ins_reg, void *ins_imm)
{
	int i;

	for (i=0; i<8; i++) {
		opcode_handles [base|(i<<1)]     = ins;		opcode_handles[base|0x200|(i<<1)]     = ins_imm;
		opcode_handles [base|(i<<1)|0x1] = ins_reg;	opcode_handles[base|0x200|(i<<1)|0x1] = ins_imm;
	}
}

void setup_handle_tables (void)
{
	int i, n;

	for (i=0; i<4096; i++) {
		opcode_handles [i] = unknown_opcode;
	}

	setup_dp_handle ( 0x00, ins_and, ins_and_reg, ins_and_imm);
	setup_dp_handle ( 0x80, ins_add, ins_add_reg, ins_add_imm);
	setup_dp_handle ( 0x40, ins_sub, ins_sub_reg, ins_sub_imm);
	setup_dp_handle (0x150, ins_cmp, ins_cmp_reg, ins_cmp_imm);
	setup_dp_handle ( 0x20, ins_eor, ins_eor_reg, ins_eor_imm);
	setup_dp_handle ( 0x60, ins_rsb, ins_rsb_reg, ins_rsb_imm);
	setup_dp_handle ( 0xA0, ins_adc, ins_adc_reg, ins_adc_imm);
	setup_dp_handle ( 0xC0, ins_sbc, ins_sbc_reg, ins_sbc_imm);
	setup_dp_handle ( 0xE0, ins_rsc, ins_rsc_reg, ins_rsc_imm);
	setup_dp_handle (0x110, ins_tst, ins_tst_reg, ins_tst_imm);
	setup_dp_handle (0x130, ins_teq, ins_teq_reg, ins_teq_imm);
	setup_dp_handle (0x170, ins_cmn, ins_cmn_reg, ins_cmn_imm);
	setup_dp_handle (0x180, ins_orr, ins_orr_reg, ins_orr_imm);
	setup_dp_handle (0x1A0, ins_mov, ins_mov_reg, ins_mov_imm);
	setup_dp_handle (0x1C0, ins_bic, ins_bic_reg, ins_bic_imm);
	setup_dp_handle (0x1E0, ins_mvn, ins_mvn_reg, ins_mvn_imm);
	setup_dp_handle ( 0x10, ins_ands, ins_ands_reg, ins_ands_imm);
	setup_dp_handle ( 0x90, ins_adds, ins_adds_reg, ins_adds_imm);
	setup_dp_handle ( 0x50, ins_subs, ins_subs_reg, ins_subs_imm);
	setup_dp_handle ( 0x30, ins_eors, ins_eors_reg, ins_eors_imm);
	setup_dp_handle ( 0x70, ins_rsbs, ins_rsbs_reg, ins_rsbs_imm);
	setup_dp_handle ( 0xB0, ins_adcs, ins_adcs_reg, ins_adcs_imm);
	setup_dp_handle ( 0xD0, ins_sbcs, ins_sbcs_reg, ins_sbcs_imm);
	setup_dp_handle ( 0xF0, ins_rscs, ins_rscs_reg, ins_rscs_imm);
	setup_dp_handle (0x190, ins_orrs, ins_orrs_reg, ins_orrs_imm);
	setup_dp_handle (0x1B0, ins_movs, ins_movs_reg, ins_movs_imm);
	setup_dp_handle (0x1D0, ins_bics, ins_bics_reg, ins_bics_imm);
	setup_dp_handle (0x1F0, ins_mvns, ins_mvns_reg, ins_mvns_imm);

	for (i=0; i<0x10; i++) {
		opcode_handles[0x100|i] = ins_mrs_cpsr;	opcode_handles[0x300|i] = ins_tst_imm;
		opcode_handles[0x120|i] = ins_msr_cpsr;	opcode_handles[0x320|i] = ins_msr_cpsr_imm;
		opcode_handles[0x140|i] = ins_mrs_spsr;	opcode_handles[0x340|i] = ins_cmp_imm;
		opcode_handles[0x160|i] = ins_msr_spsr;	opcode_handles[0x360|i] = ins_msr_spsr_imm;


		for (n=0; n<0xF; n++) {
			opcode_handles[0x800|(n<<5)|i] = ins_stm;
			opcode_handles[0x810|(n<<5)|i] = ins_ldm;
		}
	}

	setup_sdt_handles2 (0x600	 , ins_str_post_down, ins_str_post_up,
								   ins_str_pre_down, ins_str_pre_up);

	setup_sdt_handles2 (0x640	 , ins_strb_post_down, ins_strb_post_up,
								   ins_strb_pre_down, ins_strb_pre_up);

	setup_sdt_handles2 (0x400	 , ins_str_imm_post_down, ins_str_imm_post_up,
								   ins_str_imm_pre_down, ins_str_imm_pre_up);
	
	setup_sdt_handles2 (0x440	 , ins_strb_imm_post_down, ins_strb_imm_post_up,
								   ins_strb_imm_pre_down, ins_strb_imm_pre_up);

	setup_sdt_handles2 (0x600|0x10, ins_ldr_post_down, ins_ldr_post_up,
									ins_ldr_pre_down, ins_ldr_pre_up);

	setup_sdt_handles2 (0x640|0x10, ins_ldrb_post_down, ins_ldrb_post_up,
									ins_ldrb_pre_down, ins_ldrb_pre_up);

	setup_sdt_handles2 (0x400|0x10, ins_ldr_imm_post_down, ins_ldr_imm_post_up,
									ins_ldr_imm_pre_down, ins_ldr_imm_pre_up);

	setup_sdt_handles2 (0x440|0x10, ins_ldrb_imm_post_down, ins_ldrb_imm_post_up,
								   ins_ldrb_imm_pre_down, ins_ldrb_imm_pre_up);

	setup_hwdt_handles (0x5B|0x4, ins_ldrsh_imm);
	setup_hwdt_handles (0x5D, ins_ldrsb_imm);
	setup_hwdt_handles2 (0x1B, ins_ldrh_post, ins_ldrh_pre);
	setup_hwdt_handles2 (0xB, ins_strh_post, ins_strh_pre);
	setup_hwdt_handles2 (0x5B, ins_ldrh_imm_post, ins_ldrh_imm_pre);
	setup_hwdt_handles2 (0x4B, ins_strh_imm_post, ins_strh_imm_pre);
	setup_hwdt_handles (0x1B|0x4, ins_ldrsh);
	setup_hwdt_handles (0x1D, ins_ldrsh);

	opcode_handles [0x9] = ins_mul;
	opcode_handles[0x19] = ins_muls;
	opcode_handles[0x29] = ins_mla;
	opcode_handles[0x39] = ins_mlas;
	opcode_handles[0x89] = ins_mull;
	opcode_handles[0x99] = ins_mulls;
	opcode_handles[0xC9] = ins_mull_unsigned;
	opcode_handles[0xE9] = ins_mulls_unsigned;
	opcode_handles[0xA9] = ins_mlal;
	opcode_handles[0xB9] = ins_mlals;
	opcode_handles[0xE9] = ins_mlal_unsigned;
	opcode_handles[0xF9] = ins_mlals_unsigned;

	opcode_handles[0x109] = ins_swp;
	opcode_handles[0x149] = ins_swpb;
	
	opcode_handles[0x121] = ins_bx;
	
	for (i=0; i<0x100; i++) {
		opcode_handles[0xF00|i] = ins_swi;
	}

	for (i=0; i<0x80; i++) {
		opcode_handles[(0xA<<8)|i]      = ins_bpl;
		opcode_handles[(0xB<<8)|i]      = ins_blpl;
		opcode_handles[(0xA<<8)|i|0x80] = ins_bmi;
		opcode_handles[(0xB<<8)|i|0x80] = ins_blmi;
	}
}

void setup_string_tables (void)
{	
	int i, n;

	for (i=0; i<0x20; i++) {
		cpu_mode_strings [i] = "BadCpuMode";
	}

	cpu_mode_strings [0x10] = "User";
	cpu_mode_strings [0x11] = "FIQ";
	cpu_mode_strings [0x12] = "IRQ";
	cpu_mode_strings [0x13] = "Supervisor";
	cpu_mode_strings [0x17] = "Abort";
	cpu_mode_strings [0x1B] = "Undefined";
	cpu_mode_strings [0x1F] = "System";

	for (i=0; i<4096; i++) {
		opcode_strings [i] = "unknown";
	}

	for (i=0; i<0x10; i++) {
		opcode_strings      [i] = "and";    opcode_strings[0x200|i]	= "and";
		opcode_strings [0x10|i] = "ands";	opcode_strings[0x210|i] = "ands";
		opcode_strings [0x20|i] = "eor";	opcode_strings[0x220|i] = "eor";
		opcode_strings [0x30|i] = "eors";   opcode_strings[0x230|i] = "eors";
		opcode_strings [0x40|i] = "sub";	opcode_strings[0x240|i] = "sub";
		opcode_strings [0x50|i] = "subs";   opcode_strings[0x250|i] = "subs";
		opcode_strings [0x60|i] = "rsb";	opcode_strings[0x260|i] = "rsb";
		opcode_strings [0x70|i] = "rsbs";	opcode_strings[0x270|i] = "rsbs";
		opcode_strings [0x80|i] = "add";    opcode_strings[0x280|i]	= "add";
		opcode_strings [0x90|i] = "adds";	opcode_strings[0x290|i] = "adds";
		opcode_strings [0xA0|i] = "adc";	opcode_strings[0x2A0|i] = "adc";
		opcode_strings [0xB0|i] = "adcs";	opcode_strings[0x2B0|i] = "adcs";
		opcode_strings [0xC0|i] = "sbc";	opcode_strings[0x2C0|i] = "sbc";
		opcode_strings [0xD0|i] = "sbcs";	opcode_strings[0x2D0|i] = "sbcs";
		opcode_strings [0xE0|i] = "rsc";	opcode_strings[0x2E0|i] = "rsc";
		opcode_strings [0xF0|i] = "rscs";	opcode_strings[0x2F0|i] = "rscs";
		opcode_strings[0x100|i] = "mrs";	opcode_strings[0x300|i] = "tst";
		opcode_strings[0x110|i] = "tst";    opcode_strings[0x310|i] = "tst";
		opcode_strings[0x120|i] = "msr"; 	opcode_strings[0x320|i] = "msr";
		opcode_strings[0x130|i] = "teq";	opcode_strings[0x330|i] = "teq";
		opcode_strings[0x140|i] = "mrs";	opcode_strings[0x340|i] = "cmp";
		opcode_strings[0x150|i] = "cmp";    opcode_strings[0x350|i] = "cmp";
		opcode_strings[0x160|i] = "msr";	opcode_strings[0x360|i] = "msr";
		opcode_strings[0x170|i] = "cmn";	opcode_strings[0x370|i] = "cmn";
		opcode_strings[0x180|i] = "orr";	opcode_strings[0x380|i] = "orr";
		opcode_strings[0x190|i] = "orrs";	opcode_strings[0x390|i] = "orrs";
		opcode_strings[0x1A0|i] = "mov";	opcode_strings[0x3A0|i] = "mov";
		opcode_strings[0x1B0|i] = "movs";	opcode_strings[0x3B0|i] = "movs";
		opcode_strings[0x1C0|i] = "bic";	opcode_strings[0x3C0|i] = "bic";
		opcode_strings[0x1D0|i] = "bics";	opcode_strings[0x3D0|i] = "bics";
		opcode_strings[0x1E0|i] = "mvn";	opcode_strings[0x3E0|i] = "mvn";
		opcode_strings[0x1F0|i] = "mvns";	opcode_strings[0x3F0|i] = "mvns";

		for (n=0; n<0xF; n++) {
			opcode_strings[0x800|(n<<5)|i] = "stm";
			opcode_strings[0x810|(n<<5)|i] = "ldm";
		}
	}
	
	setup_sdt_strings (0x600, "str");
	setup_sdt_strings (0x640, "strb");
	setup_sdt_strings (0x400, "str");
	setup_sdt_strings (0x440, "strb");
	setup_sdt_strings (0x610|0x10, "ldr");
	setup_sdt_strings (0x640|0x10, "ldrb");
	setup_sdt_strings (0x400|0x10, "ldr");
	setup_sdt_strings (0x440|0x10, "ldrb");	

	opcode_strings [0x5B] = "ldrh";
	opcode_strings [0x20|0x5B] = "ldrh";
	opcode_strings [0x80|0x5B] = "ldrh";
	opcode_strings [0x80|0x20|0x5B] = "ldrh";
	opcode_strings [0x100|0x5B] = "ldrh";
	opcode_strings [0x100|0x20|0x5B] = "ldrh";
	opcode_strings [0x100|0x80|0x5B] = "ldrh";
	opcode_strings [0x100|0x80|0x20|0x5B] = "ldrh";

	opcode_strings [0x5B|0x4] = "ldrsh";
	opcode_strings [0x20|0x5B|0x4] = "ldrsh";
	opcode_strings [0x80|0x5B|0x4] = "ldrsh";
	opcode_strings [0x80|0x20|0x5B|0x4] = "ldrsh";
	opcode_strings [0x100|0x5B|0x4] = "ldrsh";
	opcode_strings [0x100|0x20|0x5B|0x4] = "ldrsh";
	opcode_strings [0x100|0x80|0x5B|0x4] = "ldrsh";
	opcode_strings [0x100|0x80|0x20|0x5B|0x4] = "ldrsh";
	
	opcode_strings [0x5D] = "ldrsb";
	opcode_strings [0x20|0x5D] = "ldrsb";
	opcode_strings [0x80|0x5D] = "ldrsb";
	opcode_strings [0x80|0x20|0x5D] = "ldrsb";
	opcode_strings [0x100|0x5D] = "ldrsb";
	opcode_strings [0x100|0x20|0x5D] = "ldrsb";
	opcode_strings [0x100|0x80|0x5D] = "ldrsb";
	opcode_strings [0x100|0x80|0x20|0x5D] = "ldrsb";

	opcode_strings [0x1B] = "ldrh";
	opcode_strings [0x20|0x1B] = "ldrh";
	opcode_strings [0x80|0x1B] = "ldrh";
	opcode_strings [0x80|0x20|0x1B] = "ldrh";
	opcode_strings [0x100|0x1B] = "ldrh";
	opcode_strings [0x100|0x20|0x1B] = "ldrh";
	opcode_strings [0x100|0x80|0x1B] = "ldrh";
	opcode_strings [0x100|0x80|0x20|0x1B] = "ldrh";

	opcode_strings [0x1B|0x4] = "ldrsh";
	opcode_strings [0x20|0x1B|0x4] = "ldrsh";
	opcode_strings [0x80|0x1B|0x4] = "ldrsh";
	opcode_strings [0x80|0x20|0x1B|0x4] = "ldrsh";
	opcode_strings [0x100|0x1B|0x4] = "ldrsh";
	opcode_strings [0x100|0x20|0x1B|0x4] = "ldrsh";
	opcode_strings [0x100|0x80|0x1B|0x4] = "ldrsh";
	opcode_strings [0x100|0x80|0x20|0x1B|0x4] = "ldrsh";

	opcode_strings [0x1D] = "ldrsb";
	opcode_strings [0x20|0x1D] = "ldrsb";
	opcode_strings [0x80|0x1D] = "ldrsb";
	opcode_strings [0x80|0x20|0x1D] = "ldrsb";
	opcode_strings [0x100|0x1D] = "ldrsb";
	opcode_strings [0x100|0x20|0x1D] = "ldrsb";
	opcode_strings [0x100|0x80|0x1D] = "ldrsb";
	opcode_strings [0x100|0x80|0x20|0x1D] = "ldrsb";

	opcode_strings [0x4B] = "strh";
	opcode_strings [0x20|0x4B] = "strh";
	opcode_strings [0x80|0x4B] = "strh";
	opcode_strings [0x80|0x20|0x4B] = "strh";
	opcode_strings [0x100|0x4B] = "strh";
	opcode_strings [0x100|0x20|0x4B] = "strh";
	opcode_strings [0x100|0x80|0x4B] = "strh";
	opcode_strings [0x100|0x80|0x20|0x4B] = "strh";

	opcode_strings [0xB] = "strh";
	opcode_strings [0x20|0xB] = "strh";
	opcode_strings [0x80|0xB] = "strh";
	opcode_strings [0x80|0x20|0xB] = "strh";
	opcode_strings [0x100|0xB] = "strh";
	opcode_strings [0x100|0x20|0xB] = "strh";
	opcode_strings [0x100|0x80|0xB] = "strh";
	opcode_strings [0x100|0x80|0x20|0xB] = "strh";

	opcode_strings [0x9] = "mul";
	opcode_strings[0x19] = "muls";
	opcode_strings[0x29] = "mla";
	opcode_strings[0x39] = "mlas";
	opcode_strings[0x89] = "mull";
	opcode_strings[0x99] = "mulls";
	opcode_strings[0xC9] = "mull";
	opcode_strings[0xE9] = "mulls";
	opcode_strings[0xA9] = "mlal";
	opcode_strings[0xB9] = "mlals";
	opcode_strings[0xE9] = "mlal";
	opcode_strings[0xF9] = "mlals";

	opcode_strings[0x121] = "bx";

	for (i=0; i<0x100; i++) {
		opcode_strings[0xF00|i] = "swi";
	}
	
	for (i=0; i<0x100; i++) {
		opcode_strings[(0xA<<8)|i] = "b";
		opcode_strings[(0xB<<8)|i] = "bl";
	}
}

void setup_io_handle_tables (void)
{
	int i;

	for (i=0; i<0x3FF; i++) {
	
		io_write_handles [i] = standard_io_write_handle;
	}

	io_write_handles [0x00] = reg_dispcnt_write;
	io_write_handles [0xDC] = reg_dm3cnt_write;
	io_write_handles [0xDD] = reg_dm3cnt_write;
	io_write_handles [0xDE] = reg_dm3cnt_write;
	io_write_handles [0xDF] = reg_dm3cnt_write;
}

void setup_dp_debug_handle (u32 base, void *handle)
{
	int i;

	for (i=0; i<8; i++) {
		debug_handles [base|(i<<1)]     = handle;	debug_handles[base|0x200|(i<<1)]     = handle;
		debug_handles [base|(i<<1)|0x1] = handle;	debug_handles[base|0x200|(i<<1)|0x1] = handle;
	}
}

void setup_sdt_debug_handles (u32 base, void *handle)
{	
	int i;

	for (i=0; i<0x10; i++) {
		debug_handles  [base|i]					= handle;
		debug_handles  [base|0x20|i]			= handle;
		debug_handles  [base|0x80|i]			= handle;
		debug_handles  [base|0x80|0x20|i]		= handle;
		debug_handles  [base|0x100|i]			= handle;
		debug_handles  [base|0x100|0x80|i]		= handle;
		debug_handles  [base|0x100|0x20|i]		= handle;
		debug_handles  [base|0x100|0x80|0x20|i] = handle;
	}
}

void setup_hwdt_debug_handles (u32 base, void *handle)
{	
	debug_handles [base] = handle;
	debug_handles [base|0x20] = handle;
	debug_handles [base|0x80] = handle;
	debug_handles [base|0x80|0x20] = handle;
	debug_handles [base|0x100] = handle;
	debug_handles [base|0x100|0x20] = handle;
	debug_handles [base|0x100|0x80] = handle;
	debug_handles [base|0x100|0x80|0x20] = handle;
}

void setup_debug_handles (void)
{
	int i;

	for (i=0; i<0x1000; i++) {
		debug_handles [i] = standard_debug_handle;
	}

	setup_dp_debug_handle (0x1A0, dpsingle_debug_handle);
	setup_dp_debug_handle (0x1E0, dpsingle_debug_handle);
	setup_dp_debug_handle (0x1B0, dpsingle_debug_handle);
	setup_dp_debug_handle (0x1F0, dpsingle_debug_handle);

	setup_dp_debug_handle ( 0x00, dp_debug_handle);
	setup_dp_debug_handle ( 0x80, dp_debug_handle);
	setup_dp_debug_handle ( 0x40, dp_debug_handle);
	setup_dp_debug_handle (0x150, dpnw_debug_handle);
	setup_dp_debug_handle ( 0x20, dp_debug_handle);
	setup_dp_debug_handle ( 0x60, dp_debug_handle);
	setup_dp_debug_handle ( 0xA0, dp_debug_handle);
	setup_dp_debug_handle ( 0xC0, dp_debug_handle);
	setup_dp_debug_handle ( 0xE0, dp_debug_handle);
	setup_dp_debug_handle (0x110, dpnw_debug_handle);
	setup_dp_debug_handle (0x130, dpnw_debug_handle);
	setup_dp_debug_handle (0x170, dpnw_debug_handle);
	setup_dp_debug_handle (0x180, dp_debug_handle);
	setup_dp_debug_handle (0x1C0, dp_debug_handle);
	setup_dp_debug_handle ( 0x10, dp_debug_handle);
	setup_dp_debug_handle ( 0x90, dp_debug_handle);
	setup_dp_debug_handle ( 0x50, dp_debug_handle);
	setup_dp_debug_handle ( 0x30, dp_debug_handle);
	setup_dp_debug_handle ( 0x70, dp_debug_handle);
	setup_dp_debug_handle ( 0xB0, dp_debug_handle);
	setup_dp_debug_handle ( 0xD0, dp_debug_handle);
	setup_dp_debug_handle ( 0xF0, dp_debug_handle);
	setup_dp_debug_handle (0x190, dp_debug_handle);
	setup_dp_debug_handle (0x1D0, dp_debug_handle);

	setup_sdt_debug_handles (0x600, sdt_debug_handle);
	setup_sdt_debug_handles (0x640, sdt_debug_handle);
	setup_sdt_debug_handles (0x400, sdt_debug_handle);		
	setup_sdt_debug_handles (0x440, sdt_debug_handle);
	setup_sdt_debug_handles (0x600|0x10, sdt_debug_handle);
	setup_sdt_debug_handles (0x640|0x10, sdt_debug_handle);
	setup_sdt_debug_handles (0x400|0x10, sdt_debug_handle);
	setup_sdt_debug_handles (0x440|0x10, sdt_debug_handle);
	
	setup_hwdt_debug_handles (0x5B|0x4, hwdt_debug_handle);
	setup_hwdt_debug_handles (0x5D, hwdt_debug_handle);
	setup_hwdt_debug_handles (0x1B, hwdt_debug_handle);
	setup_hwdt_debug_handles (0x1B|0x4, hwdt_debug_handle);
	setup_hwdt_debug_handles (0x1D, hwdt_debug_handle);
	setup_hwdt_debug_handles (0x4B, hwdt_debug_handle);
	setup_hwdt_debug_handles (0xB, hwdt_debug_handle);
	setup_hwdt_debug_handles (0x5B, hwdt_debug_handle);

	debug_handles [0x9] = mul_debug_handle;
	debug_handles[0x19] = mul_debug_handle;
	debug_handles[0x29] = mul_debug_handle;
	debug_handles[0x39] = mul_debug_handle;
	debug_handles[0x89] = mull_debug_handle;
	debug_handles[0x99] = mull_debug_handle;
	debug_handles[0xC9] = mull_debug_handle;
	debug_handles[0xE9] = mull_debug_handle;
	debug_handles[0xA9] = mull_debug_handle;
	debug_handles[0xB9] = mull_debug_handle;
	debug_handles[0xE9] = mull_debug_handle;
	debug_handles[0xF9] = mull_debug_handle;

	for (i=0; i<0x100; i++) {
		debug_handles[(0xA<<8)|i] = b_debug_handle;
		debug_handles[(0xB<<8)|i] = b_debug_handle;
	}
}
