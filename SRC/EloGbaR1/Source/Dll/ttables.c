void setup_handle_tables_t (void)
{
	int i;

	for (i=0; i<0x400; i++) {

		opcode_handles_t [i] = tins_unknown;
	}

	for (i=0; i<0x8; i++) {
		opcode_handles_t [0x2D0|i] = tins_push;
		opcode_handles_t [0x2F0|i] = tins_pop;
		opcode_handles_t [0x60|i] = tins_add;
		opcode_handles_t [0x70|i] = tins_add_short_imm;
	}	
	
	for (i=0; i<0x1F; i++) {
		opcode_handles_t [0x80|i] = tins_mov_imm;
		opcode_handles_t [0x240|i] = tins_sp_rel_str;
		opcode_handles_t [0x120|i] = tins_pc_rel_ldr;
		opcode_handles_t [0x180|i] = tins_str_imm;
		opcode_handles_t [0x180|0x40|i] = tins_strb_imm;

		opcode_handles_t [0x180|0x20|i] = tins_ldr_imm;
		opcode_handles_t [0x180|0x40|0x20|i] = tins_ldrb_imm;
		opcode_handles_t [i] = tins_lsl;
		opcode_handles_t [i|0x20] = tins_lsr;
		opcode_handles_t [0x40|i] = tins_asr;
		opcode_handles_t [0xC0|i] = tins_add_imm;
		opcode_handles_t [0xE0|i] = tins_sub_imm;
		opcode_handles_t [0xA0|i] = tins_cmp_imm;
		opcode_handles_t [0x200|i] = tins_strh;
		opcode_handles_t [0x200|0x20|i] = tins_ldrh;
	}

	for (i=0; i<0x4; i++) {
		opcode_handles_t [0x374|i] = tins_ble;	
	}

	for (i=0; i<0x3F; i++) {
		opcode_handles_t [0x3C0|i] = tins_bl;
	}

	opcode_handles_t [0x2C0] = tins_add_sp;
	opcode_handles_t [0x2C1] = tins_add_sp;
	opcode_handles_t [0x2C2] = tins_add_sp;

	opcode_handles_t [0x118|0x1] = tins_mov_lo_hi;
	opcode_handles_t [0x118|0x2] = tins_mov_hi_lo;
	opcode_handles_t [0x118|0x3] = tins_mov_hi_hi;

	opcode_handles_t [0x110|0x1] = tins_add_lo_hi;
	opcode_handles_t [0x110|0x2] = tins_add_hi_lo;
	opcode_handles_t [0x110|0x3] = tins_add_hi_hi;

	opcode_handles_t [0x100] = tins_and;
	opcode_handles_t [0x100|0xc] = tins_orr;
	opcode_handles_t [0x100|0xF] = tins_mvn;
	opcode_handles_t [0x100|0xE] = tins_bic;
}

void setup_string_tables_t (void)
{
	int i;

	for (i=0; i<0x400; i++) {

		opcode_strings_t [i] = "unknown";
	}

	for (i=0; i<0x8; i++) {
		opcode_strings_t [0x2D0|i] = "push";
		opcode_strings_t [0x2F0|i] = "pop";
		opcode_strings_t [0x60|i]  = "add";
		opcode_strings_t [0x70|i]  = "add";
	}	

	for (i=0; i<0x1F; i++) {
		opcode_strings_t [0x80|i] = "mov";
		opcode_strings_t [0x240|i] = "str";
		opcode_strings_t [0x120|i] = "ldr";
		opcode_strings_t [0x180|i] = "str";
		opcode_strings_t [0x180|0x40|i] = "strb";
		opcode_strings_t [0x180|0x20|i] = "ldr";
		opcode_strings_t [0x180|0x40|0x20|i] = "ldrb";
		opcode_strings_t [i] = "lsl";
		opcode_strings_t [i|0x20] = "lsr";
		opcode_strings_t [0x40|i] = "asr";
		opcode_strings_t [0xC0|i] = "add";
		opcode_strings_t [0xE0|i] = "sub";
		opcode_strings_t [0xA0|i] = "cmp";
		opcode_strings_t [0x200|i] = "strh";
		opcode_strings_t [0x200|0x20|i] = "ldrh";
	}

	for (i=0; i<0x4; i++) {
		opcode_strings_t [0x374|i] = "ble";	
	}

	for (i=0; i<0x3F; i++) {
		opcode_strings_t [0x3C0|i] = "bl";
	}

	opcode_strings_t [0x2C0] = "addsp";
	opcode_strings_t [0x2C1] = "addsp";
	opcode_strings_t [0x2C2] = "addsp";

	opcode_strings_t [0x118|0x1] = "mov";
	opcode_strings_t [0x118|0x2] = "mov";
	opcode_strings_t [0x118|0x3] = "mov";

	opcode_strings_t [0x110|0x1] = "add";
	opcode_strings_t [0x110|0x2] = "add";
	opcode_strings_t [0x110|0x3] = "add";

	opcode_strings_t [0x100] = "and";
	opcode_strings_t [0x100|0xc] = "orr";
	opcode_strings_t [0x100|0xF] = "mvn";
	opcode_strings_t [0x100|0xE] = "bic";
}