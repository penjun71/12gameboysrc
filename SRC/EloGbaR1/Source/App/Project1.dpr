program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {GBAEmu},
  Unit2 in 'Unit2.pas',
  Unit3 in 'Unit3.pas' {Debugger};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TGBAEmu, GBAEmu);
  Application.CreateForm(TDebugger, Debugger);
  Application.Run;
end.
