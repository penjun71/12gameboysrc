object Debugger: TDebugger
  Left = 308
  Top = 201
  BorderStyle = bsDialog
  Caption = 'ARM7TDMI Debugger'
  ClientHeight = 258
  ClientWidth = 529
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 344
    Top = 0
    Width = 185
    Height = 233
    Caption = 'Cpu Status'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 12
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label2: TLabel
      Left = 4
      Top = 24
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
      WordWrap = True
    end
    object Label3: TLabel
      Left = 4
      Top = 36
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label4: TLabel
      Left = 4
      Top = 48
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label5: TLabel
      Left = 4
      Top = 60
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label6: TLabel
      Left = 4
      Top = 72
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label7: TLabel
      Left = 4
      Top = 84
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label8: TLabel
      Left = 4
      Top = 96
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label9: TLabel
      Left = 4
      Top = 108
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label10: TLabel
      Left = 4
      Top = 120
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label11: TLabel
      Left = 4
      Top = 132
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R10'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label12: TLabel
      Left = 4
      Top = 144
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R11'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label13: TLabel
      Left = 4
      Top = 156
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'R12'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label14: TLabel
      Left = 4
      Top = 168
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'SP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label15: TLabel
      Left = 4
      Top = 180
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'LR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label16: TLabel
      Left = 4
      Top = 192
      Width = 29
      Height = 16
      AutoSize = False
      Caption = 'PC'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label34: TLabel
      Left = 120
      Top = 16
      Width = 35
      Height = 13
      Caption = 'Flags:'
    end
    object LabelR3: TLabel
      Left = 48
      Top = 48
      Width = 81
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR0: TLabel
      Left = 48
      Top = 12
      Width = 73
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR1: TLabel
      Left = 48
      Top = 24
      Width = 73
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
      WordWrap = True
    end
    object LabelR4: TLabel
      Left = 48
      Top = 60
      Width = 73
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR5: TLabel
      Left = 48
      Top = 72
      Width = 81
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR6: TLabel
      Left = 48
      Top = 84
      Width = 81
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR7: TLabel
      Left = 48
      Top = 96
      Width = 77
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR8: TLabel
      Left = 48
      Top = 108
      Width = 69
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR9: TLabel
      Left = 48
      Top = 120
      Width = 73
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR10: TLabel
      Left = 48
      Top = 132
      Width = 77
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR11: TLabel
      Left = 48
      Top = 144
      Width = 73
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR12: TLabel
      Left = 48
      Top = 156
      Width = 81
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR13: TLabel
      Left = 48
      Top = 168
      Width = 77
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR14: TLabel
      Left = 48
      Top = 180
      Width = 77
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR15: TLabel
      Left = 48
      Top = 192
      Width = 77
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelR2: TLabel
      Left = 48
      Top = 36
      Width = 73
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label17: TLabel
      Left = 4
      Top = 212
      Width = 37
      Height = 16
      AutoSize = False
      Caption = 'CPSR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object LabelCPSR: TLabel
      Left = 48
      Top = 212
      Width = 77
      Height = 16
      AutoSize = False
      Caption = '$00000000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object CheckBoxN: TCheckBox
      Left = 120
      Top = 32
      Width = 33
      Height = 17
      Caption = 'N'
      TabOrder = 0
    end
    object CheckBoxZ: TCheckBox
      Left = 120
      Top = 48
      Width = 33
      Height = 17
      Caption = 'Z'
      TabOrder = 1
    end
    object CheckBoxC: TCheckBox
      Left = 120
      Top = 64
      Width = 33
      Height = 17
      Caption = 'C'
      TabOrder = 2
    end
    object CheckBoxV: TCheckBox
      Left = 120
      Top = 80
      Width = 33
      Height = 17
      Caption = 'V'
      TabOrder = 3
    end
    object CheckBoxI: TCheckBox
      Left = 152
      Top = 32
      Width = 29
      Height = 17
      Caption = 'I'
      TabOrder = 4
    end
    object CheckBoxF: TCheckBox
      Left = 152
      Top = 48
      Width = 29
      Height = 17
      Caption = 'F'
      TabOrder = 5
    end
    object CheckBoxT: TCheckBox
      Left = 152
      Top = 80
      Width = 29
      Height = 17
      Caption = 'T'
      TabOrder = 6
    end
  end
  object ListBox1: TListBox
    Left = 0
    Top = 4
    Width = 341
    Height = 229
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 1
    TabWidth = 22
  end
  object Button1: TButton
    Left = 0
    Top = 236
    Width = 75
    Height = 21
    Caption = 'Step'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 192
    Top = 236
    Width = 75
    Height = 21
    Caption = 'Breakpoint'
    TabOrder = 3
    OnClick = Button2Click
  end
  object EditStepValue: TEdit
    Left = 80
    Top = 236
    Width = 69
    Height = 21
    TabOrder = 4
    Text = '$00000001'
  end
  object EditBreakpoint: TEdit
    Left = 272
    Top = 236
    Width = 69
    Height = 21
    TabOrder = 5
    Text = '$08000000'
  end
end
