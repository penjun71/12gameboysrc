object GBAEmu: TGBAEmu
  Left = 467
  Top = 226
  BorderStyle = bsSingle
  Caption = 'GBA Emu by Eloist'
  ClientHeight = 179
  ClientWidth = 240
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 160
    Width = 240
    Height = 19
    Panels = <
      item
        Text = 'FPS:'
        Width = 60
      end
      item
        Width = 130
      end
      item
        Text = 'Mode:'
        Width = 50
      end>
    SimplePanel = False
  end
  object MainMenu1: TMainMenu
    Left = 24
    Top = 8
    object File1: TMenuItem
      Caption = 'File'
      object LoadBinary1: TMenuItem
        Caption = 'Load Binary...'
        OnClick = LoadBinary1Click
      end
      object Quit1: TMenuItem
        Caption = '-'
      end
      object Quit2: TMenuItem
        Caption = 'Quit....'
        OnClick = Quit2Click
      end
    end
    object Emulation1: TMenuItem
      Caption = 'Emulation'
      object Start1: TMenuItem
        Caption = 'Start'
        OnClick = Start1Click
      end
      object Reset1: TMenuItem
        Caption = 'Reset'
        OnClick = Reset1Click
      end
    end
    object Debug1: TMenuItem
      Caption = 'Debug'
      object Debugger1: TMenuItem
        Caption = 'ARM Debugger'
        OnClick = Debugger1Click
      end
      object GBADebugger1: TMenuItem
        Caption = 'GBA Debugger ;-)'
      end
    end
    object Display1: TMenuItem
      Caption = 'Display'
      object x11: TMenuItem
        Caption = 'Size'
        object x12: TMenuItem
          Caption = 'x1'
        end
        object x21: TMenuItem
          Caption = 'x2'
        end
        object x31: TMenuItem
          Caption = 'x3'
        end
      end
    end
    object About1: TMenuItem
      Caption = 'About'
      object Checll33taboutmsg1: TMenuItem
        Caption = 'Check l33t about msg'
      end
    end
  end
  object OpenDialogBin: TOpenDialog
    InitialDir = 'c:\gba\demos'
    Left = 24
    Top = 56
  end
end
