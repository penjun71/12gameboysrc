unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Menus;
type
  RunThread = class(TThread)
     keyfield:integer;
  private
    { Private-Deklarationen }
  protected
    procedure Execute; override;
  end;

implementation
uses Unit1;
      procedure run_frame; cdecl; external 'gbaemu.dll'
      procedure blit_frame; cdecl; external 'gbaemu.dll'
      procedure set_keyfield (keyfield:integer); cdecl; external 'gbaemu.dll'
      function get_rom (offset:longword):longword; cdecl; external 'gbaemu.dll'

procedure RunThread.Execute;
var
   von, bis, i:integer;
   temp: longword;
begin
  while (not terminated) do
  begin
       von:=gettickcount;
       for i:=1 to 10 do
       begin
          run_frame;
          blit_frame;
          set_keyfield (keyfield);
       end;
       bis:=gettickcount;
       Gbaemu.StatusBar1.panels[0].text := 'FPS: ' + Format('%*.*f',[3,2,10000/(bis-von)]);
       temp := get_rom ($04000000);
       GBAemu.StatusBar1.panels[2].text := 'Mode: ' + IntToStr (temp and $7);
  end;
end;

end.
