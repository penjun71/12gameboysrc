unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Unit1;

type
  TDebugger = class(TForm)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    LabelR0: TLabel;
    LabelR1: TLabel;
    LabelR2: TLabel;
    LabelR3: TLabel;
    LabelR5: TLabel;
    LabelR4: TLabel;
    LabelR6: TLabel;
    LabelR7: TLabel;
    LabelR8: TLabel;
    LabelR9: TLabel;
    LabelR10: TLabel;
    LabelR11: TLabel;
    LabelR12: TLabel;
    LabelR13: TLabel;
    LabelR14: TLabel;
    LabelR15: TLabel;
    ListBox1: TListBox;
    Label34: TLabel;
    CheckBoxN: TCheckBox;
    CheckBoxZ: TCheckBox;
    CheckBoxC: TCheckBox;
    CheckBoxV: TCheckBox;
    CheckBoxI: TCheckBox;
    CheckBoxF: TCheckBox;
    CheckBoxT: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    EditStepValue: TEdit;
    EditBreakpoint: TEdit;
    Label17: TLabel;
    LabelCPSR: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CheckBoxThumbClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Debugger: TDebugger;


implementation
procedure exec_step(); cdecl; external 'gbaemu.dll'
procedure run_breakpoint(breakpoint:longword); cdecl; external 'gbaemu.dll'
function get_rom_u16 (offset:longword):integer; cdecl; external 'gbaemu.dll'
procedure decode_opcode_thumb (op:integer;adress:longword;destination:pchar); cdecl; external 'gbaemu.dll'
 function get_rom (offset:longword):longword; cdecl; external 'gbaemu.dll'
     function get_rom_size_u32 ():longword; cdecl; external 'gbaemu.dll'
     procedure decode_opcode (op:longword;adress:longword;destination:pchar); cdecl; external 'gbaemu.dll'

{$R *.DFM}

procedure TDebugger.Button1Click(Sender: TObject);
var
   i:longword;
begin
with GBAEmu do
begin
     if bin_loaded = true AND threadrunning = false then
     begin
          for i:=1 to StrToInt(EditStepValue.text) do
          begin
              exec_step();
          end;
          UpdateDebugger;
     end;
end;
end;

procedure TDebugger.Button2Click(Sender: TObject);
var
   temp : longword;
begin
     with GBAEmu do begin
     if bin_loaded = true AND threadrunning = false then
     begin
          temp := StrToInt (EditBreakPoint.text);
          run_breakpoint (temp);
          UpdateDebugger;
     end;
     end;
end;

procedure TDebugger.CheckBoxThumbClick(Sender: TObject);
begin
     Gbaemu.UpdateDebugger;
end;

end.
