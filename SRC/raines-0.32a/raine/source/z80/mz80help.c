/******************************************************************************/
/*                                                                            */
/*                          RAINE INTERFACE FOR MZ80                          */
/*                                                                            */
/******************************************************************************/

/*

Raine version of MZ80 has a changed (improved and faster) memory interface.
When calling AddZ80xReadByte() / AddZ80xReadByte():

------+------+--------------------------------+---------------------------------
 call | user | Old MZ80                       | Raine MZ80
------+------+--------------------------------+---------------------------------
 NULL | NULL | -                              | Internal read on current z80Base
 xxxx | NULL | Call with user = NULL          | Call with user = NULL
 xxxx | xxxx | Call with user = xxxx          | Call with user = xxxx
 NULL | xxxx | -                              | Internal read on xxxx base
------+------+--------------------------------+---------------------------------

Call Z80xSetBank() to switch the z80Base address.

puser is relative to 0x0000

*/

#include "raine.h"
#include "debug.h"
#include "mz80help.h"
#include "savegame.h"

static UINT32 memory_count_rb[MAX_Z80];
static UINT32 memory_count_wb[MAX_Z80];

static UINT32 port_count_rb[MAX_Z80];
static UINT32 port_count_wb[MAX_Z80];

typedef struct Z80_BANKLIST
{
   UINT8 list[32];
   UINT32 count;
} Z80_BANKLIST;

typedef struct Z80_DATA			// Information about 1 chip
{
   Z80_BANKLIST read_bank;
   Z80_BANKLIST write_bank;
   UINT8 *base_ram;
} Z80_DATA;

struct Z80_DATA z80_data[MAX_Z80];

static void add_mz80_memory_region_rb(UINT32 cpu, UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   if(!d2){
      if(!d3){					// Add to bankswitching queue
         d3 = Z80_context[cpu].z80Base;
         z80_data[cpu].read_bank.list[z80_data[cpu].read_bank.count] = memory_count_rb[cpu];
         z80_data[cpu].read_bank.count++;
      }
      else{
         d3 = d3 - d0;
      }
   }

   Z80_memory_rb[cpu][memory_count_rb[cpu]].lowAddr    = d0;
   Z80_memory_rb[cpu][memory_count_rb[cpu]].highAddr   = d1;
   Z80_memory_rb[cpu][memory_count_rb[cpu]].memoryCall = d2;
   Z80_memory_rb[cpu][memory_count_rb[cpu]].pUserArea  = d3;
   memory_count_rb[cpu]++;
}

static void add_mz80_memory_region_wb(UINT32 cpu, UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   if(!d2){
      if(!d3){					// Add to bankswitching queue
         d3 = Z80_context[cpu].z80Base;
         z80_data[cpu].write_bank.list[z80_data[cpu].write_bank.count] = memory_count_wb[cpu];
         z80_data[cpu].write_bank.count++;
      }
      else{
         d3 = d3 - d0;
      }
   }

   Z80_memory_wb[cpu][memory_count_wb[cpu]].lowAddr    = d0;
   Z80_memory_wb[cpu][memory_count_wb[cpu]].highAddr   = d1;
   Z80_memory_wb[cpu][memory_count_wb[cpu]].memoryCall = d2;
   Z80_memory_wb[cpu][memory_count_wb[cpu]].pUserArea  = d3;
   memory_count_wb[cpu]++;
}

static void add_mz80_port_region_rb(UINT32 cpu, UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   Z80_port_rb[cpu][port_count_rb[cpu]].lowIoAddr  = d0;
   Z80_port_rb[cpu][port_count_rb[cpu]].highIoAddr = d1;
   Z80_port_rb[cpu][port_count_rb[cpu]].IOCall     = d2;
   Z80_port_rb[cpu][port_count_rb[cpu]].pUserArea  = d3;
   port_count_rb[cpu]++;
}

static void add_mz80_port_region_wb(UINT32 cpu, UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   Z80_port_wb[cpu][port_count_wb[cpu]].lowIoAddr  = d0;
   Z80_port_wb[cpu][port_count_wb[cpu]].highIoAddr = d1;
   Z80_port_wb[cpu][port_count_wb[cpu]].IOCall     = d2;
   Z80_port_wb[cpu][port_count_wb[cpu]].pUserArea  = d3;
   port_count_wb[cpu]++;
}

void Z80ASetBank(UINT8 *src)
{
   UINT32 ta,tb;

   // Update base pointer (if called during emulation)

   z80Base = src;

   // Update base pointer (if called outside emulation)

   Z80_context[0].z80Base = src;

   z80_data[0].base_ram = Z80_context[0].z80Base;

   for(ta=0; ta<z80_data[0].read_bank.count; ta++){
      tb = z80_data[0].read_bank.list[ta];			// Get bank pos
      Z80_memory_rb[0][tb].pUserArea = src;			// Write new pointer
   }
   for(ta=0; ta<z80_data[0].write_bank.count; ta++){
      tb = z80_data[0].write_bank.list[ta];			// Get bank pos
      Z80_memory_wb[0][tb].pUserArea = src;			// Write new pointer
   }
}

void Z80BSetBank(UINT8 *src)
{
   UINT32 ta,tb;

   // Update base pointer (if called during emulation)

   z80Base = src;

   // Update base pointer (if called outside emulation)

   Z80_context[1].z80Base = src;

   z80_data[1].base_ram = Z80_context[1].z80Base;

   for(ta=0; ta<z80_data[1].read_bank.count; ta++){
      tb = z80_data[1].read_bank.list[ta];			// Get bank pos
      Z80_memory_rb[1][tb].pUserArea = src;			// Write new pointer
   }
   for(ta=0; ta<z80_data[1].write_bank.count; ta++){
      tb = z80_data[1].write_bank.list[ta];			// Get bank pos
      Z80_memory_wb[1][tb].pUserArea = src;			// Write new pointer
   }
}

void Z80CSetBank(UINT8 *src)
{
   UINT32 ta,tb;

   // Update base pointer (if called during emulation)

   z80Base = src;

   // Update base pointer (if called outside emulation)

   Z80_context[2].z80Base = src;

   z80_data[2].base_ram = Z80_context[2].z80Base;

   for(ta=0; ta<z80_data[2].read_bank.count; ta++){
      tb = z80_data[2].read_bank.list[ta];			// Get bank pos
      Z80_memory_rb[2][tb].pUserArea = src;			// Write new pointer
   }
   for(ta=0; ta<z80_data[2].write_bank.count; ta++){
      tb = z80_data[2].write_bank.list[ta];			// Get bank pos
      Z80_memory_wb[2][tb].pUserArea = src;			// Write new pointer
   }
}

void Z80DSetBank(UINT8 *src)
{
   UINT32 ta,tb;

   // Update base pointer (if called during emulation)

   z80Base = src;

   // Update base pointer (if called outside emulation)

   Z80_context[3].z80Base = src;

   z80_data[3].base_ram = Z80_context[3].z80Base;

   for(ta=0; ta<z80_data[3].read_bank.count; ta++){
      tb = z80_data[3].read_bank.list[ta];			// Get bank pos
      Z80_memory_rb[3][tb].pUserArea = src;			// Write new pointer
   }
   for(ta=0; ta<z80_data[3].write_bank.count; ta++){
      tb = z80_data[3].write_bank.list[ta];			// Get bank pos
      Z80_memory_wb[3][tb].pUserArea = src;			// Write new pointer
   }
}

/*
 *  Fill in the basic structures via these functions...
 */

// FIRST EMULATED Z80

void AddZ80AROMBase(UINT8 *d0, UINT16 d1, UINT16 d2)
{
   Z80_context[0].z80Base = d0;
   z80_data[0].base_ram = Z80_context[0].z80Base;
   Z80_context[0].z80intAddr = d1;
   Z80_context[0].z80nmiAddr = d2;
}

void AddZ80AReadByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_rb(0, d0, d1, d2, d3);
}

void AddZ80AWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_wb(0, d0, d1, d2, d3);
}

void AddZ80AReadPort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_rb(0, d0, d1, d2, d3);
}

void AddZ80AWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_wb(0, d0, d1, d2, d3);
}

void AddZ80AInit(void)
{
   Z80_context[0].z80MemRead  = Z80_memory_rb[0];
   Z80_context[0].z80MemWrite = Z80_memory_wb[0];
   Z80_context[0].z80IoRead   = Z80_port_rb[0];
   Z80_context[0].z80IoWrite  = Z80_port_wb[0];

   AddLoadCallback(Z80A_load_update);
   AddSaveData(SAVE_Z80_0, (UINT8 *) &Z80_context[0], sizeof(Z80_context[0]));

   MZ80Engine=1;
}

/*
 *  Fill in the basic structures via these functions...
 */

// SECOND EMULATED Z80

void AddZ80BROMBase(UINT8 *d0, UINT16 d1, UINT16 d2)
{
   Z80_context[1].z80Base=d0;
   z80_data[1].base_ram = Z80_context[1].z80Base;
   Z80_context[1].z80intAddr=d1;
   Z80_context[1].z80nmiAddr=d2;
}

void AddZ80BReadByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_rb(1, d0, d1, d2, d3);
}

void AddZ80BWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_wb(1, d0, d1, d2, d3);
}

void AddZ80BReadPort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_rb(1, d0, d1, d2, d3);
}

void AddZ80BWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_wb(1, d0, d1, d2, d3);
}

void AddZ80BInit(void)
{
   Z80_context[1].z80MemRead  = Z80_memory_rb[1];
   Z80_context[1].z80MemWrite = Z80_memory_wb[1];
   Z80_context[1].z80IoRead   = Z80_port_rb[1];
   Z80_context[1].z80IoWrite  = Z80_port_wb[1];

   AddLoadCallback(Z80B_load_update);
   AddSaveData(SAVE_Z80_1, (UINT8 *) &Z80_context[1], sizeof(Z80_context[1]));

   MZ80Engine=2;
}

/*
 *  Fill in the basic structures via these functions...
 */

// THIRD EMULATED Z80

void AddZ80CROMBase(UINT8 *d0, UINT16 d1, UINT16 d2)
{
   Z80_context[2].z80Base=d0;
   z80_data[2].base_ram = Z80_context[2].z80Base;
   Z80_context[2].z80intAddr=d1;
   Z80_context[2].z80nmiAddr=d2;
}

void AddZ80CReadByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_rb(2, d0, d1, d2, d3);
}

void AddZ80CWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_wb(2, d0, d1, d2, d3);
}

void AddZ80CReadPort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_rb(2, d0, d1, d2, d3);
}

void AddZ80CWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_wb(2, d0, d1, d2, d3);
}

void AddZ80CInit(void)
{
   Z80_context[2].z80MemRead  = Z80_memory_rb[2];
   Z80_context[2].z80MemWrite = Z80_memory_wb[2];
   Z80_context[2].z80IoRead   = Z80_port_rb[2];
   Z80_context[2].z80IoWrite  = Z80_port_wb[2];

   AddLoadCallback(Z80C_load_update);
   AddSaveData(SAVE_Z80_2, (UINT8 *) &Z80_context[2], sizeof(Z80_context[2]));

   MZ80Engine=3;
}

/*
 *  Fill in the basic structures via these functions...
 */

// FOURTH EMULATED Z80

void AddZ80DROMBase(UINT8 *d0, UINT16 d1, UINT16 d2)
{
   Z80_context[3].z80Base=d0;
   z80_data[3].base_ram = Z80_context[3].z80Base;
   Z80_context[3].z80intAddr=d1;
   Z80_context[3].z80nmiAddr=d2;
}

void AddZ80DReadByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_rb(3, d0, d1, d2, d3);
}

void AddZ80DWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3)
{
   add_mz80_memory_region_wb(3, d0, d1, d2, d3);
}

void AddZ80DReadPort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_rb(3, d0, d1, d2, d3);
}

void AddZ80DWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3)
{
   add_mz80_port_region_wb(3, d0, d1, d2, d3);
}

void AddZ80DInit(void)
{
   Z80_context[3].z80MemRead  = Z80_memory_rb[3];
   Z80_context[3].z80MemWrite = Z80_memory_wb[3];
   Z80_context[3].z80IoRead   = Z80_port_rb[3];
   Z80_context[3].z80IoWrite  = Z80_port_wb[3];

   AddLoadCallback(Z80D_load_update);
   AddSaveData(SAVE_Z80_3, (UINT8 *) &Z80_context[3], sizeof(Z80_context[3]));

   MZ80Engine=4;
}

void Z80A_load_update(void)
{
   #ifdef RAINE_DEBUG
   print_debug("Z80A Load Callback()\n");
   #endif

   Z80_context[0].z80MemRead  = Z80_memory_rb[0];
   Z80_context[0].z80MemWrite = Z80_memory_wb[0];
   Z80_context[0].z80IoRead   = Z80_port_rb[0];
   Z80_context[0].z80IoWrite  = Z80_port_wb[0];
   Z80_context[0].z80Base     = z80_data[0].base_ram;
}

void Z80B_load_update(void)
{
   #ifdef RAINE_DEBUG
   print_debug("Z80B Load Callback()\n");
   #endif

   Z80_context[1].z80MemRead  = Z80_memory_rb[1];
   Z80_context[1].z80MemWrite = Z80_memory_wb[1];
   Z80_context[1].z80IoRead   = Z80_port_rb[1];
   Z80_context[1].z80IoWrite  = Z80_port_wb[1];
   Z80_context[1].z80Base     = z80_data[1].base_ram;
}

void Z80C_load_update(void)
{
   #ifdef RAINE_DEBUG
   print_debug("Z80C Load Callback()\n");
   #endif

   Z80_context[2].z80MemRead  = Z80_memory_rb[2];
   Z80_context[2].z80MemWrite = Z80_memory_wb[2];
   Z80_context[2].z80IoRead   = Z80_port_rb[2];
   Z80_context[2].z80IoWrite  = Z80_port_wb[2];
   Z80_context[2].z80Base     = z80_data[2].base_ram;
}

void Z80D_load_update(void)
{
   #ifdef RAINE_DEBUG
   print_debug("Z80D Load Callback()\n");
   #endif

   Z80_context[3].z80MemRead  = Z80_memory_rb[3];
   Z80_context[3].z80MemWrite = Z80_memory_wb[3];
   Z80_context[3].z80IoRead   = Z80_port_rb[3];
   Z80_context[3].z80IoWrite  = Z80_port_wb[3];
   Z80_context[3].z80Base     = z80_data[3].base_ram;
}

void ClearZ80List(void)
{
   UINT32 ta;

   for(ta = 0; ta < MAX_Z80; ta ++){

      memory_count_rb[ta] = 0;
      memory_count_wb[ta] = 0;

      port_count_rb[ta] = 0;
      port_count_wb[ta] = 0;

      z80_data[ta].read_bank.count = 0;
      z80_data[ta].write_bank.count = 0;
   }
}

void StopZ80(UINT16 address, UINT8 data)
{
   #ifdef RAINE_DEBUG
   print_debug("[StopZ80]\n");
   #endif
   mz80ReleaseTimeslice();
}


static UINT16 StopAddress=0;

void SetStopZ80Mode2(UINT16 address)
{
   StopAddress=address;
}

void StopZ80Mode2(UINT16 address, UINT8 data)
{
   #ifdef RAINE_DEBUG
   print_debug("[StopZ80]\n");
   #endif
   z80pc=StopAddress;
   mz80ReleaseTimeslice();
}

static UINT16 StopAddressB=0;

void SetStopZ80BMode2(UINT16 address)
{
   StopAddressB=address;
}

void StopZ80BMode2(UINT16 address, UINT8 data)
{
   #ifdef RAINE_DEBUG
   print_debug("[StopZ80B]\n");
   #endif
   z80pc=StopAddressB;
   mz80ReleaseTimeslice();
}

static UINT16 StopAddressC=0;

void SetStopZ80CMode2(UINT16 address)
{
   StopAddressC=address;
}

void StopZ80CMode2(UINT16 address, UINT8 data)
{
   #ifdef RAINE_DEBUG
   print_debug("[StopZ80C]\n");
   #endif
   z80pc=StopAddressC;
   mz80ReleaseTimeslice();
}

static UINT16 StopAddressD=0;

void SetStopZ80DMode2(UINT16 address)
{
   StopAddressD=address;
}

void StopZ80DMode2(UINT16 address, UINT8 data)
{
   #ifdef RAINE_DEBUG
   print_debug("[StopZ80D]\n");
   #endif
   z80pc=StopAddressD;
   mz80ReleaseTimeslice();
}

UINT16 DefBadReadZ80(UINT16 offset)
{
   #ifdef RAINE_DEBUG
      print_debug("Z80BadRead(%04x) [%04x]\n",offset,z80pc);
   #endif
   return(0xFF);
}

void DefBadWriteZ80(UINT16 offset, UINT8 data)
{
   #ifdef RAINE_DEBUG
      print_debug("Z80BadWrite(%04x,%02x) [%04x]\n",offset,data,z80pc);
   #endif
}


void WriteMZ80Byte(UINT32 address, UINT8 data)
{
   int ta;

   for(ta=0;ta<99;ta++){
      if((Z80_memory_wb[1][ta].lowAddr)==-1){
         ta=99;
         #ifdef RAINE_DEBUG
            print_debug("Wb(%04x,%02x) [Via WriteMZ80Byte]\n",address,data);
         #endif
      }
      else{
         if((address>=Z80_memory_wb[1][ta].lowAddr)&&(Z80_memory_wb[1][ta].highAddr>=address)){
            if(Z80_memory_wb[1][ta].memoryCall==NULL){
               WriteByte( ((UINT8 *) Z80_memory_wb[1][ta].pUserArea) + address,data);
               ta=99;
            }
            //else{
            //   *MC68000A_memoryall[ta].memorycall(address,data);
            //}
         }
      }
   }
}

UINT8 ReadMZ80Byte(UINT32 address)
{
   int ta;

   for(ta=0;ta<99;ta++){
      if((Z80_memory_rb[1][ta].lowAddr)==-1){
         #ifdef RAINE_DEBUG
            print_debug("Rb(%04x) [Via ReadMZ80Byte]\n",address);
         #endif
         return(0x00);
      }
      else{
         if((address>=Z80_memory_rb[1][ta].lowAddr)&&(Z80_memory_rb[1][ta].highAddr>=address)){
            if(Z80_memory_rb[1][ta].memoryCall==NULL){
               return ReadByte( ((UINT8 *) Z80_memory_rb[1][ta].pUserArea) + address);
            }
         }
      }
   }

   return 0x00;
}

// usefull to fix music tempo with mz80 in games using a 2151...
// See toaplan2.c and cps1.c on how to use.

static int cputick=0,
calc_temp=0;

int onef_cycle=0;

int cpu_calc(int len)    // len=sample of second / 60fps
{
        int bytes;
	if (!len) return 0; // no sound
        if (!calc_temp && onef_cycle) { calc_temp=onef_cycle/len; }
        cputick+= mz80GetElapsedTicks(TRUE);
	if (cputick >= calc_temp && calc_temp) {
	  bytes = cputick/calc_temp;
	  cputick%=calc_temp;
	  return bytes;
	}
        return 0;
}
