/******************************************************************************/
/*                                                                            */
/*          RAINE MAIN EMULATION ROUTINES (run game, reset game etc)          */
/*                                                                            */
/******************************************************************************/

#include "raine.h"		// General defines and stuff
#include "emumain.h"		// main loops
#include "gui.h"		// Interface stuff
#include "savegame.h"		// Save/Load game stuff
#include "sasound.h"		// Sound Sample stuff
#include "dsw.h"		// Dipswitch stuff
#include "ingame.h"		// screen handling routines
#include "control.h"		// Control/Input related stuff
#include "debug.h"		// Debugging routines
#include "config.h"		// Command Line + Config file routines
#include "hiscore.h"
#include "palette.h"
#include "dlg_sound.h"

#ifdef HAVE_6502
#include "m6502.h"		// M6502 Engine
#include "m6502hlp.h"		// M6502 Support Interface
#endif
#include "68020.h"		// M68020 Engine + Support Interface
#include "games.h"		// Game list

#include "cpumain.h"		// main loops
#include "newspr.h"

extern int StreamMode;

static UINT32 cpu_fps;
static UINT32 quit_loop;

static void frame_skip_up(void)
{
   if((display_cfg.frame_skip<9) && (display_cfg.frame_skip)){
      display_cfg.frame_skip++;
      print_ingame(120,"Drawing Every %1d Frames",display_cfg.frame_skip);
   }
}

static void frame_skip_down(void)
{
   if((display_cfg.frame_skip>1) && (display_cfg.frame_skip)){
      display_cfg.frame_skip--;
      if(display_cfg.frame_skip==1)
         print_ingame(120,"Drawing All Frames");
      else
         print_ingame(120,"Drawing Every %1d Frames",display_cfg.frame_skip);
   }
}

static void cpu_slow_down(void)
{
   if(cpu_fps<16){
      cpu_fps++;
      print_ingame(300,"cpusldn:Running CPU Every %01d Frames",cpu_fps);
   }
}

static void cpu_speed_up(void)
{
   if(cpu_fps>1){
      cpu_fps--;
      if(cpu_fps==1)
         print_ingame(120,"Running CPU All Frames");
      else
         print_ingame(120,"Running CPU Every %01d Frames",cpu_fps);
   }
}

static void next_save_slot(void)
{
   SaveSlot++;

   if(SaveSlot>9)
      SaveSlot=0;

   print_ingame(120,"Save Slot %01d Selected",SaveSlot);
}

static void key_save_screen(void)
{
   raine_cfg.req_save_screen = 1;
}

static void key_pause_game(void)
{
   raine_cfg.req_pause_game = 1;
}

static void key_unpause_game(void)
{
   raine_cfg.req_pause_game = 0;
}

static void key_pause_scroll_up(void)
{
   raine_cfg.req_pause_scroll |= 1;
}

static void key_pause_scroll_down(void)
{
   raine_cfg.req_pause_scroll |= 2;
}

static void key_pause_scroll_left(void)
{
   raine_cfg.req_pause_scroll |= 4;
}

static void key_pause_scroll_right(void)
{
   raine_cfg.req_pause_scroll |= 8;
}

extern int old_draw; // dlg_sound

static void key_stop_emulation_esc(void)
{
  if (!old_draw) {
    quit_loop = 1;
    raine_cfg.req_pause_game = 0;
  }
}

static void key_stop_emulation_tab(void)
{
  if (!old_draw) {
   quit_loop = 2;
   raine_cfg.req_pause_game = 0;
  }
}

typedef struct CORE_CTRL
{
   UINT32 key;
   void  (*proc)();
   UINT32 status;
} CORE_CTRL;

/*

ingame emulator controls

*/

static struct CORE_CTRL core_ctrl_list[] =
{
   { KB_EMU_SCREENSHOT,        key_save_screen,        },
   { KB_EMU_INC_FRAMESKIP,     frame_skip_up,          },
   { KB_EMU_DEC_FRAMESKIP,     frame_skip_down,        },
   { KB_EMU_INC_CPU_FRAMESKIP, cpu_speed_up,           },
   { KB_EMU_DEC_CPU_FRAMESKIP, cpu_slow_down,          },
   { KB_EMU_SAVE_GAME,         GameSave,               },
   { KB_EMU_SWITCH_SAVE_SLOT,  next_save_slot,         },
   { KB_EMU_LOAD_GAME,         GameLoad,               },
   { KB_EMU_SWITCH_FPS,        switch_fps_mode,        },
   { KB_EMU_PAUSE_GAME,        key_pause_game,         },
   { KB_EMU_STOP_EMULATION,    key_stop_emulation_esc, },
   { KB_EMU_OPEN_GUI,          key_stop_emulation_tab, },
   { KB_EMU_SWITCH_MIXER,      switch_mixer, },
   { 0,                        NULL,                   },
};

/*

pause mode emulator controls

*/

static struct CORE_CTRL core_ctrl_list_paused[] =
{
   { KB_EMU_PAUSE_GAME,           key_unpause_game,       },
   { KB_EMU_STOP_EMULATION,       key_stop_emulation_esc, },
   { KB_EMU_OPEN_GUI,             key_stop_emulation_tab, },
   { KB_EMU_SCREEN_UP    | 0x100, key_pause_scroll_up,    },
   { KB_EMU_SCREEN_DOWN  | 0x100, key_pause_scroll_down,  },
   { KB_EMU_SCREEN_LEFT  | 0x100, key_pause_scroll_left,  },
   { KB_EMU_SCREEN_RIGHT | 0x100, key_pause_scroll_right, },
   { 0,                           NULL,                   },
};


static void init_control_list(CORE_CTRL *ctrl_list)
{

   while(ctrl_list->proc){

      if(ctrl_list->key & 0x100)

         ctrl_list->status = 0;

      else

         ctrl_list->status = 1;

      ctrl_list++;

   }

}


void init_gui_inputs(void)
{
   init_control_list(core_ctrl_list);
}


void init_gui_inputs_paused(void)
{
   init_control_list(core_ctrl_list_paused);
}


static void parse_control_list(CORE_CTRL *ctrl_list)
{

   while(ctrl_list->proc){

      if(ctrl_list->key & 0x100){

         if((key[def_input_list_emu[ctrl_list->key & 0xFF].scancode]) ||
           (rjoy[def_input_list_emu[ctrl_list->key & 0xFF].joycode])){
               ctrl_list->proc();
         }

      }
      else{

         if((key[def_input_list_emu[ctrl_list->key & 0xFF].scancode]) ||
           (rjoy[def_input_list_emu[ctrl_list->key & 0xFF].joycode])){
            if(!ctrl_list->status){
               ctrl_list->status = 1;
               ctrl_list->proc();
            }
         }
         else{
            ctrl_list->status = 0;
         }

      }

      ctrl_list++;

   }

}


void update_gui_inputs(void)
{
   parse_control_list(core_ctrl_list);

   check_layer_switches();
}


void update_gui_inputs_paused(void)
{
   parse_control_list(core_ctrl_list_paused);
}


UINT32 run_game_emulation(void)
{
   UINT32 cpu_tick;
   UINT32 draw_screen;
   VIDEO_INFO *vid_info;
   char str[256];

   #ifdef RAINE_DEBUG
   print_debug("run_game_emulation(): Setting up...\n");
   #endif

   cpu_tick = 0;
   quit_loop = 0;
   raine_cfg.req_pause_game = 0;
   raine_cfg.req_save_screen = 0;

   #ifdef RAINE_DEBUG
   print_debug("Init Timer...\n");
   #endif
   hs_init();
   start_ingame_timer();

   #ifdef RAINE_DEBUG
   print_debug("Init Keyboard...\n");
   #endif
   install_keyboard();

   init_gui_inputs();

   #ifdef RAINE_DEBUG
   print_debug("Init Screen Buffer...\n");
   #endif
   SetupScreenBitmap();

   #ifdef RAINE_DEBUG
   print_debug("Init Video Core...\n");
   #endif
   init_video_core();

   if(GameMouse){
   #ifdef RAINE_DEBUG
   print_debug("Init mouse...\n");
   #endif
   install_mouse();
   show_mouse(NULL);
   }

   #ifdef RAINE_DEBUG
   print_debug("Init LED Emulation...\n");
   #endif
   force_update_leds();

   #ifdef RAINE_DEBUG
   print_debug("Init Colour Mapping...\n");
   #endif
   reset_palette_map();

   #ifdef TRIPLE_BUFFER
   #ifdef RAINE_DEBUG
   print_debug("Init triple buffer...\n");
   #endif
   reset_triple_buffer();
   #endif

   #ifdef RAINE_DEBUG
   print_debug("Init cpu emulation...\n");
   #endif
   start_cpu_main();

   #ifdef RAINE_DEBUG
   print_debug("run_game_emulation(): Running main loop...\n");
   #endif

   vid_info = current_game->video_info;

   if(!vid_info->draw_game) quit_loop = 1; // I want the game to always draw
   
#ifdef RAINE_DEBUG
   print_debug("Reset ingame timer...\n");
#endif
   reset_ingame_timer(); // For sound we'd better init this here...

   // Placing sound init just after the timer init should avoid begining
   // streaming out of a frame limit (it happens sometimes in linux !)
   #ifdef RAINE_DEBUG
   print_debug("Init Sound...\n");
   #endif
   if(GameSound){
     if(SoundEnabled){
       if(RaineSoundCard){
	 audio_sample_rate = SampleRate;
	 saInitSoundCard( RaineSoundCard, StreamMode, audio_sample_rate );
       }
     }
   }
   
   // auto-save
   
   if (raine_cfg.auto_save){
     sprintf(str,"%s.sva",current_game->main_name);
     do_load_state(str);
   }
	
   while(!quit_loop){

     /* Handle sound FIRST : it is the most sensitive part for synchronisatino
      * So it MUST be at the begining (before getting any chance to be out
      * of sync !!! Too bad allegro does not seem to care very much about this
      */
     
      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStart(PRO_SOUND);
      #endif

      saUpdateSound(1);

      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStop(PRO_SOUND);
      #endif
     
      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStart(PRO_FRAME);
      #endif

      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStart(PRO_CPU);
      #endif

      cpu_tick++;
      if(cpu_tick>=cpu_fps){
         cpu_tick=0;
         if(current_game->exec_frame) current_game->exec_frame();
      }

      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStop(PRO_CPU);
      #endif

      draw_screen = 0;
      cpu_frame_count ++;
      skip_frame_count ++;

      if(display_cfg.frame_skip){		// Manual frame skip
         if(skip_frame_count >= display_cfg.frame_skip)
            draw_screen = 1;
      }
      else{					// Automatic frame skip
         if((read_ingame_timer() <= cpu_frame_count)||(skip_frame_count >= 60))
            draw_screen = 1;
      }

      // Cheats are now updated in the overlay_interface in ingame.c (watches)
      if(draw_screen){

      // update video emulation bitmap

      hs_update();
      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStart(PRO_RENDER);
      #endif

      vid_info->draw_game();
      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStop(PRO_RENDER);
      #endif

      // blit video emulation to pc hardware, also prints ingame messages

      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStart(PRO_BLIT);
      #endif

      BlitScreen();

      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2) ProfileStop(PRO_BLIT);
      #endif

      skip_frame_count = 0;
      render_frame_count ++;

      // limit speed if we need to

      if((display_cfg.limit_speed) && (read_ingame_timer() < cpu_frame_count)){

         #ifdef RDTSC_PROFILE
         if(raine_cfg.show_fps_mode>2) ProfileStart(PRO_FREE);
         #endif

         while(read_ingame_timer() < cpu_frame_count){	// Yikes! We're going TOO FAST!!
         }
         #ifdef RDTSC_PROFILE
         if(raine_cfg.show_fps_mode>2) ProfileStop(PRO_FREE);
         #endif

      }
      
      }

      update_leds();

      update_rjoy_list();

      update_inputs();

      update_gui_inputs();

      #ifdef RDTSC_PROFILE
      if(raine_cfg.show_fps_mode>2){
         ProfileStop(PRO_FRAME);
         UpdateProfile();
      }
      #endif

   }

   // auto-save
      
   if (raine_cfg.auto_save){
     sprintf(str,"%s.sva",current_game->main_name);
     do_save_state(str);
   }
   
   #ifdef RAINE_DEBUG
   print_debug("run_game_emulation(): Closing down...\n");
   #endif

   if(GameMouse){
   #ifdef RAINE_DEBUG
   print_debug("Free mouse...\n");
   #endif
   remove_mouse();
   }

   #ifdef TRIPLE_BUFFER
   #ifdef RAINE_DEBUG
   print_debug("Free triple buffer...\n");
   #endif
   reset_triple_buffer();
   #endif

   DestroyScreenBitmap();

   saDestroySound(0);

   clear_keybuf();
   remove_keyboard();

   #ifdef RAINE_DEBUG
   print_debug("Free cpu emulation...\n");
   #endif
   stop_cpu_main();

   #ifdef RAINE_DEBUG
   print_debug("Free ingame timer...\n");
   #endif
   stop_ingame_timer();

   #ifdef RAINE_DEBUG
   print_debug("run_game_emulation(): Completed.\n");
   #endif

   return quit_loop - 1;
}

#ifndef NO020
extern int MC68020; // newcpu.c
#endif

extern int int7_active; // f3system (sound)

void reset_game_hardware(void)
{
   #ifdef RAINE_DEBUG
   print_debug("reset_game_hardware(): Start\n");
   #endif

   cpu_fps = 1;

   clear_ingame_message_list();

   start_cpu_main();

   if(StarScreamEngine>=1){
      cpu_reset(CPU_68K_0);
      print_ingame(120,"CPU M68000#0 Reset");
   }
   if(StarScreamEngine>=2){
      cpu_reset(CPU_68K_1);
      print_ingame(120,"CPU M68000#1 Reset");
   }

#ifndef NO020
   if(MC68020){
      cpu_reset(CPU_M68020_0);
      print_ingame(120,"CPU M68020#0 Reset");
      int7_active = 0;
   }
#endif

   if(MZ80Engine>=1){
      cpu_reset(CPU_Z80_0);
      print_ingame(120,"CPU Z80#0 Reset");
   }
   if(MZ80Engine>=2){
      cpu_reset(CPU_Z80_1);
      print_ingame(120,"CPU Z80#1 Reset");
   }
   if(MZ80Engine>=3){
      cpu_reset(CPU_Z80_2);
      print_ingame(120,"CPU Z80#2 Reset");
   }
   if(MZ80Engine>=4){
      cpu_reset(CPU_Z80_3);
      print_ingame(120,"CPU Z80#3 Reset");
   }

#ifdef HAVE_6502
   if(M6502Engine>=1){
      m6502SetContext(&M6502_context[0]);
      m6502reset();
      m6502GetContext(&M6502_context[0]);

      print_ingame(120,"CPU M6502#0 Reset");
   }
   if(M6502Engine>=2){
      m6502SetContext(&M6502_context[1]);
      m6502reset();
      m6502GetContext(&M6502_context[1]);

      print_ingame(120,"CPU M6502#1 Reset");
   }
   if(M6502Engine>=3){
      m6502SetContext(&M6502_context[2]);
      m6502reset();
      m6502GetContext(&M6502_context[2]);

      print_ingame(120,"CPU M6502#2 Reset");
   }
#endif

   stop_cpu_main();

   init_leds();

   #ifdef RAINE_DEBUG
   print_debug("reset_game_hardware(): OK\n");
   #endif
}
