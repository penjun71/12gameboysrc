/**************************************************************/
/*   MSM5205 (DARIUS ADPCM chip) control                      */
/**************************************************************/
#ifndef __MSM5205_H__
#define __MSM5205_H__

#define MSM5205BF_MAX     (2)

#define MSM5205_MONO    (0)
#define MSM5205_STEREO  (1)

struct msm5205_adpcm_list {
  unsigned int start, end;
};

struct MSM5205buffer_interface {
  int num;
  int sample_rate[MSM5205BF_MAX];
  int volume[MSM5205BF_MAX];
  struct msm5205_adpcm_list *list[MSM5205BF_MAX];
  int listsize[MSM5205BF_MAX];
  unsigned char *rom[MSM5205BF_MAX];
  unsigned int  romsize[MSM5205BF_MAX];
  int updatemode;
};

int MSM5205buffer_sh_start( struct MSM5205buffer_interface *interface );
void MSM5205buffer_sh_stop( void );

void MSM5205buffer_UpdateOne( int num, void *buffer, int length );
void MSM5205buffer_Stereo_UpdateOne( int num, void **buffer, int length );


void MSM5205buffer_request( int num, int code );
void MSM5205buffer_setpan( int num, int data );

#endif
/**************** end of file ****************/
