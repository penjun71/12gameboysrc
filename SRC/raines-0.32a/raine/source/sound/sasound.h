/*
 *  SAMPLE (ADPCM) SUPPORT FOR RAINE
 *  modified by Hiromitsu Shioya
 *  change SEAL library 09/20/98
 */

#ifndef _SAMPLE_H_
#define _SAMPLE_H_

#include <stdio.h>
#include "deftypes.h"

#undef INLINE
#define INLINE DEF_INLINE

/* audio related stuff */
#define MAX_STREAM_CHANNELS 16
#define    NUMVOICES    (MAX_STREAM_CHANNELS)

#define DEF_STREAM_BUFFER_MAXA   (16)
#define DEF_MODEB_FRAME_SIZE   (60)
#define DEF_MODEB_UPDATE_COUNT   (3)
#define DEF_STREAM_BUFFER_MAXB   ((DEF_FRAME_SIZE/DEF_MODEB_UPDATE_COUNT))
#define DEF_STREAM_UPDATE_ERROR_MAX   (16) /* error wait is 16sec. */

int STREAM_BUFFER_MAXA;
int STREAM_BUFFER_MAXB;
int MODEB_UPDATE_COUNT;
int MODEB_FRAME_SIZE;
int MODEB_MASK;
int MODEB_ERROR_MAX;

int RaineSoundCardTotal;
int RaineSoundCard;

/**** add hiro-shi 10/30/98 ****/
int DariusStereo;

int change_sample_rate;

extern int SampleRate, audio_sample_rate;

extern int vchan[NUMVOICES];
extern int ventry[NUMVOICES];
extern int vbover_err, vbunder_err;

enum {
  SOUND_STREAM_NORMAL = 0,
  SOUND_STREAM_WAIT
};

extern int sound_stream_mode;

#define   SND_CONTROL_MAX   (3)

typedef struct soundrec
{
   int  first;					// 0 = Sound emulator not init; 1 = Sound emulators init
   int  init[SND_CONTROL_MAX];			// Emulator type (see inittype)
   void *intf[SND_CONTROL_MAX];			// Emulator specific interface
   int  control_max;				// Number of emulators in list
} SoundRec;

/**** pcm support ****/
#ifdef __RAINE__
int ProfileSound;
UINT8 *YM2610_Rompointers[2];
UINT32 YM2610_Romsizes[2];
UINT8 *PCMROM;			/* hiro-shi */
#endif
/************************************************/
/*    include some headers                      */
/************************************************/
#ifndef RAINECPS
#define HAS_YM2203  1
#include "2203intf.h"
// Note that the 2608 is still unused.
// Note also that 2608intf.c is not finished !!!
#define HAS_YM2608  0
#define HAS_YM2610  1
#define HAS_YM2610B 1
#include "2610intf.h"
#define HAS_AY8910 1
#include "ay8910.h"
#define HAS_M6585 1
#include "m6585.h"

// fmopl
#define HAS_YM3812 1
#include "3812intf.h"
#define HAS_MSM5205 1
#include "msm5205.h"
#define HAS_YMZ280B 1
#include "ymz280b.h"
#define HAS_ES5505 1
#include "es5506.h"
#define HAS_SMP16 1
#include "smp16bit.h"

#endif

#define HAS_YM2151  1
#define HAS_YM2151_ALT 1
#include "2151intf.h"
#define HAS_ADPCM 1
#include "adpcm.h"
#define HAS_QSOUND 1
#include "qsound.h"

/*

do not modify this order without updating sound_chip_list[] in sasound.c as well

*/

enum inittype
{
  SOUND_YM2203 = 0,
  SOUND_YM2151S,
  SOUND_YM2151J,
  SOUND_YM2610,
  SOUND_YM2610B,
  SOUND_MSM5205,
  SOUND_M6295,
  SOUND_PSG,
  SOUND_YM3812,
  SOUND_SN76496,
  SOUND_YM2413Hard,
  SOUND_SMP16,
  SOUND_M6585,
  SOUND_YMZ280B,
  SOUND_ES5505,
  SOUND_ES5506,
  SOUND_QSOUND,
  SOUND_NONE,
};

typedef struct SOUND_CHIP
{
   UINT8 *name;			// name
   void (*shutdown)(void);	// //stop
} SOUND_CHIP;

extern struct SOUND_CHIP sound_chip_list[];

extern SoundRec      *SndMachine, snd_entry;

/**** prottype ****/
void saUpdateSound( int nowclock );
BOOL saInitSoundCard( int soundcard, int buffer_mode, int sample_rate );

char *get_sound_chip_name(UINT32 id);

void init_sound_list(void);

void saDestroySound( int remove_all_resources );
void saStopSoundEmulators(void);

void saPlayBufferedStreamedSampleBase( int channel, signed char *data, int len, int freq, int volume, int bits , int pan );

void saPlayStreamedSampleBase( int channel, signed char *data, int len, int freq, int volume, int bits , int pan );

int saGetPlayChannels( int request );
void saResetPlayChannels( void );

void sa_pause_sound(void);
void sa_unpause_sound(void);

/******************************************************************************/
/*                                                                            */
/*                          'MAME' STREAMS INTERFACE                          */
/*                                                                            */
/******************************************************************************/
/* Notice : panning is not (yet) enabled... */
#define OSD_PAN_CENTER 0x80
#define OSD_PAN_LEFT   0x00
#define OSD_PAN_RIGHT  0xff

#define MIXER_PAN_CENTER OSD_PAN_CENTER
#define MIXER_PAN_LEFT OSD_PAN_LEFT
#define MIXER_PAN_RIGHT OSD_PAN_RIGHT

/*
 #define OSD_PAN_CENTER 0
 #define OSD_PAN_LEFT   1
 #define OSD_PAN_RIGHT  2
*/
#include "streams.h"

/******************************************************************************/
/*                                                                            */
/*                       HIROSHI VOLUME-PANNING CONTROL                       */
/*                                                                            */
/******************************************************************************/

extern int SampleVol[MAX_STREAM_CHANNELS];
extern int SamplePan[MAX_STREAM_CHANNELS];

void saInitVolPan( void );
void saSetVolume( int num, int data );
void saSetPan( int num, int data );
void saSetPanMulti( int num, int data );
void resync_voice(int channel);

/******************************************************************************/
/*                                                                            */
/*                          SOUND CARD INFORMATION                            */
/*                                                                            */
/******************************************************************************/

char *sound_card_name(int num);

/******************************* END OF FILE **********************************/

#endif // _SAMPLE_H_
