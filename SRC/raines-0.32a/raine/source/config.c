/******************************************************************************/
/*                                                                            */
/*                            RAINE COMMAND LINE                              */
/*                                                                            */
/******************************************************************************/

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <zlib.h>

#include "raine.h"		// General defines and stuff
#include "gui.h"		// Interface stuff
#include "savegame.h"		// Save/Load game stuff
#include "sasound.h"		// Sound Sample stuff
#include "dsw.h"		// Dipswitch stuff
#include "ingame.h"		// screen handling routines
#include "arpro.h"		// Action Replay stuff
#include "control.h"		// Control/Input related stuff
#include "debug.h"		// Debugging routines
#include "config.h"		// config and command parsing routines
#include "games.h"		// Games List
#include "loadroms.h"		//

static int ArgCount;		// Number of arguments in the command line
static int ArgPosition;		// Current position in the argument list
static char *ArgList[256];	// Pointers to each argument string

static int verbose;		// show extra information for some options

/*

process the -help/-?/--help/-h option

*/

static void CLI_Help(void)
{
   int private=0;
   int i;

#ifndef PRIVATE
   for(i=0; i<game_count; i++)
      if (game_list[i]->flags & GAME_PRIVATE)
         private++;
#endif

   allegro_message(
	"USE: Raine <commands> <options>\n"
	"\n"
	"Commands:\n"
	"\n"
	"-game/-g [gamename]            : Select a game to load (see game list)\n"
	"-help/-h/-?/--help             : Show command line options and list games\n"
	"-joystick/-j [type]            : Select joystick type (see list in raine.cfg)\n"
	"-limitspeed/-l                 : Limit emulation speed to 60fps\n"
	"-gamelist/-gl                  : Quick list of all games\n"
	"-gameinfo/-listinfo <gamename> : List info for a game, or all games\n"
	"-romcheck/-rc <gamename>       : Check roms are valid for a game, or all games\n"
	"-nogui                         : Disable GUI (useful for frontends)\n"
	"-leds                          : Enable keyboard LED usage\n"
	"-noleds                        : Disable keyboard LED usage\n"
	"-cont                          : Enable continuous playing\n"
	"-nocont                        : Disable continuous playing\n"
	"-screenx/-sx [width]           : Select screen width\n"
	"-screeny/-sy [height]          : Select screen height\n"
	"-screenmode/sm [type]          : Select screen type (see list in raine.cfg)\n"
	"-bpp/-depth [number]           : Select screen colour depth\n"
	"-rotate/-r [angle]             : Rotate screen 0,90,180 or 270 degrees\n"
	"-ror                           : Rotate screen 90 degrees\n"
	"-rol                           : Rotate screen 270 degrees\n"
	"-norotate/-nor                 : Ignore default rotation in game drivers\n"
	"-flip/-f [0-3]                 : Flip screen on none, x, y or x+y axis\n"
	"-flipx/-fx                     : Flip screen on x axis\n"
	"-flipy/-fy                     : Flip screen on y axis\n"
	"-noflip/-nof                   : Ignore default flipping in game drivers\n"
	"-hide                          : Hide the gui (play at work)\n"
	"-listdsw <gamename>            : List dipswitches for a game, or all games\n"
	"\n"
	"Options:\n"
	"\n"
	"-verbose                       : Show extra information for some options\n"
	"\n"
	"Other options are available only from the GUI/config file for now.\n"
	"\n"

	"Version: " VERSION "\n"
	"Games:   %d\n"
	"\n", game_count-private
   );

   exit(1);
}

/*

process the -limitspeed/-l option

*/

static void CLI_LimitSpeed(void)
{
   display_cfg.limit_speed = 1;
}

/*

process the -nogui/-n option

*/

static void CLI_NoGUI(void)
{
   raine_cfg.no_gui = 1;
}

/*

process the -game/-g option

*/

static void CLI_game_load_alt(void)
{
   int i;

   for(i=0; i<game_count; i++){

      if(!(stricmp(ArgList[ArgPosition], game_list[i]->main_name))){
#ifndef PRIVATE
	if (!(game_list[i]->flags & GAME_PRIVATE)) {
#endif
          raine_cfg.req_load_game = 1;
          raine_cfg.req_game_index = i;

          return;
#ifndef PRIVATE
	}
#endif
      }

   }

   allegro_message(
      "Unsupported game\n"
      "Type 'Raine -gamelist' for a list of available games.\n"
#ifdef RAINE_WIN32
      "Well, actually don't do that : -gamelist is broken in win32... !\n"
#endif
      "\n"
   );
   exit(1);

}

static void CLI_game_load(void)
{
   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;
      CLI_game_load_alt();

   }
   else{

   allegro_message(
      "Unsupported game\n"
      "Type 'Raine -gamelist' for a list of available games.\n"
      "\n"
   );
   exit(1);

   }

}

// CLI_DisableLeds():
// Disable LED usage

static void CLI_DisableLeds(void)
{
   use_leds = 0;
}

// CLI_EnableLeds():
// Disable LED usage

static void CLI_EnableLeds(void)
{
   use_leds = 1;
}

// CLI_screen_x():
// Process the -screenx/-sx option

static void CLI_screen_x(void)
{
   ArgPosition++;
   sscanf(ArgList[ArgPosition], "%d", &display_cfg.screen_x);
}

// autosave

static void CLI_EnableCont(void)
{
  raine_cfg.auto_save = 1;
}

static void CLI_DisableCont(void)
{
  raine_cfg.auto_save = 0;
}

// CLI_screen_y():
// Process the -screeny/-sy option

static void CLI_screen_y(void)
{
   ArgPosition++;
   sscanf(ArgList[ArgPosition], "%d", &display_cfg.screen_y);
}

// CLI_screen_mode():
// Process the -screenmode/-sm option

typedef struct CLI_VIDEO
{
   UINT8 *name;		// Mode name/string
   UINT32 id;		// ID_ Value for this mode
} CLI_VIDEO;

static CLI_VIDEO video_options[] =
{
   { "0",		GFX_AUTODETECT			},
   { "Autodetect",	GFX_AUTODETECT			},
#ifdef GFX_VGA
   { "VGA",		GFX_VGA				},
#endif
#ifdef GFX_MODEX
   { "MODX",		GFX_MODEX			},
   { "ModeX",		GFX_MODEX			},
   { "ARCM",		GFX_ARCMON			},
#ifndef RAINE_UNIX
   { "arcade_monitor",	GFX_ARCMON			},
#endif   
#endif
#ifdef GFX_SVGA
	 { "Svgalib",		GFX_SVGALIB		},
#endif   
#ifdef GFX_DRIVER_VESA1
   { "VBE1",		GFX_VESA1			},
   { "Vesa1",		GFX_VESA1			},
#endif
#ifdef GFX_DRIVER_VESA2B
   { "VB2B",		GFX_VESA2B			},
   { "Vesa2b",		GFX_VESA2B			},
#endif
#ifdef GFX_DRIVER_VESA2L
   { "VB2L",		GFX_VESA2L			},
   { "Vesa2l",		GFX_VESA2L			},
#endif
#ifdef GFX_DRIVER_VESA3
   { "VBE3",		GFX_VESA3			},
   { "Vesa3",		GFX_VESA3			},
#endif
#ifdef GFX_DRIVER_VBEAF
   { "VBAF",		GFX_VBEAF			},
   { "Vbeaf",		GFX_VBEAF			},
#endif
#ifdef GFX_DRIVER_XTENDED
   { "XTND",		GFX_XTENDED			},
   { "extended",	GFX_XTENDED			},
#endif
#ifdef GFX_DRIVER_DIRECTX
   { "DXAC",		GFX_DIRECTX_ACCEL		},
   { "dx_accel",	GFX_DIRECTX_ACCEL		},
   { "DXSA",		GFX_DIRECTX_SAFE		},
   { "dx_safe",		GFX_DIRECTX_SAFE		},
   { "DXSO",		GFX_DIRECTX_SOFT		},
   { "dx_soft",		GFX_DIRECTX_SOFT		},
#endif
   { NULL,		0        			}
};

static void CLI_screen_mode(void)
{
   int ta;

   ArgPosition++;

   ta = 0;
   while( video_options[ta].name ){

      if(!( stricmp( video_options[ta].name, ArgList[ArgPosition] ))){

         display_cfg.screen_type = video_options[ta].id;
         return;

      }

      ta++;

   }

   allegro_message(
      "Unsupported video mode\n"
      "See raine.cfg for a list of modes\n"
      "eg. raine -screenmode vbaf (for vbe/af)\n"
      "\n"
   );
   exit(1);

}

/*

process -rotate n

*/

static void CLI_screen_rotate(void)
{
   int i;

   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;
      sscanf(ArgList[ArgPosition], "%d", &i);

   }
   else{

      i = 0;

   }

   switch(i){
      case 90:
      case 1:
         display_cfg.user_rotate = 1;
      break;
      case 180:
      case 2:
         display_cfg.user_rotate = 2;
      break;
      case 270:
      case 3:
         display_cfg.user_rotate = 3;
      break;
      default:
         display_cfg.user_rotate = 0;
      break;
   }
}

/*

process -ror

*/

static void CLI_screen_rotate_right(void)
{
   display_cfg.user_rotate = 1;
}

/*

process -rol

*/

static void CLI_screen_rotate_left(void)
{
   display_cfg.user_rotate = 3;
}

/*

process -norotate

*/

static void CLI_screen_no_rotate(void)
{
   display_cfg.no_rotate = 1;
}

/*

process -flip n

*/

static void CLI_screen_flip(void)
{
   int i;

   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;
      sscanf(ArgList[ArgPosition], "%d", &i);

   }
   else{

      i = 0;

   }

   switch(i){
      case 1:
         display_cfg.user_flip = 1;
      break;
      case 2:
         display_cfg.user_flip = 2;
      break;
      case 3:
         display_cfg.user_flip = 3;
      break;
      default:
         display_cfg.user_flip = 0;
      break;
   }
}

/*

process -flipx

*/

static void CLI_screen_flip_x(void)
{
   display_cfg.user_flip |= 1;
}

/*

process -flipy

*/

static void CLI_screen_flip_y(void)
{
   display_cfg.user_flip |= 2;
}

/*

process -noflip

*/

static void CLI_screen_no_flip(void)
{
   display_cfg.no_flip = 1;
}

/*

process -bpp

*/

static void CLI_screen_bpp(void)
{
   int i;

   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;
      sscanf(ArgList[ArgPosition], "%d", &i);

   }
   else{

      i = 0;

   }

   switch(i){
      case 8:
      case 15:
      case 16:
      case 24:
      case 32:
         display_cfg.bpp = i;
      break;
      default:
         display_cfg.bpp = 8;
      break;
   }
}

/*

process -hide

*/

static void CLI_hide(void)
{
   raine_cfg.hide = 1;
}

// CLI_Joystick():
// Process the -joystick/-j option

typedef struct CLI_JOY
{
   char *name;		// Mode name/string
   UINT32 id;		// ID_ Value for this mode
} CLI_JOY;

static CLI_JOY joy_options[] =
{
   { "-1",		JOY_TYPE_AUTODETECT	},
   { "Autodetect",	JOY_TYPE_AUTODETECT	},
   { "0",		JOY_TYPE_NONE		},
   { "None",		JOY_TYPE_NONE		},
#ifdef JOYSTICK_DRIVER_STANDARD
   { "STD",		JOY_TYPE_STANDARD	},
   { "standard",	JOY_TYPE_STANDARD	},
   { "4BUT",		JOY_TYPE_4BUTTON	},
   { "4buttons",	JOY_TYPE_4BUTTON	},
   { "6BUT",		JOY_TYPE_6BUTTON	},
   { "6buttons",	JOY_TYPE_6BUTTON	},
   { "8BUT",		JOY_TYPE_8BUTTON	},
   { "8buttons",	JOY_TYPE_8BUTTON	},
   { "2PAD",		JOY_TYPE_2PADS		},
   { "2pads",		JOY_TYPE_2PADS		},
   { "FPRO",		JOY_TYPE_FSPRO		},
   { "flightstickpro",	JOY_TYPE_FSPRO		},
   { "WING",		JOY_TYPE_WINGEX		},
   { "wingman",		JOY_TYPE_WINGEX		},
   { "wingmanextreme",	JOY_TYPE_WINGEX		},
#endif
#ifdef JOYSTICK_DRIVER_WINGWARRIOR
   { "WWAR",		JOY_TYPE_WINGWARRIOR	},
   { "wingwarrior",	JOY_TYPE_WINGWARRIOR	},
#endif
#ifdef JOYSTICK_DRIVER_SIDEWINDER
   { "SW",		JOY_TYPE_SIDEWINDER	},
   { "sidewinder",	JOY_TYPE_SIDEWINDER	},
#endif
#ifdef JOY_TYPE_SIDEWINDER_AG
   { "SWAG",		JOY_TYPE_SIDEWINDER_AG	},
   { "sidewinderag",	JOY_TYPE_SIDEWINDER_AG	},
#endif
#ifdef JOYSTICK_DRIVER_GAMEPAD_PRO
   { "GPRO",		JOY_TYPE_GAMEPAD_PRO	},
   { "gamepadpro",	JOY_TYPE_GAMEPAD_PRO	},
#endif
#ifdef JOYSTICK_DRIVER_GRIP
   { "GRIP",		JOY_TYPE_GRIP		},
   { "grip",		JOY_TYPE_GRIP		},
   { "GRI4",		JOY_TYPE_GRIP4		},
   { "grip4",		JOY_TYPE_GRIP4		},
#endif
#ifdef JOYSTICK_DRIVER_SNESPAD
   { "SNE1",		JOY_TYPE_SNESPAD_LPT1	},
   { "snespad1",	JOY_TYPE_SNESPAD_LPT1	},
   { "SNE2",		JOY_TYPE_SNESPAD_LPT2	},
   { "snespad2",	JOY_TYPE_SNESPAD_LPT2	},
   { "SNE3",		JOY_TYPE_SNESPAD_LPT3	},
   { "snespad3",	JOY_TYPE_SNESPAD_LPT3	},
#endif
#ifdef JOYSTICK_DRIVER_PSXPAD
   { "PSX1",		JOY_TYPE_PSXPAD_LPT1	},
   { "psxpad1",		JOY_TYPE_PSXPAD_LPT1	},
   { "PSX2",		JOY_TYPE_PSXPAD_LPT2	},
   { "psxpad2",		JOY_TYPE_PSXPAD_LPT2	},
   { "PSX3",		JOY_TYPE_PSXPAD_LPT3	},
   { "psxpad3",		JOY_TYPE_PSXPAD_LPT3	},
#endif
#ifdef JOYSTICK_DRIVER_N64PAD
   { "N641",		JOY_TYPE_N64PAD_LPT1	},
   { "n64pad1",		JOY_TYPE_N64PAD_LPT1	},
   { "N642",		JOY_TYPE_N64PAD_LPT2	},
   { "n64pad2",		JOY_TYPE_N64PAD_LPT2	},
   { "N643",		JOY_TYPE_N64PAD_LPT3	},
   { "n64pad3",		JOY_TYPE_N64PAD_LPT3	},
#endif
#ifdef JOYSTICK_DRIVER_DB9
   { "DB91",            JOY_TYPE_DB9_LPT1,      },
   { "db9pad1",         JOY_TYPE_DB9_LPT1,      },
   { "DB92",            JOY_TYPE_DB9_LPT2,      },
   { "db9pad2",         JOY_TYPE_DB9_LPT2,      },
   { "DB93",            JOY_TYPE_DB9_LPT3,      },
   { "db9pad3",         JOY_TYPE_DB9_LPT3,      },
#endif
#ifdef JOYSTICK_DRIVER_TURBOGRAFX
   { "TGX1",            JOY_TYPE_TURBOGRAFX_LPT1, },
   { "turbografx1",     JOY_TYPE_TURBOGRAFX_LPT1, },
   { "TGX2",            JOY_TYPE_TURBOGRAFX_LPT2, },
   { "turbografx2",     JOY_TYPE_TURBOGRAFX_LPT2, },
   { "TGX3",            JOY_TYPE_TURBOGRAFX_LPT3, },
   { "turbografx3",     JOY_TYPE_TURBOGRAFX_LPT3, },
#endif
#ifdef JOYSTICK_DRIVER_IFSEGA_ISA
   { "SEGI",		JOY_TYPE_IFSEGA_ISA	},
   { "segaisa",		JOY_TYPE_IFSEGA_ISA	},
#endif
#ifdef JOYSTICK_DRIVER_IFSEGA_PCI
   { "SEGP",		JOY_TYPE_IFSEGA_PCI	},
   { "segapci",		JOY_TYPE_IFSEGA_PCI	},
#endif
#ifdef JOYSTICK_DRIVER_IFSEGA_PCI_FAST
   { "SGPF",		JOY_TYPE_IFSEGA_PCI_FAST },
   { "segapcifast",	JOY_TYPE_IFSEGA_PCI_FAST },
#endif
#ifdef JOYSTICK_DRIVER_WIN32
   { "W32",		JOY_TYPE_WIN32		},
   { "win32",		JOY_TYPE_WIN32		},
#endif
   { NULL,		0        		}
};

static void CLI_Joystick(void)
{
   int ta;

   ArgPosition++;

   ta = 0;
   while( joy_options[ta].name ){

      if(!( stricmp( joy_options[ta].name, ArgList[ArgPosition] ))){

         JoystickType = joy_options[ta].id;
         return;

      }

      ta++;

   }

   allegro_message(
      "Unsupported joystick type\n"
      "See raine.cfg for a list of types\n"
      "eg. raine -joystick sidewinder\n"
      "\n"
   );
   exit(1);

}

// CLI_game_list():
// Output list of all games

static void CLI_game_list(void)
{
   int ta;
   int private=0;

   for(ta=0; ta<game_count; ta++)

#ifndef PRIVATE
      if (!(game_list[ta]->flags & GAME_PRIVATE))
#endif
         printf("%-8s : %-30s\n", game_list[ta]->main_name, game_list[ta]->long_name);
#ifndef PRIVATE
      else
         private++;
#endif

   printf("\n");
   printf("%d Games Supported\n\n", game_count-private);

   exit(0);
}

// CLI_Verbose():
// Output extra info for some options

static void CLI_Verbose(void)
{
   verbose = 1;
}

static void CheckGame(GAME_MAIN *game_info)
{
   DIR_INFO *dir_list;
   ROM_INFO *rom_list;
   UINT8    *ram;
   UINT8    *outbuf;
   UINT32     crc_32bit, len, bad_set;

   outbuf = malloc(0x4000);
   outbuf[0] = 0;

   printf("%-8s | %-30s", game_info->main_name, game_info->long_name);

   rom_list = game_info->rom_list;
   dir_list = game_info->dir_list;

   bad_set = 0;

   while(rom_list->name){

      sprintf(outbuf+strlen(outbuf), "rom:%-12s size:0x%08x crc32:0x%08x -- ",rom_list->name,rom_list->size,rom_list->crc32);

      ram = malloc(rom_list->size);

      if(load_rom_dir(dir_list, rom_list->name, ram, rom_list->size, rom_list->crc32)){

         len = rom_size_dir(dir_list, rom_list->name, rom_list->size, rom_list->crc32);

         if(len != rom_list->size){

            sprintf(outbuf+strlen(outbuf), "bad size: 0x%08x\n",len);
            bad_set = 1;

         }
         else{

         crc_32bit = crc32(0, ram, rom_list->size);

         if(crc_32bit != rom_list->crc32){

            sprintf(outbuf+strlen(outbuf), "bad crc32: 0x%08x\n", crc_32bit);
            bad_set = 1;

         }
         else{

            sprintf(outbuf+strlen(outbuf), "ok\n");
         }

         }

      }
      else{

         sprintf(outbuf+strlen(outbuf), "rom not found\n");
         bad_set = 1;

      }

      free(ram);

      rom_list++;

   }

   if(bad_set)

      printf(" | BAD\n");

   else

      printf(" | OK\n");


   if((verbose)||(bad_set))

      printf(outbuf);


   free(outbuf);

}

// CLI_game_check():
// Check roms for a single or all games (todo - wildstar support)

static void CLI_game_check(void)
{
   int i;

   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;

      for(i=0; i<game_count; i++){
         if(
#ifndef PRIVATE
	    (!(game_list[i]->flags & GAME_PRIVATE)) &&
#endif
             !(stricmp(ArgList[ArgPosition], game_list[i]->main_name))){
                CheckGame(game_list[i]);
                exit(0);
         }
      }

      allegro_message(
	 "Unsupported game\n"
	 "Type 'Raine -gamelist' for a list of available games.\n"
	 "\n"
      );
      exit(1);

   }
   else{

      for(i=0; i<game_count; i++)
#ifndef PRIVATE
         if (!(game_list[i]->flags & GAME_PRIVATE))
#endif
            CheckGame(game_list[i]);

   }

   exit(0);
}

#define QUOTE	"\""
#define INDENT	"\t"

static void GameInfo_Begin(void)
{
   int private=0;
   int i;

#ifndef PRIVATE
   for(i=0; i<game_count; i++)
      if (game_list[i]->flags & GAME_PRIVATE)
         private++;
#endif

   allegro_message("emulator (\n"
   INDENT "name " QUOTE EMUNAME QUOTE "\n"
   INDENT "version " QUOTE VERSION QUOTE "\n"
   INDENT "games %d\n)\n\n", game_count-private);

}

static void GameInfo_End(void)
{
}

static void GameInfo(GAME_MAIN *game_info)
{
   int tb,tc,td,romof,cloneof;
   DIR_INFO *dir_list;
   ROM_INFO *rom_list;
   DSW_INFO *dsw_list;
   VIDEO_INFO *vid_info;
   SOUND_INFO *sound_list;

   printf("game (\n");

   /*

   'id' name (8 char, lower case)

   */

   printf(INDENT "name %s\n", game_info->main_name);

   /*

   full name (description)

   */

   printf(INDENT "description " QUOTE "%s" QUOTE, game_info->long_name);

   if(game_info->long_name_jpn)

      printf(" japanese " QUOTE "%s" QUOTE, game_info->long_name_jpn);

   printf("\n");

   /*

   manufacturer

   */

   printf(INDENT "manufacturer " QUOTE "%s" QUOTE "\n", game_company_name(game_info->company_id));

   /*

   year

   */

   if(game_info->year)

      printf(INDENT "year %4d\n", game_info->year);

   /*

   board number [optional]

   */

   if(game_info->board)

      printf(INDENT "board " QUOTE "%s" QUOTE "\n", game_info->board);

   /*

   romof %s [optional]

   */

   romof = 0;

   dir_list = game_info->dir_list;

   while(dir_list->maindir){

      if(dir_list->maindir[0] == '#'){

            printf(INDENT "romof %s\n", (dir_list->maindir) + 1 );

            romof ++;

      }

      dir_list ++;

   }

   /*

   cloneof %s [optional]

   */

   cloneof = 0;

   dir_list = game_info->dir_list;

   while(dir_list->maindir){

      if(dir_list->maindir[0] == '$'){

            printf(INDENT "cloneof %s\n", (dir_list->maindir) + 1 );

            cloneof ++;

      }

      dir_list ++;

   }

   /*

   rom ( name %s merge %s size %d crc32 %08x )

   */

   rom_list = game_info->rom_list;

   while(rom_list->name){

      printf(INDENT "rom ( name %s", rom_list->name);

      if(romof){

         tc = find_alternative_file_names(rom_list, game_info->dir_list);

         for(td=0; td<tc; td++)

            printf(" merge %s", alt_names[td]);

      }

      printf(" size %d crc32 %08x )\n", rom_list->size, rom_list->crc32);

      rom_list++;

   }

   /*

   archive ( name %s ... )

   */

   dir_list = game_info->dir_list;

   printf(INDENT "archive ( ");

   while(dir_list->maindir){

      if((dir_list->maindir[0] != '#') && (dir_list->maindir[0] != '$'))

         printf("name " QUOTE "%s" QUOTE " ", dir_list->maindir);

      dir_list++;

   }

   printf(")\n");

   /*

   chip ( type audio name %s ) [optional]

   */

   sound_list = game_info->sound_list;

   if(sound_list){

      while(sound_list->interface){

         printf(INDENT "chip ( type audio name %s )\n", get_sound_chip_name(sound_list->type));

         sound_list++;

      }

   }

   /*

   input ( dipswitches %d ) [optional]

   */

   dsw_list = game_info->dsw_list;

   tb = 0;
   if(dsw_list){

      while(dsw_list->data){

         tb++;

         dsw_list++;

      }

   }

   if(tb){

      printf(INDENT "input ( ");
      printf("dipswitches %d ", tb);
      printf(")\n");

   }

   /*

   video ( screen raster x %d y %d colors %d freq %d )

   */

   vid_info = game_info->video_info;

   printf(INDENT "video ( screen raster ");

   if(vid_info->flags & VIDEO_ROTATABLE){

   switch(VIDEO_ROTATE( vid_info->flags )){
      case VIDEO_ROTATE_NORMAL:
         printf("orientation horizontal ");
      break;
      case VIDEO_ROTATE_90:
         printf("orientation vertical ");
      break;
      case VIDEO_ROTATE_180:
         printf("orientation horizontal ");
      break;
      case VIDEO_ROTATE_270:
         printf("orientation vertical ");
      break;
   }

   }
   else{
      printf("orientation undefined ");
   }

   printf("x %d y %d ", vid_info->screen_x, vid_info->screen_y);
   printf("freq 60 )\n");

   /*

   driver ( status good color good colordeep 8 credits %s )

   */

   printf(INDENT "driver ( status good color good colordeep 8 credits " QUOTE "%s" QUOTE " )\n", "Antiriad & Raine Team");

   printf(")\n\n");
}

// CLI_game_info():
// List game info for a combination of games (todo - wildstar support)

static void CLI_game_info(void)
{
   int i;

   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;

      for(i=0; i<game_count; i++){
         if(
#ifndef PRIVATE
	    (!(game_list[i]->flags & GAME_PRIVATE)) &&
#endif
            !(stricmp(ArgList[ArgPosition], game_list[i]->main_name))){

            GameInfo_Begin();
            GameInfo(game_list[i]);
            GameInfo_End();

            exit(0);
         }
      }

      printf(
	 "Unsupported game\n"
	 "Type 'Raine -gamelist' for a list of available games.\n"
	 "\n"
      );
      exit(1);

   }
   else{

      GameInfo_Begin();

      for(i=0;i<game_count;i++)

#ifndef PRIVATE
         if (!(game_list[i]->flags & GAME_PRIVATE))
#endif
            GameInfo(game_list[i]);

      GameInfo_End();

   }

   exit(0);

}

static void dsw_info_Begin(void)
{
   int private=0;
   int i;

#ifndef PRIVATE
   for(i=0; i<game_count; i++)
      if (game_list[i]->flags & GAME_PRIVATE)
         private++;
#endif

   printf("emulator (\n");
   printf(INDENT "name " QUOTE EMUNAME QUOTE "\n");
   printf(INDENT "version " QUOTE VERSION QUOTE "\n");
   printf(INDENT "games %d\n", game_count-private);
   printf(")\n\n");
}

static void dsw_info_End(void)
{
}

static void dsw_info(GAME_MAIN *game_info)
{
   DSW_INFO *dsw_list;
   DSW_DATA *dsw_data;
   UINT32 i;

   printf("game (\n");

   /*

   'id' name (8 char, lower case)

   */

   printf(INDENT "name %s\n", game_info->main_name);

   /*

   full name (description)

   */

   printf(INDENT "description " QUOTE "%s" QUOTE "\n", game_info->long_name);

   /*

   manufacturer

   */

   printf(INDENT "manufacturer " QUOTE "%s" QUOTE "\n", game_company_name(game_info->company_id));

   /*

   dsw ( type audio name %s ) [optional]

   */

   dsw_list = game_info->dsw_list;

   i = 0;

   if(dsw_list){

      while(dsw_list->data){

         dsw_data = dsw_list->data;
         
         printf(INDENT "dipswitch ( number %d factory 0x%02x )\n", i, dsw_list->factory_setting);

         while(dsw_data->name){

				if(dsw_data->count)

          	  printf(INDENT "dipdata ( type head bitmask 0x%02x name " QUOTE "%s" QUOTE " )\n", dsw_data->bit_mask, dsw_data->name);

				else

          	  printf(INDENT "dipdata ( type body bitmask 0x%02x name " QUOTE "%s" QUOTE " )\n", dsw_data->bit_mask, dsw_data->name);

            dsw_data++;

         }

         dsw_list++;
         i++;

      }

   }

   printf(")\n\n");
}

// CLI_dsw_info():
// List game info for a combination of games (todo - wildstar support)

static void CLI_dsw_info(void)
{
   int i;

   if( ((ArgPosition+1) < ArgCount) && (ArgList[ArgPosition+1][0] != '-') ){

      ArgPosition++;

      for(i=0; i<game_count; i++){
         if(
#ifndef PRIVATE
            (!(game_list[i]->flags & GAME_PRIVATE)) &&
#endif
            !(stricmp(ArgList[ArgPosition], game_list[i]->main_name))){

            dsw_info_Begin();
            dsw_info(game_list[i]);
            dsw_info_End();

            exit(0);
         }
      }

      printf(
	 "Unsupported game\n"
	 "Type 'Raine -gamelist' for a list of available games.\n"
	 "\n"
      );
      exit(1);

   }
   else{

      dsw_info_Begin();

      for(i=0;i<game_count;i++)

#ifndef PRIVATE
         if (!(game_list[i]->flags & GAME_PRIVATE))
#endif
            dsw_info(game_list[i]);

      dsw_info_End();

   }

   exit(0);

}

// CLI_OPTION:
// Type for command line process list

typedef struct CLI_OPTION
{
   char *Name;			// Option name/string
   void (*Process)();		// Pointer to option process function
} CLI_OPTION;

// Command_Options:
// List of command line options and their handlers

static CLI_OPTION cli_commands[] =
{
   { "-gamelist",	CLI_game_list		},
   { "-gl",		CLI_game_list		},
   { "-gameinfo",	CLI_game_info		},
   { "-listinfo",	CLI_game_info		},
   { "-romcheck",	CLI_game_check		},
   { "-rc",		CLI_game_check		},
   { "-verifyroms",	CLI_game_check		},
   { "-game",		CLI_game_load		},
   { "-g",		CLI_game_load		},
   { "-help",		CLI_Help		},
   { "-?",		CLI_Help		},
   { "--help",          CLI_Help                },
   { "-h",              CLI_Help                },
   { "-joystick",	CLI_Joystick		},
   { "-j",		CLI_Joystick		},
   { "-limitspeed",	CLI_LimitSpeed		},
   { "-l",		CLI_LimitSpeed		},
   { "-nogui",		CLI_NoGUI		},
   { "-n",		CLI_NoGUI		},
   { "-leds",		CLI_EnableLeds		},
   { "-noleds",		CLI_DisableLeds		},
   { "-cont",		CLI_EnableCont		},
   { "-nocont",		CLI_DisableCont		},
   { "-screenx",	CLI_screen_x		},
   { "-sx",		CLI_screen_x		},
   { "-screeny",	CLI_screen_y		},
   { "-sy",		CLI_screen_y		},
   { "-screenmode",	CLI_screen_mode		},
   { "-sm",		CLI_screen_mode		},
   { "-rotate",		CLI_screen_rotate	},
   { "-r",		CLI_screen_rotate	},
   { "-ror",		CLI_screen_rotate_right	},
   { "-rol",		CLI_screen_rotate_left	},
   { "-norotate",	CLI_screen_no_rotate	},
   { "-nor",		CLI_screen_no_rotate	},
   { "-flip",		CLI_screen_flip		},
   { "-f",		CLI_screen_flip		},
   { "-flipx",		CLI_screen_flip_x	},
   { "-fx",		CLI_screen_flip_x	},
   { "-flipy",		CLI_screen_flip_y	},
   { "-fy",		CLI_screen_flip_y	},
   { "-noflip",		CLI_screen_no_flip	},
   { "-nof",		CLI_screen_no_flip	},
   { "-hide",           CLI_hide                },
   { "-listdsw",	CLI_dsw_info		},
   { "-bpp",            CLI_screen_bpp          },
   { "-depth",          CLI_screen_bpp          },
   { NULL,		NULL        		}
};

static CLI_OPTION cli_options[] =
{
   { "-verbose",	CLI_Verbose		},
   { "-v",		CLI_Verbose		},
   { NULL,		NULL        		}
};

/*

parse_command_line(): process command line options.

*/

void parse_command_line(int argc, char *argv[])
{
   UINT32 ta;

   if(argc){

   verbose = 0;

   ArgCount = argc;

   for(ta=0; ta<(UINT32)ArgCount; ta++)
      ArgList[ta] = argv[ta];

   // check for options first

   for(ArgPosition=1; ArgPosition<ArgCount; ArgPosition++){

       if(*ArgList[ArgPosition] == '-'){

       ta=0;
       while((cli_options[ta].Process) && (ta!=255)){
          if(!(stricmp(cli_options[ta].Name, ArgList[ArgPosition]))){
             ArgList[ArgPosition] = NULL;
             cli_options[ta].Process();
             ta=255;
          }
          else{
             ta++;
          }
       }

       }

   }

   // now check for commands

   for(ArgPosition=1; ArgPosition<ArgCount; ArgPosition++){

       if(ArgList[ArgPosition]){

       // command line options start with '-'

       if(*ArgList[ArgPosition] == '-'){

       ta=0;
       while((cli_commands[ta].Process) && (ta!=255)){
          if(!(stricmp(cli_commands[ta].Name, ArgList[ArgPosition]))){
             cli_commands[ta].Process();
             ta=255;
          }
          else{
             ta++;
          }
       }
       if(ta!=255){
          printf(
		"Unsupported command line option: %s\n"
		"Type 'Raine -help' for a list of available options.\n"
		"\n",
		ArgList[ArgPosition]
          );
          exit(1);
       }

       }
       else{

          // allow raine <gamename> (preferred use is raine -game <gamename>)

          CLI_game_load_alt();

       }

       }

   }

   }
}

// CLI_WaitKeyPress():
// wait for a key routine.

void CLI_WaitKeyPress(void)
{
   install_keyboard();

   clear_keybuf();
   do{
   }while(!keypressed());
   if((readkey()&0xFF)==27){
      exit(0);
   }

   remove_keyboard();
}

/*

load_main_config(void): load main raine settings

*/

void load_main_config(void)
{
   char str[256];

   // config/raine.cfg -------------------------------------

   raine_push_config_state();

   sprintf(str,"%sconfig/%s", dir_cfg.exe_path, dir_cfg.config_file);
   raine_set_config_file(str);


   raine_pop_config_state();

}

/*

save_main_config(void): load main raine settings

*/

void save_main_config(void)
{
   char str[256];

   // config/raine.cfg -------------------------------------

   raine_push_config_state();

   sprintf(str,"%sconfig/%s", dir_cfg.exe_path, dir_cfg.config_file);
   raine_set_config_file(str);


   raine_pop_config_state();

}

void load_screen_settings(char *section)
{
   if(raine_cfg.save_game_screen_settings){

   // DISPLAY

   if(display_cfg.scanlines == 2) display_cfg.screen_y <<= 1;

   display_cfg.screen_type	= raine_get_config_id(	section,	"screen_type",		display_cfg.screen_type);
   display_cfg.screen_x		= raine_get_config_int(	section,	"screen_x",		display_cfg.screen_x);
   display_cfg.screen_y		= raine_get_config_int(	section,	"screen_y",		display_cfg.screen_y);
   display_cfg.scanlines	= raine_get_config_int(	section,	"scanlines",		display_cfg.scanlines);
   display_cfg.vsync		= raine_get_config_int(	section,	"vsync",		display_cfg.vsync);
   display_cfg.triple_buffer	= raine_get_config_int(	section,	"triple_buffer",	display_cfg.triple_buffer);
   display_cfg.eagle_fx		= raine_get_config_int(	section,	"eagle_fx",		display_cfg.eagle_fx);
   display_cfg.pixel_double	= raine_get_config_int(	section,	"pixel_double",		display_cfg.pixel_double);

   if(display_cfg.scanlines == 2) display_cfg.screen_y >>= 1;

   if(display_cfg.vsync > 1) display_cfg.vsync = 0;

   }
}

void save_screen_settings(char *section)
{
   // remove all old settings

   raine_clear_config_section(section);

   if(raine_cfg.save_game_screen_settings){

   // DISPLAY

   if(display_cfg.scanlines == 2) display_cfg.screen_y <<= 1;

   raine_set_config_id (	section,	"screen_type",		display_cfg.screen_type);
   raine_set_config_int(	section,	"screen_x",		display_cfg.screen_x);
   raine_set_config_int(	section,	"screen_y",		display_cfg.screen_y);
   raine_set_config_int(	section,	"scanlines",		display_cfg.scanlines);
   raine_set_config_int(	section,	"vsync",		display_cfg.vsync);
   raine_set_config_int(	section,	"triple_buffer",	display_cfg.triple_buffer);
   raine_set_config_int(	section,	"eagle_fx",		display_cfg.eagle_fx);
   raine_set_config_int(	section,	"pixel_double",		display_cfg.pixel_double);

   if(display_cfg.scanlines == 2) display_cfg.screen_y >>= 1;

   }
}

/*

load_game_config(int game): load game specific settings for a certain game.

*/

void load_game_config(void)
{
   char str[256];

   // config/games.cfg -------------------------------------

   raine_push_config_state();

   sprintf(str,"%sconfig/games.cfg", dir_cfg.exe_path);
   raine_set_config_file(str);

   // Load Key Settings

   sprintf(str,"%s:keyconfig", current_game->main_name);
   load_game_keys(str);

   // Load Joy Settings

   sprintf(str,"%s:joyconfig", current_game->main_name);
   load_game_joys(str);

   // Load DSW Settings

   sprintf(str,"%s:dipswitch", current_game->main_name);
   load_dipswitches(str);

   // Load ROM Version Settings

   sprintf(str,"%s:romversion", current_game->main_name);
   load_romswitches(str);

   // Load Screen Settings

   sprintf(str,"%s:display", current_game->main_name);
   load_screen_settings(str);

   raine_pop_config_state();

   // config/cheats.cfg ------------------------------------

   raine_push_config_state();

   sprintf(str,"%sconfig/%s", dir_cfg.exe_path, dir_cfg.cheat_file);
   raine_set_config_file(str);

   // Load Cheat Settings

   sprintf(str,"%s:cheats", current_game->main_name);
   load_arpro_cheats(str);

   raine_pop_config_state();

}

/*

save_game_config(int game): save game specific settings for a certain game.

*/

void save_game_config(void)
{
   char str[256];

   // config/games.cfg -------------------------------------

   raine_push_config_state();

   sprintf(str,"%sconfig/games.cfg", dir_cfg.exe_path);
   raine_set_config_file(str);

   // Save Key Settings

   sprintf(str,"%s:keyconfig", current_game->main_name);
   save_game_keys(str);

   // Save Joy Settings

   sprintf(str,"%s:joyconfig", current_game->main_name);
   save_game_joys(str);

   // Save DSW Settings

   sprintf(str,"%s:dipswitch", current_game->main_name);
   save_dipswitches(str);

   // Save ROM Version Settings

   sprintf(str,"%s:romversion", current_game->main_name);
   save_romswitches(str);

   // Save screeen settings

   sprintf(str,"%s:display", current_game->main_name);
   save_screen_settings(str);

   raine_pop_config_state();

   // config/cheats.cfg ------------------------------------

   raine_push_config_state();

   sprintf(str,"%sconfig/%s", dir_cfg.exe_path, dir_cfg.cheat_file);
   raine_set_config_file(str);

   // Save Cheat Settings

   sprintf(str,"%s:cheats", current_game->main_name);
   save_arpro_cheats(str);

   raine_pop_config_state();

}
