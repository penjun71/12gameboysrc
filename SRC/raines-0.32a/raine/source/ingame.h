/******************************************************************************/
/*                                                                            */
/*      INGAME (screen blit, eagle, message list, rdtsc profile, timer)       */
/*                                                                            */
/******************************************************************************/

#include "raine.h"

/*

ingame message list

*/

#ifdef RAINE_DOS
void print_ingame(int showtime, const char *format, ...) __attribute__ ((format (printf, 2, 3)));
#else
void print_ingame(int showtime, const char *format, ...);
#endif
void clear_ingame_message_list(void);

/*

bitmap handling

*/

void SetupScreenBitmap(void);

void BlitScreen(void);

void DestroyScreenBitmap(void);

void ReClipScreen(void);

void clear_game_screen(int pen);

#ifdef TRIPLE_BUFFER

void reset_triple_buffer(void);

#endif

// RDTSC Profiler
// --------------

int use_rdtsc;

enum profile_section
{
   PRO_FRAME = 0,
   PRO_CPU,
   PRO_SOUND,
   PRO_RENDER,
   PRO_BLIT,
   PRO_PAL,
   PRO_FREE,
   PRO_COUNT,			// End marker and list size
};

void ProfileStart(UINT8 mode);
void ProfileStop(UINT8 mode);
void UpdateProfile(void);

void switch_fps_mode(void);
void init_fps(void);

UINT32 timer_next_update;	// timer_frame_count at next fps() update
UINT32 timer_start_count;	// timer_frame_count at fps() start
UINT32 render_start_count;	// render_frame_count at fps() start

UINT32 cpu_frame_count;		// number of frames of cpu emulation
UINT32 render_frame_count;	// number of frames of video emulation
UINT32 skip_frame_count;		// number of frames since last video emulation

void start_ingame_timer(void);
void stop_ingame_timer(void);
#ifdef RDTSC_PROFILE

extern UINT32 cycles_per_frame;			// cpu cycles per frame, or 0 if not computed

UINT32 read_ingame_cycles(UINT64 *last_rdtsc_counter);
void reset_rdtsc_timer(UINT64 *last_rdtsc_counter);
#endif
UINT32 read_ingame_timer(void);
void reset_ingame_timer(void);

void reset_layer_switches(void);
void check_layer_switches(void);
int add_layer_info(char *name);
int check_layer_enabled(int layer);
void get_screen_coordinates(int *Xoff2,int *Yoff2, int *Destx, int *Desty, int *Xxx, int *Yyy);
