/**************************************************************/
/* Command Line Processing                                    */
/**************************************************************/

void parse_command_line(int argc, char *argv[]);

void CLI_WaitKeyPress(void);

void load_main_config(void);
void save_main_config(void);

void load_game_config(void);
void save_game_config(void);
