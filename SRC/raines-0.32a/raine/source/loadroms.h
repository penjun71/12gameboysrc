/******************************************************************************/
/*                                                                            */
/*                            LOAD ROMS FROM ROM_INFO                         */
/*                                                                            */
/******************************************************************************/

#ifndef __loadroms_h_
#define __loadroms_h_

#include "deftypes.h"

enum region_type
{
   REGION_NONE = 0,
   REGION_ROM1,
   REGION_ROM2,
   REGION_ROM3,
   REGION_ROM4,
   REGION_GFX1,
   REGION_GFX2,
   REGION_GFX3,
   REGION_GFX4,
   REGION_SMP1,
   REGION_SMP2,
   REGION_SMP3,
   REGION_SMP4,
   REGION_MAX,
};

enum region_flags
{
   LOAD_NORMAL = 0,

   // Rom loading
   LOAD_8_16,
   LOAD_8_32,
   LOAD_8_64,
   LOAD_16_32,
   LOAD_16_64,

   // sprites
   LOAD_SWAP_16,
   LOAD8X8_16X16,
   LOAD_8_16S, // 8_16 for sprites (without GfxLayout)
};

typedef struct ROM_INFO
{
   UINT8 *name;                 // file name
   UINT32  size;                 // file size
   UINT32  crc32;                // file crc32
   UINT8  region;               // rom region (optional)
   UINT32  offset;               // offset within rom region (optional)
   UINT8  flags;                // loading flags (optional)
} ROM_INFO;

typedef struct DIR_INFO
{
   UINT8 *maindir;              // directory name
} DIR_INFO;

extern UINT8 *load_region[REGION_MAX];

/*

search for any alternative file names that a rom could have. this uses the
romlist data for any 'romof' games, if a rom matches on crc32 and size, we
assume it is a match.

*/

extern UINT8 *alt_names[8];
UINT32 find_alternative_file_names(ROM_INFO *rom_info, DIR_INFO *dir_list);

UINT32 get_region_size(UINT32 region);

void load_game_rom_info(void);

int load_rom(UINT8 *rom, UINT8 *dest, UINT32 size);

int load_rom_8_16(UINT8 *rom, UINT8 *dest, UINT32 size);
int load_rom_8_32(UINT8 *rom, UINT8 *dest, UINT32 size);
int load_rom_8_64(UINT8 *rom, UINT8 *dest, UINT32 size);
int load_rom_16_32(UINT8 *rom, UINT8 *dest, UINT32 size);
int load_rom_16_64(UINT8 *rom, UINT8 *dest, UINT32 size);

int load_rom_swap_16(UINT8 *rom, UINT8 *dest, UINT32 size);
int load_rom_8x8_16x16(UINT8 *rom, UINT8 *dst, UINT32 len);


int load_rom_index(UINT32 num, UINT8 *dest, UINT32 size);

int load_rom_dir(DIR_INFO *dir_list, UINT8 *rom, UINT8 *dest, UINT32 size, UINT32 crc32);

int rom_size_dir(DIR_INFO *dir_list, UINT8 *rom, UINT32 size, UINT32 crc32);

#endif // __loadroms_h_
