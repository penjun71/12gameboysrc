/******************************************************************************/
/*                                                                            */
/*          RAINE MAIN EMULATION ROUTINES (run game, reset game etc)          */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

UINT32 run_game_emulation(void);

void reset_game_hardware(void);

void init_gui_inputs(void);
void init_gui_inputs_paused(void);

void update_gui_inputs(void);
void update_gui_inputs_paused(void);
