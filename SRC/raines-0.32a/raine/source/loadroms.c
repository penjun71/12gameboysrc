/******************************************************************************/
/*                                                                            */
/*                            LOAD ROMS FROM ROM_INFO                         */
/*                                                                            */
/******************************************************************************/

#include "loadroms.h"
#include "files.h"
#include "gameinc.h"
#include "debug.h"
#include "unzip.h"
#include "string.h"
#include "gui.h" // load_progress

UINT8 *load_region[REGION_MAX];
UINT8 *alt_names[8];

static UINT8 *temp_buffer;
static UINT32 temp_buffer_size;

static UINT8 *get_temp_buffer(UINT32 size)
{
   if((temp_buffer) && (temp_buffer_size >= size))
   {
      return temp_buffer;
   }
   else
   {
      if(temp_buffer)
         FreeMem(temp_buffer);

      if(!(temp_buffer = AllocateMem(size)))
         return NULL;

      temp_buffer_size = size;

      return temp_buffer;

   }
}

static void free_temp_buffer(void)
{
   if(temp_buffer)
      FreeMem(temp_buffer);
}

UINT32 get_region_size(UINT32 region)
{
   ROM_INFO *rom_list;
   UINT32 i, j;

   i = 0;
   j = 0;

   rom_list = current_game->rom_list;
   while(rom_list->name)
   {
     if(rom_list->region == region)
      {
	switch(rom_list->flags)
	  {
	  case LOAD_NORMAL:
	  case LOAD_SWAP_16:
	  case LOAD8X8_16X16:
	    j = rom_list->offset + rom_list->size;
            break;
	  case LOAD_8_16:
	  case LOAD_8_16S:
	    j = rom_list->offset + (rom_list->size * 2) - 1;
            break;
	  case LOAD_8_32:
	    j = rom_list->offset + (rom_list->size * 4) - 3;
	    break;
	  case LOAD_8_64:
	    j = rom_list->offset + (rom_list->size * 8) - 7;
            break;
	  case LOAD_16_32:
	    j = rom_list->offset + (rom_list->size * 2) - 2;
            break;
	  case LOAD_16_64:
	    j = rom_list->offset + (rom_list->size * 4) - 6;
            break;
         }

         if(i < j)
         {
            i = j;
         }
      }

      rom_list++;
   }
#ifdef RAINE_DEBUG
   print_debug("region: 0x%02x size: 0x%08x\n", region, i);
#endif
   return i;
}

/*

find a game based on main_name (useful for looking up romof games)

*/

GAME_MAIN *find_game(UINT8 *main_name)
{
   UINT32 i;

   for(i=0 ; i<(UINT32)game_count ; i++){

      if(! stricmp(main_name, game_list[i]->main_name) )

         return game_list[i];

   }

   return NULL;
}

static int load_zipped(char *zipfile, ROM_INFO *rom_info, UINT8 *dest)
{
   unzFile uf;
   int err;

   uf = unzOpen(zipfile);

   if(!uf)			// Fail: Unable to find/open zipfile
      return 0;

   err = unz_locate_file_crc32(uf,rom_info->crc32);
   if(err!=UNZ_OK){
      #ifdef RAINE_DEBUG
      print_debug("unz_locate_file_crc32(): Error #%d\nNow trying with file name...\n",err);
      #endif
      unzClose(uf);

      uf = unzOpen(zipfile);

      if(!uf)			// Fail: Unable to find/open zipfile
         return 0;

      err = unz_locate_file_name(uf,rom_info->name);
      if(err!=UNZ_OK){
         #ifdef RAINE_DEBUG
         print_debug("unz_locate_file_name(): Error #%d\nNow giving up...\n",err);
         #endif
         unzClose(uf);
         return 0;		// Fail: File not in zip
      } else if (rom_info->crc32) { // found by name, but not by crc...
	// if given crc is 0, then we don't know about crc !
	load_error |= LOAD_WARNING;

	if (load_debug)
	  sprintf(load_debug+strlen(load_debug),
		  "Bad CRC for ROM %s\n",rom_info->name);
      }
   }

   err = unzOpenCurrentFile(uf);
   if(err!=UNZ_OK){
      #ifdef RAINE_DEBUG
      print_debug("unzOpenCurrentFile(): Error #%d\n",err);
      #endif
      unzCloseCurrentFile(uf);	// Is this needed when open failed?
      unzClose(uf);
      return 0;			// Fail: Something internal
   }

   err = unzReadCurrentFile(uf,dest,rom_info->size);
   if(err<0){
      #ifdef RAINE_DEBUG
      print_debug("unzReadCurrentFile(): Error #%d\n",err);
      #endif
      unzCloseCurrentFile(uf);
      unzClose(uf);
      return 0;			// Fail: Something internal
   }

   err = unzCloseCurrentFile(uf);
   if(err!=UNZ_OK){
      #ifdef RAINE_DEBUG
      print_debug("unzCloseCurrentFile(): Error #%d\n",err);
      #endif
      unzClose(uf);
      return -1;		// Clean up Failed: Something internal
   }

   unzClose(uf);
   return -1;
}

static int size_zipped(char *zipfile, ROM_INFO *rom_info)
{
   unzFile uf;
   unz_file_info file_info;
   int err;

   uf = unzOpen(zipfile);

   if(!uf)			// Fail: Unable to find/open zipfile
      return 0;

   err = unz_locate_file_crc32(uf,rom_info->crc32);
   if(err!=UNZ_OK){
      #ifdef RAINE_DEBUG
      print_debug("unz_locate_file_crc32(): Error #%d\nNow trying with file name...\n",err);
      #endif
      unzClose(uf);

      uf = unzOpen(zipfile);

      if(!uf)			// Fail: Unable to find/open zipfile
         return 0;

      err = unz_locate_file_name(uf,rom_info->name);
      if(err!=UNZ_OK){
         #ifdef RAINE_DEBUG
         print_debug("unz_locate_file_name(): Error #%d\nNow giving up...\n",err);
         #endif
         unzClose(uf);
         return 0;		// Fail: File not in zip
      }
   }

   err = unzGetCurrentFileInfo(uf,&file_info,NULL,0,NULL,0,NULL,0);
   if(err!=UNZ_OK){
      #ifdef RAINE_DEBUG
      print_debug("unzGetCurrentFileInfo(): Error #%d\n",err);
      #endif
      unzClose(uf);
      return 0;			// Fail: Something internal
   }

   unzClose(uf);

   if( file_info.uncompressed_size > 0 )
      return file_info.uncompressed_size;
   else
      return 0;
}

/* recursively search for a rom. recursion allows rom inheritance to */
/* any depth (eg. game a uses roms from game b, game b gets the roms */
/* from game c and game c gets them from game d! yes, I know it's a */
/* bit unlikely, but you never know and recursion makes it look nice). */

static ROM_INFO  rec_rom_info;      // the rom to load
static UINT8    *rec_dest;          // destination memory buffer

UINT32 recursive_rom_load(DIR_INFO *head)
{
   UINT8 *dir;
   
   dir = head[0].maindir;

   if( dir ){

      if( IS_ROMOF(dir) ){

         GAME_MAIN *game_romof;

         game_romof = find_game(dir+1);

         if(game_romof){

            UINT32 len;

            len = recursive_rom_load( game_romof->dir_list );

            if(len)

               return len;

         }

      }
      else{

         UINT32 ta;
         UINT8 path[512];
	 
         for(ta = 0; ta < 4; ta ++){

            if(dir_cfg.rom_dir[ta][0]){

               sprintf(path, "%s%s.zip", dir_cfg.rom_dir[ta], dir);
               if((load_zipped(path, &rec_rom_info, rec_dest)))
                  return 1;

               sprintf(path, "%s%s/%s", dir_cfg.rom_dir[ta], dir, rec_rom_info.name);
               if((load_file(path, rec_dest, rec_rom_info.size)))
                  return 1;

            }
         }

      }

      return recursive_rom_load( head+1 );

   }
   else{

      return 0;

   }
}

UINT32 recursive_rom_size(DIR_INFO *head)
{
   UINT8 *dir;

   dir = head[0].maindir;

   if( dir ){

      if( IS_ROMOF(dir) ){

         GAME_MAIN *game_romof;

         game_romof = find_game(dir+1);

         if(game_romof){

            UINT32 len;

            len = recursive_rom_size( game_romof->dir_list );

            if(len)

               return len;

         }

      }
      else{

         UINT32 ta,len;
         UINT8 path[512];

         for(ta = 0; ta < 4; ta ++){

            if(dir_cfg.rom_dir[ta][0]){

               sprintf(path, "%s%s.zip", dir_cfg.rom_dir[ta], dir);
               if( ( len=size_zipped(path, &rec_rom_info) ) )
                  return len;

               sprintf(path, "%s%s/%s", dir_cfg.rom_dir[ta], dir, rec_rom_info.name);
               if( ( len=size_file(path) ) )
                  return len;

            }
         }

      }

      return recursive_rom_size( head+1 );

   }
   else{

      return 0;

   }
}

/*

dumps the search path for a dir_list. it recursively lists
any romof path list(s)

*/

void dump_search_path(DIR_INFO *dir_list)
{
   UINT8 *dir;

   while(dir_list->maindir){

      dir = dir_list->maindir;

      if( IS_ROMOF(dir) ){

         GAME_MAIN *game_romof;

         game_romof = find_game(dir+1);

         if(game_romof)

            dump_search_path(game_romof->dir_list);

      }
      else{

         if( ! IS_CLONEOF(dir) ){

            UINT32 i;

            for(i = 0; i < 4; i ++){

               if(dir_cfg.rom_dir[i][0]){

                  sprintf(load_debug+strlen(load_debug),"%s%s [.zip]\n",dir_cfg.rom_dir[i], dir);

               }
            }
         }
      }

      dir_list++;
   }
}

UINT32 find_alternative_file_names(ROM_INFO *rom_info, DIR_INFO *dir_list)
{
   GAME_MAIN *game_romof;
   ROM_INFO  *rom_list_romof;
   UINT8     *dir;
   UINT32      alt_name_count;

   alt_name_count = 0;

   if((!rom_info->crc32) || (!rom_info->size))

      return 0;

   while(dir_list->maindir){

      dir = dir_list->maindir;

      if( IS_ROMOF(dir) ){

         game_romof = find_game(dir+1);

         if(game_romof){

            rom_list_romof = game_romof->rom_list;

            while(rom_list_romof->name){

               if( (rom_list_romof->size  == rom_info->size )
                && (rom_list_romof->crc32 == rom_info->crc32) ){

                  alt_names[alt_name_count] = rom_list_romof->name;
                  alt_name_count = (alt_name_count+1)&7;

               }

               rom_list_romof ++;

            }

         }

      }

      dir_list ++;
   }

   return alt_name_count;
}

// Hack to load correctly these 16x16x8 sprites...
// I know it's ugly, but I spent one hour trying to figure out how these
// gfx layout worked... After that, enough was enough !


int load_sprite_8_16b(UINT8 *rom, UINT8 *dst, UINT32 len){
  UINT8 *src;
  int ta,tb;

  // Called for (offsets & 1)==0
  
  src = get_temp_buffer(len);
  if(src){
    if(load_rom(rom, src, len)){
      for(ta = 0, tb=0; ta < len; ta++, tb += 2){
	dst[tb+1] = (src[ta] << 0) & 0xF0;
	dst[tb+0] = (src[ta] << 4) & 0xF0;
      }
      return 1;
    }
  }
  return 0;
}

int load_sprite_8_16(UINT8 *rom, UINT8 *dst, UINT32 len){
  UINT8 *src;
  int ta,tb;

  // Called for offsets & 1
  src = get_temp_buffer(len);
  if(src){
    if(load_rom(rom, src, len)){
      for(ta = 0,tb=0; ta < len; ta ++, tb += 2){
	dst[tb+1] |= (src[ta] >> 4) & 0x0F;
	dst[tb+0] |= (src[ta] >> 0) & 0x0F;
      }
      return 1;
    }
  }
  return 0;
}

static UINT32 load_region_files(UINT32 region, UINT8 *dest)
{
   ROM_INFO *rom_list;

   rom_list = current_game->rom_list;
   while(rom_list->name)
   {
      if(rom_list->region == region)
      {
	switch(rom_list->flags)
	  {
	  case LOAD_NORMAL:
	    if(!load_rom(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
            break;
	  case LOAD_8_16S:
	    if (rom_list->offset & 1){
	      if(!load_sprite_8_16(rom_list->name, dest + (rom_list->offset & ~1), rom_list->size)) return 0;
	    } else {
	      if(!load_sprite_8_16b(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
	    }
	    break;
	  case LOAD_8_16:
	      if(!load_rom_8_16(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
	      break;
	  case LOAD_8_32:
	    if(!load_rom_8_32(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
            break;
	  case LOAD_8_64:
	    if(!load_rom_8_64(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
	    break;
	  case LOAD_16_32:
	    if(!load_rom_16_32(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
            break;
	  case LOAD_16_64:
	    if(!load_rom_16_64(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
            break;
	  case LOAD_SWAP_16:
	    if(!load_rom_swap_16(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
            break;
	  case LOAD8X8_16X16:
	    if(!load_rom_8x8_16x16(rom_list->name, dest + rom_list->offset, rom_list->size)) return 0;
            break;	    
	  }
      }

      rom_list++;
   }

   return 1;
}

static UINT32 load_rom_region(UINT32 region)
{
   UINT32 region_size;

   region_size = get_region_size(region);

   if(region_size)
   {
      if(!(load_region[region] = AllocateMem(region_size))) return 0;

      memset(load_region[region], 0x00, region_size);

      if(!load_region_files(region, load_region[region])) return 0;
   }

   return 1;
}

static UINT32 load_gfx_region(UINT32 region)
{
   UINT32 region_size;
   VIDEO_INFO *video_info;
   GFX_LIST *gfx_list;
   
   video_info = current_game->video_info;
   gfx_list = video_info->gfx_list;
   if (!gfx_list) return 1; // Correct, but nothing to load
   
   while(gfx_list->region) // region 0 is NONE or end of list
     {
       if(gfx_list->region == region)
         {
	   break;
         }

         gfx_list++;
     }

   // Special case for 8x8 -> 16x16 sprites in cave games...
   // I want it included as the others types...
   if (!gfx_list->layout)
     return load_rom_region(region);
   
   region_size = get_region_size(region);

   if(region_size)
   {
      UINT8 *buffer;

      if(!(buffer = get_temp_buffer(region_size))) return 0;

      memset(buffer, 0x00, region_size);

      if(!load_region_files(region, buffer)) return 0;
      if(!(load_region[region] = decode_gfx(buffer, region_size, gfx_list->layout))) return 0;

   }

   return 1;
}

void load_game_rom_info(void)
{
   UINT32 i;

   for(i = 0; i < REGION_MAX; i++)

      load_region[i] = NULL;

   temp_buffer = NULL;
   temp_buffer_size = 0;

   /*

   program rom regions

   */

   if(!load_rom_region(REGION_ROM1)) return;
   if(!load_rom_region(REGION_ROM2)) return;
   if(!load_rom_region(REGION_ROM3)) return;
   if(!load_rom_region(REGION_ROM4)) return;

   /*

   video rom regions

   */

   if(!load_gfx_region(REGION_GFX1)) return;
   if(!load_gfx_region(REGION_GFX2)) return;
   if(!load_gfx_region(REGION_GFX3)) return;
   if(!load_gfx_region(REGION_GFX4)) return;

   /*

   audio rom regions

   */

   if(!load_rom_region(REGION_SMP1)) return;
   if(!load_rom_region(REGION_SMP2)) return;
   if(!load_rom_region(REGION_SMP3)) return;
   if(!load_rom_region(REGION_SMP4)) return;

   /*

   map to some old globals

   */

   ROM = load_region[REGION_ROM1];
   GFX = load_region[REGION_GFX1];

   free_temp_buffer();
}

/*

load rom from a filename

*/

int load_rom(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   DIR_INFO *dir_list;
   ROM_INFO *rom_list;
   UINT32     ta,tb,tc;
   int found;
   
   dir_list = current_game->dir_list;

   // locate the full rom info (ie. crc32)

   rec_rom_info.name  = rom;
   rec_rom_info.size  = size;
   rec_rom_info.crc32 = 0;

   rom_list = current_game->rom_list;

   found=0;
   while((rom_list->name) && (!rec_rom_info.crc32)){

      if(! stricmp(rom_list->name, rom) )
      {
         found++;
         rec_rom_info.crc32 = rom_list->crc32;
      }

      rom_list++;
   }

#ifdef RAINE_DEBUG
   if (!found)
     {
       load_error |= LOAD_WARNING;
       
       sprintf(load_debug+strlen(load_debug),
	       "Warning: ROM not in gameinfo %s\n",rec_rom_info.name);
     }
#endif
   
   // now try loading it

   rec_dest = dest;
   
   ta = recursive_rom_load( dir_list );

   if(!ta){

   // try to locate alternative filenames via the crc32 & size data (merged sets)
     
   tc = find_alternative_file_names( &rec_rom_info, dir_list );

   // try loading with any alternative file names we found

   ta = 0;

   for(tb=0; tb<tc; tb++){

     rec_rom_info.name = alt_names[tb];

      ta = recursive_rom_load( dir_list );

      if(ta)
         tb = tc;

   }

   }

   // Error Logging

   if(!ta)
   {
      sprintf(load_debug+strlen(load_debug),"Unable to open '%s'\n",rom);
      sprintf(load_debug+strlen(load_debug),"\n");
      sprintf(load_debug+strlen(load_debug),"Search path:\n");
      sprintf(load_debug+strlen(load_debug),"\n");

      dump_search_path(dir_list);

      sprintf(load_debug+strlen(load_debug),"\n");

      load_error |= LOAD_FATAL_ERROR;

   }
   else
   {
      load_progress();
   }

   return ta;

}

/*

load rom from an index in the game rom_list[]

*/

int load_rom_index(UINT32 num, UINT8 *dest, UINT32 size)
{
   ROM_INFO *rom_list;

   rom_list = current_game->rom_list;

   return load_rom(rom_list[num].name, dest, size);
}

// User specified dir_list, no error log

int load_rom_dir(DIR_INFO *dir_list, UINT8 *rom, UINT8 *dest, UINT32 size, UINT32 crc32)
{
   UINT32 ta,tb,tc;

   // locate the full rom info (ie. crc32)

   rec_rom_info.name  = rom;
   rec_rom_info.size  = size;
   rec_rom_info.crc32 = crc32;

   // now try loading it

   rec_dest = dest;

   ta = recursive_rom_load( dir_list );

   if(!ta){

   // try to locate alternative filenames via the crc32 & size data (merged sets)

   tc = find_alternative_file_names( &rec_rom_info, dir_list );

   // try loading with any alternative file names we found

   ta = 0;

   for(tb=0; tb<tc; tb++){

      rec_rom_info.name = alt_names[tb];

      ta = recursive_rom_load( dir_list );

      if(ta)
         tb = tc;

   }

   }

   return ta;
}


static int load_rom_bytemap(UINT8 *rom, UINT8 *dest, UINT32 size, UINT32 mode)
{
   UINT8 *tmp;

   tmp = AllocateMem(size);

   if(tmp)
   {
      if(load_rom(rom, tmp, size))
      {
         UINT32 i;

         switch(mode)
         {
            case 0:
               for(i = 0; i < size; i ++)
                  dest[i << 1] = tmp[i];
            break;
            case 1:
               for(i = 0; i < size; i ++)
                  dest[i << 2] = tmp[i];
            break;
	 case 2: // load_16_32
               for(i = 0; i < size; i += 2)
                  WriteWord(&dest[i << 1], ReadWord(&tmp[i]));
            break;
	 case 3: // load_16_64
               for(i = 0; i < size; i+=2)
                  WriteWord(&dest[i << 2], ReadWord(&tmp[i]));
            break;
	 case 4: // load_8_64
               for(i = 0; i < size; i++)
                  dest[i << 3] = tmp[i];
            break;
         }

         FreeMem(tmp);
         return 1;
      }
      else
      {
         FreeMem(tmp);
         return 0;
      }
   }
   else

      return 0;

}


int load_rom_8_16(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   return load_rom_bytemap(rom, dest, size, 0);
}


int load_rom_8_32(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   return load_rom_bytemap(rom, dest, size, 1);
}

int load_rom_8_64(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   return load_rom_bytemap(rom, dest, size, 4);
}

int load_rom_16_32(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   return load_rom_bytemap(rom, dest, size, 2);
}

int load_rom_16_64(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   return load_rom_bytemap(rom, dest, size, 3);
}

int load_rom_swap_16(UINT8 *rom, UINT8 *dest, UINT32 size)
{
   if(load_rom(rom, dest, size))
   {
      ByteSwap(dest, size);
      return 1;
   }
   else
   {
      return 0;
   }
}

int load_rom_8x8_16x16(UINT8 *rom, UINT8 *dst, UINT32 len){
  UINT8 *src;
  int ta,tb,line,row;

  // Normally used in cave games to convert 8x8 sprites to 16x16...
  
  src = get_temp_buffer(len);
  if(src){
    if(load_rom(rom, src, len)){
      for(ta = 0, tb=0; ta < len;){
	for (row=0; row<2; row++) { // 2 rows of 2 8x8 sprites
	  for (line=0; line<8; line++) { // 8 lines of 16 pixels / row
	    dst[tb+0]  = (src[ta+0] >> 4) & 0x0F;
	    dst[tb+1]  = (src[ta+0] >> 0) & 0x0F;
	    dst[tb+2]  = (src[ta+2] >> 4) & 0x0F;
	    dst[tb+3]  = (src[ta+2] >> 0) & 0x0F;
	    dst[tb+4]  = (src[ta+4] >> 4) & 0x0F;
	    dst[tb+5]  = (src[ta+4] >> 0) & 0x0F;
	    dst[tb+6]  = (src[ta+6] >> 4) & 0x0F;
	    dst[tb+7]  = (src[ta+6] >> 0) & 0x0F;
	    dst[tb+0] |= (src[ta+1] << 0) & 0xF0;
	    dst[tb+1] |= (src[ta+1] << 4) & 0xF0;
	    dst[tb+2] |= (src[ta+3] << 0) & 0xF0;
	    dst[tb+3] |= (src[ta+3] << 4) & 0xF0;
	    dst[tb+4] |= (src[ta+5] << 0) & 0xF0;
	    dst[tb+5] |= (src[ta+5] << 4) & 0xF0;
	    dst[tb+6] |= (src[ta+7] << 0) & 0xF0;
	    dst[tb+7] |= (src[ta+7] << 4) & 0xF0;

	    // Cool, we've just moved the 1st line of 8 pixels.
	    // Now we need the next 8 pixels, on the next 8x8 sprite !

	    tb+=8; ta+=64;
	    dst[tb+0]  = (src[ta+0] >> 4) & 0x0F;
	    dst[tb+1]  = (src[ta+0] >> 0) & 0x0F;
	    dst[tb+2]  = (src[ta+2] >> 4) & 0x0F;
	    dst[tb+3]  = (src[ta+2] >> 0) & 0x0F;
	    dst[tb+4]  = (src[ta+4] >> 4) & 0x0F;
	    dst[tb+5]  = (src[ta+4] >> 0) & 0x0F;
	    dst[tb+6]  = (src[ta+6] >> 4) & 0x0F;
	    dst[tb+7]  = (src[ta+6] >> 0) & 0x0F;
	    dst[tb+0] |= (src[ta+1] << 0) & 0xF0;
	    dst[tb+1] |= (src[ta+1] << 4) & 0xF0;
	    dst[tb+2] |= (src[ta+3] << 0) & 0xF0;
	    dst[tb+3] |= (src[ta+3] << 4) & 0xF0;
	    dst[tb+4] |= (src[ta+5] << 0) & 0xF0;
	    dst[tb+5] |= (src[ta+5] << 4) & 0xF0;
	    dst[tb+6] |= (src[ta+7] << 0) & 0xF0;
	    dst[tb+7] |= (src[ta+7] << 4) & 0xF0;
	    
	    // Go back to next line of 1st sprite and continue...
	    ta -= 64-8; tb += 8;
	  }
	  // tb is where it should be.
	  // ta is on the 2nd sprite. Must go to the third one now !
	  ta += 64;
	}
	// Here we have just moved the 4 8x8 sprites.
	// ta would be on the 5th sprite which happens to be correct !
      }
      return 1;
    }  // if load_rom
  }
  return 0;
}

int rom_size_dir(DIR_INFO *dir_list, UINT8 *rom, UINT32 size, UINT32 crc32)
{
   UINT32 ta,tb,tc;

   // locate the full rom info (ie. crc32)

   rec_rom_info.name  = rom;
   rec_rom_info.size  = size;
   rec_rom_info.crc32 = crc32;

   // now try loading it

   ta = recursive_rom_size( dir_list );

   if(!ta){

   // try to locate alternative filenames via the crc32 & size data (merged sets)

   tc = find_alternative_file_names( &rec_rom_info, dir_list );

   // try loading with any alternative file names we found

   ta = 0;

   for(tb=0; tb<tc; tb++){

      rec_rom_info.name = alt_names[tb];

      ta = recursive_rom_size( dir_list );

      if(ta)
         tb = tc;

   }

   }

   return ta;
}
