/******************************************************************************/
/*                                                                            */
/*                 CONTROL SUPPORT [KEYBOARD/JOYSTICK/LEDS]                   */
/*                                                                            */
/******************************************************************************/

#include "raine.h"
#include "games.h"
#include "control.h"

/******************************************************************************/
/*                                                                            */
/*                        DEFAULT GAME KEY SETTINGS                           */
/*                                                                            */
/******************************************************************************/

struct DEF_INPUT def_input_list[KB_DEF_COUNT] =
{
 { KEY_3,       RJOY_1_BUTTON10,"Def Coin A",           },      // KB_DEF_COIN1,
 { KEY_4,       RJOY_2_BUTTON10,"Def Coin B",           },      // KB_DEF_COIN2,
 { KEY_7,       RJOY_3_BUTTON10,"Def Coin C",           },      // KB_DEF_COIN3,
 { KEY_8,       RJOY_4_BUTTON10,"Def Coin D",           },      // KB_DEF_COIN4,

 { KEY_T,       0x00,           "Def Tilt",             },      // KB_DEF_TILT,
 { KEY_Y,       0x00,           "Def Service",          },      // KB_DEF_SERVICE,
 { KEY_U,       0x00,           "Def Test",             },      // KB_DEF_TEST,

 { KEY_1,       RJOY_1_BUTTON9, "Def P1 Start",         },      // KB_DEF_P1_START,

 { KEY_UP,      RJOY_1_UP,      "Def P1 Up",            },      // KB_DEF_P1_UP,
 { KEY_DOWN,    RJOY_1_DOWN,    "Def P1 Down",          },      // KB_DEF_P1_DOWN,
 { KEY_LEFT,    RJOY_1_LEFT,    "Def P1 Left",          },      // KB_DEF_P1_LEFT,
 { KEY_RIGHT,   RJOY_1_RIGHT,   "Def P1 Right",         },      // KB_DEF_P1_RIGHT,

 { KEY_V,       RJOY_1_BUTTON1, "Def P1 Button 1",      },      // KB_DEF_P1_B1,
 { KEY_B,       RJOY_1_BUTTON2, "Def P1 Button 2",      },      // KB_DEF_P1_B2,
 { KEY_N,       RJOY_1_BUTTON3, "Def P1 Button 3",      },      // KB_DEF_P1_B3,
 { KEY_G,       RJOY_1_BUTTON4, "Def P1 Button 4",      },      // KB_DEF_P1_B4,
 { KEY_H,       RJOY_1_BUTTON5, "Def P1 Button 5",      },      // KB_DEF_P1_B5,
 { KEY_J,       RJOY_1_BUTTON6, "Def P1 Button 6",      },      // KB_DEF_P1_B6,
 { KEY_M,       RJOY_1_BUTTON7, "Def P1 Button 7",      },      // KB_DEF_P1_B7,
 { KEY_K,       RJOY_1_BUTTON8, "Def P1 Button 8",      },      // KB_DEF_P1_B8,

 { KEY_2,       RJOY_2_BUTTON9, "Def P2 Start",         },      // KB_DEF_P2_START,

 { KEY_S,       RJOY_2_UP,      "Def P2 Up",            },      // KB_DEF_P2_UP,
 { KEY_X,       RJOY_2_DOWN,    "Def P2 Down",          },      // KB_DEF_P2_DOWN,
 { KEY_Z,       RJOY_2_LEFT,    "Def P2 Left",          },      // KB_DEF_P2_LEFT,
 { KEY_C,       RJOY_2_RIGHT,   "Def P2 Right",         },      // KB_DEF_P2_RIGHT,

 { KEY_Q,       RJOY_2_BUTTON1, "Def P2 Button 1",      },      // KB_DEF_P2_B1,
 { KEY_W,       RJOY_2_BUTTON2, "Def P2 Button 2",      },      // KB_DEF_P2_B2,
 { KEY_E,       RJOY_2_BUTTON3, "Def P2 Button 3",      },      // KB_DEF_P2_B3,
 { KEY_R,       RJOY_2_BUTTON4, "Def P2 Button 4",      },      // KB_DEF_P2_B4,
 { KEY_A,       RJOY_2_BUTTON5, "Def P2 Button 5",      },      // KB_DEF_P2_B5,
 { KEY_D,       RJOY_2_BUTTON6, "Def P2 Button 6",      },      // KB_DEF_P2_B6,
 { KEY_F,       RJOY_2_BUTTON7, "Def P2 Button 7",      },      // KB_DEF_P2_B7,
 { KEY_G,       RJOY_2_BUTTON8, "Def P2 Button 8",      },      // KB_DEF_P2_B8,

 { KEY_5,       RJOY_3_BUTTON9, "Def P3 Start",         },      // KB_DEF_P3_START,

 { 0x00,        RJOY_3_UP,      "Def P3 Up",            },      // KB_DEF_P3_UP,
 { 0x00,        RJOY_3_DOWN,    "Def P3 Down",          },      // KB_DEF_P3_DOWN,
 { 0x00,        RJOY_3_LEFT,    "Def P3 Left",          },      // KB_DEF_P3_LEFT,
 { 0x00,        RJOY_3_RIGHT,   "Def P3 Right",         },      // KB_DEF_P3_RIGHT,

 { 0x00,        RJOY_3_BUTTON1, "Def P3 Button 1",      },      // KB_DEF_P3_B1,
 { 0x00,        RJOY_3_BUTTON2, "Def P3 Button 2",      },      // KB_DEF_P3_B2,
 { 0x00,        RJOY_3_BUTTON3, "Def P3 Button 3",      },      // KB_DEF_P3_B3,
 { 0x00,        RJOY_3_BUTTON4, "Def P3 Button 4",      },      // KB_DEF_P3_B4,
 { 0x00,        RJOY_3_BUTTON5, "Def P3 Button 5",      },      // KB_DEF_P3_B5,
 { 0x00,        RJOY_3_BUTTON6, "Def P3 Button 6",      },      // KB_DEF_P3_B6,
 { 0x00,        RJOY_3_BUTTON7, "Def P3 Button 7",      },      // KB_DEF_P3_B7,
 { 0x00,        RJOY_3_BUTTON8, "Def P3 Button 8",      },      // KB_DEF_P3_B8,

 { KEY_6,       RJOY_4_BUTTON9, "Def P4 Start",         },      // KB_DEF_P4_START,

 { 0x00,        RJOY_4_UP,      "Def P4 Up",            },      // KB_DEF_P4_UP,
 { 0x00,        RJOY_4_DOWN,    "Def P4 Down",          },      // KB_DEF_P4_DOWN,
 { 0x00,        RJOY_4_LEFT,    "Def P4 Left",          },      // KB_DEF_P4_LEFT,
 { 0x00,        RJOY_4_RIGHT,   "Def P4 Right",         },      // KB_DEF_P4_RIGHT,

 { 0x00,        RJOY_4_BUTTON1, "Def P4 Button 1",      },      // KB_DEF_P4_B1,
 { 0x00,        RJOY_4_BUTTON2, "Def P4 Button 2",      },      // KB_DEF_P4_B2,
 { 0x00,        RJOY_4_BUTTON3, "Def P4 Button 3",      },      // KB_DEF_P4_B3,
 { 0x00,        RJOY_4_BUTTON4, "Def P4 Button 4",      },      // KB_DEF_P4_B4,
 { 0x00,        RJOY_4_BUTTON5, "Def P4 Button 5",      },      // KB_DEF_P4_B5,
 { 0x00,        RJOY_4_BUTTON6, "Def P4 Button 6",      },      // KB_DEF_P4_B6,
 { 0x00,        RJOY_4_BUTTON7, "Def P4 Button 7",      },      // KB_DEF_P4_B7,
 { 0x00,        RJOY_4_BUTTON8, "Def P4 Button 8",      },      // KB_DEF_P4_B8,

 { KEY_LCONTROL,0x00,           "Def Flipper 1 Left",   },      // KB_DEF_FLIPPER_1_L,
 { KEY_RCONTROL,0x00,           "Def Flipper 1 Right",  },      // KB_DEF_FLIPPER_1_R,
 { KEY_LSHIFT,  0x00,           "Def Flipper 2 Left",   },      // KB_DEF_FLIPPER_2_L,
 { KEY_RSHIFT,  0x00,           "Def Flipper 2 Right",  },      // KB_DEF_FLIPPER_2_R,
 { KEY_BACKSLASH2,0x00,         "Def Tilt Left",        },      // KB_DEF_TILT_L,
 { KEY_SLASH,   0x00,           "Def Tilt Right",       },      // KB_DEF_TILT_R,
 { KEY_Z,       0x00,           "Def Button 1 Left",    },      // KB_DEF_B1_L,
 { KEY_STOP,    0x00,           "Def Button 1 Right",   },      // KB_DEF_B1_R,
};

/******************************************************************************/
/*                                                                            */
/*                       DEFAULT EMULATOR KEY SETTINGS                        */
/*                                                                            */
/******************************************************************************/

struct DEF_INPUT def_input_list_emu[KB_EMU_DEF_COUNT] =
{
 { KEY_0,       0x00,           "Save Screenshot",      },      // KB_EMU_SCREENSHOT,
 { KEY_PGUP,    0x00,           "Increase frameskip",   },      // KB_EMU_INC_FRAMESKIP,
 { KEY_PGDN,    0x00,           "Decrease frameskip",   },      // KB_EMU_DEC_FRAMESKIP,
 { KEY_HOME,    0x00,           "Increase cpu skip",    },      // KB_EMU_INC_CPU_FRAMESKIP,
 { KEY_END,     0x00,           "Decrease cpu skip",    },      // KB_EMU_DEC_CPU_FRAMESKIP,
 { KEY_F2,      0x00,           "Save game",            },      // KB_EMU_SAVE_GAME,
 { KEY_F3,      0x00,           "Switch save slot",     },      // KB_EMU_SWITCH_SAVE_SLOT,
 { KEY_F4,      0x00,           "Load game",            },      // KB_EMU_LOAD_GAME,
 { KEY_F11,     0x00,           "Switch fps display",   },      // KB_EMU_SWITCH_FPS,
 { KEY_P,       0x00,           "Pause game",           },      // KB_EMU_PAUSE_GAME,
 { KEY_ESC,     0x00,           "Stop emulation",       },      // KB_EMU_STOP_EMULATION,
 { KEY_TAB,     0x00,           "Return to gui",        },      // KB_EMU_OPEN_GUI,
 { KEY_UP,      RJOY_1_UP,      "Screen up",            },      // KB_EMU_SCREEN_UP,
 { KEY_DOWN,    RJOY_1_DOWN,    "Screen down",          },      // KB_EMU_SCREEN_DOWN,
 { KEY_LEFT,    RJOY_1_LEFT,    "Screen left",          },      // KB_EMU_SCREEN_LEFT,
 { KEY_RIGHT,   RJOY_1_RIGHT,   "Screen right",         },      // KB_EMU_SCREEN_RIGHT,
 { KEY_TILDE,   0x00,           "Switch Mixer",         },      // Mixer
};


static void set_key_from_default(INPUT *inp)
{

   if(!(inp->default_key&0x0100))

      inp->Key = inp->default_key & 0xFF;

   else

      inp->Key = def_input_list[inp->default_key & 0xFF].scancode;

}

static void set_joy_from_default(INPUT *inp)
{

   if(!(inp->default_key&0x0100))

      inp->Joy = 0;

   else

      inp->Joy = def_input_list[inp->default_key & 0xFF].joycode;

}

void init_inputs(void)
{
   INPUT_INFO *input_src;

   InputCount = 0;

   input_src = current_game->input_list;

   if(input_src){

   while(input_src[InputCount].name){

   InputList[InputCount].default_key = input_src[InputCount].default_key;
   InputList[InputCount].InputName   = input_src[InputCount].name;
   InputList[InputCount].Address     = input_src[InputCount].offset;
   InputList[InputCount].Bit         = input_src[InputCount].bit_mask;
   InputList[InputCount].high_bit    = input_src[InputCount].flags;
   InputList[InputCount].auto_rate   = 0;
   InputList[InputCount].active_time = 0;

   set_key_from_default(&InputList[InputCount]);
   set_joy_from_default(&InputList[InputCount]);

   InputCount++;
   }

   }

}

void reset_game_keys(void)
{
   int ta;

   for(ta=0;ta<InputCount;ta++)
      set_key_from_default(&InputList[ta]);
}

void joy_reset_game_keys(void)
{
   int ta;

   for(ta=0;ta<InputCount;ta++)
      set_joy_from_default(&InputList[ta]);
}

void no_spaces(char *str)
{
   int ta,tb;

   tb=strlen(str);

   for(ta=0;ta<tb;ta++){
      if(((str[ta]<'A')||(str[ta]>'Z'))&&
         ((str[ta]<'a')||(str[ta]>'z'))&&
         ((str[ta]<'0')||(str[ta]>'9'))){
         str[ta]='_';
      }
   }
}

void load_game_keys(char *section)
{
   int ta;
   char key_name[64];

   use_custom_keys = raine_get_config_int(section,"use_custom_keys",0);

   // load keys if using custom keys

   if(use_custom_keys){

      for(ta=0;ta<InputCount;ta++){
         sprintf(key_name,"%s",InputList[ta].InputName);
         no_spaces(key_name);
         InputList[ta].Key = raine_get_config_int(section,key_name,InputList[ta].Key);
      }

   }
}

void save_game_keys(char *section)
{
   int ta;
   char key_name[64];

   // clear the old settings first (keep the file tidy)

   raine_clear_config_section(section);

   // save keys if using custom keys

   if(use_custom_keys){

      raine_set_config_int(section,"use_custom_keys",use_custom_keys);

      for(ta=0;ta<InputCount;ta++){
         sprintf(key_name,"%s",InputList[ta].InputName);
         no_spaces(key_name);
         raine_set_config_int(section,key_name,InputList[ta].Key);
      }

   }
}

void load_game_joys(char *section)
{
   int ta;
   char joy_name[64];

   joy_use_custom_keys = raine_get_config_int(section,"use_custom_joystick",0);

   // load joys if using custom joys

   if(joy_use_custom_keys){

      for(ta=0;ta<InputCount;ta++){
         sprintf(joy_name,"%s",InputList[ta].InputName);
         no_spaces(joy_name);
         InputList[ta].Joy = raine_get_config_int(section,joy_name,InputList[ta].Joy);
      }

   }
}

void save_game_joys(char *section)
{
   int ta;
   char joy_name[64];

   // clear the old settings first (keep the file tidy)

   raine_clear_config_section(section);

   // save joys if using custom joys

   if(joy_use_custom_keys){

      raine_set_config_int(section,"use_custom_joystick",joy_use_custom_keys);

      for(ta=0;ta<InputCount;ta++){
         sprintf(joy_name,"%s",InputList[ta].InputName);
         no_spaces(joy_name);
         raine_set_config_int(section,joy_name,InputList[ta].Joy);
      }

   }
}

void load_default_keys(char *section)
{
   int ta;
   char key_name[64];

   use_custom_keys = 0;

   for(ta=0;ta<KB_DEF_COUNT;ta++){
      sprintf(key_name,"%s",def_input_list[ta].name);
      no_spaces(key_name);
      def_input_list[ta].scancode = raine_get_config_int(section,key_name,def_input_list[ta].scancode);
   }
}

void save_default_keys(char *section)
{
   int ta;
   char key_name[64];

   for(ta=0;ta<KB_DEF_COUNT;ta++){
      sprintf(key_name,"%s",def_input_list[ta].name);
      no_spaces(key_name);
      raine_set_config_int(section,key_name,def_input_list[ta].scancode);
   }
}

void load_default_joys(char *section)
{
   int ta;
   char joy_name[64];

   joy_use_custom_keys = 0;

   for(ta=0;ta<KB_DEF_COUNT;ta++){
      sprintf(joy_name,"%s",def_input_list[ta].name);
      no_spaces(joy_name);
      def_input_list[ta].joycode = raine_get_config_int(section,joy_name,def_input_list[ta].joycode);
   }
}

void save_default_joys(char *section)
{
   int ta;
   char joy_name[64];

   for(ta=0;ta<KB_DEF_COUNT;ta++){
      sprintf(joy_name,"%s",def_input_list[ta].name);
      no_spaces(joy_name);
      raine_set_config_int(section,joy_name,def_input_list[ta].joycode);
   }
}

void load_emulator_keys(char *section)
{
   int ta;
   char key_name[64];

   for(ta=0;ta<KB_EMU_DEF_COUNT;ta++){
      sprintf(key_name,"%s",def_input_list_emu[ta].name);
      no_spaces(key_name);
      def_input_list_emu[ta].scancode = raine_get_config_int(section,key_name,def_input_list_emu[ta].scancode);
   }
}

void save_emulator_keys(char *section)
{
   int ta;
   char key_name[64];

   for(ta=0;ta<KB_EMU_DEF_COUNT;ta++){
      sprintf(key_name,"%s",def_input_list_emu[ta].name);
      no_spaces(key_name);
      raine_set_config_int(section,key_name,def_input_list_emu[ta].scancode);
   }
}

void load_emulator_joys(char *section)
{
   int ta;
   char joy_name[64];

   for(ta=0;ta<KB_EMU_DEF_COUNT;ta++){
      sprintf(joy_name,"%s",def_input_list_emu[ta].name);
      no_spaces(joy_name);
      def_input_list_emu[ta].joycode = raine_get_config_int(section,joy_name,def_input_list_emu[ta].joycode);
   }
}

void save_emulator_joys(char *section)
{
   int ta;
   char joy_name[64];

   for(ta=0;ta<KB_EMU_DEF_COUNT;ta++){
      sprintf(joy_name,"%s",def_input_list_emu[ta].name);
      no_spaces(joy_name);
      raine_set_config_int(section,joy_name,def_input_list_emu[ta].joycode);
   }
}

void update_rjoy_list(void)
{
   memset(&rjoy,0x00,RJOY_COUNT);

   if(JoystickType){

      poll_joystick();

      rjoy[0]=			FALSE;

      rjoy[RJOY_1_UP]=		joy[0].stick[0].axis[1].d1;
      rjoy[RJOY_1_DOWN]=	joy[0].stick[0].axis[1].d2;
      rjoy[RJOY_1_LEFT]=	joy[0].stick[0].axis[0].d1;
      rjoy[RJOY_1_RIGHT]=	joy[0].stick[0].axis[0].d2;
      rjoy[RJOY_1_BUTTON1]=	joy[0].button[0].b;
      rjoy[RJOY_1_BUTTON2]=	joy[0].button[1].b;
      rjoy[RJOY_1_BUTTON3]=	joy[0].button[2].b;
      rjoy[RJOY_1_BUTTON4]=	joy[0].button[3].b;
      rjoy[RJOY_1_BUTTON5]=	joy[0].button[4].b;
      rjoy[RJOY_1_BUTTON6]=	joy[0].button[5].b;
      rjoy[RJOY_1_BUTTON7]=	joy[0].button[6].b;
      rjoy[RJOY_1_BUTTON8]=	joy[0].button[7].b;

      rjoy[RJOY_1_BUTTON9]=	joy[0].button[8].b;
      rjoy[RJOY_1_BUTTON10]=	joy[0].button[9].b;
      rjoy[RJOY_1_BUTTON11]=	joy[0].button[10].b;
      rjoy[RJOY_1_BUTTON12]=	joy[0].button[11].b;

      rjoy[RJOY_2_UP]=		joy[1].stick[0].axis[1].d1;
      rjoy[RJOY_2_DOWN]=	joy[1].stick[0].axis[1].d2;
      rjoy[RJOY_2_LEFT]=	joy[1].stick[0].axis[0].d1;
      rjoy[RJOY_2_RIGHT]=	joy[1].stick[0].axis[0].d2;
      rjoy[RJOY_2_BUTTON1]=	joy[1].button[0].b;
      rjoy[RJOY_2_BUTTON2]=	joy[1].button[1].b;
      rjoy[RJOY_2_BUTTON3]=	joy[1].button[2].b;
      rjoy[RJOY_2_BUTTON4]=	joy[1].button[3].b;
      rjoy[RJOY_2_BUTTON5]=	joy[1].button[4].b;
      rjoy[RJOY_2_BUTTON6]=	joy[1].button[5].b;
      rjoy[RJOY_2_BUTTON7]=	joy[1].button[6].b;
      rjoy[RJOY_2_BUTTON8]=	joy[1].button[7].b;

      rjoy[RJOY_2_BUTTON9]=	joy[1].button[8].b;
      rjoy[RJOY_2_BUTTON10]=	joy[1].button[9].b;
      rjoy[RJOY_2_BUTTON11]=	joy[1].button[10].b;
      rjoy[RJOY_2_BUTTON12]=	joy[1].button[11].b;

      rjoy[RJOY_3_UP]=		joy[2].stick[0].axis[1].d1;
      rjoy[RJOY_3_DOWN]=	joy[2].stick[0].axis[1].d2;
      rjoy[RJOY_3_LEFT]=	joy[2].stick[0].axis[0].d1;
      rjoy[RJOY_3_RIGHT]=	joy[2].stick[0].axis[0].d2;
      rjoy[RJOY_3_BUTTON1]=	joy[2].button[0].b;
      rjoy[RJOY_3_BUTTON2]=	joy[2].button[1].b;
      rjoy[RJOY_3_BUTTON3]=	joy[2].button[2].b;
      rjoy[RJOY_3_BUTTON4]=	joy[2].button[3].b;
      rjoy[RJOY_3_BUTTON5]=	joy[2].button[4].b;
      rjoy[RJOY_3_BUTTON6]=	joy[2].button[5].b;
      rjoy[RJOY_3_BUTTON7]=	joy[2].button[6].b;
      rjoy[RJOY_3_BUTTON8]=	joy[2].button[7].b;

      rjoy[RJOY_3_BUTTON9]=	joy[2].button[8].b;
      rjoy[RJOY_3_BUTTON10]=	joy[2].button[9].b;
      rjoy[RJOY_3_BUTTON11]=	joy[2].button[10].b;
      rjoy[RJOY_3_BUTTON12]=	joy[2].button[11].b;

      rjoy[RJOY_4_UP]=		joy[3].stick[0].axis[1].d1;
      rjoy[RJOY_4_DOWN]=	joy[3].stick[0].axis[1].d2;
      rjoy[RJOY_4_LEFT]=	joy[3].stick[0].axis[0].d1;
      rjoy[RJOY_4_RIGHT]=	joy[3].stick[0].axis[0].d2;
      rjoy[RJOY_4_BUTTON1]=	joy[3].button[0].b;
      rjoy[RJOY_4_BUTTON2]=	joy[3].button[1].b;
      rjoy[RJOY_4_BUTTON3]=	joy[3].button[2].b;
      rjoy[RJOY_4_BUTTON4]=	joy[3].button[3].b;
      rjoy[RJOY_4_BUTTON5]=	joy[3].button[4].b;
      rjoy[RJOY_4_BUTTON6]=	joy[3].button[5].b;
      rjoy[RJOY_4_BUTTON7]=	joy[3].button[6].b;
      rjoy[RJOY_4_BUTTON8]=	joy[3].button[7].b;

      rjoy[RJOY_4_BUTTON9]=	joy[3].button[8].b;
      rjoy[RJOY_4_BUTTON10]=	joy[3].button[9].b;
      rjoy[RJOY_4_BUTTON11]=	joy[3].button[10].b;
      rjoy[RJOY_4_BUTTON12]=	joy[3].button[11].b;
   }
}

// update_inputs():
// Goes through the input list setting/clearing the mapped RAM[] bits

void update_inputs(void)
{
   static UINT8 autofire_timer[6];

   UINT8  stick_logic[4];
   UINT8 *buffer;
   int    ta;
   int    input_valid;

   stick_logic[0]=0x00;
   stick_logic[1]=0x00;
   stick_logic[2]=0x00;
   stick_logic[3]=0x00;

   // Autofire timer emulation

   for(ta=0;ta<6;ta++){
      autofire_timer[ta] ++;
      if(autofire_timer[ta] >= (ta<<1))
         autofire_timer[ta] = 0;
   }

   for(ta=0;ta<InputCount;ta++){

      if((key[InputList[ta].Key])||(rjoy[InputList[ta].Joy])){

      // Increment active timer

      InputList[ta].active_time ++;

      // Assume Input is valid

      input_valid=1;

      // Toggle Autofire settings

      if(InputList[ta].auto_rate){
         if(autofire_timer[InputList[ta].auto_rate] >= InputList[ta].auto_rate) input_valid=0;
      }

      // Disable the following situations:
      // 1) Impossible joystick inputs (joystick can be up or down, but not both)
      // 2) Coin inputs must last approx 250ms (prevent taito coin error)

      switch(InputList[ta].default_key){
         case KB_DEF_P1_UP:
            stick_logic[0] |= 0x01;
            if((stick_logic[0]&0x02)) input_valid=0;
         break;
         case KB_DEF_P1_DOWN:
            stick_logic[0] |= 0x02;
            if((stick_logic[0]&0x01)) input_valid=0;
         break;
         case KB_DEF_P1_LEFT:
            stick_logic[0] |= 0x04;
            if((stick_logic[0]&0x08)) input_valid=0;
         break;
         case KB_DEF_P1_RIGHT:
            stick_logic[0] |= 0x08;
            if((stick_logic[0]&0x04)) input_valid=0;
         break;
         case KB_DEF_P2_UP:
            stick_logic[1] |= 0x01;
            if((stick_logic[1]&0x02)) input_valid=0;
         break;
         case KB_DEF_P2_DOWN:
            stick_logic[1] |= 0x02;
            if((stick_logic[1]&0x01)) input_valid=0;
         break;
         case KB_DEF_P2_LEFT:
            stick_logic[1] |= 0x04;
            if((stick_logic[1]&0x08)) input_valid=0;
         break;
         case KB_DEF_P2_RIGHT:
            stick_logic[1] |= 0x08;
            if((stick_logic[1]&0x04)) input_valid=0;
         break;
         case KB_DEF_P3_UP:
            stick_logic[2] |= 0x01;
            if((stick_logic[2]&0x02)) input_valid=0;
         break;
         case KB_DEF_P3_DOWN:
            stick_logic[2] |= 0x02;
            if((stick_logic[2]&0x01)) input_valid=0;
         break;
         case KB_DEF_P3_LEFT:
            stick_logic[2] |= 0x04;
            if((stick_logic[2]&0x08)) input_valid=0;
         break;
         case KB_DEF_P3_RIGHT:
            stick_logic[2] |= 0x08;
            if((stick_logic[2]&0x04)) input_valid=0;
         break;
         case KB_DEF_P4_UP:
            stick_logic[3] |= 0x01;
            if((stick_logic[3]&0x02)) input_valid=0;
         break;
         case KB_DEF_P4_DOWN:
            stick_logic[3] |= 0x02;
            if((stick_logic[3]&0x01)) input_valid=0;
         break;
         case KB_DEF_P4_LEFT:
            stick_logic[3] |= 0x04;
            if((stick_logic[3]&0x08)) input_valid=0;
         break;
         case KB_DEF_P4_RIGHT:
            stick_logic[3] |= 0x08;
            if((stick_logic[3]&0x04)) input_valid=0;
         break;
         case KB_DEF_COIN1:
         case KB_DEF_COIN2:
         case KB_DEF_COIN3:
         case KB_DEF_COIN4:
            if(InputList[ta].active_time > 15) input_valid=0;
         break;
         default:
         break;
      }

      }
      else{
         InputList[ta].active_time = 0;
         input_valid = 0;
      }


      if(InputList[ta].Address < 0x100)

         buffer = input_buffer;

      else

         buffer = RAM;

      buffer += InputList[ta].Address;


      if(input_valid ^ InputList[ta].high_bit)

         *buffer &= ~ InputList[ta].Bit;

      else

         *buffer |=   InputList[ta].Bit;

   }

}

/******************************************************************************/
/*                                                                            */
/*                              GLOBAL STRINGS                                */
/*                                                                            */
/******************************************************************************/

char MSG_COIN1[]        = "Coin A";
char MSG_COIN2[]        = "Coin B";
char MSG_COIN3[]        = "Coin C";
char MSG_COIN4[]        = "Coin D";

char MSG_TILT[]         = "Tilt";
char MSG_SERVICE[]      = "Service";
char MSG_TEST[]         = "Test";
char MSG_UNKNOWN[]      = "Unknown";
char MSG_YES[] = "Yes";
char MSG_NO[] = "No";
char MSG_FREE_PLAY[] = "Free Play";
char MSG_UNUSED[] = "Unused";
char MSG_COINAGE[] = "Coinage";

char MSG_P1_START[]     = "Player1 Start";

char MSG_P1_UP[]        = "Player1 Up";
char MSG_P1_DOWN[]      = "Player1 Down";
char MSG_P1_LEFT[]      = "Player1 Left";
char MSG_P1_RIGHT[]     = "Player1 Right";

char MSG_P1_B1[]        = "Player1 Button1";
char MSG_P1_B2[]        = "Player1 Button2";
char MSG_P1_B3[]        = "Player1 Button3";
char MSG_P1_B4[]        = "Player1 Button4";
char MSG_P1_B5[]        = "Player1 Button5";
char MSG_P1_B6[]        = "Player1 Button6";
char MSG_P1_B7[]        = "Player1 Button7";
char MSG_P1_B8[]        = "Player1 Button8";

char MSG_P2_START[]     = "Player2 Start";

char MSG_P2_UP[]        = "Player2 Up";
char MSG_P2_DOWN[]      = "Player2 Down";
char MSG_P2_LEFT[]      = "Player2 Left";
char MSG_P2_RIGHT[]     = "Player2 Right";

char MSG_P2_B1[]        = "Player2 Button1";
char MSG_P2_B2[]        = "Player2 Button2";
char MSG_P2_B3[]        = "Player2 Button3";
char MSG_P2_B4[]        = "Player2 Button4";
char MSG_P2_B5[]        = "Player2 Button5";
char MSG_P2_B6[]        = "Player2 Button6";
char MSG_P2_B7[]        = "Player2 Button7";
char MSG_P2_B8[]        = "Player2 Button8";

char MSG_P3_START[]     = "Player3 Start";

char MSG_P3_UP[]        = "Player3 Up";
char MSG_P3_DOWN[]      = "Player3 Down";
char MSG_P3_LEFT[]      = "Player3 Left";
char MSG_P3_RIGHT[]     = "Player3 Right";

char MSG_P3_B1[]        = "Player3 Button1";
char MSG_P3_B2[]        = "Player3 Button2";
char MSG_P3_B3[]        = "Player3 Button3";
char MSG_P3_B4[]        = "Player3 Button4";
char MSG_P3_B5[]        = "Player3 Button5";
char MSG_P3_B6[]        = "Player3 Button6";
char MSG_P3_B7[]        = "Player3 Button7";
char MSG_P3_B8[]        = "Player3 Button8";

char MSG_P4_START[]     = "Player4 Start";

char MSG_P4_UP[]        = "Player4 Up";
char MSG_P4_DOWN[]      = "Player4 Down";
char MSG_P4_LEFT[]      = "Player4 Left";
char MSG_P4_RIGHT[]     = "Player4 Right";

char MSG_P4_B1[]        = "Player4 Button1";
char MSG_P4_B2[]        = "Player4 Button2";
char MSG_P4_B3[]        = "Player4 Button3";
char MSG_P4_B4[]        = "Player4 Button4";
char MSG_P4_B5[]        = "Player4 Button5";
char MSG_P4_B6[]        = "Player4 Button6";
char MSG_P4_B7[]        = "Player4 Button7";
char MSG_P4_B8[]        = "Player4 Button8";

char MSG_FLIPPER_1_L[]  = "Flipper 1 Left";
char MSG_FLIPPER_1_R[]  = "Flipper 1 Right";
char MSG_FLIPPER_2_L[]  = "Flipper 2 Left";
char MSG_FLIPPER_2_R[]  = "Flipper 2 Right";
char MSG_TILT_L[]       = "Tilt Left";
char MSG_TILT_R[]       = "Tilt Right";
char MSG_B1_L[]         = "Button 1 Left";
char MSG_B1_R[]         = "Button 1 Right";

/* DSW SECTION */

char MSG_DSWA_BIT1[]    = "DSW A Bit 1";        // Since most dsw info sheets
char MSG_DSWA_BIT2[]    = "DSW A Bit 2";        // number the bits 1-8, we will
char MSG_DSWA_BIT3[]    = "DSW A Bit 3";        // too, although 0-7 is a more
char MSG_DSWA_BIT4[]    = "DSW A Bit 4";        // correct syntax for progammers.
char MSG_DSWA_BIT5[]    = "DSW A Bit 5";
char MSG_DSWA_BIT6[]    = "DSW A Bit 6";
char MSG_DSWA_BIT7[]    = "DSW A Bit 7";
char MSG_DSWA_BIT8[]    = "DSW A Bit 8";

char MSG_DSWB_BIT1[]    = "DSW B Bit 1";
char MSG_DSWB_BIT2[]    = "DSW B Bit 2";
char MSG_DSWB_BIT3[]    = "DSW B Bit 3";
char MSG_DSWB_BIT4[]    = "DSW B Bit 4";
char MSG_DSWB_BIT5[]    = "DSW B Bit 5";
char MSG_DSWB_BIT6[]    = "DSW B Bit 6";
char MSG_DSWB_BIT7[]    = "DSW B Bit 7";
char MSG_DSWB_BIT8[]    = "DSW B Bit 8";

char MSG_DSWC_BIT1[]    = "DSW C Bit 1";
char MSG_DSWC_BIT2[]    = "DSW C Bit 2";
char MSG_DSWC_BIT3[]    = "DSW C Bit 3";
char MSG_DSWC_BIT4[]    = "DSW C Bit 4";
char MSG_DSWC_BIT5[]    = "DSW C Bit 5";
char MSG_DSWC_BIT6[]    = "DSW C Bit 6";
char MSG_DSWC_BIT7[]    = "DSW C Bit 7";
char MSG_DSWC_BIT8[]    = "DSW C Bit 8";

char MSG_COIN_SLOTS[]   = "Coin Slots";

char MSG_1COIN_1PLAY[]  = "1 Coin/1 Credit";
char MSG_1COIN_2PLAY[]  = "1 Coin/2 Credits";
char MSG_1COIN_3PLAY[]  = "1 Coin/3 Credits";
char MSG_1COIN_4PLAY[]  = "1 Coin/4 Credits";
char MSG_1COIN_5PLAY[]  = "1 Coin/5 Credits";
char MSG_1COIN_6PLAY[]  = "1 Coin/6 Credits";
char MSG_1COIN_7PLAY[]  = "1 Coin/7 Credits";
char MSG_1COIN_8PLAY[]  = "1 Coin/8 Credits";
char MSG_1COIN_9PLAY[]  = "1 Coin/9 Credits";

char MSG_2COIN_1PLAY[]  = "2 Coins/1 Credit";
char MSG_2COIN_2PLAY[]  = "2 Coins/2 Credits";
char MSG_2COIN_3PLAY[]  = "2 Coins/3 Credits";
char MSG_2COIN_4PLAY[]  = "2 Coins/4 Credits";
char MSG_2COIN_5PLAY[]  = "2 Coins/5 Credits";
char MSG_2COIN_6PLAY[]  = "2 Coins/6 Credits";
char MSG_2COIN_7PLAY[]  = "2 Coins/7 Credits";
char MSG_2COIN_8PLAY[]  = "2 Coins/8 Credits";

char MSG_3COIN_1PLAY[]  = "3 Coins/1 Credit";
char MSG_3COIN_2PLAY[]  = "3 Coins/2 Credits";
char MSG_3COIN_3PLAY[]  = "3 Coins/3 Credits";
char MSG_3COIN_4PLAY[]  = "3 Coins/4 Credits";
char MSG_3COIN_5PLAY[]  = "3 Coins/5 Credits";
char MSG_3COIN_6PLAY[]  = "3 Coins/6 Credits";
char MSG_3COIN_7PLAY[]  = "3 Coins/7 Credits";
char MSG_3COIN_8PLAY[]  = "3 Coins/8 Credits";

char MSG_4COIN_1PLAY[]  = "4 Coins/1 Credit";
char MSG_4COIN_2PLAY[]  = "4 Coins/2 Credits";
char MSG_4COIN_3PLAY[]  = "4 Coins/3 Credits";
char MSG_4COIN_4PLAY[]  = "4 Coins/4 Credits";
char MSG_4COIN_5PLAY[]  = "4 Coins/5 Credits";
char MSG_4COIN_6PLAY[]  = "4 Coins/6 Credits";
char MSG_4COIN_7PLAY[]  = "4 Coins/7 Credits";
char MSG_4COIN_8PLAY[]  = "4 Coins/8 Credits";

char MSG_5COIN_1PLAY[]  = "5 Coins/1 Credit";
char MSG_5COIN_2PLAY[]  = "5 Coins/2 Credits";
char MSG_5COIN_3PLAY[]  = "5 Coins/3 Credits";
char MSG_5COIN_4PLAY[]  = "5 Coins/4 Credits";

char MSG_6COIN_1PLAY[]  = "6 Coins/1 Credit";
char MSG_6COIN_2PLAY[]  = "6 Coins/2 Credits";
char MSG_6COIN_3PLAY[]  = "6 Coins/3 Credits";
char MSG_6COIN_4PLAY[]  = "6 Coins/4 Credits";

char MSG_7COIN_1PLAY[]  = "7 Coins/1 Credit";
char MSG_7COIN_2PLAY[]  = "7 Coins/2 Credits";
char MSG_7COIN_3PLAY[]  = "7 Coins/3 Credits";
char MSG_7COIN_4PLAY[]  = "7 Coins/4 Credits";

char MSG_8COIN_1PLAY[]  = "8 Coins/1 Credit";
char MSG_8COIN_2PLAY[]  = "8 Coins/2 Credits";
char MSG_8COIN_3PLAY[]  = "8 Coins/3 Credits";
char MSG_8COIN_4PLAY[]  = "8 Coins/4 Credits";

char MSG_9COIN_1PLAY[] = "9 Coins/1 Credit";

char MSG_OFF[]          = "Off";
char MSG_ON[]           = "On";

char MSG_SCREEN[]       = "Flip Screen";
char MSG_NORMAL[]       = "Normal";
char MSG_INVERT[]       = "Invert";

char MSG_TEST_MODE[]    = "Test Mode";

char MSG_DEMO_SOUND[]   = "Demo Sound";

char MSG_CONTINUE_PLAY[]= "Continue Play";
char MSG_EXTRA_LIFE[]   = "Extra Life";
char MSG_LIVES[]        = "Lives";

char MSG_CHEAT[]        = "Cheat";

char MSG_DIFFICULTY[]   = "Difficulty";
char MSG_EASY[]         = "Easy";
char MSG_HARD[]         = "Hard";
char MSG_HARDEST[]      = "Hardest";

char MSG_CABINET[]      = "Cabinet";
char MSG_UPRIGHT[]      = "Upright";
char MSG_TABLE[]        = "Table";

/******************************************************************************/
/*                                                                            */
/*                          KEYBOARD LED SUPPORT                              */
/*                                                                            */
/******************************************************************************/

// status of leds and old status (internal)

static UINT32 leds,oldleds;

#if 0
static UINT32 led_data;
#endif

// switch led: call this to turn a led on/off

void switch_led(int num, int on)
{
   static const UINT32 led_flags[3] =
   {
      KB_NUMLOCK_FLAG,
      KB_CAPSLOCK_FLAG,
      KB_SCROLOCK_FLAG
   };

   // Update Keyboard LEDS (support only 3)

   if(num<3){

      if(on&1)

	 leds |=  (led_flags[num]);

      else

	 leds &= ~(led_flags[num]);

   }
}

// update leds: call this once a frame

void update_leds(void)
{
   if(use_leds){

   if(leds!=oldleds){
      oldleds=leds;
      set_leds(leds);
   }

   }
}

// turn off all leds: call this when resetting game

void init_leds(void)
{
   if(use_leds){

      leds = 0;
      oldleds = 1;

   }
}

// forced update leds: call this when going back into game

void force_update_leds(void)
{
   if(use_leds)

      set_leds(leds);

}

/******************************************************************************/
