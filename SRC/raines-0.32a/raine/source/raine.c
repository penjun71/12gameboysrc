/******************************************************************************/
/*									      */
/*	   RAINE (680x0 arcade emulation) v0.30 (c) 1998-2001 Raine Team      */
/*									      */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include "rgui.h" // setup mode lists...

#ifdef RAINE_DOS
#include <conio.h>
#endif

#include "raine.h"              // General defines and stuff
#include "gui.h"                // Interface stuff
#include "savegame.h"           // Save/Load game stuff
#include "sasound.h"            // Sound Sample stuff
#include "dsw.h"                // Dipswitch stuff
#include "ingame.h"             // screen handling routines
#include "arpro.h"              // Action Replay stuff
#include "control.h"            // Control/Input related stuff
#include "debug.h"              // Debugging routines
#include "config.h"             // Command Line + Config file routines

#include "starhelp.h"           // M68000 Support Interface
#include "mz80help.h"           // MZ80 Support Interface
#ifdef HAVE_6502
#include "m6502hlp.h"           // M6502 Support Interface
#endif
#include "68020.h"              // M68020 Engine + Support Interface
#include "games.h"              // Game list
#include "files.h"

#ifdef __MINGW32__
#define mkdir_rwx(name) mkdir(name)
#else
#define mkdir_rwx(name) mkdir(name, 0700)
#endif

#ifdef OLD_ALLEG
// Cutback Allegro
// ---------------
#ifdef RAINE_DOS
DECLARE_COLOR_DEPTH_LIST(
   COLOR_DEPTH_8
)
#endif
#endif

#if HAS_YM3812
extern int use_emulated_ym3812;
#endif

#define STREAM_MODE	SOUND_STREAM_WAIT /* new sasound */
//#define STREAM_MODE	SOUND_STREAM_OLDTYPE /* old sasound(sample.c) */

// new version crashes!! :(
//#define NEW_SND

int StreamMode = SOUND_STREAM_WAIT; /* default : new version */

/**************************************************************/

// like strcat, but adds s2 to the start instead of the end of s1

void strcatrev(char *s1, char *s2)
{
   char tmp_str[256];

   sprintf(tmp_str,"%s%s",s2,s1);
   strcpy(s1,tmp_str);
}

void backslash(char *s)
{
#ifndef RAINE_UNIX
  while(*s){

      if(*s == '/')

	 *s = '\\';

      s ++;
   }
#endif
}

#ifndef RAINE_DOS

#define cprintf printf

void textcolor(int i)
{
	(void)(i);
}

#endif

#ifdef RAINE_UNIX
int x_display = 0; // Indique si on est sous X...
#endif

// main():
// Start here, end here.

int main(int argc,char *argv[])
{
   int i;
   char str[256];
#ifdef GFX_FBCON
   FILE *f;
#endif
   
   /*

   show emulator version on the command line

   */

   textcolor(7);  cprintf(" ");
   textcolor(12); cprintf("R");
   textcolor(14); cprintf("A");
   textcolor(10); cprintf("I");
   textcolor(9);  cprintf("N");
   textcolor(13); cprintf("E");
   textcolor(15); cprintf(" (680x0 Arcade Emulation) " VERSION " (c)1998-2001 " HANDLE);
   textcolor(7);
   cprintf("\r\n\r\n");

#ifdef RAINE_DEBUG
   textcolor(5); cprintf("[PRIVATE BETA VERSION]");
   textcolor(7);
   cprintf("\r\n\r\n");
#endif
   
   /*

   get executable path

   */

#if !defined(RAINE_WIN32) || defined(__MINGW32__)
   strcpy(dir_cfg.exe_path, argv[0]);
#else
   strcpy(dir_cfg.exe_path, __argv[0]);
#endif
   strcpy(get_filename(dir_cfg.exe_path),"");
   strlwr(dir_cfg.exe_path);
   backslash(dir_cfg.exe_path);

   // set config filename

#ifdef RAINE_DOS
   strcpy(dir_cfg.config_file, "raine.cfg");
#else
#ifdef RAINE_WIN32
   strcpy(dir_cfg.config_file, "raine32.cfg");
#else
   strcpy(dir_cfg.config_file, "rainex.cfg");
#endif
#endif

   // set default screen settings

#ifdef RAINE_DOS
   display_cfg.screen_type	= GFX_MODEX;
   display_cfg.screen_x 	= 320;
   display_cfg.screen_y 	= 240;
#else
#ifdef RAINE_UNIX
   display_cfg.screen_type	= GFX_AUTODETECT;
   display_cfg.screen_x 	= 320;
   display_cfg.screen_y 	= 240;
#else
#ifdef _DEBUG
   display_cfg.screen_type	= GFX_DIRECTX_WIN;
#else
   display_cfg.screen_type	= GFX_DIRECTX;
#endif
   display_cfg.screen_x 	= 320;
   display_cfg.screen_y 	= 240;
#endif
#endif

#ifdef RAINE_UNIX
   install_allegro(SYSTEM_NONE,&errno,atexit);
   /* This is necessary in the rare case when you want to run a -romcheck */
   /* on a remote system (I had to do it...). If you call allegro_init, it */
   /* installs a X conection, which slows things to a crawl. Believe me, */
   /* it's better this way ! */
   /* The only problem is that you can't append raine.dat in the exe here */
#else
   allegro_init();
#endif   

#ifdef RAINE_UNIX
   if (getenv("DISPLAY")) {
//     int bpp = desktop_color_depth();
//     printf("depth detected : %d\n",bpp);
//     if (bpp>8)
       x_display=1;
       wants_switch_res = 1;
   }
#endif
#ifdef GFX_SVGALIB
   if (!x_display && (!geteuid()))
     setup_svga_modes();
#endif

   set_color_depth(8);

   set_color_conversion(COLORCONV_TOTAL);

   sprintf(str,"%sraine.dat",dir_cfg.exe_path);
   RaineData = load_datafile(str);

   if(!RaineData){
      RaineData = load_datafile("#");
      if(!RaineData){
	 allegro_message("Unable to find raine.dat (must be in raine dir, or attached to raine.exe)\n");
	 exit(1);
      }
   }

   raine_cfg.no_gui = 0;
   raine_cfg.hide = 0;
   raine_cfg.req_load_game = 0;
   raine_cfg.req_game_index = 0;

   sprintf(str, "%sconfig",   dir_cfg.exe_path );
   mkdir_rwx(str);
   sprintf(str, "%ssavegame", dir_cfg.exe_path );
   mkdir_rwx(str);
   sprintf(str, "%ssavedata", dir_cfg.exe_path );
   mkdir_rwx(str);
   sprintf(str, "%sscreens",  dir_cfg.exe_path );
   mkdir_rwx(str);
   sprintf(str, "%sroms",     dir_cfg.exe_path );
   mkdir_rwx(str);

   sprintf(dir_cfg.screen_dir,"%sscreens" SLASH, dir_cfg.exe_path);
   sprintf(dir_cfg.rom_dir[0],"%sroms" SLASH, dir_cfg.exe_path);

   dir_cfg.rom_dir[1][0] = 0;
   dir_cfg.rom_dir[2][0] = 0;
   dir_cfg.rom_dir[3][0] = 0;

   load_main_config();

   sprintf(str,"%sconfig" SLASH "%s",dir_cfg.exe_path, dir_cfg.config_file);

   raine_push_config_state();
   raine_set_config_file(str);

#ifdef RAINE_DEBUG
   debug_mode			= raine_get_config_int( "General",      "debug_mode",           0);
   open_debug();
#endif

   raine_cfg.wibble		= raine_get_config_int( "General",      "wibble",               0);

   raine_cfg.extra_games = 0;

   raine_cfg.run_count		= raine_get_config_int( "General",      "run_count",                            0);
   raine_cfg.version_no 	= raine_get_config_int( "General",      "version",                              0);
   display_cfg.limit_speed	= raine_get_config_int( "General",      "LimitSpeed",                           1);
   display_cfg.frame_skip	= raine_get_config_int( "General",      "frame_skip",                           0);
   raine_cfg.show_fps_mode	= raine_get_config_int( "General",      "ShowFPS",                              0);
   use_rdtsc			= raine_get_config_int( "General",      "UseRdtsc",                             1);
   use_leds			= raine_get_config_int( "General",      "UseLEDS",                              1);

   raine_cfg.save_game_screen_settings	  = raine_get_config_int(	"General",      "save_game_screen_settings",            0);

   raine_cfg.auto_save	= raine_get_config_int( "General", "auto_save", 0);

   if((use_rdtsc==0)&&(raine_cfg.show_fps_mode>2)) raine_cfg.show_fps_mode=0;

   // DISPLAY

   if(display_cfg.scanlines == 2) display_cfg.screen_y <<= 1;

   display_cfg.screen_type	= raine_get_config_id(	"Display",      "screen_type",          display_cfg.screen_type);
   display_cfg.screen_x 	= raine_get_config_int( "Display",      "screen_x",             display_cfg.screen_x);
   display_cfg.screen_y 	= raine_get_config_int( "Display",      "screen_y",             display_cfg.screen_y);
   display_cfg.bpp		= raine_get_config_int( "Display",      "bpp",                  8);
   display_cfg.scanlines	= raine_get_config_int( "Display",      "scanlines",            0);
   display_cfg.vsync		= raine_get_config_int( "Display",      "vsync",                0);
   display_cfg.triple_buffer	= raine_get_config_int( "Display",      "triple_buffer",        0);
   display_cfg.eagle_fx 	= raine_get_config_int( "Display",      "eagle_fx",             0);
   display_cfg.pixel_double	= raine_get_config_int( "Display",      "pixel_double",         0);
   display_cfg.fast_set_pal	= raine_get_config_int( "Display",      "fast_set_pal",         1);
   display_cfg.user_rotate	= raine_get_config_int( "Display",      "rotate",               0);
   display_cfg.user_flip	= raine_get_config_int( "Display",      "flip",                 0);
   display_cfg.no_rotate	= raine_get_config_int( "Display",      "no_rotate",            0);
   display_cfg.no_flip		= raine_get_config_int( "Display",      "no_flip",              0);
#ifdef RAINE_DOS
   display_cfg.arcade_h_timing	= raine_get_config_int( "Display",      "arcade_h_timing",      105);
   display_cfg.arcade_v_timing	= raine_get_config_int( "Display",      "arcade_v_timing",      9);
   display_cfg.arcade_center_x	= raine_get_config_int( "Display",      "arcade_center_x",      8);
   display_cfg.arcade_center_y	= raine_get_config_int( "Display",      "arcade_center_y",      4);
#endif

   if(display_cfg.scanlines == 2) display_cfg.screen_y >>= 1;

   if(display_cfg.vsync > 1) display_cfg.vsync = 0;

   // RAINE GUI

   rgui_cfg.gui_col_text_1	= raine_get_config_hex( "GUI",          "gui_col_text_1",               0xFFFFFF);
   rgui_cfg.gui_col_text_2	= raine_get_config_hex( "GUI",          "gui_col_text_2",               0x6FEFEF);
   rgui_cfg.gui_col_black	= raine_get_config_hex( "GUI",          "gui_col_black",                0x000000);
   rgui_cfg.gui_col_select	= raine_get_config_hex( "GUI",          "gui_col_select",               0x787878);
   rgui_cfg.gui_box_col_high_2	= raine_get_config_hex( "GUI",          "gui_box_col_high_2",           0x5858A8);
   rgui_cfg.gui_box_col_high_1	= raine_get_config_hex( "GUI",          "gui_box_col_high_1",           0x383888);
   rgui_cfg.gui_box_col_middle	= raine_get_config_hex( "GUI",          "gui_box_col_middle",           0x282878);
   rgui_cfg.gui_box_col_low_1	= raine_get_config_hex( "GUI",          "gui_box_col_low_1",            0x181868);
   rgui_cfg.gui_box_col_low_2	= raine_get_config_hex( "GUI",          "gui_box_col_low_2",            0x080848);

   rgui_cfg.game_list_mode	= raine_get_config_int( "GUI",          "game_list_mode",       0);

   strcpy(rgui_cfg.bg_image,	 raine_get_config_string( "GUI",        "bg_image",             ""));

   strcpy(rgui_cfg.font_datafile,     raine_get_config_string( "GUI",   "font_datafile",                ""));

   // DIRECTORIES

#ifdef RAINE_UNIX
   dir_cfg.long_file_names = 1;
#else
   dir_cfg.long_file_names	= raine_get_config_int( "Directories",  "long_file_names",      1);
#endif

   strcpy(dir_cfg.screen_dir,	 raine_get_config_string( "Directories", "screenshots",   dir_cfg.screen_dir));
   strcpy(dir_cfg.rom_dir[0],	 raine_get_config_string( "Directories", "rom_dir_0",     dir_cfg.rom_dir[0]));
   strcpy(dir_cfg.rom_dir[1],	 raine_get_config_string( "Directories", "rom_dir_1",     dir_cfg.rom_dir[1]));
   strcpy(dir_cfg.rom_dir[2],	 raine_get_config_string( "Directories", "rom_dir_2",     dir_cfg.rom_dir[2]));
   strcpy(dir_cfg.rom_dir[3],	 raine_get_config_string( "Directories", "rom_dir_3",     dir_cfg.rom_dir[3]));
   strcpy(dir_cfg.cheat_file,	 raine_get_config_string( "Directories", "cheat_file",    "cheats.cfg"));
   strcpy(dir_cfg.language_file, raine_get_config_string( "Directories", "language_file", "english.cfg"));

   put_backslash(dir_cfg.screen_dir);
   strlwr(dir_cfg.screen_dir);

   for(i = 0; i < 4; i ++){
      if(dir_cfg.rom_dir[i][0]){
	 put_backslash(dir_cfg.rom_dir[i]);
	 strlwr(dir_cfg.rom_dir[i]);
      }
   }

   /*

   make sure any "output" directories are created

   */

   mkdir_rwx(dir_cfg.screen_dir);

   SoundEnabled=1;

#ifdef NEW_SND
   /**** add hiro-shi!! ****/
   /***********************************************************/
   /*	stream update mode				      */
   /*	SOUND_STREAM_WAIT   : new update mode		      */
   /*	SOUND_STREAM_OLDTYPE : old update mode (raine v0.1x)  */
   /***********************************************************/
   StreamMode		= raine_get_config_int(       "Sound",       "UpdateMode", SOUND_STREAM_WAIT );/* default : new mode */
   if( StreamMode < SOUND_STREAM_WAIT || StreamMode >= SOUND_STREAM_TIMER )
#endif
     StreamMode = SOUND_STREAM_WAIT;


   sound_cfg.sound_card = raine_get_config_id(	"Sound",        "sound_card",           DIGI_NONE);
   sound_cfg.sample_rate= raine_get_config_int( "Sound",        "sample_rate",          22050 );

   if (sound_cfg.sample_rate < 15000)
      sound_cfg.sample_rate = 11025;
   else if (sound_cfg.sample_rate < 30000)
	 sound_cfg.sample_rate = 22050;
      else
	 sound_cfg.sample_rate = 44100;

   RaineSoundCard	= raine_get_config_int( "Sound",        "SoundCard",            0);
   SampleRate = sound_cfg.sample_rate;
#if HAS_YM3812
   use_emulated_ym3812	= 1;
#endif

   STREAM_BUFFER_MAXA	= raine_get_config_int( "Sound",        "BufferMaxA",           DEF_STREAM_BUFFER_MAXA);
   MODEB_FRAME_SIZE	= raine_get_config_int( "Sound",        "ModeBFrameSize",       DEF_MODEB_FRAME_SIZE);          // 60
   MODEB_UPDATE_COUNT	= raine_get_config_int( "Sound",        "ModeBUpdateCount",     DEF_MODEB_UPDATE_COUNT);        // 4
   MODEB_ERROR_MAX	= raine_get_config_int( "Sound",        "ModeBErrorWaitMax",    DEF_STREAM_UPDATE_ERROR_MAX );  // 16
   STREAM_BUFFER_MAXB	= MODEB_FRAME_SIZE;			/* don't change this!! */
   MODEB_MASK		= MODEB_FRAME_SIZE/MODEB_UPDATE_COUNT;	/* don't change this!! */

   DariusStereo 	= raine_get_config_int( "Sound",        "DariusStereo",         0); /* 0:monaural 1=stereo */

   JoystickType 	= raine_get_config_id(	"Control",      "JoystickType",         JOY_TYPE_NONE);

   // KEYBOARD DEFAULT

   load_default_keys("default_game_key_config");

   // JOYSTICK DEFAULT

   load_default_joys("default_game_joy_config");

   // KEYBOARD EMULATOR

   load_emulator_keys("emulator_key_config");

   // JOYSTICK EMULATOR

   load_emulator_joys("emulator_joy_config");

   raine_pop_config_state();

   // JOYSTICK ALLEGRO

   sprintf(str,"%sconfig" SLASH "%s",dir_cfg.exe_path, dir_cfg.config_file);
   load_joystick_data(str);

   init_game_list();
   parse_command_line(argc,argv);
#ifdef RAINE_UNIX
   allegro_exit();

   allegro_init();
#endif
   
#ifdef GFX_FBCON
   // With Frame buffer, I have to avoid the standard modex, or raine freezes!

   if (!x_display) {
     f = fopen("/dev/fb0","rb");
     if (f) {
       fclose(f);
       setup_fb_modes();
       
       if (display_cfg.screen_x < 640 || display_cfg.screen_y < 480) {
	 display_cfg.screen_x = 640;
	 display_cfg.screen_y = 480;
       }
       display_cfg.screen_type = GFX_FBCON;
     }
   }
#endif
#ifdef GFX_XDGA2
   if (geteuid()==0){ // root only
     setup_dga_modes();
   }
#endif
#ifdef GFX_XWINDOWS_FULLSCREEN
   setup_xfs_modes();
#endif
#ifndef RAINE_DEBUG
   /*----[New Users Message (no config file)]--------------*/

   if(!(raine_cfg.run_count)){
      allegro_message(
	"This seems to be the first time you have run " EMUNAME " " VERSION ".\n"
	"Please read the docs before running... also read raine.cfg.\n"
	"If you have any problems, make sure you read RaineFAQ.txt\n"
	"before sending any email. Latest versions of Raine and the\n"
	"FAQ available at: http://www.rainemu.com/\n"
	"\n"
	"ISSUES:\n"
#ifdef RAINE_WIN32
	"SOUND : Use AllegMix setting !!!\n"
#endif
	"SOUND : The sound core is rather slow now. If you have problems\n"
	"with speed, then disable sound (choose 'Silence')\n"
	"CPS1 : some rare games have priorities problem, and some qsound\n"
	"games do not work at all... for now.\n"
	"\n"
      );
#ifdef RAINE_UNIX
      if (!x_display) {
#endif
#ifndef RAINE_WIN32
	// Win32 waits for a click with allegro_message...
	printf( "[Press a key to continue] [Press ESC to exit]\n"
		"\n");
	CLI_WaitKeyPress();
#endif
#ifdef RAINE_UNIX
      }
#endif
   }
   raine_cfg.run_count++;

   /*----[New Release Message (config version is old)]--------*/
/* Removed by katharsis as Raine ain't dead =)
   if(raine_cfg.version_no != 28){
      raine_cfg.version_no = 28;
      printf(
	"(This will only appear once only)\n"
	"\n"
	"Thanks for trying " EMUNAME " " VERSION ".\n"
	"\n"
	"ISSUES:\n"
	"\n"
	"� This is an alpha release - I expect there are bugs and the\n"
	"  docs are a bit out of date. This is due to lack of time, a\n"
	"  problem that will effect raine for these last few days of\n"
	"  it's life.\n"
	"\n"
	"[Press a key to continue] [Press ESC to exit]\n"
	"\n"
      );
      CLI_WaitKeyPress();
   }
*/
#endif

   /*----[Joystick Initialisation]----------------*/

   if(JoystickType != JOY_TYPE_NONE){
      if(install_joystick(JoystickType)){
	allegro_message("� Error initialising joystick:\n  %s\n\n",allegro_error);
	JoystickType=JOY_TYPE_NONE;
      }
      if(!(num_joysticks)){
	 allegro_message("� No Joysticks Detected\n");
	 JoystickType=JOY_TYPE_NONE;
      }
      else{
	 for(i = 0; i < num_joysticks; i++){
	    if(joy[i].flags & JOYFLAG_CALIBRATE){
	       allegro_message("� Joystick %d needs calibrating in the gui\n", i);
	       //JoystickType=0;
	    }
	 }
      }
   }

   /*----[Sound Initialisation]----------------*/

   RaineSoundCardTotal=8;

   audio_sample_rate = SampleRate;			// hiro-shi (copy SampleRate. audio_sample_rate has been changed with saInitSoundCard.)
   saInitSoundCard( RaineSoundCard, StreamMode, audio_sample_rate );	// hiro-shi (get audio info)
   ProfileSound = PRO_SOUND;
   //saDestroySound(1);

   /*------------------------------------------*/

   s68000init();

   StartGUI();

   sprintf(str,"%sconfig" SLASH "%s", dir_cfg.exe_path, dir_cfg.config_file);
#ifdef RAINE_UNIX
   save_file(str, RaineData[ConfigUnix].dat, RaineData[ConfigUnix].size);
#else
#ifdef RAINE_WIN32
   save_file(str, RaineData[ConfigWin].dat, RaineData[ConfigWin].size);
#else
   save_file(str, RaineData[ConfigDos].dat, RaineData[ConfigDos].size);
#endif
#endif
   save_main_config();

   raine_push_config_state();

   sprintf(str,"%sconfig" SLASH "%s", dir_cfg.exe_path, dir_cfg.config_file);
   raine_set_config_file(str);

   #ifdef RAINE_DEBUG
   if(debug_mode) raine_set_config_int("General","debug_mode",debug_mode);
   close_debug();
   #endif

   if(raine_cfg.wibble)
   raine_set_config_int(	"General",      "wibble",               raine_cfg.wibble);

   raine_set_config_int(	"General",      "run_count",            raine_cfg.run_count);
   raine_set_config_int(	"General",      "version",              raine_cfg.version_no);
   raine_set_config_int(	"General",      "LimitSpeed",           display_cfg.limit_speed);
   raine_set_config_int(	"General",      "frame_skip",           display_cfg.frame_skip);
   raine_set_config_int(	"General",      "ShowFPS",              raine_cfg.show_fps_mode);
   raine_set_config_int(	"General",      "UseRdtsc",             use_rdtsc);
   raine_set_config_int(	"General",      "UseLEDS",              use_leds);

   raine_set_config_int(	"General",      "save_game_screen_settings",    raine_cfg.save_game_screen_settings);
   raine_set_config_int("General","auto_save",  raine_cfg.auto_save);

   // DISPLAY

   if(display_cfg.scanlines == 2) display_cfg.screen_y <<= 1;

   raine_set_config_id (	"Display",      "screen_type",          display_cfg.screen_type);
   raine_set_config_int(	"Display",      "screen_x",             display_cfg.screen_x);
   raine_set_config_int(	"Display",      "screen_y",             display_cfg.screen_y);
   raine_set_config_int(	"Display",      "scanlines",            display_cfg.scanlines);
   raine_set_config_int(	"Display",      "vsync",                display_cfg.vsync);
   raine_set_config_int(	"Display",      "triple_buffer",        display_cfg.triple_buffer);
   raine_set_config_int(	"Display",      "eagle_fx",             display_cfg.eagle_fx);
   raine_set_config_int(	"Display",      "pixel_double",         display_cfg.pixel_double);
   raine_set_config_int(	"Display",      "fast_set_pal",         display_cfg.fast_set_pal);
   raine_set_config_int(	"Display",      "rotate",               display_cfg.user_rotate);
   raine_set_config_int(	"Display",      "flip",                 display_cfg.user_flip);
   raine_set_config_int(	"Display",      "no_rotate",            display_cfg.no_rotate);
   raine_set_config_int(	"Display",      "no_flip",              display_cfg.no_flip);
   raine_set_config_int(	"Display",      "bpp",                  display_cfg.bpp);

   if(display_cfg.scanlines == 2) display_cfg.screen_y >>= 1;

#ifdef RAINE_DOS
   raine_set_config_int(	"Display",      "arcade_h_timing",      display_cfg.arcade_h_timing);
   raine_set_config_int(	"Display",      "arcade_v_timing",      display_cfg.arcade_v_timing);
   raine_set_config_int(	"Display",      "arcade_center_x",      display_cfg.arcade_center_x);
   raine_set_config_int(	"Display",      "arcade_center_y",      display_cfg.arcade_center_y);
#endif

   // RAINE GUI

   raine_set_config_hex(	"GUI",          "gui_col_text_1",       rgui_cfg.gui_col_text_1);
   raine_set_config_hex(	"GUI",          "gui_col_text_2",       rgui_cfg.gui_col_text_2);
   raine_set_config_hex(	"GUI",          "gui_col_black",        rgui_cfg.gui_col_black);
   raine_set_config_hex(	"GUI",          "gui_col_select",       rgui_cfg.gui_col_select);
   raine_set_config_hex(	"GUI",          "gui_box_col_high_2",   rgui_cfg.gui_box_col_high_2);
   raine_set_config_hex(	"GUI",          "gui_box_col_high_1",   rgui_cfg.gui_box_col_high_1);
   raine_set_config_hex(	"GUI",          "gui_box_col_middle",   rgui_cfg.gui_box_col_middle);
   raine_set_config_hex(	"GUI",          "gui_box_col_low_1",    rgui_cfg.gui_box_col_low_1);
   raine_set_config_hex(	"GUI",          "gui_box_col_low_2",    rgui_cfg.gui_box_col_low_2);

   raine_set_config_int(	"GUI",          "game_list_mode",       rgui_cfg.game_list_mode);

   if(rgui_cfg.bg_image[0])
   raine_set_config_string(	"GUI",          "bg_image",             rgui_cfg.bg_image);

   if(rgui_cfg.font_datafile[0])
   raine_set_config_string(	"GUI",          "font_datafile",        rgui_cfg.font_datafile);

   // DIRECTORIES

#ifndef RAINE_UNIX
   raine_set_config_int(	"Directories",  "long_file_names",      dir_cfg.long_file_names);
#endif

   raine_set_config_string(	"Directories",  "ScreenShots",          dir_cfg.screen_dir);
   raine_set_config_string(	"Directories",  "rom_dir_0",            dir_cfg.rom_dir[0]);
   raine_set_config_string(	"Directories",  "rom_dir_1",            dir_cfg.rom_dir[1]);
   raine_set_config_string(	"Directories",  "rom_dir_2",            dir_cfg.rom_dir[2]);
   raine_set_config_string(	"Directories",  "rom_dir_3",            dir_cfg.rom_dir[3]);
   raine_set_config_string(	"Directories",  "cheat_file",           dir_cfg.cheat_file);
   raine_set_config_string(	"Directories",  "language_file",        dir_cfg.language_file);

   // SOUND
#ifdef NEW_SND
   raine_set_config_int(	"Sound",        "UpdateMode",           StreamMode );
#endif
   raine_set_config_id( 	"Sound",        "sound_card",           sound_cfg.sound_card);
   raine_set_config_int(	"Sound",        "sample_rate",          sound_cfg.sample_rate);

   raine_set_config_int(	"Sound",        "SoundCard",            RaineSoundCard);
   //raine_set_config_int(	"Sound",        "SampleRate",           SampleRate);

   raine_set_config_int(	"Sound",        "BufferMaxA",           STREAM_BUFFER_MAXA);
   raine_set_config_int(	"Sound",        "ModeBFrameSize",       MODEB_FRAME_SIZE);
   raine_set_config_int(	"Sound",        "ModeBUpdateCount",     MODEB_UPDATE_COUNT);
   raine_set_config_int(	"Sound",        "ModeBErrorWaitMax",    MODEB_ERROR_MAX);

   raine_set_config_int(	"Sound",        "DariusStereo",         DariusStereo);

   // CONTROL

   raine_set_config_id( 	"Control",      "JoystickType",         JoystickType);

   // KEYBOARD DEFAULT

   save_default_keys("default_game_key_config");

   // JOYSTICK DEFAULT

   save_default_joys("default_game_joy_config");

   // KEYBOARD EMULATOR

   save_emulator_keys("emulator_key_config");

   // JOYSTICK EMULATOR

   save_emulator_joys("emulator_joy_config");

   raine_pop_config_state();
   raine_config_cleanup();

   sprintf(str,"%sconfig" SLASH "%s",dir_cfg.exe_path, dir_cfg.config_file);

   // JOYSTICK ALLEGRO

   save_joystick_data(str);

   unload_datafile(RaineData);

   return 0;
}

END_OF_MAIN();
