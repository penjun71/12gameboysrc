/******************************************************************************/
/*                                                                            */
/*                  RAINE GUI: Based on allegro GUI routines                  */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "raine.h"
#include "emumain.h"
#include "gui.h"
#include "gui2.h"
#include "default.h"
#include "dsw.h"
#include "ingame.h"
#include "arpro.h"
#include "control.h"
#include "sasound.h"
#include "config.h"
#include "games.h"
#include "tilemod.h"
#include "palette.h"
#include "hiscore.h"
#include "loadroms.h"
#include "newmem.h"
#include "dlg_sound.h"
#include "debug.h"
#include "savegame.h"

#ifndef RAINE_WIN32
#include "arcmon.h"
#endif

PALETTE gui_pal;

static   BITMAP *snapshot=NULL;
static int snapshot_cols;

#ifdef RAINE_UNIX
#ifndef ALLEGRO_LINUX 
// These are not defined by freebsd, but they could...
static inline unsigned char inportb(unsigned short port)
{
   unsigned char value;
   __asm__ volatile ("inb %1, %0" : "=a" (value) : "d" (port));
   return value;
}

static inline void outportb(unsigned short port, unsigned char value)
{
   __asm__ volatile ("outb %0, %1" : : "a" (value), "d" (port));
}

static inline void outportw(unsigned short port, unsigned short value)
{
   __asm__ volatile ("outw %0, %1" : : "a" (value), "d" (port));
}

#endif

#define outp(a,b)	outportb(a,b)
#define inp(a)	        inportb(a)

/* #define strcasecmp	_stricmp */

#endif

#define enable()        asm(" sti")
#define disable()       asm(" cli")

#if defined( RAINE_WIN32 )

#define strcasecmp stricmp

#endif

// Size of the game list box
#define GLIST_SIZE 12

extern int bestw, besth,bestbpp; // rgui.c to switch mode
int screen_valid;	// 1 if screen contains a valid image

int wants_switch_res = 0;
static int WantScreen;
static int WantQuit;
static int WantPlay;

#if 0
#define MAX_NAME_BUFFER 60
#define BOARD_POS 32 // Position of the board name...
static char name_buffer[MAX_NAME_BUFFER];
#endif

gamestr game_select_string1;
gamestr game_company,game_year,game_sound,game_strtype;
gamestr pad_buf;

int allocate_pens(int pens);
void free_pens(int pens);
void free_all_pens(void);

void free_game_avail_list(void);

/*

raine_centre_dialog(): centre a dialog onscreen

*/

static int scale = 8;

void raine_centre_dialog(DIALOG *dialog)
{
   int min_x, min_y;
   int max_x, max_y;
   int dx, dy;
   int i, new_scale;

   /*

   place window at (0,0)

   */

   dx = dialog[0].x;
   dy = dialog[0].y;

   for(i=0; dialog[i].proc; i++){
  
      dialog[i].x -= dx;
      dialog[i].y -= dy;

   }

   /*

   scale window

   */

   new_scale = text_height(gui_main_font);

   for(i=0; dialog[i].proc; i++){
  
      dialog[i].x = (dialog[i].x * new_scale) / scale;
      dialog[i].y = (dialog[i].y * new_scale) / scale;
      dialog[i].w = (dialog[i].w * new_scale) / scale;
      dialog[i].h = (dialog[i].h * new_scale) / scale;

   }

   /*

   centre window

   */

   min_x = dialog[0].x;
   min_y = dialog[0].y;
   max_x = dialog[0].x + dialog[0].w;
   max_y = dialog[0].y + dialog[0].h;

   dx = (SCREEN_W - (max_x - min_x)) / 2 - min_x;
   dy = (SCREEN_H - (max_y - min_y)) / 2 - min_y;

   for(i=0; dialog[i].proc; i++){

      dialog[i].x += dx;
      dialog[i].y += dy;

   }
}

static DATAFILE *font_data;

static void setup_font() 
{
  if(rgui_cfg.font_datafile[0])
     font_data = load_datafile(rgui_cfg.font_datafile);
   if (display_cfg.screen_x >= 640 && display_cfg.screen_y >= 480)
     font_data = load_datafile("bigfont.dat");
   else if(!rgui_cfg.font_datafile[0]) {
     unload_datafile(font_data);
     font_data = NULL;
   }
   
   if(font_data){
     gui_main_font     = font_data[2].dat;
     gui_fixed_font    = font_data[1].dat;
     ingame_font       = font_data[0].dat;
     font              = gui_main_font;
   } else {
     gui_main_font     = RaineData[font_gui_main].dat;
     gui_fixed_font    = RaineData[font_gui_fixed].dat;
     ingame_font       = RaineData[font_8x8_ingame].dat;
     font              = gui_main_font;
   }
}

/*

centre_all_dialogs(): centre all raine dialogs

*/

static void centre_all_dialogs(void)
{
   UINT32 i;
   
   setup_font();
   
   i = 0;   
   
   while(raine_dialog_list[i])

      raine_centre_dialog( raine_dialog_list[i++] );

   scale = text_height(gui_main_font);

   MakeGUIBack();
}

// Unselect_Button():
// Force a Button object to redraw itself unselected+unfocused
// Otherwise, they don't get redrawn till after the button's action
// (which looks ugly...).

void Unselect_Button(DIALOG *d)
{
   d->flags &= ~(D_SELECTED|D_GOTFOCUS);
     scare_mouse();
   x_raine_button_proc(MSG_DRAW,d,0);
     unscare_mouse();
   //dialog_oxygen();
}


UINT32 darken(UINT32 src)
{
   UINT32 r,g,b;

   r = (src >> 16) & 0xFF;
   g = (src >>  8) & 0xFF;
   b = (src >>  0) & 0xFF;

   r = (r * 0x100) / 0x148;
   g = (g * 0x100) / 0x148;
   b = (b * 0x100) / 0x148;

   return (r<<16) | (g<<8) | (b<<0);
}

int CGUI_COL_TEXT_1,CGUI_COL_TEXT_2,CGUI_COL_BLACK,
  CGUI_BOX_COL_HIGH_2,CGUI_BOX_COL_HIGH_1,CGUI_BOX_COL_MIDDLE,
  CGUI_BOX_COL_LOW_1,CGUI_BOX_COL_LOW_2,CGUI_COL_SELECT;

void set_gui_palette(void)
{
   gui_pal[GUI_COL_TEXT_1].r=(rgui_cfg.gui_col_text_1>>18)&0x3F;
   gui_pal[GUI_COL_TEXT_1].g=(rgui_cfg.gui_col_text_1>>10)&0x3F;
   gui_pal[GUI_COL_TEXT_1].b=(rgui_cfg.gui_col_text_1>>2)&0x3F;
   SET_PAL(GUI_COL_TEXT_1);

   gui_pal[GUI_COL_TEXT_2].r=(rgui_cfg.gui_col_text_2>>18)&0x3F;
   gui_pal[GUI_COL_TEXT_2].g=(rgui_cfg.gui_col_text_2>>10)&0x3F;
   gui_pal[GUI_COL_TEXT_2].b=(rgui_cfg.gui_col_text_2>>2)&0x3F;
   SET_PAL(GUI_COL_TEXT_2);

   gui_pal[GUI_COL_BLACK].r=(rgui_cfg.gui_col_black>>18)&0x3F;
   gui_pal[GUI_COL_BLACK].g=(rgui_cfg.gui_col_black>>10)&0x3F;
   gui_pal[GUI_COL_BLACK].b=(rgui_cfg.gui_col_black>>2)&0x3F;
   SET_PAL(GUI_COL_BLACK);

   gui_pal[GUI_COL_SELECT].r=(rgui_cfg.gui_col_select>>18)&0x3F;
   gui_pal[GUI_COL_SELECT].g=(rgui_cfg.gui_col_select>>10)&0x3F;
   gui_pal[GUI_COL_SELECT].b=(rgui_cfg.gui_col_select>>2)&0x3F;
   SET_PAL(GUI_COL_SELECT);

   gui_pal[GUI_BOX_COL_HIGH_2].r=(rgui_cfg.gui_box_col_high_2>>18)&0x3F;
   gui_pal[GUI_BOX_COL_HIGH_2].g=(rgui_cfg.gui_box_col_high_2>>10)&0x3F;
   gui_pal[GUI_BOX_COL_HIGH_2].b=(rgui_cfg.gui_box_col_high_2>>2)&0x3F;
   SET_PAL(GUI_BOX_COL_HIGH_2);

   gui_pal[GUI_BOX_COL_HIGH_1].r=(rgui_cfg.gui_box_col_high_1>>18)&0x3F;
   gui_pal[GUI_BOX_COL_HIGH_1].g=(rgui_cfg.gui_box_col_high_1>>10)&0x3F;
   gui_pal[GUI_BOX_COL_HIGH_1].b=(rgui_cfg.gui_box_col_high_1>>2)&0x3F;
   SET_PAL(GUI_BOX_COL_HIGH_1);

   gui_pal[GUI_BOX_COL_MIDDLE].r=(rgui_cfg.gui_box_col_middle>>18)&0x3F;
   gui_pal[GUI_BOX_COL_MIDDLE].g=(rgui_cfg.gui_box_col_middle>>10)&0x3F;
   gui_pal[GUI_BOX_COL_MIDDLE].b=(rgui_cfg.gui_box_col_middle>>2)&0x3F;
   SET_PAL(GUI_BOX_COL_MIDDLE);

   gui_pal[GUI_BOX_COL_LOW_1].r=(rgui_cfg.gui_box_col_low_1>>18)&0x3F;
   gui_pal[GUI_BOX_COL_LOW_1].g=(rgui_cfg.gui_box_col_low_1>>10)&0x3F;
   gui_pal[GUI_BOX_COL_LOW_1].b=(rgui_cfg.gui_box_col_low_1>>2)&0x3F;
   SET_PAL(GUI_BOX_COL_LOW_1);

   gui_pal[GUI_BOX_COL_LOW_2].r=(rgui_cfg.gui_box_col_low_2>>18)&0x3F;
   gui_pal[GUI_BOX_COL_LOW_2].g=(rgui_cfg.gui_box_col_low_2>>10)&0x3F;
   gui_pal[GUI_BOX_COL_LOW_2].b=(rgui_cfg.gui_box_col_low_2>>2)&0x3F;
   SET_PAL(GUI_BOX_COL_LOW_2);

   gui_pal[DGUI_COL_TEXT_1].r=(darken(rgui_cfg.gui_col_text_1)>>18)&0x3F;
   gui_pal[DGUI_COL_TEXT_1].g=(darken(rgui_cfg.gui_col_text_1)>>10)&0x3F;
   gui_pal[DGUI_COL_TEXT_1].b=(darken(rgui_cfg.gui_col_text_1)>>2)&0x3F;

   gui_pal[DGUI_COL_TEXT_2].r=(darken(rgui_cfg.gui_col_text_2)>>18)&0x3F;
   gui_pal[DGUI_COL_TEXT_2].g=(darken(rgui_cfg.gui_col_text_2)>>10)&0x3F;
   gui_pal[DGUI_COL_TEXT_2].b=(darken(rgui_cfg.gui_col_text_2)>>2)&0x3F;

   gui_pal[DGUI_BOX_COL_HIGH_2].r=(darken(rgui_cfg.gui_box_col_high_2)>>18)&0x3F;
   gui_pal[DGUI_BOX_COL_HIGH_2].g=(darken(rgui_cfg.gui_box_col_high_2)>>10)&0x3F;
   gui_pal[DGUI_BOX_COL_HIGH_2].b=(darken(rgui_cfg.gui_box_col_high_2)>>2)&0x3F;

   gui_pal[DGUI_BOX_COL_HIGH_1].r=(darken(rgui_cfg.gui_box_col_high_1)>>18)&0x3F;
   gui_pal[DGUI_BOX_COL_HIGH_1].g=(darken(rgui_cfg.gui_box_col_high_1)>>10)&0x3F;
   gui_pal[DGUI_BOX_COL_HIGH_1].b=(darken(rgui_cfg.gui_box_col_high_1)>>2)&0x3F;

   gui_pal[DGUI_BOX_COL_MIDDLE].r=(darken(rgui_cfg.gui_box_col_middle)>>18)&0x3F;
   gui_pal[DGUI_BOX_COL_MIDDLE].g=(darken(rgui_cfg.gui_box_col_middle)>>10)&0x3F;
   gui_pal[DGUI_BOX_COL_MIDDLE].b=(darken(rgui_cfg.gui_box_col_middle)>>2)&0x3F;

   gui_pal[DGUI_BOX_COL_LOW_1].r=(darken(rgui_cfg.gui_box_col_low_1)>>18)&0x3F;
   gui_pal[DGUI_BOX_COL_LOW_1].g=(darken(rgui_cfg.gui_box_col_low_1)>>10)&0x3F;
   gui_pal[DGUI_BOX_COL_LOW_1].b=(darken(rgui_cfg.gui_box_col_low_1)>>2)&0x3F;

   gui_pal[DGUI_BOX_COL_LOW_2].r=(darken(rgui_cfg.gui_box_col_low_2)>>18)&0x3F;
   gui_pal[DGUI_BOX_COL_LOW_2].g=(darken(rgui_cfg.gui_box_col_low_2)>>10)&0x3F;
   gui_pal[DGUI_BOX_COL_LOW_2].b=(darken(rgui_cfg.gui_box_col_low_2)>>2)&0x3F;
}

// FadeGUI():
// Remaps screen image of gui to darker colours. Used to 'fade out'
// Inactive gui areas

void FadeGUI(void)
{
   BITMAP *fade_backdrop;
   UINT8 *BIT;
   UINT32 ta,tb;
   UINT8 convert[256];

   if (display_cfg.bpp == 8) {
     
     for(ta=0;ta<256;ta++){
       convert[ta]=ta;
     }
     convert[GUI_COL_TEXT_1]      = DGUI_COL_TEXT_1;
     convert[GUI_COL_TEXT_2]      = DGUI_COL_TEXT_2;
     convert[GUI_COL_BLACK]       = GUI_COL_BLACK;
     convert[GUI_COL_SELECT]      = DGUI_BOX_COL_MIDDLE;
     convert[GUI_BOX_COL_HIGH_2]  = DGUI_BOX_COL_HIGH_2;
     convert[GUI_BOX_COL_HIGH_1]  = DGUI_BOX_COL_HIGH_1;
     convert[GUI_BOX_COL_MIDDLE]  = DGUI_BOX_COL_MIDDLE;
     convert[GUI_BOX_COL_LOW_1]   = DGUI_BOX_COL_LOW_1;
     convert[GUI_BOX_COL_LOW_2]   = DGUI_BOX_COL_LOW_2;
 
     // This bitmap must be created in 8bpp because of the palette effects
     fade_backdrop = create_bitmap_ex(8, display_cfg.screen_x,display_cfg.screen_y);

       scare_mouse();
     blit(screen,fade_backdrop,0,0,0,0,display_cfg.screen_x,display_cfg.screen_y);

     for(tb=0;tb<(UINT32)display_cfg.screen_y;tb++){
       BIT=fade_backdrop->line[tb];
       for(ta=0;ta<(UINT32)display_cfg.screen_x;ta++){
         BIT[ta]=convert[BIT[ta]];
       }
     }

     blit(fade_backdrop,screen,0,0,0,0,display_cfg.screen_x,display_cfg.screen_y);
       unscare_mouse();

       destroy_bitmap(fade_backdrop);
   }
   
   //dialog_oxygen();
}

#ifndef RAINE_WIN32

static DEF_INLINE void WaitVBlank(void)
{
  while(inportb(0x3DA)&0x08);
  while(!(inportb(0x3DA)&0x08));
}

static int Stretch256x240(int SType)
{
   static UINT8 fwnesdata[] = {
   0x4F,0x3F,0x40,0x92,0x46,0x10,0x0D,0x3E,
   0x00,0x41,0x00,0x00,0x00,0x00,0x00,0x00,
   0xEA,0xAC,0xDF,0x20,0x00,0xE7,0x06,0xE3
   };

   int ta,ret;

   if(display_cfg.triple_buffer)
      ret = set_gfx_mode(SType,256,240,256,240*3);
   else
      ret = set_gfx_mode(SType,256,240,256,240);

   if(!ret){

   disable();
   WaitVBlank();

   //outportw(0x3C4,0x100);
   outportb(0x3C2,0xE3);
   //outportw(0x3C4,0x300);

   outportw(0x3D4,((fwnesdata[0x11]&0x7F)<<8)+0x11);
   for(ta=0;ta<24;ta++){
      outportw(0x3D4,(fwnesdata[ta]<<8)+ta);
   }
   //inportb(0x3DA);
   //outportb(0x3c0,0x31);
   //outportb(0x3c0,0x3f);
   inportb(0x3DA);
   //outportb(0x3C0,0x11  | 0x20);
   //outportb(0x3C0,0x40);
   enable();

   }
   return ret;
}

// SetDarkForceMode():
// Set a normal vesa mode (via allegro), then divide
// the number of screen lines...

static int SetDarkForceMode(int SType, int SX, int SY, int NewSY)
{
   int ret,lines;
   UINT8 scanline;

   if(display_cfg.triple_buffer)
      ret = set_gfx_mode(SType,SX,SY,SX,NewSY*3);
   else
      ret = set_gfx_mode(SType,SX,SY,SX,SY);

   if(!ret){
      lines=SY/NewSY;
      if(lines<1)lines=1;
      if(lines>4)lines=4;
      outp(0x3D4,0x09);		// CRTC Register index 9
      scanline=inp(0x3D5);	// get scanline height
      scanline&=0x60;		// clear any existing bits
      scanline|=(lines-1);	// set multiply bits
      outp(0x3D5,scanline); 	// set new line height      
   }

   return ret;
}

// Set320x256():
// Set a normal mode-x mode (via allegro), then twiddle
// the vga registers and the vtable

static int Set320x256(int SType)
{
   int n,ret;
   UINT16 port;
   UINT8 *q,reg,data;
   static unsigned char gfx_320x256_data[] = {
      194,3,0,227,212,3,0,95,212,3,1,79,212,3,2,80,212,3,3,130,212,3,4,84,212,3,5,154,212,3,6,35,212,
      3,7,178,212,3,8,0,212,3,9,97,212,3,16,10,212,3,17,172,212,3,18,255,212,3,19,40,212,3,20,0,212,3,
      21,7,212,3,22,26,212,3,23,227,196,3,1,1,196,3,4,6,206,3,5,64,206,3,6,5,192,3,16,65,192,3,19,
      0,};

   if(display_cfg.triple_buffer)
      ret = set_gfx_mode(SType,320,480,320,256*3);
   else
      ret = set_gfx_mode(SType,320,480,320,256);

   if(ret)
      return ret;

   disable();
   WaitVBlank();

   /* ready the vga registers */
   outp(0x3d4,0x11);
   data = inp(0x3d5) & 0x7f;
   outp(0x3d4,0x11);
   outp(0x3d5,data);

   n = 25;
   q = &gfx_320x256_data[0];
   do {
      port = *q++;
      port |= ((*q++) << 8);
      reg = *q++;
      data = *q++;
      switch(port)
      {
      case 0x3c0:
         inp(0x3da);         /* reset read/write flip-flop */
         outp(0x3c0, reg | 0x20);
                                 /* ensure VGA output is enabled */
         outp(0x3c0, data);
         break;

      case 0x3c2:
      case 0x3c3:
         outp(port, data);   /* directly to the port */
			break;

      case 0x3c4:
      case 0x3ce:
      case 0x3d4:
      default:                   /* This is the default method: */
         outp(port, reg);    /* index to port               */
         outp(port+1, data); /* value to port+1             */
			break;
      }
   } while(--n);

   enable();
   screen->h = 256;
   set_clip(screen, 0,0,319,255);
   return ret;
}

static int set_arcade_mode(int SType, int SX, int SY)
{
   int ret=1;
#ifdef GFX_MODEX
// Actually GFX_MODEX is defined in dos and in linux.
// Apparently it is not defined in freebsd...

   // Currently only accept 320x240

   if((SX!=320)||(SY!=240))
      return -1;

   // Set Mode X as the base mode

   if(display_cfg.triple_buffer)
      ret = set_gfx_mode(GFX_MODEX,SX,SY,SX,SY*3);
   else
      ret = set_gfx_mode(GFX_MODEX,SX,SY,SX,SY);

   // Apply hacks for arcade monitor rates

   if(!ret){
      arcade_monitor_mode(
         display_cfg.arcade_h_timing,
         display_cfg.arcade_v_timing,
         display_cfg.arcade_center_x,
         display_cfg.arcade_center_y
      );
   }

#endif
   return ret;

}
#endif

static int raine_set_gfx_mode(int SType,int SX,int SY)
{
  int ret;
#ifdef RAINE_DEBUG
   print_debug("raine_gfx_mode bpp %d\n",display_cfg.bpp);
#endif
   set_color_depth(display_cfg.bpp);

#ifndef RAINE_WIN32
#ifdef RAINE_UNIX
   if (!x_display) {
#endif
     if((SX==256)&&(SY==240)) return Stretch256x240(SType);
     if((SX==320)&&(SY==256)) return Set320x256(SType);

     if((SX==400)&&(SY==150)) return SetDarkForceMode(SType,SX,300,SY);
     if((SX==640)&&(SY==100)) return SetDarkForceMode(SType,SX,400,SY);
     if((SX==640)&&(SY==150)) return SetDarkForceMode(SType,SX,400,SY);
     if((SX==640)&&(SY==200)) return SetDarkForceMode(SType,SX,400,SY);
     if((SX==640)&&(SY==120)) return SetDarkForceMode(SType,SX,480,SY);
     if((SX==640)&&(SY==160)) return SetDarkForceMode(SType,SX,480,SY);
     if((SX==640)&&(SY==240)) return SetDarkForceMode(SType,SX,480,SY);
     if((SX==800)&&(SY==150)) return SetDarkForceMode(SType,SX,600,SY);
     if((SX==800)&&(SY==200)) return SetDarkForceMode(SType,SX,600,SY);
     if((SX==800)&&(SY==300)) return SetDarkForceMode(SType,SX,600,SY);
     if((SX==1024)&&(SY==192)) return SetDarkForceMode(SType,SX,768,SY);
     if((SX==1024)&&(SY==256)) return SetDarkForceMode(SType,SX,768,SY);
     if((SX==1024)&&(SY==384)) return SetDarkForceMode(SType,SX,768,SY);
     
#ifdef GFX_DRIVER_MODEX
     if(SType==GFX_ARCMON) return set_arcade_mode(SType,SX,SY);
#endif     
#ifdef RAINE_UNIX
   }
#endif
#endif

   if(display_cfg.triple_buffer)
      ret= set_gfx_mode(SType,SX,SY,SX,SY*3);
   else
      ret= set_gfx_mode(SType,SX,SY,SX,SY);
#ifdef RAINE_DEBUG
   print_debug("raine_gfx_mode result %d\n",ret);
#endif
   return ret;
}
  
static int new_set_gfx_mode(int SType,int SX,int SY,int SLines)
{
	int ta,tb,bpp;

	screen_valid=0;
	
	if(SLines==0){				// Set a Normal Mode
		ta=raine_set_gfx_mode(SType,SX,SY);
		if(!ta){
			screen->w=SX;
			screen->h=SY;
			screen->cr=screen->w;
			screen->cb=screen->h;
			gfx_driver->w=screen->w;
			gfx_driver->h=screen->h;
			centre_all_dialogs();
		}
		bpp = desktop_color_depth();
		return ta;
	}
	else
	{
#ifdef RAINE_UNIX
		// Disable scanlines in xwindows (windowed mode)
		// They are useless in this mode, and they don't work...
		if (display_cfg.screen_type != GFX_XWINDOWS
#ifdef GFX_XWINDOWS_FULLSCREEN
		    && display_cfg.screen_type != GFX_XWINDOWS_FULLSCREEN
#endif
		    )
#endif
		{  // Set a Mode with Scanlines Hack
			ta=raine_set_gfx_mode(SType,SX,SY<<1);
			if(!ta){
				screen->w=SX;
				screen->h=SY;
				screen->cr=screen->w;
				screen->cb=screen->h;
				gfx_driver->w=screen->w;
				gfx_driver->h=screen->h;
				if(display_cfg.triple_buffer){
					for(tb=0;tb<SY*3;tb++){
						screen->line[tb]=screen->line[tb+tb];
					}
				}
				else{
					for(tb=0;tb<SY;tb++){
						screen->line[tb]=screen->line[tb+tb];
					}
				}
				centre_all_dialogs();
			}
			return ta;
		}
	}
	return -1; // Error : can't set mode
}

// for any mapped bitmap including mouse and logos in the dialogs...
BITMAP *make_mapped_bitmap(BITMAP *src, int *start, PALETTE *src_pal, UINT32 cols)
{
   BITMAP *dest;
   UINT8 *line,*line_2;
   int ta,x,y;

   dest = create_bitmap_ex(8, src->w, src->h);
   ta = allocate_pens(cols);

   if(ta>0){

     for(y = 0; y < src->h; y++){
       line   = src->line[y];
       line_2 = dest->line[y];
       for(x = 0; x < src->w; x++){
	   if(line[x])
	     line_2[x] = line[x] + ta;
	   else
	     line_2[x] = GUI_BOX_COL_MIDDLE;
       }
     }

     memcpy(&gui_pal[ta], src_pal, cols*4);
     set_palette_range(gui_pal, ta, ta+cols-1, 0);

   }
   else{
     ta = 0;
   }

   *start = ta;

   return dest;
}

BITMAP *make_mapped_bitmap_2(BITMAP *src, int *start, PALETTE *src_pal, UINT32 cols)
{
   BITMAP *dest;
   UINT8 *line,*line_2;
   int ta,x,y;

   dest = create_bitmap_ex(8, src->w, src->h);	
   ta = allocate_pens(cols);
     
   if(ta>0){
	  
     for(y=0;y<src->h;y++){
       line   = src->line[y];
       line_2 = dest->line[y];
       for(x=0;x<src->w;x++){
	 if(line[x]==0)
	   line_2[x] = 0;
	 else
	   line_2[x] = line[x]+ta;
       }
     }
     memcpy(&gui_pal[ta], src_pal, cols*4);
     set_palette_range(gui_pal, ta, ta+cols-1, 0);
       
   }
   else{
     ta = 0;
   }
      
   *start = ta;

   return dest;
   // Without breaking the palette in 16bpp. But I don't need this now !
}

void destroy_mapped_bitmap(BITMAP *dest, UINT32 cols)
{
   free_pens(cols);
   destroy_bitmap(dest);
}

void ScreenChange(void)
{
    show_mouse(NULL);

   if(display_cfg.scanlines==2)
      display_cfg.screen_y >>= 1;

   if(new_set_gfx_mode(display_cfg.screen_type,display_cfg.screen_x,display_cfg.screen_y,display_cfg.scanlines)){	// Didn't work:
      memcpy(&display_cfg, &prev_display_cfg, sizeof(DISPLAY_CFG));
      new_set_gfx_mode(display_cfg.screen_type,display_cfg.screen_x,display_cfg.screen_y,display_cfg.scanlines);	// Revert to old mode
   }
   else{
     ReClipScreen();
      if (prev_display_cfg.bpp != display_cfg.bpp) {
	if (current_colour_mapper){
	  set_colour_mapper(current_colour_mapper);
	  ResetPalette();
	}
      }
   }

   set_palette_range(gui_pal,0,255,0);
   sprintf(screenname, raine_translate_text("%s %dx%d %dbpp; %s"), raine_translate_text("Screen:"), display_cfg.screen_x, display_cfg.screen_y, display_cfg.bpp, gfx_card_name(display_cfg.screen_type));
     show_mouse(screen);
}

// screen_select_proc():
// Display then process Screen Select dialog

int screen_select_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();

      memcpy(&prev_display_cfg, &display_cfg, sizeof(DISPLAY_CFG));

      ret = raine_gfx_mode_select();

      if(ret){
         WantScreen=1;
         return D_EXIT;
      }
      else{
         memcpy(&display_cfg, &prev_display_cfg, sizeof(DISPLAY_CFG));
         return D_REDRAW;
      }
   }
   return ret;
}

// Data for game list and available/missing games
 
static int *GameAvail = NULL;
static UINT32 GameAvailCount = 0;
static int *GameMissing = NULL;
static UINT32 GameMissingCount = 0;

static int gtype = 0,gcompany=0; // Default values
static int my_game_count;
static struct GAME_MAIN **my_game_list;

static char *game_type[] =
{ "All",
  "Breakout",
  "Shoot'em up",
  "Beat'em up",
  "Puzzle",
  "Platform",
  "Misc",
  "Sport",
  "Adult",
  "Race",
  "Quizz",
};

static int nb_game_type;
static UINT8 ssound, syear, sstatus;

void build_game_avail_list(); // further...
static void update_game_list();

int res_radio_proc(int msg, DIALOG *d, int c){
  wants_switch_res = game_select[GAME_SWITCH_RES+1].flags&D_SELECTED;
  return x_raine_radio_proc(msg, d, c);
}

int sound_radio_proc(int msg, DIALOG *d, int c)
{
	int i;
	int ret=x_raine_radio_proc(msg, d, c);
	
	if((msg==MSG_KEY)||(msg==MSG_CLICK)){
		
		// Get selected List Mode
		// ----------------------
		// I hate these constants, but see no easy way to do it better for now
		
		if((game_select_opts[2].flags)&D_SELECTED) i = 0;
		else if((game_select_opts[3].flags)&D_SELECTED) i = 1;
		else if((game_select_opts[4].flags)&D_SELECTED) i = 2;
		else i = 0;
		
		if (i != ssound) {
			ssound = i;
			update_game_list();
		}
	}
	return ret;
}

int status_radio_proc(int msg, DIALOG *d, int c)
{
	int i;
	int ret=x_raine_radio_proc(msg, d, c);
	
	if((msg==MSG_KEY)||(msg==MSG_CLICK)){
		
		if((game_select_opts[6].flags)&D_SELECTED) i = 0;
		else if((game_select_opts[7].flags)&D_SELECTED) i = 1;
		else if((game_select_opts[8].flags)&D_SELECTED) i = 2;
		else i = 0;
		
		if (i != sstatus) {
			sstatus = i;
			update_game_list();
		}
	}
   return ret;
}

int year_radio_proc(int msg, DIALOG *d, int c)
{
	int i;
	int ret=x_raine_radio_proc(msg, d, c);
	
	if((msg==MSG_KEY)||(msg==MSG_CLICK)){
		
		if((game_select_opts[10].flags)&D_SELECTED) i = 0;
		else if((game_select_opts[11].flags)&D_SELECTED) i = 1;
		else if((game_select_opts[12].flags)&D_SELECTED) i = 2;
		else i = 0;
		
		if (i != syear) {
			syear = i;
			update_game_list();
		}
	}
	return ret;
}

static void build_game_list()
{
  int n;
  struct GAME_MAIN *game;
  nb_game_type = sizeof(game_type)/sizeof(char*);
  
  if (my_game_list) free(my_game_list);
  my_game_count = 0;

  my_game_list = malloc(game_count * sizeof(GAME_MAIN*));
  for (n=0; n<game_count; n++) {
    game = game_list[n];
    if (
#ifndef PRIVATE
	(!(game->flags & GAME_PRIVATE)) &&
#endif	
	(((!sstatus) && !(game->flags & (GAME_NOT_WORKING | GAME_PARTIALLY_WORKING))) ||
	(sstatus==1 && (game->flags & GAME_PARTIALLY_WORKING)) ||
	(sstatus==2 && (game->flags & GAME_NOT_WORKING)))) {

      if (!ssound || (ssound==1 && game->sound_list) || // sound
	  (ssound==2 && !game->sound_list)) { // no sound

	if (!syear || (syear==1 && (game->year >= 1990)) ||
	    (syear==2 && (game->year < 1990))) {
       
	  if (!gtype || (game->flags & (1<<(gtype-1)))) {
	    if (!gcompany || game->company_id==gcompany)
	      my_game_list[my_game_count++] = game_list[n];
	  }
	}
      }
    }
  }
}

void build_game_avail_list(); // further...
void set_game_list_mode(int i);

static void update_game_list()
{
  build_game_avail_list();
  set_game_list_mode(rgui_cfg.game_list_mode);
}

char *game_type_getter(int index, int *list_size)
{
  if (index == -1){ // Nb of indexes
    *list_size = nb_game_type;
  } else if (index == -4) { // clic !
    gtype = *list_size;
    update_game_list();
  } else if (index >= 0 && index < nb_game_type) {
    return game_type[index];
  }
  return NULL;
}

char *game_company_getter(int index, int *list_size)
{
  if (index == -1){ // Nb of indexes
    *list_size = nb_companies;
  } else if (index == -4) { // clic !
    gcompany = *list_size;
    update_game_list();
  } else if (index > 0 && index < nb_companies) {
    return game_company_name(index);
  } else if (index == 0)
    return "Any";
  return NULL;
}

// build_game_avail_list():
// Updates available and missing game lists, called
// before doing game select dialog.


void set_game_list_mode(int i)
{
   int ta, tb;

   switch(rgui_cfg.game_list_mode){
      case 0x00:
         tb = game_select[GAME_LIST].d1;
      break;
      case 0x01:
         tb = GameAvail[game_select[GAME_LIST].d1];
      break;
      case 0x02:
         tb = GameMissing[game_select[GAME_LIST].d1];
      break;
      // When none of the options above match (which should be never)
	  // Default to option 0;
	  default:
         tb = game_select[GAME_LIST].d1;
      break;
   }

   rgui_cfg.game_list_mode = i;

   switch(rgui_cfg.game_list_mode){
      case 0x00:
	if (my_game_count<10)
	  sprintf(game_select[9].dp,"%d/%d Games Listed      ", my_game_count, my_game_count);
	else 
	  sprintf(game_select[9].dp,"%d/%d Games Listed     ", my_game_count, my_game_count);
         game_select[GAME_LIST].d1 = tb;
      break;
      case 0x01:
	if (my_game_count<10)
	  sprintf(game_select[9].dp,"%d/%d Games Listed      ", GameAvailCount, my_game_count);
	else
	  sprintf(game_select[9].dp,"%d/%d Games Listed     ", GameAvailCount, my_game_count);
         game_select[GAME_LIST].d1 = 0;
         for(ta = 0; (UINT32)ta < GameAvailCount; ta++)
            if(GameAvail[ta] == tb)
               game_select[GAME_LIST].d1 = ta;
      break;
      case 0x02:
	if (my_game_count<10)
	  sprintf(game_select[9].dp,"%d/%d Games Listed      ", GameMissingCount, my_game_count);
	else
	  sprintf(game_select[9].dp,"%d/%d Games Listed     ", GameMissingCount, my_game_count);	  
	game_select[GAME_LIST].d1 = 0;
	for(ta = 0; (UINT32)ta < GameMissingCount; ta++)
            if(GameMissing[ta] == tb)
               game_select[GAME_LIST].d1 = ta;
      break;
   }
}

int game_exists(int num)
{
   DIR_INFO *dir_list;
   UINT8 str[256];
   UINT32 ta;

   dir_list = my_game_list[num]->dir_list;

   while( (dir_list->maindir) ){

      if((dir_list->maindir[0] != '#')&&(dir_list->maindir[0] != '$')){

         for(ta = 0; ta < 4; ta ++){

            if(dir_cfg.rom_dir[ta][0]){

               sprintf(str,"%s%s.zip", dir_cfg.rom_dir[ta], dir_list->maindir);
               if((file_exists(str,255,NULL))) return 1;

               sprintf(str,"%s%s", dir_cfg.rom_dir[ta], dir_list->maindir);
               if((file_exists(str,255,NULL))) return 1;

            }

         }

      }

      dir_list++;
   }

   return 0;

}

void build_game_avail_list(void)
{
   int ta,oldcount;

   build_game_list();
   if(GameAvail) free_game_avail_list();
   oldcount = my_game_count;
   if (!my_game_count) my_game_count = 1; // To avoid the segfault with efence
   // when there are no games at all !!!
   
   GameAvail   = (int *) malloc(sizeof(int) * my_game_count);
   GameMissing = (int *) malloc(sizeof(int) * my_game_count);
   my_game_count=oldcount;
   
   GameAvailCount = 0;
   GameMissingCount = 0;

   for(ta=0; ta<my_game_count; ta++){

      if( game_exists(ta) )

         GameAvail[GameAvailCount++] = ta;

      else

         GameMissing[GameMissingCount++] = ta;

   }

   ta = rgui_cfg.game_list_mode;
   rgui_cfg.game_list_mode = 0;
   game_select[GAME_LIST].d1 = raine_cfg.req_game_index;

   set_game_list_mode(ta);
}

void free_game_avail_list(void)
{
   if(GameAvail) free(GameAvail);
   GameAvail = NULL;

   if(GameMissing) free(GameMissing);
   GameMissing = NULL;
}

// listbox_getter():
// Get game names for game select list, three
// different listing modes.

char *get_long_name(GAME_MAIN *game)
{
#if 0
	int len;
#endif
  
  return game->long_name;
  // The rest of this function was to add a "S" for sound at the end...
#if 0
  strncpy(name_buffer,game->long_name,BOARD_POS);
  len = strlen(name_buffer);
  if (len < BOARD_POS) {
    memset(&name_buffer[len],32,BOARD_POS-len);
    name_buffer[BOARD_POS] = 0;
  }
  if (game->sound_list) {
    strncat(name_buffer,"S",MAX_NAME_BUFFER-BOARD_POS);
  }
  name_buffer[MAX_NAME_BUFFER-1] = 0;
  return name_buffer;
#endif
}

static int get_normal_index(int index)
{
  return index;
}

static int get_avail_index(int index)
{
  return GameAvail[index];
}

static int get_missing_index(int index)
{
  return GameMissing[index];
}

static char* pad(char* s,int len)
{
  int len0=strlen(s);
  strcpy(pad_buf,s);
  if (len>len0)
    memset(&pad_buf[len0],0x20,len-len0);
  pad_buf[len] = 0;
  return pad_buf;
}

static char* ipad(int n, int len)
{
  char buff[10];
  sprintf(buff,"%d",n);
  return pad(buff,len);
}

static char* game_type_name(int type,int len)
{
  char buff[50];
  int ta;
  buff[0]=0;
  for (ta=0; ta<=7; ta++)
    if (type & (1 << ta)){
      sprintf(buff+strlen(buff),"%s ",game_type[ta+1]);
      break;
    }
  return pad(buff,len);
}

static void setup_game_bitmap() 
{
  int new_scale,hx,hy,vx,vy;
  game_select[GAME_BITMAP].dp   = snapshot;
  new_scale = text_height(gui_main_font);
  hx = 130 * new_scale / 8;
  hy = 76 * new_scale / 8;
  vx = 70 * new_scale / 8;
  vy = 120 * new_scale / 8;
  if (snapshot->w > snapshot->h) { // horizontal
    game_select[GAME_BITMAP].x = ((game_select[0].x+150)*new_scale)/8;
    game_select[GAME_BITMAP].y = ((game_select[0].y+145)*new_scale)/8+5;
    game_select[GAME_BITMAP].w    = (snapshot->w < hx ? snapshot->w : hx);
    game_select[GAME_BITMAP].h    = (snapshot->h < hy ? snapshot->h : hy);
  } else { // vertical
    game_select[GAME_BITMAP].x = ((game_select[0].x + 235)*new_scale)/8;
    game_select[GAME_BITMAP].y = ((game_select[0].y + 100)*new_scale)/8+5;
    game_select[GAME_BITMAP].w    = (snapshot->w < vx ? snapshot->w : vx);
    game_select[GAME_BITMAP].h    = (snapshot->h < vy ? snapshot->h : vy);
  }
}

void destroy_snapshot(int redraw) 
{
  if (redraw) // stupid allegro...
    rectfill(screen, game_select[GAME_BITMAP].x, game_select[GAME_BITMAP].y, game_select[GAME_BITMAP].x+game_select[GAME_BITMAP].w-1, game_select[GAME_BITMAP].y+game_select[GAME_BITMAP].h-1, CGUI_BOX_COL_MIDDLE);
  destroy_mapped_bitmap(snapshot,snapshot_cols);
  snapshot = NULL;
}

static BITMAP* load_snapshot(char *name) 
{
  char str[256];
  BITMAP *snapshot;
  PALETTE my_palette;
  
  if (text_height(gui_main_font) > 8) 
    sprintf(str,"%ssnapshot" SLASH "big" SLASH "%s.pcx", dir_cfg.exe_path,name);
  else
    sprintf(str,"%ssnapshot" SLASH "%s.pcx", dir_cfg.exe_path,name);

  snapshot = load_pcx(str,my_palette);
  if (!snapshot && text_height(gui_main_font) > 8) {
    sprintf(str,"%ssnapshot" SLASH "%s.pcx", dir_cfg.exe_path,name);
    snapshot = load_pcx(str,my_palette);
  }
  if (snapshot)
    set_palette_range(my_palette,0,239,1);
  return snapshot;
}

static int listbox_active = 0; // to know if we can redraw or not...

char *listbox_getter(int index, int *list_size)
{
   int ta=0,list_length=0;
   char tb[2];
   char tc[2];
   int (*get_index)(int)=get_normal_index;
   DIR_INFO *head;
   UINT8 *dir;
   
   switch(rgui_cfg.game_list_mode){
   case 0x00:			// Mode 0: List all games
     list_length = my_game_count;
     get_index = get_normal_index;
     break;
   case 0x01:			// Mode 1: List available games
     list_length = GameAvailCount;
     get_index = get_avail_index;
     break;
   case 2:
     list_length = GameMissingCount;
     get_index = get_missing_index;
   }
   
   switch(index){
   case -1:			// Return List Size
     *list_size = list_length;
     return NULL;
     break;
   case -2:			// Act upon List Object Selection
     break;
   case -4: 

     if (snapshot)
       destroy_snapshot(1);
     if ((rgui_cfg.game_list_mode==0 && my_game_count>0) ||
	 (rgui_cfg.game_list_mode==1 && GameAvailCount>0) ||
	 (rgui_cfg.game_list_mode==2 && GameMissingCount>0)) {
       ta = get_index(*list_size);
       sprintf(game_select[GAME_DATA].dp,
	       pad(game_company_name(my_game_list[ta]->company_id),20));
       sprintf(game_select[GAME_DATA+1].dp,
	       ipad(my_game_list[ta]->year,20));
       if (my_game_list[ta]->sound_list)
	 sprintf(game_select[GAME_DATA+2].dp,"Yes  ");
       else
	 sprintf(game_select[GAME_DATA+2].dp,"No   ");
       sprintf(game_select[GAME_DATA+3].dp,
	       game_type_name(my_game_list[ta]->flags & 0xff,14));
	 
       snapshot = load_snapshot(my_game_list[ta]->main_name);
       
       snapshot_cols = 240;
       
       if (!snapshot) {
	 head = my_game_list[ta]->dir_list;
	 for (; head; head++) {
	   dir = head[0].maindir;

	   if( dir ){
	     
	     if( IS_ROMOF(dir) ){
	     
	       GAME_MAIN *game_romof;

	       game_romof = find_game(dir+1);
	     
	       snapshot = load_snapshot(game_romof->main_name);
	       
	       if (snapshot) break;
	     }
	   } else
	     break;
	 }
       }
     } else {
       sprintf(game_select[GAME_DATA].dp,
	       pad("---",20));
       sprintf(game_select[GAME_DATA+1].dp,
	       pad("---",20));
       if (my_game_list[ta]->sound_list)
	 sprintf(game_select[GAME_DATA+2].dp,"--- ");
       else
	 sprintf(game_select[GAME_DATA+2].dp,"--- ");
       sprintf(game_select[GAME_DATA+3].dp,
	       pad("---",14));
       snapshot = NULL;       
     }
       
     if (!snapshot) {
       snapshot_cols = 7;
       snapshot = make_mapped_bitmap(RaineData[RaineLogo].dat, &ta, RaineData[GUIPalette].dat, snapshot_cols);
     }
     
     setup_game_bitmap();
     
     if (listbox_active) { 
       // Redraw data fields
       for (ta = 0; ta < 4; ta++)
	 SEND_MESSAGE(&game_select[GAME_DATA+ta], MSG_DRAW, 0);

     // Redraw titles
       for (ta = -4; ta < 0; ta++)
	 SEND_MESSAGE(&game_select[GAME_DATA+ta], MSG_DRAW, 0);
       SEND_MESSAGE(&game_select[GAME_BITMAP],MSG_DRAW,0);
     }
     return NULL;
   case -3:			// Act Keyboard Input
     tb[0] = *list_size & 0xFF;
     tb[1] = 0;
     tc[1] = 0;
     for(ta=0;ta<list_length;ta++){
       tc[0] = my_game_list[get_index(ta)]->long_name[0];
       if(!(strcasecmp(tb,tc))){
	 game_select[GAME_LIST].d1 = ta;
	 if(ta < (game_select[GAME_LIST].d2     )) game_select[GAME_LIST].d2 = ta;
	 if(ta > (game_select[GAME_LIST].d2 + GLIST_SIZE)) game_select[GAME_LIST].d2 = ta - GLIST_SIZE+1;
	 *list_size = D_USED_CHAR;
	 return NULL;
       }
     }
     return NULL;
     break;
   default:
     if((index >= 0)&&(index<list_length))
       return get_long_name(my_game_list[get_index(index)]);
     else
       return NULL;
     break;
   }
   return NULL;
}

// game_radio_proc():
// Radio buttons for game list mode, need to be custom
// to incorporate automatic update on button selection.

int game_radio_proc(int msg, DIALOG *d, int c)
{
   int i;

   int ret=x_raine_radio_proc(msg, d, c);

   if((msg==MSG_KEY)||(msg==MSG_CLICK)){

      // Get selected List Mode
      // ----------------------

      if((game_select[5+0].flags)&D_SELECTED) i = 0;
      else if((game_select[5+1].flags)&D_SELECTED) i = 1;
      else if((game_select[5+2].flags)&D_SELECTED) i = 2;
	  else i = 0;

      set_game_list_mode(i);

      // Refresh the Game List
      // ---------------------

      SEND_MESSAGE(&game_select[GAME_LIST], MSG_END, 0);
      SEND_MESSAGE(&game_select[GAME_LIST], MSG_START, 0);
      
	scare_mouse();
      broadcast_dialog_message(MSG_DRAW, 0);

	unscare_mouse();

      // Wait for release
      // ----------------

	while(gui_mouse_b()){
	  dialog_oxygen();
	}
   }
   return ret;
}

static UINT32 current_game_rom_count;
static UINT32 current_game_rom_load_count;

int load_game_proc(int msg, DIALOG *d, int c)
{
  int oldbpp = 0;
	(void)(d);
	(void)(c);
  switch(msg){
      case MSG_IDLE:

         mouse_on_real_screen();
         dialog_on_real_screen();

         LoadDefault();

         load_error = 0;
         load_debug = malloc(0x10000);

         sprintf(load_debug,"Load Game\n---------\n\n");

	 if (my_game_list) // normal loading
	   current_game = my_game_list[raine_cfg.req_game_index];
	 else // uses -g switch
	   current_game = game_list[raine_cfg.req_game_index];

	 // I have to change the depth BEFORE loading.
	 // Probably because of the set_color_mapper in the loading function

	 if(wants_switch_res) // && switch_res(current_game->video_info))){
	   switch_res(current_game->video_info);
#if 0
	 // For now I disable automatic depth changing, now that almost
	 // every game does support all the depths...
	 // Because some people running raine in a 16bpp display might
	 // prefer running 8bpp games in 16bpp (like me)
	   if (bestbpp) {
	     oldbpp = display_cfg.bpp;
	     display_cfg.bpp = bestbpp;
	   }
#endif	 

         /*

         count roms

         */

         {
            ROM_INFO *rom_list;

            current_game_rom_count = 0;
            current_game_rom_load_count = 0;
            rom_list = current_game->rom_list;

            while(rom_list->name)
            {
               current_game_rom_count ++;
               rom_list ++;
            }

            if(!current_game_rom_count)
               current_game_rom_count ++;

            loading_dialog[2].d1 = current_game_rom_load_count;
            loading_dialog[2].d2 = current_game_rom_count;
	      scare_mouse();
            SEND_MESSAGE(&loading_dialog[2], MSG_DRAW,  0);
	      unscare_mouse();
         }

         load_game_rom_info();
	 
	 if (!(load_error & LOAD_FATAL_ERROR)) {
	   current_game->load_game();

	   // bpp must be changed in the last moment !!!
	   if (oldbpp)
	     display_cfg.bpp = oldbpp;
	   
	   init_inputs();
	   init_dsw();
	   init_romsw();
	   init_sound_list();
	   hs_open();
	 }

         dialog_on_buffer_screen();
         mouse_on_buffer_screen();
         loading_dialog[2].d1 = current_game_rom_count;
         loading_dialog[2].d2 = current_game_rom_count;
	   scare_mouse();
         SEND_MESSAGE(&loading_dialog[2], MSG_DRAW,  0);
	   unscare_mouse();
         dialog_oxygen();
         return D_EXIT;
      break;
      default:
         return D_O_K;
      break;
   }
}

void do_load_game(void)
{
   BITMAP *load_mouse;
   int ta,size;

   /*

   satisfy the request

   */

   raine_cfg.req_load_game = 0;

   /*

   don't reload if the game is already in memory

   */

   if(my_game_list && current_game == my_game_list[raine_cfg.req_game_index])

      return;

   /*

   close down the current game (if there is one)

   */

   if(current_game){

      save_game_config();
      hs_close();

      current_game->clear_game();

      ClearDefault();

      current_game = NULL;

   }

      // Do the load (via a loading window)

     load_mouse = make_mapped_bitmap_2(RaineData[mouse_busy].dat, &ta, RaineData[mouse_busy_pal].dat, 16);
      set_mouse_sprite(load_mouse);

      loading_dialog[2].d1 = 0;
      loading_dialog[2].d2 = 1;

      FadeGUI();
      
      raine_do_dialog(loading_dialog,-1);

      set_mouse_sprite(RaineData[Mouse].dat);
      destroy_mapped_bitmap(load_mouse, 16);
      // Check if the load was successful, if ok, load game config, if bad clear the mess

      switch(load_error&3){
      case LOAD_WARNING:			// WARNING - IT MIGHT RUN OK

         FadeGUI();
	 strcat(load_debug,"\n\nThe game might not run correctly.");
         load_problem[2].dp = load_debug;
         raine_do_dialog(load_problem,-1);

      case 0x00:				// SUCCESS
	size = GetMemoryPoolSize()/1024; // Minimum Kb
	memcpy(&prev_display_cfg, &display_cfg, sizeof(DISPLAY_CFG));

	load_game_config();
	reset_game_hardware();
	
	{
	  char name[40];
	  
	  strncpy(name,current_game->long_name,39);
	  name[39] = 0;
	  
	  sprintf(gamename, "%s %s (%dMb)", raine_translate_text("Game:"), name,size/1024);
	}
	if (current_game_has_hiscores())
	  sprintf(hiscores,"[Hiscores]");
	else
	  *hiscores=0;
	if (CheatCount)
	  sprintf(cheats,"[Cheats]");
	else
	  *cheats = 0;

#if 0
	if (bestbpp > 0 && bestbpp <=32) {
	  WantScreen = 1;
	  display_cfg.bpp = bestbpp;
	  bestbpp = 0;
	}
#endif	
	
	if (bestw && besth) {
	  //display_cfg.screen_type = bestmode;
	  display_cfg.screen_x = bestw;
	  display_cfg.screen_y = besth;
	  display_cfg.scanlines = display_cfg.eagle_fx = 0;
	  WantScreen = 1;
	  bestw = besth = 0;
	}
	  
	if (raine_cfg.save_game_screen_settings)
	  WantScreen=1;
      break;
      case LOAD_FATAL_ERROR:			// FATAL ERROR - REMOVE GAME
      case LOAD_FATAL_ERROR|LOAD_WARNING:

         FadeGUI();
         load_problem[2].dp = load_debug;
         raine_do_dialog(load_problem,-1);

         current_game->clear_game();

         ClearDefault();

         current_game = NULL;

         sprintf(gamename,"%s %s", raine_translate_text("Game:"), raine_translate_text("<No Game Loaded>"));
      break;
      }

      free(load_debug);
}

void load_progress(void)
{
   if(current_game_rom_load_count < current_game_rom_count)
      current_game_rom_load_count ++;

   loading_dialog[2].d1 = current_game_rom_load_count;
   loading_dialog[2].d2 = current_game_rom_count;

     scare_mouse();
   SEND_MESSAGE(&loading_dialog[2], MSG_DRAW,  0);
     unscare_mouse();
}

// game_select_proc():
// Display then process Game Select dialog

int game_select_opts_proc(int msg, DIALOG *d, int c)
{
  int ret=x_raine_button_proc(msg, d, c);

  if(ret==D_CLOSE){
    Unselect_Button(d);
    FadeGUI();

    // sound
    game_select_opts[2].flags=
      game_select_opts[3].flags=
      game_select_opts[4].flags=
    
      // status
      game_select_opts[6].flags = game_select_opts[7].flags =
      game_select_opts[8].flags =

      // year
      game_select_opts[10].flags = game_select_opts[11].flags =
      game_select_opts[12].flags =0;

    if (ssound==0) game_select_opts[2].flags = D_SELECTED;
    else
      game_select_opts[2+ssound].flags=D_SELECTED;
    game_select_opts[6+sstatus].flags = D_SELECTED;
    game_select_opts[10+syear].flags = D_SELECTED;

    raine_centre_dialog(game_select_opts);
    raine_do_dialog(game_select_opts,-1);
     SEND_MESSAGE(&game_select[GAME_LIST], MSG_END, 0);
     SEND_MESSAGE(&game_select[GAME_LIST], MSG_START, 0);
     SEND_MESSAGE(&game_select[GAME_LIST], MSG_DRAW, 0);
     SEND_MESSAGE(&game_select[9], MSG_DRAW, 0);
     // Update infos
     listbox_getter(-4,&game_select[GAME_LIST].d1);
     
     return D_REDRAW;
  }
  return ret;
    
}

int game_select_proc(int msg, DIALOG *d, int c)
{
  int ta;
  int ret=x_raine_button_proc(msg, d, c);
  
  if(ret==D_CLOSE){
    
    Unselect_Button(d);
    FadeGUI();
    
    /*
      
    set radio buttons
    
    */
    // I want these outside the main dlg, but they can be initialized here...

#ifdef RAINE_WIN32
      if (display_cfg.screen_type == GFX_DIRECTX_WIN ||
	  display_cfg.screen_type == GFX_GDI)
	      wants_switch_res = 1;
#endif      
    
    if(rgui_cfg.game_list_mode>2) rgui_cfg.game_list_mode = 2;
    if(ssound>2) ssound = 2;
    if (sstatus>2) sstatus = 2;
    if (wants_switch_res>1) wants_switch_res = 1;
    if (syear >2) syear = 2;
      
    game_select[5+0].flags=
      game_select[5+1].flags=
      game_select[5+2].flags=

    // switch res
    game_select[GAME_SWITCH_RES+1].flags =
      game_select[GAME_SWITCH_RES+2].flags = 0;      

    game_select[GAME_SWITCH_RES+2-wants_switch_res].flags = D_SELECTED;
      
    game_select[5+rgui_cfg.game_list_mode].flags = D_SELECTED;

    build_game_avail_list();

    // Do the Dialog
    // -------------

    listbox_active = 0;
    listbox_getter(-4,&game_select[GAME_LIST].d1);
    listbox_active = 1;
    
    ret=raine_do_dialog(game_select,-1);

    listbox_active = 0;
    
    destroy_snapshot(0);
    set_palette(gui_pal);
    
    
    // Load game (if user didn't click cancel)
    // ---------------------------------------
    if((ret!=3)&&(ret!=-1)){

      switch(rgui_cfg.game_list_mode){
      case 0x00:
	ta = game_select[GAME_LIST].d1;
	if((ta >= 0)&&(ta < my_game_count)){
	  raine_cfg.req_game_index = ta;
	  do_load_game();
	}
	break;
      case 0x01:
	ta = game_select[GAME_LIST].d1;
	if((ta >= 0)&&((UINT32)ta < GameAvailCount)){
	  raine_cfg.req_game_index = GameAvail[ta];
	  do_load_game();
	}
	break;
      case 0x02:
	ta = game_select[GAME_LIST].d1;
	if((ta >= 0)&&((UINT32)ta < GameMissingCount)){
	  raine_cfg.req_game_index = GameMissing[ta];
	  do_load_game();
	}
	break;
      }

    }

    free_game_avail_list();

    if(WantScreen)

      return D_EXIT;

    else

      return D_REDRAW;

  }
  return ret;
}

int romdir_edit_proc(int msg, DIALOG *d, int c)
{
   int i;
   int ret=x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();

      sprintf(romedit[0],"%s",dir_cfg.rom_dir[0]);
      sprintf(romedit[1],"%s",dir_cfg.rom_dir[1]);
      sprintf(romedit[2],"%s",dir_cfg.rom_dir[2]);
      sprintf(romedit[3],"%s",dir_cfg.rom_dir[3]);

      ret=raine_do_dialog(romdir_edit_dialog,-1);
      if(ret==1){

      sprintf(dir_cfg.rom_dir[0],"%s",romedit[0]);
      sprintf(dir_cfg.rom_dir[1],"%s",romedit[1]);
      sprintf(dir_cfg.rom_dir[2],"%s",romedit[2]);
      sprintf(dir_cfg.rom_dir[3],"%s",romedit[3]);

      for(i = 0; i < 4; i ++){
         if(dir_cfg.rom_dir[i][0]){
            put_backslash(dir_cfg.rom_dir[i]);
            strlwr(dir_cfg.rom_dir[i]);
         }
      }

      build_game_avail_list();

      scare_mouse();
      SEND_MESSAGE(&game_select[GAME_LIST],  MSG_START, 0);
      SEND_MESSAGE(&game_select[GAME_LIST],  MSG_DRAW,  0);
      SEND_MESSAGE(&game_select[9], MSG_DRAW,  0);
      unscare_mouse();
      dialog_oxygen();

      }
      return D_REDRAW;
   }
   return ret;
}

int dir_edit_proc(int msg, DIALOG *d, int c)
{
   char dir_path[256];
   char *ta;
   int ret;

   ret = x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();

      sprintf(dir_path,"%s",romedit[d->d1]);

      if(raine_file_select("Select directory", dir_path, NULL)){
	
      // Cut any filename

      ta = get_filename(dir_path);
      if(ta) ta[0] = 0;

      sprintf(romedit[d->d1],"%s",dir_path);

      }
      
      return D_REDRAW;
   }
   return ret;
}

/******************************************************************************/

int auto_edit_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      if(InputCount){
         InputList[auto_select[1].d1].auto_rate ++;
         if(InputList[auto_select[1].d1].auto_rate>5) InputList[auto_select[1].d1].auto_rate = 0;
      }
      ret = D_REDRAW;
   }
   return ret;
}

static char *AutoName(char rate)
{
   switch(rate){
   case 0:  return "Off";
   case 1:  return "1 (30 fps)";
   case 2:  return "2 (15 fps)";
   case 3:  return "3 (10 fps)";
   case 4:  return "4 (07 fps)";
   case 5:  return "5 (06 fps)";
   default: return "Off";
   }
}

char *autolist_getter(int index, int *list_size)
{
   static char S[128];

   if(InputCount){

   switch(index){
   case -1:			// Return List Size
      *list_size=InputCount;
      return NULL;
   break;
   case -2:			// Act upon List Object Selection
      if(InputCount){
         InputList[auto_select[1].d1].auto_rate ++;
         if(InputList[auto_select[1].d1].auto_rate>5) InputList[auto_select[1].d1].auto_rate = 0;
      }
      *list_size=D_REDRAW;
      return NULL;
   break;
   case -3:			// Act Keyboard Input
      return NULL;
   break;
   default:
      if((index >= 0)&&(index < InputCount)){
         sprintf(S,"%-21s %s",InputList[index].InputName, AutoName(InputList[index].auto_rate));
         return S;
      }
      else
         return NULL;
   break;
   }

   }
   else{

   if(index==-1){
      *list_size=1;
      return NULL;
   }
   else{
      if(index==0){
         return(raine_translate_text("<No Inputs Mapped>"));
      }
      else{
         return NULL;
      }
   }

   }
}

int auto_select_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();

      ret=0;
      while((ret!=2)&&(ret!=-1)){
         ret=raine_do_dialog(auto_select,-1);
         if((ret!=2)&&(ret!=-1)){
            if(InputCount){
               InputList[auto_select[1].d1].auto_rate ++;
               if(InputList[auto_select[1].d1].auto_rate>5) InputList[auto_select[1].d1].auto_rate = 0;
            }
         }
      }

      ret = D_REDRAW;
   }
   return ret;
}

/******************************************************************************/

typedef struct GUI_JOY
{
   char *name;		// Mode name/string
   UINT32 id;		// ID_ Value for this mode
} GUI_JOY;

static GUI_JOY joystick_data[] =
{
   { "Autodetect",              JOY_TYPE_AUTODETECT,      },
   { "None",                    JOY_TYPE_NONE,            },
#ifdef JOYSTICK_DRIVER_STANDARD
   { "Joystick (2 buttons)",    JOY_TYPE_STANDARD,        }, // Standard
   { "Joystick (4 buttons)",    JOY_TYPE_4BUTTON,         },
   { "Joystick (6 buttons)",    JOY_TYPE_6BUTTON,         },
   { "Joystick (8 buttons)",    JOY_TYPE_8BUTTON,         },
   { "Joystick (2 pads)",       JOY_TYPE_2PADS,           },
   { "Flightstick Pro",         JOY_TYPE_FSPRO,           },
   { "Wingman Extreme",         JOY_TYPE_WINGEX,          },
#endif
#ifdef JOYSTICK_DRIVER_WINGWARRIOR
   { "Wingman Warrior",         JOY_TYPE_WINGWARRIOR,     }, // Wingman Warrior
#endif
#ifdef JOYSTICK_DRIVER_GAMEPAD_PRO
   { "Gamepad Pro",             JOY_TYPE_GAMEPAD_PRO,     }, // Game Pad Pro
#endif
#ifdef JOYSTICK_DRIVER_GRIP
   { "Grip",                    JOY_TYPE_GRIP,            }, // Grip
   { "Grip 4 way",              JOY_TYPE_GRIP4,           },
#endif
#ifdef JOYSTICK_DRIVER_SIDEWINDER
   { "Sidewinder",              JOY_TYPE_SIDEWINDER,      }, // Sidewinder
#endif
#ifdef JOY_TYPE_SIDEWINDER_AG
   { "Sidewinder Aggressive",   JOY_TYPE_SIDEWINDER_AG,   }, // Sidewinder Aggressive
#endif
#ifdef JOYSTICK_DRIVER_SNESPAD
   { "SNES (port 1)",           JOY_TYPE_SNESPAD_LPT1,    }, // SNES
   { "SNES (port 2)",           JOY_TYPE_SNESPAD_LPT2,    },
   { "SNES (port 3)",           JOY_TYPE_SNESPAD_LPT3,    },
#endif
#ifdef JOYSTICK_DRIVER_PSXPAD
   { "PSX (port 1)",            JOY_TYPE_PSXPAD_LPT1,     }, // PSX
   { "PSX (port 2)",            JOY_TYPE_PSXPAD_LPT2,     },
   { "PSX (port 3)",            JOY_TYPE_PSXPAD_LPT3,     },
#endif
#ifdef JOYSTICK_DRIVER_N64PAD
   { "N64 (port 1)",            JOY_TYPE_N64PAD_LPT1,     }, // N64
   { "N64 (port 2)",            JOY_TYPE_N64PAD_LPT2,     },
   { "N64 (port 3)",            JOY_TYPE_N64PAD_LPT3,     },
#endif
#ifdef JOYSTICK_DRIVER_DB9
   { "DB9 (port 1)",            JOY_TYPE_DB9_LPT1,        }, // DB9
   { "DB9 (port 2)",            JOY_TYPE_DB9_LPT2,        },
   { "DB9 (port 3)",            JOY_TYPE_DB9_LPT3,        },
#endif
#ifdef JOYSTICK_DRIVER_TURBOGRAFX
   { "Turbografx (port 1)",     JOY_TYPE_TURBOGRAFX_LPT1, }, // TURBOGFX
   { "Turbografx (port 2)",     JOY_TYPE_TURBOGRAFX_LPT2, },
   { "Turbografx (port 3)",     JOY_TYPE_TURBOGRAFX_LPT3, },
#endif
#ifdef JOYSTICK_DRIVER_IFSEGA_ISA
   { "SEGA (isa)",              JOY_TYPE_IFSEGA_ISA,      }, // SEGA ISA
#endif
#ifdef JOYSTICK_DRIVER_IFSEGA_PCI
   { "SEGA (pci)",              JOY_TYPE_IFSEGA_PCI,      }, // SEGA PCI
#endif
#ifdef JOYSTICK_DRIVER_IFSEGA_PCI_FAST
   { "SEGA (pci fast)",         JOY_TYPE_IFSEGA_PCI_FAST, }, // SEGA PCI FAST
#endif
#ifdef JOYSTICK_DRIVER_WIN32
   { "Win32 Joystick Driver",   JOY_TYPE_WIN32,           }, // Win32/DirectX
#endif
};

/* gfx_card_getter:
 *  Listbox data getter routine for the graphics card list.
 */

char *joylist_getter(int index, int *list_size)
{
   switch(index){
   case -1:			// Return List Size
      if(list_size){
	 *list_size = (sizeof(joystick_data) / sizeof(GUI_JOY));
      }
      return NULL;
   break;
   case -2:
      FadeGUI();
      DoJoyEdit();
      *list_size=D_REDRAW;
      return NULL;
   break;
   case -3:			// Act Keyboard Input
      return NULL;
   break;
   default:
      if((index >= 0)&&(index < (sizeof(joystick_data) / sizeof(GUI_JOY))))
         return joystick_data[index].name;
      else
         return NULL;
   break;
   }
}

// joy_name():
// Get card name for card ID

char *joy_name(int index)
{
   int ta,tb;

   tb = (sizeof(joystick_data) / sizeof(GUI_JOY));

   for(ta=0;ta<tb;ta++){
      if(joystick_data[ta].id == (UINT32)index)
         return joystick_data[ta].name;
   }

   return joystick_data[1].name;	// None
}

int joystick_number(int index)
{
   int ta,tb;

   tb = (sizeof(joystick_data) / sizeof(GUI_JOY));

   for(ta=0;ta<tb;ta++){
      if(joystick_data[ta].id == (UINT32)index)
         return ta;
   }

   return 1;				// None
}

// joystick_id():
// Get joy ID for joy number

int joystick_id(int index)
{
   int tb;

   tb = (sizeof(joystick_data) / sizeof(GUI_JOY));

   if((index>=0)&&(index<tb)){
      return joystick_data[index].id;
   }

   return joystick_data[1].id;		// None
}

void DoJoyEdit(void)
{
   int ta,tb;
   char *msg;

   ta = joystick_id(joy_select[1].d1);

   remove_joystick();

   JoystickType = JOY_TYPE_NONE;

   switch(ta){
   case JOY_TYPE_NONE:
      JoystickType=JOY_TYPE_NONE;
      return;
   break;
   default:		// Allegro

     show_mouse(NULL);
   if(install_joystick(ta)){
       show_mouse(screen);
      raine_alert(raine_translate_text("Control Error"),raine_translate_text("Unable to initialise Joystick"),allegro_error,NULL,raine_translate_text("&Okay"),NULL,'O',0);
      JoystickType=JOY_TYPE_NONE;
      return;
   }

   if(!(num_joysticks)){
       show_mouse(screen);
      raine_alert(raine_translate_text("Control Error"),raine_translate_text("No Joysticks of type"),joy_name(ta),raine_translate_text("could be detected"),raine_translate_text("&Okay"),NULL,'O',0);
      JoystickType=JOY_TYPE_NONE;
      return;
   }

   for(tb=0; tb<num_joysticks; tb++){

   while(joy[tb].flags & JOYFLAG_CALIBRATE){
      msg=((char*)calibrate_joystick_name(tb));
	show_mouse(screen);
      raine_alert(raine_translate_text("Control Setup"),raine_translate_text("Joystick Calibration:"),msg,raine_translate_text("and press space..."),raine_translate_text("Space"),NULL,' ',0);
	show_mouse(NULL);
      if(calibrate_joystick(tb)){
         show_mouse(screen);
         raine_alert(raine_translate_text("Control Error"),NULL,raine_translate_text("Joystick Calibration error"),NULL,raine_translate_text("&Okay"),NULL,'O',0);
         JoystickType=JOY_TYPE_NONE;
         return;
      }
   }

   }

     show_mouse(screen);
   JoystickType=ta;
   return;
   break;
   }
}

int joy_edit_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      DoJoyEdit();
      ret = D_REDRAW;
   }
   return ret;
}

int joy_calib_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      ret=0;
      while((ret!=2)&&(ret!=-1)){
         joy_select[1].d1 = joystick_number(JoystickType);
         ret=raine_do_dialog(joy_select,-1);
         if((ret!=2)&&(ret!=-1)){
            FadeGUI();
            DoJoyEdit();
         }
      }
      ret = D_REDRAW;
   }
   return ret;
}

int game_reset_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      reset_game_hardware();
      ret = D_REDRAW;
   }
   return ret;
}

void DoLangEdit(void)
{
   int ta;
   ta=language_dialog[1].d1;
   if(ta<LanguageSw.Count){
      SetLanguageSwitch(ta);
      sprintf(langname,"%s",LanguageSw.Mode[ta]);
   }
}

// language_getter():
// Get mode names for langauge/version select list

char *language_getter(int index, int *list_size)
{
   switch(index){
   case -1:
      *list_size=LanguageSw.Count;
      return NULL;
   break;
   case -2:			// Act upon List Object Selection
      DoLangEdit();
      *list_size=D_REDRAW;
      return NULL;
   break;
   case -3:			// Act Keyboard Input
      return NULL;
   break;
   default:
      if((index >= 0)&&(index<LanguageSw.Count))
         return LanguageSw.Mode[index];
      else
         return NULL;
   break;
   }
}

int language_proc(int msg, DIALOG *d, int c)
{
   int ret=x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      ret=GetLanguageSwitch();
      if(LanguageSw.Count){
         sprintf(langname,"%s",LanguageSw.Mode[ret]);
         language_dialog[1].d1=ret;
      }
      else{
         sprintf(langname,raine_translate_text("<No language switches>"));
      }
      ret=0;
      while((ret!=2)&&(ret!=-1)){
         ret = raine_do_dialog(language_dialog,-1);
         if((ret!=2)&&(ret!=-1)){
            DoLangEdit();
         }
      }
      return D_REDRAW;
   }
   return ret;
}

int language_sel_proc(int msg, DIALOG *d, int c)
{
   int ret=x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      DoLangEdit();
      return D_REDRAW;
   }
   return ret;
}

// game_setup_proc():
// Does game setup options dialog

int game_setup_proc(int msg, DIALOG *d, int c)
{
   int ret=x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      ret=0;
      while((ret!=2)&&(ret!=-1)){
         ret = raine_do_dialog(game_setup_dialog,-1);
      }
      return D_REDRAW;
   }
   return ret;
}

// control_setup_proc():
// Does game setup options dialog

int control_setup_proc(int msg, DIALOG *d, int c)
{
   int ret=x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      ret=0;
      while((ret!=2)&&(ret!=-1)){
         ret=raine_do_dialog(control_setup_dialog,-1);
      }
      return D_REDRAW;
   }
   return ret;
}

// raine_quit_proc():
// Process a click on 'quit raine' button

int raine_quit_proc(int msg, DIALOG *d, int c)
{
   int ret=x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      if((raine_alert(raine_translate_text("Warning"),NULL,raine_translate_text("Quit Raine?"),NULL,raine_translate_text("&Yes"),raine_translate_text("&No"),'Y','N'))!=1){
         return D_REDRAW;
      }
      else{
         WantQuit=1;
         return D_EXIT;
      }
   }
   return ret;
}

// game_play_proc():
// Process a click on 'play game' button

int game_play_proc(int msg, DIALOG *d, int c)
{
   int ret = x_raine_button_proc(msg, d, c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();
      if(!current_game){
         raine_alert(raine_translate_text("Error"),raine_translate_text("No Game Loaded"),raine_translate_text("Click on Change Game"),raine_translate_text("to Select a Game."),raine_translate_text("&Okay"),NULL,'O',0);
         ret = D_REDRAW;
      }
      else{
         WantPlay = 1;
         ret = D_EXIT;
      }
   }
   return ret;
}

// real primitive pen allocation

static int used_pens;

int allocate_pens(int pens)
{
   int ret;

   if((used_pens+pens)<=256){
      ret = used_pens;
      used_pens += pens;
   }
   else{
      ret = -1;
   }
   return ret;
}

void free_pens(int pens)
{
   if((used_pens-pens)>=0){
      used_pens -= pens;
   }
}

void free_all_pens(void)
{
   used_pens = 0;
}

// MakeGUIBack():
// Make a faded copy of the game screen to go behind the gui.

void MakeGUIBack(void)
{
   BITMAP *gui_backdrop = NULL;

   static BITMAP *backdrop = NULL;
   static PALETTE backdrop_pal;
   PALETTE new_pal;
   UINT8 *BIT;
   UINT8 mapcol[256];
   int ccnt,ta,tb,tc,td,te,tcr,tcg,tcb;
   
   if(screen_valid){
     
     if (display_cfg.bpp == 8) {
       gui_backdrop = create_bitmap_ex(display_cfg.bpp, display_cfg.screen_x, display_cfg.screen_y);
       
       free_all_pens();
       
       memset(new_pal,0x00, 256*4);
       memset(mapcol, 0xFF, 256);
       
       ccnt=1;	// Black=Colour0

       blit(screen,gui_backdrop,0,0,0,0,display_cfg.screen_x, display_cfg.screen_y);

       for(tb=0;tb<display_cfg.screen_y;tb++){
	 BIT=gui_backdrop->line[tb];
	 for(ta=0;ta<display_cfg.screen_x;ta++){
	   tc=BIT[ta];
	   if((te=mapcol[tc])==255){
	     tcr=pal[tc].r;
	     tcg=pal[tc].g;
	     tcb=pal[tc].b;
	     tcr = (tcr * 64) / 96;
	     if(tcr<0) tcr=0;
	     if(tcr>63) tcr=63;
	     tcg = (tcg * 64) / 80;
	     if(tcg<0) tcg=0;
	     if(tcg>63) tcg=63;
	     tcb = (tcb * 64) / 64;
	     if(tcb<0) tcb=0;
	     if(tcb>63) tcb=63;
	     te=255;
	     for(td=0;td<ccnt;td++){
	       if((tcr==new_pal[td].r)&&(tcg==new_pal[td].g)&&(tcb==new_pal[td].b)){
		 te=td;
		 td=ccnt;
		 mapcol[tc]=te;
	       }
	     }
	     if(te==255){
	       if(ccnt<216){
		 new_pal[ccnt].r=tcr;
		 new_pal[ccnt].g=tcg;
		 new_pal[ccnt].b=tcb;
		 te=ccnt;
		 ccnt++;
	       }
	       else{			// No room! Find a close match... (allegro: slow)
		 te=bestfit_color(new_pal,tcr,tcg,tcb);
	       }
	     }
	   }
	   BIT[ta]=te;
	 }

       }

       memcpy(&gui_pal, &new_pal, ccnt*4);
       allocate_pens(ccnt);
     }
     
   }
   else{

     // The backdrop image has historically always been 8bpp...
     // For now it's better to keep it this way...
     gui_backdrop = create_bitmap_ex(8, display_cfg.screen_x, display_cfg.screen_y);

     free_all_pens();

     memset(new_pal,0x00, 256*4);
     memset(mapcol, 0xFF, 256);

     ccnt=1;	// Black=Colour0

     if(!backdrop){

       if(rgui_cfg.bg_image[0])

	 backdrop = load_pcx(rgui_cfg.bg_image, backdrop_pal);

       if(!backdrop){
	 
	 backdrop = RaineData[Backdrop].dat;
	 memcpy(&backdrop_pal, RaineData[Backdrop_pal].dat, 256*4);
       }
       
     }

     clear_to_color(gui_backdrop,getpixel(backdrop,0,0));
     blit(backdrop,gui_backdrop,0,0,(display_cfg.screen_x - backdrop->w)/2,(display_cfg.screen_y - backdrop->h)/2,backdrop->w,backdrop->h);

     for(tb=0;tb<display_cfg.screen_y;tb++){
      BIT=gui_backdrop->line[tb];
      for(ta=0;ta<display_cfg.screen_x;ta++){
      tc=BIT[ta];
      if((te=mapcol[tc])==255){
         tcr=backdrop_pal[tc].r;
         tcg=backdrop_pal[tc].g;
         tcb=backdrop_pal[tc].b;
         te=255;
         for(td=0;td<ccnt;td++){
            if((tcr==new_pal[td].r)&&(tcg==new_pal[td].g)&&(tcb==new_pal[td].b)){
               te=td;
               td=ccnt;
               mapcol[tc]=te;
            }
         }
         if(te==255){
            if(ccnt<216){
               new_pal[ccnt].r=tcr;
               new_pal[ccnt].g=tcg;
               new_pal[ccnt].b=tcb;
               te=ccnt;
               ccnt++;
            }
            else{			// No room! Find a close match... (allegro: slow)
               te=bestfit_color(new_pal,tcr,tcg,tcb);
            }
         }
      }
      BIT[ta]=te;
      }

      }
      memcpy(&gui_pal, &new_pal, ccnt*4);
      allocate_pens(ccnt);
   }

   set_palette_range(gui_pal,0,255,1);
   if (gui_backdrop) {
     blit(gui_backdrop,screen,0,0,0,0,display_cfg.screen_x,display_cfg.screen_y);
     destroy_bitmap(gui_backdrop);
     gui_backdrop = NULL;
   }

   if(raine_cfg.hide)
   {
      clear(screen);
   }

}

#include "shiftjis.c"

// StartGUI():
// Actually does the gui/game loop, called from raine.c
// Caters for command line load and no gui options

int StartGUI(void)
{
   char str[256];
   
#ifdef RAINE_DEBUG
   print_debug("StartGUI(): START\n");
#endif
   
   SpriteGun1        = RaineData[Gun1].dat;
   SpriteGun2        = RaineData[Gun2].dat;

   sprintf(str, "%sconfig/language/%s", dir_cfg.exe_path, dir_cfg.language_file);
   raine_push_config_state();
   raine_set_config_file(str);

   if(new_set_gfx_mode(display_cfg.screen_type,display_cfg.screen_x,display_cfg.screen_y,display_cfg.scanlines)!=0){
     if(new_set_gfx_mode(GFX_AUTODETECT,320,240,0)){ 
       printf("%s\n",raine_translate_text("Unable to detect any screenmode."));
#ifdef RAINE_UNIX
	 printf("%s\n",raine_translate_text("On the console, you need to be root to use svgalib (or modex) !"));
#else												 
	 printf("%s\n",raine_translate_text("Get Display Doctor from www.scitechsoft.com to fix this."));
#endif				 
         printf("%s\n\n",raine_translate_text("Mail about this problem is not appreciated!"));
         exit(1);
      }
      else{
         display_cfg.screen_type= GFX_AUTODETECT;
         display_cfg.screen_x   = 320;
         display_cfg.screen_y   = 240;
         display_cfg.scanlines  = 0;
      }
   }

   sprintf(gamename, "%s %s", raine_translate_text("Game:"), raine_translate_text("<No Game Loaded>"));
   sprintf(screenname,  raine_translate_text("%s %dx%d %dbpp; %s"), raine_translate_text("Screen:"), display_cfg.screen_x, display_cfg.screen_y, display_cfg.bpp, gfx_card_name(display_cfg.screen_type));

   set_sound_variables(0);
   memset(&gui_pal, 0x00, 256*4);

   WantScreen=0;
   WantQuit=0;
   WantPlay=0;

#ifdef RAINE_DEBUG
   print_debug("StartGUI(): prepare international\n");
#endif

   register_uformat(U_SJIS, sjis_getc, sjis_getx, sjis_setc, sjis_width, sjis_cwidth, sjis_isok, 2);
   set_uformat(U_SJIS);
   while(!WantQuit){		// ----- Main Loop ------

     set_gui_palette();
     gui_fg_color = CGUI_COL_TEXT_1;
     gui_bg_color = CGUI_BOX_COL_MIDDLE;

     /* Be carefull here. Windows seems to have very unstable threads. You */
     /* MUST init these in this order, and avoid the mouse in full-screen... */
     install_timer();
     install_keyboard();
     install_mouse();

   MakeGUIBack();

     set_mouse_sprite(RaineData[Mouse].dat);
     show_mouse(screen);

   /*

   check for load game request (from the command line)

   */

   if(raine_cfg.req_load_game)

      do_load_game();

   if(!raine_cfg.no_gui)	// GUI MODE
   {

#ifdef RAINE_DEBUG
   print_debug("StartGUI(): raine_do_dialog loop\n");
#endif
      if(!WantScreen)
      {
         while(raine_do_dialog(main_dialog, -1) == -1)
         {
         }
      }

   }
   else				// NO GUI MODE
   {

   WantQuit=1;

   if(current_game)

      WantPlay = 1;

   else

      WantPlay = 0;

   }

   if(WantScreen){		// Are we here for a screenchange?
      WantScreen=0;
      ScreenChange();
   }

     remove_mouse();
   clear_keybuf();
   remove_keyboard();
   remove_timer();

   if(display_cfg.scanlines){
      if(display_cfg.triple_buffer){
         BITMAP *buf;
         buf = create_bitmap(display_cfg.screen_x,display_cfg.screen_y*3);
         clear(buf);
         blit(buf,screen,0,0,0,0,display_cfg.screen_x,display_cfg.screen_y*3);
         destroy_bitmap(buf);
      }
      else{
         BITMAP *buf;
         buf = create_bitmap(display_cfg.screen_x,display_cfg.screen_y);
         clear(buf);
         blit(buf,screen,0,0,0,0,display_cfg.screen_x,display_cfg.screen_y);
         destroy_bitmap(buf);
      }
   }
   else{
       clear(screen);
   }

   text_mode(-1);

   if(WantPlay){		// Are we able to and wanting to play?
      WantPlay = 0;
      if(run_game_emulation()){	// In no gui mode, tab will reactivate the gui (req. by bubbles)
         raine_cfg.no_gui = 0;
         WantQuit = 0;
      }
      screen_valid=1;
   }

   }				// ----- Main Loop -----

   /*

   close down the current game (if there is one)

   */

   if(current_game){

      save_game_config();
      hs_close();

      current_game->clear_game();

      ClearDefault();

      current_game = NULL;
   }

   raine_pop_config_state();

   return 0;
}
