/******************************************************************************/
/*                                                                            */
/*                                  RAINE GUI                                 */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

extern PALETTE gui_pal;

typedef struct RGUI_CFG
{
   UINT32 gui_col_text_1;
   UINT32 gui_col_text_2;
   UINT32 gui_col_black;
   UINT32 gui_col_select;
   UINT32 gui_box_col_high_2;
   UINT32 gui_box_col_high_1;
   UINT32 gui_box_col_middle;
   UINT32 gui_box_col_low_1;
   UINT32 gui_box_col_low_2;
   UINT32 game_list_mode;		// Show full, available or missing games
   UINT8 bg_image[256];			// bg image
   UINT8 font_datafile[256];	// font file
} RGUI_CFG;

struct RGUI_CFG rgui_cfg;
extern int wants_switch_res;

#define SET_PAL( COL ) switch (display_cfg.bpp){\
  case 8: C##COL = COL; break; \
  case 15: GET_PEN_FOR_COLOUR_15(gui_pal[COL].r,gui_pal[COL].g,gui_pal[COL].b,C##COL); break; \
  case 16: GET_PEN_FOR_COLOUR_16(gui_pal[COL].r,gui_pal[COL].g,gui_pal[COL].b,C##COL); break; \
  case 24: GET_PEN_FOR_COLOUR_24(gui_pal[COL].r,gui_pal[COL].g,gui_pal[COL].b,C##COL); break; \
  case 32: GET_PEN_FOR_COLOUR_32(gui_pal[COL].r,gui_pal[COL].g,gui_pal[COL].b,C##COL); break; \
}

// StartGUI():
// Does the whole gui loop, currently always returns 0.

int StartGUI(void);
void Unselect_Button(DIALOG *d);
void FadeGUI(void);
BITMAP *make_mapped_bitmap(BITMAP *src, int *start, PALETTE *src_pal, UINT32 cols);
void destroy_mapped_bitmap(BITMAP *dest, UINT32 cols);
void set_gui_palette(void);
void MakeGUIBack(void);
void raine_centre_dialog(DIALOG *dialog);
void load_progress(void);
