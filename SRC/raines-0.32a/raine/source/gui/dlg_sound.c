#include "raine.h"
#include "rgui.h"
#include "rguiproc.h"
#include "gui.h"
#include "dlg_sound.h"
#include "palette.h"
#include "ingame.h"

#include "sasound.h"

extern int use_emulated_ym3812;
extern char soundname[96];

DIALOG sound_setup_dialog[] =
{
   /* (dialog proc)     (x)   (y)   (w)   (h)   (fg)             (bg)                 (key) (flags)  (d1)  (d2)  (dp) */
   { d_raine_window_proc,0,    0,    216,  190,  GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    0,    "Sound Setup"},
   { d_raine_list_proc,  8,    28,   200,  80,   GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  0,    D_EXIT,  0,    0,    soundcardlist_getter},
   { x_raine_button_proc,8,    166,  48,   16,   GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  'O',  D_EXIT,  0,    0,    "&Okay"},
   { x_raine_button_proc,64,   166,  48,   16,   GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  'C',  D_EXIT,  0,    0,    "&Cancel"},
   { x_text_proc,        8,    16,   0,    8,    GUI_COL_TEXT_2,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    0,    "Choose Sound Driver:"},
   { x_text_proc,        8,    114,  1,    1,    GUI_COL_TEXT_2,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    0,    "Sample Rate:"},
   { x_raine_radio_proc, 8,    124,  64,   9,    GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    1,    "11025"},
   { x_raine_radio_proc, 8,    134,  64,   9,    GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    1,    "22050"},
   { x_raine_radio_proc, 8,    144,  64,   9,    GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    1,    "44100"},
   { NULL,               0,    0,    0,    0,    0,               0,                   0,    0,       0,    0,    NULL}
};


char *soundcardlist_getter(int index, int *list_size)
{
   if(index==-1){
      *list_size = RaineSoundCardTotal;
      return NULL;
   }
   else{
      if((index >= 0)&&(index<RaineSoundCardTotal))
         return sound_card_name(index);
      else
         return NULL;
   }
}

void get_sound_variables(void)
{
      // Initialize Sample Rate switch
      // -----------------------------

      sound_setup_dialog[SND_RATE_LIST].flags=0;
      sound_setup_dialog[SND_RATE_LIST+1].flags=0;
      sound_setup_dialog[SND_RATE_LIST+2].flags=0;

      switch(SampleRate){
          case 11025:
             sound_setup_dialog[SND_RATE_LIST+0].flags=D_SELECTED;
          break;
          case 44100:
             sound_setup_dialog[SND_RATE_LIST+2].flags=D_SELECTED;
          break;
          case 22050:
          default:
             SampleRate=22050;
             sound_setup_dialog[SND_RATE_LIST+1].flags=D_SELECTED;
          break;
      }

      // Initialize Soundcard switch
      // ---------------------------

      sound_setup_dialog[1].d1=RaineSoundCard;
}

void set_sound_variables(UINT8 update)
{
   if(update){
   RaineSoundCard=sound_setup_dialog[1].d1;

   if((sound_setup_dialog[SND_RATE_LIST].flags)&D_SELECTED)   SampleRate=11025;
   if((sound_setup_dialog[SND_RATE_LIST+1].flags)&D_SELECTED) SampleRate=22050;
   if((sound_setup_dialog[SND_RATE_LIST+2].flags)&D_SELECTED) SampleRate=44100;

   audio_sample_rate = sound_cfg.sample_rate = SampleRate;
   
   change_sample_rate = 1;
   }

   sprintf(soundname, "%s %s", raine_translate_text("Sound:"), raine_translate_text(sound_card_name(RaineSoundCard)));
   sprintf(soundname, "%s; %02dkHz",soundname,SampleRate/1000);
}

// sound_setup_proc():
// Does sound setup options dialog

int sound_setup_proc(int msg, DIALOG *d, int c)
{
   int ret=x_raine_button_proc(msg,d,c);

   if(ret==D_CLOSE){
      Unselect_Button(d);
      FadeGUI();

      get_sound_variables();

      ret=raine_do_dialog(sound_setup_dialog,-1);

      if((ret!=3)&&(ret!=-1)){
         set_sound_variables(1);
      }

      return D_REDRAW;
   }
   return ret;
}

void *old_draw = NULL;
extern int screen_valid;
extern int SampleVol[MAX_STREAM_CHANNELS];
static void *player = NULL;
extern BITMAP *BlitSource;

static void INLINE helper(DIALOG *dlg,AL_METHOD(int, proc, (int, struct DIALOG *, int )),
   int x, int y, int w, int h,   /* position and size of the object */
   int fg, int bg,               /* foreground and background colors */
   int key,                      /* keyboard shortcut (ASCII code) */
   int flags,                    /* flags about the object state */
   int d1, int d2,               /* any data the object might require */
   void *dp,void *dp2,void *dp3) {       /* pointers to more object data */
  dlg->proc = proc;
  dlg->x=x; dlg->y=y; dlg->w=w; dlg->h=h;
  dlg->fg=fg; dlg->bg=bg;
  dlg->flags=flags;
  dlg->d1=d1; dlg->d2=d2;
  dlg->dp = dp; dlg->dp2 = dp2; dlg->dp3 = dp3;
}

static int mixer_slider(void *dp3,int d2) {
  int *pvol = ((int*)dp3);
  int id = pvol - SampleVol;
  saSetVolume(id,d2);
  return D_O_K;
}    
  
static int kill; // indicate wether to kill the gui_screen...

static void mixer_draw(int *kill) {
  static DIALOG mixer[64];
  const char *name;
  int x=25, y=0,n=1,channel,ymax=0;
  int old_valid = screen_valid;
  
  if (!GameMouse)
    install_mouse();
  screen_valid = 1;
  set_mouse_sprite(RaineData[Mouse].dat); // This is really messy !!!
  MakeGUIBack();
  screen_valid = old_valid;
  setup_gui_screen(kill); // also shows mouse
  
  helper(mixer,d_raine_window_proc,x,   y,   270,  240,  GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  0,    0,       0,    0,    "Mixer",NULL,NULL);
  x+=10; y+=20;
  for (channel = 0;channel < MAX_STREAM_CHANNELS;channel ++) {
    name = stream_get_name(channel);
    if (name) {
      
      helper(&mixer[n++],x_text_proc,x, y, 1, 1, GUI_COL_TEXT_2, GUI_BOX_COL_MIDDLE, 0, 0, 0, 0, ((void*)name),NULL,NULL);
      y+=10;
      helper(&mixer[n++],raine_slider_proc,x,y,100,10,GUI_COL_TEXT_2,GUI_BOX_COL_MIDDLE,0,0,255,SampleVol[channel],NULL,mixer_slider,&SampleVol[channel]);
      y+=10;
      if (y>180){
	ymax = y;
	x+=130; y=20;
      }
    }
  }

  if (ymax) { // The button is at the bottom !
    y = ymax;
    x-= 130;
  }
  
  if (y == 20) { // No sound
    helper(&mixer[n++],x_text_proc,x, y, 1, 1, GUI_COL_TEXT_2, GUI_BOX_COL_MIDDLE, 0, 0, 0, 0, "No stream allocated",NULL,NULL);
    y+=20;
  } else
    y+=10;
  
  // Exit button
  helper(&mixer[n++],x_raine_button_proc,x,y, 48,  16,   GUI_COL_TEXT_1,  GUI_BOX_COL_MIDDLE,  'E',  D_EXIT,  0,    0,    "&Exit",NULL,NULL);
  
  helper(&mixer[n],NULL,               0,    0,    0,    0,    0,    0,    0,    0,       0,    0,    NULL,NULL,NULL);
  if (ymax)
    x += 130; // To centre...
  mixer[0].w = x+130;
  mixer[0].h = y+25;
  raine_centre_dialog(mixer);
  player = init_dialog(mixer,n-1);  
}

static void my_update_dlg() {
  if (!update_dialog(player)) {
    switch_mixer();
  }
}

void switch_mixer() {
   VIDEO_INFO *vid_info;
   vid_info = current_game->video_info;
   if (old_draw) { // restores normal screen
     if (player) {
       shutdown_dialog(player);
       player = NULL;
     }
     reset_palette_map();
     show_mouse(NULL);
     if (!GameMouse)
       remove_mouse();
     done_gui_screen(&kill);
     vid_info->draw_game = old_draw;
     old_draw=NULL;
   } else {
     old_draw = vid_info->draw_game;
     vid_info->draw_game = my_update_dlg;
     mixer_draw(&kill);
   }
}
