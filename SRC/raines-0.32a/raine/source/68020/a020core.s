	.file	"core.c"
gcc2_compiled.:
___gnu_compiled_c:

#include "asmdefs.inc"

CODE_SEG

#ifdef RAINE_UNIX
	#include "source/68020/asm02x.s"
#endif
#ifdef __MINGW32__
	#include "source/68020/asm02x.s"
#endif
	#include "source/68020/asm020.s"
	#include "source/68020/asm021.s"
	#include "source/68020/asm022.s"
	#include "source/68020/asm023.s"
	#include "source/68020/asm024.s"
	#include "source/68020/asm025.s"
	#include "source/68020/asm026.s"
	#include "source/68020/asm027.s"
	#include "source/68020/asm028.s"
	#include "source/68020/asm029.s"
	#include "source/68020/asm02a.s"
	#include "source/68020/asm02b.s"
	#include "source/68020/asm02c.s"
	#include "source/68020/asm02d.s"
	#include "source/68020/asm02e.s"
	#include "source/68020/asm02f.s"
.end
