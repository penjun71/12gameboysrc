/******************************************************************************/
/*                                                                            */
/*                            RAINE MEMORY MANAGEMENT                         */
/*                                                                            */
/******************************************************************************/

#ifndef __newmem_h_
#define __newmem_h_

#include "deftypes.h"

// ---
// Memory leak tracking, from Mirko Buffoni !!
// ---

#ifdef MEMORY_DEBUG

#define malloc(A) mymalloc(A, __FILE__, __LINE__)
#define realloc(A,B) myrealloc(A, B, __FILE__, __LINE__)
#define free(A) myfree(A, __FILE__, __LINE__)
#define InitPurify mbInitPurify
#define DonePurify mbDonePurify

void mbInitPurify(void);
void mbDonePurify(void);
void *mymalloc(UINT32 size, char *fname, UINT32 fline);
void *myrealloc(void *ptr, UINT32 size, char *fname, UINT32 fline);
void myfree(void *ptr, char *fname, UINT32 fline);

#endif

// ---

UINT8 *AllocateMem(UINT32 size);

void FreeMem(UINT8 *mem);

void ResetMemoryPool(void);

void FreeMemoryPool(void);
int GetMemoryPoolSize();

#endif // __newmem_h_
