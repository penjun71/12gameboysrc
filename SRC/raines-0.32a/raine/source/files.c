/******************************************************************************/
/*                                                                            */
/*                        RAINE FILE ACCESS/ZIP SUPPORT                       */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "debug.h"
#include "unzip.h"
#include "files.h"
#include "starhelp.h"

int load_file(char *filename, UINT8 *dest, UINT32 size)
{
   FILE *file_ptr;

   file_ptr = fopen(filename,"rb");

   if(file_ptr){
      fread(dest, 1, size, file_ptr);
      fclose(file_ptr);
      return 1;		// Success
   }
   else{
      return 0;		// Failure
   }
}

int save_file(char *filename, UINT8 *source, UINT32 size)
{
   FILE *file_ptr;

   file_ptr = fopen(filename,"wb");

   if(file_ptr){
      fwrite(source, 1, size, file_ptr);
      fclose(file_ptr);
      return 1;		// Success
   }
   else{
      return 0;		// Failure
   }
}

int size_file(char *filename)
{
   int len;
   FILE *file_ptr;

   len = 0;

   file_ptr = fopen(filename,"rb");

   if(file_ptr){

      if(!(fseek(file_ptr,0,SEEK_END))){
         len = ftell(file_ptr);
         if(len<0) len=0;
      }

      fclose(file_ptr);
   }

   return len;
}

/*

find a file by filename

*/

int unz_locate_file_name(unzFile file, char *name)
{
	int err;

    	if(!name)
		return UNZ_PARAMERROR;

	err = unzGoToFirstFile(file);

	while (err == UNZ_OK)
	{
		char current[256+1];

		unzGetCurrentFileInfo(file,NULL,current,256,NULL,0,NULL,0);

		if(!unzStringFileNameCompare(current,name,2))
			return UNZ_OK;

		err = unzGoToNextFile(file);
	}

	return err;
}

/*

find a file by crc32

*/

int unz_locate_file_crc32(unzFile file, UINT32 crc32)
{
	int err;

	if (!crc32)
		return UNZ_PARAMERROR;

	err = unzGoToFirstFile(file);

	while (err == UNZ_OK)
	{
		unz_file_info file_info;

		unzGetCurrentFileInfo(file,&file_info,NULL,0,NULL,0,NULL,0);

		if (file_info.crc == crc32)
			return UNZ_OK;

		err = unzGoToNextFile(file);
	}

	return err;
}

#ifdef RAINE_DEBUG

void save_debug(UINT8 *name, UINT8 *src, UINT32 size, UINT32 mode)
{
   if(debug_mode){
      char str[256];

      if(src){

         if(mode)
            ByteSwap(src,size);

         sprintf(str,"%sdebug/%s", dir_cfg.exe_path, name);
         save_file(str, src, size);

         #ifdef RAINE_DEBUG
         print_debug("Debug Save: '%s' saved.\n", name);
         #endif

      }
      else{

         #ifdef RAINE_DEBUG
         print_debug("Debug Save: '%s' is NULL.\n", name);
         #endif

      }

   }

}

#endif
