/******************************************************************************/
/*                                                                            */
/*                       MC68000 INTERFACE (STARSCREAM)                       */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

#include "starcpu.h"

#define MAX_68000	(2)
#define MAX_PROGRAM	(4)
#define MAX_DATA	(32)

struct S68000CONTEXT            M68000_context[MAX_68000];

struct STARSCREAM_PROGRAMREGION M68000_programregion[MAX_68000][MAX_PROGRAM];

struct STARSCREAM_DATAREGION    M68000_dataregion_rb[MAX_68000][MAX_DATA];
struct STARSCREAM_DATAREGION    M68000_dataregion_rw[MAX_68000][MAX_DATA];
struct STARSCREAM_DATAREGION    M68000_dataregion_wb[MAX_68000][MAX_DATA];
struct STARSCREAM_DATAREGION    M68000_dataregion_ww[MAX_68000][MAX_DATA];

struct STARSCREAM_DATAREGION    MC68000A_memoryall[128];

void *M68000_resethandler[MAX_68000];

/*
 *  Fill in the basic structures via these functions...
 */

void AddMemFetch (UINT32 d0, UINT32 d1, UINT8 *d2);
void AddReadByte (UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddReadWord (UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
// AddReadByte + AddReadWord :
void AddReadBW (UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);

void AddWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddWriteWord(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddWriteBW(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
// AddReadByte+AddReadWord+AddWriteByte+AddWriteWord !!!
void AddRWBW (UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);

void AddResetHandler(void *d0);

void AddMemFetchMC68000B (UINT32 d0, UINT32 d1, UINT8 *d2);
void AddReadByteMC68000B (UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddReadWordMC68000B (UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddWriteByteMC68000B(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddWriteWordMC68000B(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);

/*
 *  Set Memory pointers to our structures...
 */

void AddInitMemory(void);
void AddInitMemoryMC68000B(void);

void M68000A_load_update(void);
void M68000A_save_update(void);
void M68000B_load_update(void);
void M68000B_save_update(void);

void WriteStarScreamByte(UINT32 address, UINT8 data);
UINT8 ReadStarScreamByte(UINT32 address);

void Clear68000List(void);

/*
 *  Helper Functions for Starscream memory read/write structs
 *  ---------------------------------------------------------
 */

void Stop68000(UINT32 address, UINT8 data);

UINT8 DefBadReadByte(UINT32 address);
UINT16 DefBadReadWord(UINT32 address);
void DefBadWriteByte(UINT32 address, UINT8 data);
void DefBadWriteWord(UINT32 address, UINT16 data);

/*
 *  Byte Swapping
 */

void ByteSwap(UINT8 *MEM, UINT32 size);
