/******************************************************************************/
/*                                                                            */
/*                              DEBUG FILE SUPPORT                            */
/*                                                                            */
/******************************************************************************/

#include "raine.h"

#ifdef RAINE_DEBUG

extern int debug_mode;

void open_debug(void);

#ifdef RAINE_DOS
void print_debug(const char *format, ...) __attribute__ ((format (printf, 1, 2)));
#else
void print_debug(const char *format, ...);
#endif

void close_debug(void);

#endif
