/******************************************************************************/
/*                                                                            */
/*                               LOAD/SAVE GAME                               */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "cpumain.h"
#include "files.h"
#include "debug.h"
#include "sasound.h"

/*

Todo:

- Change ram area, so there is a savedata entry in each game driver (then it is
  not a special case in here).
- Add savedata and any callbacks needed for sound chip emulators and U68020.
- Make save_data_list into a linked list, for more expandable code.
- Think of something useful to save in the global header, currently it is empty.
- Remove old load support, after the next release or two.

*/

#include "raine.h"
#include "savegame.h"

#include "starhelp.h"
#include "mz80help.h"
#include "68020.h"

#include "config.h"
#include "m68k.h"
#include "memory.h"
#include "newcpu.h"

#include "games.h"		// Games List

#include "ingame.h"

// ---- Save File Formats ------------------------------

#define SAVE_FILE_TYPE_0      ASCII_ID('J','3','d','!')
#define SAVE_FILE_TYPE_1      ASCII_ID('R','N','E','!')

// ---- Saved Data Types -------------------------------

#define SAVE_RAM              ASCII_ID('R','A','M',0x00)
#define SAVE_END              ASCII_ID('E','N','D',0x00)

// -----------------------------------------------------

typedef struct SAVE_DATA
{
   UINT32 id;			// Unique ASCII_ID for this data
   UINT8 *source;		// Pointer to the data in memory
   UINT32 size;			// Size of the data in bytes
} SAVE_DATA;

static struct SAVE_DATA save_data_list[0x40];

int SaveDataCount;

void AddSaveData(UINT32 id, UINT8 *src, UINT32 size)
{
   save_data_list[SaveDataCount].id     = id;
   save_data_list[SaveDataCount].source = src;
   save_data_list[SaveDataCount].size   = size;
   SaveDataCount++;
}

// -----------------------------------------------------

#define CALLBACK_LOAD      (0x00000001)
#define CALLBACK_SAVE      (0x00000002)
#define CALLBACK_CORE      (0x00000004)

typedef struct SAVE_CALLBACK
{
   UINT32 flags;			// Unique ASCII_ID for this data
   void (*proc)();		// Callback routine ptr
} SAVE_CALLBACK;

static struct SAVE_CALLBACK save_callback_list[0x40];

UINT32 SaveCallbackCount;

void AddLoadCallback(void *callback)
{
   save_callback_list[SaveCallbackCount].flags     = CALLBACK_LOAD;
   save_callback_list[SaveCallbackCount].proc      = callback;
   SaveCallbackCount++;
}
void AddSaveCallback(void *callback)
{
   save_callback_list[SaveCallbackCount].flags     = CALLBACK_SAVE;
   save_callback_list[SaveCallbackCount].proc      = callback;
   SaveCallbackCount++;
}

void AddLoadCallback_Internal(void *callback)
{
   save_callback_list[SaveCallbackCount].flags     = CALLBACK_LOAD|CALLBACK_CORE;
   save_callback_list[SaveCallbackCount].proc      = callback;
   SaveCallbackCount++;
}

void AddSaveCallback_Internal(void *callback)
{
   save_callback_list[SaveCallbackCount].flags     = CALLBACK_SAVE|CALLBACK_CORE;
   save_callback_list[SaveCallbackCount].proc      = callback;
   SaveCallbackCount++;
}

void ProcessCallbackList(UINT32 flags)
{
   UINT32 ta;

   for(ta=0; ta<SaveCallbackCount; ta++){
      if(save_callback_list[ta].flags == flags){
         if(save_callback_list[ta].proc)
            save_callback_list[ta].proc();
      }
   }
}

// -----------------------------------------------------

void NewSave(PACKFILE *fout)
{
   int ta;

   // global header

   pack_mputl(SAVE_FILE_TYPE_1, fout);
   pack_iputl(0x00000000, fout);

   // ram header

   pack_mputl(SAVE_RAM, fout);
   pack_iputl(RAMSize, fout);
   pack_fwrite(RAM, RAMSize, fout);

   // user data header

   for(ta=0;ta<SaveDataCount;ta++){
      pack_mputl(save_data_list[ta].id, fout);
      pack_iputl(save_data_list[ta].size, fout);
      pack_fwrite(save_data_list[ta].source, save_data_list[ta].size, fout);
   }

   pack_mputl(SAVE_END, fout);
}

void do_save_state(char *name) {
   PACKFILE *fout;
   char str[256];
   if(RAMSize){
     sprintf(str,"%ssavegame" SLASH "%s",dir_cfg.exe_path,name);
     if (!(fout=pack_fopen(str,F_WRITE))){
      print_ingame(120,"Unable to save: savegame" SLASH "%s.sv%d",current_game->main_name,SaveSlot);
      return;
   }

   sa_pause_sound();
   stop_cpu_main();

   ProcessCallbackList(CALLBACK_SAVE);
   ProcessCallbackList(CALLBACK_SAVE|CALLBACK_CORE);

   NewSave(fout);

   pack_fclose(fout);

   print_ingame(120,"Saved to: savegame" SLASH "%s.sv%d",current_game->main_name,SaveSlot);
   start_cpu_main();
   sa_unpause_sound();
   reset_ingame_timer();

   }
   else{
      print_ingame(120,"Game does not support Saving.");
   }

   #ifdef RAINE_DEBUG
   print_debug("END: GameSave()\n");
   #endif
}
  
void GameSave(void)
{
   char str[256];

   #ifdef RAINE_DEBUG
   print_debug("BEGIN: GameSave()\n");
   #endif

   sprintf(str,"%s.sv%d",current_game->main_name,SaveSlot);
   do_save_state(str);
}

/******************************************************************************/

void read_safe_data(UINT8 *dest, UINT32 dest_size, UINT32 data_size, PACKFILE *fin)
{
   UINT32 ta,tb;

   if(dest_size==data_size){
      pack_fread(dest,data_size,fin);
      return;
   }
   if(dest_size>data_size){
      #ifdef RAINE_DEBUG
      print_debug("Actual size is bigger than load data!\n");
      #endif
      if(data_size>0) pack_fread(dest,data_size,fin);
      return;
   }
   if(dest_size<data_size){
      #ifdef RAINE_DEBUG
      print_debug("Actual size is smaller than load data!\n");
      #endif
      if(dest_size>0) pack_fread(dest,dest_size,fin);
      for(ta=dest_size;ta<data_size;ta++){	// Slowly skip the excess data
         tb = pack_getc(fin);
      }
      return;
   }
}

void NewLoad(PACKFILE *fin)
{
   int ta,tb,load_done;
   UINT32 t_size,t_id;

   // global header

   t_size = pack_igetl(fin);

   load_done=0;

   do{

   // read header

   t_id   = pack_mgetl(fin);
   t_size = pack_igetl(fin);

   switch(t_id){
      case SAVE_RAM:
         read_safe_data(RAM,RAMSize,t_size,fin);
      break;
      case SAVE_END:
         #ifdef RAINE_DEBUG
         print_debug("Load Completed Successfully\n");
         #endif
         load_done=1;
      break;
      case EOF:
         #ifdef RAINE_DEBUG
         print_debug("Load Completed, but 'END.' marker was not found\n");
         #endif
         load_done=1;
      break;
      default:
         tb=0;
         for(ta=0;ta<SaveDataCount && tb==0;ta++){
            if(save_data_list[ta].id == t_id){
               read_safe_data(save_data_list[ta].source,save_data_list[ta].size,t_size,fin);
               tb=1; 
            }
         }
         if(tb==0){
            #ifdef RAINE_DEBUG
            print_debug("Unexpected ID in savefile: %08x/%08x\n",t_id,t_size);
            #endif
            read_safe_data(NULL,0,t_size,fin);
         }
      break;
   }

   }while(!load_done);
}

void do_load_state(char *name) {
  PACKFILE *fin;
  int ta;
  char str[256];
  
   if(RAMSize){
     sprintf(str,"%ssavegame" SLASH "%s", dir_cfg.exe_path, name);
   if(!(fin=pack_fopen(str,F_READ))){
      print_ingame(120,"Unable to load: savegame" SLASH "%s.sv%d", current_game->main_name, SaveSlot);
      return;
   }

   ta = pack_mgetl(fin);

   sa_pause_sound();
   stop_cpu_main();

   switch(ta){
      case SAVE_FILE_TYPE_0:
	print_ingame(120,"savegame" SLASH "%s.sv%d is too ancient!", current_game->main_name, SaveSlot);
         pack_fclose(fin);
         return;
      break;
      case SAVE_FILE_TYPE_1:
         NewLoad(fin);
      break;
      default:
	print_ingame(120,"savegame" SLASH "%s.sv%d is not recognised", current_game->main_name, SaveSlot);
         pack_fclose(fin);
         return;
      break;
   }

   pack_fclose(fin);

   print_ingame(120,"Loaded from: savegame" SLASH "%s.sv%d", current_game->main_name, SaveSlot);

   RefreshBuffers=1;

   ProcessCallbackList(CALLBACK_LOAD|CALLBACK_CORE);
   ProcessCallbackList(CALLBACK_LOAD);

   start_cpu_main();
   sa_unpause_sound();
   reset_ingame_timer();

   }
   else{
      print_ingame(120,"Game does not support Loading.");
   }

   #ifdef RAINE_DEBUG
   print_debug("END: GameLoad()\n");
   #endif
}

void GameLoad(void)
{
   char str[256];

#ifdef RAINE_DEBUG
   print_debug("BEGIN: GameLoad()\n");
#endif

   sprintf(str,"%s.sv%d", current_game->main_name, SaveSlot);
   do_load_state(str);
}

/******************************************************************************/

#define MAX_EPROM	(4)

typedef struct EPR_DATA
{
   UINT8 *source;		// Pointer to the data in memory
   UINT32 size;			// Size of the data in bytes
   UINT8 flags;			// Flags
} EPR_DATA;

static struct EPR_DATA eeprom_list[MAX_EPROM];

int eeprom_count;

void clear_eeprom_list(void)
{
   eeprom_count=0;
}

void add_eeprom(UINT8 *source, UINT32 size, UINT8 flags)
{
   int ta;

   ta = eeprom_count;

   eeprom_list[ta].source = source;
   eeprom_list[ta].size   = size;
   eeprom_list[ta].flags  = flags;

   memset(eeprom_list[ta].source,0xff,eeprom_list[ta].size);

   eeprom_count++;
}

static char *epr_ext[MAX_EPROM]=
{
   ".epr",
   ".ep0",
   ".ep1",
   ".ep2",
};

// Include working eeprom images because it cuts down
// on mail from people who fail to read the docs and
// then ask what to do at the 'backup data failed -
// push test switch' message (ie. lamers).

#define INIT_EEPROM_FOR_DUMB_USERS

#ifdef INIT_EEPROM_FOR_DUMB_USERS

typedef struct EPR_INIT
{
   char *name;
   UINT32 data_id;
} EPR_INIT;

static struct EPR_INIT eeprom_init[]=
{
   { "arabianm", eeprom_arabianm, },
   { "arkretrn", eeprom_arkretrn, },
   { "bubblem",  eeprom_bubblem,  },
   { "bubsymph", eeprom_bubsymph, },
   { "cleofort", eeprom_cleofort, },
   { "cupfinal", eeprom_cupfinal, },
   { "dariusg",  eeprom_dariusg,  },
   { "dariusgx", eeprom_dariusgx, },
   { "eaction2", eeprom_eaction2, },
   { "kaiserkn", eeprom_kaiserkn, },
   { "pbobble2", eeprom_pbobble2, },
   { "pbobble3", eeprom_pbobble3, },
   { "puchicar", eeprom_puchicar, },
   { "puzbob2x", eeprom_puzbob2x, },
   { "trstars",  eeprom_trstars,  },
   { "twinqix",  eeprom_twinqix,  },
   { "esprade",  eeprom_esprade,  },
   { "guwange",  eeprom_guwange,  },
   { "ddonpach", eeprom_ddonpach, },
   { "uopoko",   eeprom_uopoko,   },
   { "dfeveron", eeprom_dfeveron, },
   {  NULL,      0,               },
};

#endif

int load_eeprom(void)
{
   char str[256];
   int ta,ret=0;

#ifdef INIT_EEPROM_FOR_DUMB_USERS
   int tb;
   DATAFILE *eeprom_df;
#endif

   for(ta=0;ta<eeprom_count;ta++){

   sprintf(str,"%ssavedata" SLASH "%s%s", dir_cfg.exe_path, current_game->main_name, epr_ext[ta]);
   ret = load_file(str, eeprom_list[ta].source, eeprom_list[ta].size);
   if(!ret){
     //fprintf(stderr,"clearing eeprom\n"); // naughty!
      memset(eeprom_list[ta].source,0xff,eeprom_list[ta].size);

#ifdef INIT_EEPROM_FOR_DUMB_USERS
      eeprom_df = (DATAFILE *) RaineData[eeprom].dat;

      tb=0;
      while(eeprom_init[tb].name){
         if(!( stricmp( eeprom_init[tb].name, current_game->main_name )))
         {
            memcpy(eeprom_list[ta].source, eeprom_df[eeprom_init[tb].data_id].dat, eeprom_list[ta].size);
            #ifdef RAINE_DEBUG
            print_debug("Using '%s' eeprom template...\n", current_game->main_name);
            #endif
         }
         tb++;
      };

#endif

   }

   }
   return ret;
}

void save_eeprom(void)
{
   char str[256];
   int ta;

   for(ta=0;ta<eeprom_count;ta++){
      sprintf(str,"%ssavedata" SLASH "%s%s", dir_cfg.exe_path, current_game->main_name, epr_ext[ta]);
      save_file(str, eeprom_list[ta].source, eeprom_list[ta].size);
   }
}




