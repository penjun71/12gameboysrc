/******************************************************************************/
/*                                                                            */
/*                          RAINE INTERFACE FOR MZ80                          */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"
#include "mz80.h"

#define MZ80_V3
#define MAX_Z80		(4)
#define MAX_Z80_DATA	(32)
#define MAX_Z80_PORT	(16)

extern UINT32 z80pc; /* [Kayamon] - mz80 doesn't expose this itself anymore */
struct mz80context	Z80_context[MAX_Z80];

struct MemoryReadByte	Z80_memory_rb[MAX_Z80][MAX_Z80_DATA];
struct MemoryWriteByte	Z80_memory_wb[MAX_Z80][MAX_Z80_DATA];
struct z80PortRead	Z80_port_rb[MAX_Z80][MAX_Z80_PORT];
struct z80PortWrite	Z80_port_wb[MAX_Z80][MAX_Z80_PORT];

void AddZ80AROMBase(UINT8 *d0, UINT16 d1, UINT16 d2);

void AddZ80AReadByte( UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80AWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80AReadPort( UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);
void AddZ80AWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);

void AddZ80AInit(void);

void AddZ80BROMBase(UINT8 *d0, UINT16 d1, UINT16 d2);

void AddZ80BReadByte( UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80BWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80BReadPort( UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);
void AddZ80BWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);

void AddZ80BInit(void);

void AddZ80CROMBase(UINT8 *d0, UINT16 d1, UINT16 d2);

void AddZ80CReadByte( UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80CWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80CReadPort( UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);
void AddZ80CWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);

void AddZ80CInit(void);

void AddZ80DROMBase(UINT8 *d0, UINT16 d1, UINT16 d2);

void AddZ80DReadByte( UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80DWriteByte(UINT32 d0, UINT32 d1, void *d2, UINT8 *d3);
void AddZ80DReadPort( UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);
void AddZ80DWritePort(UINT16 d0, UINT16 d1, void *d2, UINT8 *d3);

void AddZ80DInit(void);

void Z80A_load_update(void);
void Z80B_load_update(void);
void Z80C_load_update(void);
void Z80D_load_update(void);

/*----------------------------------------------*/

void ClearZ80List(void);

void StopZ80(UINT16 address, UINT8 data);

void SetStopZ80Mode2(UINT16 address);
void StopZ80Mode2(UINT16 address, UINT8 data);

void SetStopZ80BMode2(UINT16 address);
void StopZ80BMode2(UINT16 address, UINT8 data);

void SetStopZ80CMode2(UINT16 address);
void StopZ80CMode2(UINT16 address, UINT8 data);

void SetStopZ80DMode2(UINT16 address);
void StopZ80DMode2(UINT16 address, UINT8 data);

UINT16 DefBadReadZ80(UINT16 offset);
void DefBadWriteZ80(UINT16 offset, UINT8 data);

void Z80ASetBank(UINT8 *src);
void Z80BSetBank(UINT8 *src);
void Z80CSetBank(UINT8 *src);
void Z80DSetBank(UINT8 *src);

void WriteMZ80Byte(UINT32 address, UINT8 data);
UINT8 ReadMZ80Byte(UINT32 address);
