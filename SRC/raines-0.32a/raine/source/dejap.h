/* Allegro datafile object indexes, produced by grabber v3.9.37 (CVS), Unix */
/* Datafile: /home/manu/raine/raine.dat */
/* Date: Thu Jun 21 21:17:03 2001 */
/* Do not hand edit! */

#define Antiriad                         0        /* BMP  */
#define Backdrop                         1        /* BMP  */
#define Backdrop_pal                     2        /* PAL  */
#define company                          3        /* FILE */

#define company_capcom                   0        /* BMP  */
#define company_capcom_pal               1        /* PAL  */
#define company_ex_system                2        /* BMP  */
#define company_ex_system_pal            3        /* PAL  */
#define company_jaleco                   4        /* BMP  */
#define company_jaleco_pal               5        /* PAL  */
#define company_nichibutsu               6        /* BMP  */
#define company_nichibutsu_pal           7        /* PAL  */
#define company_nmk                      8        /* BMP  */
#define company_nmk_pal                  9        /* PAL  */
#define company_raizing                  10       /* BMP  */
#define company_raizing_pal              11       /* PAL  */
#define company_seta                     12       /* BMP  */
#define company_seta_pal                 13       /* PAL  */
#define company_tad                      14       /* BMP  */
#define company_tad_pal                  15       /* PAL  */
#define company_taito                    16       /* BMP  */
#define company_taito_pal                17       /* PAL  */
#define company_technos                  18       /* BMP  */
#define company_technos_pal              19       /* PAL  */
#define company_tecmo                    20       /* BMP  */
#define company_tecmo_pal                21       /* PAL  */
#define company_toaplan                  22       /* BMP  */
#define company_toaplan_pal              23       /* PAL  */
#define company_upl                      24       /* BMP  */
#define company_upl_pal                  25       /* PAL  */
#define company_visco                    26       /* BMP  */
#define company_visco_pal                27       /* PAL  */
#define company_COUNT                    28

#define ConfigDos                        4        /* DATA */
#define ConfigUnix                       5        /* DATA */
#define ConfigWin                        6        /* DATA */
#define eeprom                           7        /* FILE */

#define eeprom_arabianm                  0        /* DATA */
#define eeprom_arkretrn                  1        /* DATA */
#define eeprom_bubblem                   2        /* DATA */
#define eeprom_bubsymph                  3        /* DATA */
#define eeprom_cleofort                  4        /* DATA */
#define eeprom_cupfinal                  5        /* DATA */
#define eeprom_dariusg                   6        /* DATA */
#define eeprom_dariusgx                  7        /* DATA */
#define eeprom_ddonpach                  8        /* DATA */
#define eeprom_dfeveron                  9        /* DATA */
#define eeprom_eaction2                  10       /* DATA */
#define eeprom_esprade                   11       /* DATA */
#define eeprom_guwange                   12       /* DATA */
#define eeprom_kaiserkn                  13       /* DATA */
#define eeprom_pbobble2                  14       /* DATA */
#define eeprom_pbobble3                  15       /* DATA */
#define eeprom_puchicar                  16       /* DATA */
#define eeprom_puzbob2x                  17       /* DATA */
#define eeprom_trstars                   18       /* DATA */
#define eeprom_twinqix                   19       /* DATA */
#define eeprom_uopoko                    20       /* DATA */
#define eeprom_COUNT                     21

#define font_8x8_ingame                  8        /* DATA */
#define font_gui_fixed                   9        /* FONT */
#define font_gui_main                    10       /* FONT */
#define GUIPalette                       11       /* PAL  */
#define Gun1                             12       /* DATA */
#define Gun2                             13       /* DATA */
#define Jump1630_HACK                    14       /* DATA */
#define Jumping_HACK                     15       /* DATA */
#define Mouse                            16       /* BMP  */
#define mouse_busy                       17       /* BMP  */
#define mouse_busy_pal                   18       /* PAL  */
#define RaineLogo                        19       /* BMP  */

