#include "deftypes.h"

void DecodeStDragon(UINT8 *src);

void DecodePlusAlpha(UINT8 *src);

void DecodeRodlandE(UINT8 *src);

void DecodePipiBibi(UINT8 *src);

void DecodePlotting(UINT8 *src);

void DecodeBombJackTwin_OBJ(UINT8 *src, UINT32 size);

void DecodeBombJackTwin_BG0(UINT8 *src, UINT32 size);

void DecodeThunderDragon_OBJ(UINT8 *src);

void DecodeThunderDragon_ROM(UINT8 *src);
