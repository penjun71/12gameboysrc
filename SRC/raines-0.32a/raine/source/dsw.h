/******************************************************************************/
/*                                                                            */
/*                              DIPSWITCH SUPPORT                             */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

#define MAX_DIPSWITCHES		3

typedef struct DSW
{
   char *DSWName;		// Switch or setting name
   UINT8 bits;			// Switch bitmask or setting bitpattern
   UINT8 values;		// Number of settings defined for this switch
} DSW;

typedef struct DSTAT
{
   UINT8 pos;			//
   UINT8 offset;		//
} DSTAT;

typedef struct DIPSW
{
   UINT8 value;			// Current value
   UINT8 def;			// Default value (factory setting)
   UINT32 count;			// Items in diplist
   UINT32 address;		// RAM[] address to place dsw
   DSW   diplist[64];
   UINT32 statcount;		// Items in statlist
   DSTAT statlist[16];
} DIPSW;

struct DIPSW dipswitch[MAX_DIPSWITCHES];

// Fill DSW bytes from stat list

void make_dipswitch_bytes(void);

// Fill stat list from DSW bytes

void make_dipswitch_statlist(void);

void init_dsw(void);

extern struct DSW_DATA dsw_data_default_0[];
extern struct DSW_DATA dsw_data_default_1[];
extern struct DSW_DATA dsw_data_default_2[];

void RestoreDSWDefault(void);

UINT8 get_dsw(int i);

void load_dipswitches(char *section);

void save_dipswitches(char *section);

/******************************************************************************/
/*                                                                            */
/*         ROM SWITCH SUPPORT (consider them unofficial dip switches)         */
/*                                                                            */
/******************************************************************************/

typedef struct ROMSW
{
   char *Name;			// ROMSwitch Name
   UINT32 Address;		// Address of Switch
   int Count;			// Number of Settings(modes) for this switch
   char *Mode[16];		// Mode Name x16
   UINT8 Data[16];		// Mode Byte x16
   UINT8 def;
} ROMSW;

struct ROMSW LanguageSw;	// ROMSwitch for Language Selection (Taito roms)

void init_romsw(void);

void SetLanguageSwitch(int number);

int GetLanguageSwitch(void);

void load_romswitches(char *section);

void save_romswitches(char *section);

/******************************************************************************/
/*                                                                            */
/*                ROM PATCHING SUPPORT (something like an .ips)               */
/*                                                                            */
/******************************************************************************/

typedef struct ROM_PATCH
{
   UINT32 offset;		// offset to patch
   UINT32 data_0;		// first longword
   UINT32 data_1;		// second longword
} ROM_PATCH;

void patch_rom(UINT8 *src, ROM_PATCH *patch);

/******************************************************************************/
