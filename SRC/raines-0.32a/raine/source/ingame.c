/******************************************************************************/
/*									      */
/*	INGAME (screen blit, eagle, message list, rdtsc profile, timer)       */
/*									      */
/******************************************************************************/

#include <stdarg.h>

#include "raine.h"
#include "debug.h"
#include "config.h"
#include "sasound.h"
#include "ingame.h"
#include "eagle.h"
#include "blitasm.h"
#include "palette.h"
#include "games.h"
#include "newspr.h"
#include "emumain.h"
#include "control.h"
#include "tilemod.h"
#include "blit_x2.h"
#include "arpro.h"

typedef struct PROFILE_RESULTS
{
   UINT8 *name;
   UINT32 percent;
   UINT32 cycles;
} PROFILE_RESULTS;

static struct PROFILE_RESULTS profile_results[PRO_COUNT] =
{
   { " Misc", 0, 0 },
   { "  Cpu", 0, 0 },
   { "Sound", 0, 0 },
   { " Draw", 0, 0 },
   { " Blit", 0, 0 },
   { "  Pal", 0, 0 },
   { " Free", 0, 0 },
};

static char fps[32];		// fps() message string

/**[Message List Stuff]*************************************/

#define MSG_LIST_SIZE	10	// maximum messages onscreen

typedef struct MESSAGE
{
   INT32  messagetime;		// time before message expires
   UINT8 message[256];		// message string
} MESSAGE;

static struct MESSAGE MsgList[MSG_LIST_SIZE];

static int mbase;		// Which message is top of the list

// print_ingame():
// Add Message to Ingame Message List, using a printf() style format string.

void print_ingame(int showtime, const char *format, ...)
{
   va_list ap;
   va_start(ap,format);
   vsprintf(MsgList[mbase].message,format,ap);
   va_end(ap);

   MsgList[mbase].messagetime = showtime;

   mbase++;
   if(mbase>=MSG_LIST_SIZE) mbase=0;
}

// clear_ingame_message_list():
// Clear the ingame message list

void clear_ingame_message_list(void)
{
   int ta;

   mbase = 0;
   for(ta = 0; ta < MSG_LIST_SIZE; ta ++){
      MsgList[ta].messagetime = 0;
      sprintf(MsgList[ta].message," ");
   }
}

typedef struct RAINEBITMAP
{
   int xfull;		// Full bitmap width
   int yfull;		// Full bitmap height
   int xtop;		// X Offset of viewable area
   int ytop;		// Y offset of viewable area
   int xview;		// Viewable bitmap width
   int yview;		// Viewable bitmap height
} RAINEBITMAP;

RAINEBITMAP GameScreen;

BITMAP *BlitSource;		// *Full*
BITMAP *BlitViewSource; 	// *Viewable* part of bitmap

int disp_screen_x;
int disp_screen_y;

#ifdef TRIPLE_BUFFER

static int triple_frame;

static int scroll_async(int x, int y);

void reset_triple_buffer(void)
{
   if(display_cfg.triple_buffer){
      triple_frame = 0;
      scroll_async(0,0);
   }
}

#endif

/**[Screenshot Saving]*****************************************/

static void do_save_screen(void)
{
   UINT8 full_name[256];
   UINT8 file_name[32];

   raine_cfg.req_save_screen = 0;

   /*

   first try gamename.pcx

   */

   sprintf(file_name, "%s.pcx", current_game->main_name);

   sprintf(full_name, "%s%s", dir_cfg.screen_dir, file_name);

   /*

   otherwise, find the next gamename_NNN.pcx (or gamenNNN.pcx)

   */

   while( exists(full_name) ){

      if(dir_cfg.last_screenshot_num > 999)

	 return;

      if(dir_cfg.long_file_names)

	 sprintf(file_name, "%s_%03d.pcx", current_game->main_name, dir_cfg.last_screenshot_num++);

      else

	 sprintf(file_name, "%.5s%03d.pcx", current_game->main_name, dir_cfg.last_screenshot_num++);

      sprintf(full_name, "%s%s", dir_cfg.screen_dir, file_name);

   };

   save_pcx(full_name, BlitViewSource, pal);

   print_ingame(120, "Screen Saved to %s", file_name);
}

/*

.----.
|    | Current Onscreen Frame
|----|
|    | Next Frame - Blit here then request to display it (it's not immediately shown)
|----|
|    | Next-Next Frame - Incase the pc refresh rate is less than the game refresh rate
'----'

*/


void raine_blit_eagle(BITMAP *src, BITMAP *dest, int s_x, int s_y, int d_x, int d_y, int w, int h)
{
   UINT32 ta,d_w;

   d_w = dest->w;

   for(ta=0;ta<(UINT32)(h-1);ta++){

   eagle(
      (unsigned long *) (src->line[ta+s_y]+s_x),
      (unsigned long *) (src->line[ta+s_y+1]+s_x),
      (UINT16) w,
      dest->seg,
      (UINT32) (dest->line[0]+d_x+(d_w*(ta+ta+d_y))),
      (UINT32) (dest->line[0]+d_x+(d_w*(ta+ta+d_y+1)))
   );

   }

   ta = (h-1);

   eagle(
      (unsigned long *) (src->line[ta+s_y]+s_x),
      (unsigned long *) (src->line[ta+s_y]+s_x),
      (UINT16) w,
      dest->seg,
      (UINT32) (dest->line[0]+d_x+(d_w*(ta+ta+d_y))),
      (UINT32) (dest->line[0]+d_x+(d_w*(ta+ta+d_y+1)))
   );
}

void raine_blit_normal(BITMAP *src, BITMAP *dest, int s_x, int s_y, int d_x, int d_y, int w, int h)
{
  //UINT32 d_w;
   SourceSeg  = src->seg;
   DestSeg    = dest->seg;
   SourceStart= (UINT32) src->line[s_y]+s_x;
   DestStart  = (UINT32) dest->line[d_y]+d_x;
   BlitWidth  = w>>2;
   BlitHeight = h;
   SourceAdd  = (src->w) - w;
   DestAdd    = (dest->line[1] - dest->line[0]) - w;
   fast_blit_movsl();

}

void raine_fast_blit(BITMAP *source, BITMAP *dest, UINT32 x1, UINT32 y1, UINT32 x2, UINT32 y2, UINT32 w, UINT32 h)
{
   /*

   hack -- fool allegro into allowing offscreen blits

   */

#ifdef TRIPLE_BUFFER

   if(display_cfg.triple_buffer){

      dest->clip = 0;
      dest->cl	 = 0;
      dest->ct	 = 0;
      dest->cr	 = display_cfg.screen_x;
      dest->cb	 = display_cfg.screen_y * 3;
      dest->w	 = display_cfg.screen_x;
      dest->h	 = display_cfg.screen_y * 3;

   }
   else{

      triple_frame = 0;

   }

#endif

   /*

   do the required blit

   */

//   print_debug("(0x%04x, 0x%04x) (0x%04x, 0x%04x)\n", x1, y1, x2, y2);

#if defined(TRIPLE_BUFFER)

   if(is_linear_bitmap(dest) && (gfx_driver->linear)){

      if(display_cfg.pixel_double){

	 if(display_cfg.pixel_double == 1)
	 {
	    switch(display_cfg.bpp)
	    {
	       case 8:
		  raine_blit_x2_y2(source, dest, x1, y1, x2*2, (y2*2) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	       case 15:
	       case 16:
		  raine_blit_x2_y2_16(source, dest, x1, y1, x2*2, (y2*2) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	       case 24:
		  raine_blit_x2_y2_24(source, dest, x1, y1, x2*2, (y2*2) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	       case 32:
		  raine_blit_x2_y2_32(source, dest, x1, y1, x2*2, (y2*2) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	    }
	 }
	 else
	 {
	    switch(display_cfg.bpp)
	    {
	       case 8:
		  raine_blit_x2_y1(source, dest, x1, y1, x2*2, (y2*1) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	       case 15:
	       case 16:
		  raine_blit_x2_y1_16(source, dest, x1, y1, x2*2, (y2*1) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	       case 24:
		  raine_blit_x2_y1_24(source, dest, x1, y1, x2*2, (y2*1) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	       case 32:
		  raine_blit_x2_y1_32(source, dest, x1, y1, x2*2, (y2*1) + (display_cfg.screen_y * triple_frame), w, h);
	       break;
	    }
	 }

      }
      else{

	 if(display_cfg.bpp == 8){

	    if(display_cfg.eagle_fx)

	       raine_blit_eagle(source, dest, x1, y1, x2*2, (y2*2) + (display_cfg.screen_y * triple_frame), w, h);

	    else
	       raine_blit_normal(source, dest, x1, y1, x2, y2 + (display_cfg.screen_y * triple_frame), w, h);

	 }
	 else{

	    blit(source, dest, x1, y1, x2, y2 + (display_cfg.screen_y * triple_frame), w, h);

	 }

      }

   }
   else{

      blit(source, dest, x1, y1, x2, y2 + (display_cfg.screen_y * triple_frame), w, h);

   }

   /*

   scroll triple buffer

   */

   if(display_cfg.triple_buffer){

      scroll_async(0, display_cfg.screen_y * triple_frame);

      triple_frame ++;

      if(triple_frame > 2)

	 triple_frame = 0;

   }

#else

   blit(source, dest, x1, y1, x2, y2, w, h);

#endif

}

// DrawNormal():
// Clips/Blits game screen from pc memory > pc vram
// Also overlays the messages, fps counter...

static int destx, desty, xxx, yyy, xoff2, yoff2;

void get_screen_coordinates(int *Xoff2,int *Yoff2, int *Destx, int *Desty, int *Xxx, int *Yyy){
  *Xoff2 = xoff2;
  *Yoff2 = yoff2;
  *Xxx = xxx;
  *Yyy = yyy;
  *Destx = destx;
  *Desty = desty;
}

static UINT32 pause_time;

void overlay_ingame_interface(void)
{
   int ta,tb;

   /*

   print ingame message list

   */

   update_arpro_cheats();
   
   for(tb=0;tb<MSG_LIST_SIZE;tb++){

      ta=tb+mbase;

      if(ta>=MSG_LIST_SIZE)
	 ta-=MSG_LIST_SIZE;

      if(MsgList[ta].messagetime>0){

	 MsgList[ta].messagetime -= (INT32) skip_frame_count;
	 
	 textout_fast(MsgList[ta].message,xoff2,(yoff2+yyy-8)-(((MSG_LIST_SIZE-1)-tb)<<3),get_white_pen());

      }

   }

   /*

   print speed profile (fps)

   */

   switch(raine_cfg.show_fps_mode){
   case 0x00:				// Show nothing
   break;
   case 0x01:				// Show Accurate FPS (changes rapidly)
      ta = read_ingame_timer();
      if((UINT32)ta >= timer_next_update){					// we have done 60 frames
	 sprintf(fps,"%02d/60",render_frame_count-render_start_count);  // make message string
	 render_start_count = render_frame_count;			// render count at start
	 timer_next_update = ta + 60;					// set time of next update
      }
      //fprintf(stderr,"%s\n",fps);
      textout_fast(fps,xoff2+xxx-(5*6),yoff2,get_white_pen());
   break;
   case 0x02:				// Show Average FPS (takes a while to adapt to changes)
      ta = read_ingame_timer();
      if((UINT32)ta >= timer_next_update){					// we have done 60 frames
	 sprintf(fps,"Avg:%02d/60",fixtoi(fdiv(itofix(render_frame_count-render_start_count),itofix(ta-timer_start_count)/60)));
	 timer_next_update = ta + 60;					// set time of next update
      }
      textout_fast(fps,xoff2+xxx-(9*6),yoff2,get_white_pen());
   break;
   case 0x03:				// Show Profile results (percent)
      for(ta=0;ta<PRO_COUNT;ta++){
      sprintf(fps,"%s: %2d%%",profile_results[ta].name, profile_results[ta].percent);
      textout_fast(fps,xoff2+xxx-(10*6),yoff2+(ta*8),get_white_pen());
      }
   break;
   case 0x04:				// Show Profile results (cycles)
      for(ta=0;ta<PRO_COUNT;ta++){
      sprintf(fps,"%s: %6x",profile_results[ta].name, profile_results[ta].cycles);
      textout_fast(fps,xoff2+xxx-(13*6),yoff2+(ta*8),get_white_pen());
      }
   break;
   default:				// Show nothing
   break;
   }

   /*

   print <paused>

   */

   if((raine_cfg.req_pause_game) && (!(pause_time & 0x20)))

      textout_fast("<Paused>",xoff2+((xxx-(6*8))>>1),yoff2+((yyy-8)>>1),get_white_pen());

}

extern void *old_draw; // dlg_sound
static void DrawNormal(void)
{
   /*

   save screenshots now (before we overwrite the image)

   */
  if (old_draw) return;
   if(raine_cfg.req_save_screen)

      do_save_screen();

   /*

   draw the ingame 'interface' (message list, fps)

   */

   overlay_ingame_interface();

   /*

   hack -- in games with direct mapping (exactly 256 colours),
   we cannot guarantee colour 0 is always black...

   */

   if(display_cfg.scanlines){

      pal[0].r = 0;
      pal[0].g = 0;
      pal[0].b = 0;

   }

   /*

   update host (pc) palette

   */

   #if 1
   update_screen_palette(pal);
   #else
   update_screen_palette(pal_12bit);
   #endif

   /*

   blit image to host (pc) videoram

   */

   raine_fast_blit(BlitSource, screen, xoff2, yoff2, destx, desty, xxx, yyy);

   RefreshBuffers = 0;
}

static BITMAP *pause_buffer;
static int show_fps_mode_store;
static int VSyncStore;

void InitDrawPaused(void)
{
   sa_pause_sound();

   show_fps_mode_store=raine_cfg.show_fps_mode;
   raine_cfg.show_fps_mode=0;
   VSyncStore=display_cfg.vsync;
   display_cfg.vsync=1;

   pause_time = 0;

   raine_cfg.req_pause_scroll = 0;

   pause_buffer = create_bitmap(GameScreen.xfull, GameScreen.yfull);
   blit(BlitSource, pause_buffer, 0, 0, 0, 0, GameScreen.xfull, GameScreen.yfull);

   init_gui_inputs_paused();
}

void EndDrawPaused(void)
{
   init_gui_inputs();

   raine_cfg.show_fps_mode = show_fps_mode_store;

   display_cfg.vsync = VSyncStore;

   destroy_bitmap(pause_buffer);

   sa_unpause_sound();

   reset_ingame_timer();
}

void DrawPaused(void)
{
   cpu_frame_count++;

   /*

   scroll game screen

   */

   if(raine_cfg.req_pause_scroll & 1)
      if(yoff2 > GameScreen.ytop)
	 yoff2--;

   if(raine_cfg.req_pause_scroll & 2)
      if((yoff2 + disp_screen_y) < (GameScreen.ytop + GameScreen.yview))
	 yoff2++;

   if(raine_cfg.req_pause_scroll & 4)
      if(xoff2 > GameScreen.xtop)
	 xoff2--;

   if(raine_cfg.req_pause_scroll & 8)
      if((xoff2 + disp_screen_x) < (GameScreen.xtop + GameScreen.xview))
	 xoff2++;

   raine_cfg.req_pause_scroll = 0;

   blit(pause_buffer, BlitSource, xoff2, yoff2, xoff2, yoff2, xxx, yyy);

   DrawNormal();		// Overlay text interface, blit to screen

   pause_time++;

   if(display_cfg.limit_speed==1){
      while( read_ingame_timer() < cpu_frame_count){
      }
   }
}

// BlitScreen():
// Entry to screen blitting, takes care of pause mode, eagle mode

void BlitScreen(void)
{
      if(!raine_cfg.req_pause_game){
	 DrawNormal();
	 return;
      }
      else{
	 InitDrawPaused();

	 while(raine_cfg.req_pause_game){

	    DrawPaused();

	    update_rjoy_list();

	    update_gui_inputs_paused();

	 }

	 EndDrawPaused();

	 return;

      }
}

void ReClipScreen(void)
{
   // clip x

   if(GameScreen.xview < disp_screen_x){
      destx = (disp_screen_x - GameScreen.xview)>>1;
      xxx = GameScreen.xview;
      xoff2 = GameScreen.xtop;
   }
   else{
      destx = 0;
      xxx = disp_screen_x;
      xoff2 = GameScreen.xtop + ((GameScreen.xview - disp_screen_x)>>1);
   }

   // clip y

   if(GameScreen.yview < disp_screen_y){
      desty = (disp_screen_y - GameScreen.yview)>>1;
      yyy = GameScreen.yview;
      yoff2 = GameScreen.ytop;
   }
   else{
      desty = 0;
      yyy = disp_screen_y;
      yoff2 = GameScreen.ytop + ((GameScreen.yview - disp_screen_y)>>1);
   }
}

void clear_game_screen(int pen)
{
   clear_to_color(GameViewBitmap, pen);
}

static void SetScreenBitmap(int xfull, int yfull, int xtop, int ytop, int xview, int yview)
{
   VIDEO_INFO *vid_info;

   vid_info = current_game->video_info;

   display_cfg.rotate = 0;
   display_cfg.flip = 0;

   if(vid_info->flags & VIDEO_ROTATABLE){

      /*

      check if we use the game rotation

      */

      if(!display_cfg.no_rotate)

	 display_cfg.rotate = VIDEO_ROTATE( vid_info->flags );

      /*

      check if we use the game flipping

      */

      if(!display_cfg.no_flip)

	 display_cfg.flip = VIDEO_FLIP( vid_info->flags );

      /*

      add user rotation

      */

      display_cfg.rotate += display_cfg.user_rotate;
      display_cfg.rotate &= 3;

      /*

      add user flipping

      */

      display_cfg.flip ^= display_cfg.user_flip;

   }

   switch(display_cfg.rotate){
   case 0x00:
      GameScreen.xfull=xfull;
      GameScreen.yfull=yfull;
      GameScreen.xtop =xtop;
      GameScreen.ytop =ytop;
      GameScreen.xview=xview;
      GameScreen.yview=yview;
   break;
   case 0x01:
      GameScreen.xfull=yfull;
      GameScreen.yfull=xfull;
      GameScreen.xtop =ytop;
      GameScreen.ytop =xtop;
      GameScreen.xview=yview;
      GameScreen.yview=xview;
   break;
   case 0x02:
      GameScreen.xfull=xfull;
      GameScreen.yfull=yfull;
      GameScreen.xtop =xtop;
      GameScreen.ytop =ytop;
      GameScreen.xview=xview;
      GameScreen.yview=yview;
   break;
   case 0x03:
      GameScreen.xfull=yfull;
      GameScreen.yfull=xfull;
      GameScreen.xtop =ytop;
      GameScreen.ytop =xtop;
      GameScreen.xview=yview;
      GameScreen.yview=xview;
   break;
   }

   check_tile_rotation();

   disp_screen_y = display_cfg.screen_y;
   disp_screen_x = display_cfg.screen_x;

   /*

   these modes double the image size (it's simpler to consider
   them as halving the display resolution)

   */

   if(is_linear_bitmap(screen) && (gfx_driver->linear)){

      if(display_cfg.pixel_double){

	 if(display_cfg.pixel_double == 1){

	    disp_screen_x /= 2;
	    disp_screen_y /= 2;

	 }
	 else{

	    disp_screen_x /= 2;

	 }

      }
      else{

	 if(display_cfg.bpp == 8){

	    if(display_cfg.eagle_fx){

	       disp_screen_x /= 2;
	       disp_screen_y /= 2;

	    }

	 }

      }

   }

   ReClipScreen();
}

void SetupScreenBitmap(void)
{
   VIDEO_INFO *vid_info;

   vid_info = current_game->video_info;

   SetScreenBitmap(
      vid_info->screen_x + vid_info->border_size + vid_info->border_size,
      vid_info->screen_y + vid_info->border_size + vid_info->border_size,
      vid_info->border_size,
      vid_info->border_size,
      vid_info->screen_x,
      vid_info->screen_y
   );

   GameBitmap = create_bitmap_ex(internal_bpp(display_cfg.bpp), GameScreen.xfull, GameScreen.yfull);
   GameViewBitmap = create_sub_bitmap(GameBitmap, GameScreen.xtop, GameScreen.ytop, GameScreen.xview, GameScreen.yview);

   clear(GameViewBitmap);

   BlitSource = GameBitmap;
   BlitViewSource = GameViewBitmap;

   RefreshBuffers=1;
}

void DestroyScreenBitmap(void)
{
   destroy_bitmap(GameBitmap);
   destroy_bitmap(GameViewBitmap);
}

#include "profile.c"

/******************************************************************************/

#ifdef TRIPLE_BUFFER

#define ALLEGRO_SCROLL_CHANGED

#ifdef ALLEGRO_SCROLL_CHANGED

/*

This version requires modifications to allegro (works in vbe/af and stuff)

*/

static int scroll_async(int x, int y)
{
   if(display_cfg.scanlines)
      return scroll_screen(x,y * 2);
   else
      return scroll_screen(x,y);
}

#else

/*

This version is the mame hack of allegro (works in less modes, because it
is only based on the vesa 1/2/3 code)

*/

static int scroll_async(int x, int y)
{
   int ret, seg;
   long a;
	extern void (*_pm_vesa_scroller)(void); /* in Allegro */
	extern int _mmio_segment;	/* in Allegro */
	#define BYTES_PER_PIXEL(bpp)	 (((int)(bpp) + 7) / 8) /* in Allegro */
	extern __dpmi_regs _dpmi_reg;	/* in Allegro... I think */

   if (_pm_vesa_scroller) {	       /* use protected mode interface? */
      seg = _mmio_segment ? _mmio_segment : _my_ds();

      a = ((x * BYTES_PER_PIXEL(screen->vtable->color_depth)) +
	   (y * ((unsigned long)screen->line[1] - (unsigned long)screen->line[0]))) / 4;

      asm (
	 "  pushw %%es ; "
	 "  movw %w1, %%es ; "         /* set the IO segment */
	 "  call *%0 ; "               /* call the VESA function */
	 "  popw %%es "

      : 			       /* no outputs */

      : "S" (_pm_vesa_scroller),       /* function pointer in esi */
	"a" (seg),                     /* IO segment in eax */
	"b" (0x00),                    /* mode in ebx */
	"c" (a & 0xFFFF),              /* low word of address in ecx */
	"d" (a >> 16)                  /* high word of address in edx */

      : "memory", "%edi", "%cc"        /* clobbers edi and flags */
      );

      ret = 0;
   }
   else {			       /* use a real mode interrupt call */
      _dpmi_reg.x.ax = 0x4F07;
      _dpmi_reg.x.bx = 0;
      _dpmi_reg.x.cx = x;

      if(display_cfg.scanlines == 2)
	 _dpmi_reg.x.dx = y * 2;
      else
	 _dpmi_reg.x.dx = y;

      __dpmi_int(0x10, &_dpmi_reg);
      ret = _dpmi_reg.h.ah;

   }

   return (ret ? -1 : 0);
}

#endif // ALLEGRO_SCROLL_CHANGED

#endif // TRIPLE_BUFFER

/******************************************************************************/
/*									      */
/*			  VIDEO LAYER TOGGLE CONTROLS			      */
/*									      */
/******************************************************************************/

#define MAX_LAYER_INFO	(16)

typedef struct LAYER_INFO
{
   UINT8 enabled;
   UINT8 flip;
   UINT8 keycode;
   char *name;
} LAYER_INFO;

static struct LAYER_INFO layer_info_list[MAX_LAYER_INFO];

static int layer_info_count;

static int key_data[MAX_LAYER_INFO]=
{
   KEY_F5,
   KEY_F6,
   KEY_F7,
   KEY_F8,
   KEY_F9,
   KEY_F10,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
   KEY_F12,
};

void reset_layer_switches(void)
{
   layer_info_count = 0;
}

void check_layer_switches(void)
{
   int ta;

   for(ta=0; ta<layer_info_count; ta++){

      if( key[layer_info_list[ta].keycode] ){

	 if( ! layer_info_list[ta].flip ){

	    layer_info_list[ta].flip = 1;
	    layer_info_list[ta].enabled ^= 1;
	    print_ingame(60, "%s: %01d", layer_info_list[ta].name, layer_info_list[ta].enabled);

	 }

      }
      else{

	 layer_info_list[ta].flip = 0;

      }

   }
}

int add_layer_info(char *name)
{
   int ta;

   if(layer_info_count < MAX_LAYER_INFO){

      ta = layer_info_count;

      layer_info_list[ta].name	  = name;
      layer_info_list[ta].keycode = key_data[ta];
      layer_info_list[ta].enabled = 1;
      layer_info_list[ta].flip	  = 0;
      layer_info_count++;

      return ta;

   }
   else{
      #ifdef RAINE_DEBUG
      print_debug("add_layer_info(): error -- too many layers\n");
      #endif
      return MAX_LAYER_INFO - 1;

   }
}

int check_layer_enabled(int layer)
{
  if(layer<layer_info_count){

      return  layer_info_list[layer].enabled;


   }
   else{

      #ifdef RAINE_DEBUG
      print_debug("check_layer_enabled(): error - incorrectly initialized\n");
      #endif

      return 1;

   }
}

