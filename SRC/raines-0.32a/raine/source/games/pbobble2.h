
void LoadPuzzleBobble2(void);
void ClearPuzzleBobble2(void);

void LoadPuzzleBobble2x(void);
void ClearPuzzleBobble2x(void);

void DrawPuzzleBobble2(void);
void ExecutePuzzleBobble2Frame(void);

void LoadPBobble2Sound(void);
void ClearPBobble2Sound(void);

void DrawPuzzleBobble2Sound(void);
void ExecutePBobble2SoundFrame(void);
