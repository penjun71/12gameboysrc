/******************************************************************************/
/*                                                                            */
/*                     MEGA BLAST (C) 1992 TAITO CORPORATION                  */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "megab.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO mega_blast_dirs[] =
{
   { "mega_blast", },
   { "megab", },
   { "megabl", },
   { NULL, },
};

static struct ROM_INFO mega_blast_roms[] =
{
   {       "c11-01", 0x00080000, 0xfd1ea532, 0, 0, 0, },
   {       "c11-02", 0x00080000, 0x451cc187, 0, 0, 0, },
   {       "c11-03", 0x00080000, 0x46718c7a, 0, 0, 0, },
   {       "c11-04", 0x00080000, 0x663f33cc, 0, 0, 0, },
   {       "c11-05", 0x00080000, 0x733e6d8e, 0, 0, 0, },
   {       "c11-06", 0x00020000, 0x7c249894, 0, 0, 0, },
   {       "c11-07", 0x00020000, 0x11d228b6, 0, 0, 0, },
   {       "c11-08", 0x00020000, 0xa79d4dca, 0, 0, 0, },
   {       "c11-11", 0x00020000, 0x263ecbf9, 0, 0, 0, },
   {       "c11-12", 0x00010000, 0xb11094f1, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO mega_blast_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03C00E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03C00E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03C00E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03C00E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x03C00E, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x03C004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x03C004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x03C004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x03C004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x03C004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x03C004, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x03C00E, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x03C006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x03C006, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x03C006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x03C006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x03C006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x03C006, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_mega_blast_0[] =
{
   { "Cabinet",               0x01, 0x02 },
   { "Table",                 0x01, 0x00 },
   { "Upright",               0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_mega_blast_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Bonus / K=10,000",      0x0c, 0x04 },
   { "10k, 110K, 210K, 310K", 0x0c, 0x00 },
   { "20k, 220K, 420K, 620K", 0x08, 0x00 },
   { "15K, 145K, 365K, 515K", 0x04, 0x00 },
   { "No Bonus Lives",        0x00, 0x00 },
   { "Lives",                 0x30, 0x04 },
   { "3",                     0x30, 0x00 },
   { "4",                     0x20, 0x00 },
   { "1",                     0x10, 0x00 },
   { "2",                     0x00, 0x00 },
   { "Control Panel",         0x40, 0x02 },
   { "Double",                0x40, 0x00 },
   { "Single",                0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO mega_blast_dsw[] =
{
   { 0x03C000, 0xFF, dsw_data_mega_blast_0 },
   { 0x03C002, 0xFF, dsw_data_mega_blast_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_mega_blast_0[] =
{
   { "Taito America",         0x01 },
   { "Taito Japan",           0x02 },
   { NULL,                    0    },
};

static struct ROMSW_INFO mega_blast_romsw[] =
{
   { 0x07FFFF, 0x02, romsw_data_mega_blast_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO mega_blast_video =
{
   DrawMegaBlast,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_mega_blast =
{
   mega_blast_dirs,
   mega_blast_roms,
   mega_blast_inputs,
   mega_blast_dsw,
   mega_blast_romsw,

   LoadMegaBlast,
   ClearMegaBlast,
   &mega_blast_video,
   ExecuteMegaBlastFrame,
   "megab",
   "Mega Blast",
   "メガブラスト",
   COMPANY_ID_TAITO,
   "C11",
   1989,
   taito_ym2610_sound,
   GAME_SHOOT,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

void LoadMegaBlast(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(GFX=AllocateMem(0x304000))) return;

   GFX_SPR	= GFX+0x100000;

   tb=0x100000;
   if(!load_rom("c11-03", RAM, 0x80000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x80000;ta+=4){
      GFX[tb+0]=RAM[ta+0]&15;
      GFX[tb+1]=RAM[ta+0]>>4;
      GFX[tb+4]=RAM[ta+1]&15;
      GFX[tb+5]=RAM[ta+1]>>4;
      GFX[tb+8]=RAM[ta+2]&15;
      GFX[tb+9]=RAM[ta+2]>>4;
      GFX[tb+12]=RAM[ta+3]&15;
      GFX[tb+13]=RAM[ta+3]>>4;
      tb+=16;
   }
   tb=0x100000;
   if(!load_rom("c11-04", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta+=4){
      GFX[tb+2]=RAM[ta+0]&15;
      GFX[tb+3]=RAM[ta+0]>>4;
      GFX[tb+6]=RAM[ta+1]&15;
      GFX[tb+7]=RAM[ta+1]>>4;
      GFX[tb+10]=RAM[ta+2]&15;
      GFX[tb+11]=RAM[ta+2]>>4;
      GFX[tb+14]=RAM[ta+3]&15;
      GFX[tb+15]=RAM[ta+3]>>4;
      tb+=16;
   }
   tb=0;
   if(!load_rom("c11-05", RAM, 0x80000)) return;		// 8x8 BG TILES
   for(ta=0;ta<0x80000;ta+=2){
      GFX[tb+3]=RAM[ta+0]&15;
      GFX[tb+2]=RAM[ta+0]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+0]=RAM[ta+1]>>4;
      tb+=4;
   }

   RAMSize=0x40000+0x10000;

   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(ROM=AllocateMem(0x80000))) return;

   if(!load_rom("c11-07", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("c11-08", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("c11-06", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("c11-11", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("c11-12", Z80ROM, 0x10000)) return;		// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x100000))) return;
   if(!load_rom("c11-02",PCMROM,0x80000)) return;		// ADPCM A rom
   if(!load_rom("c11-01",PCMROM+0x80000,0x80000)) return;	// ADPCM B rom
   YM2610SetBuffers(PCMROM, PCMROM+0x80000, 0x080000, 0x080000);

   AddTaitoYM2610(0x023A, 0x01BA, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x18000;
   RAM_SCROLL = RAM+0x3C100;
   RAM_OBJECT = RAM+0x10000;
   RAM_INPUT  = RAM+0x3C000;
   GFX_FG0    = GFX+0x300000;

   ROM[0x00628]=0x4E;
   ROM[0x00629]=0x71;
   ROM[0x0062A]=0x4E;
   ROM[0x0062B]=0x71;

   ROM[0x0770]=0x13;		// move.b #$00,$AA0000
   ROM[0x0771]=0xFC;		// Speed Hack
   ROM[0x0772]=0x00;
   ROM[0x0773]=0x00;
   ROM[0x0774]=0x00;
   ROM[0x0775]=0xAA;
   ROM[0x0776]=0x00;
   ROM[0x0777]=0x00;

   ROM[0x0778]=0x60;
   ROM[0x0779]=0x100-(6+10);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x3A000, 0x100, 0x10, 0x1000);

   GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x4000);

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x3FFF;
   tc0100scn[0].layer[0].scr_x	=16;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x3FFF;
   tc0100scn[0].layer[1].scr_x	=16;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=1;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=16;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);
   tc0100scn_0_copy_gfx_fg0(ROM+0x10FC2, 0x1000);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x1FFF;
   tc0200obj.ofs_x	= -80;
   tc0200obj.ofs_y	= -16;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x600000, 0x61FFFF, NULL, RAM+0x018000);			// SCREEN RAM
   AddReadByte(0x120000, 0x12000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x100000, 0x100003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x180000, 0x180FFF, NULL, RAM+0x038000);			// C-CHIP (HACKED?)
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x600000, 0x61FFFF, NULL, RAM+0x018000);			// SCREEN RAM
   AddReadWord(0x800000, 0x807FFF, NULL, RAM_OBJECT);			// SPRITE RAM
   AddReadWord(0x300000, 0x301FFF, NULL, RAM+0x03A000);			// COLOR RAM
   AddReadWord(0x120000, 0x12000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x600000, 0x61FFFF, NULL, RAM+0x018000);		// SCREEN RAM
   AddWriteByte(0x800000, 0x807FFF, NULL, RAM_OBJECT);			// SPRITE RAM
   AddWriteByte(0x400000, 0x4000FF, NULL, RAM+0x039000);		// ???
   AddWriteByte(0x100000, 0x100003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x180000, 0x180FFF, NULL, RAM+0x038000);		// C-CHIP (HACKED?)
   AddWriteByte(0x120000, 0x12000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x600000, 0x61FFFF, NULL, RAM+0x018000);		// SCREEN RAM
   AddWriteWord(0x800000, 0x807FFF, NULL, RAM_OBJECT);			// SPRITE RAM
   AddWriteWord(0x300000, 0x301FFF, NULL, RAM+0x03A000);		// COLOR RAM
   AddWriteWord(0x620000, 0x6200FF, NULL, RAM+0x03C100);		// SCROLL RAM
   AddWriteWord(0x400000, 0x4000FF, NULL, RAM+0x039000);		// ???
   AddWriteWord(0x120000, 0x12000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearMegaBlast(void)
{
   RemoveTaitoYM2610();
}

void ExecuteMegaBlastFrame(void)
{
   RAM[0x38802]=0x01;

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 5);
   cpu_interrupt(CPU_68K_0, 6);

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawMegaBlast(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // OBJECT
   // ------

   render_tc0200obj_mapped_b();

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);
}
