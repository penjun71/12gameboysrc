/******************************************************************************/
/*                                                                            */
/*                        LAME SETA GAMES (C) 19xx SETA                       */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "seta.h"
#include "setax1.h"

static struct DIR_INFO meta_fox_dirs[] =
{
   { "meta_fox", },
   { "metafox", },
   { NULL, },
};

static struct ROM_INFO meta_fox_roms[] =
{
   {     "p1003161", 0x00040000, 0x4fd6e6a1, 0, 0, 0, },
   {     "p1004162", 0x00040000, 0xb6356c9a, 0, 0, 0, },
   {     "p1006163", 0x00040000, 0x80f69c7c, 0, 0, 0, },
   {     "p1007164", 0x00040000, 0xd137e1a3, 0, 0, 0, },
   {     "p1008165", 0x00040000, 0x57494f2b, 0, 0, 0, },
   {     "p1009166", 0x00040000, 0x8344afd2, 0, 0, 0, },
   {     "up001001", 0x00010000, 0x0db7a505, 0, 0, 0, },
   {     "up001002", 0x00010000, 0xce91c987, 0, 0, 0, },
   {     "up001005", 0x00002000, 0x2ac5e3e3, 0, 0, 0, },
   {     "up001010", 0x00080000, 0xbfbab472, 0, 0, 0, },
   {     "up001011", 0x00080000, 0x26cea381, 0, 0, 0, },
   {     "up001012", 0x00080000, 0xfed2c5f9, 0, 0, 0, },
   {     "up001013", 0x00080000, 0xadabf9ea, 0, 0, 0, },
   {     "up001014", 0x00080000, 0xfca6315e, 0, 0, 0, },
   {     "up001015", 0x00080000, 0x2e20e39f, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_meta_fox =
{
   meta_fox_dirs,
   meta_fox_roms,
   NULL,
   NULL,
   NULL,

   LoadMetaFox,
   ClearMetaFox,
   DrawMetaFox,
   ExecuteMetaFoxFrame,
   "MetaFox",
   "Meta Fox",
   NULL,
   COMPANY_ID_SETA,
};

static struct DIR_INFO twin_eagle_dirs[] =
{
   { "twin_eagle", },
   { "twineagl", },
   { NULL, },
};

static struct ROM_INFO twin_eagle_roms[] =
{
   {        "ua2-1", 0x00080000, 0x5c3fe531, 0, 0, 0, },
   {       "ua2-10", 0x00080000, 0x5bbe1f56, 0, 0, 0, },
   {       "ua2-11", 0x00080000, 0x624e6057, 0, 0, 0, },
   {       "ua2-12", 0x00080000, 0x3068ff64, 0, 0, 0, },
   {        "ua2-2", 0x00002000, 0x783ca84e, 0, 0, 0, },
   {        "ua2-3", 0x00040000, 0x1124417a, 0, 0, 0, },
   {        "ua2-4", 0x00040000, 0x8b7532d6, 0, 0, 0, },
   {        "ua2-5", 0x00040000, 0x6e450d28, 0, 0, 0, },
   {        "ua2-6", 0x00040000, 0x99d8dbba, 0, 0, 0, },
   {        "ua2-7", 0x00080000, 0xfce56907, 0, 0, 0, },
   {        "ua2-8", 0x00080000, 0x7d3a8d73, 0, 0, 0, },
   {        "ua2-9", 0x00080000, 0xa451eae9, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_twin_eagle =
{
   twin_eagle_dirs,
   twin_eagle_roms,
   NULL,
   NULL,
   NULL,

   LoadTwinEagle,
   ClearTwinEagle,
   DrawMetaFox,
   ExecuteMetaFoxFrame,
   "twineagl",
   "Twin Eagle",
   NULL,
   COMPANY_ID_SETA,
};

static struct DIR_INFO us_classic_golf_dirs[] =
{
   { "us_classic_golf", },
   { "usclgolf", },
   { NULL, },
};

static struct ROM_INFO us_classic_golf_roms[] =
{
   { "ue001005.132", 0x00080000, 0xc5fea37c, 0, 0, 0, },
   { "ue001006.116", 0x00080000, 0xa6ab6ef4, 0, 0, 0, },
   { "ue001007.117", 0x00080000, 0xb48a885c, 0, 0, 0, },
   { "ue001008.118", 0x00080000, 0x5947d9b5, 0, 0, 0, },
   { "ue001009.119", 0x00080000, 0xdc065204, 0, 0, 0, },
   { "ue001010.120", 0x00080000, 0xdd683031, 0, 0, 0, },
   { "ue001011.121", 0x00080000, 0x0e27bc49, 0, 0, 0, },
   { "ue001012.122", 0x00080000, 0x961dfcdc, 0, 0, 0, },
   { "ue001013.123", 0x00080000, 0x03e9eb79, 0, 0, 0, },
   { "ue001014.124", 0x00080000, 0x9576ace7, 0, 0, 0, },
   { "ue001015.125", 0x00080000, 0x631d6eb1, 0, 0, 0, },
   { "ue001016.126", 0x00080000, 0xf44a8686, 0, 0, 0, },
   { "ue001017.127", 0x00080000, 0x7f568258, 0, 0, 0, },
   { "ue001018.128", 0x00080000, 0x4bd98f23, 0, 0, 0, },
   { "ue001019.129", 0x00080000, 0x6d9f5a33, 0, 0, 0, },
   { "ue001020.130", 0x00080000, 0xbc07403f, 0, 0, 0, },
   { "ue001021.131", 0x00080000, 0x98c03efd, 0, 0, 0, },
   { "ue002u61.004", 0x00040000, 0x476e9f60, 0, 0, 0, },
   {   "ue2000.u14", 0x00020000, 0x69454bc2, 0, 0, 0, },
   {   "ue2001.u20", 0x00020000, 0x18b41421, 0, 0, 0, },
   {   "ue2002.u22", 0x00020000, 0xa7bbe248, 0, 0, 0, },
   {   "ue2003.u30", 0x00020000, 0x29601906, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_us_classic_golf =
{
   us_classic_golf_dirs,
   us_classic_golf_roms,
   NULL,
   NULL,
   NULL,

   LoadUSClassicGolf,
   ClearUSClassicGolf,
   DrawMetaFox,
   ExecuteMetaFoxFrame,
   "USClGolf",
   "US Classic Golf",
   NULL,
   COMPANY_ID_SETA,
};

static struct DIR_INFO zing_zing_zip_dirs[] =
{
   { "zing_zing_zip", },
   { "zingzip", },
   { NULL, },
};

static struct ROM_INFO zing_zing_zip_roms[] =
{
   {     "uy001001", 0x00040000, 0x1a1687ec, 0, 0, 0, },
   {     "uy001002", 0x00040000, 0x62e3b0c4, 0, 0, 0, },
   {     "uy001007", 0x00080000, 0xec5b3ab9, 0, 0, 0, },
   {     "uy001008", 0x00200000, 0x0d07d34b, 0, 0, 0, },
   {     "uy001010", 0x00200000, 0x0129408a, 0, 0, 0, },
   {     "uy001015", 0x00080000, 0x4aac128e, 0, 0, 0, },
   {     "uy001016", 0x00080000, 0x46e4a7d8, 0, 0, 0, },
   {     "uy001017", 0x00080000, 0xd2cda2eb, 0, 0, 0, },
   {     "uy001018", 0x00080000, 0x3d30229a, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_zing_zing_zip =
{
   zing_zing_zip_dirs,
   zing_zing_zip_roms,
   NULL,
   NULL,
   NULL,

   LoadZingZingZip,
   ClearZingZingZip,
   DrawMetaFox,
   ExecuteMetaFoxFrame,
   "ZingZip",
   "Zing Zing Zip",
   NULL,
   COMPANY_ID_SETA,
};

static UINT8 seta_gfx_code[0x100]=
{
   0x30,0x20,0x10,0x00,0x31,0x21,0x11,0x01,0x32,0x22,0x12,0x02,0x33,0x23,0x13,0x03,
   0x34,0x24,0x14,0x04,0x35,0x25,0x15,0x05,0x36,0x26,0x16,0x06,0x37,0x27,0x17,0x07,
   0x70,0x60,0x50,0x40,0x71,0x61,0x51,0x41,0x72,0x62,0x52,0x42,0x73,0x63,0x53,0x43,
   0x74,0x64,0x54,0x44,0x75,0x65,0x55,0x45,0x76,0x66,0x56,0x46,0x77,0x67,0x57,0x47,
   0xB0,0xA0,0x90,0x80,0xB1,0xA1,0x91,0x81,0xB2,0xA2,0x92,0x82,0xB3,0xA3,0x93,0x83,
   0xB4,0xA4,0x94,0x84,0xB5,0xA5,0x95,0x85,0xB6,0xA6,0x96,0x86,0xB7,0xA7,0x97,0x87,
   0xF0,0xE0,0xD0,0xC0,0xF1,0xE1,0xD1,0xC1,0xF2,0xE2,0xD2,0xC2,0xF3,0xE3,0xD3,0xC3,
   0xF4,0xE4,0xD4,0xC4,0xF5,0xE5,0xD5,0xC5,0xF6,0xE6,0xD6,0xC6,0xF7,0xE7,0xD7,0xC7,
   0x38,0x28,0x18,0x08,0x39,0x29,0x19,0x09,0x3A,0x2A,0x1A,0x0A,0x3B,0x2B,0x1B,0x0B,
   0x3C,0x2C,0x1C,0x0C,0x3D,0x2D,0x1D,0x0D,0x3E,0x2E,0x1E,0x0E,0x3F,0x2F,0x1F,0x0F,
   0x78,0x68,0x58,0x48,0x79,0x69,0x59,0x49,0x7A,0x6A,0x5A,0x4A,0x7B,0x6B,0x5B,0x4B,
   0x7C,0x6C,0x5C,0x4C,0x7D,0x6D,0x5D,0x4D,0x7E,0x6E,0x5E,0x4E,0x7F,0x6F,0x5F,0x4F,
   0xB8,0xA8,0x98,0x88,0xB9,0xA9,0x99,0x89,0xBA,0xAA,0x9A,0x8A,0xBB,0xAB,0x9B,0x8B,
   0xBC,0xAC,0x9C,0x8C,0xBD,0xAD,0x9D,0x8D,0xBE,0xAE,0x9E,0x8E,0xBF,0xAF,0x9F,0x8F,
   0xF8,0xE8,0xD8,0xC8,0xF9,0xE9,0xD9,0xC9,0xFA,0xEA,0xDA,0xCA,0xFB,0xEB,0xDB,0xCB,
   0xFC,0xEC,0xDC,0xCC,0xFD,0xED,0xDD,0xCD,0xFE,0xEE,0xDE,0xCE,0xFF,0xEF,0xDF,0xCF,
};

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

/*

6502 Info
---------

- I really hate this chip

- 0xFFFA = [7117] NMI Vector
- 0xFFFC = [7000] PC Reset Vector
- 0xFFFE = [70E1] Interrupt Vector

Seta 6502 Info
--------------

- Due to some of the opcodes in the rom, it seems to be a 6510

E000-FFFF = ROM?
6000-7FFF = ROM MIRROR?

*/

UINT8 ReadROM(UINT16 offset)
{
   return M6502ROM[offset&0x7FFF];
}

void WriteROM(UINT16 offset, UINT8 data)
{
   M6502ROM[offset&0x7FFF] = data;
}

void LoadMetaFox(void)
{
   int ta,tb,tc,td;

   RAMSize=0x40000+0x10000;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0xA0000))) return;
   if(!(GFX=AllocateMem(0x200000+0x400000))) return;

   GFX_SPR = GFX+0x000000;

   if(!load_rom("P1006163", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]=((tc&0x80)>>7)<<0;
      GFX[tb+1]=((tc&0x40)>>6)<<0;
      GFX[tb+2]=((tc&0x20)>>5)<<0;
      GFX[tb+3]=((tc&0x10)>>4)<<0;
      GFX[tb+4]=((tc&0x08)>>3)<<0;
      GFX[tb+5]=((tc&0x04)>>2)<<0;
      GFX[tb+6]=((tc&0x02)>>1)<<0;
      GFX[tb+7]=((tc&0x01)>>0)<<0;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("P1007164", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<1;
      GFX[tb+1]|=((tc&0x40)>>6)<<1;
      GFX[tb+2]|=((tc&0x20)>>5)<<1;
      GFX[tb+3]|=((tc&0x10)>>4)<<1;
      GFX[tb+4]|=((tc&0x08)>>3)<<1;
      GFX[tb+5]|=((tc&0x04)>>2)<<1;
      GFX[tb+6]|=((tc&0x02)>>1)<<1;
      GFX[tb+7]|=((tc&0x01)>>0)<<1;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("P1008165", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<2;
      GFX[tb+1]|=((tc&0x40)>>6)<<2;
      GFX[tb+2]|=((tc&0x20)>>5)<<2;
      GFX[tb+3]|=((tc&0x10)>>4)<<2;
      GFX[tb+4]|=((tc&0x08)>>3)<<2;
      GFX[tb+5]|=((tc&0x04)>>2)<<2;
      GFX[tb+6]|=((tc&0x02)>>1)<<2;
      GFX[tb+7]|=((tc&0x01)>>0)<<2;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("P1009166", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<3;
      GFX[tb+1]|=((tc&0x40)>>6)<<3;
      GFX[tb+2]|=((tc&0x20)>>5)<<3;
      GFX[tb+3]|=((tc&0x10)>>4)<<3;
      GFX[tb+4]|=((tc&0x08)>>3)<<3;
      GFX[tb+5]|=((tc&0x04)>>2)<<3;
      GFX[tb+6]|=((tc&0x02)>>1)<<3;
      GFX[tb+7]|=((tc&0x01)>>0)<<3;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }

   if(!load_rom("UP001010", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UP001011", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x400000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UP001012", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UP001013", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x400000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }

   if(!load_rom("P1003161", RAM+0x00000, 0x40000)) return;
   if(!load_rom("UP001002", RAM+0x40000, 0x10000)) return;
   for(ta=0;ta<0x50000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("P1004162", RAM+0x00000, 0x40000)) return;
   if(!load_rom("UP001001", RAM+0x40000, 0x10000)) return;
   for(ta=0;ta<0x50000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
/*
   Rotate16x16(GFX+0x200000,0x4000);
   Flip16x16_Y(GFX+0x200000,0x4000);
*/
   Rotate16x16(GFX+0x000000,0x2000);
   Flip16x16_X(GFX+0x000000,0x2000);

   /* ----- M6510/M6502 ----- */

   M6502ROM = RAM+0x40000;

   memset(M6502ROM,0xFF,0x10000);

   if(!load_rom("UP001005", M6502ROM+0x6000, 0x2000)) return;

   M6502ROM[0x7239]=0x8D;	// STA $FFFC
   M6502ROM[0x723A]=0xFC;
   M6502ROM[0x723B]=0xFF;

   SetStopM6502Mode2(0x720D);

   // Mirror

   memcpy(M6502ROM+0x8000,M6502ROM,0x8000);

   // Setup Z80 memory map
   // --------------------

   AddM6502AROMBase(M6502ROM);

   AddM6502AReadByte(0x0000, 0xFFFF, ReadROM,                     NULL); // Testing
   AddM6502AReadByte(0x0000, 0xFFFF, DefBadReadM6502,             NULL);
   AddM6502AReadByte(    -1,     -1, NULL,                        NULL);

   AddM6502AWriteByte(0x0000, 0xFFF0, WriteROM,                   NULL); // Testing
   AddM6502AWriteByte(0xFFFC, 0xFFFC, StopM6502Mode2,             NULL);
   AddM6502AWriteByte(0x0000, 0xFFFF, DefBadWriteM6502,           NULL);
   AddM6502AWriteByte(    -1,     -1, NULL,                       NULL);

   AddM6502AInit();

   /* ----------------------- */

   memset(RAM+0x00000,0x00,0x40000);

   // Speed Hack

   WriteLong68k(&ROM[0x00D0A],0x4EF80300);
   WriteLong68k(&ROM[0x00300],0x13FC0000);
   WriteLong68k(&ROM[0x00304],0x00AA0000);
   WriteWord68k(&ROM[0x00308],0x6100-10);

   // Fix CP ERROR

   WriteLong68k(&ROM[0x8AA26],0x4E714E71);
   WriteLong68k(&ROM[0x8AA76],0x4E714E71);
   WriteLong68k(&ROM[0x8AAC6],0x4E714E71);
   WriteLong68k(&ROM[0x8AB08],0x4E714E71);

   InitPaletteMap(RAM+0x15000, 0x20, 0x10, 0x8000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);

   //GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x4000);

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   // Init seta_x1 emulation
   // ----------------------

   seta_x1.RAM_A	= RAM+0x0C000;
   seta_x1.RAM_B	= RAM+0x16000;
   seta_x1.GFX		= GFX_SPR;
   seta_x1.MASK		= GFX_SPR_SOLID;
   seta_x1.bmp_x	= 32;
   seta_x1.bmp_y	= 32;
   seta_x1.bmp_w	= 240;
   seta_x1.bmp_h	= 356;
   seta_x1.tile_mask	= 0x1FFF;
   seta_x1.scr_x	= 0;
   seta_x1.scr_y	= 0;

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0xA0000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x09FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x09FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0xF00000, 0xF03FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x100000, 0x103FFF, NULL, RAM+0x004000);			// ? RAM
   AddReadByte(0x900000, 0x903FFF, NULL, RAM+0x008000);			// BG0 RAM
   AddReadByte(0xD00000, 0xD00FFF, NULL, RAM+0x016000);			// ? RAM
   AddReadByte(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);			// OBJECT RAM
   AddReadByte(0x21C000, 0x21FFFF, NULL, RAM+0x010000);			// CP
   AddReadByte(0xB00000, 0xB00FFF, NULL, RAM+0x014000);			// COM RAM
   AddReadByte(0x700000, 0x7003FF, NULL, RAM+0x015000);			// COLOUR RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x09FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0xF00000, 0xF03FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x100000, 0x103FFF, NULL, RAM+0x004000);			// ? RAM
   AddReadWord(0x900000, 0x903FFF, NULL, RAM+0x008000);			// BG0 RAM
   AddReadWord(0xD00000, 0xD00FFF, NULL, RAM+0x016000);			// ? RAM
   AddReadWord(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);			// OBJECT RAM
   AddReadWord(0x21C000, 0x21FFFF, NULL, RAM+0x010000);			// CP
   AddReadWord(0xB00000, 0xB00FFF, NULL, RAM+0x014000);			// COM RAM
   AddReadWord(0x700000, 0x7003FF, NULL, RAM+0x015000);			// COLOUR RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0xF00000, 0xF03FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x100000, 0x103FFF, NULL, RAM+0x004000);		// ? RAM
   AddWriteByte(0x900000, 0x903FFF, NULL, RAM+0x008000);		// BG0 RAM
   AddWriteByte(0xD00000, 0xD00FFF, NULL, RAM+0x016000);		// ? RAM
   AddWriteByte(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);		// OBJECT RAM
   AddWriteByte(0x21C000, 0x21FFFF, NULL, RAM+0x010000);		// CP
   AddWriteByte(0xB00000, 0xB00FFF, NULL, RAM+0x014000);		// COM RAM
   AddWriteByte(0x700000, 0x7003FF, NULL, RAM+0x015000);		// COLOUR RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0xF00000, 0xF03FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x100000, 0x103FFF, NULL, RAM+0x004000);		// ? RAM
   AddWriteWord(0x900000, 0x903FFF, NULL, RAM+0x008000);		// BG0 RAM
   AddWriteWord(0xD00000, 0xD00FFF, NULL, RAM+0x016000);		// ? RAM
   AddWriteWord(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);		// OBJECT RAM
   AddWriteWord(0x21C000, 0x21FFFF, NULL, RAM+0x010000);		// CP
   AddWriteWord(0xB00000, 0xB00FFF, NULL, RAM+0x014000);		// COM RAM
   AddWriteWord(0x700000, 0x7003FF, NULL, RAM+0x015000);		// COLOUR RAM
   AddWriteWord(0x800000, 0x80000F, NULL, RAM+0x017000);		// SCROLL RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearMetaFox(void)
{
   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x0A0000,1);
      save_debug("RAM.bin",RAM,0x080000,1);
      //save_debug("GFX.bin",GFX,0x600000,0);
   #endif
}

void LoadTwinEagle(void)
{
   int ta,tb,tc,td;

   RAMSize=0x40000+0x10000;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0x80000))) return;
   if(!(GFX=AllocateMem(0x200000+0x400000))) return;

   GFX_SPR = GFX+0x000000;

   if(!load_rom("UA2-4", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]=((tc&0x80)>>7)<<0;
      GFX[tb+1]=((tc&0x40)>>6)<<0;
      GFX[tb+2]=((tc&0x20)>>5)<<0;
      GFX[tb+3]=((tc&0x10)>>4)<<0;
      GFX[tb+4]=((tc&0x08)>>3)<<0;
      GFX[tb+5]=((tc&0x04)>>2)<<0;
      GFX[tb+6]=((tc&0x02)>>1)<<0;
      GFX[tb+7]=((tc&0x01)>>0)<<0;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("UA2-3", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<1;
      GFX[tb+1]|=((tc&0x40)>>6)<<1;
      GFX[tb+2]|=((tc&0x20)>>5)<<1;
      GFX[tb+3]|=((tc&0x10)>>4)<<1;
      GFX[tb+4]|=((tc&0x08)>>3)<<1;
      GFX[tb+5]|=((tc&0x04)>>2)<<1;
      GFX[tb+6]|=((tc&0x02)>>1)<<1;
      GFX[tb+7]|=((tc&0x01)>>0)<<1;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("UA2-6", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<2;
      GFX[tb+1]|=((tc&0x40)>>6)<<2;
      GFX[tb+2]|=((tc&0x20)>>5)<<2;
      GFX[tb+3]|=((tc&0x10)>>4)<<2;
      GFX[tb+4]|=((tc&0x08)>>3)<<2;
      GFX[tb+5]|=((tc&0x04)>>2)<<2;
      GFX[tb+6]|=((tc&0x02)>>1)<<2;
      GFX[tb+7]|=((tc&0x01)>>0)<<2;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("UA2-5", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<3;
      GFX[tb+1]|=((tc&0x40)>>6)<<3;
      GFX[tb+2]|=((tc&0x20)>>5)<<3;
      GFX[tb+3]|=((tc&0x10)>>4)<<3;
      GFX[tb+4]|=((tc&0x08)>>3)<<3;
      GFX[tb+5]|=((tc&0x04)>>2)<<3;
      GFX[tb+6]|=((tc&0x02)>>1)<<3;
      GFX[tb+7]|=((tc&0x01)>>0)<<3;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }

   if(!load_rom("UA2-8", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UA2-10", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x400000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UA2-7", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UA2-9", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x400000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }

   if(!load_rom("UA2-1", ROM+0x00000, 0x80000)) return;

/*
   Rotate16x16(GFX+0x200000,0x4000);
   Flip16x16_Y(GFX+0x200000,0x4000);
*/
   Rotate16x16(GFX+0x000000,0x2000);
   Flip16x16_X(GFX+0x000000,0x2000);

   /* ----- M6510/M6502 ----- */

   M6502ROM = RAM+0x40000;

   memset(M6502ROM,0xFF,0x10000);

   if(!load_rom("UA2-2", M6502ROM+0x6000, 0x2000)) return;

   M6502ROM[0x7239]=0x8D;	// STA $FFFC
   M6502ROM[0x723A]=0xFC;
   M6502ROM[0x723B]=0xFF;

   SetStopM6502Mode2(0x720D);

   // Mirror

   memcpy(M6502ROM+0x8000,M6502ROM,0x8000);

   // Setup Z80 memory map
   // --------------------

   AddM6502AROMBase(M6502ROM);

   AddM6502AReadByte(0x0000, 0xFFFF, ReadROM,                     NULL); // Testing
   AddM6502AReadByte(0x0000, 0xFFFF, DefBadReadM6502,             NULL);
   AddM6502AReadByte(    -1,     -1, NULL,                        NULL);

   AddM6502AWriteByte(0x0000, 0xFFF0, WriteROM,                   NULL); // Testing
   AddM6502AWriteByte(0xFFFC, 0xFFFC, StopM6502Mode2,             NULL);
   AddM6502AWriteByte(0x0000, 0xFFFF, DefBadWriteM6502,           NULL);
   AddM6502AWriteByte(    -1,     -1, NULL,                       NULL);

   AddM6502AInit();

   /* ----------------------- */

   memset(RAM+0x00000,0x00,0x40000);

   // Checksum

   WriteWord68k(&ROM[0x0033E],0x6000);

   // Fix ERROR X

   WriteWord68k(&ROM[0x00370],0x6000);

   // Speed Hack

   WriteLong68k(&ROM[0x04108],0x4EF800C0);	//

   WriteWord68k(&ROM[0x000C0],0x4A39);		// tst.b $FFCDB4
   WriteLong68k(&ROM[0x000C2],0x00FFCDB4);	//

   WriteWord68k(&ROM[0x000C6],0x6600+4);	// bne.s <stopcpu>
   WriteLong68k(&ROM[0x000C8],0x4EF84110);	//

   WriteLong68k(&ROM[0x000CC],0x13FC0000);	//
   WriteLong68k(&ROM[0x000D0],0x00AA0000);	//
   WriteLong68k(&ROM[0x000D4],0x4EF800C0);	//

   InitPaletteMap(RAM+0x15000, 0x20, 0x10, 0x8000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);

   //GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x4000);

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   // Init seta_x1 emulation
   // ----------------------

   seta_x1.RAM_A	= RAM+0x0C000;
   seta_x1.RAM_B	= RAM+0x16000;
   seta_x1.GFX		= GFX_SPR;
   seta_x1.MASK		= GFX_SPR_SOLID;
   seta_x1.bmp_x	= 32;
   seta_x1.bmp_y	= 32;
   seta_x1.bmp_w	= 240;
   seta_x1.bmp_h	= 356;
   seta_x1.tile_mask	= 0x1FFF;
   seta_x1.scr_x	= 0;
   seta_x1.scr_y	= 0;

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0xFFC000, 0xFFFFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x100000, 0x103FFF, NULL, RAM+0x004000);			// PCM RAM
   AddReadByte(0x900000, 0x903FFF, NULL, RAM+0x008000);			// BG0 RAM    [V-RAM]
   AddReadByte(0xD00000, 0xD00FFF, NULL, RAM+0x016000);			// OBJECT RAM [VPOS]
   AddReadByte(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);			// OBJECT RAM
   AddReadByte(0x21C000, 0x21FFFF, NULL, RAM+0x010000);			// CP
   AddReadByte(0xB00000, 0xB00FFF, NULL, RAM+0x014000);			// COM RAM
   AddReadByte(0x700000, 0x7003FF, NULL, RAM+0x015000);			// COLOUR RAM
   AddReadByte(0xF00000, 0xF000FF, NULL, RAM+0x017100);			// SOME EXTRA RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0xFFC000, 0xFFFFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x100000, 0x103FFF, NULL, RAM+0x004000);			// PCM RAM
   AddReadWord(0x900000, 0x903FFF, NULL, RAM+0x008000);			// BG0 RAM    [V-RAM]
   AddReadWord(0xD00000, 0xD00FFF, NULL, RAM+0x016000);			// OBJECT RAM [VPOS]
   AddReadWord(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);			// OBJECT RAM
   AddReadWord(0x21C000, 0x21FFFF, NULL, RAM+0x010000);			// CP
   AddReadWord(0xB00000, 0xB00FFF, NULL, RAM+0x014000);			// COM RAM
   AddReadWord(0x700000, 0x7003FF, NULL, RAM+0x015000);			// COLOUR RAM
   AddReadWord(0xF00000, 0xF000FF, NULL, RAM+0x017100);			// SOME EXTRA RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0xFFC000, 0xFFFFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x100000, 0x103FFF, NULL, RAM+0x004000);		// PCM RAM
   AddWriteByte(0x900000, 0x903FFF, NULL, RAM+0x008000);		// BG0 RAM    [V-RAM]
   AddWriteByte(0xD00000, 0xD00FFF, NULL, RAM+0x016000);		// OBJECT RAM [VPOS]
   AddWriteByte(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);		// OBJECT RAM
   AddWriteByte(0x21C000, 0x21FFFF, NULL, RAM+0x010000);		// CP
   AddWriteByte(0xB00000, 0xB00FFF, NULL, RAM+0x014000);		// COM RAM
   AddWriteByte(0x700000, 0x7003FF, NULL, RAM+0x015000);		// COLOUR RAM
   AddWriteByte(0xF00000, 0xF000FF, NULL, RAM+0x017100);		// SOME EXTRA RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0xFFC000, 0xFFFFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x100000, 0x103FFF, NULL, RAM+0x004000);		// PCM RAM
   AddWriteWord(0x900000, 0x903FFF, NULL, RAM+0x008000);		// BG0 RAM    [V-RAM]
   AddWriteWord(0xD00000, 0xD00FFF, NULL, RAM+0x016000);		// OBJECT RAM [VPOS]
   AddWriteWord(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);		// OBJECT RAM
   AddWriteWord(0x21C000, 0x21FFFF, NULL, RAM+0x010000);		// CP
   AddWriteWord(0xB00000, 0xB00FFF, NULL, RAM+0x014000);		// COM RAM
   AddWriteWord(0x700000, 0x7003FF, NULL, RAM+0x015000);		// COLOUR RAM
   AddWriteWord(0x800000, 0x80000F, NULL, RAM+0x017000);		// SCROLL RAM
   AddWriteWord(0xF00000, 0xF000FF, NULL, RAM+0x017100);		// SOME EXTRA RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearTwinEagle(void)
{
   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x080000,1);
      //save_debug("GFX.bin",GFX,0x600000,0);
   #endif
}

void LoadZingZingZip(void)
{
   int ta,tb,tc,td;

   RAMSize=0x40000+0x10000;

   if(!(ROM=AllocateMem(0x80000))) return;
   if(!(GFX=AllocateMem(0x200000+0x400000))) return;
   if(!(RAM=AllocateMem(0x100000))) return;

   GFX_SPR = GFX+0x000000;

   if(!load_rom("UY001015", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      tc=RAM[ta];
      GFX[tb+0]=((tc&0x80)>>7)<<0;
      GFX[tb+1]=((tc&0x40)>>6)<<0;
      GFX[tb+2]=((tc&0x20)>>5)<<0;
      GFX[tb+3]=((tc&0x10)>>4)<<0;
      GFX[tb+4]=((tc&0x08)>>3)<<0;
      GFX[tb+5]=((tc&0x04)>>2)<<0;
      GFX[tb+6]=((tc&0x02)>>1)<<0;
      GFX[tb+7]=((tc&0x01)>>0)<<0;
      tc=RAM[ta+1];
      GFX[tb+0]|=((tc&0x80)>>7)<<1;
      GFX[tb+1]|=((tc&0x40)>>6)<<1;
      GFX[tb+2]|=((tc&0x20)>>5)<<1;
      GFX[tb+3]|=((tc&0x10)>>4)<<1;
      GFX[tb+4]|=((tc&0x08)>>3)<<1;
      GFX[tb+5]|=((tc&0x04)>>2)<<1;
      GFX[tb+6]|=((tc&0x02)>>1)<<1;
      GFX[tb+7]|=((tc&0x01)>>0)<<1;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("UY001016", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<2;
      GFX[tb+1]|=((tc&0x40)>>6)<<2;
      GFX[tb+2]|=((tc&0x20)>>5)<<2;
      GFX[tb+3]|=((tc&0x10)>>4)<<2;
      GFX[tb+4]|=((tc&0x08)>>3)<<2;
      GFX[tb+5]|=((tc&0x04)>>2)<<2;
      GFX[tb+6]|=((tc&0x02)>>1)<<2;
      GFX[tb+7]|=((tc&0x01)>>0)<<2;
      tc=RAM[ta+1];
      GFX[tb+0]|=((tc&0x80)>>7)<<3;
      GFX[tb+1]|=((tc&0x40)>>6)<<3;
      GFX[tb+2]|=((tc&0x20)>>5)<<3;
      GFX[tb+3]|=((tc&0x10)>>4)<<3;
      GFX[tb+4]|=((tc&0x08)>>3)<<3;
      GFX[tb+5]|=((tc&0x04)>>2)<<3;
      GFX[tb+6]|=((tc&0x02)>>1)<<3;
      GFX[tb+7]|=((tc&0x01)>>0)<<3;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }

   if(!load_rom("UY001008", RAM, 0x100000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x100000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UY001010", RAM, 0x100000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x100000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }

   if(!load_rom("UY001001", RAM+0x00000, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("UY001002", RAM+0x00000, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
/*
   Rotate16x16(GFX+0x200000,0x4000);
   Flip16x16_Y(GFX+0x200000,0x4000);
*/
   Rotate16x16(GFX+0x000000,0x2000);
   Flip16x16_X(GFX+0x000000,0x2000);

   /* ----- M6510/M6502 ----- */

   M6502ROM = RAM+0x40000;

   memset(M6502ROM,0xFF,0x10000);

   //if(!load_rom("UP001005", M6502ROM+0x6000, 0x2000)) return;

   M6502ROM[0x7239]=0x8D;	// STA $FFFC
   M6502ROM[0x723A]=0xFC;
   M6502ROM[0x723B]=0xFF;

   SetStopM6502Mode2(0x720D);

   // Mirror

   memcpy(M6502ROM+0x8000,M6502ROM,0x8000);

   // Setup Z80 memory map
   // --------------------

   AddM6502AROMBase(M6502ROM);

   AddM6502AReadByte(0x0000, 0xFFFF, ReadROM,                     NULL); // Testing
   AddM6502AReadByte(0x0000, 0xFFFF, DefBadReadM6502,             NULL);
   AddM6502AReadByte(    -1,     -1, NULL,                        NULL);

   AddM6502AWriteByte(0x0000, 0xFFF0, WriteROM,                   NULL); // Testing
   AddM6502AWriteByte(0xFFFC, 0xFFFC, StopM6502Mode2,             NULL);
   AddM6502AWriteByte(0x0000, 0xFFFF, DefBadWriteM6502,           NULL);
   AddM6502AWriteByte(    -1,     -1, NULL,                       NULL);

   AddM6502AInit();

   /* ----------------------- */

   memset(RAM+0x00000,0x00,0x40000);
/*
   // Speed Hack

   WriteLong68k(&ROM[0x00D0A],0x4EF80300);
   WriteLong68k(&ROM[0x00300],0x13FC0000);
   WriteLong68k(&ROM[0x00304],0x00AA0000);
   WriteWord68k(&ROM[0x00308],0x6100-10);

   // Fix CP ERROR

   WriteLong68k(&ROM[0x8AA26],0x4E714E71);
   WriteLong68k(&ROM[0x8AA76],0x4E714E71);
   WriteLong68k(&ROM[0x8AAC6],0x4E714E71);
   WriteLong68k(&ROM[0x8AB08],0x4E714E71);
*/
   InitPaletteMap(RAM+0x15000, 0x20, 0x10, 0x8000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);

   //GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x4000);

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   // Init seta_x1 emulation
   // ----------------------

   seta_x1.RAM_A	= RAM+0x0C000;
   seta_x1.RAM_B	= RAM+0x16000;
   seta_x1.GFX		= GFX_SPR;
   seta_x1.MASK		= GFX_SPR_SOLID;
   seta_x1.bmp_x	= 32;
   seta_x1.bmp_y	= 32;
   seta_x1.bmp_w	= 240;
   seta_x1.bmp_h	= 356;
   seta_x1.tile_mask	= 0x1FFF;
   seta_x1.scr_x	= 0;
   seta_x1.scr_y	= 0;

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x800000, 0x803FFF, NULL, RAM+0x014000);			// BG0 RAM
   AddReadByte(0x880000, 0x883FFF, NULL, RAM+0x018000);			// BG0 RAM
   AddReadByte(0x700000, 0x700FFF, NULL, RAM+0x01C000);			// VPOS RAM
   AddReadByte(0xB00000, 0xB03FFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x803FFF, NULL, RAM+0x014000);			// BG0 RAM
   AddReadWord(0x880000, 0x883FFF, NULL, RAM+0x018000);			// BG0 RAM
   AddReadWord(0x700000, 0x700FFF, NULL, RAM+0x01C000);			// VPOS RAM
   AddReadWord(0xB00000, 0xB03FFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddWriteByte(0x800000, 0x803FFF, NULL, RAM+0x014000);			// BG0 RAM
   AddWriteByte(0x880000, 0x883FFF, NULL, RAM+0x018000);			// BG0 RAM
   AddWriteByte(0x700000, 0x700FFF, NULL, RAM+0x01C000);			// VPOS RAM
   AddWriteByte(0xB00000, 0xB03FFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddWriteWord(0x800000, 0x803FFF, NULL, RAM+0x014000);			// BG0 RAM
   AddWriteWord(0x880000, 0x883FFF, NULL, RAM+0x018000);			// BG0 RAM
   AddWriteWord(0x700000, 0x700FFF, NULL, RAM+0x01C000);			// VPOS RAM
   AddWriteWord(0xB00000, 0xB03FFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearZingZingZip(void)
{
   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x080000,1);
      save_debug("GFX.bin",GFX,0x600000,0);
   #endif
}

void LoadUSClassicGolf(void)
{
   int ta,tb,tc,td;

   RAMSize=0x40000+0x10000;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0xA0000))) return;
   if(!(GFX=AllocateMem(0x200000+0x400000))) return;

   if(!load_rom("P1006163", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]=((tc&0x80)>>7)<<0;
      GFX[tb+1]=((tc&0x40)>>6)<<0;
      GFX[tb+2]=((tc&0x20)>>5)<<0;
      GFX[tb+3]=((tc&0x10)>>4)<<0;
      GFX[tb+4]=((tc&0x08)>>3)<<0;
      GFX[tb+5]=((tc&0x04)>>2)<<0;
      GFX[tb+6]=((tc&0x02)>>1)<<0;
      GFX[tb+7]=((tc&0x01)>>0)<<0;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("P1007164", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<1;
      GFX[tb+1]|=((tc&0x40)>>6)<<1;
      GFX[tb+2]|=((tc&0x20)>>5)<<1;
      GFX[tb+3]|=((tc&0x10)>>4)<<1;
      GFX[tb+4]|=((tc&0x08)>>3)<<1;
      GFX[tb+5]|=((tc&0x04)>>2)<<1;
      GFX[tb+6]|=((tc&0x02)>>1)<<1;
      GFX[tb+7]|=((tc&0x01)>>0)<<1;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("P1008165", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<2;
      GFX[tb+1]|=((tc&0x40)>>6)<<2;
      GFX[tb+2]|=((tc&0x20)>>5)<<2;
      GFX[tb+3]|=((tc&0x10)>>4)<<2;
      GFX[tb+4]|=((tc&0x08)>>3)<<2;
      GFX[tb+5]|=((tc&0x04)>>2)<<2;
      GFX[tb+6]|=((tc&0x02)>>1)<<2;
      GFX[tb+7]|=((tc&0x01)>>0)<<2;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   if(!load_rom("P1009166", RAM, 0x40000)) return;		// 16x16 SPRITES
   tb=0;
   for(ta=0;ta<0x40000;ta++){
      tc=RAM[ta];
      GFX[tb+0]|=((tc&0x80)>>7)<<3;
      GFX[tb+1]|=((tc&0x40)>>6)<<3;
      GFX[tb+2]|=((tc&0x20)>>5)<<3;
      GFX[tb+3]|=((tc&0x10)>>4)<<3;
      GFX[tb+4]|=((tc&0x08)>>3)<<3;
      GFX[tb+5]|=((tc&0x04)>>2)<<3;
      GFX[tb+6]|=((tc&0x02)>>1)<<3;
      GFX[tb+7]|=((tc&0x01)>>0)<<3;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }

   if(!load_rom("UP001010", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UP001011", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x400000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] = (((tc&0x80)>>7)<<3) | (((tc&0x08)>>3)<<2);
      GFX[td|seta_gfx_code[tb+1]] = (((tc&0x40)>>6)<<3) | (((tc&0x04)>>2)<<2);
      GFX[td|seta_gfx_code[tb+2]] = (((tc&0x20)>>5)<<3) | (((tc&0x02)>>1)<<2);
      GFX[td|seta_gfx_code[tb+3]] = (((tc&0x10)>>4)<<3) | (((tc&0x01)>>0)<<2);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UP001012", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x200000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }
   if(!load_rom("UP001013", RAM, 0x80000)) return;		// 16x16 SPRITES
   tb=0;
   td=0x400000;
   for(ta=0;ta<0x80000;ta++){
      tc=RAM[ta];
      GFX[td|seta_gfx_code[tb+0]] |= (((tc&0x80)>>7)<<1) | (((tc&0x08)>>3)<<0);
      GFX[td|seta_gfx_code[tb+1]] |= (((tc&0x40)>>6)<<1) | (((tc&0x04)>>2)<<0);
      GFX[td|seta_gfx_code[tb+2]] |= (((tc&0x20)>>5)<<1) | (((tc&0x02)>>1)<<0);
      GFX[td|seta_gfx_code[tb+3]] |= (((tc&0x10)>>4)<<1) | (((tc&0x01)>>0)<<0);
      tb+=4; if(tb==0x100){ tb=0; td+=0x100; }
   }

   if(!load_rom("P1003161", RAM+0x00000, 0x40000)) return;
   if(!load_rom("UP001002", RAM+0x40000, 0x10000)) return;
   for(ta=0;ta<0x50000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("P1004162", RAM+0x00000, 0x40000)) return;
   if(!load_rom("UP001001", RAM+0x40000, 0x10000)) return;
   for(ta=0;ta<0x50000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
/*
   Rotate16x16(GFX+0x200000,0x4000);
   Flip16x16_Y(GFX+0x200000,0x4000);
*/
   Rotate16x16(GFX+0x000000,0x2000);
   Flip16x16_X(GFX+0x000000,0x2000);

   /* ----- M6510/M6502 ----- */

   M6502ROM = RAM+0x40000;

   memset(M6502ROM,0xFF,0x10000);

   if(!load_rom("UP001005", M6502ROM+0x6000, 0x2000)) return;

   M6502ROM[0x7239]=0x8D;	// STA $FFFC
   M6502ROM[0x723A]=0xFC;
   M6502ROM[0x723B]=0xFF;

   SetStopM6502Mode2(0x720D);

   // Mirror

   memcpy(M6502ROM+0x8000,M6502ROM,0x8000);

   // Setup Z80 memory map
   // --------------------

   AddM6502AROMBase(M6502ROM);

   AddM6502AReadByte(0x0000, 0xFFFF, ReadROM,                     NULL); // Testing
   AddM6502AReadByte(0x0000, 0xFFFF, DefBadReadM6502,             NULL);
   AddM6502AReadByte(    -1,     -1, NULL,                        NULL);

   AddM6502AWriteByte(0x0000, 0xFFF0, WriteROM,                   NULL); // Testing
   AddM6502AWriteByte(0xFFFC, 0xFFFC, StopM6502Mode2,             NULL);
   AddM6502AWriteByte(0x0000, 0xFFFF, DefBadWriteM6502,           NULL);
   AddM6502AWriteByte(    -1,     -1, NULL,                       NULL);

   AddM6502AInit();

   /* ----------------------- */

   memset(RAM+0x00000,0x00,0x40000);

   // Speed Hack

   WriteLong68k(&ROM[0x00D0A],0x4EF80300);
   WriteLong68k(&ROM[0x00300],0x13FC0000);
   WriteLong68k(&ROM[0x00304],0x00AA0000);
   WriteWord68k(&ROM[0x00308],0x6100-10);

   // Fix CP ERROR

   WriteLong68k(&ROM[0x8AA26],0x4E714E71);
   WriteLong68k(&ROM[0x8AA76],0x4E714E71);
   WriteLong68k(&ROM[0x8AAC6],0x4E714E71);
   WriteLong68k(&ROM[0x8AB08],0x4E714E71);

   InitPaletteMap(RAM+0x15000, 0x20, 0x10, 0x8000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);

   //GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x4000);

   //GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0xA0000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x09FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x09FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0xF00000, 0xF03FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x100000, 0x103FFF, NULL, RAM+0x004000);			// ? RAM
   AddReadByte(0x900000, 0x903FFF, NULL, RAM+0x008000);			// BG0 RAM
   AddReadByte(0xD00000, 0xD00FFF, NULL, RAM+0x016000);			// ? RAM
   AddReadByte(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);			// OBJECT RAM
   AddReadByte(0x21C000, 0x21FFFF, NULL, RAM+0x010000);			// CP
   AddReadByte(0xB00000, 0xB00FFF, NULL, RAM+0x014000);			// COM RAM
   AddReadByte(0x700000, 0x7003FF, NULL, RAM+0x015000);			// COLOUR RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x09FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0xF00000, 0xF03FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x100000, 0x103FFF, NULL, RAM+0x004000);			// ? RAM
   AddReadWord(0x900000, 0x903FFF, NULL, RAM+0x008000);			// BG0 RAM
   AddReadWord(0xD00000, 0xD00FFF, NULL, RAM+0x016000);			// ? RAM
   AddReadWord(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);			// OBJECT RAM
   AddReadWord(0x21C000, 0x21FFFF, NULL, RAM+0x010000);			// CP
   AddReadWord(0xB00000, 0xB00FFF, NULL, RAM+0x014000);			// COM RAM
   AddReadWord(0x700000, 0x7003FF, NULL, RAM+0x015000);			// COLOUR RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0xF00000, 0xF03FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x100000, 0x103FFF, NULL, RAM+0x004000);		// ? RAM
   AddWriteByte(0x900000, 0x903FFF, NULL, RAM+0x008000);		// BG0 RAM
   AddWriteByte(0xD00000, 0xD00FFF, NULL, RAM+0x016000);		// ? RAM
   AddWriteByte(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);		// OBJECT RAM
   AddWriteByte(0x21C000, 0x21FFFF, NULL, RAM+0x010000);		// CP
   AddWriteByte(0xB00000, 0xB00FFF, NULL, RAM+0x014000);		// COM RAM
   AddWriteByte(0x700000, 0x7003FF, NULL, RAM+0x015000);		// COLOUR RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0xF00000, 0xF03FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x100000, 0x103FFF, NULL, RAM+0x004000);		// ? RAM
   AddWriteWord(0x900000, 0x903FFF, NULL, RAM+0x008000);		// BG0 RAM
   AddWriteWord(0xD00000, 0xD00FFF, NULL, RAM+0x016000);		// ? RAM
   AddWriteWord(0xE00000, 0xE03FFF, NULL, RAM+0x00C000);		// OBJECT RAM
   AddWriteWord(0x21C000, 0x21FFFF, NULL, RAM+0x010000);		// CP
   AddWriteWord(0xB00000, 0xB00FFF, NULL, RAM+0x014000);		// COM RAM
   AddWriteWord(0x700000, 0x7003FF, NULL, RAM+0x015000);		// COLOUR RAM
   AddWriteWord(0x800000, 0x80000F, NULL, RAM+0x017000);		// SCROLL RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearUSClassicGolf(void)
{
   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x0A0000,1);
      save_debug("RAM.bin",RAM,0x080000,1);
      //save_debug("GFX.bin",GFX,0x600000,0);
   #endif
}

void ExecuteMetaFoxFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(16,60));	// M68000 16MHz (60fps)
   #ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif
   cpu_interrupt(CPU_68K_0, 2);

   m6502exec(CPU_FRAME_MHz(4,60));	// M6502 4MHz (60fps)
   #ifdef RAINE_DEBUG
     print_debug("M6502_PC:%04x\n",m6502pc);
   #endif
   //m6502int();
}

void DrawMetaFox(void)
{
   int zz,zzz,zzzz,x16,y16,x,y,ta;
   UINT8 *map;

   ClearPaletteMap();

   // BG0
   // ---

   MAKE_SCROLL_1024x512_2_16(
       ReadWord(&RAM[0x17000]),
       ReadWord(&RAM[0x17002])
   );

   START_SCROLL_1024x512_2_16_R270(32,32,240,356);

      ta = ReadWord(&RAM[0x8000+zz])&0x3FFF;

      MAP_PALETTE_MAPPED(
         Map_15bit_xRGB,
         RAM[0x9000+zz]&0x1F,
         16,
         map
      );

      switch(RAM[0x8001+zz]&0xC0){
      case 0x00: Draw16x16_Mapped(&GFX[0x200000+(ta<<8)], x, y, map);        break;
      case 0x40: Draw16x16_Mapped_FlipY(&GFX[0x200000+(ta<<8)], x, y, map);  break;
      case 0x80: Draw16x16_Mapped_FlipX(&GFX[0x200000+(ta<<8)], x, y, map);  break;
      case 0xC0: Draw16x16_Mapped_FlipXY(&GFX[0x200000+(ta<<8)], x, y, map); break;
      }

   END_SCROLL_1024x512_2_16();

   render_seta_x1_68000_r270();

}

/*

800000 - SCROLL X
800002 - SCROLL Y
800004 - CTRL

*/
