/******************************************************************************/
/*                                                                            */
/*                SOLITARY FIGHTER (C) 1990 TAITO CORPORATION                 */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "solfigtr.h"
#include "tc100scn.h"
#include "tc110pcr.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO solitary_fighter_dirs[] =
{
   { "solitary_fighter", },
   { "solfigtr", },
   { NULL, },
};

static struct ROM_INFO solitary_fighter_roms[] =
{
   {       "c91-01", 0x00100000, 0x0f3f4e00, 0, 0, 0, },
   {       "c91-02", 0x00100000, 0xe14ab98e, 0, 0, 0, },
   {       "c91-03", 0x00100000, 0x8965da12, 0, 0, 0, },
   {       "c91-04", 0x00080000, 0x390b1065, 0, 0, 0, },
   {       "c91-05", 0x00040000, 0xc1260e7c, 0, 0, 0, },
   {       "c91-07", 0x00010000, 0xe471a05a, 0, 0, 0, },
   {       "c91-09", 0x00040000, 0xd82b5266, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO solitary_fighter_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03A10E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03A10E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03A10E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03A10E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x03A104, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x03A104, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x03A104, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x03A104, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x03A104, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x03A104, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x03A104, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x03A104, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x03A106, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x03A106, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x03A106, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x03A106, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x03A106, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x03A106, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x03A106, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x03A106, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_solitary_fighter_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

struct DSW_DATA dsw_data_solitary_fighter_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO solitary_fighter_dsw[] =
{
   { 0x03A100, 0xFF, dsw_data_solitary_fighter_0 },
   { 0x03A102, 0xFF, dsw_data_solitary_fighter_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_solitary_fighter_0[] =
{
   { "Taito Japan (notice)",   0x01 },
   { "Taito America",          0x02 },
   { "Taito Japan",            0x03 },
   { NULL,                     0    },
};

static struct ROMSW_INFO solitary_fighter_romsw[] =
{
   { 0x03FFFF, 0x03, romsw_data_solitary_fighter_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO solitary_fighter_video =
{
   draw_solitary_fighter,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_solitary_fighter =
{
   solitary_fighter_dirs,
   solitary_fighter_roms,
   solitary_fighter_inputs,
   solitary_fighter_dsw,
   solitary_fighter_romsw,

   load_solitary_fighter,
   clear_solitary_fighter,
   &solitary_fighter_video,
   execute_solitary_fighter_frame,
   "solfigtr",
   "Solitary Fighter",
   "ダイノレックス",
   COMPANY_ID_TAITO,
   "C91",
   1990,
   taito_ym2610_sound,
   GAME_BEAT,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_COLOUR;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BANK;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static void solitary_fighter_ioc_0_wb(UINT32 offset, UINT8 data)
{
   switch(offset & 6){
      case 0:
         tc0220ioc_wb(0, data);
      break;
      case 2:
         tc0220ioc_wb(2, data);
      break;
      case 4:
         tc0220ioc_wb(8, data);
      break;
      default:
      break;
   }
}

static void solitary_fighter_ioc_0_ww(UINT32 offset, UINT16 data)
{
   solitary_fighter_ioc_0_wb(offset, (UINT8) (data & 0xFF));
}

static UINT8 solitary_fighter_ioc_0_rb(UINT32 offset)
{
   switch(offset & 6){
      case 0:
         return tc0220ioc_rb(0);
      break;
      case 2:
         return tc0220ioc_rb(2);
      break;
      case 4:
         return tc0220ioc_rb(8);
      break;
      default:
         return 0xFF;
      break;
   }
}

static UINT16 solitary_fighter_ioc_0_rw(UINT32 offset)
{
   return solitary_fighter_ioc_0_rb(offset);
}

static void solitary_fighter_ioc_1_wb(UINT32 offset, UINT8 data)
{
   switch(offset & 6){
      case 0:
         tc0220ioc_wb(4, data);
      break;
      case 2:
         tc0220ioc_wb(6, data);
      break;
      case 4:
         tc0220ioc_wb(14, data);
      break;
      default:
      break;
   }
}

static void solitary_fighter_ioc_1_ww(UINT32 offset, UINT16 data)
{
   solitary_fighter_ioc_1_wb(offset, (UINT8) (data & 0xFF));
}

static UINT8 solitary_fighter_ioc_1_rb(UINT32 offset)
{
   switch(offset & 6){
      case 0:
         return tc0220ioc_rb(4);
      break;
      case 2:
         return tc0220ioc_rb(6);
      break;
      case 4:
         return tc0220ioc_rb(14);
      break;
      default:
         return 0xFF;
      break;
   }
}

static UINT16 solitary_fighter_ioc_1_rw(UINT32 offset)
{
   return solitary_fighter_ioc_1_rb(offset);
}

void load_solitary_fighter(void)
{
   int ta,tb;

   if(!(GFX=AllocateMem(0x200000+0x400000))) return;
   if(!(RAM=AllocateMem(0x100000))) return;

   GFX_BG0	=GFX+0x000000;
   GFX_SPR	=GFX+0x200000;

   tb=0;
   if(!load_rom("c91-03", RAM, 0x100000)) return;		// 8x8 TILES
   for(ta=0;ta<0x100000;ta+=2,tb+=4){
      GFX_BG0[tb+3]=RAM[ta]&15;
      GFX_BG0[tb+2]=RAM[ta]>>4;
      GFX_BG0[tb+1]=RAM[ta+1]&15;
      GFX_BG0[tb+0]=RAM[ta+1]>>4;
   }

   tb=0;
   if(!load_rom("c91-01", RAM, 0x100000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x100000;ta++,tb+=2){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }
   if(!load_rom("c91-02", RAM, 0x100000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x100000;ta++,tb+=2){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }

   FreeMem(RAM);

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0x80000))) return;

   if(!load_rom("c91-05", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("c91-09", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x48000;
   if(!load_rom("c91-07", Z80ROM, 0x10000)) return;			// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("c91-04", PCMROM, 0x80000)) return;			// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x80000, 0x80000);

   AddTaitoYM2610(0x01A9, 0x0155, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x20000;
   RAM_SCROLL = RAM+0x3A000;
   RAM_OBJECT = RAM+0x10000;
   RAM_COLOUR = RAM+0x30000;
   RAM_INPUT  = RAM+0x3A100;
   GFX_FG0    = RAM+0x40000;
   GFX_BANK   = RAM+0x3A200;

   RAMSize=0x58000;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x8000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x4000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM_COLOUR, 0x100, 0x10, 0x1000);

   // SBCD flag bug - game relies on undefined flags (not supported in starscream)

   WriteWord68k(&ROM[0x09B9A],0x4E71);

   // 68000 Speed Hack

   WriteLong68k(&ROM[0x0058E],0x4EF800C0);

   WriteLong68k(&ROM[0x000C0],0x46FC2000);

   WriteLong68k(&ROM[0x000C4],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x000C8],0x00AA0000);
   WriteWord68k(&ROM[0x000CC],0x6100-10);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX_BG0;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x7FFF;
   tc0100scn[0].layer[0].scr_x	= 19;
   tc0100scn[0].layer[0].scr_y	= 8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX_BG0;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x7FFF;
   tc0100scn[0].layer[1].scr_x	= 19;
   tc0100scn[0].layer[1].scr_y	= 8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	= 16;
   tc0100scn[0].layer[2].scr_y	= 8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT+0x0000;
   tc0200obj.RAM_B	= RAM_OBJECT+0x8000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x3FFF;
   tc0200obj.ofs_x	= 0 - 0x13;
   tc0200obj.ofs_y	= 0 - 0x60;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x103FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x200000, 0x201FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadByte(0x300000, 0x300007, solitary_fighter_ioc_0_rb, NULL);	// INPUT
   AddReadByte(0x320000, 0x320007, solitary_fighter_ioc_1_rb, NULL);	// INPUT
   AddReadByte(0x400000, 0x400003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x103FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x200000, 0x201FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadWord(0x300000, 0x300007, solitary_fighter_ioc_0_rw, NULL);	// INPUT
   AddReadWord(0x320000, 0x320007, solitary_fighter_ioc_1_rw, NULL);	// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x103FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteByte(0x400000, 0x400003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x200000, 0x201FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteByte(0x300000, 0x300007, solitary_fighter_ioc_0_wb, NULL);	// INPUT
   AddWriteByte(0x320000, 0x320007, solitary_fighter_ioc_1_wb, NULL);	// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x103FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x200000, 0x201FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteWord(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x500000, 0x50001F, NULL, GFX_BANK);			// BANK
   AddWriteWord(0xB00000, 0xB0001F, NULL, GFX_BANK+0x20);		// ???
   AddWriteWord(0x300000, 0x300007, solitary_fighter_ioc_0_ww, NULL);	// INPUT
   AddWriteWord(0x320000, 0x320007, solitary_fighter_ioc_1_ww, NULL);	// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void clear_solitary_fighter(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("RAM.bin",RAM,0x040000,1);
   #endif
}

void execute_solitary_fighter_frame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)

   cpu_interrupt(CPU_68K_0, 5);
   cpu_interrupt(CPU_68K_0, 6);

   Taito2610_Frame();			// Z80 and YM2610
}

void draw_solitary_fighter(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // OBJECT
   // ------

   make_object_bank(GFX_BANK);
   render_tc0200obj_mapped_soliltary_fighter();

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);
}
