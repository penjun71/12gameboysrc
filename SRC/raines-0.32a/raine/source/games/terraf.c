/******************************************************************************/
/*                                                                            */
/*                      TERRA FORCE (C) 1990 NICHBUTSU                        */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "terraf.h"
#include "nichisnd.h"

static struct DIR_INFO terra_force_dirs[] =
{
   { "terra_force", },
   { "terraf", },
   { NULL, },
};

static struct ROM_INFO terra_force_roms[] =
{
   { "terrafor.010", 0x00010000, 0x58b5f43b, 0, 0, 0, },
   { "terrafor.002", 0x00010000, 0x148aa0c5, 0, 0, 0, },
   { "terrafor.003", 0x00010000, 0xd74085a1, 0, 0, 0, },
   { "terrafor.004", 0x00010000, 0x2144d8e0, 0, 0, 0, },
   { "terrafor.005", 0x00010000, 0x744f5c9e, 0, 0, 0, },
   { "terrafor.006", 0x00010000, 0x25d23dfd, 0, 0, 0, },
   { "terrafor.007", 0x00010000, 0xb9b0fe27, 0, 0, 0, },
   { "terrafor.008", 0x00008000, 0xbc6f7cbc, 0, 0, 0, },
   { "terrafor.009", 0x00008000, 0xd1014280, 0, 0, 0, },
   { "terrafor.001", 0x00010000, 0xeb6b4138, 0, 0, 0, },
   { "terrafor.011", 0x00010000, 0x5320162a, 0, 0, 0, },
   { "terrafor.012", 0x00008000, 0x4f0e1d76, 0, 0, 0, },
   { "terrafor.013", 0x00010000, 0xa86951e0, 0, 0, 0, },
   { "terrafor.014", 0x00010000, 0x8e5f557f, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO terra_force_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x018001, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x018001, 0x08, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x018001, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x018000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x018000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x018000, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x018000, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x018000, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x018000, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x018001, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x018002, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x018002, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x018002, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x018002, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x018002, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x018002, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_terra_force_0[] =
{
   { "Lives",                 0x03, 0x04 },
   { "3",                     0x03, 0x00 },
   { "4",                     0x02, 0x00 },
   { "5",                     0x01, 0x00 },
   { "6",                     0x00, 0x00 },
   { "Extra Life at",         0x04, 0x02 },
   { "20,000",                0x04, 0x00 },
   { "50,000",                0x00, 0x00 },
   { "Second Life at",        0x08, 0x02 },
   { "60,000",                0x08, 0x00 },
   { "90,000",                0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x10, 0x02 },
   { MSG_ON,                  0x10, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_CABINET,             0x20, 0x02 },
   { MSG_TABLE,               0x20, 0x00 },
   { MSG_UPRIGHT,             0x00, 0x00 },
   { MSG_DIFFICULTY,          0xC0, 0x04 },
   { MSG_EASY,                0xC0, 0x00 },
   { MSG_NORMAL,              0x80, 0x00 },
   { MSG_HARD,                0x40, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_terra_force_1[] =
{
   { MSG_COIN1,               0x03, 0x04 },
   { MSG_1COIN_1PLAY,         0x03, 0x00 },
   { MSG_1COIN_2PLAY,         0x02, 0x00 },
   { MSG_2COIN_1PLAY,         0x01, 0x00 },
   { MSG_FREE_PLAY,             0x00, 0x00 },
   { MSG_COIN2,               0x0C, 0x04 },
   { MSG_1COIN_1PLAY,         0x0C, 0x00 },
   { MSG_1COIN_2PLAY,         0x08, 0x00 },
   { MSG_2COIN_1PLAY,         0x04, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { MSG_SCREEN,              0x20, 0x02 },
   { MSG_NORMAL,              0x20, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { "Continues",             0xC0, 0x04 },
   { "0",                     0xC0, 0x00 },
   { "3",                     0x80, 0x00 },
   { "5",                     0x40, 0x00 },
   { "Infinite",              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO terra_force_dsw[] =
{
   { 0x018004, 0xFF, dsw_data_terra_force_0 },
   { 0x018006, 0xFF, dsw_data_terra_force_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO terra_force_video =
{
   DrawTerraF,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL,
};

struct GAME_MAIN game_terra_force =
{
   terra_force_dirs,
   terra_force_roms,
   terra_force_inputs,
   terra_force_dsw,
   NULL,

   LoadTerraF,
   ClearTerraF,
   &terra_force_video,
   ExecuteTerraFFrame,
   "terraf",
   "Terra Force",
   "テラフォース",
   COMPANY_ID_NICHIBUTSU,
   NULL,
   1987,
   nichi_ym3812_sound,
   GAME_SHOOT,
};

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG1;
static UINT8 *GFX_SPR;
static UINT8 *GFX_FG0;

static UINT8 *MSK_BG1;
static UINT8 *MSK_SPR;
static UINT8 *MSK_FG0;

static UINT8 *RAM_SCR;

void LoadTerraF(void)
{
   int ta,tb;

   RAMSize=0x20100+0x10000;

   if(!(ROM=AllocateMem(0x50000))) return;
   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(GFX=AllocateMem(0xD0000))) return;

   if(!load_rom("terrafor.014",&RAM[0x0000],0x10000)) return;
   for(ta=0;ta<0x10000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("terrafor.011",&RAM[0x0000],0x10000)) return;
   for(ta=0;ta<0x10000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("terrafor.013",&RAM[0x0000],0x10000)) return;
   for(ta=0;ta<0x10000;ta++){
      ROM[ta+ta+0x20000]=RAM[ta];
   }
   if(!load_rom("terrafor.010",&RAM[0x0000],0x10000)) return;
   for(ta=0;ta<0x10000;ta++){
      ROM[ta+ta+0x20001]=RAM[ta];
   }
   if(!load_rom("terrafor.012",&RAM[0x0000],0x8000)) return;
   for(ta=0;ta<0x8000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("terrafor.009",&RAM[0x0000],0x8000)) return;
   for(ta=0;ta<0x8000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x20100;
   if(!load_rom("terrafor.001", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   AddNichibutsuYM3526(0x0085, 0x007A);

   AddNichiSample(0x0173, 0x0175, 1);
   AddNichiSample(0x0176, 0x0178, 1);

   AddNichiSample(0x5040, 0x53F0, 1);
   AddNichiSample(0x53F1, 0x5511, 1);
   AddNichiSample(0x5512, 0x5AD2, 1);
   AddNichiSample(0x5AD3, 0x70D3, 3);

   AddNichiSample(0x8040, 0x88DB, 2);
   AddNichiSample(0x88DC, 0x9687, 2);
   AddNichiSample(0x9688, 0xA9C4, 3);
   AddNichiSample(0xA9C5, 0xAFF5, 3);
   AddNichiSample(0xAFF6, 0xCA26, 3);
   AddNichiSample(0xCA27, 0xDC27, 3);
   AddNichiSample(0xDC28, 0xDD48, 1);
   AddNichiSample(0xDD49, 0xE3B6, 2);
   AddNichiSample(0xE3B7, 0xF0B7, 1);

   /*-----------------------*/

   GFX_BG0 = GFX+0x000000;
   GFX_BG1 = GFX+0x040000;
   GFX_SPR = GFX+0x080000;
   GFX_FG0 = GFX+0x0C0000;

   tb=0;
   if(!load_rom("terrafor.004", RAM+0x00000, 0x10000)) return;	// GFX BG0
   if(!load_rom("terrafor.005", RAM+0x10000, 0x10000)) return;
   for(ta=0;ta<0x20000;ta++,tb+=2){
      GFX_BG0[tb+0]=(RAM[ta]&15)^15;
      GFX_BG0[tb+1]=(RAM[ta]>>4)^15;
   }

   tb=0;
   if(!load_rom("terrafor.006", RAM+0x00000, 0x10000)) return;	// GFX BG1
   if(!load_rom("terrafor.007", RAM+0x10000, 0x10000)) return;
   for(ta=0;ta<0x20000;ta++,tb+=2){
      GFX_BG1[tb+0]=(RAM[ta]&15)^15;
      GFX_BG1[tb+1]=(RAM[ta]>>4)^15;
   }

   tb=0;
   if(!load_rom("terrafor.003", RAM+0x00000, 0x10000)) return;	// GFX SPR
   if(!load_rom("terrafor.002", RAM+0x10000, 0x10000)) return;
   for(ta=0;ta<0x10000;ta++,tb+=4){
      GFX_SPR[tb+0]=(RAM[ta+0x00000]&15)^15;
      GFX_SPR[tb+1]=(RAM[ta+0x00000]>>4)^15;
      GFX_SPR[tb+2]=(RAM[ta+0x10000]&15)^15;
      GFX_SPR[tb+3]=(RAM[ta+0x10000]>>4)^15;
   }

   tb=0;
   if(!load_rom("terrafor.008", RAM+0x00000, 0x08000)) return;	// GFX FG0
   for(ta=0;ta<0x8000;ta++,tb+=2){
      GFX_FG0[tb+0]=(RAM[ta]&15)^15;
      GFX_FG0[tb+1]=(RAM[ta]>>4)^15;
   }

   MSK_BG1 = make_solid_mask_16x16(GFX_BG1, 0x0400);
   MSK_SPR = make_solid_mask_16x16(GFX_SPR, 0x0400);
   MSK_FG0 = make_solid_mask_8x8  (GFX_FG0, 0x0400);

   RAM_SCR = RAM+0x1C000;

   memset(RAM+0x00000,0x00,0x20100);
   memset(RAM+0x18000,0xFF,0x00100);

   // 68000 Speed Hack
   // ----------------

   WriteLong68k(&ROM[0x00132],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x00136],0x00AA0000);	//	(Speed Hack)

   // Change Sound Comm Address (for speed)
   // -------------------------------------

   WriteLong68k(&ROM[0x0784A],0x00BB0000);

   // Scroll Write Fix
   // ----------------

   WriteLong68k(&ROM[0x1F724],0x0007D000);


   InitPaletteMap(RAM+0x4000, 0x100, 0x10, 0x1000);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x50000);
   ByteSwap(RAM,0x20000);

   AddMemFetch(0x000000, 0x04FFFF, ROM+0x000000-0x000000);
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x04FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x060000, 0x07FFFF, NULL, RAM+0x000000);			// ALL RAM
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x04FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x060000, 0x07FFFF, NULL, RAM+0x000000);			// ALL RAM
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x060000, 0x07FFFF, NULL, RAM+0x000000);		// ALL RAM
   AddWriteByte(0x0C0000, 0x0C000F, NULL, RAM+0x020000);		// ???
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x060000, 0x07FFFF, NULL, RAM+0x000000);		// ALL RAM
   AddWriteWord(0xBB0000, 0xBB0001, NichiSoundCommWrite68k, NULL);	// SOUND COMM
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers...
}

void ClearTerraF(void)
{
   RemoveNichibutsuYM3526();
}

void ExecuteTerraFFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 1);

   Nichibutsu3526_Frame();		// Z80 and YM3526
}

void DrawTerraF(void)
{
   UINT8 *map;
   int x,y,ta,zz,zzz,zzzz,x16,y16;

   ClearPaletteMap();

   // BG0
   // ---

   if(((zzz=ReadWord(RAM_SCR))&0x0200)==0){
      clear_game_screen(0);			// Game has no solid BG0
   }
   else{

   zzz=15+(ReadWord(&RAM[0x1C004]));
   zzzz=((zzz&0x1F0)>>4)<<1;			// X Offset (16-511)
   x16=15-(zzz&15);				// X Offset (0-15)
   zzz=95+(ReadWord(&RAM[0x1C002]));
   zzzz+=((zzz&0x3F0)>>4)<<6;			// Y Offset (16-511)
   y16=15-(zzz&15);				// Y Offset (0-15)

   zzzz&=0xFFF;

   for(x=(16+y16);x<(320+32);x+=16){
   zz=zzzz;
   for(y=(16+x16);y<(224+32);y+=16){

      MAP_PALETTE_MAPPED(
         Map_12bit_xxxxRRRRGGGGBBBB_Rev,
         (RAM[0x14001+zz]>>3)|0x60,
         16,
         map
      );

      Draw16x16_Mapped(&GFX_BG0[(ReadWord(&RAM[0x14000+zz])&0x3FF)<<8],x,y,map);

   zz+=2;
   if((zz&0x3F)==0){zz-=0x40;}
   }
   zzzz+=0x40;
   zzzz&=0xFFF;
   }

   }

   // OBJECT LOW
   // ----------

   if(((zzz=ReadWord(RAM_SCR))&0x0800)!=0){

   for(zz=0;zz<0x600;zz+=8){

      if((ReadWord(&RAM[zz+0])&0x1000)==0){

      ta=ReadWord(&RAM[zz+2])&0x3FF;
      if(MSK_SPR[ta]!=0){				// No pixels; skip

      y=((320+32+32) - ReadWord(&RAM[zz+0]))&0x1FF;
      x=(ReadWord(&RAM[zz+6]) - (32+32))&0x1FF;

      if((x>16)&&(y>16)&&(x<320+32)&&(y<224+32)){

         MAP_PALETTE_MAPPED(
            Map_12bit_xxxxRRRRGGGGBBBB_Rev,
            (RAM[zz+5]&0x1F)|0x20,
            16,
            map
         );

         if(MSK_SPR[ta]==1){				// Some pixels; trans
            switch(RAM[zz+3]&0x30){
            case 0x00: Draw16x16_Trans_Mapped(&GFX_SPR[ta<<8],x,y,map);        break;
            case 0x10: Draw16x16_Trans_Mapped_FlipX(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x20: Draw16x16_Trans_Mapped_FlipY(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x30: Draw16x16_Trans_Mapped_FlipXY(&GFX_SPR[ta<<8],x,y,map); break;
            }
         }
         else{						// all pixels; solid
            switch(RAM[zz+3]&0x30){
            case 0x00: Draw16x16_Mapped(&GFX_SPR[ta<<8],x,y,map);        break;
            case 0x10: Draw16x16_Mapped_FlipX(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x20: Draw16x16_Mapped_FlipY(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x30: Draw16x16_Mapped_FlipXY(&GFX_SPR[ta<<8],x,y,map); break;
            }
         }


      }
      }
      }
   }
   }

   // BG1
   // ---

   if(((zzz=ReadWord(RAM_SCR))&0x0400)!=0){

   zzz=15+((RAM[0x1C007])|(((RAM[0x1D001]>>0)&3)<<8));
   zzzz=((zzz&0x1F0)>>4)<<1;			// X Offset (16-511)
   x16=15-(zzz&15);				// X Offset (0-15)
   zzz=95+((RAM[0x1C009])|(((RAM[0x1D001]>>4)&3)<<8));
   zzzz+=((zzz&0x3F0)>>4)<<6;			// Y Offset (16-511)
   y16=15-(zzz&15);				// Y Offset (0-15)

   zzzz&=0xFFF;

   for(x=(16+y16);x<(320+32);x+=16){
   zz=zzzz;
   for(y=(16+x16);y<(224+32);y+=16){

      ta=ReadWord(&RAM[0x10000+zz])&0x3FF;
      if(MSK_BG1[ta]!=0){				// No pixels; skip

      MAP_PALETTE_MAPPED(
         Map_12bit_xxxxRRRRGGGGBBBB_Rev,
         (RAM[0x10001+zz]>>3)|0x40,
         16,
         map
      );

      if(MSK_BG1[ta]==1){				// Some pixels; trans
         Draw16x16_Trans_Mapped(&GFX_BG1[ta<<8],x,y,map);
      }
      else{						// all pixels; solid
         Draw16x16_Mapped(&GFX_BG1[ta<<8],x,y,map);
      }

      }

   zz+=2;
   if((zz&0x3F)==0){zz-=0x40;}
   }
   zzzz+=0x40;
   zzzz&=0xFFF;
   }

   }

   // OBJECT HIGH
   // -----------

   if(((zzz=ReadWord(RAM_SCR))&0x0800)!=0){

   for(zz=0;zz<0x600;zz+=8){

      if((ReadWord(&RAM[zz+0])&0x1000)!=0){

      ta=ReadWord(&RAM[zz+2])&0x3FF;
      if(MSK_SPR[ta]!=0){				// No pixels; skip

      y=((320+32+32) - ReadWord(&RAM[zz+0]))&0x1FF;
      x=(ReadWord(&RAM[zz+6]) - (32+32))&0x1FF;

      if((x>16)&&(y>16)&&(x<320+32)&&(y<224+32)){

         MAP_PALETTE_MAPPED(
            Map_12bit_xxxxRRRRGGGGBBBB_Rev,
            (RAM[zz+5]&0x1F)|0x20,
            16,
            map
         );

         if(MSK_SPR[ta]==1){				// Some pixels; trans
            switch(RAM[zz+3]&0x30){
            case 0x00: Draw16x16_Trans_Mapped(&GFX_SPR[ta<<8],x,y,map);        break;
            case 0x10: Draw16x16_Trans_Mapped_FlipX(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x20: Draw16x16_Trans_Mapped_FlipY(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x30: Draw16x16_Trans_Mapped_FlipXY(&GFX_SPR[ta<<8],x,y,map); break;
            }
         }
         else{						// all pixels; solid
            switch(RAM[zz+3]&0x30){
            case 0x00: Draw16x16_Mapped(&GFX_SPR[ta<<8],x,y,map);        break;
            case 0x10: Draw16x16_Mapped_FlipX(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x20: Draw16x16_Mapped_FlipY(&GFX_SPR[ta<<8],x,y,map);  break;
            case 0x30: Draw16x16_Mapped_FlipXY(&GFX_SPR[ta<<8],x,y,map); break;
            }
         }


      }
      }
      }
   }
   }

   // FG0
   // ---
 
   if(((zzz=ReadWord(RAM_SCR))&0x0100)!=0){

   zz=64*2;
   for(y=224+24;y>=32;y-=8){
   for(x=32+32;x<256+32+32;x+=8){

   ta=(((RAM[0x8800+zz]&3)<<14)|((RAM[0x8000+zz])<<6));
   if(ta!=0){

      MAP_PALETTE_MAPPED(
         Map_12bit_xxxxRRRRGGGGBBBB_Rev,
         (RAM[0x8800+zz]>>4),
         16,
         map
      );

      Draw8x8_Trans_Mapped(&GFX_FG0[ta],x,y,map);
   }

   zz+=2;
   }
   }

   zz=64*2;
   for(y=224+24;y>=32;y-=8){
   for(x=0+32;x<32+32;x+=8){

   ta=(((RAM[0x9800+zz+0x38]&3)<<14)|((RAM[0x9000+zz+0x38])<<6));
   if(ta!=0){

      MAP_PALETTE_MAPPED(
         Map_12bit_xxxxRRRRGGGGBBBB_Rev,
         (RAM[0x9800+zz+0x38]>>4),
         16,
         map
      );

      Draw8x8_Trans_Mapped(&GFX_FG0[ta],x,y,map);
   }

   ta=(((RAM[0x9800+zz]&3)<<14)|((RAM[0x9000+zz])<<6));
   if(ta!=0){

      MAP_PALETTE_MAPPED(
         Map_12bit_xxxxRRRRGGGGBBBB_Rev,
         (RAM[0x9800+zz]>>4),
         16,
         map
      );

      Draw8x8_Trans_Mapped(&GFX_FG0[ta],x+256+32,y,map);
   }

   zz+=2;
   }
   zz+=56;
   }

   }
}

/*

-----+--------+--------------------
Byte | Bit(s) | Info
-----+76543210+--------------------
  0  |....xxxx| Sprite Y (high)
  1  |xxxxxxxx| Sprite Y (low)
  2  |...x....| Sprite Flip X Axis
  2  |..x.....| Sprite Flip Y Axis
  2  |......xx| Sprite Number (high)
  3  |xxxxxxxx| Sprite Number (low)
  4  |...xxxxx| Colour Bank
  6  |xxxxxxxx| Sprite X (high)
  7  |xxxxxxxx| Sprite X (low)  
-----+--------+--------------------

*/
