/******************************************************************************/
/*                                                                            */
/*                   GROWL/RUNARK (C) 1990 TAITO CORPORATION                  */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "growl.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

static struct DIR_INFO growl_dirs[] =
{
   { "growl", },
   { NULL, },
};

static struct ROM_INFO growl_roms[] =
{
   { "growl_04.rom", 0x00100000, 0x2d97edf2, 0, 0, 0, },
   { "growl_02.rom", 0x00100000, 0x15a21506, 0, 0, 0, },
   { "growl_03.rom", 0x00100000, 0x1a0d8951, 0, 0, 0, },
   { "growl_01.rom", 0x00100000, 0x3434ce80, 0, 0, 0, },
   { "growl_05.rom", 0x00080000, 0xe29c0828, 0, 0, 0, },
   { "growl_08.rom", 0x00040000, 0xaa35dd9e, 0, 0, 0, },
   { "growl_10.rom", 0x00040000, 0xca81a20b, 0, 0, 0, },
   { "growl_11.rom", 0x00040000, 0xee3bd6d5, 0, 0, 0, },
   { "growl_12.rom", 0x00010000, 0xbb6ed668, 0, 0, 0, },
   { "growl_14.rom", 0x00040000, 0xb6c24ec7, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO growl_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x032384, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x032384, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x032384, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x032384, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x032380, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x032380, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x032380, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x032380, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x032380, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x032380, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x032380, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x032380, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x032382, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x032382, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x032382, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x032382, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x032382, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x032382, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x032382, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x032382, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_growl_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_1COIN_2PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_1COIN_2PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_growl_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Game Type",             0x30, 0x04 },
   { "1 Credit/2P",           0x30, 0x00 },
   { "4 Credits/4P",          0x20, 0x00 },
   { "1 Credit/4P",           0x10, 0x00 },
   { "2 Credits/4P",          0x00, 0x00 },
   { "Final Stage Cont",      0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO growl_dsw[] =
{
   { 0x032100, 0xFF, dsw_data_growl_0 },
   { 0x032102, 0xFF, dsw_data_growl_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_growl_0[] =
{
   { "Taito Japan (Runark)",    0x01 },
   { "Taito America (Growl)",   0x02 },
   { "Taito Worldwide (Growl)", 0x03 },
   { NULL,                      0    },
};

static struct ROMSW_INFO growl_romsw[] =
{
   { 0x0FFFFF, 0x03, romsw_data_growl_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO growl_video =
{
   DrawGrowl,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_growl =
{
   growl_dirs,
   growl_roms,
   growl_inputs,
   growl_dsw,
   growl_romsw,

   LoadGrowl,
   ClearGrowl,
   &growl_video,
   ExecuteGrowlFrame,
   "growl",
   "Growl",
   "���i�[�N",
   COMPANY_ID_TAITO,
   "C74",
   1990,
   taito_ym2610_sound,
   GAME_BEAT,
};

static UINT8 *RAM_INPUT;
static UINT8 *RAM_VIDEO;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_SCROLL;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static int BadReadWord(UINT32 address)
{
#ifdef RAINE_DEBUG
       print_debug("Rw(%06x) [%06x]\n",address,s68000context.pc);
#endif
   return(0xFFFF);
}

void LoadGrowl(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x100000))) return;
   if(!(GFX=AllocateMem(0x600000))) return;

   GFX_BG0 = GFX+0x000000;
   GFX_SPR = GFX+0x200000;

   tb=0;
   if(!load_rom("growl_01.rom", RAM, 0x100000)) return;
   for(ta=0;ta<0x100000;ta+=2){
      GFX[tb+0]=RAM[ta+1]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+2]=RAM[ta+0]>>4;
      GFX[tb+3]=RAM[ta+0]&15;
      tb+=4;
   }
   if(!load_rom("growl_03.rom", RAM, 0x100000)) return;
   for(ta=0;ta<0x100000;ta++){
      GFX[tb++]=RAM[ta]&15;
      GFX[tb++]=RAM[ta]>>4;
   }
   if(!load_rom("growl_02.rom", RAM, 0x100000)) return;
   for(ta=0;ta<0x100000;ta++){
      GFX[tb++]=RAM[ta]&15;
      GFX[tb++]=RAM[ta]>>4;
   }

   RAMSize=0x40000+0x10000;
   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(ROM=AllocateMem(0x100000))) return;

   if(!load_rom("growl_10.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("growl_08.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("growl_11.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+0x80000]=RAM[ta];
   }
   if(!load_rom("growl_14.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+0x80001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("growl_12.rom", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x100000))) return;
   if(!load_rom("growl_04.rom",PCMROM,0x100000)) return;		// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x100000, 0x100000);

   AddTaitoYM2610(0x01A9, 0x0155, 0x10000);

   /*-----------------------*/

   RAM_VIDEO  = RAM+0x10000;
   RAM_OBJECT = RAM+0x20000;
   RAM_SCROLL = RAM+0x32000;
   RAM_INPUT  = RAM+0x32100;

   memset(RAM+0x00000,0x00,0x40000);
   memset(RAM+0x32100,0xFF,0x08000);

   GFX_FG0 = RAM+0x3C000;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x8000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x4000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x30000, 0x100, 0x10, 0x1000);

   ROM[0x01010]=0x4E;		// SKIP OLD CODE (NO ROOM FOR HACK)
   ROM[0x01011]=0xF9;		// (JMP $72900)
   ROM[0x01012]=0x00;
   ROM[0x01013]=0x07;
   ROM[0x01014]=0x29;
   ROM[0x01015]=0x00;

   ROM[0x72900]=0x4E;		// jsr $BB4
   ROM[0x72901]=0xB9;		// (random number)
   ROM[0x72902]=0x00;
   ROM[0x72903]=0x00;
   ROM[0x72904]=0x0B;
   ROM[0x72905]=0xB4;

   ROM[0x72906]=0x13;		// move.b #$00,$AA0000
   ROM[0x72907]=0xFC;		// (Speed Hack)
   ROM[0x72908]=0x00;
   ROM[0x72909]=0x00;
   ROM[0x7290A]=0x00;
   ROM[0x7290B]=0xAA;
   ROM[0x7290C]=0x00;
   ROM[0x7290D]=0x00;

   ROM[0x7290E]=0x60;		// Loop
   ROM[0x7290F]=0x100-16;

   // Frame Sync Hack
   // ---------------

   WriteLong68k(&ROM[0x75B0],0x13FC0000);
   WriteLong68k(&ROM[0x75B4],0x00AA0000);
   WriteLong68k(&ROM[0x75B8],0x4E714E71);

   // Fix Sprite/Int6 Wait
   // --------------------

   WriteLong68k(&ROM[0x4CEE],0x4E714E71);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX_BG0;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x7FFF;
   tc0100scn[0].layer[0].scr_x	= 0x13;
   tc0100scn[0].layer[0].scr_y	= 0x08;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX_BG0;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x7FFF;
   tc0100scn[0].layer[1].scr_x	= 0x13;
   tc0100scn[0].layer[1].scr_y	= 0x08;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	= 0x13;
   tc0100scn[0].layer[2].scr_y	= 0x08;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT+0x0000;
   tc0200obj.RAM_B	= RAM_OBJECT+0x8000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x3FFF;
   tc0200obj.ofs_x	= 0; // - 0x13;
   tc0200obj.ofs_y	= 0; // - 0x60;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x34000);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x0FFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x300000, 0x30000F, NULL, RAM_INPUT);			// tc0220ioc
   AddReadByte(0x320000, 0x32000F, NULL, RAM+0x032380);			// INPUT
   AddReadByte(0x400000, 0x400003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x0FFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x200000, 0x201FFF, NULL, RAM+0x030000);			// COLOR RAM
   AddReadWord(0x300000, 0x30000F, NULL, RAM_INPUT);			// tc0220ioc
   AddReadWord(0x320000, 0x32000F, NULL, RAM+0x032380);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, BadReadWord, NULL);			// <Bad Reads>
   AddReadWord(-1, -1, NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x300000, 0x30000F, tc0220ioc_wb, NULL);		// tc0220ioc
   AddWriteByte(0x400000, 0x400003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x200000, 0x201FFF, NULL, RAM+0x030000);		// COLOR RAM
   AddWriteWord(0x300000, 0x30000F, tc0220ioc_ww, NULL);		// tc0220ioc
   AddWriteWord(0x820000, 0x82000F, NULL, RAM+0x032000);		// SCROLL RAM
   AddWriteWord(0x500000, 0x50000F, NULL, RAM+0x032400);		// OBJECT BANK?
   AddWriteWord(0x380000, 0x38000F, NULL, RAM+0x032180);		// ???
   AddWriteWord(0x600000, 0x60000F, NULL, RAM+0x032280);		// ???
   AddWriteWord(0xB00000, 0xB000FF, NULL, RAM+0x032300);		// ???
   AddWriteWord(0x340000, 0x34000F, NULL, RAM+0x032380);		// ???
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearGrowl(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("RAM.bin",RAM,0x040000,1);
   #endif
}

void ExecuteGrowlFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   /*#ifdef RAINE_DEBUG
      print_debug("PC0:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif*/
   cpu_interrupt(CPU_68K_0, 5);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   /*#ifdef RAINE_DEBUG
      print_debug("PC1:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif*/
   cpu_interrupt(CPU_68K_0, 6);

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawGrowl(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // OBJECT
   // ------

   if(tc0100scn_layer_count==0){	// Check if Screen needs cleaning
      tc0100scn_layer_count++;
      clear_game_screen(0);
   }

   make_object_bank(RAM+0x032400);
   render_tc0200obj_mapped_soliltary_fighter();

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);

}

