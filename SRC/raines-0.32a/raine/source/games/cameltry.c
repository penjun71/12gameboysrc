/******************************************************************************/
/*                                                                            */
/*                   CAMEL TRY (C) 1991 TAITO CORPORATION                     */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "cameltry.h"
#include "tc005rot.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO camel_try_dirs[] =
{
   { "camel_try", },
   { "cameltry", },
   { NULL, },
};

static struct ROM_INFO camel_try_roms[] =
{
   {   "c38-01.bin", 0x00080000, 0xc170ff36, 0, 0, 0, },
   {   "c38-02.bin", 0x00020000, 0x1a11714b, 0, 0, 0, },
   {   "c38-03.bin", 0x00020000, 0x59fa59a7, 0, 0, 0, },
   {   "c38-08.bin", 0x00010000, 0x7ff78873, 0, 0, 0, },
   {   "c38-09.bin", 0x00020000, 0x2ae01120, 0, 0, 0, },
   {   "c38-10.bin", 0x00020000, 0x48d8ff56, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO camel_try_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03000E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03000E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03000E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03000E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x030004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x030020, 0xFF, BIT_ACTIVE_1 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x030021, 0xFF, BIT_ACTIVE_1 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x030004, 0x10, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x030006, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x030030, 0xFF, BIT_ACTIVE_1 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x030031, 0xFF, BIT_ACTIVE_1 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x030006, 0x10, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_camel_try_0[] =
{
   { "Game Style",            0x01, 0x02 },
   { "Table",                 0x01, 0x00 },
   { "Upright",               0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_1COIN_2PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_1COIN_2PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_camel_try_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Start Time",            0x0C, 0x04 },
   { "50 sec",                0x0C, 0x00 },
   { "60 sec",                0x08, 0x00 },
   { "40 sec",                0x04, 0x00 },
   { "35 sec",                0x00, 0x00 },
   { "Continue Time Add",     0x30, 0x04 },
   { "30 sec",                0x30, 0x00 },
   { "40 sec",                0x20, 0x00 },
   { "25 sec",                0x10, 0x00 },
   { "20 sec",                0x00, 0x00 },
   { "Continue Play",         0x40, 0x02 },
   { MSG_ON,                  0x40, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { "2 Player Mode",         0x80, 0x02 },
   { "Single",                0x80, 0x00 },
   { "Together",              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO camel_try_dsw[] =
{
   { 0x030000, 0xFF, dsw_data_camel_try_0 },
   { 0x030002, 0xFF, dsw_data_camel_try_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO camel_try_video =
{
   draw_camel_try,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_camel_try =
{
   camel_try_dirs,
   camel_try_roms,
   camel_try_inputs,
   camel_try_dsw,
   NULL,

   load_camel_try,
   clear_camel_try,
   &camel_try_video,
   execute_camel_try_frame,
   "cameltry",
   "Camel Try",
   "キャメルトライ",
   COMPANY_ID_TAITO,
   "C38",
   1989,
   taito_ym2610_sound,
   GAME_PUZZLE,
};

static struct DIR_INFO camel_try_alt_dirs[] =
{
   { "camel_try_alternate", },
   { "camel_try_alt", },
   { "cameltra", },
   { "cameltru", },
   { ROMOF("cameltry"), },
   { CLONEOF("cameltry"), },
   { NULL, },
};

static struct ROM_INFO camel_try_alt_roms[] =
{
   {   "c38-01.bin", 0x00080000, 0xc170ff36, 0, 0, 0, },
   {   "c38-02.bin", 0x00020000, 0x1a11714b, 0, 0, 0, },
   {   "c38-03.bin", 0x00020000, 0x59fa59a7, 0, 0, 0, },
   {   "c38-08.bin", 0x00010000, 0x7ff78873, 0, 0, 0, },
   {       "c38-11", 0x00020000, 0xbe172da0, 0, 0, 0, },
   {       "c38-14", 0x00020000, 0xffa430de, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct ROMSW_DATA romsw_data_camel_try_alt_0[] =
{
   { "Taito Japan (Japanese)", 0x00 },
   { "Taito America",          0x01 },
   { "Taito Japan",            0x02 },
   { "Taito America (Romstar)",0x03 },
   { "Taito (Phoenix)",        0x04 },
   { NULL,                     0    },
};

static struct ROMSW_INFO camel_try_alt_romsw[] =
{
   { 0x03FFFF, 0x01, romsw_data_camel_try_alt_0 },
   { 0,        0,    NULL },
};

struct GAME_MAIN game_camel_try_alt =
{
   camel_try_alt_dirs,
   camel_try_alt_roms,
   camel_try_inputs,
   camel_try_dsw,
   camel_try_alt_romsw,

   load_camel_try,
   clear_camel_try,
   &camel_try_video,
   execute_camel_try_frame,
   "cameltru",
   "Camel Try (US)",
   "キャメルトライ (alternate)",
   COMPANY_ID_TAITO,
   "C38",
   1989,
   taito_ym2610_sound,
   GAME_PUZZLE,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static UINT8 *RAM_ROTATE;

static UINT8 *GFX_ROT;

void load_camel_try(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(GFX=AllocateMem(0x100000+0x100000))) return;

   GFX_ROT	= GFX+0x000000;
   GFX_SPR	= GFX+0x100000;

   tb=0;
   if(!load_rom_index(1, RAM+0x00000, 0x20000)) return;	// 8x8 TILES
   if(!load_rom_index(1, RAM+0x20000, 0x20000)) return;	// 8x8 TILES
   if(!load_rom_index(1, RAM+0x40000, 0x20000)) return;	// 8x8 TILES
   if(!load_rom_index(1, RAM+0x60000, 0x20000)) return;	// 8x8 TILES
   for(ta=0;ta<0x80000;ta++,tb+=2){
      GFX_ROT[tb+0]=RAM[ta]>>4;
      GFX_ROT[tb+1]=RAM[ta]&15;
   }

   tb=0;
   if(!load_rom_index(0, RAM, 0x80000)) return;		// 16x16 SPRITES ($1000)
   for(ta=0;ta<0x80000;ta++,tb+=2){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }

   if(!(ROM=AllocateMem(0x40000))) return;

   if(!load_rom_index(4, RAM, 0x20000)) return;		// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom_index(5, RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x48000;
   if(!load_rom_index(3, Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x20000))) return;
   if(!load_rom_index(2, PCMROM, 0x20000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x20000, 0x20000);

   AddTaitoYM2610(0x01E6, 0x0185, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_ROTATE = RAM+0x2C000;
   RAM_VIDEO  = RAM+0x18000;
   RAM_SCROLL = RAM+0x30200;
   RAM_INPUT  = RAM+0x30000;
   GFX_FG0    = RAM+0x40000;

   RAMSize=0x68000;

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x1000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x2E000, 0x100, 0x10, 0x1000);

   // 68000 Speed Hack

   WriteLong68k(&ROM[0x00BA2],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x00BA6],0x00AA0000);
   WriteWord68k(&ROM[0x00BAA],0x6100-16);	// bra.s <loop>

   // Set vcu type

   WriteWord68k(&ROM[0x3FF8C],0x0000);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=19;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM+0x10000;
   tc0200obj.RAM_B	= NULL;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x0FFF;
   tc0200obj.ofs_x	= 0-96;
   tc0200obj.ofs_y	= 0-19;
   tc0200obj.cols	= 16;

   init_tc0200obj();

   // Init tc0005rot emulation
   // ------------------------

   tc0005rot.RAM     = RAM_ROTATE;
   tc0005rot.RAM_SCR = RAM+0x30100;
   tc0005rot.GFX_ROT = GFX_ROT;

   init_tc0005rot(1);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);		// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x900000, 0x907FFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddReadByte(0x800000, 0x813FFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0xA00000, 0xA01FFF, NULL, RAM+0x02C000);			// SCREEN RAM (ROTATION)
   AddReadByte(0x320000, 0x320003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x300000, 0x30001F, NULL, RAM_INPUT);			// INPUT RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x900000, 0x907FFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddReadWord(0x800000, 0x813FFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x200000, 0x201FFF, NULL, RAM+0x02E000);			// COLOR RAM
   AddReadWord(0xA00000, 0xA01FFF, NULL, RAM+0x02C000);			// SCREEN RAM (ROTATION)
   AddReadWord(0x300000, 0x30001F, NULL, RAM_INPUT);			// INPUT RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x900000, 0x907FFF, NULL, RAM+0x010000);		// OBJECT RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x813FFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0xA00000, 0xA01FFF, NULL, RAM+0x02C000);		// SCREEN RAM (ROTATION)
   AddWriteByte(0x320000, 0x320003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x300000, 0x30001F, tc0220ioc_wb, NULL);		// INPUT RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x900000, 0x907FFF, NULL, RAM+0x010000);		// OBJECT RAM
   AddWriteWord(0xA00000, 0xA01FFF, tc0005rot_bg0_ww, NULL);		// SCREEN RAM (ROTATE)
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x813FFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x200000, 0x201FFF, NULL, RAM+0x02E000);		// COLOR RAM
   AddWriteWord(0xA02000, 0xA0200F, NULL, RAM+0x030100);		// SCROLL RAM (ROTATION)
   AddWriteWord(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0xD00000, 0xD0001F, NULL, RAM+0x030300);		// ? RAM
   AddWriteWord(0x300000, 0x30001F, tc0220ioc_ww, NULL);		// INPUT RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 

   GameMouse=1;
}

void clear_camel_try(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("RAM.bin",RAM,0x040000,1);
   #endif
}

void execute_camel_try_frame(void)
{
   static int p1x,p1y;
   static int p2x,p2y;
   int px,py;

   p1x=p1x/2;
   p1y=p1y/2;
   p2x=p2x/2;
   p2y=p2y/2;

   /*------[Mouse Hack]-------*/

   get_mouse_mickeys(&px,&py);

   p1x+=px/4;
   p1y+=py/4;

   if(mouse_b&1){RAM[0x30004]&=0x10^255;}

   if((RAM[0x30020]!=0)&&(p1x> -0x40)) p1x-=0x08;
   if((RAM[0x30021]!=0)&&(p1x<  0x3F)) p1x+=0x08;

   if((RAM[0x30030]!=0)&&(p2x> -0x40)) p2x-=0x08;
   if((RAM[0x30031]!=0)&&(p2x<  0x3F)) p2x+=0x08;

   WriteWord(&RAM[0x30018],p1x);
   WriteWord(&RAM[0x3001C],p2x);

   tc0005rot_set_bitmap();

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);

   Taito2610_FrameCameltry();		// Z80 and YM2610

   tc0005rot_unset_bitmap();
}

void draw_camel_try(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 1;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   if(RefreshBuffers){
      tc0005rot_refresh_buffer();
   }

   clear_game_screen(0);

   // OBJECT

   render_tc0200obj_mapped_cameltry(0xC0);
   render_tc0200obj_mapped_cameltry(0x80);

   // ROT

   if(ReadLong(&RAM[0x30310])!=0)
      tc0005rot_draw_rot((ReadWord(&RAM[0x30302])&0x3F)<<2);

   // OBJECT

   render_tc0200obj_mapped_cameltry(0x40);
   render_tc0200obj_mapped_cameltry(0x00);

   // FG0

   render_tc0100scn_layer_mapped(0,2);
}
