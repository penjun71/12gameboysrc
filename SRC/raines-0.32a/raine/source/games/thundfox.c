/******************************************************************************/
/*                                                                            */
/*                  THUNDER FOX (C) 1990 TAITO CORPORATION                    */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "thundfox.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

static struct DIR_INFO thunder_fox_dirs[] =
{
   { "thunder_fox", },
   { "thundfox", },
   { NULL, },
};

static struct ROM_INFO thunder_fox_roms[] =
{
   {   "c28scr1.01", 0x00080000, 0x6230a09d, 0, 0, 0, },
   {     "c28lo.07", 0x00020000, 0x24419abb, 0, 0, 0, },
   {  "c28mainh.13", 0x00020000, 0xacb07013, 0, 0, 0, },
   {  "c28mainl.12", 0x00020000, 0xf04db477, 0, 0, 0, },
   {   "c28objh.04", 0x00080000, 0xba7ed535, 0, 0, 0, },
   {   "c28objl.03", 0x00080000, 0x51bdc7af, 0, 0, 0, },
   {     "c28hi.08", 0x00020000, 0x38e038f1, 0, 0, 0, },
   {   "c28scr2.01", 0x00080000, 0x44552b25, 0, 0, 0, },
   {    "c28snd.14", 0x00010000, 0x45ef3616, 0, 0, 0, },
   {   "c28snda.06", 0x00080000, 0xdb6983db, 0, 0, 0, },
   {   "c28sndb.05", 0x00080000, 0xd3b238fa, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO thunder_fox_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x02E20E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x02E20E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x02E20E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x02E20E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x02E204, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x02E204, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x02E204, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x02E204, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x02E204, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x02E204, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x02E204, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x02E204, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x02E206, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x02E206, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x02E206, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x02E206, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x02E206, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x02E206, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x02E206, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x02E206, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_thunder_fox_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_thunder_fox_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Time Limit",            0x04, 0x02 },
   { MSG_ON,                  0x04, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { "Lives",                 0x30, 0x04 },
   { "3",                     0x30, 0x00 },
   { "2",                     0x20, 0x00 },
   { "4",                     0x10, 0x00 },
   { "5",                     0x00, 0x00 },
   { "Continue Play",         0x40, 0x02 },
   { MSG_ON,                  0x40, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { "Controls",              0x80, 0x02 },
   { "Dual",                  0x80, 0x00 },
   { "Single",                0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO thunder_fox_dsw[] =
{
   { 0x02E200, 0xFF, dsw_data_thunder_fox_0 },
   { 0x02E202, 0xFF, dsw_data_thunder_fox_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_thunder_fox_0[] =
{
   { "Taito Japan",           0x00 },
   { "Taito America",         0x01 },
   { "Taito",                 0x02 },
   { NULL,                    0    },
};

static struct ROMSW_INFO thunder_fox_romsw[] =
{
   { 0x03FFFF, 0x00, romsw_data_thunder_fox_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO thunder_fox_video =
{
   DrawThunderFox,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_thunder_fox =
{
   thunder_fox_dirs,
   thunder_fox_roms,
   thunder_fox_inputs,
   thunder_fox_dsw,
   thunder_fox_romsw,

   LoadThunderFox,
   ClearThunderFox,
   &thunder_fox_video,
   ExecuteThunderFoxFrame,
   "thundfox",
   "Thunder Fox",
   "サンダーフォックス",
   COMPANY_ID_TAITO,
   "C28",
   1990,
   taito_ym2610_sound,
   GAME_SHOOT,
};

static UINT8 *RAM_INPUT;

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;

static UINT8 *RAM_VIDEO2;
static UINT8 *RAM_SCROLL2;

static UINT8 *GFX_BG0_SOLID;
static UINT8 *GFX_BG2_SOLID;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG2;

static UINT8 *RAM_OBJECT;
static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static int SoundBad;

static void BadWriteByte(UINT32 address, UINT8 data)
{
#ifdef RAINE_DEBUG
      if(address!=0x800002)print_debug("Wb(%06x,%02x) [%06x]\n",address,data,s68000context.pc);
#endif
}
static void BadWriteWord(UINT32 address, UINT16 data)
{
   #ifdef RAINE_DEBUG
      if(address!=0x200000)print_debug("Ww(%06x,%04x) [%06x]\n",address,data,s68000context.pc);
   #endif
}

void LoadThunderFox(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(GFX=AllocateMem(0x200000+0x200000))) return;

   GFX_BG0=GFX+0x000000;
   GFX_BG2=GFX+0x100000;
   GFX_SPR=GFX+0x200000;

   tb=0;
   if(!load_rom("c28scr1.01", RAM, 0x100000)) return;		// 8x8 TILES
   for(ta=0;ta<0x80000;ta+=2,tb+=4){
      GFX[tb+3]=RAM[ta]&15;
      GFX[tb+2]=RAM[ta]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+0]=RAM[ta+1]>>4;
   }
   if(!load_rom("c28scr2.01", RAM, 0x80000)) return;		// 8x8 TILES
   for(ta=0;ta<0x80000;ta+=2,tb+=4){
      GFX[tb+3]=RAM[ta]&15;
      GFX[tb+2]=RAM[ta]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+0]=RAM[ta+1]>>4;
   }
   tb=0;
   if(!load_rom("c28objl.03", RAM, 0x100000)) return;		// 16x16 SPRITES ($2000)
   for(ta=0;ta<0x80000;ta++,tb+=4){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }
   tb=2;
   if(!load_rom("c28objh.04", RAM, 0x100000)) return;		// 16x16 SPRITES ($2000)
   for(ta=0;ta<0x80000;ta++,tb+=4){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }

   if(!(RAM=AllocateMem(0x40000+0x10000))) return;
   if(!(ROM=AllocateMem(0x80000))) return;

   if(!load_rom("c28mainh.13", RAM, 0x20000)) return;		// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("c28mainl.12", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("c28hi.08", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("c28lo.07", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("c28snd.14", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(Z80ROM[0x001F]==0x00){
      SoundBad=0;			// ROM is good
   }
   else{
      SoundBad=1;			// ROM is corrupt
   }

   if(SoundBad==0){

   if(!(PCMROM=AllocateMem(0x100000))) return;
   if(!load_rom("c28snda.06",PCMROM+0x00000,0x80000)) return;		// ADPCM A rom
   if(!load_rom("c28sndb.05",PCMROM+0x80000,0x80000)) return;		// ADPCM B rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x100000, 0x100000);

   AddTaitoYM2610(0x023A, 0x01BA, 0x10000);

   }

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_INPUT  = RAM+0x2E200;

   RAM_VIDEO  = RAM+0x04000;
   RAM_SCROLL = RAM+0x2E000;

   RAM_VIDEO2 = RAM+0x14000;
   RAM_SCROLL2= RAM+0x2E100;

   RAM_OBJECT = RAM+0x24000;

   GFX_FG0    = RAM+0x30000;
   GFX_FG1    = RAM+0x34000;

   RAMSize=0x38000;

   GFX_BG0_SOLID = make_solid_mask_8x8(GFX_BG0, 0x4000);
   GFX_BG2_SOLID = make_solid_mask_8x8(GFX_BG2, 0x4000);

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x2C000, 0x100, 0x10, 0x1000);


   // 68000 Speed Hack
   // ----------------

   WriteLong68k(&ROM[0x0A0C],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x0A10],0x00AA0000);
   WriteWord68k(&ROM[0x0A14],0x6100-16);	// bra.s <loop>

   WriteLong68k(&ROM[0x07B0],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x07B4],0x00AA0000);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX_BG0;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x3FFF;
   tc0100scn[0].layer[0].scr_x	=16;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX_BG0;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x3FFF;
   tc0100scn[0].layer[1].scr_x	=16;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=1;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=16;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   tc0100scn[1].layer[0].RAM	=RAM_VIDEO2 +0x0000;
   tc0100scn[1].layer[0].GFX	=GFX_BG2;
   tc0100scn[1].layer[0].MASK	=GFX_BG2_SOLID;
   tc0100scn[1].layer[0].SCR	=RAM_SCROLL2 +0;
   tc0100scn[1].layer[0].type	=0;
   tc0100scn[1].layer[0].bmp_x	=32;
   tc0100scn[1].layer[0].bmp_y	=32;
   tc0100scn[1].layer[0].bmp_w	=320;
   tc0100scn[1].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[1].layer[0].tile_mask=0x3FFF;
   tc0100scn[1].layer[0].scr_x	=17;
   tc0100scn[1].layer[0].scr_y	=15;

   tc0100scn[1].layer[1].RAM	=RAM_VIDEO2 +0x8000;
   tc0100scn[1].layer[1].GFX	=GFX_BG2;
   tc0100scn[1].layer[1].MASK	=GFX_BG2_SOLID;
   tc0100scn[1].layer[1].SCR	=RAM_SCROLL2 +2;
   tc0100scn[1].layer[1].type	=0;
   tc0100scn[1].layer[1].bmp_x	=32;
   tc0100scn[1].layer[1].bmp_y	=32;
   tc0100scn[1].layer[1].bmp_w	=320;
   tc0100scn[1].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[1].layer[1].tile_mask=0x3FFF;
   tc0100scn[1].layer[1].scr_x	=17;
   tc0100scn[1].layer[1].scr_y	=15;

   tc0100scn[1].layer[2].RAM	=RAM_VIDEO2 +0x4000;
   tc0100scn[1].layer[2].GFX	=GFX_FG1;
   tc0100scn[1].layer[2].SCR	=RAM_SCROLL2 +4;
   tc0100scn[1].layer[2].type	=1;
   tc0100scn[1].layer[2].bmp_x	=32;
   tc0100scn[1].layer[2].bmp_y	=32;
   tc0100scn[1].layer[2].bmp_w	=320;
   tc0100scn[1].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[1].layer[2].scr_x	=16;
   tc0100scn[1].layer[2].scr_y	=8;

   tc0100scn[1].RAM     = RAM_VIDEO2;
   tc0100scn[1].GFX_FG0 = GFX_FG1;

   init_tc0100scn(1);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x1FFF;
   tc0200obj.ofs_x	= -96;
   tc0200obj.ofs_y	= -16;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x30000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x300000, 0x303FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN0 RAM
   AddReadByte(0x500000, 0x50FFFF, NULL, RAM_VIDEO2);			// SCREEN1 RAM
   AddReadByte(0x600000, 0x607FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x200000, 0x20000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x220000, 0x220003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x300000, 0x303FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN0 RAM
   AddReadWord(0x500000, 0x50FFFF, NULL, RAM_VIDEO2);			// SCREEN1 RAM
   AddReadWord(0x600000, 0x607FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x100000, 0x101FFF, NULL, RAM+0x02C000);			// COLOR RAM
   AddReadWord(0x200000, 0x20000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x300000, 0x303FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x406000, 0x406FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN0 RAM
   AddWriteByte(0x506000, 0x506FFF, tc0100scn_1_gfx_fg0_wb, NULL);	// FG1 GFX RAM
   AddWriteByte(0x500000, 0x50FFFF, NULL, RAM_VIDEO2);			// SCREEN1 RAM
   AddWriteByte(0x600000, 0x607FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x220000, 0x220003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x200000, 0x20000F, tc0220ioc_wb, NULL);		// INPUT RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, BadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x300000, 0x303FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x406000, 0x406FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN0 RAM
   AddWriteWord(0x506000, 0x506FFF, tc0100scn_1_gfx_fg0_ww, NULL);	// FG1 GFX RAM
   AddWriteWord(0x500000, 0x50FFFF, NULL, RAM_VIDEO2);			// SCREEN1 RAM
   AddWriteWord(0x600000, 0x607FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x100000, 0x101FFF, NULL, RAM+0x02C000);		// COLOR RAM
   AddWriteWord(0x420000, 0x42000F, NULL, RAM_SCROLL);			// SCROLL0 RAM
   AddWriteWord(0x520000, 0x52000F, NULL, RAM_SCROLL2);			// SCROLL1 RAM
   AddWriteWord(0x200000, 0x20000F, tc0220ioc_ww, NULL);		// INPUT RAM
   AddWriteWord(0x000000, 0xFFFFFF, BadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearThunderFox(void)
{
   if(SoundBad==0){
   RemoveTaitoYM2610();
   }

   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x040000,1);
      //save_debug("GFX.bin",GFX,0x200000,0);
   #endif
}

void ExecuteThunderFoxFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   /*#ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif*/
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)

   if(SoundBad==0){
   Taito2610_Frame();			// Z80 and YM2610
   }
}

void DrawThunderFox(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL + 12);
   tc0100scn[1].ctrl = ReadWord(RAM_SCROLL2 + 12);

   // BG0 A
   // -----

   render_tc0100scn_layer_mapped(1,0);

   // BG0 B
   // -----

   render_tc0100scn_layer_mapped(0,0);

   // BG1 A
   // -----

   render_tc0100scn_layer_mapped(1,1);

   // BG1 B
   // -----

   render_tc0100scn_layer_mapped(0,1);

   // OBJECT
   // ------

   render_tc0200obj_mapped_b();

   // FG0 A
   // -----

   render_tc0100scn_layer_mapped(1,2);

   // FG0 B
   // -----

   render_tc0100scn_layer_mapped(0,2);
}
