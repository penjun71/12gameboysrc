/******************************************************************************/
/*                                                                            */
/*                    DRIFT OUT (C) 1991 VISCO CORPORATION                    */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "driftout.h"
#include "tc005rot.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO drift_out_dirs[] =
{
   { "drift_out", },
   { "driftout", },
   { NULL, },
};

static struct ROM_INFO drift_out_roms[] =
{
   {    "do_50.rom", 0x00010000, 0xffe10124, 0, 0, 0, },
   {    "do_46.rom", 0x00080000, 0xf960363e, 0, 0, 0, },
   {    "do_45.rom", 0x00080000, 0xe3fe66b9, 0, 0, 0, },
   {   "do_obj.rom", 0x00080000, 0x5491f1c4, 0, 0, 0, },
   {   "do_piv.rom", 0x00080000, 0xc4f012f7, 0, 0, 0, },
   {   "do_snd.rom", 0x00080000, 0xf2deb82b, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO drift_out_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x02000E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x02000E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x02000E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x02000E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x020004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x020004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x020004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x020004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x020004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x020004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x020004, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_drift_out_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_1COIN_2PLAY,         0x10, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_2COIN_1PLAY,         0x80, 0x00 },
   { MSG_1COIN_2PLAY,         0x40, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_drift_out_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Control",               0x0C, 0x04 },
   { "Lever",                 0x0C, 0x00 },
   { "Paddle A",              0x08, 0x00 },
   { "Lever",                 0x04, 0x00 },
   { "Paddle B",              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO drift_out_dsw[] =
{
   { 0x020000, 0xFF, dsw_data_drift_out_0 },
   { 0x020002, 0xFF, dsw_data_drift_out_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO drift_out_video =
{
   DrawDriftOut,
   224,
   320,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_drift_out =
{
   drift_out_dirs,
   drift_out_roms,
   drift_out_inputs,
   drift_out_dsw,
   NULL,

   LoadDriftOut,
   ClearDriftOut,
   &drift_out_video,
   ExecuteDriftOutFrame,
   "driftout",
   "Drift Out",
   "ドリフトアウト",
   COMPANY_ID_VISCO,
   NULL,
   1991,
   taito_ym2610_sound,
   GAME_RACE,
};

static struct DIR_INFO drive_out_dirs[] =
{
   { "drive_out", },
   { "driveout", },
   { ROMOF("driftout"), },
   { CLONEOF("driftout"), },
   { NULL, },
};

static struct ROM_INFO drive_out_roms[] =
{
   { "driveout.028", 0x00080000, 0xcbde0b66, 0, 0, 0, },
   { "driveout.003", 0x00080000, 0xdc431e4e, 0, 0, 0, },
   { "driveout.020", 0x00008000, 0x99aaeb2e, 0, 0, 0, },
   { "driveout.002", 0x00080000, 0x6f9063f4, 0, 0, 0, },
   { "driveout.029", 0x00020000, 0x0aba2026, 0, 0, 0, },
   { "do_piv.rom",   0x00080000, 0xc4f012f7, 0, 0, 0, },
   { "driveout.081", 0x00040000, 0x0e9a3e9e, 0, 0, 0, },
   { "driveout.084", 0x00040000, 0x530ac420, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_drive_out =
{
   drive_out_dirs,
   drive_out_roms,
   drift_out_inputs,
   drift_out_dsw,
   NULL,

   LoadDriveOut,
   ClearDriveOut,
   &drift_out_video,
   ExecuteDriftOutFrame,
   "driveout",
   "Drive Out",
   NULL,
   COMPANY_ID_BOOTLEG,
   NULL,
   1991,
   taito_ym2610_sound,
   GAME_RACE,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_ROTATE;
static UINT8 *RAM_INPUT;
static UINT8 *RAM_OBJECT;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static UINT8 *GFX_ROT;

void LoadDriftOut(void)
{
   int ta,tb;

   RAMSize=0x40000+0x10000;

   if(!(ROM=AllocateMem(0x80000))) return;
   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(GFX=AllocateMem(0x200000))) return;

   GFX_ROT = GFX+0x000000;
   GFX_SPR = GFX+0x100000;

   tb=0;
   if(!load_rom("do_piv.rom", ROM, 0x80000)) return;		// 8x8 TILES
   ROM[0]=0;								// Bad Byte?
   for(ta=0;ta<0x80000;ta++,tb+=2){
      GFX[tb+0]=ROM[ta]>>4;
      GFX[tb+1]=ROM[ta]&15;
   }
   tb=0;
   if(!load_rom("do_obj.rom", ROM, 0x80000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x80000;ta++,tb+=2){
      GFX_SPR[tb+0]=ROM[ta]&15;
      GFX_SPR[tb+1]=ROM[ta]>>4;
   }

   Rotate16x16(GFX_SPR,0x1000);
   Flip16x16_X(GFX_SPR,0x1000);

   if(!load_rom("do_46.rom", RAM, 0x40000)) return;		// 68000 ROM
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("do_45.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("do_50.rom", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("do_snd.rom",PCMROM,0x80000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x80000, 0x80000);

   AddTaitoYM2610(0x023A, 0x01BA, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_INPUT  = RAM+0x20000;
   RAM_OBJECT = RAM+0x1C000;
   RAM_VIDEO  = RAM+0x16000-0x4000;
   RAM_SCROLL = RAM+0x20020;
   RAM_ROTATE = RAM+0x10000;
   GFX_FG0    = RAM+0x21000;

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x1000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);
   InitPaletteMap(RAM+0x14000, 0x100, 0x10, 0x8000);

   // 68000 Speed Hack
   // ----------------

   WriteLong68k(&ROM[0x0724],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x0728],0x00AA0000);	//
   WriteWord68k(&ROM[0x072C],0x6100-14);	//	bra.s	<loop>

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT;
   tc0200obj.RAM_B	= NULL;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 224;
   tc0200obj.bmp_h	= 320;
// Mapper disabled
   tc0200obj.tile_mask	= 0x0FFF;
   tc0200obj.ofs_x	= -8;
   tc0200obj.ofs_y	= 8;

   init_tc0200obj();

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=1;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=224;
   tc0100scn[0].layer[2].bmp_h	=320;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=24;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0005rot emulation
   // ------------------------

   tc0005rot.RAM     = RAM_ROTATE;
   tc0005rot.RAM_SCR = RAM_ROTATE+0x2000;
   tc0005rot.GFX_ROT = GFX_ROT;

   init_tc0005rot(0);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x300000, 0x30FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0xB00000, 0xB0001F, NULL, RAM_INPUT);			// INPUT RAM
   AddReadByte(0x200000, 0x200003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x300000, 0x30FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x400000, 0x40200F, NULL, RAM_ROTATE);			// SCREEN/SCROLL (ROT)
   AddReadWord(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0xB00000, 0xB0001F, NULL, RAM_INPUT);			// INPUT RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x300000, 0x30FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x200000, 0x200003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0xB00000, 0xB0001F, tc0220ioc_wb, NULL);		// INPUT RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x300000, 0x30FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x400000, 0x401FFF, tc0005rot_bg0_ww, NULL);		// SCREEN RAM (ROTATE)
   AddWriteWord(0x700000, 0x701FFF, NULL, RAM+0x014000);		// COLOR RAM
   AddWriteWord(0x402000, 0x40200F, NULL, RAM+0x012000);		// SCROLL RAM (ROTATE)
   AddWriteWord(0x804000, 0x805FFF, NULL, RAM+0x016000);		// SCREEN RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww_r270, NULL);	// FG0 GFX RAM
   AddWriteWord(0x820000, 0x82000F, NULL, RAM+0x020020);		// SCROLL RAM
   AddWriteWord(0xA00000, 0xA0001F, NULL, RAM+0x020030);		// ??? RAM
   AddWriteWord(0xB00000, 0xB0001F, tc0220ioc_ww, NULL);		// INPUT RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void LoadDriveOut(void)
{
   int ta,tb;

   RAMSize=0x40000+0x10000;

   if(!(ROM=AllocateMem(0x80000))) return;
   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(GFX=AllocateMem(0x200000))) return;

   GFX_ROT = GFX+0x000000;
   GFX_SPR = GFX+0x100000;

   tb=0;
   if(!load_rom("do_piv.rom", ROM, 0x80000)) return;	// 8x8 TILES
   for(ta=0;ta<0x80000;ta++){
      GFX[tb++]=ROM[ta]>>4;
      GFX[tb++]=ROM[ta]&15;
   }

   tb=0;
   if(!load_rom("driveout.081", ROM+0x00000, 0x40000)) return; // 16x16 SPRITES
   if(!load_rom("driveout.084", ROM+0x40000, 0x40000)) return; // 16x16 SPRITES
   for(ta=0;ta<0x40000;ta++,tb+=4){
      GFX_SPR[tb+3]=ROM[ta+0x00000]>>4;
      GFX_SPR[tb+2]=ROM[ta+0x00000]&15;
      GFX_SPR[tb+1]=ROM[ta+0x40000]>>4;
      GFX_SPR[tb+0]=ROM[ta+0x40000]&15;
   }

   Rotate16x16(GFX_SPR,0x1000);
   Flip16x16_X(GFX_SPR,0x1000);

   if(!load_rom("do_46.rom", RAM, 0x40000)) return;		// 68000 ROM
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("do_45.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("do_50.rom", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("do_snd.rom",PCMROM,0x80000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x80000, 0x80000);

   AddTaitoYM2610(0x023A, 0x01BA, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_INPUT  = RAM+0x20000;
   RAM_OBJECT = RAM+0x1C000;
   RAM_VIDEO  = RAM+0x16000-0x4000;
   RAM_SCROLL = RAM+0x20020;
   RAM_ROTATE = RAM+0x10000;
   GFX_FG0    = RAM+0x21000;

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x1000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);
   InitPaletteMap(RAM+0x14000, 0x100, 0x10, 0x8000);

   // 68000 Speed Hack
   // ----------------

   WriteLong68k(&ROM[0x0724],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x0728],0x00AA0000);	//
   WriteWord68k(&ROM[0x072C],0x6100-14);	//	bra.s	<loop>

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT;
   tc0200obj.RAM_B	= NULL;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 224;
   tc0200obj.bmp_h	= 320;
// Mapper disabled
   tc0200obj.tile_mask	= 0x0FFF;
   tc0200obj.ofs_x	= -8;
   tc0200obj.ofs_y	= 8;

   init_tc0200obj();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=1;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=224;
   tc0100scn[0].layer[2].bmp_h	=320;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=24;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0005rot emulation
   // ------------------------

   tc0005rot.RAM     = RAM_ROTATE;
   tc0005rot.RAM_SCR = RAM_ROTATE+0x2000;
   tc0005rot.GFX_ROT = GFX_ROT;

   init_tc0005rot(0);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x300000, 0x30FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0xB00000, 0xB0001F, NULL, RAM_INPUT);			// INPUT RAM
   AddReadByte(0x200000, 0x200003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x300000, 0x30FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x400000, 0x40200F, NULL, RAM_ROTATE);			// SCREEN/SCROLL (ROT)
   AddReadWord(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0xB00000, 0xB0001F, NULL, RAM_INPUT);			// INPUT RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x300000, 0x30FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x200000, 0x200003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0xB00000, 0xB0001F, tc0220ioc_wb, NULL);		// INPUT RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x300000, 0x30FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x900000, 0x903FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x400000, 0x401FFF, tc0005rot_bg0_ww, NULL);		// SCREEN RAM (ROTATE)
   AddWriteWord(0x700000, 0x701FFF, NULL, RAM+0x014000);		// COLOR RAM
   AddWriteWord(0x402000, 0x40200F, NULL, RAM+0x012000);		// SCROLL RAM (ROTATE)
   AddWriteWord(0x804000, 0x805FFF, NULL, RAM+0x016000);		// SCREEN RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww_r270, NULL);	// FG0 GFX RAM
   AddWriteWord(0x820000, 0x82000F, NULL, RAM+0x020020);		// SCROLL RAM
   AddWriteWord(0xA00000, 0xA0001F, NULL, RAM+0x020030);		// ??? RAM
   AddWriteWord(0xB00000, 0xB0001F, tc0220ioc_ww, NULL);		// INPUT RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearDriftOut(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x040000,1);
      //save_debug("GFX.bin",GFX,0x200000,0);
   #endif
}

void ExecuteDriftOutFrame(void)
{
   tc0005rot_set_bitmap();

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);

   Taito2610_Frame();			// Z80 and YM2610

   tc0005rot_unset_bitmap();
}

void DrawDriftOut(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 1;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   if(RefreshBuffers){
      tc0005rot_refresh_buffer();
   }

   // ROTATE LAYER
   // ------------

   tc0005rot_draw_rot_r270((ReadWord(&RAM[0x20032])&0x3F)<<2);

   // OBJECT
   // ------

   render_tc0200obj_mapped_r270_b();

   // FG0
   // ---

   if(RAM[0x180B0]==0x7C){
   render_tc0100scn_layer_mapped_r270(0,2);
   }

}
