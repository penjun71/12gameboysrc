/******************************************************************************/
/*                                                                            */
/*                               RAINE GAME LISTS                             */
/*                                                                            */
/******************************************************************************/

#ifndef __raine_games_h_
#define __raine_games_h_

#include "deftypes.h"
#include "loadroms.h"

enum company_num
{
   COMPANY_ID_UNKNOWN = 0,
   COMPANY_ID_BOOTLEG,
   COMPANY_ID_CAPCOM,
   COMPANY_ID_DECO,
   COMPANY_ID_IREM,
   COMPANY_ID_JALECO,
   COMPANY_ID_KONAMI,
   COMPANY_ID_NICHIBUTSU,
   COMPANY_ID_SEGA,
   COMPANY_ID_SNK,
   COMPANY_ID_TAITO,
   COMPANY_ID_TECMO,
   COMPANY_ID_TOAPLAN,
   COMPANY_ID_BANPREST,
   COMPANY_ID_UPL,
   COMPANY_ID_NAMCO,
   COMPANY_ID_NTC,
   COMPANY_ID_VISCO,
   COMPANY_ID_FACE,
   COMPANY_ID_SEIBU,
   COMPANY_ID_SAMMY,
   COMPANY_ID_VIDEOSYSTEM,
   COMPANY_ID_ALPHA,
   COMPANY_ID_NAZCA,
   COMPANY_ID_TECHNOS,
   COMPANY_ID_NMK,
   COMPANY_ID_SUNSOFT,
   COMPANY_ID_AICOM,
   COMPANY_ID_YUMEKOBO,
   COMPANY_ID_HUDSON,
   COMPANY_ID_TAD,
   COMPANY_ID_COMAD,
   COMPANY_ID_SETA,
   COMPANY_ID_EX_SYSTEM,
   COMPANY_ID_KANEKO,
   COMPANY_ID_RAIZING,
   COMPANY_ID_CAVE,
   COMPANY_ID_ZEUS,
   COMPANY_ID_GAELCO,
   COMPANY_ID_PSIKYO,
   COMPANY_ID_EAST_TECHNOLOGY,
   COMPANY_ID_WILLIAMS,
   COMPANY_ID_HOT_B,
   COMPANY_ID_MARBLE,
};

extern const int nb_companies;

#define ROMOF(game) \
"#" game            \

#define CLONEOF(game) \
"$" game              \

#define IS_ROMOF(string) \
(string##[0] == '#')     \

#define IS_CLONEOF(string) \
(string##[0] == '$')       \

typedef struct INPUT_INFO
{
   UINT16  default_key;          // default input mapping
   UINT8 *name;                 // input name
   UINT32  offset;               // offset in (input) ram
   UINT8  bit_mask;             // bit mask
   UINT8  flags;                // type flags
} INPUT_INFO;

typedef struct DSW_DATA
{
   UINT8 *name;                 // dsw name          / dsw setting name
   UINT8  bit_mask;             // dsw bit mask      / dsw setting bit pattern
   UINT8  count;                // dsw setting count / zero
} DSW_DATA;

typedef struct DSW_INFO
{
   UINT32     offset;            // offset in (dsw) ram
   UINT8     factory_setting;   // dsw default data
   DSW_DATA *data;              // dsw data list
} DSW_INFO;

typedef struct ROMSW_DATA
{
   UINT8 *name;                 // romsw setting name
   UINT8  data;                 // romsw setting byte
} ROMSW_DATA;

typedef struct ROMSW_INFO
{
   UINT32       offset;          // offset in rom
   UINT8       factory_setting; // romsw default data
   ROMSW_DATA *data;            // romsw data list
} ROMSW_INFO;

/*

GFX LAYOUT

*/

#define MAX_GFX_PLANES   (8)
#define MAX_GFX_SIZE     (64)

#define RGN_FRAC(num,den)   (0x80000000 | (((num) & 0x0f) << 27) | (((den) & 0x0f) << 23))
#define IS_FRAC(offset)     ((offset) & 0x80000000)
#define FRAC_NUM(offset)    (((offset) >> 27) & 0x0f)
#define FRAC_DEN(offset)    (((offset) >> 23) & 0x0f)
#define FRAC_OFFSET(offset) ((offset) & 0x007fffff)

#define STEP4(START,STEP)   (START), (START)+1*(STEP), (START)+2*(STEP), (START)+3*(STEP)
#define STEP8(START,STEP)   STEP4(START,STEP), STEP4((START)+4*(STEP), STEP)
#define STEP16(START,STEP)  STEP8(START,STEP), STEP8((START)+8*(STEP), STEP)

typedef struct GFX_LAYOUT
{
   UINT16 width,height;                // width and height (in pixels) of chars/sprites
   UINT32 total;                       // total numer of chars/sprites in the rom
   UINT16 planes;                      // number of bitplanes
   UINT32 planeoffset[MAX_GFX_PLANES]; // start of every bitplane (in bits)
   UINT32 xoffset[MAX_GFX_SIZE];       // position of the bit corresponding to the pixel
   UINT32 yoffset[MAX_GFX_SIZE];       // of the given coordinates
   UINT16 charincrement;               // distance between two consecutive characters/sprites (in bits)
} GFX_LAYOUT;

typedef struct GFX_LIST
{
   UINT32       region;
   GFX_LAYOUT *layout;
} GFX_LIST;

/*

flags for VIDEO_INFO

*/

#define VIDEO_ROTATE_NORMAL	(0x00000000)
#define VIDEO_ROTATE_90		(0x00000001)
#define VIDEO_ROTATE_180	(0x00000002)
#define VIDEO_ROTATE_270	(0x00000003)

#define VIDEO_ROTATE(flags)	((flags >> 0) & 3)

#define VIDEO_ROTATABLE		(0x00000004)

#define VIDEO_FLIP_NORMAL	(0x00000000)
#define VIDEO_FLIP_X		(0x00000008)
#define VIDEO_FLIP_Y		(0x00000010)
#define VIDEO_FLIP_XY		(0x00000018)

#define VIDEO_FLIP(flags)	((flags >> 3) & 3)

#define VIDEO_NEEDS_16BPP       (0x00000020)

typedef struct VIDEO_INFO
{
   void       (*draw_game)();   // pointer to screen update function
   int        screen_x;         // screen width
   int        screen_y;         // screen height
   int        border_size;      // clipping border size
   UINT32      flags;		// extra info
   GFX_LIST  *gfx_list;
} VIDEO_INFO;

#ifdef interface
// stupid windows api...
#undef interface
#endif

typedef struct SOUND_INFO
{
   UINT32      type;             // chip type
   void      *interface;        // chip specific inteface
} SOUND_INFO;

/*

main game structure

*/

/* For the game type definitions : it might not be necessary to define one
   bit for each type, since the games should not require to have more than
   one type... Anyway, since I have 32 bits available, I will keep the bits
   definition for now */

#define GAME_BREAKOUT 1
#define GAME_SHOOT    2
#define GAME_BEAT     4
#define GAME_PUZZLE   8
#define GAME_PLATFORM 0x10
#define GAME_MISC     0x20
#define GAME_SPORTS   0x40
#define GAME_ADULT    0X80
#define GAME_RACE     0x100
#define GAME_QUIZZ    0x200

// Status
#define GAME_NOT_WORKING       0x10000
#define GAME_PARTIALLY_WORKING 0x20000
#define GAME_PRIVATE           0x40000

typedef struct GAME_MAIN
{
   DIR_INFO    *dir_list;       // dir list
   ROM_INFO    *rom_list;       // rom list
   INPUT_INFO  *input_list;     // input list
   DSW_INFO    *dsw_list;       // dsw list
   ROMSW_INFO  *romsw_list;     // romsw list

   void       (*load_game)();   // Pointer to load/init game function
   void       (*clear_game)();  // Pointer to clear game function
   VIDEO_INFO  *video_info;     // video hardware information
   void       (*exec_frame)();  // Pointer to exec game function

   /*

   game 'database' information

   */

   UINT8       *main_name;      // id game name (8 char, lower case)
   UINT8       *long_name;      // full game name
   UINT8       *long_name_jpn;  // full game name
   UINT8        company_id;     // company who produced the game
   UINT8       *board;          // board number (company specific)
   UINT32        year;           // year of release

   /*

   sound cpu information

   */

   SOUND_INFO  *sound_list;     // sound list
  UINT32 flags;
} GAME_MAIN;

/*

the list of all games

*/

extern struct GAME_MAIN *game_list[];

/*

the number of games in game_list

*/

int game_count;

/*

the currently loaded game (or NULL)

*/

struct GAME_MAIN *current_game;

/*

initialize game_list (and other things)

*/

void init_game_list(void);

/*

return string for company_id

*/

char *game_company_name(UINT8 company_id);

/*

check the current game

*/

int is_current_game(UINT8 *main_name);

GAME_MAIN *find_game(UINT8 *main_name); // in loadroms.c

#endif // __raine_games_h_
