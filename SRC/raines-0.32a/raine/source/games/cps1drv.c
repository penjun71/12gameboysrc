/*****************************************************************************/
/*                                                                           */
/*                   CAPCOM SYSTEM 1 / CPS1 (C) 1990 CAPCOM                  */
/* This contains only declarations for the gmaes. No code here               */
/*****************************************************************************/

#include "gameinc.h"
#include "cps1.h"
#include "sasound.h"

// Roms structs (generated)
// Except for pang3 : this one uses a CONTINUE_ROM in its gfx1 area.
// There is no direct equivalent in raine for now. You must change it
// my hand to load_normal, and load the 2 sets one after the other.

static struct ROM_INFO forgottn_roms[] =
{
  { "lwu11a", 0x20000, 0xddf78831 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "lwu15a", 0x20000, 0xf7ce2097 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "lwu10a", 0x20000, 0x8cb38c81 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "lwu14a", 0x20000, 0xd70ef9fd , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "lw-07", 0x80000, 0xfd252a26 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "lw-02", 0x80000, 0x43e6c5c8, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "lw-09", 0x80000, 0x899cb4ad, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "lw-06", 0x80000, 0x5b9edffc, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "lw-13", 0x80000, 0x8e058ef5, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "lw-01", 0x80000, 0x0318f298, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "lw-08", 0x80000, 0x25a8e43c, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "lw-05", 0x80000, 0xe4552fd7, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "lw-12", 0x80000, 0x8e6a832b, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "lwu00", 0x10000, 0x59df2a63 , REGION_ROM2, 0, LOAD_NORMAL },
  { "lw-03u", 0x20000, 0x807d051f , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "lw-04u", 0x20000, 0xe6cd098e , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO lostwrld_roms[] =
{
  { "lw-11c.14f", 0x20000, 0x67e42546 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "lw-15c.14g", 0x20000, 0x402e2a46 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "lw-10c.13f", 0x20000, 0xc46479d7 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "lw-14c.13g", 0x20000, 0x97670f4a , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "lw-07", 0x80000, 0xfd252a26 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "lw-02", 0x80000, 0x43e6c5c8, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "lw-09", 0x80000, 0x899cb4ad, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "lw-06", 0x80000, 0x5b9edffc, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "lw-13", 0x80000, 0x8e058ef5, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "lw-01", 0x80000, 0x0318f298, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "lw-08", 0x80000, 0x25a8e43c, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "lw-05", 0x80000, 0xe4552fd7, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "lw-12", 0x80000, 0x8e6a832b, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "lwu00", 0x10000, 0x59df2a63 , REGION_ROM2, 0, LOAD_NORMAL },
  { "lw-03.14c", 0x20000, 0xce2159e7 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "lw-04.13c", 0x20000, 0x39305536 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO ghouls_roms[] =
{
  { "ghl29.bin", 0x20000, 0x166a58a2 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "ghl30.bin", 0x20000, 0x7ac8407a , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "ghl27.bin", 0x20000, 0xf734b2be , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "ghl28.bin", 0x20000, 0x03d3e714 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ghl17.bin", 0x80000, 0x3ea1b0f2 , REGION_ROM1, 0x80000, LOAD_NORMAL },
  { "ghl5.bin", 0x80000, 0x0ba9c0b0, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ghl7.bin", 0x80000, 0x5d760ab9, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ghl6.bin", 0x80000, 0x4ba90b59, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ghl8.bin", 0x80000, 0x4bdee9de, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ghl09.bin", 0x10000, 0xae24bb19, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "ghl18.bin", 0x10000, 0xd34e271a, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "ghl13.bin", 0x10000, 0x3f70dd37, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "ghl22.bin", 0x10000, 0x7e69e2e6, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "ghl11.bin", 0x10000, 0x37c9b6c6, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "ghl20.bin", 0x10000, 0x2f1345b4, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "ghl15.bin", 0x10000, 0x3c2a212a, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "ghl24.bin", 0x10000, 0x889aac05, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "ghl10.bin", 0x10000, 0xbcc0f28c, REGION_GFX1, 0x280000, LOAD_8_64 },
  { "ghl19.bin", 0x10000, 0x2a40166a, REGION_GFX1, 0x280001, LOAD_8_64 },
  { "ghl14.bin", 0x10000, 0x20f85c03, REGION_GFX1, 0x280002, LOAD_8_64 },
  { "ghl23.bin", 0x10000, 0x8426144b, REGION_GFX1, 0x280003, LOAD_8_64 },
  { "ghl12.bin", 0x10000, 0xda088d61, REGION_GFX1, 0x280004, LOAD_8_64 },
  { "ghl21.bin", 0x10000, 0x17e11df0, REGION_GFX1, 0x280005, LOAD_8_64 },
  { "ghl16.bin", 0x10000, 0xf187ba1c, REGION_GFX1, 0x280006, LOAD_8_64 },
  { "ghl25.bin", 0x10000, 0x29f79c78, REGION_GFX1, 0x280007, LOAD_8_64 },
  { "ghl26.bin", 0x10000, 0x3692f6e5 , REGION_ROM2, 0, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO ghoulsu_roms[] =
{
  { "dmu29", 0x20000, 0x334d85b2 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "dmu30", 0x20000, 0xcee8ceb5 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "dmu27", 0x20000, 0x4a524140 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "dmu28", 0x20000, 0x94aae205 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ghl17.bin", 0x80000, 0x3ea1b0f2 , REGION_ROM1, 0x80000, LOAD_NORMAL },
  { "ghl5.bin", 0x80000, 0x0ba9c0b0, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ghl7.bin", 0x80000, 0x5d760ab9, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ghl6.bin", 0x80000, 0x4ba90b59, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ghl8.bin", 0x80000, 0x4bdee9de, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ghl09.bin", 0x10000, 0xae24bb19, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "ghl18.bin", 0x10000, 0xd34e271a, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "ghl13.bin", 0x10000, 0x3f70dd37, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "ghl22.bin", 0x10000, 0x7e69e2e6, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "ghl11.bin", 0x10000, 0x37c9b6c6, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "ghl20.bin", 0x10000, 0x2f1345b4, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "ghl15.bin", 0x10000, 0x3c2a212a, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "ghl24.bin", 0x10000, 0x889aac05, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "ghl10.bin", 0x10000, 0xbcc0f28c, REGION_GFX1, 0x280000, LOAD_8_64 },
  { "ghl19.bin", 0x10000, 0x2a40166a, REGION_GFX1, 0x280001, LOAD_8_64 },
  { "ghl14.bin", 0x10000, 0x20f85c03, REGION_GFX1, 0x280002, LOAD_8_64 },
  { "ghl23.bin", 0x10000, 0x8426144b, REGION_GFX1, 0x280003, LOAD_8_64 },
  { "ghl12.bin", 0x10000, 0xda088d61, REGION_GFX1, 0x280004, LOAD_8_64 },
  { "ghl21.bin", 0x10000, 0x17e11df0, REGION_GFX1, 0x280005, LOAD_8_64 },
  { "ghl16.bin", 0x10000, 0xf187ba1c, REGION_GFX1, 0x280006, LOAD_8_64 },
  { "ghl25.bin", 0x10000, 0x29f79c78, REGION_GFX1, 0x280007, LOAD_8_64 },
  { "ghl26.bin", 0x10000, 0x3692f6e5 , REGION_ROM2, 0, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO daimakai_roms[] =
{
  { "dmj_38.bin", 0x20000, 0x82fd1798 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "dmj_39.bin", 0x20000, 0x35366ccc , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "dmj_40.bin", 0x20000, 0xa17c170a , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "dmj_41.bin", 0x20000, 0x6af0b391 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ghl17.bin", 0x80000, 0x3ea1b0f2 , REGION_ROM1, 0x80000, LOAD_NORMAL },
  { "ghl5.bin", 0x80000, 0x0ba9c0b0, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ghl7.bin", 0x80000, 0x5d760ab9, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ghl6.bin", 0x80000, 0x4ba90b59, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ghl8.bin", 0x80000, 0x4bdee9de, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ghl09.bin", 0x10000, 0xae24bb19, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "ghl18.bin", 0x10000, 0xd34e271a, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "ghl13.bin", 0x10000, 0x3f70dd37, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "ghl22.bin", 0x10000, 0x7e69e2e6, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "ghl11.bin", 0x10000, 0x37c9b6c6, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "ghl20.bin", 0x10000, 0x2f1345b4, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "ghl15.bin", 0x10000, 0x3c2a212a, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "ghl24.bin", 0x10000, 0x889aac05, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "ghl10.bin", 0x10000, 0xbcc0f28c, REGION_GFX1, 0x280000, LOAD_8_64 },
  { "ghl19.bin", 0x10000, 0x2a40166a, REGION_GFX1, 0x280001, LOAD_8_64 },
  { "ghl14.bin", 0x10000, 0x20f85c03, REGION_GFX1, 0x280002, LOAD_8_64 },
  { "ghl23.bin", 0x10000, 0x8426144b, REGION_GFX1, 0x280003, LOAD_8_64 },
  { "ghl12.bin", 0x10000, 0xda088d61, REGION_GFX1, 0x280004, LOAD_8_64 },
  { "ghl21.bin", 0x10000, 0x17e11df0, REGION_GFX1, 0x280005, LOAD_8_64 },
  { "ghl16.bin", 0x10000, 0xf187ba1c, REGION_GFX1, 0x280006, LOAD_8_64 },
  { "ghl25.bin", 0x10000, 0x29f79c78, REGION_GFX1, 0x280007, LOAD_8_64 },
  { "ghl26.bin", 0x10000, 0x3692f6e5 , REGION_ROM2, 0, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO strider_roms[] =
{
  { "strider.30", 0x20000, 0xda997474 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "strider.35", 0x20000, 0x5463aaa3 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "strider.31", 0x20000, 0xd20786db , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "strider.36", 0x20000, 0x21aa2863 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "strider.32", 0x80000, 0x9b3cfc08 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "strider.06", 0x80000, 0x4eee9aea, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "strider.08", 0x80000, 0x2d7f21e4, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "strider.02", 0x80000, 0x7705aa46, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "strider.04", 0x80000, 0x5b18b722, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "strider.05", 0x80000, 0x005f000b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "strider.07", 0x80000, 0xb9441519, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "strider.01", 0x80000, 0xb7d04e8b, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "strider.03", 0x80000, 0x6b4713b4, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "strider.09", 0x10000, 0x2ed403bc , REGION_ROM2, 0, LOAD_NORMAL },
  { "strider.18", 0x20000, 0x4386bc80 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "strider.19", 0x20000, 0x444536d7 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO striderj_roms[] =
{
  { "sthj23.bin", 0x080000, 0x046e7b12 , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "strider.32", 0x80000, 0x9b3cfc08 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "strider.06", 0x80000, 0x4eee9aea, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "strider.08", 0x80000, 0x2d7f21e4, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "strider.02", 0x80000, 0x7705aa46, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "strider.04", 0x80000, 0x5b18b722, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "strider.05", 0x80000, 0x005f000b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "strider.07", 0x80000, 0xb9441519, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "strider.01", 0x80000, 0xb7d04e8b, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "strider.03", 0x80000, 0x6b4713b4, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "strider.09", 0x10000, 0x2ed403bc , REGION_ROM2, 0, LOAD_NORMAL },
  { "strider.18", 0x20000, 0x4386bc80 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "strider.19", 0x20000, 0x444536d7 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO stridrja_roms[] =
{
  { "sth36.bin", 0x20000, 0x53c7b006 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sth42.bin", 0x20000, 0x4037f65f , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sth37.bin", 0x20000, 0x80e8877d , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sth43.bin", 0x20000, 0x6b3fa466 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "strider.32", 0x80000, 0x9b3cfc08 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "strider.06", 0x80000, 0x4eee9aea, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "strider.08", 0x80000, 0x2d7f21e4, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "strider.02", 0x80000, 0x7705aa46, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "strider.04", 0x80000, 0x5b18b722, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "strider.05", 0x80000, 0x005f000b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "strider.07", 0x80000, 0xb9441519, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "strider.01", 0x80000, 0xb7d04e8b, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "strider.03", 0x80000, 0x6b4713b4, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "strider.09", 0x10000, 0x2ed403bc , REGION_ROM2, 0, LOAD_NORMAL },
  { "strider.18", 0x20000, 0x4386bc80 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "strider.19", 0x20000, 0x444536d7 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO dwj_roms[] =
{
  { "36.bin", 0x20000, 0x1a516657 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "42.bin", 0x20000, 0x12a290a0 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "37.bin", 0x20000, 0x932fc943 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "43.bin", 0x20000, 0x872ad76d , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "34.bin", 0x20000, 0x8f663d00 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "40.bin", 0x20000, 0x1586dbf3 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "35.bin", 0x20000, 0x9db93d7a , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "41.bin", 0x20000, 0x1aae69a4 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "09.bin", 0x20000, 0xc3e83c69, REGION_GFX1, 0x000000, LOAD_8_64 },
  { "01.bin", 0x20000, 0x187b2886, REGION_GFX1, 0x000001, LOAD_8_64 },
  { "13.bin", 0x20000, 0x0273d87d, REGION_GFX1, 0x000002, LOAD_8_64 },
  { "05.bin", 0x20000, 0x339378b8, REGION_GFX1, 0x000003, LOAD_8_64 },
  { "24.bin", 0x20000, 0xc6909b6f, REGION_GFX1, 0x000004, LOAD_8_64 },
  { "17.bin", 0x20000, 0x2e2f8320, REGION_GFX1, 0x000005, LOAD_8_64 },
  { "38.bin", 0x20000, 0xcd7923ed, REGION_GFX1, 0x000006, LOAD_8_64 },
  { "32.bin", 0x20000, 0x21a0a453, REGION_GFX1, 0x000007, LOAD_8_64 },
  { "10.bin", 0x20000, 0xff28f8d0, REGION_GFX1, 0x100000, LOAD_8_64 },
  { "02.bin", 0x20000, 0xcc83c02f, REGION_GFX1, 0x100001, LOAD_8_64 },
  { "14.bin", 0x20000, 0x18fb232c, REGION_GFX1, 0x100002, LOAD_8_64 },
  { "06.bin", 0x20000, 0x6f9edd75, REGION_GFX1, 0x100003, LOAD_8_64 },
  { "25.bin", 0x20000, 0x152ea74a, REGION_GFX1, 0x100004, LOAD_8_64 },
  { "18.bin", 0x20000, 0x1833f932, REGION_GFX1, 0x100005, LOAD_8_64 },
  { "39.bin", 0x20000, 0xbc09b360, REGION_GFX1, 0x100006, LOAD_8_64 },
  { "33.bin", 0x20000, 0x89de1533, REGION_GFX1, 0x100007, LOAD_8_64 },
  { "11.bin", 0x20000, 0x29eaf490, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "03.bin", 0x20000, 0x7bf51337, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "15.bin", 0x20000, 0xd36cdb91, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "07.bin", 0x20000, 0xe04af054, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "26.bin", 0x20000, 0x07fc714b, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "19.bin", 0x20000, 0x7114e5c6, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "28.bin", 0x20000, 0xaf62bf07, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "21.bin", 0x20000, 0x523f462a, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "12.bin", 0x20000, 0x38652339, REGION_GFX1, 0x300000, LOAD_8_64 },
  { "04.bin", 0x20000, 0x4951bc0f, REGION_GFX1, 0x300001, LOAD_8_64 },
  { "16.bin", 0x20000, 0x381608ae, REGION_GFX1, 0x300002, LOAD_8_64 },
  { "08.bin", 0x20000, 0xb475d4e9, REGION_GFX1, 0x300003, LOAD_8_64 },
  { "27.bin", 0x20000, 0xa27e81fa, REGION_GFX1, 0x300004, LOAD_8_64 },
  { "20.bin", 0x20000, 0x002796dc, REGION_GFX1, 0x300005, LOAD_8_64 },
  { "29.bin", 0x20000, 0x6b41f82d, REGION_GFX1, 0x300006, LOAD_8_64 },
  { "22.bin", 0x20000, 0x52145369, REGION_GFX1, 0x300007, LOAD_8_64 },
  { "23.bin", 0x10000, 0xb3b79d4f , REGION_ROM2, 0, LOAD_NORMAL },
  { "30.bin", 0x20000, 0x7e5f6cb4 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "31.bin", 0x20000, 0x4a30c737 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO willow_roms[] =
{
  { "wlu_30.rom", 0x20000, 0xd604dbb1 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "wlu_35.rom", 0x20000, 0xdaee72fe , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "wlu_31.rom", 0x20000, 0x0eb48a83 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "wlu_36.rom", 0x20000, 0x36100209 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "wl_32.rom", 0x80000, 0xdfd9f643 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "wl_gfx5.rom", 0x80000, 0xafa74b73, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "wl_gfx7.rom", 0x80000, 0x12a0dc0b, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "wl_gfx1.rom", 0x80000, 0xc6f2abce, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "wl_gfx3.rom", 0x80000, 0x4aa4c6d3, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "wl_24.rom", 0x20000, 0x6f0adee5, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "wl_14.rom", 0x20000, 0x9cf3027d, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "wl_26.rom", 0x20000, 0xf09c8ecf, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "wl_16.rom", 0x20000, 0xe35407aa, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "wl_20.rom", 0x20000, 0x84992350, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "wl_10.rom", 0x20000, 0xb87b5a36, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "wl_22.rom", 0x20000, 0xfd3f89f0, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "wl_12.rom", 0x20000, 0x7da49d69, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "wl_09.rom", 0x10000, 0xf6b3d060 , REGION_ROM2, 0, LOAD_NORMAL },
  { "wl_18.rom", 0x20000, 0xbde23d4d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "wl_19.rom", 0x20000, 0x683898f5 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO willowj_roms[] =
{
  { "wl36.bin", 0x20000, 0x2b0d7cbc , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "wl42.bin", 0x20000, 0x1ac39615 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "wl37.bin", 0x20000, 0x30a717fa , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "wl43.bin", 0x20000, 0xd0dddc9e , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "wl_32.rom", 0x80000, 0xdfd9f643 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "wl_gfx5.rom", 0x80000, 0xafa74b73, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "wl_gfx7.rom", 0x80000, 0x12a0dc0b, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "wl_gfx1.rom", 0x80000, 0xc6f2abce, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "wl_gfx3.rom", 0x80000, 0x4aa4c6d3, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "wl_24.rom", 0x20000, 0x6f0adee5, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "wl_14.rom", 0x20000, 0x9cf3027d, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "wl_26.rom", 0x20000, 0xf09c8ecf, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "wl_16.rom", 0x20000, 0xe35407aa, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "wl_20.rom", 0x20000, 0x84992350, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "wl_10.rom", 0x20000, 0xb87b5a36, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "wl_22.rom", 0x20000, 0xfd3f89f0, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "wl_12.rom", 0x20000, 0x7da49d69, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "wl_09.rom", 0x10000, 0xf6b3d060 , REGION_ROM2, 0, LOAD_NORMAL },
  { "wl_18.rom", 0x20000, 0xbde23d4d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "wl_19.rom", 0x20000, 0x683898f5 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO unsquad_roms[] =
{
  { "unsquad.30", 0x20000, 0x24d8f88d , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "unsquad.35", 0x20000, 0x8b954b59 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "unsquad.31", 0x20000, 0x33e9694b , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "unsquad.36", 0x20000, 0x7cc8fb9e , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "unsquad.32", 0x80000, 0xae1d7fb0 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "unsquad.05", 0x80000, 0xbf4575d8, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "unsquad.07", 0x80000, 0xa02945f4, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "unsquad.01", 0x80000, 0x5965ca8d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "unsquad.03", 0x80000, 0xac6db17d, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "unsquad.09", 0x10000, 0xf3dd1367 , REGION_ROM2, 0, LOAD_NORMAL },
  { "unsquad.18", 0x20000, 0x584b43a9 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO area88_roms[] =
{
  { "ar36.bin", 0x20000, 0x65030392 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "ar42.bin", 0x20000, 0xc48170de , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "unsquad.31", 0x20000, 0x33e9694b , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "unsquad.36", 0x20000, 0x7cc8fb9e , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "unsquad.32", 0x80000, 0xae1d7fb0 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "unsquad.05", 0x80000, 0xbf4575d8, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "unsquad.07", 0x80000, 0xa02945f4, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "unsquad.01", 0x80000, 0x5965ca8d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "unsquad.03", 0x80000, 0xac6db17d, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "unsquad.09", 0x10000, 0xf3dd1367 , REGION_ROM2, 0, LOAD_NORMAL },
  { "unsquad.18", 0x20000, 0x584b43a9 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO ffight_roms[] =
{
  { "ff30-36.bin", 0x20000, 0xf9a5ce83 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "ff35-42.bin", 0x20000, 0x65f11215 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "ff31-37.bin", 0x20000, 0xe1033784 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "ff36-43.bin", 0x20000, 0x995e968a , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ff32-32m.bin", 0x80000, 0xc747696e , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ff05-05m.bin", 0x80000, 0x9c284108, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ff07-07m.bin", 0x80000, 0xa7584dfb, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ff01-01m.bin", 0x80000, 0x0b605e44, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ff03-03m.bin", 0x80000, 0x52291cd2, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ff09-09.bin", 0x10000, 0xb8367eb5 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ff18-18.bin", 0x20000, 0x375c66e7 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ff19-19.bin", 0x20000, 0x1ef137f9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO ffightu_roms[] =
{
  { "36", 0x20000, 0xe2a48af9 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "42", 0x20000, 0xf4bb480e , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "37", 0x20000, 0xc371c667 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "43", 0x20000, 0x2f5771f9 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ff32-32m.bin", 0x80000, 0xc747696e , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ff05-05m.bin", 0x80000, 0x9c284108, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ff07-07m.bin", 0x80000, 0xa7584dfb, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ff01-01m.bin", 0x80000, 0x0b605e44, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ff03-03m.bin", 0x80000, 0x52291cd2, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ff09-09.bin", 0x10000, 0xb8367eb5 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ff18-18.bin", 0x20000, 0x375c66e7 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ff19-19.bin", 0x20000, 0x1ef137f9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO ffightj_roms[] =
{
  { "ff30-36.bin", 0x20000, 0xf9a5ce83 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "ff35-42.bin", 0x20000, 0x65f11215 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "ff31-37.bin", 0x20000, 0xe1033784 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "ff43.bin", 0x20000, 0xb6dee1c3 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ff32-32m.bin", 0x80000, 0xc747696e , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ff09.bin", 0x20000, 0x5b116d0d, REGION_GFX1, 0x000000, LOAD_8_64 },
  { "ff01.bin", 0x20000, 0x815b1797, REGION_GFX1, 0x000001, LOAD_8_64 },
  { "ff13.bin", 0x20000, 0x8721a7da, REGION_GFX1, 0x000002, LOAD_8_64 },
  { "ff05.bin", 0x20000, 0xd0fcd4b5, REGION_GFX1, 0x000003, LOAD_8_64 },
  { "ff24.bin", 0x20000, 0xa1ab607a, REGION_GFX1, 0x000004, LOAD_8_64 },
  { "ff17.bin", 0x20000, 0x2dc18cf4, REGION_GFX1, 0x000005, LOAD_8_64 },
  { "ff38.bin", 0x20000, 0x6535a57f, REGION_GFX1, 0x000006, LOAD_8_64 },
  { "ff32.bin", 0x20000, 0xc8bc4a57, REGION_GFX1, 0x000007, LOAD_8_64 },
  { "ff10.bin", 0x20000, 0x624a924a, REGION_GFX1, 0x100000, LOAD_8_64 },
  { "ff02.bin", 0x20000, 0x5d91f694, REGION_GFX1, 0x100001, LOAD_8_64 },
  { "ff14.bin", 0x20000, 0x0a2e9101, REGION_GFX1, 0x100002, LOAD_8_64 },
  { "ff06.bin", 0x20000, 0x1c18f042, REGION_GFX1, 0x100003, LOAD_8_64 },
  { "ff25.bin", 0x20000, 0x6e8181ea, REGION_GFX1, 0x100004, LOAD_8_64 },
  { "ff18.bin", 0x20000, 0xb19ede59, REGION_GFX1, 0x100005, LOAD_8_64 },
  { "ff39.bin", 0x20000, 0x9416b477, REGION_GFX1, 0x100006, LOAD_8_64 },
  { "ff33.bin", 0x20000, 0x7369fa07, REGION_GFX1, 0x100007, LOAD_8_64 },
  { "ff09-09.bin", 0x10000, 0xb8367eb5 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ff18-18.bin", 0x20000, 0x375c66e7 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ff19-19.bin", 0x20000, 0x1ef137f9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO _1941_roms[] =
{
  { "41e_30.rom", 0x20000, 0x9deb1e75 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "41e_35.rom", 0x20000, 0xd63942b3 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "41e_31.rom", 0x20000, 0xdf201112 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "41e_36.rom", 0x20000, 0x816a818f , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "41_32.rom", 0x80000, 0x4e9648ca , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "41_gfx5.rom", 0x80000, 0x01d1cb11, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "41_gfx7.rom", 0x80000, 0xaeaa3509, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "41_gfx1.rom", 0x80000, 0xff77985a, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "41_gfx3.rom", 0x80000, 0x983be58f, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "41_09.rom", 0x10000, 0x0f9d8527 , REGION_ROM2, 0, LOAD_NORMAL },
  { "41_18.rom", 0x20000, 0xd1f15aeb , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "41_19.rom", 0x20000, 0x15aec3a6 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO _1941j_roms[] =
{
  { "4136.bin", 0x20000, 0x7fbd42ab , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "4142.bin", 0x20000, 0xc7781f89 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "4137.bin", 0x20000, 0xc6464b0b , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "4143.bin", 0x20000, 0x440fc0b5 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "41_32.rom", 0x80000, 0x4e9648ca , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "41_gfx5.rom", 0x80000, 0x01d1cb11, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "41_gfx7.rom", 0x80000, 0xaeaa3509, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "41_gfx1.rom", 0x80000, 0xff77985a, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "41_gfx3.rom", 0x80000, 0x983be58f, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "41_09.rom", 0x10000, 0x0f9d8527 , REGION_ROM2, 0, LOAD_NORMAL },
  { "41_18.rom", 0x20000, 0xd1f15aeb , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "41_19.rom", 0x20000, 0x15aec3a6 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mercs_roms[] =
{
  { "so2_30e.rom", 0x20000, 0xe17f9bf7 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "so2_35e.rom", 0x20000, 0x78e63575 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "so2_31e.rom", 0x20000, 0x51204d36 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "so2_36e.rom", 0x20000, 0x9cfba8b4 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "so2_32.rom", 0x80000, 0x2eb5cf0c , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "so2_gfx6.rom", 0x80000, 0xaa6102af, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "so2_gfx8.rom", 0x80000, 0x839e6869, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "so2_gfx2.rom", 0x80000, 0x597c2875, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "so2_gfx4.rom", 0x80000, 0x912a9ca0, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "so2_24.rom", 0x20000, 0x3f254efe, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "so2_14.rom", 0x20000, 0xf5a8905e, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "so2_26.rom", 0x20000, 0xf3aa5a4a, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "so2_16.rom", 0x20000, 0xb43cd1a8, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "so2_20.rom", 0x20000, 0x8ca751a3, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "so2_10.rom", 0x20000, 0xe9f569fd, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "so2_22.rom", 0x20000, 0xfce9a377, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "so2_12.rom", 0x20000, 0xb7df8a06, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "so2_09.rom", 0x10000, 0xd09d7c7a , REGION_ROM2, 0, LOAD_NORMAL },
  { "so2_18.rom", 0x20000, 0xbbea1643 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "so2_19.rom", 0x20000, 0xac58aa71 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mercsu_roms[] =
{
  { "so2_30e.rom", 0x20000, 0xe17f9bf7 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "s02-35", 0x20000, 0x4477df61 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "so2_31e.rom", 0x20000, 0x51204d36 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "so2_36e.rom", 0x20000, 0x9cfba8b4 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "so2_32.rom", 0x80000, 0x2eb5cf0c , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "so2_gfx6.rom", 0x80000, 0xaa6102af, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "so2_gfx8.rom", 0x80000, 0x839e6869, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "so2_gfx2.rom", 0x80000, 0x597c2875, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "so2_gfx4.rom", 0x80000, 0x912a9ca0, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "so2_24.rom", 0x20000, 0x3f254efe, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "so2_14.rom", 0x20000, 0xf5a8905e, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "so2_26.rom", 0x20000, 0xf3aa5a4a, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "so2_16.rom", 0x20000, 0xb43cd1a8, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "so2_20.rom", 0x20000, 0x8ca751a3, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "so2_10.rom", 0x20000, 0xe9f569fd, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "so2_22.rom", 0x20000, 0xfce9a377, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "so2_12.rom", 0x20000, 0xb7df8a06, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "so2_09.rom", 0x10000, 0xd09d7c7a , REGION_ROM2, 0, LOAD_NORMAL },
  { "so2_18.rom", 0x20000, 0xbbea1643 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "so2_19.rom", 0x20000, 0xac58aa71 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mercsj_roms[] =
{
  { "so2_30e.rom", 0x20000, 0xe17f9bf7 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "so2_42.bin", 0x20000, 0x2c3884c6 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "so2_31e.rom", 0x20000, 0x51204d36 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "so2_36e.rom", 0x20000, 0x9cfba8b4 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "so2_32.rom", 0x80000, 0x2eb5cf0c , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "so2_gfx6.rom", 0x80000, 0xaa6102af, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "so2_gfx8.rom", 0x80000, 0x839e6869, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "so2_gfx2.rom", 0x80000, 0x597c2875, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "so2_gfx4.rom", 0x80000, 0x912a9ca0, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "so2_24.rom", 0x20000, 0x3f254efe, REGION_GFX1, 0x200000, LOAD_8_64 },
  { "so2_14.rom", 0x20000, 0xf5a8905e, REGION_GFX1, 0x200001, LOAD_8_64 },
  { "so2_26.rom", 0x20000, 0xf3aa5a4a, REGION_GFX1, 0x200002, LOAD_8_64 },
  { "so2_16.rom", 0x20000, 0xb43cd1a8, REGION_GFX1, 0x200003, LOAD_8_64 },
  { "so2_20.rom", 0x20000, 0x8ca751a3, REGION_GFX1, 0x200004, LOAD_8_64 },
  { "so2_10.rom", 0x20000, 0xe9f569fd, REGION_GFX1, 0x200005, LOAD_8_64 },
  { "so2_22.rom", 0x20000, 0xfce9a377, REGION_GFX1, 0x200006, LOAD_8_64 },
  { "so2_12.rom", 0x20000, 0xb7df8a06, REGION_GFX1, 0x200007, LOAD_8_64 },
  { "so2_09.rom", 0x10000, 0xd09d7c7a , REGION_ROM2, 0, LOAD_NORMAL },
  { "so2_18.rom", 0x20000, 0xbbea1643 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "so2_19.rom", 0x20000, 0xac58aa71 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mtwins_roms[] =
{
  { "che_30.rom", 0x20000, 0x9a2a2db1 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "che_35.rom", 0x20000, 0xa7f96b02 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "che_31.rom", 0x20000, 0xbbff8a99 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "che_36.rom", 0x20000, 0x0fa00c39 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ch_32.rom", 0x80000, 0x9b70bd41 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ch_gfx5.rom", 0x80000, 0x4ec75f15, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ch_gfx7.rom", 0x80000, 0xd85d00d6, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ch_gfx1.rom", 0x80000, 0xf33ca9d4, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ch_gfx3.rom", 0x80000, 0x0ba2047f, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ch_09.rom", 0x10000, 0x4d4255b7 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ch_18.rom", 0x20000, 0xf909e8de , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ch_19.rom", 0x20000, 0xfc158cf7 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO chikij_roms[] =
{
  { "chj36a.bin", 0x20000, 0xec1328d8 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "chj42a.bin", 0x20000, 0x4ae13503 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "chj37a.bin", 0x20000, 0x46d2cf7b , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "chj43a.bin", 0x20000, 0x8d387fe8 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ch_32.rom", 0x80000, 0x9b70bd41 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ch_gfx5.rom", 0x80000, 0x4ec75f15, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ch_gfx7.rom", 0x80000, 0xd85d00d6, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ch_gfx1.rom", 0x80000, 0xf33ca9d4, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ch_gfx3.rom", 0x80000, 0x0ba2047f, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ch_09.rom", 0x10000, 0x4d4255b7 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ch_18.rom", 0x20000, 0xf909e8de , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ch_19.rom", 0x20000, 0xfc158cf7 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO msword_roms[] =
{
  { "mse_30.rom", 0x20000, 0x03fc8dbc , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "mse_35.rom", 0x20000, 0xd5bf66cd , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "mse_31.rom", 0x20000, 0x30332bcf , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "mse_36.rom", 0x20000, 0x8f7d6ce9 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ms_32.rom", 0x80000, 0x2475ddfc , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ms_gfx5.rom", 0x80000, 0xc00fe7e2, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ms_gfx7.rom", 0x80000, 0x4ccacac5, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ms_gfx1.rom", 0x80000, 0x0d2bbe00, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ms_gfx3.rom", 0x80000, 0x3a1a5bf4, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ms_9.rom", 0x10000, 0x57b29519 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ms_18.rom", 0x20000, 0xfb64e90d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ms_19.rom", 0x20000, 0x74f892b9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mswordu_roms[] =
{
  { "msu30", 0x20000, 0xd963c816 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "msu35", 0x20000, 0x72f179b3 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "msu31", 0x20000, 0x20cd7904 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "msu36", 0x20000, 0xbf88c080 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ms_32.rom", 0x80000, 0x2475ddfc , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ms_gfx5.rom", 0x80000, 0xc00fe7e2, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ms_gfx7.rom", 0x80000, 0x4ccacac5, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ms_gfx1.rom", 0x80000, 0x0d2bbe00, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ms_gfx3.rom", 0x80000, 0x3a1a5bf4, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ms_9.rom", 0x10000, 0x57b29519 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ms_18.rom", 0x20000, 0xfb64e90d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ms_19.rom", 0x20000, 0x74f892b9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mswordj_roms[] =
{
  { "msj_36.bin", 0x20000, 0x04f0ef50 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "msj_42.bin", 0x20000, 0x9fcbb9cd , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "msj_37.bin", 0x20000, 0x6c060d70 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "msj_43.bin", 0x20000, 0xaec77787 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ms_32.rom", 0x80000, 0x2475ddfc , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ms_gfx5.rom", 0x80000, 0xc00fe7e2, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ms_gfx7.rom", 0x80000, 0x4ccacac5, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ms_gfx1.rom", 0x80000, 0x0d2bbe00, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ms_gfx3.rom", 0x80000, 0x3a1a5bf4, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ms_9.rom", 0x10000, 0x57b29519 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ms_18.rom", 0x20000, 0xfb64e90d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ms_19.rom", 0x20000, 0x74f892b9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO cawing_roms[] =
{
  { "cae_30a.rom", 0x20000, 0x91fceacd , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "cae_35a.rom", 0x20000, 0x3ef03083 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "cae_31a.rom", 0x20000, 0xe5b75caf , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "cae_36a.rom", 0x20000, 0xc73fd713 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "ca_32.rom", 0x80000, 0x0c4837d4 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "ca_gfx5.rom", 0x80000, 0x66d4cc37, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ca_gfx7.rom", 0x80000, 0xb6f896f2, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ca_gfx1.rom", 0x80000, 0x4d0620fd, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ca_gfx3.rom", 0x80000, 0x0b0341c3, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ca_9.rom", 0x10000, 0x96fe7485 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ca_18.rom", 0x20000, 0x4a613a2c , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ca_19.rom", 0x20000, 0x74584493 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO cawingj_roms[] =
{
  { "cae_30a.rom", 0x20000, 0x91fceacd , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "caj42a.bin", 0x20000, 0x039f8362 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "cae_31a.rom", 0x20000, 0xe5b75caf , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "cae_36a.rom", 0x20000, 0xc73fd713 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "caj34.bin", 0x20000, 0x51ea57f4 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "caj40.bin", 0x20000, 0x2ab71ae1 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "caj35.bin", 0x20000, 0x01d71973 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "caj41.bin", 0x20000, 0x3a43b538 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "caj09.bin", 0x20000, 0x41b0f9a6, REGION_GFX1, 0x000000, LOAD_8_64 },
  { "caj01.bin", 0x20000, 0x1002d0b8, REGION_GFX1, 0x000001, LOAD_8_64 },
  { "caj13.bin", 0x20000, 0x6f3948b2, REGION_GFX1, 0x000002, LOAD_8_64 },
  { "caj05.bin", 0x20000, 0x207373d7, REGION_GFX1, 0x000003, LOAD_8_64 },
  { "caj24.bin", 0x20000, 0xe356aad7, REGION_GFX1, 0x000004, LOAD_8_64 },
  { "caj17.bin", 0x20000, 0x540f2fd8, REGION_GFX1, 0x000005, LOAD_8_64 },
  { "caj38.bin", 0x20000, 0x2464d4ab, REGION_GFX1, 0x000006, LOAD_8_64 },
  { "caj32.bin", 0x20000, 0x9b5836b3, REGION_GFX1, 0x000007, LOAD_8_64 },
  { "caj10.bin", 0x20000, 0xbf8a5f52, REGION_GFX1, 0x100000, LOAD_8_64 },
  { "caj02.bin", 0x20000, 0x125b018d, REGION_GFX1, 0x100001, LOAD_8_64 },
  { "caj14.bin", 0x20000, 0x8458e7d7, REGION_GFX1, 0x100002, LOAD_8_64 },
  { "caj06.bin", 0x20000, 0xcf80e164, REGION_GFX1, 0x100003, LOAD_8_64 },
  { "caj25.bin", 0x20000, 0xcdd0204d, REGION_GFX1, 0x100004, LOAD_8_64 },
  { "caj18.bin", 0x20000, 0x29c1d4b1, REGION_GFX1, 0x100005, LOAD_8_64 },
  { "caj39.bin", 0x20000, 0xeea23b67, REGION_GFX1, 0x100006, LOAD_8_64 },
  { "caj33.bin", 0x20000, 0xdde3891f, REGION_GFX1, 0x100007, LOAD_8_64 },
  { "ca_9.rom", 0x10000, 0x96fe7485 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ca_18.rom", 0x20000, 0x4a613a2c , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "ca_19.rom", 0x20000, 0x74584493 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO nemo_roms[] =
{
  { "nme_30a.rom", 0x20000, 0xd2c03e56 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "nme_35a.rom", 0x20000, 0x5fd31661 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "nme_31a.rom", 0x20000, 0xb2bd4f6f , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "nme_36a.rom", 0x20000, 0xee9450e3 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "nm_32.rom", 0x80000, 0xd6d1add3 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "nm_gfx5.rom", 0x80000, 0x487b8747, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "nm_gfx7.rom", 0x80000, 0x203dc8c6, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "nm_gfx1.rom", 0x80000, 0x9e878024, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "nm_gfx3.rom", 0x80000, 0xbb01e6b6, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "nm_09.rom", 0x10000, 0x0f4b0581 , REGION_ROM2, 0, LOAD_NORMAL },
  { "nm_18.rom", 0x20000, 0xbab333d4 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "nm_19.rom", 0x20000, 0x2650a0a8 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO nemoj_roms[] =
{
  { "nm36.bin", 0x20000, 0xdaeceabb , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "nm42.bin", 0x20000, 0x55024740 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "nm37.bin", 0x20000, 0x619068b6 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "nm43.bin", 0x20000, 0xa948a53b , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "nm_32.rom", 0x80000, 0xd6d1add3 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "nm_gfx5.rom", 0x80000, 0x487b8747, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "nm_gfx7.rom", 0x80000, 0x203dc8c6, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "nm_gfx1.rom", 0x80000, 0x9e878024, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "nm_gfx3.rom", 0x80000, 0xbb01e6b6, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "nm_09.rom", 0x10000, 0x0f4b0581 , REGION_ROM2, 0, LOAD_NORMAL },
  { "nm_18.rom", 0x20000, 0xbab333d4 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "nm_19.rom", 0x20000, 0x2650a0a8 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2_roms[] =
{
  { "sf2_30a.bin", 0x20000, 0x57bd7051 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2e_37b.rom", 0x20000, 0x62691cdd , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2_31a.bin", 0x20000, 0xa673143d , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2_38a.bin", 0x20000, 0x4c2ccef7 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2_28a.bin", 0x20000, 0x4009955e , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2_35a.bin", 0x20000, 0x8c1f3994 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ua_roms[] =
{
  { "sf2u.30a", 0x20000, 0x08beb861 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2u.37a", 0x20000, 0xb7638d69 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2u.31a", 0x20000, 0x0d5394e0 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2u.38a", 0x20000, 0x42d6a79e , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2u.28a", 0x20000, 0x387a175c , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2u.35a", 0x20000, 0xa1a5adcc , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ub_roms[] =
{
  { "sf2_30a.bin", 0x20000, 0x57bd7051 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2u.37b", 0x20000, 0x4a54d479 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2_31a.bin", 0x20000, 0xa673143d , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2_38a.bin", 0x20000, 0x4c2ccef7 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2_28a.bin", 0x20000, 0x4009955e , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2_35a.bin", 0x20000, 0x8c1f3994 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ue_roms[] =
{
  { "sf2u.30e", 0x20000, 0xf37cd088 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2u.37e", 0x20000, 0x6c61a513 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2u.31e", 0x20000, 0x7c4771b4 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2u.38e", 0x20000, 0xa4bd0cd9 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2u.28e", 0x20000, 0xe3b95625 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2u.35e", 0x20000, 0x3648769a , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ui_roms[] =
{
  { "sf2u.30i", 0x20000, 0xfe39ee33 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2u.37i", 0x20000, 0x9df707dd , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2u.31i", 0x20000, 0x69a0a301 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2u.38i", 0x20000, 0x4cb46daf , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2u.28i", 0x20000, 0x1580be4c , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2u.35i", 0x20000, 0x1468d185 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2j_roms[] =
{
  { "sf2j30.bin", 0x20000, 0x79022b31 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2j37.bin", 0x20000, 0x516776ec , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2j31.bin", 0x20000, 0xfe15cb39 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2j38.bin", 0x20000, 0x38614d70 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2j28.bin", 0x20000, 0xd283187a , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2j35.bin", 0x20000, 0xd28158e4 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ja_roms[] =
{
  { "sf2_30a.bin", 0x20000, 0x57bd7051 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2j_37a.bin", 0x20000, 0x1e1f6844 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2_31a.bin", 0x20000, 0xa673143d , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2_38a.bin", 0x20000, 0x4c2ccef7 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2_28a.bin", 0x20000, 0x4009955e , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2_35a.bin", 0x20000, 0x8c1f3994 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2jc_roms[] =
{
  { "sf2_30c.bin", 0x20000, 0x8add35ec , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "sf2j_37c.bin", 0x20000, 0x0d74a256 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "sf2_31c.bin", 0x20000, 0xc4fff4a9 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "sf2_38c.bin", 0x20000, 0x8210fc0e , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "sf2_28c.bin", 0x20000, 0x6eddd5e8 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "sf2_35c.bin", 0x20000, 0x6bcb404c , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "sf2_29a.bin", 0x20000, 0xbb4af315 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "sf2_36a.bin", 0x20000, 0xc02a13eb , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "sf2_06.bin", 0x80000, 0x22c9cc8e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "sf2_08.bin", 0x80000, 0x57213be8, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "sf2_05.bin", 0x80000, 0xba529b4f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "sf2_07.bin", 0x80000, 0x4b1b33a8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "sf2_15.bin", 0x80000, 0x2c7e2229, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "sf2_17.bin", 0x80000, 0xb5548f17, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "sf2_14.bin", 0x80000, 0x14b84312, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "sf2_16.bin", 0x80000, 0x5e9cd89a, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "sf2_25.bin", 0x80000, 0x994bfa58, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "sf2_27.bin", 0x80000, 0x3e66ad9d, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "sf2_24.bin", 0x80000, 0xc1befaa8, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "sf2_26.bin", 0x80000, 0x0627c831, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "sf2_09.bin", 0x10000, 0xa4823a1b , REGION_ROM2, 0, LOAD_NORMAL },
  { "sf2_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "sf2_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO _3wonders_roms[] =
{
  { "rte.30a", 0x20000, 0xef5b8b33 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "rte.35a", 0x20000, 0x7d705529 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "rte.31a", 0x20000, 0x32835e5e , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "rte.36a", 0x20000, 0x7637975f , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "3wonders.28", 0x20000, 0x054137c8 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "3wonders.33", 0x20000, 0x7264cb1b , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "rte.29a", 0x20000, 0xcddaa919 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "rte.34a", 0x20000, 0xed52e7e5 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "3wonders.05", 0x80000, 0x86aef804, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "3wonders.07", 0x80000, 0x4f057110, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "3wonders.01", 0x80000, 0x902489d0, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "3wonders.03", 0x80000, 0xe35ce720, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "3wonders.06", 0x80000, 0x13cb0e7c, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "3wonders.08", 0x80000, 0x1f055014, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "3wonders.02", 0x80000, 0xe9a034f4, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "3wonders.04", 0x80000, 0xdf0eea8b, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "3wonders.09", 0x10000, 0xabfca165 , REGION_ROM2, 0, LOAD_NORMAL },
  { "3wonders.18", 0x20000, 0x26b211ab , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "3wonders.19", 0x20000, 0xdbe64ad0 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO _3wonderu_roms[] =
{
  { "3wonders.30", 0x20000, 0x0b156fd8 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "3wonders.35", 0x20000, 0x57350bf4 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "3wonders.31", 0x20000, 0x0e723fcc , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "3wonders.36", 0x20000, 0x523a45dc , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "3wonders.28", 0x20000, 0x054137c8 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "3wonders.33", 0x20000, 0x7264cb1b , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "3wonders.29", 0x20000, 0x37ba3e20 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "3wonders.34", 0x20000, 0xf99f46c0 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "3wonders.05", 0x80000, 0x86aef804, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "3wonders.07", 0x80000, 0x4f057110, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "3wonders.01", 0x80000, 0x902489d0, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "3wonders.03", 0x80000, 0xe35ce720, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "3wonders.06", 0x80000, 0x13cb0e7c, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "3wonders.08", 0x80000, 0x1f055014, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "3wonders.02", 0x80000, 0xe9a034f4, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "3wonders.04", 0x80000, 0xdf0eea8b, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "3wonders.09", 0x10000, 0xabfca165 , REGION_ROM2, 0, LOAD_NORMAL },
  { "3wonders.18", 0x20000, 0x26b211ab , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "3wonders.19", 0x20000, 0xdbe64ad0 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO wonder3_roms[] =
{
  { "rtj36.bin", 0x20000, 0xe3741247 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "rtj42.bin", 0x20000, 0xb4baa117 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "rtj37.bin", 0x20000, 0xa1f677b0 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "rtj43.bin", 0x20000, 0x85337a47 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "3wonders.28", 0x20000, 0x054137c8 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "3wonders.33", 0x20000, 0x7264cb1b , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "rtj35.bin", 0x20000, 0xe72f9ea3 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "rtj41.bin", 0x20000, 0xa11ee998 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "3wonders.05", 0x80000, 0x86aef804, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "3wonders.07", 0x80000, 0x4f057110, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "3wonders.01", 0x80000, 0x902489d0, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "3wonders.03", 0x80000, 0xe35ce720, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "3wonders.06", 0x80000, 0x13cb0e7c, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "3wonders.08", 0x80000, 0x1f055014, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "3wonders.02", 0x80000, 0xe9a034f4, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "3wonders.04", 0x80000, 0xdf0eea8b, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "rt23.bin", 0x10000, 0x7d5a77a7 , REGION_ROM2, 0, LOAD_NORMAL },
  { "3wonders.18", 0x20000, 0x26b211ab , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "3wonders.19", 0x20000, 0xdbe64ad0 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO kod_roms[] =
{
  { "kod30.rom", 0x20000, 0xc7414fd4 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "kod37.rom", 0x20000, 0xa5bf40d2 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "kod31.rom", 0x20000, 0x1fffc7bd , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "kod38.rom", 0x20000, 0x89e57a82 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "kod28.rom", 0x20000, 0x9367bcd9 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "kod35.rom", 0x20000, 0x4ca6a48a , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "kod29.rom", 0x20000, 0x6a0ba878 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "kod36.rom", 0x20000, 0xb509b39d , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "kod02.rom", 0x80000, 0xe45b8701, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kod04.rom", 0x80000, 0xa7750322, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kod01.rom", 0x80000, 0x5f74bf78, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kod03.rom", 0x80000, 0x5e5303bf, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kod11.rom", 0x80000, 0x113358f3, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kod13.rom", 0x80000, 0x38853c44, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kod10.rom", 0x80000, 0x9ef36604, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kod12.rom", 0x80000, 0x402b9b4f, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kod09.rom", 0x10000, 0xf5514510 , REGION_ROM2, 0, LOAD_NORMAL },
  { "kod18.rom", 0x20000, 0x69ecb2c8 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kod19.rom", 0x20000, 0x02d851c1 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO kodu_roms[] =
{
  { "kdu-30b.bin", 0x20000, 0x825817f9 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "kdu-37b.bin", 0x20000, 0xd2422dfb , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "kdu-31b.bin", 0x20000, 0x9af36039 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "kdu-38b.bin", 0x20000, 0xbe8405a1 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "kod28.rom", 0x20000, 0x9367bcd9 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "kod35.rom", 0x20000, 0x4ca6a48a , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "kd-29.bin", 0x20000, 0x0360fa72 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "kd-36a.bin", 0x20000, 0x95a3cef8 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "kod02.rom", 0x80000, 0xe45b8701, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kod04.rom", 0x80000, 0xa7750322, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kod01.rom", 0x80000, 0x5f74bf78, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kod03.rom", 0x80000, 0x5e5303bf, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kod11.rom", 0x80000, 0x113358f3, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kod13.rom", 0x80000, 0x38853c44, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kod10.rom", 0x80000, 0x9ef36604, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kod12.rom", 0x80000, 0x402b9b4f, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kd09.bin", 0x10000, 0xbac6ec26 , REGION_ROM2, 0, LOAD_NORMAL },
  { "kd18.bin", 0x20000, 0x4c63181d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kd19.bin", 0x20000, 0x92941b80 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO kodj_roms[] =
{
  { "kd30.bin", 0x20000, 0xebc788ad , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "kd37.bin", 0x20000, 0xe55c3529 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "kd31.bin", 0x20000, 0xc710d722 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "kd38.bin", 0x20000, 0x57d6ed3a , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "kd33.bin", 0x80000, 0x9bd7ad4b, REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "kod02.rom", 0x80000, 0xe45b8701, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kod04.rom", 0x80000, 0xa7750322, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kod01.rom", 0x80000, 0x5f74bf78, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kod03.rom", 0x80000, 0x5e5303bf, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kod11.rom", 0x80000, 0x113358f3, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kod13.rom", 0x80000, 0x38853c44, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kod10.rom", 0x80000, 0x9ef36604, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kod12.rom", 0x80000, 0x402b9b4f, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kd09.bin", 0x10000, 0xbac6ec26 , REGION_ROM2, 0, LOAD_NORMAL },
  { "kd18.bin", 0x20000, 0x4c63181d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kd19.bin", 0x20000, 0x92941b80 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO kodb_roms[] =
{
  { "kod.17", 0x080000, 0x036dd74c , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "kod.18", 0x080000, 0x3e4b7295 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "kod02.rom", 0x80000, 0xe45b8701, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kod04.rom", 0x80000, 0xa7750322, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kod01.rom", 0x80000, 0x5f74bf78, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kod03.rom", 0x80000, 0x5e5303bf, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kod11.rom", 0x80000, 0x113358f3, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kod13.rom", 0x80000, 0x38853c44, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kod10.rom", 0x80000, 0x9ef36604, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kod12.rom", 0x80000, 0x402b9b4f, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kod.15", 0x10000, 0x01cae60c , REGION_ROM2, 0, LOAD_NORMAL },
  { "kd18.bin", 0x20000, 0x4c63181d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kd19.bin", 0x20000, 0x92941b80 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO captcomm_roms[] =
{
  { "cce_23d.rom", 0x80000, 0x19c58ece , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "cc_22d.rom", 0x80000, 0xa91949b7 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "cc_24d.rom", 0x20000, 0x680e543f , REGION_ROM1, 0x100000, LOAD_8_16 },
  { "cc_28d.rom", 0x20000, 0x8820039f , REGION_ROM1, 0x100001, LOAD_8_16 },
  { "gfx_01.rom", 0x80000, 0x7261d8ba, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "gfx_03.rom", 0x80000, 0x6a60f949, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "gfx_02.rom", 0x80000, 0x00637302, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "gfx_04.rom", 0x80000, 0xcc87cf61, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "gfx_05.rom", 0x80000, 0x28718bed, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "gfx_07.rom", 0x80000, 0xd4acc53a, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "gfx_06.rom", 0x80000, 0x0c69f151, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "gfx_08.rom", 0x80000, 0x1f9ebb97, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "cc_09.rom", 0x10000, 0x698e8b58 , REGION_ROM2, 0, LOAD_NORMAL },
  { "cc_18.rom", 0x20000, 0x6de2c2db , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "cc_19.rom", 0x20000, 0xb99091ae , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO captcomu_roms[] =
{
  { "23b", 0x80000, 0x03da44fd , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "22c", 0x80000, 0x9b82a052 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "24b", 0x20000, 0x84ff99b2 , REGION_ROM1, 0x100000, LOAD_8_16 },
  { "28b", 0x20000, 0xfbcec223 , REGION_ROM1, 0x100001, LOAD_8_16 },
  { "gfx_01.rom", 0x80000, 0x7261d8ba, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "gfx_03.rom", 0x80000, 0x6a60f949, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "gfx_02.rom", 0x80000, 0x00637302, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "gfx_04.rom", 0x80000, 0xcc87cf61, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "gfx_05.rom", 0x80000, 0x28718bed, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "gfx_07.rom", 0x80000, 0xd4acc53a, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "gfx_06.rom", 0x80000, 0x0c69f151, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "gfx_08.rom", 0x80000, 0x1f9ebb97, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "cc_09.rom", 0x10000, 0x698e8b58 , REGION_ROM2, 0, LOAD_NORMAL },
  { "cc_18.rom", 0x20000, 0x6de2c2db , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "cc_19.rom", 0x20000, 0xb99091ae , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO captcomj_roms[] =
{
  { "cc23.bin", 0x80000, 0x5b482b62 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "cc22.bin", 0x80000, 0x0fd34195 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "cc24.bin", 0x20000, 0x3a794f25 , REGION_ROM1, 0x100000, LOAD_8_16 },
  { "cc28.bin", 0x20000, 0xfc3c2906 , REGION_ROM1, 0x100001, LOAD_8_16 },
  { "gfx_01.rom", 0x80000, 0x7261d8ba, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "gfx_03.rom", 0x80000, 0x6a60f949, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "gfx_02.rom", 0x80000, 0x00637302, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "gfx_04.rom", 0x80000, 0xcc87cf61, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "gfx_05.rom", 0x80000, 0x28718bed, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "gfx_07.rom", 0x80000, 0xd4acc53a, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "gfx_06.rom", 0x80000, 0x0c69f151, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "gfx_08.rom", 0x80000, 0x1f9ebb97, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "cc_09.rom", 0x10000, 0x698e8b58 , REGION_ROM2, 0, LOAD_NORMAL },
  { "cc_18.rom", 0x20000, 0x6de2c2db , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "cc_19.rom", 0x20000, 0xb99091ae , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO knights_roms[] =
{
  { "kr_23e.rom", 0x80000, 0x1b3997eb , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "kr_22.rom", 0x80000, 0xd0b671a9 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "kr_gfx1.rom", 0x80000, 0x9e36c1a4, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kr_gfx3.rom", 0x80000, 0xc5832cae, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kr_gfx2.rom", 0x80000, 0xf095be2d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kr_gfx4.rom", 0x80000, 0x179dfd96, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kr_gfx5.rom", 0x80000, 0x1f4298d2, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kr_gfx7.rom", 0x80000, 0x37fa8751, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kr_gfx6.rom", 0x80000, 0x0200bc3d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kr_gfx8.rom", 0x80000, 0x0bb2b4e7, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kr_09.rom", 0x10000, 0x5e44d9ee , REGION_ROM2, 0, LOAD_NORMAL },
  { "kr_18.rom", 0x20000, 0xda69d15f , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kr_19.rom", 0x20000, 0xbfc654e9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO knightsu_roms[] =
{
  { "kru23.rom", 0x80000, 0x252bc2ba , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "kr_22.rom", 0x80000, 0xd0b671a9 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "kr_gfx1.rom", 0x80000, 0x9e36c1a4, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kr_gfx3.rom", 0x80000, 0xc5832cae, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kr_gfx2.rom", 0x80000, 0xf095be2d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kr_gfx4.rom", 0x80000, 0x179dfd96, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kr_gfx5.rom", 0x80000, 0x1f4298d2, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kr_gfx7.rom", 0x80000, 0x37fa8751, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kr_gfx6.rom", 0x80000, 0x0200bc3d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kr_gfx8.rom", 0x80000, 0x0bb2b4e7, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kr_09.rom", 0x10000, 0x5e44d9ee , REGION_ROM2, 0, LOAD_NORMAL },
  { "kr_18.rom", 0x20000, 0xda69d15f , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kr_19.rom", 0x20000, 0xbfc654e9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO knightsj_roms[] =
{
  { "krj30.bin", 0x20000, 0xad3d1a8e , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "krj37.bin", 0x20000, 0xe694a491 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "krj31.bin", 0x20000, 0x85596094 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "krj38.bin", 0x20000, 0x9198bf8f , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "kr_22.rom", 0x80000, 0xd0b671a9 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "kr_gfx1.rom", 0x80000, 0x9e36c1a4, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "kr_gfx3.rom", 0x80000, 0xc5832cae, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "kr_gfx2.rom", 0x80000, 0xf095be2d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "kr_gfx4.rom", 0x80000, 0x179dfd96, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "kr_gfx5.rom", 0x80000, 0x1f4298d2, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "kr_gfx7.rom", 0x80000, 0x37fa8751, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "kr_gfx6.rom", 0x80000, 0x0200bc3d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "kr_gfx8.rom", 0x80000, 0x0bb2b4e7, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "kr_09.rom", 0x10000, 0x5e44d9ee , REGION_ROM2, 0, LOAD_NORMAL },
  { "kr_18.rom", 0x20000, 0xda69d15f , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "kr_19.rom", 0x20000, 0xbfc654e9 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ce_roms[] =
{
  { "sf2ce.23", 0x80000, 0x3f846b74 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "sf2ce.22", 0x80000, 0x99f1cca4 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "s92_21a.bin", 0x80000, 0x925a7877 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ceua_roms[] =
{
  { "s92u-23a", 0x80000, 0xac44415b , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "sf2ce.22", 0x80000, 0x99f1cca4 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "s92_21a.bin", 0x80000, 0x925a7877 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2ceub_roms[] =
{
  { "s92-23b", 0x80000, 0x996a3015 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "s92-22b", 0x80000, 0x2bbe15ed , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "s92-21b", 0x80000, 0xb383cb1c , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2cej_roms[] =
{
  { "s92j_23b.bin", 0x80000, 0x140876c5 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "s92j_22b.bin", 0x80000, 0x2fbb3bfe , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "s92_21a.bin", 0x80000, 0x925a7877 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2rb_roms[] =
{
  { "sf2d__23.rom", 0x80000, 0x450532b0 , REGION_ROM1, 0x000000, LOAD_NORMAL },
  { "sf2d__22.rom", 0x80000, 0xfe9d9cf5 , REGION_ROM1, 0x080000, LOAD_NORMAL },
  { "s92_21a.bin", 0x80000, 0x925a7877 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2rb2_roms[] =
{
  { "27.bin", 0x20000, 0x40296ecd , REGION_ROM1, 0x000000, LOAD_8_16 },
  { "31.bin", 0x20000, 0x87954a41 , REGION_ROM1, 0x000001, LOAD_8_16 },
  { "26.bin", 0x20000, 0xa6974195 , REGION_ROM1, 0x040000, LOAD_8_16 },
  { "30.bin", 0x20000, 0x8141fe32 , REGION_ROM1, 0x040001, LOAD_8_16 },
  { "25.bin", 0x20000, 0x9ef8f772 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "29.bin", 0x20000, 0x7d9c479c , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "24.bin", 0x20000, 0x93579684 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "28.bin", 0x20000, 0xff728865 , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "s92_21a.bin", 0x80000, 0x925a7877 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2red_roms[] =
{
  { "sf2red.23", 0x80000, 0x40276abb , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "sf2red.22", 0x80000, 0x18daf387 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "sf2red.21", 0x80000, 0x52c486bb , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2v004_roms[] =
{
  { "sf2v004.23", 0x80000, 0x52d19f2c , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "sf2v004.22", 0x80000, 0x4b26fde7 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "sf2red.21", 0x80000, 0x52c486bb , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2accp2_roms[] =
{
  { "sf2ca-23.bin", 0x80000, 0x36c3ba2f , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "sf2ca-22.bin", 0x80000, 0x0550453d , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "sf2ca-21.bin", 0x40000, 0x4c1c43ba , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s92_10.bin", 0x80000, 0x960687d5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s92_11.bin", 0x80000, 0x978ecd18, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s92_12.bin", 0x80000, 0xd6ec9a0a, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s92_13.bin", 0x80000, 0xed2c67f6, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO varth_roms[] =
{
  { "vae_30a.rom", 0x20000, 0x7fcd0091 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "vae_35a.rom", 0x20000, 0x35cf9509 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "vae_31a.rom", 0x20000, 0x15e5ee81 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "vae_36a.rom", 0x20000, 0x153a201e , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "vae_28a.rom", 0x20000, 0x7a0e0d25 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "vae_33a.rom", 0x20000, 0xf2365922 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "vae_29a.rom", 0x20000, 0x5e2cd2c3 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "vae_34a.rom", 0x20000, 0x3d9bdf83 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "va_gfx5.rom", 0x80000, 0xb1fb726e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "va_gfx7.rom", 0x80000, 0x4c6588cd, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "va_gfx1.rom", 0x80000, 0x0b1ace37, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "va_gfx3.rom", 0x80000, 0x44dfe706, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "va_09.rom", 0x10000, 0x7a99446e , REGION_ROM2, 0, LOAD_NORMAL },
  { "va_18.rom", 0x20000, 0xde30510e , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "va_19.rom", 0x20000, 0x0610a4ac , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO varthu_roms[] =
{
  { "vau23a.bin", 0x80000, 0xfbe68726 , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "vae_28a.rom", 0x20000, 0x7a0e0d25 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "vae_33a.rom", 0x20000, 0xf2365922 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "vae_29a.rom", 0x20000, 0x5e2cd2c3 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "vae_34a.rom", 0x20000, 0x3d9bdf83 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "va_gfx5.rom", 0x80000, 0xb1fb726e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "va_gfx7.rom", 0x80000, 0x4c6588cd, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "va_gfx1.rom", 0x80000, 0x0b1ace37, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "va_gfx3.rom", 0x80000, 0x44dfe706, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "va_09.rom", 0x10000, 0x7a99446e , REGION_ROM2, 0, LOAD_NORMAL },
  { "va_18.rom", 0x20000, 0xde30510e , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "va_19.rom", 0x20000, 0x0610a4ac , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO varthj_roms[] =
{
  { "vaj36b.bin", 0x20000, 0x1d798d6a , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "vaj42b.bin", 0x20000, 0x0f720233 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "vaj37b.bin", 0x20000, 0x24414b17 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "vaj43b.bin", 0x20000, 0x34b4b06c , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "vaj34b.bin", 0x20000, 0x87c79aed , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "vaj40b.bin", 0x20000, 0x210b4bd0 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "vaj35b.bin", 0x20000, 0x6b0da69f , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "vaj41b.bin", 0x20000, 0x6542c8a4 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "va_gfx5.rom", 0x80000, 0xb1fb726e, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "va_gfx7.rom", 0x80000, 0x4c6588cd, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "va_gfx1.rom", 0x80000, 0x0b1ace37, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "va_gfx3.rom", 0x80000, 0x44dfe706, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "va_09.rom", 0x10000, 0x7a99446e , REGION_ROM2, 0, LOAD_NORMAL },
  { "va_18.rom", 0x20000, 0xde30510e , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "va_19.rom", 0x20000, 0x0610a4ac , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO cworld2j_roms[] =
{
  { "q536.bin", 0x20000, 0x38a08099 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "q542.bin", 0x20000, 0x4d29b3a4 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "q537.bin", 0x20000, 0xeb547ebc , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "q543.bin", 0x20000, 0x3ef65ea8 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "q534.bin", 0x20000, 0x7fcc1317 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "q540.bin", 0x20000, 0x7f14b7b4 , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "q535.bin", 0x20000, 0xabacee26 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "q541.bin", 0x20000, 0xd3654067 , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "q509.bin", 0x20000, 0x48496d80, REGION_GFX1, 0x000000, LOAD_8_64 },
  { "q501.bin", 0x20000, 0xc5453f56, REGION_GFX1, 0x000001, LOAD_8_64 },
  { "q513.bin", 0x20000, 0xc741ac52, REGION_GFX1, 0x000002, LOAD_8_64 },
  { "q505.bin", 0x20000, 0x143e068f, REGION_GFX1, 0x000003, LOAD_8_64 },
  { "q524.bin", 0x20000, 0xb419d139, REGION_GFX1, 0x000004, LOAD_8_64 },
  { "q517.bin", 0x20000, 0xbd3b4d11, REGION_GFX1, 0x000005, LOAD_8_64 },
  { "q538.bin", 0x20000, 0x9c24670c, REGION_GFX1, 0x000006, LOAD_8_64 },
  { "q532.bin", 0x20000, 0x3ef9c7c2, REGION_GFX1, 0x000007, LOAD_8_64 },
  { "q510.bin", 0x20000, 0x119e5e93, REGION_GFX1, 0x100000, LOAD_8_64 },
  { "q502.bin", 0x20000, 0xa2cadcbe, REGION_GFX1, 0x100001, LOAD_8_64 },
  { "q514.bin", 0x20000, 0xa8755f82, REGION_GFX1, 0x100002, LOAD_8_64 },
  { "q506.bin", 0x20000, 0xc92a91fc, REGION_GFX1, 0x100003, LOAD_8_64 },
  { "q525.bin", 0x20000, 0x979237cb, REGION_GFX1, 0x100004, LOAD_8_64 },
  { "q518.bin", 0x20000, 0xc57da03c, REGION_GFX1, 0x100005, LOAD_8_64 },
  { "q539.bin", 0x20000, 0xa5839b25, REGION_GFX1, 0x100006, LOAD_8_64 },
  { "q533.bin", 0x20000, 0x04d03930, REGION_GFX1, 0x100007, LOAD_8_64 },
  { "q523.bin", 0x10000, 0xe14dc524 , REGION_ROM2, 0, LOAD_NORMAL },
  { "q530.bin", 0x20000, 0xd10c1b68 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "q531.bin", 0x20000, 0x7d17e496 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO wof_roms[] =
{
  { "tk2e_23b.rom", 0x80000, 0x11fb2ed1 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "tk2e_22b.rom", 0x80000, 0x479b3f24 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "tk2_gfx1.rom", 0x80000, 0x0d9cb9bf, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "tk2_gfx3.rom", 0x80000, 0x45227027, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "tk2_gfx2.rom", 0x80000, 0xc5ca2460, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "tk2_gfx4.rom", 0x80000, 0xe349551c, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "tk2_gfx5.rom", 0x80000, 0x291f0f0b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "tk2_gfx7.rom", 0x80000, 0x3edeb949, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "tk2_gfx6.rom", 0x80000, 0x1abd14d6, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "tk2_gfx8.rom", 0x80000, 0xb27948e3, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "tk2_qa.rom", 0x20000, 0xc9183a0d , REGION_ROM2, 0, LOAD_NORMAL },
  { "tk2_q1.rom", 0x80000, 0x611268cf , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "tk2_q2.rom", 0x80000, 0x20f55ca9 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "tk2_q3.rom", 0x80000, 0xbfcf6f52 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "tk2_q4.rom", 0x80000, 0x36642e88 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO wofa_roms[] =
{
  { "tk2a_23b.rom", 0x80000, 0x2e024628 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "tk2a_22b.rom", 0x80000, 0x900ad4cd , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "tk2_gfx1.rom", 0x80000, 0x0d9cb9bf, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "tk2_gfx3.rom", 0x80000, 0x45227027, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "tk2_gfx2.rom", 0x80000, 0xc5ca2460, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "tk2_gfx4.rom", 0x80000, 0xe349551c, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "tk2_gfx5.rom", 0x80000, 0x291f0f0b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "tk2_gfx7.rom", 0x80000, 0x3edeb949, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "tk2_gfx6.rom", 0x80000, 0x1abd14d6, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "tk2_gfx8.rom", 0x80000, 0xb27948e3, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "tk2_qa.rom", 0x20000, 0xc9183a0d , REGION_ROM2, 0, LOAD_NORMAL },
  { "tk2_q1.rom", 0x80000, 0x611268cf , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "tk2_q2.rom", 0x80000, 0x20f55ca9 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "tk2_q3.rom", 0x80000, 0xbfcf6f52 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "tk2_q4.rom", 0x80000, 0x36642e88 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO wofu_roms[] =
{
  { "tk2u.23c", 0x80000, 0x29b89c12 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "tk2u.22c", 0x80000, 0xf5af4774 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "tk2_gfx1.rom", 0x80000, 0x0d9cb9bf, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "tk2_gfx3.rom", 0x80000, 0x45227027, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "tk2_gfx2.rom", 0x80000, 0xc5ca2460, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "tk2_gfx4.rom", 0x80000, 0xe349551c, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "tk2_gfx5.rom", 0x80000, 0x291f0f0b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "tk2_gfx7.rom", 0x80000, 0x3edeb949, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "tk2_gfx6.rom", 0x80000, 0x1abd14d6, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "tk2_gfx8.rom", 0x80000, 0xb27948e3, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "tk2_qa.rom", 0x20000, 0xc9183a0d , REGION_ROM2, 0, LOAD_NORMAL },
  { "tk2_q1.rom", 0x80000, 0x611268cf , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "tk2_q2.rom", 0x80000, 0x20f55ca9 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "tk2_q3.rom", 0x80000, 0xbfcf6f52 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "tk2_q4.rom", 0x80000, 0x36642e88 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO wofj_roms[] =
{
  { "tk2j23c.bin", 0x80000, 0x9b215a68 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "tk2j22c.bin", 0x80000, 0xb74b09ac , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "tk2_gfx1.rom", 0x80000, 0x0d9cb9bf, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "tk2_gfx3.rom", 0x80000, 0x45227027, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "tk2_gfx2.rom", 0x80000, 0xc5ca2460, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "tk2_gfx4.rom", 0x80000, 0xe349551c, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "tk205.bin", 0x80000, 0xe4a44d53, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "tk206.bin", 0x80000, 0x58066ba8, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "tk207.bin", 0x80000, 0xd706568e, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "tk208.bin", 0x80000, 0xd4a19a02, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "tk2_qa.rom", 0x20000, 0xc9183a0d , REGION_ROM2, 0, LOAD_NORMAL },
  { "tk2_q1.rom", 0x80000, 0x611268cf , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "tk2_q2.rom", 0x80000, 0x20f55ca9 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "tk2_q3.rom", 0x80000, 0xbfcf6f52 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "tk2_q4.rom", 0x80000, 0x36642e88 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2t_roms[] =
{
  { "sf2.23", 0x80000, 0x89a1fc38 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "sf2_22.bin", 0x80000, 0xaea6e035 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "sf2_21.bin", 0x80000, 0xfd200288 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s2t_10.bin", 0x80000, 0x3c042686, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s2t_11.bin", 0x80000, 0x8b7e7183, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s2t_12.bin", 0x80000, 0x293c888c, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s2t_13.bin", 0x80000, 0x842b35a4, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO sf2tj_roms[] =
{
  { "s2tj_23.bin", 0x80000, 0xea73b4dc , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "s2t_22.bin", 0x80000, 0xaea6e035 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "s2t_21.bin", 0x80000, 0xfd200288 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "s92_01.bin", 0x80000, 0x03b0d852, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "s92_02.bin", 0x80000, 0x840289ec, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "s92_03.bin", 0x80000, 0xcdb5f027, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "s92_04.bin", 0x80000, 0xe2799472, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "s92_05.bin", 0x80000, 0xba8a2761, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "s92_06.bin", 0x80000, 0xe584bfb5, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "s92_07.bin", 0x80000, 0x21e3f87d, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "s92_08.bin", 0x80000, 0xbefc47df, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "s2t_10.bin", 0x80000, 0x3c042686, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "s2t_11.bin", 0x80000, 0x8b7e7183, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "s2t_12.bin", 0x80000, 0x293c888c, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "s2t_13.bin", 0x80000, 0x842b35a4, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "s92_09.bin", 0x10000, 0x08f6b60e , REGION_ROM2, 0, LOAD_NORMAL },
  { "s92_18.bin", 0x20000, 0x7f162009 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "s92_19.bin", 0x20000, 0xbeade53f , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO dino_roms[] =
{
  { "cde_23a.rom", 0x80000, 0x8f4e585e , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "cde_22a.rom", 0x80000, 0x9278aa12 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "cde_21a.rom", 0x80000, 0x66d23de2 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "cd_gfx01.rom", 0x80000, 0x8da4f917, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "cd_gfx03.rom", 0x80000, 0x6c40f603, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "cd_gfx02.rom", 0x80000, 0x09c8fc2d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "cd_gfx04.rom", 0x80000, 0x637ff38f, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "cd_gfx05.rom", 0x80000, 0x470befee, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "cd_gfx07.rom", 0x80000, 0x22bfb7a3, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "cd_gfx06.rom", 0x80000, 0xe7599ac4, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "cd_gfx08.rom", 0x80000, 0x211b4b15, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "cd_q.rom", 0x20000, 0x605fdb0b , REGION_ROM2, 0, LOAD_NORMAL },
  { "cd_q1.rom", 0x80000, 0x60927775 , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "cd_q2.rom", 0x80000, 0x770f4c47 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "cd_q3.rom", 0x80000, 0x2f273ffc , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "cd_q4.rom", 0x80000, 0x2c67821d , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO dinoj_roms[] =
{
  { "cdj-23a.8f", 0x80000, 0x5f3ece96 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "cdj-22a.7f", 0x80000, 0xa0d8de29 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "cde_21a.rom", 0x80000, 0x66d23de2 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "cd_gfx01.rom", 0x80000, 0x8da4f917, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "cd_gfx03.rom", 0x80000, 0x6c40f603, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "cd_gfx02.rom", 0x80000, 0x09c8fc2d, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "cd_gfx04.rom", 0x80000, 0x637ff38f, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "cd_gfx05.rom", 0x80000, 0x470befee, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "cd_gfx07.rom", 0x80000, 0x22bfb7a3, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "cd_gfx06.rom", 0x80000, 0xe7599ac4, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "cd_gfx08.rom", 0x80000, 0x211b4b15, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "cd_q.rom", 0x20000, 0x605fdb0b , REGION_ROM2, 0, LOAD_NORMAL },
  { "cd_q1.rom", 0x80000, 0x60927775 , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "cd_q2.rom", 0x80000, 0x770f4c47 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "cd_q3.rom", 0x80000, 0x2f273ffc , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "cd_q4.rom", 0x80000, 0x2c67821d , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO punisher_roms[] =
{
  { "pse_26.rom", 0x20000, 0x389a99d2 , REGION_ROM1, 0x000000, LOAD_8_16 },
  { "pse_30.rom", 0x20000, 0x68fb06ac , REGION_ROM1, 0x000001, LOAD_8_16 },
  { "pse_27.rom", 0x20000, 0x3eb181c3 , REGION_ROM1, 0x040000, LOAD_8_16 },
  { "pse_31.rom", 0x20000, 0x37108e7b , REGION_ROM1, 0x040001, LOAD_8_16 },
  { "pse_24.rom", 0x20000, 0x0f434414 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "pse_28.rom", 0x20000, 0xb732345d , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "pse_25.rom", 0x20000, 0xb77102e2 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "pse_29.rom", 0x20000, 0xec037bce , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "ps_21.rom", 0x80000, 0x8affa5a9 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "ps_gfx1.rom", 0x80000, 0x77b7ccab, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ps_gfx3.rom", 0x80000, 0x0122720b, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ps_gfx2.rom", 0x80000, 0x64fa58d4, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ps_gfx4.rom", 0x80000, 0x60da42c8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ps_gfx5.rom", 0x80000, 0xc54ea839, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "ps_gfx7.rom", 0x80000, 0x04c5acbd, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "ps_gfx6.rom", 0x80000, 0xa544f4cc, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "ps_gfx8.rom", 0x80000, 0x8f02f436, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "ps_q.rom", 0x20000, 0x49ff4446 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ps_q1.rom", 0x80000, 0x31fd8726 , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "ps_q2.rom", 0x80000, 0x980a9eef , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "ps_q3.rom", 0x80000, 0x0dd44491 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "ps_q4.rom", 0x80000, 0xbed42f03 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO punishru_roms[] =
{
  { "psu26.rom", 0x20000, 0x9236d121 , REGION_ROM1, 0x000000, LOAD_8_16 },
  { "psu30.rom", 0x20000, 0x8320e501 , REGION_ROM1, 0x000001, LOAD_8_16 },
  { "psu27.rom", 0x20000, 0x61c960a1 , REGION_ROM1, 0x040000, LOAD_8_16 },
  { "psu31.rom", 0x20000, 0x78d4c298 , REGION_ROM1, 0x040001, LOAD_8_16 },
  { "psu24.rom", 0x20000, 0x1cfecad7 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "psu28.rom", 0x20000, 0xbdf921c1 , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "psu25.rom", 0x20000, 0xc51acc94 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "psu29.rom", 0x20000, 0x52dce1ca , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "ps_21.rom", 0x80000, 0x8affa5a9 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "ps_gfx1.rom", 0x80000, 0x77b7ccab, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ps_gfx3.rom", 0x80000, 0x0122720b, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ps_gfx2.rom", 0x80000, 0x64fa58d4, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ps_gfx4.rom", 0x80000, 0x60da42c8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ps_gfx5.rom", 0x80000, 0xc54ea839, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "ps_gfx7.rom", 0x80000, 0x04c5acbd, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "ps_gfx6.rom", 0x80000, 0xa544f4cc, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "ps_gfx8.rom", 0x80000, 0x8f02f436, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "ps_q.rom", 0x20000, 0x49ff4446 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ps_q1.rom", 0x80000, 0x31fd8726 , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "ps_q2.rom", 0x80000, 0x980a9eef , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "ps_q3.rom", 0x80000, 0x0dd44491 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "ps_q4.rom", 0x80000, 0xbed42f03 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO punishrj_roms[] =
{
  { "psj23.bin", 0x80000, 0x6b2fda52 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "psj22.bin", 0x80000, 0xe01036bc , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "ps_21.rom", 0x80000, 0x8affa5a9 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "ps_gfx1.rom", 0x80000, 0x77b7ccab, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "ps_gfx3.rom", 0x80000, 0x0122720b, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "ps_gfx2.rom", 0x80000, 0x64fa58d4, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "ps_gfx4.rom", 0x80000, 0x60da42c8, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "ps_gfx5.rom", 0x80000, 0xc54ea839, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "ps_gfx7.rom", 0x80000, 0x04c5acbd, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "ps_gfx6.rom", 0x80000, 0xa544f4cc, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "ps_gfx8.rom", 0x80000, 0x8f02f436, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "ps_q.rom", 0x20000, 0x49ff4446 , REGION_ROM2, 0, LOAD_NORMAL },
  { "ps_q1.rom", 0x80000, 0x31fd8726 , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "ps_q2.rom", 0x80000, 0x980a9eef , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "ps_q3.rom", 0x80000, 0x0dd44491 , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "ps_q4.rom", 0x80000, 0xbed42f03 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO slammast_roms[] =
{
  { "mbe_23e.rom", 0x80000, 0x5394057a , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "mbe_24b.rom", 0x20000, 0x95d5e729 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "mbe_28b.rom", 0x20000, 0xb1c7cbcb , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "mbe_25b.rom", 0x20000, 0xa50d3fd4 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "mbe_29b.rom", 0x20000, 0x08e32e56 , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "mbe_21a.rom", 0x80000, 0xd5007b05 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "mbe_20a.rom", 0x80000, 0xaeb557b0 , REGION_ROM1, 0x180000, LOAD_SWAP_16 },
  { "mb_gfx01.rom", 0x80000, 0x41468e06, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "mb_gfx03.rom", 0x80000, 0xf453aa9e, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "mb_gfx02.rom", 0x80000, 0x2ffbfea8, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "mb_gfx04.rom", 0x80000, 0x1eb9841d, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "mb_05.bin", 0x80000, 0x506b9dc9, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "mb_07.bin", 0x80000, 0xaff8c2fb, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "mb_06.bin", 0x80000, 0xb76c70e9, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "mb_08.bin", 0x80000, 0xe60c9556, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "mb_10.bin", 0x80000, 0x97976ff5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "mb_12.bin", 0x80000, 0xb350a840, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "mb_11.bin", 0x80000, 0x8fb94743, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "mb_13.bin", 0x80000, 0xda810d5f, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "mb_qa.rom", 0x20000, 0xe21a03c4 , REGION_ROM2, 0, LOAD_NORMAL },
  { "mb_q1.bin", 0x80000, 0x0630c3ce , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "mb_q2.bin", 0x80000, 0x354f9c21 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "mb_q3.bin", 0x80000, 0x7838487c , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "mb_q4.bin", 0x80000, 0xab66e087 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { "mb_q5.bin", 0x80000, 0xc789fef2 , REGION_SMP1, 0x200000, LOAD_NORMAL },
  { "mb_q6.bin", 0x80000, 0xecb81b61 , REGION_SMP1, 0x280000, LOAD_NORMAL },
  { "mb_q7.bin", 0x80000, 0x041e49ba , REGION_SMP1, 0x300000, LOAD_NORMAL },
  { "mb_q8.bin", 0x80000, 0x59fe702a , REGION_SMP1, 0x380000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO slammasu_roms[] =
{
  { "mbu-23e.rom", 0x80000, 0x224f0062 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "mbe_24b.rom", 0x20000, 0x95d5e729 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "mbe_28b.rom", 0x20000, 0xb1c7cbcb , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "mbe_25b.rom", 0x20000, 0xa50d3fd4 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "mbe_29b.rom", 0x20000, 0x08e32e56 , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "mbe_21a.rom", 0x80000, 0xd5007b05 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "mbu-20a.rom", 0x80000, 0xfc848af5 , REGION_ROM1, 0x180000, LOAD_SWAP_16 },
  { "mb_gfx01.rom", 0x80000, 0x41468e06, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "mb_gfx03.rom", 0x80000, 0xf453aa9e, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "mb_gfx02.rom", 0x80000, 0x2ffbfea8, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "mb_gfx04.rom", 0x80000, 0x1eb9841d, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "mb_05.bin", 0x80000, 0x506b9dc9, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "mb_07.bin", 0x80000, 0xaff8c2fb, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "mb_06.bin", 0x80000, 0xb76c70e9, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "mb_08.bin", 0x80000, 0xe60c9556, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "mb_10.bin", 0x80000, 0x97976ff5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "mb_12.bin", 0x80000, 0xb350a840, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "mb_11.bin", 0x80000, 0x8fb94743, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "mb_13.bin", 0x80000, 0xda810d5f, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "mb_qa.rom", 0x20000, 0xe21a03c4 , REGION_ROM2, 0, LOAD_NORMAL },
  { "mb_q1.bin", 0x80000, 0x0630c3ce , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "mb_q2.bin", 0x80000, 0x354f9c21 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "mb_q3.bin", 0x80000, 0x7838487c , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "mb_q4.bin", 0x80000, 0xab66e087 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { "mb_q5.bin", 0x80000, 0xc789fef2 , REGION_SMP1, 0x200000, LOAD_NORMAL },
  { "mb_q6.bin", 0x80000, 0xecb81b61 , REGION_SMP1, 0x280000, LOAD_NORMAL },
  { "mb_q7.bin", 0x80000, 0x041e49ba , REGION_SMP1, 0x300000, LOAD_NORMAL },
  { "mb_q8.bin", 0x80000, 0x59fe702a , REGION_SMP1, 0x380000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mbomberj_roms[] =
{
  { "mbj23e", 0x80000, 0x0d06036a , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "mbe_24b.rom", 0x20000, 0x95d5e729 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "mbe_28b.rom", 0x20000, 0xb1c7cbcb , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "mbe_25b.rom", 0x20000, 0xa50d3fd4 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "mbe_29b.rom", 0x20000, 0x08e32e56 , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "mbe_21a.rom", 0x80000, 0xd5007b05 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "mbe_20a.rom", 0x80000, 0xaeb557b0 , REGION_ROM1, 0x180000, LOAD_SWAP_16 },
  { "mbj_01.bin", 0x80000, 0xa53b1c81, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "mbj_03.bin", 0x80000, 0x23fe10f6, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "mbj_02.bin", 0x80000, 0xcb866c2f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "mbj_04.bin", 0x80000, 0xc9143e75, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "mb_05.bin", 0x80000, 0x506b9dc9, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "mb_07.bin", 0x80000, 0xaff8c2fb, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "mb_06.bin", 0x80000, 0xb76c70e9, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "mb_08.bin", 0x80000, 0xe60c9556, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "mb_10.bin", 0x80000, 0x97976ff5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "mb_12.bin", 0x80000, 0xb350a840, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "mb_11.bin", 0x80000, 0x8fb94743, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "mb_13.bin", 0x80000, 0xda810d5f, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "mb_qa.rom", 0x20000, 0xe21a03c4 , REGION_ROM2, 0, LOAD_NORMAL },
  { "mb_q1.bin", 0x80000, 0x0630c3ce , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "mb_q2.bin", 0x80000, 0x354f9c21 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "mb_q3.bin", 0x80000, 0x7838487c , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "mb_q4.bin", 0x80000, 0xab66e087 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { "mb_q5.bin", 0x80000, 0xc789fef2 , REGION_SMP1, 0x200000, LOAD_NORMAL },
  { "mb_q6.bin", 0x80000, 0xecb81b61 , REGION_SMP1, 0x280000, LOAD_NORMAL },
  { "mb_q7.bin", 0x80000, 0x041e49ba , REGION_SMP1, 0x300000, LOAD_NORMAL },
  { "mb_q8.bin", 0x80000, 0x59fe702a , REGION_SMP1, 0x380000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mbombrd_roms[] =
{
  { "mbd_26.bin", 0x20000, 0x72b7451c , REGION_ROM1, 0x000000, LOAD_8_16 },
  { "mbde_30.rom", 0x20000, 0xa036dc16 , REGION_ROM1, 0x000001, LOAD_8_16 },
  { "mbd_27.bin", 0x20000, 0x4086f534 , REGION_ROM1, 0x040000, LOAD_8_16 },
  { "mbd_31.bin", 0x20000, 0x085f47f0 , REGION_ROM1, 0x040001, LOAD_8_16 },
  { "mbd_24.bin", 0x20000, 0xc20895a5 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "mbd_28.bin", 0x20000, 0x2618d5e1 , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "mbd_25.bin", 0x20000, 0x9bdb6b11 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "mbd_29.bin", 0x20000, 0x3f52d5e5 , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "mbd_21.bin", 0x80000, 0x690c026a , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "mbd_20.bin", 0x80000, 0xb8b2139b , REGION_ROM1, 0x180000, LOAD_SWAP_16 },
  { "mb_gfx01.rom", 0x80000, 0x41468e06, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "mb_gfx03.rom", 0x80000, 0xf453aa9e, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "mb_gfx02.rom", 0x80000, 0x2ffbfea8, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "mb_gfx04.rom", 0x80000, 0x1eb9841d, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "mb_05.bin", 0x80000, 0x506b9dc9, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "mb_07.bin", 0x80000, 0xaff8c2fb, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "mb_06.bin", 0x80000, 0xb76c70e9, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "mb_08.bin", 0x80000, 0xe60c9556, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "mb_10.bin", 0x80000, 0x97976ff5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "mb_12.bin", 0x80000, 0xb350a840, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "mb_11.bin", 0x80000, 0x8fb94743, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "mb_13.bin", 0x80000, 0xda810d5f, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "mb_q.bin", 0x20000, 0xd6fa76d1 , REGION_ROM2, 0, LOAD_NORMAL },
  { "mb_q1.bin", 0x80000, 0x0630c3ce , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "mb_q2.bin", 0x80000, 0x354f9c21 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "mb_q3.bin", 0x80000, 0x7838487c , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "mb_q4.bin", 0x80000, 0xab66e087 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { "mb_q5.bin", 0x80000, 0xc789fef2 , REGION_SMP1, 0x200000, LOAD_NORMAL },
  { "mb_q6.bin", 0x80000, 0xecb81b61 , REGION_SMP1, 0x280000, LOAD_NORMAL },
  { "mb_q7.bin", 0x80000, 0x041e49ba , REGION_SMP1, 0x300000, LOAD_NORMAL },
  { "mb_q8.bin", 0x80000, 0x59fe702a , REGION_SMP1, 0x380000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO mbombrdj_roms[] =
{
  { "mbd_26.bin", 0x20000, 0x72b7451c , REGION_ROM1, 0x000000, LOAD_8_16 },
  { "mbdj_30.bin", 0x20000, 0xbeff31cf , REGION_ROM1, 0x000001, LOAD_8_16 },
  { "mbd_27.bin", 0x20000, 0x4086f534 , REGION_ROM1, 0x040000, LOAD_8_16 },
  { "mbd_31.bin", 0x20000, 0x085f47f0 , REGION_ROM1, 0x040001, LOAD_8_16 },
  { "mbd_24.bin", 0x20000, 0xc20895a5 , REGION_ROM1, 0x080000, LOAD_8_16 },
  { "mbd_28.bin", 0x20000, 0x2618d5e1 , REGION_ROM1, 0x080001, LOAD_8_16 },
  { "mbd_25.bin", 0x20000, 0x9bdb6b11 , REGION_ROM1, 0x0c0000, LOAD_8_16 },
  { "mbd_29.bin", 0x20000, 0x3f52d5e5 , REGION_ROM1, 0x0c0001, LOAD_8_16 },
  { "mbd_21.bin", 0x80000, 0x690c026a , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "mbd_20.bin", 0x80000, 0xb8b2139b , REGION_ROM1, 0x180000, LOAD_SWAP_16 },
  { "mbj_01.bin", 0x80000, 0xa53b1c81, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "mbj_03.bin", 0x80000, 0x23fe10f6, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "mbj_02.bin", 0x80000, 0xcb866c2f, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "mbj_04.bin", 0x80000, 0xc9143e75, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "mb_05.bin", 0x80000, 0x506b9dc9, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "mb_07.bin", 0x80000, 0xaff8c2fb, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "mb_06.bin", 0x80000, 0xb76c70e9, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "mb_08.bin", 0x80000, 0xe60c9556, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "mb_10.bin", 0x80000, 0x97976ff5, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "mb_12.bin", 0x80000, 0xb350a840, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "mb_11.bin", 0x80000, 0x8fb94743, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "mb_13.bin", 0x80000, 0xda810d5f, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "mb_q.bin", 0x20000, 0xd6fa76d1 , REGION_ROM2, 0, LOAD_NORMAL },
  { "mb_q1.bin", 0x80000, 0x0630c3ce , REGION_SMP1, 0x000000, LOAD_NORMAL },
  { "mb_q2.bin", 0x80000, 0x354f9c21 , REGION_SMP1, 0x080000, LOAD_NORMAL },
  { "mb_q3.bin", 0x80000, 0x7838487c , REGION_SMP1, 0x100000, LOAD_NORMAL },
  { "mb_q4.bin", 0x80000, 0xab66e087 , REGION_SMP1, 0x180000, LOAD_NORMAL },
  { "mb_q5.bin", 0x80000, 0xc789fef2 , REGION_SMP1, 0x200000, LOAD_NORMAL },
  { "mb_q6.bin", 0x80000, 0xecb81b61 , REGION_SMP1, 0x280000, LOAD_NORMAL },
  { "mb_q7.bin", 0x80000, 0x041e49ba , REGION_SMP1, 0x300000, LOAD_NORMAL },
  { "mb_q8.bin", 0x80000, 0x59fe702a , REGION_SMP1, 0x380000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO pnickj_roms[] =
{
  { "pnij36.bin", 0x20000, 0x2d4ffb2b , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "pnij42.bin", 0x20000, 0xc085dfaf , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "pnij09.bin", 0x20000, 0x48177b0a, REGION_GFX1, 0x000000, LOAD_8_64 },
  { "pnij01.bin", 0x20000, 0x01a0f311, REGION_GFX1, 0x000001, LOAD_8_64 },
  { "pnij13.bin", 0x20000, 0x406451b0, REGION_GFX1, 0x000002, LOAD_8_64 },
  { "pnij05.bin", 0x20000, 0x8c515dc0, REGION_GFX1, 0x000003, LOAD_8_64 },
  { "pnij26.bin", 0x20000, 0xe2af981e, REGION_GFX1, 0x000004, LOAD_8_64 },
  { "pnij18.bin", 0x20000, 0xf17a0e56, REGION_GFX1, 0x000005, LOAD_8_64 },
  { "pnij38.bin", 0x20000, 0xeb75bd8c, REGION_GFX1, 0x000006, LOAD_8_64 },
  { "pnij32.bin", 0x20000, 0x84560bef, REGION_GFX1, 0x000007, LOAD_8_64 },
  { "pnij10.bin", 0x20000, 0xc2acc171, REGION_GFX1, 0x100000, LOAD_8_64 },
  { "pnij02.bin", 0x20000, 0x0e21fc33, REGION_GFX1, 0x100001, LOAD_8_64 },
  { "pnij14.bin", 0x20000, 0x7fe59b19, REGION_GFX1, 0x100002, LOAD_8_64 },
  { "pnij06.bin", 0x20000, 0x79f4bfe3, REGION_GFX1, 0x100003, LOAD_8_64 },
  { "pnij27.bin", 0x20000, 0x83d5cb0e, REGION_GFX1, 0x100004, LOAD_8_64 },
  { "pnij19.bin", 0x20000, 0xaf08b230, REGION_GFX1, 0x100005, LOAD_8_64 },
  { "pnij39.bin", 0x20000, 0x70fbe579, REGION_GFX1, 0x100006, LOAD_8_64 },
  { "pnij33.bin", 0x20000, 0x3ed2c680, REGION_GFX1, 0x100007, LOAD_8_64 },
  { "pnij17.bin", 0x10000, 0xe86f787a , REGION_ROM2, 0, LOAD_NORMAL },
  { "pnij24.bin", 0x20000, 0x5092257d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "pnij25.bin", 0x20000, 0x22109aaa , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO qad_roms[] =
{
  { "qdu_36a.rom", 0x20000, 0xde9c24a0 , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "qdu_42a.rom", 0x20000, 0xcfe36f0c , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "qdu_37a.rom", 0x20000, 0x10d22320 , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "qdu_43a.rom", 0x20000, 0x15e6beb9 , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "qdu_09.rom", 0x20000, 0x8c3f9f44, REGION_GFX1, 0x000000, LOAD_8_64 },
  { "qdu_01.rom", 0x20000, 0xf688cf8f, REGION_GFX1, 0x000001, LOAD_8_64 },
  { "qdu_13.rom", 0x20000, 0xafbd551b, REGION_GFX1, 0x000002, LOAD_8_64 },
  { "qdu_05.rom", 0x20000, 0xc3db0910, REGION_GFX1, 0x000003, LOAD_8_64 },
  { "qdu_24.rom", 0x20000, 0x2f1bd0ec, REGION_GFX1, 0x000004, LOAD_8_64 },
  { "qdu_17.rom", 0x20000, 0xa812f9e2, REGION_GFX1, 0x000005, LOAD_8_64 },
  { "qdu_38.rom", 0x20000, 0xccdddd1f, REGION_GFX1, 0x000006, LOAD_8_64 },
  { "qdu_32.rom", 0x20000, 0xa8d295d3, REGION_GFX1, 0x000007, LOAD_8_64 },
  { "qdu_23.rom", 0x10000, 0xcfb5264b , REGION_ROM2, 0, LOAD_NORMAL },
  { "qdu_30.rom", 0x20000, 0xf190da84 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "qdu_31.rom", 0x20000, 0xb7583f73 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO qadj_roms[] =
{
  { "qad23a.bin", 0x080000, 0x4d3553de , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "qad22a.bin", 0x80000, 0x3191ddd0 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "qad01.bin", 0x80000, 0x9d853b57, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "qad02.bin", 0x80000, 0xb35976c4, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "qad03.bin", 0x80000, 0xcea4ca8c, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "qad04.bin", 0x80000, 0x41b74d1b, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "qad09.bin", 0x10000, 0x733161cc , REGION_ROM2, 0, LOAD_NORMAL },
  { "qad18.bin", 0x20000, 0x2bfe6f6a , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "qad19.bin", 0x20000, 0x13d3236b , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO qtono2_roms[] =
{
  { "tn2j-30.11e", 0x20000, 0x9226eb5e , REGION_ROM1, 0x00000, LOAD_8_16 },
  { "tn2j-37.11f", 0x20000, 0xd1d30da1 , REGION_ROM1, 0x00001, LOAD_8_16 },
  { "tn2j-31.12e", 0x20000, 0x015e6a8a , REGION_ROM1, 0x40000, LOAD_8_16 },
  { "tn2j-38.12f", 0x20000, 0x1f139bcc , REGION_ROM1, 0x40001, LOAD_8_16 },
  { "tn2j-28.9e", 0x20000, 0x86d27f71 , REGION_ROM1, 0x80000, LOAD_8_16 },
  { "tn2j-35.9f", 0x20000, 0x7a1ab87d , REGION_ROM1, 0x80001, LOAD_8_16 },
  { "tn2j-29.10e", 0x20000, 0x9c384e99 , REGION_ROM1, 0xc0000, LOAD_8_16 },
  { "tn2j-36.10f", 0x20000, 0x4c4b2a0a , REGION_ROM1, 0xc0001, LOAD_8_16 },
  { "tn2-02m.4a", 0x80000, 0xf2016a34, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "tn2-04m.6a", 0x80000, 0x094e0fb1, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "tn2-01m.3a", 0x80000, 0xcb950cf9, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "tn2-03m.5a", 0x80000, 0x18a5bf59, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "tn2-11m.4c", 0x80000, 0xd0edd30b, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "tn2-13m.6c", 0x80000, 0x426621c3, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "tn2-10m.3c", 0x80000, 0xa34ece70, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "tn2-12m.5c", 0x80000, 0xe04ff2f4, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "tn2j-09.12a", 0x08000, 0x6d8edcef , REGION_ROM2, 0x00000, LOAD_NORMAL },
  { "tn2j-18.11c", 0x20000, 0xa40bf9a7 , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "tn2j-19.12c", 0x20000, 0x5b3b931e , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO pang3_roms[] =
{
  { "pa3w-17.11l", 0x80000, 0x12138234 , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "pa3w-16.10l", 0x80000, 0xd1ba585c , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "pa3-01m.2c", 0x200000, 0x068a152c, REGION_GFX1, 0, LOAD_NORMAL },
  { "pa3-07m.2f", 0x200000, 0x3a4a619d, REGION_GFX1, 0x200000, LOAD_NORMAL },
  { "pa3-11.11f", 0x08000, 0x90a08c46 , REGION_ROM2, 0x00000, LOAD_NORMAL },
  { "pa3-05.10d", 0x20000, 0x73a10d5d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "pa3-06.11d", 0x20000, 0xaffa4f82 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO pang3j_roms[] =
{
  { "pa3j-17.11l", 0x80000, 0x21f6e51f , REGION_ROM1, 0x00000, LOAD_SWAP_16 },
  { "pa3j-16.10l", 0x80000, 0xca1d7897 , REGION_ROM1, 0x80000, LOAD_SWAP_16 },
  { "pa3-01m.2c", 0x200000, 0x068a152c, REGION_GFX1, 0, LOAD_NORMAL },
  { "pa3-07m.2f", 0x200000, 0x3a4a619d, REGION_GFX1, 0x200000, LOAD_NORMAL },
  { "pa3-11.11f", 0x08000, 0x90a08c46 , REGION_ROM2, 0x00000, LOAD_NORMAL },
  { "pa3-05.10d", 0x20000, 0x73a10d5d , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "pa3-06.11d", 0x20000, 0xaffa4f82 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO megaman_roms[] =
{
  { "rcma_23b.rom", 0x80000, 0x61e4a397 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "rcma_22b.rom", 0x80000, 0x708268c4 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "rcma_21a.rom", 0x80000, 0x4376ea95 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "rcm_01.rom", 0x80000, 0x6ecdf13f, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "rcm_02.rom", 0x80000, 0x944d4f0f, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "rcm_03.rom", 0x80000, 0x36f3073c, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "rcm_04.rom", 0x80000, 0x54e622ff, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "rcm_05.rom", 0x80000, 0x5dd131fd, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "rcm_06.rom", 0x80000, 0xf0faf813, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "rcm_07.rom", 0x80000, 0x826de013, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "rcm_08.rom", 0x80000, 0xfbff64cf, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "rcm_10.rom", 0x80000, 0x4dc8ada9, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "rcm_11.rom", 0x80000, 0xf2b9ee06, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "rcm_12.rom", 0x80000, 0xfed5f203, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "rcm_13.rom", 0x80000, 0x5069d4a9, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "rcm_14.rom", 0x80000, 0x303be3bd, REGION_GFX1, 0x600000, LOAD_16_64 },
  { "rcm_15.rom", 0x80000, 0x4f2d372f, REGION_GFX1, 0x600002, LOAD_16_64 },
  { "rcm_16.rom", 0x80000, 0x93d97fde, REGION_GFX1, 0x600004, LOAD_16_64 },
  { "rcm_17.rom", 0x80000, 0x92371042, REGION_GFX1, 0x600006, LOAD_16_64 },
  { "rcm_09.rom", 0x20000, 0x9632d6ef , REGION_ROM2, 0, LOAD_NORMAL },
  { "rcm_18.rom", 0x20000, 0x80f1f8aa , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "rcm_19.rom", 0x20000, 0xf257dbe1 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};

static struct ROM_INFO rockmanj_roms[] =
{
  { "rcm23a.bin", 0x80000, 0xefd96cb2 , REGION_ROM1, 0x000000, LOAD_SWAP_16 },
  { "rcm22a.bin", 0x80000, 0x8729a689 , REGION_ROM1, 0x080000, LOAD_SWAP_16 },
  { "rcm21a.bin", 0x80000, 0x517ccde2 , REGION_ROM1, 0x100000, LOAD_SWAP_16 },
  { "rcm_01.rom", 0x80000, 0x6ecdf13f, REGION_GFX1, 0x000000, LOAD_16_64 },
  { "rcm_02.rom", 0x80000, 0x944d4f0f, REGION_GFX1, 0x000002, LOAD_16_64 },
  { "rcm_03.rom", 0x80000, 0x36f3073c, REGION_GFX1, 0x000004, LOAD_16_64 },
  { "rcm_04.rom", 0x80000, 0x54e622ff, REGION_GFX1, 0x000006, LOAD_16_64 },
  { "rcm_05.rom", 0x80000, 0x5dd131fd, REGION_GFX1, 0x200000, LOAD_16_64 },
  { "rcm_06.rom", 0x80000, 0xf0faf813, REGION_GFX1, 0x200002, LOAD_16_64 },
  { "rcm_07.rom", 0x80000, 0x826de013, REGION_GFX1, 0x200004, LOAD_16_64 },
  { "rcm_08.rom", 0x80000, 0xfbff64cf, REGION_GFX1, 0x200006, LOAD_16_64 },
  { "rcm_10.rom", 0x80000, 0x4dc8ada9, REGION_GFX1, 0x400000, LOAD_16_64 },
  { "rcm_11.rom", 0x80000, 0xf2b9ee06, REGION_GFX1, 0x400002, LOAD_16_64 },
  { "rcm_12.rom", 0x80000, 0xfed5f203, REGION_GFX1, 0x400004, LOAD_16_64 },
  { "rcm_13.rom", 0x80000, 0x5069d4a9, REGION_GFX1, 0x400006, LOAD_16_64 },
  { "rcm_14.rom", 0x80000, 0x303be3bd, REGION_GFX1, 0x600000, LOAD_16_64 },
  { "rcm_15.rom", 0x80000, 0x4f2d372f, REGION_GFX1, 0x600002, LOAD_16_64 },
  { "rcm_16.rom", 0x80000, 0x93d97fde, REGION_GFX1, 0x600004, LOAD_16_64 },
  { "rcm_17.rom", 0x80000, 0x92371042, REGION_GFX1, 0x600006, LOAD_16_64 },
  { "rcm_09.rom", 0x20000, 0x9632d6ef , REGION_ROM2, 0, LOAD_NORMAL },
  { "rcm_18.rom", 0x20000, 0x80f1f8aa , REGION_SMP1, 0x00000, LOAD_NORMAL },
  { "rcm_19.rom", 0x20000, 0xf257dbe1 , REGION_SMP1, 0x20000, LOAD_NORMAL },
  { NULL, 0, 0, 0, 0, 0 }
};


// DSW (generated)

static struct DSW_DATA dsw_data_forgottn_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_2COIN_2PLAY, 0x06, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_2COIN_2PLAY, 0x30, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { MSG_DEMO_SOUND, 0x40, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x40, 0x00 },
  { MSG_SCREEN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_forgottn_2[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SERVICE, 0x40,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x40,0 },
  { "Freeze", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_forgottn_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_INFO forgottn_dsw[] =
{
  { 2, 0xff, dsw_data_forgottn_1 },
  { 4, 0xff, dsw_data_forgottn_2 },
  { 6, 0xff, dsw_data_forgottn_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_ghouls_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_CABINET, 0x80, 2 },
  { MSG_UPRIGHT, 0x80, 0x00 },
  { MSG_TABLE, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_ghouls_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easier" , 0x04, 0x00 },
  { "Very Easy" , 0x05, 0x00 },
  { "Easy" , 0x06, 0x00 },
  { "Normal" , 0x07, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Very Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_EXTRA_LIFE, 0x30, 4 },
  { "10K30K and every 30K" , 0x20, 0x00 },
  { "20K50K and every 70K" , 0x10, 0x00 },
  { "30K60K and every 70K" , 0x30, 0x00 },
  { "40K70K and every 80K" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_ghouls_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "3" , 0x03, 0x00 },
  { "4" , 0x02, 0x00 },
  { "5" , 0x01, 0x00 },
  { "6" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x20, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO ghouls_dsw[] =
{
  { 2, 0xff, dsw_data_ghouls_1 },
  { 4, 0xff, dsw_data_ghouls_2 },
  { 6, 0xff, dsw_data_ghouls_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_strider_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { MSG_CABINET, 0xc0, 3 },
  { "Upright 1 Player" , 0xc0, 0x00 },
  { "Upright 2 Players" , 0x80, 0x00 },
  { MSG_TABLE, 0x00, 0x00 },
  { MSG_CABINET, 0xc0, 3 },
  { "Upright 1 Player" , 0xc0, 0x00 },
  { "Upright 2 Players" , 0x80, 0x00 },
  { MSG_TABLE, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_strider_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easiest" , 0x04, 0x00 },
  { "Easier" , 0x05, 0x00 },
  { "Easy" , 0x06, 0x00 },
  { "Normal" , 0x07, 0x00 },
  { "Medium" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Harder" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easiest" , 0x04, 0x00 },
  { "Easier" , 0x05, 0x00 },
  { "Easy" , 0x06, 0x00 },
  { "Normal" , 0x07, 0x00 },
  { "Medium" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Harder" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Continue Coinage ?", 0x08, 2 },
  { "1 Coin" , 0x00, 0x00 },
  { "2 Coins" , 0x08, 0x00 },
  { MSG_EXTRA_LIFE, 0x30, 4 },
  { "20000 60000" , 0x10, 0x00 },
  { "30000 60000" , 0x00, 0x00 },
  { "20000 40000 60000" , 0x30, 0x00 },
  { "30000 50000 70000" , 0x20, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_strider_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "3" , 0x03, 0x00 },
  { "4" , 0x02, 0x00 },
  { "5" , 0x01, 0x00 },
  { "6" , 0x00, 0x00 },
  { "Freeze", 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x20, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO strider_dsw[] =
{
  { 2, 0xff, dsw_data_strider_1 },
  { 4, 0x37, dsw_data_strider_2 },
  { 6, 0xff, dsw_data_strider_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_dwj_1[] =
{
  { MSG_COIN1, 0x07, 7 },
  { MSG_4COIN_1PLAY, 0x01, 0x00 },
  { MSG_3COIN_1PLAY, 0x02, 0x00 },
  { MSG_2COIN_1PLAY, 0x03, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_COIN2, 0x38, 7 },
  { MSG_4COIN_1PLAY, 0x08, 0x00 },
  { MSG_3COIN_1PLAY, 0x10, 0x00 },
  { MSG_2COIN_1PLAY, 0x18, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_COIN2, 0x38, 7 },
  { MSG_4COIN_1PLAY, 0x08, 0x00 },
  { MSG_3COIN_1PLAY, 0x10, 0x00 },
  { MSG_2COIN_1PLAY, 0x18, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_dwj_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Very Easy" , 0x04, 0x00 },
  { "Easy 2" , 0x05, 0x00 },
  { "Easy 1" , 0x06, 0x00 },
  { "Normal" , 0x07, 0x00 },
  { "Difficult 1" , 0x03, 0x00 },
  { "Difficult 2" , 0x02, 0x00 },
  { "Difficult 3" , 0x01, 0x00 },
  { "Very difficult" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_dwj_3[] =
{
  { "Freeze", 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Turbo Mode", 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x20, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO dwj_dsw[] =
{
  { 2, 0xff, dsw_data_dwj_1 },
  { 4, 0xff, dsw_data_dwj_2 },
  { 6, 0xff, dsw_data_dwj_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_willow_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x01, 0x00 },
  { MSG_3COIN_1PLAY, 0x02, 0x00 },
  { MSG_2COIN_1PLAY, 0x03, 0x00 },
  { "2 Coins/1 Credit (1 to continue)" , 0x00, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x08, 0x00 },
  { MSG_3COIN_1PLAY, 0x10, 0x00 },
  { MSG_2COIN_1PLAY, 0x18, 0x00 },
  { "2 Coins/1 Credit (1 to continue)" , 0x00, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_CABINET, 0xc0, 3 },
  { "Upright 1 Player" , 0xc0, 0x00 },
  { "Upright 2 Players" , 0x80, 0x00 },
  { MSG_TABLE, 0x00, 0x00 },
  { MSG_CABINET, 0xc0, 3 },
  { "Upright 1 Player" , 0xc0, 0x00 },
  { "Upright 2 Players" , 0x80, 0x00 },
  { MSG_TABLE, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_willow_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Very easy" , 0x04, 0x00 },
  { "Easier" , 0x05, 0x00 },
  { "Easy" , 0x06, 0x00 },
  { "Normal" , 0x07, 0x00 },
  { "Medium" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Harder" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Nando Speed", 0x18, 4 },
  { "Slow" , 0x10, 0x00 },
  { "Normal" , 0x18, 0x00 },
  { "Fast" , 0x08, 0x00 },
  { "Very Fast" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Stage Magic Continue (power up?)", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Stage Magic Continue (power up?)", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Stage Magic Continue (power up?)", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Stage Magic Continue (power up?)", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_willow_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "1" , 0x02, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x01, 0x00 },
  { "4" , 0x00, 0x00 },
  { "Vitality", 0x0c, 4 },
  { "2" , 0x00, 0x00 },
  { "3" , 0x0c, 0x00 },
  { "4" , 0x08, 0x00 },
  { "5" , 0x04, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x20, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO willow_dsw[] =
{
  { 2, 0xff, dsw_data_willow_1 },
  { 4, 0xbf, dsw_data_willow_2 },
  { 6, 0xff, dsw_data_willow_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_unsquad_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x01, 0x00 },
  { MSG_3COIN_1PLAY, 0x02, 0x00 },
  { MSG_2COIN_1PLAY, 0x03, 0x00 },
  { "2 Coins/1 Credit (1 to continue)" , 0x00, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x08, 0x00 },
  { MSG_3COIN_1PLAY, 0x10, 0x00 },
  { MSG_2COIN_1PLAY, 0x18, 0x00 },
  { "2 Coins/1 Credit (1 to continue)" , 0x00, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_unsquad_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Super Easy" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Very Difficult" , 0x02, 0x00 },
  { "Super Difficult" , 0x01, 0x00 },
  { "Ultra Super Difficult" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_unsquad_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO unsquad_dsw[] =
{
  { 2, 0xff, dsw_data_unsquad_1 },
  { 4, 0xff, dsw_data_unsquad_2 },
  { 6, 0x9f, dsw_data_unsquad_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_ffight_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNUSED, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_ffight_2[] =
{
  { "Difficulty Level 1", 0x07, 8 },
  { "Very easy" , 0x07, 0x00 },
  { "Easier" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Medium" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Harder" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Difficulty Level 2", 0x18, 4 },
  { "Easy" , 0x18, 0x00 },
  { "Normal" , 0x10, 0x00 },
  { "Hard" , 0x08, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_EXTRA_LIFE, 0x60, 4 },
  { "100k" , 0x60, 0x00 },
  { "200k" , 0x40, 0x00 },
  { "100k and every 200k" , 0x20, 0x00 },
  { "None" , 0x00, 0x00 },
  { MSG_UNUSED, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_ffight_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO ffight_dsw[] =
{
  { 2, 0xff, dsw_data_ffight_1 },
  { 4, 0xf4, dsw_data_ffight_2 },
  { 6, 0x9f, dsw_data_ffight_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data__1941_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data__1941_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "0 (Easier)" , 0x07, 0x00 },
  { "1" , 0x06, 0x00 },
  { "2" , 0x05, 0x00 },
  { "3" , 0x04, 0x00 },
  { "4" , 0x03, 0x00 },
  { "5" , 0x02, 0x00 },
  { "6" , 0x01, 0x00 },
  { "7 (Harder)" , 0x00, 0x00 },
  { "Life Bar", 0x18, 4 },
  { "More Slowly" , 0x18, 0x00 },
  { "Slowly" , 0x10, 0x00 },
  { "Quickly" , 0x08, 0x00 },
  { "More Quickly" , 0x00, 0x00 },
  { "Bullet's Speed", 0x60, 4 },
  { "Very Slow" , 0x60, 0x00 },
  { "Slow" , 0x40, 0x00 },
  { "Fast" , 0x20, 0x00 },
  { "Very Fast" , 0x00, 0x00 },
  { "Initial Vitality", 0x80, 2 },
  { "3 Bars" , 0x80, 0x00 },
  { "4 Bars" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data__1941_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO _1941_dsw[] =
{
  { 2, 0xff, dsw_data__1941_1 },
  { 4, 0xff, dsw_data__1941_2 },
  { 6, 0x9f, dsw_data__1941_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_mercs_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_UNKNOWN, 0x38, 8 },
  { "0" , 0x00, 0x00 },
  { "1" , 0x08, 0x00 },
  { "2" , 0x10, 0x00 },
  { "3" , 0x38, 0x00 },
  { "4" , 0x30, 0x00 },
  { "5" , 0x28, 0x00 },
  { "6" , 0x20, 0x00 },
  { "7" , 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_mercs_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easiest" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Very Difficult" , 0x02, 0x00 },
  { "Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Coin Slots", 0x08, 2 },
  { "1" , 0x00, 0x00 },
  { "3" , 0x08, 0x00 },
  { "Max Players", 0x10, 2 },
  { "2" , 0x00, 0x00 },
  { "3" , 0x10, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_mercs_3[] =
{
  { MSG_UNKNOWN, 0x07, 8 },
  { "0" , 0x00, 0x00 },
  { "1" , 0x01, 0x00 },
  { "2" , 0x02, 0x00 },
  { "3" , 0x03, 0x00 },
  { "4" , 0x04, 0x00 },
  { "5" , 0x05, 0x00 },
  { "6" , 0x06, 0x00 },
  { "7" , 0x07, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO mercs_dsw[] =
{
  { 2, 0xff, dsw_data_mercs_1 },
  { 4, 0xfc, dsw_data_mercs_2 },
  { 6, 0x9f, dsw_data_mercs_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_mtwins_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_mtwins_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Super Easy" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Very Difficult" , 0x02, 0x00 },
  { "Super Difficult" , 0x01, 0x00 },
  { "Ultra Super Difficult" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_LIVES, 0x30, 3 },
  { "1" , 0x20, 0x00 },
  { "2" , 0x10, 0x00 },
  { "3" , 0x00, 0x00 },
  { MSG_LIVES, 0x30, 3 },
  { "1" , 0x20, 0x00 },
  { "2" , 0x10, 0x00 },
  { "3" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_mtwins_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO mtwins_dsw[] =
{
  { 2, 0xff, dsw_data_mtwins_1 },
  { 4, 0xcf, dsw_data_mtwins_2 },
  { 6, 0x9f, dsw_data_mtwins_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_msword_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_msword_2[] =
{
  { "Level 1", 0x07, 8 },
  { "Easiest" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Very Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Level 2", 0x38, 8 },
  { "Easiest" , 0x20, 0x00 },
  { "Very Easy" , 0x28, 0x00 },
  { "Easy" , 0x30, 0x00 },
  { "Normal" , 0x38, 0x00 },
  { "Difficult" , 0x18, 0x00 },
  { "Hard" , 0x10, 0x00 },
  { "Very Hard" , 0x08, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Stage Select", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_msword_3[] =
{
  { "Vitality", 0x03, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO msword_dsw[] =
{
  { 2, 0xff, dsw_data_msword_1 },
  { 4, 0xbc, dsw_data_msword_2 },
  { 6, 0x9f, dsw_data_msword_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_cawing_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_cawing_2[] =
{
  { "Difficulty Level (Enemy Strength)", 0x07, 8 },
  { "Very Easy" , 0x07, 0x00 },
  { "Easy 2" , 0x06, 0x00 },
  { "Easy 1" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult 1" , 0x03, 0x00 },
  { "Difficult 2" , 0x02, 0x00 },
  { "Difficult 3" , 0x01, 0x00 },
  { "Very Difficult" , 0x00, 0x00 },
  { "Difficulty Level (Player Strength)", 0x18, 4 },
  { "Easy" , 0x10, 0x00 },
  { "Normal" , 0x18, 0x00 },
  { "Difficult" , 0x08, 0x00 },
  { "Very Difficult" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_cawing_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO cawing_dsw[] =
{
  { 2, 0xff, dsw_data_cawing_1 },
  { 4, 0xf7, dsw_data_cawing_2 },
  { 6, 0x9f, dsw_data_cawing_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_nemo_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_nemo_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Very Easy" , 0x07, 0x00 },
  { "Easy 1" , 0x06, 0x00 },
  { "Easy 2" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult 1" , 0x03, 0x00 },
  { "Difficult 2" , 0x02, 0x00 },
  { "Difficult 3" , 0x01, 0x00 },
  { "Very Difficult" , 0x00, 0x00 },
  { "Life Bar", 0x18, 3 },
  { "Minimun" , 0x00, 0x00 },
  { "Medium" , 0x18, 0x00 },
  { "Maximum" , 0x08, 0x00 },
  { "Life Bar", 0x18, 3 },
  { "Minimun" , 0x00, 0x00 },
  { "Medium" , 0x18, 0x00 },
  { "Maximum" , 0x08, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_nemo_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "1" , 0x02, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x01, 0x00 },
  { "4" , 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO nemo_dsw[] =
{
  { 2, 0xff, dsw_data_nemo_1 },
  { 4, 0xff, dsw_data_nemo_2 },
  { 6, 0x9f, dsw_data_nemo_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_sf2_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_sf2_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easier" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Very Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_sf2_3[] =
{
  { MSG_UNKNOWN, 0x03, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO sf2_dsw[] =
{
  { 2, 0xff, dsw_data_sf2_1 },
  { 4, 0xfc, dsw_data_sf2_2 },
  { 6, 0x9f, dsw_data_sf2_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_sf2j_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_sf2j_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easier" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Very Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "2 Players Game", 0x08, 2 },
  { "1 Credit/No Continue" , 0x08, 0x00 },
  { "2 Credits/Winner Continue" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_sf2j_3[] =
{
  { MSG_UNKNOWN, 0x03, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO sf2j_dsw[] =
{
  { 2, 0xff, dsw_data_sf2j_1 },
  { 4, 0xf4, dsw_data_sf2j_2 },
  { 6, 0x9f, dsw_data_sf2j_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data__3wonders_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data__3wonders_2[] =
{
  { "Action Lives", 0x03, 4 },
  { "1" , 0x03, 0x00 },
  { "2" , 0x02, 0x00 },
  { "3" , 0x01, 0x00 },
  { "5" , 0x00, 0x00 },
  { "Action Difficulty", 0x0c, 4 },
  { "Easy" , 0x0c, 0x00 },
  { "Normal" , 0x08, 0x00 },
  { "Hard" , 0x04, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Shooting Lives", 0x30, 4 },
  { "1" , 0x30, 0x00 },
  { "2" , 0x20, 0x00 },
  { "3" , 0x10, 0x00 },
  { "5" , 0x00, 0x00 },
  { "Shooting Difficulty", 0xc0, 4 },
  { "Easy" , 0xc0, 0x00 },
  { "Normal" , 0x80, 0x00 },
  { "Hard" , 0x40, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data__3wonders_3[] =
{
  { "Puzzle Lives", 0x03, 4 },
  { "1" , 0x03, 0x00 },
  { "2" , 0x02, 0x00 },
  { "3" , 0x01, 0x00 },
  { "5" , 0x00, 0x00 },
  { "Puzzle Difficulty", 0x0c, 4 },
  { "Easy" , 0x0c, 0x00 },
  { "Normal" , 0x08, 0x00 },
  { "Hard" , 0x04, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO _3wonders_dsw[] =
{
  { 2, 0xff, dsw_data__3wonders_1 },
  { 4, 0xff, dsw_data__3wonders_2 },
  { 6, 0x9f, dsw_data__3wonders_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_kod_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { "Coin Slots", 0x08, 2 },
  { "1" , 0x00, 0x00 },
  { "3" , 0x08, 0x00 },
  { "Max Players", 0x10, 2 },
  { "2" , 0x00, 0x00 },
  { "3" , 0x10, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_kod_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easiest" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Medium" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Very Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_LIVES, 0x38, 8 },
  { "8" , 0x00, 0x00 },
  { "7" , 0x08, 0x00 },
  { "6" , 0x10, 0x00 },
  { "5" , 0x18, 0x00 },
  { "4" , 0x20, 0x00 },
  { "3" , 0x28, 0x00 },
  { "2" , 0x38, 0x00 },
  { "1" , 0x30, 0x00 },
  { MSG_EXTRA_LIFE, 0xc0, 4 },
  { "80k and every 400k" , 0x80, 0x00 },
  { "100k and every 450k" , 0xc0, 0x00 },
  { "160k and every 450k" , 0x40, 0x00 },
  { "None" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_kod_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO kod_dsw[] =
{
  { 2, 0xff, dsw_data_kod_1 },
  { 4, 0xfc, dsw_data_kod_2 },
  { 6, 0x9f, dsw_data_kod_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_captcomm_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_captcomm_2[] =
{
  { "Difficulty 1", 0x07, 8 },
  { "Very Easy" , 0x07, 0x00 },
  { "Easy 1" , 0x06, 0x00 },
  { "Easy 2" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Very Difficult" , 0x02, 0x00 },
  { "Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Difficulty 2", 0x18, 4 },
  { "1" , 0x18, 0x00 },
  { "2" , 0x10, 0x00 },
  { "3" , 0x08, 0x00 },
  { "4" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Max Players", 0xc0, 4 },
  { "1" , 0x40, 0x00 },
  { "2" , 0xc0, 0x00 },
  { "3" , 0x80, 0x00 },
  { "4" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_captcomm_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO captcomm_dsw[] =
{
  { 2, 0xff, dsw_data_captcomm_1 },
  { 4, 0xff, dsw_data_captcomm_2 },
  { 6, 0x9f, dsw_data_captcomm_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_knights_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_UNUSED, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNUSED, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNUSED, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNUSED, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_knights_2[] =
{
  { "Player speed and vitality consumption", 0x07, 8 },
  { "Very easy" , 0x07, 0x00 },
  { "Easier" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Medium" , 0x03, 0x00 },
  { "Hard" , 0x02, 0x00 },
  { "Harder" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { "Enemy's vitality and attack power", 0x38, 8 },
  { "Very Easy" , 0x10, 0x00 },
  { "Easier" , 0x08, 0x00 },
  { "Easy" , 0x00, 0x00 },
  { "Normal" , 0x38, 0x00 },
  { "Medium" , 0x30, 0x00 },
  { "Hard" , 0x28, 0x00 },
  { "Harder" , 0x20, 0x00 },
  { "Hardest" , 0x18, 0x00 },
  { "Coin Slots", 0x40, 2 },
  { "1" , 0x00, 0x00 },
  { "3" , 0x40, 0x00 },
  { "Max Players", 0x80, 2 },
  { "2" , 0x00, 0x00 },
  { "3" , 0x80, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_knights_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x03, 0x00 },
  { "3" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO knights_dsw[] =
{
  { 2, 0xff, dsw_data_knights_1 },
  { 4, 0xfc, dsw_data_knights_2 },
  { 6, 0x9f, dsw_data_knights_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_varth_1[] =
{
  { MSG_COIN1, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_COIN2, 0x38, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x08, 0x00 },
  { MSG_2COIN_1PLAY, 0x10, 0x00 },
  { MSG_1COIN_1PLAY, 0x38, 0x00 },
  { MSG_1COIN_1PLAY, 0x30, 0x00 },
  { MSG_1COIN_3PLAY, 0x28, 0x00 },
  { MSG_1COIN_4PLAY, 0x20, 0x00 },
  { MSG_1COIN_6PLAY, 0x18, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_varth_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Very Easy" , 0x07, 0x00 },
  { "Easy 1" , 0x06, 0x00 },
  { "Easy 2" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Difficult" , 0x03, 0x00 },
  { "Very Difficult" , 0x02, 0x00 },
  { "Hard" , 0x01, 0x00 },
  { "Hardest" , 0x00, 0x00 },
  { MSG_EXTRA_LIFE, 0x18, 4 },
  { "600k and every 1.400k" , 0x18, 0x00 },
  { "600k 2.000k and 4500k" , 0x10, 0x00 },
  { "1.200k 3.500k" , 0x08, 0x00 },
  { "2000k only" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_varth_3[] =
{
  { MSG_LIVES, 0x03, 4 },
  { "1" , 0x02, 0x00 },
  { "2" , 0x01, 0x00 },
  { "3" , 0x03, 0x00 },
  { "4" , 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x40, 0x00 },
  { MSG_YES, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO varth_dsw[] =
{
  { 2, 0xff, dsw_data_varth_1 },
  { 4, 0xff, dsw_data_varth_2 },
  { 6, 0x9f, dsw_data_varth_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_cworld2j_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_UNKNOWN, 0x38, 2 },
  { MSG_OFF, 0x38, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Test Mode (Use with Service Mode)", 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_cworld2j_2[] =
{
  { MSG_DIFFICULTY, 0x07, 5 },
  { "0" , 0x06, 0x00 },
  { "1" , 0x05, 0x00 },
  { "2" , 0x04, 0x00 },
  { "3" , 0x03, 0x00 },
  { "4" , 0x02, 0x00 },
  { "Extend", 0x18, 3 },
  { "N" , 0x18, 0x00 },
  { "E" , 0x10, 0x00 },
  { "D" , 0x00, 0x00 },
  { MSG_LIVES, 0xe0, 5 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x80, 0x00 },
  { "3" , 0xe0, 0x00 },
  { "4" , 0xa0, 0x00 },
  { "5" , 0xc0, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_cworld2j_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO cworld2j_dsw[] =
{
  { 2, 0xff, dsw_data_cworld2j_1 },
  { 4, 0xfe, dsw_data_cworld2j_2 },
  { 6, 0xdf, dsw_data_cworld2j_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_wof_3[] =
{
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO wof_dsw[] =
{
  { 6, 0x08, dsw_data_wof_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_dino_3[] =
{
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO dino_dsw[] =
{
  { 6, 0x0f, dsw_data_dino_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_punisher_3[] =
{
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_INFO punisher_dsw[] =
{
  { 6, 0xff, dsw_data_punisher_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_slammast_3[] =
{
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO slammast_dsw[] =
{
  { 6, 0xff, dsw_data_slammast_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_pnickj_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { "Coin Slots", 0x08, 2 },
  { "1" , 0x08, 0x00 },
  { "2" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_pnickj_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Easiest" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Hard" , 0x03, 0x00 },
  { "Very Hard" , 0x02, 0x00 },
  { "Hardest" , 0x01, 0x00 },
  { "Master Level" , 0x00, 0x00 },
  { "Unknkown", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x30, 4 },
  { "1" , 0x00, 0x00 },
  { "2" , 0x10, 0x00 },
  { "3" , 0x20, 0x00 },
  { "4" , 0x30, 0x00 },
  { "Vs Play Mode", 0xc0, 4 },
  { "1 Game Match" , 0xc0, 0x00 },
  { "3 Games Match" , 0x80, 0x00 },
  { "5 Games Match" , 0x40, 0x00 },
  { "7 Games Match" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_pnickj_3[] =
{
  { MSG_UNKNOWN, 0x03, 4 },
  { "1" , 0x03, 0x00 },
  { "2" , 0x02, 0x00 },
  { "3" , 0x01, 0x00 },
  { "4" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO pnickj_dsw[] =
{
  { 2, 0xff, dsw_data_pnickj_1 },
  { 4, 0xf4, dsw_data_pnickj_2 },
  { 6, 0xdf, dsw_data_pnickj_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_qad_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_qad_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "Very Easy" , 0x07, 0x00 },
  { "Very Easy" , 0x06, 0x00 },
  { "Easy" , 0x05, 0x00 },
  { "Normal" , 0x04, 0x00 },
  { "Hard" , 0x03, 0x00 },
  { "Very Hard" , 0x02, 0x00 },
  { "Very Hard" , 0x01, 0x00 },
  { "Very Hard" , 0x00, 0x00 },
  { "Wisdom", 0x18, 4 },
  { "Low" , 0x18, 0x00 },
  { "Normal" , 0x10, 0x00 },
  { "High" , 0x08, 0x00 },
  { "Brilliant" , 0x00, 0x00 },
  { MSG_LIVES, 0xe0, 8 },
  { "1" , 0x60, 0x00 },
  { "2" , 0x80, 0x00 },
  { "3" , 0xa0, 0x00 },
  { "4" , 0xc0, 0x00 },
  { "5" , 0xe0, 0x00 },
  { "1" , 0x40, 0x00 },
  { "1" , 0x20, 0x00 },
  { "1" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_qad_3[] =
{
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x20, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO qad_dsw[] =
{
  { 2, 0xff, dsw_data_qad_1 },
  { 4, 0xfe, dsw_data_qad_2 },
  { 6, 0xfc, dsw_data_qad_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_qadj_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_qadj_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "0" , 0x07, 0x00 },
  { "1" , 0x06, 0x00 },
  { "2" , 0x05, 0x00 },
  { "3" , 0x04, 0x00 },
  { "4" , 0x03, 0x00 },
  { "4" , 0x02, 0x00 },
  { "4" , 0x01, 0x00 },
  { "4" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_LIVES, 0xe0, 8 },
  { "1" , 0xa0, 0x00 },
  { "2" , 0xc0, 0x00 },
  { "3" , 0xe0, 0x00 },
  { "1" , 0x00, 0x00 },
  { "1" , 0x20, 0x00 },
  { "1" , 0x80, 0x00 },
  { "2" , 0x40, 0x00 },
  { "3" , 0x60, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_qadj_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO qadj_dsw[] =
{
  { 2, 0xff, dsw_data_qadj_1 },
  { 4, 0xff, dsw_data_qadj_2 },
  { 6, 0xdf, dsw_data_qadj_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_qtono2_1[] =
{
  { MSG_COINAGE, 0x07, 8 },
  { MSG_4COIN_1PLAY, 0x00, 0x00 },
  { MSG_3COIN_1PLAY, 0x01, 0x00 },
  { MSG_2COIN_1PLAY, 0x02, 0x00 },
  { MSG_1COIN_1PLAY, 0x07, 0x00 },
  { MSG_1COIN_1PLAY, 0x06, 0x00 },
  { MSG_1COIN_3PLAY, 0x05, 0x00 },
  { MSG_1COIN_4PLAY, 0x04, 0x00 },
  { MSG_1COIN_6PLAY, 0x03, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "2 Coins to Start,1 to Continue", 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_qtono2_2[] =
{
  { MSG_DIFFICULTY, 0x07, 8 },
  { "1" , 0x07, 0x00 },
  { "2" , 0x06, 0x00 },
  { "3" , 0x05, 0x00 },
  { "4" , 0x04, 0x00 },
  { "5" , 0x03, 0x00 },
  { "6" , 0x02, 0x00 },
  { "7" , 0x01, 0x00 },
  { "8" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_LIVES, 0xe0, 8 },
  { "1" , 0x60, 0x00 },
  { "2" , 0x80, 0x00 },
  { "3" , 0xe0, 0x00 },
  { "4" , 0xa0, 0x00 },
  { "5" , 0xc0, 0x00 },
  { "?" , 0x40, 0x00 },
  { "?" , 0x20, 0x00 },
  { "?" , 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_qtono2_3[] =
{
  { MSG_UNKNOWN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_FREE_PLAY, 0x04, 2 },
  { MSG_OFF, 0x04, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SCREEN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x40, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x40, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO qtono2_dsw[] =
{
  { 2, 0xff, dsw_data_qtono2_1 },
  { 4, 0xff, dsw_data_qtono2_2 },
  { 6, 0xdf, dsw_data_qtono2_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_pang3_3[] =
{
  { "Freeze", 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_INFO pang3_dsw[] =
{
  { 6, 0x08, dsw_data_pang3_3 },
  { 0, 0, NULL }
};

static struct DSW_DATA dsw_data_megaman_1[] =
{
  { MSG_COINAGE, 0x1f, 19 },
  { MSG_9COIN_1PLAY, 0x0f, 0x00 },
  { MSG_8COIN_1PLAY, 0x10, 0x00 },
  { MSG_7COIN_1PLAY, 0x11, 0x00 },
  { MSG_6COIN_1PLAY, 0x12, 0x00 },
  { MSG_5COIN_1PLAY, 0x13, 0x00 },
  { MSG_4COIN_1PLAY, 0x14, 0x00 },
  { MSG_3COIN_1PLAY, 0x15, 0x00 },
  { MSG_2COIN_1PLAY, 0x16, 0x00 },
  { "2 Coins/1 Credit - 1 to continue (if on)" , 0x0e, 0x00 },
  { MSG_1COIN_1PLAY, 0x1f, 0x00 },
  { MSG_1COIN_1PLAY, 0x1e, 0x00 },
  { MSG_1COIN_3PLAY, 0x1d, 0x00 },
  { MSG_1COIN_4PLAY, 0x1c, 0x00 },
  { MSG_1COIN_5PLAY, 0x1b, 0x00 },
  { MSG_1COIN_6PLAY, 0x1a, 0x00 },
  { MSG_1COIN_7PLAY, 0x19, 0x00 },
  { MSG_1COIN_8PLAY, 0x18, 0x00 },
  { MSG_1COIN_9PLAY, 0x17, 0x00 },
  { MSG_FREE_PLAY, 0x0d, 0x00 },
  { MSG_COINAGE, 0x1f, 19 },
  { MSG_9COIN_1PLAY, 0x0f, 0x00 },
  { MSG_8COIN_1PLAY, 0x10, 0x00 },
  { MSG_7COIN_1PLAY, 0x11, 0x00 },
  { MSG_6COIN_1PLAY, 0x12, 0x00 },
  { MSG_5COIN_1PLAY, 0x13, 0x00 },
  { MSG_4COIN_1PLAY, 0x14, 0x00 },
  { MSG_3COIN_1PLAY, 0x15, 0x00 },
  { MSG_2COIN_1PLAY, 0x16, 0x00 },
  { "2 Coins/1 Credit - 1 to continue (if on)" , 0x0e, 0x00 },
  { MSG_1COIN_1PLAY, 0x1f, 0x00 },
  { MSG_1COIN_1PLAY, 0x1e, 0x00 },
  { MSG_1COIN_3PLAY, 0x1d, 0x00 },
  { MSG_1COIN_4PLAY, 0x1c, 0x00 },
  { MSG_1COIN_5PLAY, 0x1b, 0x00 },
  { MSG_1COIN_6PLAY, 0x1a, 0x00 },
  { MSG_1COIN_7PLAY, 0x19, 0x00 },
  { MSG_1COIN_8PLAY, 0x18, 0x00 },
  { MSG_1COIN_9PLAY, 0x17, 0x00 },
  { MSG_FREE_PLAY, 0x0d, 0x00 },
  { "2 Player Game", 0x60, 3 },
  { "1 Credit" , 0x20, 0x00 },
  { "2 Credits" , 0x40, 0x00 },
  { "2 Credits - pl1 may play on right" , 0x60, 0x00 },
  { "2 Player Game", 0x60, 3 },
  { "1 Credit" , 0x20, 0x00 },
  { "2 Credits" , 0x40, 0x00 },
  { "2 Credits - pl1 may play on right" , 0x60, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_megaman_2[] =
{
  { MSG_DIFFICULTY, 0x03, 4 },
  { "Easy" , 0x03, 0x00 },
  { "Normal" , 0x02, 0x00 },
  { "Difficult" , 0x01, 0x00 },
  { "Hard" , 0x00, 0x00 },
  { "Time", 0x0c, 4 },
  { "100" , 0x0c, 0x00 },
  { "90" , 0x08, 0x00 },
  { "70" , 0x04, 0x00 },
  { "60" , 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x20, 2 },
  { MSG_OFF, 0x20, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Voice", 0x40, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x40, 0x00 },
  { MSG_UNKNOWN, 0x80, 2 },
  { MSG_OFF, 0x80, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { NULL, 0, 0}
};

static struct DSW_DATA dsw_data_megaman_3[] =
{
  { MSG_SCREEN, 0x01, 2 },
  { MSG_OFF, 0x01, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x02, 2 },
  { MSG_OFF, 0x02, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { "Allow Continue", 0x04, 2 },
  { MSG_NO, 0x00, 0x00 },
  { MSG_YES, 0x04, 0x00 },
  { MSG_UNKNOWN, 0x08, 2 },
  { MSG_OFF, 0x08, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_UNKNOWN, 0x10, 2 },
  { MSG_OFF, 0x10, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_DEMO_SOUND, 0x20, 2 },
  { MSG_OFF, 0x00, 0x00 },
  { MSG_ON, 0x20, 0x00 },
  { MSG_UNKNOWN, 0x40, 2 },
  { MSG_OFF, 0x40, 0x00 },
  { MSG_ON, 0x00, 0x00 },
  { MSG_SERVICE, 0x80,2 },
  { MSG_ON, 0,0 },
  { MSG_OFF, 0x80,0 },
  { NULL, 0, 0}
};

static struct DSW_INFO megaman_dsw[] =
{
  { 2, 0xbf, dsw_data_megaman_1 },
  { 4, 0xff, dsw_data_megaman_2 },
  { 6, 0xff, dsw_data_megaman_3 },
  { 0, 0, NULL }
};


// And finally inputs : took the longest one...

static struct INPUT_INFO cps1_inputs[] = // 4 players, 3 buttons
{
  { KB_DEF_COIN1, MSG_COIN1, 0x00, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_COIN2, MSG_COIN2, 0x00, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_SERVICE, MSG_SERVICE, 0x00, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P1_START, MSG_P1_START, 0x00, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_START, MSG_P2_START, 0x00, 0x20, BIT_ACTIVE_0 },

  { KB_DEF_P1_RIGHT, MSG_P1_RIGHT, 0x08, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P1_LEFT, MSG_P1_LEFT, 0x08, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P1_DOWN, MSG_P1_DOWN, 0x08, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P1_UP, MSG_P1_UP, 0x08, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P1_B1, MSG_P1_B1, 0x08, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P1_B2, MSG_P1_B2, 0x08, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P1_B3, MSG_P1_B3, 0x08, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P3_B3, MSG_P3_B3, 0x08, 0x80, BIT_ACTIVE_0 },
  { KB_DEF_P2_RIGHT, MSG_P2_RIGHT, 0x09, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P2_LEFT, MSG_P2_LEFT, 0x09, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P2_DOWN, MSG_P2_DOWN, 0x09, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P2_UP, MSG_P2_UP, 0x09, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P2_B1, MSG_P2_B1, 0x09, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_B2, MSG_P2_B2, 0x09, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P2_B3, MSG_P2_B3, 0x09, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P4_B3, MSG_P3_B3, 0x09, 0x80, BIT_ACTIVE_0 },

  { KB_DEF_P3_RIGHT, MSG_P3_RIGHT, 0x0a, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P3_LEFT, MSG_P3_LEFT, 0x0a, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P3_DOWN, MSG_P3_DOWN, 0x0a, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P3_UP, MSG_P3_UP, 0x0a, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P3_B1, MSG_P3_B1, 0x0a, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P3_B2, MSG_P3_B2, 0x0a, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_COIN3, MSG_COIN3, 0x0a, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P3_START, MSG_P3_START, 0x0a, 0x80, BIT_ACTIVE_0 },

  { KB_DEF_P4_RIGHT, MSG_P3_RIGHT, 0x0c, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P4_LEFT, MSG_P3_LEFT, 0x0c, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P4_DOWN, MSG_P3_DOWN, 0x0c, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P4_UP, MSG_P3_UP, 0x0c, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P4_B1, MSG_P3_B1, 0x0c, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P4_B2, MSG_P3_B2, 0x0c, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_COIN4, MSG_COIN4, 0x0c, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P4_START, MSG_P4_START, 0x0c, 0x80, BIT_ACTIVE_0 },
   { 0, NULL,        0,        0,    0            },
};

static struct INPUT_INFO sf2_inputs[] = // cps1, 6 buttons
{
  { KB_DEF_COIN1, MSG_COIN1, 0x00, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_COIN2, MSG_COIN2, 0x00, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_SERVICE, MSG_SERVICE, 0x00, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P1_START, MSG_P1_START, 0x00, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_START, MSG_P2_START, 0x00, 0x20, BIT_ACTIVE_0 },

  { KB_DEF_P1_RIGHT, MSG_P1_RIGHT, 0x08, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P1_LEFT, MSG_P1_LEFT, 0x08, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P1_DOWN, MSG_P1_DOWN, 0x08, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P1_UP, MSG_P1_UP, 0x08, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P1_B1, MSG_P1_B1, 0x08, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P1_B2, MSG_P1_B2, 0x08, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P1_B3, MSG_P1_B3, 0x08, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P2_RIGHT, MSG_P2_RIGHT, 0x09, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P2_LEFT, MSG_P2_LEFT, 0x09, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P2_DOWN, MSG_P2_DOWN, 0x09, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P2_UP, MSG_P2_UP, 0x09, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P2_B1, MSG_P2_B1, 0x09, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_B2, MSG_P2_B2, 0x09, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P2_B3, MSG_P2_B3, 0x09, 0x40, BIT_ACTIVE_0 },

  { KB_DEF_P1_B4, MSG_P1_B4, 0x0a, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P1_B5, MSG_P1_B5, 0x0a, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P1_B6, MSG_P1_B6, 0x0a, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P2_B4, MSG_P2_B4, 0x0a, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_B5, MSG_P2_B5, 0x0a, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P2_B6, MSG_P2_B6, 0x0a, 0x40, BIT_ACTIVE_0 },
   { 0, NULL,        0,        0,    0            },
};

static struct INPUT_INFO cps1b4_inputs[] = // cps1, 4 buttons
{
  { KB_DEF_COIN1, MSG_COIN1, 0x00, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_COIN2, MSG_COIN2, 0x00, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_COIN3, MSG_COIN3, 0x00, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_COIN4, MSG_COIN4, 0x00, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P1_START, MSG_P1_START, 0x00, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_START, MSG_P2_START, 0x00, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P3_START, MSG_P3_START, 0x00, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P4_START, MSG_P4_START, 0x00, 0x80, BIT_ACTIVE_0 },

  { KB_DEF_P1_RIGHT, MSG_P1_RIGHT, 0x08, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P1_LEFT, MSG_P1_LEFT, 0x08, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P1_DOWN, MSG_P1_DOWN, 0x08, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P1_UP, MSG_P1_UP, 0x08, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P1_B1, MSG_P1_B1, 0x08, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P1_B2, MSG_P1_B2, 0x08, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P1_B3, MSG_P1_B3, 0x08, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P1_B4, MSG_P1_B4, 0x08, 0x80, BIT_ACTIVE_0 },
  { KB_DEF_P2_RIGHT, MSG_P2_RIGHT, 0x09, 0x01, BIT_ACTIVE_0 },
  { KB_DEF_P2_LEFT, MSG_P2_LEFT, 0x09, 0x02, BIT_ACTIVE_0 },
  { KB_DEF_P2_DOWN, MSG_P2_DOWN, 0x09, 0x04, BIT_ACTIVE_0 },
  { KB_DEF_P2_UP, MSG_P2_UP, 0x09, 0x08, BIT_ACTIVE_0 },
  { KB_DEF_P2_B1, MSG_P2_B1, 0x09, 0x10, BIT_ACTIVE_0 },
  { KB_DEF_P2_B2, MSG_P2_B2, 0x09, 0x20, BIT_ACTIVE_0 },
  { KB_DEF_P2_B3, MSG_P2_B3, 0x09, 0x40, BIT_ACTIVE_0 },
  { KB_DEF_P2_B4, MSG_P2_B4, 0x09, 0x80, BIT_ACTIVE_0 },
   { 0, NULL,        0,        0,    0            },
};

// Stuff...

/* Piece of history : I don't understand this, and mame does not use it */
/*  static struct GFX_LAYOUT cps1_object = */
/*  { */
/*     8,8, */
/*     RGN_FRAC(1,2), */
/*     4, */
/*     { RGN_FRAC(1,2)+8, RGN_FRAC(1,2)+0, 8, 0 }, */
/*     { 0, 1, 2, 3, 4, 5, 6, 7 }, */
/*     { 0*16, 1*16, 2*16, 3*16, 4*16, 5*16, 6*16, 7*16 }, */
/*     16*8 */
/*  }; */

static struct GFX_LIST cps1_gfx[] =
{
  { REGION_GFX1, NULL }, //&cps1_object, },
   { 0,           NULL,               },
};

static struct VIDEO_INFO cps1_video =
{
   draw_cps1,
   384,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
   cps1_gfx,
};

static struct VIDEO_INFO cps1_video_270 =
{
   draw_cps1,
   384,
   224,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE,
   cps1_gfx,
};

static struct YM2151interface ym2151_interface =
{
	1,  /* 1 chip */
	3579580,    /* 3.579580 MHz ? */
	{ YM3012_VOL(200,MIXER_PAN_LEFT,200,MIXER_PAN_RIGHT) },
	{ NULL, } // cps1_irq_handler_mus }
};

static struct OKIM6295interface okim6295_interface_6061 =
{
	1,  /* 1 chip */
	{ 6061 },
	{ REGION_SMP1 },
	{ 150 }
};

static struct OKIM6295interface okim6295_interface_7576 =
{
	1,  /* 1 chip */
	{ 7576 },
	{ REGION_SMP1 },
	{ 150 }
};

static struct SOUND_INFO cps1_sound[] =
{
   { SOUND_YM2151S,  &ym2151_interface,    },
   { SOUND_M6295, &okim6295_interface_7576     },
   { 0,              NULL,                 },
};

static struct QSound_interface qsound_interface =
{
	QSOUND_CLOCK,
	REGION_SMP1,
	{ 250,250 }
};

static struct SOUND_INFO qsound_sound[] =
{
   { SOUND_QSOUND, &qsound_interface     },
   { 0,              NULL,                 },
};

static struct SOUND_INFO forgottn_sound[] =
{
   { SOUND_YM2151S,  &ym2151_interface,    },
   { SOUND_M6295, &okim6295_interface_6061     },
   { 0,              NULL,                 },
};

#define cps1_game(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

#define cps1_clone(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE,MYCLONE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { ROMOF( MYCLONE ) },                     \
   { CLONEOF( MYCLONE ) },                   \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

// same thing with 4 buttons

#define cps1b4_game(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1b4_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

#define cps1b4_clone(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE,MYCLONE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { ROMOF( MYCLONE ) },                     \
   { CLONEOF( MYCLONE ) },                   \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1b4_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

// Forgottn : only 1 game and 1 clone, and we need all this because of sound

#define forgottn_game(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   forgottn_sound,        \
   TYPE, \
}

#define forgottn_clone(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE,MYCLONE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { ROMOF( MYCLONE ) },                     \
   { CLONEOF( MYCLONE ) },                   \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   forgottn_sound,        \
   TYPE, \
}

// rotate 270

#define cps1_game_270(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video_270,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

#define cps1_clone_270(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE,CLONE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { ROMOF( CLONE ) },                     \
   { CLONEOF( CLONE ) },                   \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video_270,       \
   execute_cps1_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

// sf2 is using a special 12Mhz frame...

#define sf2_game(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   sf2_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_sf2_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

#define sf2_clone(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE,MYCLONE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { ROMOF( MYCLONE ) },                     \
   { CLONEOF( MYCLONE ) },                   \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   sf2_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_cps1,          \
   clear_cps1,         \
   &cps1_video,       \
   execute_sf2_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   cps1_sound,        \
   TYPE, \
}

// qsound

extern void clear_qsound();

#define qsound_game(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_qsound,          \
   clear_qsound,         \
   &cps1_video,       \
   execute_qsound_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   NULL, /* qsound_sound, */        \
   TYPE, \
}

#define qsound_clone(GAME,NAME,LONGNAME,ROMS,YEAR,DSW,TYPE,MYCLONE) \
                                           \
static struct DIR_INFO _##GAME##_dirs[] =     \
{                                          \
   { NAME, },                              \
   { ROMOF( MYCLONE ) },                     \
   { CLONEOF( MYCLONE ) },                   \
   { NULL, },                              \
};                                         \
                                           \
struct GAME_MAIN game_##GAME = \
{                           \
   _##GAME##_dirs,         \
   ROMS,                    \
   cps1_inputs,       \
   DSW,          \
   NULL,                    \
                            \
   load_qsound,          \
   clear_qsound,         \
   &cps1_video,       \
   execute_qsound_frame, \
   NAME,             \
   LONGNAME,         \
   NULL,                    \
   COMPANY_ID_CAPCOM,       \
   NULL,                    \
   YEAR,                    \
   NULL, /* qsound_sound, */       \
   TYPE, \
}

cps1_game(ghouls,"ghouls","Ghouls'n Ghosts (World)" ,ghouls_roms,1988,ghouls_dsw,GAME_MISC);
cps1_clone(ghoulsu,"ghoulsu","Ghouls'n Ghosts (US)" ,ghoulsu_roms,1988,ghouls_dsw,GAME_MISC, "ghouls");
cps1_clone(daimakai,"daimakai","Dai Makai-Mura (Japan)" ,daimakai_roms,1988,ghouls_dsw,GAME_MISC, "ghouls");
cps1_game(strider,"strider","Strider (US)" ,strider_roms,1989,strider_dsw,GAME_MISC);
cps1_clone(striderj,"striderj","Strider Hiryu (Japan set 1)" ,striderj_roms,1989,strider_dsw,GAME_MISC, "strider");
cps1_clone(stridrja,"stridrja","Strider Hiryu (Japan set 2)" ,stridrja_roms,1989,strider_dsw,GAME_MISC, "strider");
cps1_game(dwj,"dwj","Tenchi wo Kurau (Japan)" ,dwj_roms,1989,dwj_dsw,GAME_MISC);
cps1_game(willow,"willow","Willow (Japan,English)" ,willow_roms,1989,willow_dsw,GAME_MISC);
cps1_clone(willowj,"willowj","Willow (Japan,Japanese)" ,willowj_roms,1989,willow_dsw,GAME_MISC, "willow");
cps1_game(unsquad,"unsquad","U.N. Squadron (US)" ,unsquad_roms,1989,unsquad_dsw,GAME_MISC);
cps1_clone(area88,"area88","Area 88 (Japan)" ,area88_roms,1989,unsquad_dsw,GAME_MISC, "unsquad");
cps1_game(ffight,"ffight","Final Fight (World)" ,ffight_roms,1989,ffight_dsw,GAME_MISC);
cps1_clone(ffightu,"ffightu","Final Fight (US)" ,ffightu_roms,1989,ffight_dsw,GAME_MISC, "ffight");
cps1_clone(ffightj,"ffightj","Final Fight (Japan)" ,ffightj_roms,1989,ffight_dsw,GAME_MISC, "ffight");
cps1_game_270(1941,"1941","1941 - Counter Attack (World)" ,_1941_roms,1990,_1941_dsw,GAME_MISC);
cps1_clone_270(1941j,"1941j","1941 - Counter Attack (Japan)" ,_1941j_roms,1990,_1941_dsw,GAME_MISC, "1941");
cps1_game_270(mercs,"mercs","Mercs (World)" ,mercs_roms,1990,mercs_dsw,GAME_MISC);
cps1_clone_270(mercsu,"mercsu","Mercs (US)" ,mercsu_roms,1990,mercs_dsw,GAME_MISC, "mercs");
cps1_clone_270(mercsj,"mercsj","Senjo no Ookami II (Japan)" ,mercsj_roms,1990,mercs_dsw,GAME_MISC, "mercs");
cps1_game(mtwins,"mtwins","Mega Twins (World)" ,mtwins_roms,1990,mtwins_dsw,GAME_MISC);
cps1_clone(chikij,"chikij","Chiki Chiki Boys (Japan)" ,chikij_roms,1990,mtwins_dsw,GAME_MISC, "mtwins");
cps1_game(msword,"msword","Magic Sword - Heroic Fantasy (World)" ,msword_roms,1990,msword_dsw,GAME_MISC);
cps1_clone(mswordu,"mswordu","Magic Sword - Heroic Fantasy (US)" ,mswordu_roms,1990,msword_dsw,GAME_MISC, "msword");
cps1_clone(mswordj,"mswordj","Magic Sword (Japan)" ,mswordj_roms,1990,msword_dsw,GAME_MISC, "msword");
cps1_game(cawing,"cawing","Carrier Air Wing (World)" ,cawing_roms,1990,cawing_dsw,GAME_MISC);
cps1_clone(cawingj,"cawingj","U.S. Navy (Japan)" ,cawingj_roms,1990,cawing_dsw,GAME_MISC, "cawing");
cps1_game(nemo,"nemo","Nemo (World)" ,nemo_roms,1990,nemo_dsw,GAME_MISC);
cps1_clone(nemoj,"nemoj","Nemo (Japan)" ,nemoj_roms,1990,nemo_dsw,GAME_MISC, "nemo");
cps1_game(3wonders,"3wonders","Three Wonders (World)" ,_3wonders_roms,1991,_3wonders_dsw,GAME_MISC);
cps1_clone(3wonderu,"3wonderu","Three Wonders (US)" ,_3wonderu_roms,1991,_3wonders_dsw,GAME_MISC, "3wonders");
cps1_clone(wonder3,"wonder3","Wonder 3 (Japan)" ,wonder3_roms,1991,_3wonders_dsw,GAME_MISC, "3wonders");
cps1_game(kod,"kod","The King of Dragons (World)" ,kod_roms,1991,kod_dsw,GAME_MISC);
cps1_clone(kodu,"kodu","The King of Dragons (US)" ,kodu_roms,1991,kod_dsw,GAME_MISC, "kod");
cps1_clone(kodj,"kodj","The King of Dragons (Japan)" ,kodj_roms,1991,kod_dsw,GAME_MISC, "kod");
cps1_game(captcomm,"captcomm","Captain Commando (World)" ,captcomm_roms,1991,captcomm_dsw,GAME_MISC);
cps1_clone(captcomu,"captcomu","Captain Commando (US)" ,captcomu_roms,1991,captcomm_dsw,GAME_MISC, "captcomm");
cps1_clone(captcomj,"captcomj","Captain Commando (Japan)" ,captcomj_roms,1991,captcomm_dsw,GAME_MISC, "captcomm");
cps1_game(knights,"knights","Knights of the Round (World)" ,knights_roms,1991,knights_dsw,GAME_MISC);
cps1_clone(knightsu,"knightsu","Knights of the Round (US)" ,knightsu_roms,1991,knights_dsw,GAME_MISC, "knights");
cps1_clone(knightsj,"knightsj","Knights of the Round (Japan)" ,knightsj_roms,1991,knights_dsw,GAME_MISC, "knights");
cps1_game_270(varth,"varth","Varth - Operation Thunderstorm (World)" ,varth_roms,1992,varth_dsw,GAME_MISC);
cps1_clone_270(varthu,"varthu","Varth - Operation Thunderstorm (US)" ,varthu_roms,1992,varth_dsw,GAME_MISC, "varth");
cps1_clone_270(varthj,"varthj","Varth - Operation Thunderstorm (Japan)" ,varthj_roms,1992,varth_dsw,GAME_MISC, "varth");
cps1b4_game(cworld2j,"cworld2j","Capcom World 2 (Japan)" ,cworld2j_roms,1992,cworld2j_dsw,GAME_MISC);
cps1b4_game(qad,"qad","Quiz & Dragons (US)" ,qad_roms,1992,qad_dsw,GAME_MISC);
cps1b4_clone(qadj,"qadj","Quiz & Dragons (Japan)" ,qadj_roms,1994,qadj_dsw,GAME_MISC, "qad");
cps1b4_game(qtono2,"qtono2","Quiz Tonosama no Yabou 2 Zenkoku-ban (Japan)" ,qtono2_roms,1995,qtono2_dsw,GAME_MISC);
cps1_game(megaman,"megaman","Mega Man - The Power Battle (Asia)" ,megaman_roms,1995,megaman_dsw,GAME_MISC);
cps1_clone(rockmanj,"rockmanj","Rockman - The Power Battle (Japan)" ,rockmanj_roms,1995,megaman_dsw,GAME_MISC, "megaman");
cps1_game(pnickj,"pnickj","Pnickies (Japan)" ,pnickj_roms,1994,pnickj_dsw,GAME_MISC);

forgottn_game(forgottn,"forgottn","Forgotten Worlds (US)" ,forgottn_roms,1988,forgottn_dsw,GAME_MISC);
forgottn_clone(lostwrld,"lostwrld","Lost Worlds (Japan)" ,lostwrld_roms,1988,forgottn_dsw,GAME_MISC, "forgottn");

sf2_game(sf2,"sf2","SF2 - The World Warrior (World 910214)" ,sf2_roms,1991,sf2_dsw,GAME_MISC);
sf2_clone(sf2ua,"sf2ua","SF2 - The World Warrior (US 910206)" ,sf2ua_roms,1991,sf2_dsw,GAME_MISC, "sf2");
sf2_clone(sf2ub,"sf2ub","SF2 - The World Warrior (US 910214)" ,sf2ub_roms,1991,sf2_dsw,GAME_MISC, "sf2");
sf2_clone(sf2ue,"sf2ue","SF2 - The World Warrior (US 910228)" ,sf2ue_roms,1991,sf2_dsw,GAME_MISC, "sf2");
sf2_clone(sf2ui,"sf2ui","SF2 - The World Warrior (US 910522)" ,sf2ui_roms,1991,sf2_dsw,GAME_MISC, "sf2");
sf2_clone(sf2j,"sf2j","SF2 - The World Warrior (Japan 911210)" ,sf2j_roms,1991,sf2j_dsw,GAME_MISC, "sf2");
sf2_clone(sf2ja,"sf2ja","SF2 - The World Warrior (Japan 910214)" ,sf2ja_roms,1991,sf2j_dsw,GAME_MISC, "sf2");
sf2_clone(sf2jc,"sf2jc","SF2 - The World Warrior (Japan 910306)" ,sf2jc_roms,1991,sf2j_dsw,GAME_MISC, "sf2");
sf2_game(sf2ce,"sf2ce","SF2' - Champion Edition (World)" ,sf2ce_roms,1992,sf2_dsw,GAME_MISC);
sf2_clone(sf2ceua,"sf2ceua","SF2' - Champion Edition (US rev A)" ,sf2ceua_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2ceub,"sf2ceub","SF2' - Champion Edition (US rev B)" ,sf2ceub_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2cej,"sf2cej","SF2' - Champion Edition (Japan)" ,sf2cej_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2rb,"sf2rb","SF2' - Champion Edition (Rainbow set 1)" ,sf2rb_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2rb2,"sf2rb2","SF2' - Champion Edition (Rainbow set 2)" ,sf2rb2_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2red,"sf2red","SF2' - Champion Edition (Red Wave)" ,sf2red_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2v004,"sf2v004","SF2! - Champion Edition (V004)" ,sf2v004_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2accp2,"sf2accp2","SF2' - Champion Edition (Accelerator Pt.II)" ,sf2accp2_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2t,"sf2t","SF2' - Hyper Fighting (US)" ,sf2t_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");
sf2_clone(sf2tj,"sf2tj","SF2' Turbo - Hyper Fighting (Japan)" ,sf2tj_roms,1992,sf2_dsw,GAME_MISC, "sf2ce");

// Pang3

cps1_game(pang3,"pang3","Pang! 3 (Euro)" ,pang3_roms,1995,pang3_dsw,GAME_MISC);
cps1_clone(pang3j,"pang3j","Pang! 3 (Japan)" ,pang3j_roms,1995,pang3_dsw,GAME_MISC, "pang3");

// qsound

qsound_game(wof,"wof","Warriors of Fate (World)" ,wof_roms,1992,wof_dsw,GAME_MISC);
qsound_clone(wofa,"wofa","Sangokushi II (Asia)" ,wofa_roms,1992,wof_dsw,GAME_MISC, "wof");
qsound_clone(wofu,"wofu","Warriors of Fate (US)" ,wofu_roms,1992,wof_dsw,GAME_MISC, "wof");
qsound_clone(wofj,"wofj","Tenchi wo Kurau II - Sekiheki no Tatakai (Japan)" ,wofj_roms,1992,wof_dsw,GAME_MISC, "wof");
qsound_game(dino,"dino","Cadillacs and Dinosaurs (World)" ,dino_roms,1993,dino_dsw,GAME_MISC);
qsound_clone(dinoj,"dinoj","Cadillacs Kyouryuu-Shinseiki (Japan)" ,dinoj_roms,1993,dino_dsw,GAME_MISC, "dino");
qsound_game(punisher,"punisher","The Punisher (World)" ,punisher_roms,1993,punisher_dsw,GAME_MISC);
qsound_clone(punishru,"punishru","The Punisher (US)" ,punishru_roms,1993,punisher_dsw,GAME_MISC, "punisher");
qsound_clone(punishrj,"punishrj","The Punisher (Japan)" ,punishrj_roms,1993,punisher_dsw,GAME_MISC, "punisher");
qsound_game(slammast,"slammast","Saturday Night Slam Masters (World)" ,slammast_roms,1993,slammast_dsw,GAME_MISC);
qsound_clone(slammasu,"slammasu","Saturday Night Slam Masters (US)" ,slammasu_roms,1993,slammast_dsw,GAME_MISC, "slammast");
qsound_clone(mbomberj,"mbomberj","Muscle Bomber - The Body Explosion (Japan)" ,mbomberj_roms,1993,slammast_dsw,GAME_MISC, "slammast");
qsound_clone(mbombrd,"mbombrd","Muscle Bomber Duo - Ultimate Team Battle (World)" ,mbombrd_roms,1993,slammast_dsw,GAME_MISC, "slammast");
qsound_clone(mbombrdj,"mbombrdj","Muscle Bomber Duo - Heat Up Warriors (Japan)" ,mbombrdj_roms,1993,slammast_dsw,GAME_MISC, "slammast");
