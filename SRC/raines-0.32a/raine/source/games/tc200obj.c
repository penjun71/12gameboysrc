/******************************************************************************/
/*                                                                            */
/*              tc0200obj: TAITO OBJECT LAYER CHIP (F2-SYSTEM)                */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "tc200obj.h"

/*

This chip mainly exists in F2-System games.
It seems to behave slightly differently in most games (banking methods,
disable methods, offset methods). Horrible.

OBJECT RAM
----------

- 16 bytes/sprite
- 1024 sprites (0x4000 bytes)

-----+--------+-----------------------------------------------------------------
Byte | Bit(s) | Use
-----+76543210+-----------------------------------------------------------------
  0  |xxxxxxxx| Sprite Tile (high)
  1  |xxxxxxxx| Sprite Tile (low)
  2  |xxxxxxxx| Zoom Y
  3  |xxxxxxxx| Zoom X
  4  |.x......| Add Relative X and Y offsets [Final Blow]
  4  |.x......| Reset Relative X and Y offsets to 0 [Liquid Kids/Don Doko Don]
  4  |....xxxx| Sprite X (high)
  5  |xxxxxxxx| Sprite X (low)
  6  |x.......| Disable/Enable Sprites [Gun Frontier/Metal Black/Football Champ/Dead Connection]
  6  |....xxxx| Sprite Y (high)
  7  |xxxxxxxx| Sprite Y (low)
  8  |.....x..| Update Colour Bank and Zoom X/Y registers
  8  |......x.| Flip X Axis
  8  |.......x| Flip Y Axis
  9  |xxxxxxxx| Colour Bank
  A  |x.......| Jump to sprite Flag [Darius Gaiden]
  A  |....x...| Jump to sprite Bank A/B [Darius Gaiden]
  A  |......xx| Jump to sprite Number (high) [Darius Gaiden]
  B  |xxxxxxxx| Jump to sprite Number (low) [Darius Gaiden]
  B  |.......x| Last Sprite Marker
  D  |.......x| Last Sprite Marker (alt)
-----+--------+-----------------------------------------------------------------

Zoom:

00 = Full Size (16 sprites would span 256 pixels)
FF = Min Size  (16 sprites would span 1 pixel)

Direct Mapped Games:

Mega Blast              - OK
Thunder Fox             - OK
Camel Try               - OK [Colour Based Priorities]

Mapped 64 Colour Games:

Final Blow              - OK

Direct Mapped Rotate 180� Games:

Liquid Kids             - OK [Control bits work differently]
Don Doko Don            - OK [Control bits work differently]

Mapped Rotate 270� Games:

Gun Frontier            - OK

render_tc0200obj_mapped_f3system_b:

Arkanoid Returns        - ?
Puzzle Bobble 2         - OK
Puzzle Bobble 3         - OK
Puzzle Bobble 4         - OK
Pop'n Pop               - OK
Puchi Carat             - OK

render_tc0200obj_mapped_fchamp:

Dead Connection         - OK [bank 0x0A]
Football Champ          - OK [bank ????]
Euro Champ 92           - OK [bank ????]

*/

#define OBJ_XY_MASK	0xFFF

static UINT8 *zoom16_ofs;

static int object_id;

void init_tc0200obj(void)
{
   init_16x16_zoom();
   zoom16_ofs = make_16x16_zoom_ofs_type1();

   object_id = add_layer_info("Object");
   //object_id = add_layer_info("TC0200OBJ:OBJECT");
}

static UINT16 bank_data[8];

void make_object_bank(UINT8 *src)
{
   bank_data[0] = ((ReadWord(src+0x04)<<11)&0x3800)+0x0000;
   bank_data[1] = ((ReadWord(src+0x04)<<11)&0x3800)+0x0400;
   bank_data[2] = ((ReadWord(src+0x06)<<11)&0x3800)+0x0000;
   bank_data[3] = ((ReadWord(src+0x06)<<11)&0x3800)+0x0400;
   bank_data[4] = ((ReadWord(src+0x08)<<10)&0x3C00)+0x0000;
   bank_data[5] = ((ReadWord(src+0x0A)<<10)&0x3C00)+0x0000;
   bank_data[6] = ((ReadWord(src+0x0C)<<10)&0x3C00)+0x0000;
   bank_data[7] = ((ReadWord(src+0x0E)<<10)&0x3C00)+0x0000;
}

void make_object_bank_koshien(UINT8 *src)
{
   UINT32 data;
   static UINT32 my_data;

   data = ReadWord(src);

   if(key[KEY_LEFT]) my_data -= 0x0400;
   if(key[KEY_RIGHT]) my_data += 0x0400;

   my_data &= 0x3C00;

   #ifdef RAINE_DEBUG
   clear_ingame_message_list();
   print_ingame(60,"K_BANK[%04x] %04x",data, my_data);
   #endif

   //0101/0210/0543/0550/0554/0643

   switch(data){
   case 0x0101:
   bank_data[0]= 0x0000;
   bank_data[1]= 0x0400;
   bank_data[2]= 0x0800;
   bank_data[3]= 0x0C00;
   bank_data[4]= 0x0000;
   bank_data[5]= 0x0400;
   bank_data[6]= 0x0800;
   bank_data[7]= 0x0C00;
   break;
   case 0x0210:
   bank_data[0]= 0x0000;	// *
   bank_data[1]= 0x0400;	// *
   bank_data[2]= 0x0800;	// *
   bank_data[3]= 0x0C00;
   bank_data[4]= 0x1000;	// *
   bank_data[5]= 0x1400;
   bank_data[6]= 0x1800;	// *
   bank_data[7]= 0x1C00;
   break;
   case 0x0543:
   bank_data[0]= 0x0000;
   bank_data[1]= 0x0400;
   bank_data[2]= 0xFFFF;
   bank_data[3]= 0xFFFF;
   bank_data[4]= 0x2800;
   bank_data[5]= 0x2C00;
   bank_data[6]= 0x3000;
   bank_data[7]= 0x3400;
   break;
   case 0x0550:
   bank_data[0]= 0x0000;	// *
   bank_data[1]= 0x0400;	// *
   bank_data[2]= 0xFFFF;
   bank_data[3]= 0xFFFF;
   bank_data[4]= 0x2800;
   bank_data[5]= 0x2C00;
   bank_data[6]= 0x3000;	// *
   bank_data[7]= 0x3400;	// *
   break;
   case 0x0554:
   bank_data[0]= 0x0000;
   bank_data[1]= 0x0400;
   bank_data[2]= 0xFFFF;
   bank_data[3]= 0xFFFF;
   bank_data[4]= 0x2800;
   bank_data[5]= 0x2C00;
   bank_data[6]= 0x3000;
   bank_data[7]= 0x3400;
   break;
   case 0x0643:
   bank_data[0]= 0x0000;	// *
   bank_data[1]= 0x0400;	// *
   bank_data[2]= 0x2000;
   bank_data[3]= 0x2400;
   bank_data[4]= 0x2800;
   bank_data[5]= 0x2C00;	// *
   bank_data[6]= 0x3800;	// *
   bank_data[7]= 0x3C00;	// *
   break;
   default:
   bank_data[0]= 0xFFFF;
   bank_data[1]= 0xFFFF;
   bank_data[2]= 0xFFFF;
   bank_data[3]= 0xFFFF;
   bank_data[4]= 0xFFFF;
   bank_data[5]= 0xFFFF;
   bank_data[6]= 0xFFFF;
   bank_data[7]= 0xFFFF;
   break;
   }
}

void render_tc0200obj_mapped_b(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;

   rel_x=ReadWord(&RAM_BG[0x14]);
   rel_y=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x20;zz<0x4000;zz+=16){

      if(RAM_BG[zz+13]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_yuyugogo(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_TILE;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   RAM_TILE  = tc0200obj.RAM_TILE;

   x=0;
   y=0;
   xn=0;
   yn=0;

   rel_x=ReadWord(&RAM_BG[0x14]);
   rel_y=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   RAM_TILE+=4;
   for(zz=0x20;zz<0x4000;zz+=16,RAM_TILE+=2){

      if(RAM_BG[zz+13]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta  = ReadWord(&RAM_BG[zz])&0x03FF;
      ta |= (ReadWord(&RAM_TILE[0])<<10)&0xFC00;
      ta &= tile_mask;

      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_yuyugogo2(int pri)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_TILE;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   RAM_TILE  = tc0200obj.RAM_TILE;

   if(pri) pri = 0x80;

   x=0;
   y=0;
   xn=0;
   yn=0;

   if(((ReadWord(&RAM_BG[0x0A])&0x0001)==0) && (tc0200obj.RAM_B)){
      RAM_BG    = tc0200obj.RAM_B;
      RAM_TILE  = tc0200obj.RAM_TILE_B;
   }

   rel_x=ReadWord(&RAM_BG[0x14]);
   rel_y=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   RAM_TILE+=6;
   for(zz=0x30;zz<0x4000;zz+=16,RAM_TILE+=2){

      if(RAM_BG[zz+13]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((tb&0x80)==pri){

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta  = ReadWord(&RAM_BG[zz])&0x00FF;
      ta |= (ReadWord(&RAM_TILE[0])<<8)&0xFF00;
      ta &= tile_mask;

      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }
      }
      }

   }

}

void render_tc0200obj_mapped_64(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;

   rel_x=ReadWord(&RAM_BG[0x14]);
   rel_y=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x20;zz<0x4000;zz+=16){

      if(RAM_BG[zz+13]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MULTI_MAPPED_NEW(
            tb,
            64,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_r180_mapped(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     =(tc0200obj.bmp_x + tc0200obj.bmp_w) - tc0200obj.ofs_x;
   ofs_y     =(tc0200obj.bmp_y + tc0200obj.bmp_h) - tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;

   rel_x=ReadWord(&RAM_BG[0x24]);
   rel_y=ReadWord(&RAM_BG[0x26]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      //if(RAM_BG[zz+13]!=0) return;
      if(RAM_BG[zz+11]!=0) return;
      if((RAM_BG[zz+5]&0x10)!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x40)==0){
         if((td&0x80)==0){			// 0x04

            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0){
            rel_x=0;
            rel_y=0;
            }

            x = ofs_x - (ReadWord(&RAM_BG[zz+4])+rel_x);
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
         x  = (x-zx)&OBJ_XY_MASK;
      }
      else{
         if((td&0x80)!=0){
            zx = zoom_dat_x[tx++];
            x  = (x-zx)&OBJ_XY_MASK;
         }
      }

      if((td&0x10)==0){
         if((td&0x40)==0){					// 0x04
            y = ofs_y - (ReadWord(&RAM_BG[zz+6])+rel_y);
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
         y  = (y-zy)&OBJ_XY_MASK;
      }
      else{
         if((td&0x20)!=0){
            zy = zoom_dat_y[ty++];
            y  = (y-zy)&OBJ_XY_MASK;
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x03: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x00: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

/*

Gun Frontier

*/

void render_tc0200obj_mapped_r270(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y,spr_on;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     =(tc0200obj.bmp_y + tc0200obj.bmp_h) - tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;
   spr_on=0;

   if(((ReadWord(&RAM_BG[0x0A])&0x0001)==0) && (tc0200obj.RAM_B)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_y=ReadWord(&RAM_BG[0x14]);
   rel_x=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      //if(RAM_BG[zz+13]!=0) return;
      if(RAM_BG[zz+11]!=0) return;

      if((ReadWord(&RAM_BG[zz+6])&0x8000)!=0) spr_on^=1;

      if(spr_on!=0){

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x20)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            y = ofs_y - ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y -= rel_y;		// Minus?
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
         y  = (y-zy)&OBJ_XY_MASK;
      }
      else{
         if((td&0x80)!=0){
            zy = zoom_dat_y[ty++];
            y  = (y-zy)&OBJ_XY_MASK;
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

      }
   }

}

void render_tc0200obj_mapped(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y,spr_on;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;
   spr_on=0;

   if(((ReadWord(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_y=ReadWord(&RAM_BG[0x14]);
   rel_x=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+13]!=0) return;

      if((ReadWord(&RAM_BG[zz+6])&0x8000)!=0) spr_on^=1;

      if(spr_on!=0){

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

      }

   }

}

/*

diff:

- start at 0x20
- spr_on start = 1
- bank switch at 0x33EA

*/

void render_tc0200obj_mapped_soliltary_fighter(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y,spr_on;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;
   spr_on=1;

   // solitary fighter

   if(((ReadWord(&RAM_BG[0x33EA])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   // growl (wrong?)

   if(((ReadWord(&RAM_BG[0x321A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_y = ReadWord(&RAM_BG[0x14]);
   rel_x = ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x20;zz<0x4000;zz+=16){

      if(RAM_BG[zz+13]!=0) return;

      if((ReadWord(&RAM_BG[zz+6])&0x8000)!=0) spr_on^=1;

      if(spr_on!=0){

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);

            //if((ReadWord(&RAM_BG[zz+4])&0x8000)!=0) rel_y = ReadWord(&RAM_BG[zz+6]);
            //else y += rel_y;

            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;

            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);

            //if((ReadWord(&RAM_BG[zz+4])&0x8000)!=0) rel_x = ReadWord(&RAM_BG[zz+4]);
            //else x += rel_x;

            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;

            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta  = ReadWord(&RAM_BG[zz]);
      ta  = (ta&0x3FF) | bank_data[(ta>>10)&7];
      ta &= tile_mask;

      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

      }

   }

}

void render_tc0200obj_mapped_fchamp(int pri)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y,spr_on;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;
   spr_on=0;

   if((ReadWord(&RAM_BG[0x0A])&0x0001) && (tc0200obj.RAM_B)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_y=ReadWord(&RAM_BG[0x14]);
   rel_x=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   if((ReadWord(&RAM_BG[0x06])&0x8000)!=0){
      if((ReadWord(&RAM_BG[0x0A])&0x1000)!=0) spr_on=0;
      else spr_on=1;
   }

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+13]!=0) return;

      if((ReadWord(&RAM_BG[zz+0x06])&0x8000)!=0){
         if((ReadWord(&RAM_BG[zz+0x0A])&0x1000)!=0) spr_on=0;
         else spr_on=1;
      }

      if(spr_on!=0){

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x80)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      if((tb&0xC0) == pri){

      ta  = ReadWord(&RAM_BG[zz]);
      ta  = (ta&0x3FF) | bank_data[(ta>>10)&7];
      ta &= tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

      }

      }

   }

}

/*

diff to football champ:

- start at 0x0020
- no object bank

*/

void render_tc0200obj_mapped_cameltry(int pri)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y,spr_on;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;
   spr_on=0;

   if(((ReadWord(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_y=ReadWord(&RAM_BG[0x14]);
   rel_x=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   if((ReadWord(&RAM_BG[0x06])&0x8000)!=0){
      if((ReadWord(&RAM_BG[0x0A])&0x1000)!=0) spr_on=0;
      else spr_on=1;
   }

   for(zz=0x20;zz<0x4000;zz+=16){

      if(RAM_BG[zz+13]!=0) return;

      if((ReadWord(&RAM_BG[zz+0x06])&0x8000)!=0){
         if((ReadWord(&RAM_BG[zz+0x0A])&0x1000)!=0) spr_on=0;
         else spr_on=1;
      }

      if(spr_on!=0){

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x80)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      if((tb&0xC0) == pri){

      ta  = ReadWord(&RAM_BG[zz]) & tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

      }

      }

   }

}

void render_tc0200obj_mapped_r270_b(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     =(tc0200obj.bmp_y + tc0200obj.bmp_h) - tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;

   rel_y=ReadWord(&RAM_BG[0x14]);
   rel_x=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x20;zz<0x4000;zz+=16){

      if(RAM_BG[zz+11]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) rel_x = ReadWord(&RAM_BG[zz+6]);
            else x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x20)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            y = ofs_y - ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) rel_y = ReadWord(&RAM_BG[zz+4]);
            else y -= rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
         y  = (y-zy)&OBJ_XY_MASK;
      }
      else{
         if((td&0x80)!=0){
            zy = zoom_dat_y[ty++];
            y  = (y-zy)&OBJ_XY_MASK;
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_r270_b_rot(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   x=0;
   y=0;
   xn=0;
   yn=0;

   rel_x=ReadWord(&RAM_BG[0x14]);
   rel_y=ReadWord(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x20;zz<0x4000;zz+=16){

      if(RAM_BG[zz+11]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) rel_y = ReadWord(&RAM_BG[zz+6]);
            else y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) rel_x = ReadWord(&RAM_BG[zz+4]);
            else x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_pulirula(void)
{
   int x,y,ta,tb,zz;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_TILE;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   RAM_TILE  = tc0200obj.RAM_TILE;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;

   //if(((ReadWord(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
   if(((ReadWord(&RAM_BG[0x3216])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
      RAM_TILE = tc0200obj.RAM_TILE_B;
   }

   rel_y=ReadWord(&RAM_BG[0x24]);
   rel_x=ReadWord(&RAM_BG[0x26]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   RAM_TILE+=6;
   for(zz=0x30;zz<0x4000;zz+=16,RAM_TILE+=2){

      if(RAM_BG[zz+13]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);
            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord(&RAM_TILE[0])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            16,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_opwolf3(void)
{
   int x,y,ta,tb,zz,cols;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_TILE;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   RAM_TILE  = tc0200obj.RAM_TILE;

   cols      = tc0200obj.cols;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;

   //if(((ReadWord(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
   if(((ReadWord(&RAM_BG[0x3216])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
      RAM_TILE = tc0200obj.RAM_TILE_B;
   }

   rel_y=ReadWord(&RAM_BG[0x24]);
   rel_x=ReadWord(&RAM_BG[0x26]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   RAM_TILE+=6;
   for(zz=0x30;zz<0x4000;zz+=16,RAM_TILE+=2){

      if(RAM_BG[zz+13]!=0) return;

      td=RAM_BG[zz+9];

      if((td&0x04)==0){
         tb = RAM_BG[zz+8];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord(&RAM_BG[zz+6]);

            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) rel_y = ReadWord(&RAM_BG[zz+6]);
            else y += rel_y;

            //if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;

            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord(&RAM_BG[zz+4]);

            if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) rel_x = ReadWord(&RAM_BG[zz+4]);
            else x += rel_x;

            //if((ReadWord(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;

            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta  = ReadWord(&RAM_BG[zz])&0x00FF;
      ta |= ReadWord(&RAM_TILE[0])&0xFF00;
      ta &= tile_mask;

      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb,
            cols,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_f3system(void)
{
   int x,y,ta,tb,zz,cols;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   cols      = tc0200obj.cols;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;

   if(((ReadWord68k(&RAM_BG[0x0A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }
   if(((ReadWord68k(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_x=ReadWord68k(&RAM_BG[0x14]);
   rel_y=ReadWord68k(&RAM_BG[0x16]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+12]!=0) return;

      td=RAM_BG[zz+8];

      if((td&0x04)==0){
         tb = RAM_BG[zz+9];
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord68k(&RAM_BG[zz+4]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y + ReadWord68k(&RAM_BG[zz+6]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord68k(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb|0x100,
            cols,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_f3system_b(void)
{
   int x,y,ta,tb,zz,cols;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   int rel_x2,rel_y2;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   cols      = tc0200obj.cols;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;


   if(((ReadWord68k(&RAM_BG[0x0A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }
   if(((ReadWord68k(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_x=ReadWord68k(&RAM_BG[0x14]);
   rel_y=ReadWord68k(&RAM_BG[0x16]);

   rel_x2=ReadWord68k(&RAM_BG[0x24]);
   rel_y2=ReadWord68k(&RAM_BG[0x26]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+12]!=0) return;

      td=RAM_BG[zz+8];

      if((td&0x04)==0){
         tb = RAM_BG[zz+9];
      }

      if((td&0x40)==0){
         if((td&0x80)==0){
            x = ofs_x + ReadWord68k(&RAM_BG[zz+4]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            else x += (rel_x2+rel_x);
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x80)==0){
            y = ofs_y + ReadWord68k(&RAM_BG[zz+6]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            else y += (rel_y2+rel_y);
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord68k(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MULTI_MAPPED_NEW(
            tb|0x100,
            cols,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_f3system_b_multi(UINT8 *colour_mask)
{
   int x,y,ta,tb,zz,cols;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   int rel_x2,rel_y2;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   UINT8 *COL_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   COL_MSK   = colour_mask;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     = tc0200obj.bmp_y + tc0200obj.ofs_y;

   cols      = tc0200obj.cols;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;


   if(((ReadWord68k(&RAM_BG[0x0A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }
   if(((ReadWord68k(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_x=ReadWord68k(&RAM_BG[0x14]);
   rel_y=ReadWord68k(&RAM_BG[0x16]);

   rel_x2=ReadWord68k(&RAM_BG[0x24]);
   rel_y2=ReadWord68k(&RAM_BG[0x26]);

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+12]!=0) return;

      td=RAM_BG[zz+8];

      if((td&0x04)==0){
         tb = RAM_BG[zz+9];
      }

      if((td&0x40)==0){
         if((td&0x80)==0){
            x = ofs_x + ReadWord68k(&RAM_BG[zz+4]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            else x += (rel_x2+rel_x);
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x80)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x10)==0){
         if((td&0x80)==0){
            y = ofs_y + ReadWord68k(&RAM_BG[zz+6]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) y += rel_y;
            else y += (rel_y2+rel_y);
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
      }
      else{
         if((td&0x20)!=0){
            y  = (y+zy)&OBJ_XY_MASK;
            zy = zoom_dat_y[ty++];
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord68k(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MULTI_MAPPED_NEW(
            tb|0x100,
            COL_MSK[ta],
            map
         );

         if(RAM_MSK[ta]==1)
            Draw16x16_Trans_Mapped_ZoomXY_flip_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy,td&3);
         else
            Draw16x16_Mapped_ZoomXY_flip_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy,td&3);

      }

      }

   }

}

void render_tc0200obj_mapped_f3system_r180(void)
{
   int x,y,ta,tb,zz,cols;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   int rel_x2,rel_y2;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     =(tc0200obj.bmp_x + tc0200obj.bmp_w) - tc0200obj.ofs_x;
   ofs_y     =(tc0200obj.bmp_y + tc0200obj.bmp_h) - tc0200obj.ofs_y;

   cols      = tc0200obj.cols;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;

   rel_x=ReadWord68k(&RAM_BG[0x14]);
   rel_y=ReadWord68k(&RAM_BG[0x16]);

   rel_x2=ReadWord68k(&RAM_BG[0x24]);
   rel_y2=ReadWord68k(&RAM_BG[0x26]);

   if(((ReadWord68k(&RAM_BG[0x0A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }
   if(((ReadWord68k(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   tb = 0;
   zoom_dat_x = zoom16_ofs+(0<<5);
   zoom_dat_y = zoom16_ofs+(0<<5);
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+12]!=0) return;

      td=RAM_BG[zz+8];

      if((td&0x04)==0){
         tb = RAM_BG[zz+9];
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            x = ofs_x - ReadWord68k(&RAM_BG[zz+4]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) x -= rel_x;
            else x -= (rel_x2+rel_x);
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
         x  = (x-zx)&OBJ_XY_MASK;
      }
      else{
         if((td&0x80)!=0){
            zx = zoom_dat_x[tx++];
            x  = (x-zx)&OBJ_XY_MASK;
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            y = ofs_y - ReadWord68k(&RAM_BG[zz+6]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) y -= rel_y;
            else y -= (rel_y2+rel_y);
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
         y  = (y-zy)&OBJ_XY_MASK;
      }
      else{
         if((td&0x20)!=0){
            zy = zoom_dat_y[ty++];
            y  = (y-zy)&OBJ_XY_MASK;
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=( ReadWord68k(&RAM_BG[zz]) | ((RAM_BG[zz+11]&1)<<16) ) & tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb|0x100,
            cols,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x03: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x00: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}

void render_tc0200obj_mapped_f3system_r270_b(void)
{
   int x,y,ta,tb,zz,cols;
   int xn,yn,td,zx,zy,tx,ty;
   int rel_x,rel_y;
   int rel_x2,rel_y2;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   int sx_1,sy_1,sx_2,sy_2;
   int ofs_x,ofs_y;
   UINT32 tile_mask;
   mapper_direct *pal_map;

   if(!check_layer_enabled(object_id))
       return;

   RAM_BG    = tc0200obj.RAM;
   RAM_GFX   = tc0200obj.GFX;
   RAM_MSK   = tc0200obj.MASK;
   pal_map   = tc0200obj.mapper;
   tile_mask = tc0200obj.tile_mask;
   sx_1      = tc0200obj.bmp_x - 16;
   sy_1      = tc0200obj.bmp_y - 16;
   sx_2      = tc0200obj.bmp_x + tc0200obj.bmp_w;
   sy_2      = tc0200obj.bmp_y + tc0200obj.bmp_h;
   ofs_x     = tc0200obj.bmp_x + tc0200obj.ofs_x;
   ofs_y     =(tc0200obj.bmp_y + tc0200obj.bmp_h) - tc0200obj.ofs_y;

   cols      = tc0200obj.cols;

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;


   if(((ReadWord68k(&RAM_BG[0x0A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }
   if(((ReadWord68k(&RAM_BG[0x2A])&0x0001)!=0) && (tc0200obj.RAM_B != NULL)){
      RAM_BG = tc0200obj.RAM_B;
   }

   rel_x=ReadWord68k(&RAM_BG[0x16]);
   rel_y=ReadWord68k(&RAM_BG[0x14]);

   rel_x2=ReadWord68k(&RAM_BG[0x26]);
   rel_y2=ReadWord68k(&RAM_BG[0x24]);

   tb = 0;
   zoom_dat_x = zoom16_ofs;
   zoom_dat_y = zoom16_ofs;
   tx = 0;
   ty = 0;
   zx = zoom_dat_x[0];
   zy = zoom_dat_y[0];

   for(zz=0x30;zz<0x4000;zz+=16){

      if(RAM_BG[zz+12]!=0) return;

      td=RAM_BG[zz+8];

      if((td&0x04)==0){
         tb = RAM_BG[zz+9];
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
            x = ofs_x + ReadWord68k(&RAM_BG[zz+6]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) x += rel_x;
            else x += rel_x2;
            x &= OBJ_XY_MASK;
            xn = x;

            zoom_dat_x = zoom16_ofs+(RAM_BG[zz+2]<<5);
         }
         else{
            x = xn;
         }
         tx = 0;
         zx = zoom_dat_x[tx++];
      }
      else{
         if((td&0x20)!=0){
            x  = (x+zx)&OBJ_XY_MASK;
            zx = zoom_dat_x[tx++];
         }
      }

      if((td&0x40)==0){
         if((td&0x04)==0){
            y = ofs_y - ReadWord68k(&RAM_BG[zz+4]);
            if((ReadWord68k(&RAM_BG[zz+4])&0x4000)!=0) y -= rel_y;
            else y -= rel_y2;
            y &= OBJ_XY_MASK;
            yn = y;

            zoom_dat_y = zoom16_ofs+(RAM_BG[zz+3]<<5);
         }
         else{
            y = yn;
         }
         ty = 0;
         zy = zoom_dat_y[ty++];
         y  = (y-zy)&OBJ_XY_MASK;
      }
      else{
         if((td&0x80)!=0){
            zy = zoom_dat_y[ty++];
            y  = (y-zy)&OBJ_XY_MASK;
         }
      }

      if((x > sx_1) && (x < sx_2) && (y > sy_1) && (y < sy_2)){

      ta=ReadWord68k(&RAM_BG[zz])&tile_mask;
      if(RAM_MSK[ta]!=0){                        // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb|0x100,
            cols,
            map
         );

         if(RAM_MSK[ta]==1){                        // Some pixels; trans
            switch(td&0x03){
               case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }
         else{                                        // all pixels; solid
            switch(td&0x03){
               case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);        break;
               case 0x02: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x01: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy);  break;
               case 0x03: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&RAM_GFX[ta<<8],x,y,map,zx,zy); break;
            }
         }

      }

      }

   }

}
