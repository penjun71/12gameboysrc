/******************************************************************************/
/*                                                                            */
/*                               RAINE GAME LISTS                             */
/*                                                                            */
/******************************************************************************/

#include <string.h>
#include "raine.h"
#include "games.h"

char *company_name[] =
{
   "Unknown",
   "Bootleg",
   "Capcom",
   "Data East",
   "Irem",
   "Jaleco",
   "Konami",
   "Nichibutsu",
   "Sega",
   "SNK",
   "Taito",
   "Tecmo",
   "Toaplan",
   "Banpresto",
   "UPL",
   "Namco",
   "NTC",
   "Visco",
   "Face",
   "Seibu",
   "Sammy",
   "Video System",
   "Alpha Denshi",
   "Nazca",
   "Technos",
   "NMK",
   "Sunsoft",
   "Aicom",
   "Yumekobo",
   "Hudson Soft",
   "Tad",
   "Comad",
   "Seta",
   "Excellent System",
   "Kaneko",
   "Raizing",
   "Cave",
   "Zeus",
   "Gaelco",
   "Psikyo",
   "East Technology",
   "Williams",
   "Hot-B",
   "Marble",
};

const int nb_companies = sizeof(company_name)/4;

#include "driver.c"

void init_game_list(void)
{
   GAME_MAIN *swap;
   int i,j;

   /*

   count the games

   */

   game_count = sizeof(game_list) / sizeof(GAME_MAIN *);

   /*

   sort game_list by long_name (bubble sort)

   */

   for(i = game_count-1; i > 0; i --){

      for(j = 1; j <= i; j ++){

         if( stricmp(game_list[j-1]->long_name, game_list[j]->long_name) > 0){

            swap           = game_list[j-1];
            game_list[j-1] = game_list[j];
            game_list[j]   = swap;

         }

      }

   }

   /*

   no game is loaded

   */

   current_game = NULL;

}

char *game_company_name(UINT8 company_id)
{
   return company_name[company_id];
}

int is_current_game(UINT8 *main_name)
{
   return (!stricmp(current_game->main_name, main_name));
}

