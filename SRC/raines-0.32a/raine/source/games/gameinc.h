// Includes for game drivers
// -------------------------

#include "raine.h"		// General Raine Stuff
#include "u020help.h"		// M68020 support interface
#include "starhelp.h"		// M68000 support interface
#include "mz80help.h"		// Z80 support interface
#ifdef HAVE_6502
#include "m6502hlp.h"		// M6502/M6510 support interface
#endif
#include "files.h"		// file access routines
#include "newspr.h"		// sprite drawing routines
#include "spr64.h"		// 64x64 sprite routines
#include "spz16x16.h"		// 16x16 zoomed sprite routines
#include "spz16x8.h"		// 16x8 zoomed sprite routines
#include "tilemod.h"		// sprite flipping/rotating/masking
#include "loadroms.h"		// sprite flipping/rotating/masking
#include "newmem.h"		// memory handling routines
#include "../control.h"		// control/input handling routines (../ is due to conflict with another control.h in direct-x)
#include "dsw.h"		// dipswitch handling routines
#include "ingame.h"		// screen handling routines
#include "palette.h"		// colour mapping routines
#include "scroll.h"		// scroll routines
#include "games.h"		// game struct info
#include "cpumain.h"		// generic cpu routines

