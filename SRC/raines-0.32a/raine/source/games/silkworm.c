/***************************************************************************/
/*                                                                         */
/*                          SILKWORM (C) 1988 TECMO                        */
/*                                                                         */
/*                    Z80 + Z80 + YM3812 + MSM5205(not yet emulated)       */
/*                                                                         */
/***************************************************************************/

/*
	Converted to Raine from Mame sourcecode.
	Original Mame driver written by: Nicola Salmoria


	* The MSM5205 chip is not yet emulated, so no ADPCM samples are heard.
	  (ADPCM samples are used for the drums in background music).

*/


#include "gameinc.h"
#include "silkworm.h"
#include "taitosnd.h"
#include "msm5205.h"
#include "sasound.h"		// sample support routines

static int sound_command;
static int bank_offs;
static int nmi_trigger;

/******************
   SILKWORM SET 1
 ******************/

static struct DIR_INFO silkworm_dirs[] =
{
   { "silkworm", },
   { "silkworm", },
   { NULL, },
};

static struct ROM_INFO silkworm_roms[] =
{
   {   "silkworm.4", 0x10000, 0xa5277cce, 0, 0, 0, },	// cpu1
   {   "silkworm.5", 0x10000, 0xa6c7bb51, 0, 0, 0, }, // cpu1
   {   "silkworm.3", 0x08000, 0xb589f587, 0, 0, 0, }, // cpu2
   {   "silkworm.2", 0x08000, 0xe80a1cd9, 0, 0, 0, }, // char
   {   "silkworm.6", 0x10000, 0x1138d159, 0, 0, 0, }, // spr
   {   "silkworm.7", 0x10000, 0xd96214f7, 0, 0, 0, }, // spr
   {   "silkworm.8", 0x10000, 0x0494b38e, 0, 0, 0, }, // spr
   {   "silkworm.9", 0x10000, 0x8ce3cdf5, 0, 0, 0, }, // spr
   {   "silkworm.10",0x10000, 0x8c7138bb, 0, 0, 0, }, // tile1
   {   "silkworm.11",0x10000, 0x6c03c476, 0, 0, 0, }, // tile1
   {   "silkworm.12",0x10000, 0xbb0f568f, 0, 0, 0, }, // tile1
   {   "silkworm.13",0x10000, 0x773ad0a4, 0, 0, 0, }, // tile1
   {   "silkworm.14",0x10000, 0x409df64b, 0, 0, 0, }, // tile2
   {   "silkworm.15",0x10000, 0x6e4052c9, 0, 0, 0, }, // tile2
   {   "silkworm.16",0x10000, 0x9292ed63, 0, 0, 0, }, // tile2
   {   "silkworm.17",0x10000, 0x3fa4563d, 0, 0, 0, }, // tile2
   {   "silkworm.1", 0x08000, 0x5b553644, 0, 0, 0, }, // adpcm
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO silkworm_inputs[] =
{
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x030000, 0x01, BIT_ACTIVE_1 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x030000, 0x02, BIT_ACTIVE_1 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x030000, 0x04, BIT_ACTIVE_1 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x030000, 0x08, BIT_ACTIVE_1 },

   { KB_DEF_P1_B1,        MSG_P1_B1,               0x030001, 0x01, BIT_ACTIVE_1 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x030001, 0x02, BIT_ACTIVE_1 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x030001, 0x04, BIT_ACTIVE_1 },

   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x030002, 0x01, BIT_ACTIVE_1 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x030002, 0x02, BIT_ACTIVE_1 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x030002, 0x04, BIT_ACTIVE_1 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x030002, 0x08, BIT_ACTIVE_1 },

   { KB_DEF_P2_B1,        MSG_P2_B1,               0x030003, 0x01, BIT_ACTIVE_1 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x030003, 0x02, BIT_ACTIVE_1 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x030003, 0x04, BIT_ACTIVE_1 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x03000F, 0x01, BIT_ACTIVE_1 },
   { KB_DEF_P2_START,     MSG_P2_START,            0x03000F, 0x02, BIT_ACTIVE_1 },
   { KB_DEF_COIN1,        MSG_COIN1,               0x03000F, 0x04, BIT_ACTIVE_1 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03000F, 0x08, BIT_ACTIVE_1 },
   { 0,                   NULL,                    0,        0,    0            },
};

/* Dipswitch B */
static struct DSW_DATA dsw_data_silkworm_B[] =
{
   { MSG_EXTRA_LIFE,          0x07, 0x07 },
   { "50k, 200k and 500k",    0x00, 0x00 },
   { "100k, 300k and 800k",   0x01, 0x00 },
   { "50k and 200k",          0x02, 0x00 },
   { "100k and 300k",         0x03, 0x00 },
   { "50k only",              0x04, 0x00 },
   { "100k only",             0x05, 0x00 },
   { "200k only",             0x06, 0x00 },
   { "None",                  0x07, 0x00 },
   { MSG_DSWB_BIT3,           0x08, 0x02 },
   { MSG_OFF,                 0x08, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DIFFICULTY,          0x70, 0x06 },
   { "0",                     0x00, 0x00 },
   { "1",                     0x10, 0x00 },
   { "2",                     0x20, 0x00 },
   { "3",                     0x30, 0x00 },
   { "4",                     0x40, 0x00 },
   { "5",                     0x50, 0x00 },
   /* 0x06 and 0x07 are the same as 0x00 */
   { MSG_CONTINUE_PLAY,       0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

/* Dipswitch A */
static struct DSW_DATA dsw_data_silkworm_A[] =
{
   { MSG_COIN1,               0x03, 0x04 }, 
   { MSG_2COIN_1PLAY,         0x01, 0x00 },
   { MSG_1COIN_1PLAY,         0x00, 0x00 },
   { MSG_1COIN_2PLAY,         0x02, 0x00 },
   { MSG_1COIN_3PLAY,         0x03, 0x00 },
   { MSG_COIN2,               0x0C, 0x04 }, 
   { MSG_2COIN_1PLAY,         0x04, 0x00 },
   { MSG_1COIN_1PLAY,         0x00, 0x00 },
   { MSG_1COIN_2PLAY,         0x08, 0x00 },
   { MSG_1COIN_3PLAY,         0x0C, 0x00 },
   { MSG_LIVES,               0x30, 0x04 },
   { "2",                     0x30, 0x00 },
   { "3",                     0x00, 0x00 },
   { "4",                     0x10, 0x00 },
   { "5",                     0x20, 0x00 },
   { MSG_DSWA_BIT6,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x80, 0x02 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_ON,                  0x80, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO silkworm_dsw[] =
{
   { 0x030006, 0x80, dsw_data_silkworm_A },
   { 0x030008, 0x00, dsw_data_silkworm_B },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO silkworm_video =
{
   DrawSilkworm,
   256,
   256,
   32,
   VIDEO_ROTATE_NORMAL,
};

static struct YM3812interface ym3812_interface =
{
   1,              // 1 chip
   4000000,        // 4.0 MHz
   { 255 },        // Volume (emu only)
   { NULL }
};

static struct SOUND_INFO silkworm_sound[] =
{
   { SOUND_YM3812,  &ym3812_interface,    },
   { 0,             NULL,                 },
};

struct GAME_MAIN game_silkworm =
{
   silkworm_dirs,
   silkworm_roms,
   silkworm_inputs,
   silkworm_dsw,
   NULL,

   LoadSilkworm,
   ClearSilkworm,
   &silkworm_video,
   ExecuteSilkwormFrame,
   "silkworm",
   "Silkworm (Set 1)",
   NULL,
   COMPANY_ID_TECMO,
   "6217",
   1988,
   silkworm_sound,
   GAME_SHOOT,
};

/******************
   SILKWORM SET 2
 ******************/

static struct DIR_INFO silkworm_set2_dirs[] =
{
   { "silkworm_set2", },
   { "silkwrm2", },
   { ROMOF("silkworm"), },
   { CLONEOF("silkworm"), },
   { NULL, },
};

static struct ROM_INFO silkworm_set2_roms[] =
{
   {           "r4", 0x10000, 0x6df3df22, 0, 0, 0, },   // cpu1
   {   "silkworm.5", 0x10000, 0xa6c7bb51, 0, 0, 0, }, // cpu1
   {           "r3", 0x08000, 0xb79848d0, 0, 0, 0, }, // cpu2
   {   "silkworm.2", 0x08000, 0xe80a1cd9, 0, 0, 0, }, // char
   {   "silkworm.6", 0x10000, 0x1138d159, 0, 0, 0, }, // spr
   {   "silkworm.7", 0x10000, 0xd96214f7, 0, 0, 0, }, // spr
   {   "silkworm.8", 0x10000, 0x0494b38e, 0, 0, 0, }, // spr
   {   "silkworm.9", 0x10000, 0x8ce3cdf5, 0, 0, 0, }, // spr
   {   "silkworm.10",0x10000, 0x8c7138bb, 0, 0, 0, }, // tile1
   {   "silkworm.11",0x10000, 0x6c03c476, 0, 0, 0, }, // tile1
   {   "silkworm.12",0x10000, 0xbb0f568f, 0, 0, 0, }, // tile1
   {   "silkworm.13",0x10000, 0x773ad0a4, 0, 0, 0, }, // tile1
   {   "silkworm.14",0x10000, 0x409df64b, 0, 0, 0, }, // tile2
   {   "silkworm.15",0x10000, 0x6e4052c9, 0, 0, 0, }, // tile2
   {   "silkworm.16",0x10000, 0x9292ed63, 0, 0, 0, }, // tile2
   {   "silkworm.17",0x10000, 0x3fa4563d, 0, 0, 0, }, // tile2
   {   "silkworm.1", 0x08000, 0x5b553644, 0, 0, 0, }, // adpcm
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_silkworm_set2 =
{
   silkworm_set2_dirs,
   silkworm_set2_roms,
   silkworm_inputs,
   silkworm_dsw,
   NULL,

   LoadSilkworm,
   ClearSilkworm,
   &silkworm_video,
   ExecuteSilkwormFrame,
   "silkwrm2",
   "Silkworm (Set 2)",
   NULL,
   COMPANY_ID_TECMO,
   "6217",
   1988,
   silkworm_sound,
   GAME_SHOOT,
};

/**********************************************************************/
/**********************************************************************/


static UINT8 *TMP;
static UINT8 *RAM1;
static UINT8 *RAM2;
static UINT8 *ROM1;
static UINT8 *ROM2;
static UINT8 *RAM_INPUT;

static UINT8 *RAM_VIDEO_BG;
static UINT8 *RAM_COLOR_BG;
static UINT8 *RAM_VIDEO_FG;
static UINT8 *RAM_COLOR_FG;
static UINT8 *RAM_VIDEO_TX;
static UINT8 *RAM_COLOR_TX;
static UINT8 *RAM_VIDEO_SPR;
static UINT8 *RAM_PALETTE;

static UINT8 *GFX_CHAR;
static UINT8 *GFX_CHAR_SOLID;
static UINT8 *GFX_TILE;
static UINT8 *GFX_TILE_SOLID;
static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static int   flip_screen;
static int	 rom_bank;

/************************************************************************/
/*	CPU1 write ports									*/
/************************************************************************/
void sw_sound_command(UINT32 address, UINT8 data){
	(void)(data);
	sound_command = data;
	if(RaineSoundCard){
		nmi_trigger = 1;
//		cpu_int_nmi(CPU_Z80_1);
//		print_ingame(120, "CPU1 Writes: %i", sound_command);
	}
}
void sw_flip_screen(UINT32 address, UINT8 data){
//	print_ingame(120, "Flip screen: %i", data);
	flip_screen = data;
}
void sw_bankswitch(UINT32 address, UINT8 data){
	bank_offs = 0x1000 + ( (data&0xF8) << 8);
}

UINT8 sw_read_bank(UINT32 address){
	return ROM1[bank_offs + address];
}

void sw_palette_write(UINT32 address, UINT8 data){
	// write to real palette (not used)
	RAM1[address] = data;

	// write to fake palette
	if( (address&0x01)==0)
		RAM1[address+0x30000] = ((data & 0x0F) << 4);
	else
		RAM1[address+0x30000] = data;

}


/************************************************************************/
/*	CPU1 read ports									*/
/************************************************************************/
UINT8 sw_dsw_a_lo(UINT32 address){
	return ((RAM_INPUT[0x06] & 0x0F) + 0xF0);
}
UINT8 sw_dsw_a_hi(UINT32 address){
	return (((RAM_INPUT[0x06]& 0xF0)>>4) + 0xF0);
}
UINT8 sw_dsw_b_lo(UINT32 address){
	return ((RAM_INPUT[0x08] & 0x0F) + 0xF0);
}
UINT8 sw_dsw_b_hi(UINT32 address){
	return (((RAM_INPUT[0x08] & 0xF0)>>4) + 0xF0);
}


/************************************************************************/
/*	CPU2 write ports									*/
/************************************************************************/

void sw_ym3812_control_port(UINT32 address, UINT8 data){
	if(RaineSoundCard){
		YM3812_control_port_0_w(address, data);
//		print_ingame(120, "YM3812 control: %i", data);
	}
}
void sw_ym3812_write_port(UINT32 address, UINT8 data){
	if(RaineSoundCard){
		YM3812_write_port_0_w(address, data);
//		print_ingame(120, "YM3812 write: %i", data);
	}
}
void sw_adpcm_start(UINT32 address, UINT8 data){
	if(RaineSoundCard){
//		print_ingame(120, "ADPCM Start: %i", data);
	}
}
void sw_adpcm_end(UINT32 address, UINT8 data){
	if(RaineSoundCard){
//		print_ingame(120, "ADPCM End: %i", data);
	}
}
void sw_adpcm_volume(UINT32 address, UINT8 data){
	if(RaineSoundCard){
//		print_ingame(120, "ADPCM Volume: %i", data);
	}
}


/************************************************************************/
/*	CPU2 read ports									*/
/************************************************************************/

UINT8 sw_soundlatch(UINT32 address){
	int ta = 0x00;
	if(RaineSoundCard)
		ta = sound_command;
	nmi_trigger = 0;
	sound_command = 0;
//	print_ingame(120, "CPU2 Reads: %i", sound_command);
	return ta;
}



/************************************************************************/
/*	GFX LOADING										*/
/************************************************************************/

void DecodeSilkwormGfx_8x8(char *file, int gfx_offs, int gfx_size){
	int ta,tb;

	if(!(load_rom(file, TMP, gfx_size))){
		print_ingame(120, "ERROR: Unable to load gfx rom: %s", file);
	}else{
		tb = gfx_offs * 0x20000;
		for(ta=0; ta<gfx_size; ta++){
			GFX[tb+1] = TMP[ta] & 0x0F;
			GFX[tb+0] = TMP[ta] >> 4;
			tb += 2;
		}
	}

}


void DecodeSilkwormGfx_16x16(char *file, UINT8 gfx_offs, UINT32 gfx_size){
	int ta,tb;
	int i,j;

	if(!(load_rom(file, TMP, gfx_size))){
		print_ingame(120, "ERROR: Unable to load gfx rom: %s", file);
	}else{
		tb = gfx_offs * 0x20000;
		ta = 0;
		for(i=0; i<(gfx_size/128); i++){
			for(j=0; j<8; j++){
				GFX[tb+0]  = (TMP[ta+0] >> 4) & 0x0F;
				GFX[tb+1]  = (TMP[ta+0] >> 0) & 0x0F;
				GFX[tb+2]  = (TMP[ta+1] >> 4) & 0x0F;
				GFX[tb+3]  = (TMP[ta+1] >> 0) & 0x0F;
				GFX[tb+4]  = (TMP[ta+2] >> 4) & 0x0F;
				GFX[tb+5]  = (TMP[ta+2] >> 0) & 0x0F;
				GFX[tb+6]  = (TMP[ta+3] >> 4) & 0x0F;
				GFX[tb+7]  = (TMP[ta+3] >> 0) & 0x0F;
				GFX[tb+8]  = (TMP[ta+0+32] >> 4) & 0x0F;
				GFX[tb+9]  = (TMP[ta+0+32] >> 0) & 0x0F;
				GFX[tb+10] = (TMP[ta+1+32] >> 4) & 0x0F;
				GFX[tb+11] = (TMP[ta+1+32] >> 0) & 0x0F;
				GFX[tb+12] = (TMP[ta+2+32] >> 4) & 0x0F;
				GFX[tb+13] = (TMP[ta+2+32] >> 0) & 0x0F;
				GFX[tb+14] = (TMP[ta+3+32] >> 4) & 0x0F;
				GFX[tb+15] = (TMP[ta+3+32] >> 0) & 0x0F;
				tb += 16;
				ta += 4;
			}
			ta += 32;
			for(j=0; j<8; j++){
				GFX[tb+0]  = (TMP[ta+0] >> 4) & 0x0F;
				GFX[tb+1]  = (TMP[ta+0] >> 0) & 0x0F;
				GFX[tb+2]  = (TMP[ta+1] >> 4) & 0x0F;
				GFX[tb+3]  = (TMP[ta+1] >> 0) & 0x0F;
				GFX[tb+4]  = (TMP[ta+2] >> 4) & 0x0F;
				GFX[tb+5]  = (TMP[ta+2] >> 0) & 0x0F;
				GFX[tb+6]  = (TMP[ta+3] >> 4) & 0x0F;
				GFX[tb+7]  = (TMP[ta+3] >> 0) & 0x0F;
				GFX[tb+8]  = (TMP[ta+0+32] >> 4) & 0x0F;
				GFX[tb+9]  = (TMP[ta+0+32] >> 0) & 0x0F;
				GFX[tb+10] = (TMP[ta+1+32] >> 4) & 0x0F;
				GFX[tb+11] = (TMP[ta+1+32] >> 0) & 0x0F;
				GFX[tb+12] = (TMP[ta+2+32] >> 4) & 0x0F;
				GFX[tb+13] = (TMP[ta+2+32] >> 0) & 0x0F;
				GFX[tb+14] = (TMP[ta+3+32] >> 4) & 0x0F;
				GFX[tb+15] = (TMP[ta+3+32] >> 0) & 0x0F;
				tb += 16;
				ta += 4;
			}
			ta += 32;
		}
	}
}


/************************************************************************/
/*	GAME LOAD										*/
/************************************************************************/

void LoadSilkworm(void)
{
   RAMSize=0x41000;

   if(!(ROM=AllocateMem(0x20000+0x10000)))	return;
   if(!(RAM=AllocateMem(RAMSize)))			return;
   if(!(GFX=AllocateMem(0x1A0000)))			return; 
   if(!(TMP=AllocateMem(0x10000))) 			return; 

   ROM1 = ROM+0x00000;	// Cpu1
   ROM2 = ROM+0x20000;  // Cpu2 (sound)

   RAM1	  = RAM+0x00000; // Cpu1
   RAM2       = RAM+0x20000; // Cpu2
   RAM_INPUT  = RAM+0x30000; // Cpu1 input
   RAM_PALETTE= RAM+0x3E800; // Fake palette ram

   // CPU1
   if(!load_rom_index(0,ROM1+0x00000,0x10000)) return;
   if(!load_rom("silkworm.5",ROM1+0x10000,0x10000)) return;

   memset(RAM, 0x00, RAMSize);

   memcpy(RAM1, ROM1, 0x10000);			// load static rom
   memcpy(RAM1+0xF000, ROM1+0x10000, 0x0800);	// load first rom bank
   bank_offs = 0x1000;
   rom_bank = 0;
   flip_screen = 0;

   /* SPEED HACK */
   RAM1[0x05da] = 0xD3;		// OUTA (AAh)
   RAM1[0x05db] = 0xAA;
   RAM1[0x05dc] = 0x00;		// NOP
   RAM1[0x05dd] = 0x00;		// NOP
   SetStopZ80Mode2(0x05dc);

   AddZ80AROMBase(RAM1, 0x0038, 0x0066);

   AddZ80AReadByte(0x0000, 0xBFFF, NULL,				RAM1+0x0000);	// rom
   AddZ80AReadByte(0xC000, 0xC1FF, NULL,				RAM1+0xC000);	// bg video
   AddZ80AReadByte(0xC200, 0xC3FF, NULL,				RAM1+0xC200);	// bg color
   AddZ80AReadByte(0xC400, 0xC5FF, NULL,				RAM1+0xC400);	// fg video
   AddZ80AReadByte(0xC600, 0xC7FF, NULL,				RAM1+0xC600);	// fg color
   AddZ80AReadByte(0xC800, 0xCBFF, NULL,				RAM1+0xC800);	// tx video
   AddZ80AReadByte(0xCC00, 0xCFFF, NULL,				RAM1+0xCC00);	// tx color
   AddZ80AReadByte(0xD000, 0xDFFF, NULL,				RAM1+0xD000);	// Work  RAM
   AddZ80AReadByte(0xE000, 0xE7FF, NULL,				RAM1+0xE000);	// Sprites
   AddZ80AReadByte(0xE800, 0xEFFF, NULL,				RAM1+0xE800);	// Palette RAM
//   AddZ80AReadByte(0xF000, 0xF7FF, NULL,				RAM1+0xF000);	// Banked ROM
   AddZ80AReadByte(0xF000, 0xF7FF, sw_read_bank,		NULL);		// Banked ROM

   AddZ80AReadByte(0xF800, 0xF800, NULL,				RAM_INPUT+0x00);	// input_1_a
   AddZ80AReadByte(0xF801, 0xF801, NULL,				RAM_INPUT+0x01);	// input_1_b
   AddZ80AReadByte(0xF802, 0xF802, NULL,				RAM_INPUT+0x02);	// input_2_a
   AddZ80AReadByte(0xF803, 0xF803, NULL,				RAM_INPUT+0x03);	// input_2_b
   AddZ80AReadByte(0xF806, 0xF806, sw_dsw_a_lo,			NULL);		// dsw 1_a
   AddZ80AReadByte(0xF807, 0xF807, sw_dsw_a_hi,			NULL);		// dsw 1_b
   AddZ80AReadByte(0xF808, 0xF808, sw_dsw_b_lo,			NULL);		// dsw 2_a
   AddZ80AReadByte(0xF809, 0xF809, sw_dsw_b_hi,			NULL);		// dsw 2_b
   AddZ80AReadByte(0xF80F, 0xF80F, NULL,				RAM_INPUT+0x0F);	// coin
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,		NULL);		// <bad reads>
   AddZ80AReadByte(-1, -1, NULL, NULL);


   AddZ80AWriteByte(0xC000, 0xC1FF, NULL,				RAM1+0xC000);	// bg video
   AddZ80AWriteByte(0xC200, 0xC3FF, NULL,				RAM1+0xC200);	// bg color
   AddZ80AWriteByte(0xC400, 0xC5FF, NULL,				RAM1+0xC400);	// fg video
   AddZ80AWriteByte(0xC600, 0xC7FF, NULL,				RAM1+0xC600);	// fg color
   AddZ80AWriteByte(0xC800, 0xCBFF, NULL,				RAM1+0xC800);	// tx video
   AddZ80AWriteByte(0xCC00, 0xCFFF, NULL,				RAM1+0xCC00);	// tx color
   AddZ80AWriteByte(0xD000, 0xDFFF, NULL,				RAM1+0xD000);	// Work  RAM
   AddZ80AWriteByte(0xE000, 0xE7FF, NULL,				RAM1+0xE000);	// Sprites
   AddZ80AWriteByte(0xE800, 0xEFFF, sw_palette_write,		NULL);		// Palette RAM

   AddZ80AWriteByte(0xF800, 0xF801, NULL,				RAM1+0xF800);	// bg scroll-x 1
   AddZ80AWriteByte(0xF802, 0xF802, NULL,				RAM1+0xF802);	// bg scroll-y 1
   AddZ80AWriteByte(0xF803, 0xF804, NULL,				RAM1+0xF803);	// bg scroll-x 2
   AddZ80AWriteByte(0xF805, 0xF805, NULL,				RAM1+0xF805);	// bg scroll-y 2
   AddZ80AWriteByte(0xF806, 0xF806, sw_sound_command,		NULL);	// Sound command
   AddZ80AWriteByte(0xF807, 0xF807, sw_flip_screen,		NULL);	// Screen flip
   AddZ80AWriteByte(0xF808, 0xF808, sw_bankswitch,		NULL);	// Bank switch
   AddZ80AWriteByte(0xF809, 0xF809, NULL,				NULL);	// ???
   AddZ80AWriteByte(0xF80B, 0xF80B, NULL,				NULL);	// ???
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,		NULL);	// <bad writes>
   AddZ80AWriteByte(-1, -1, NULL, NULL);

   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,			NULL);	// <bad reads>
   AddZ80AReadPort(-1, -1, NULL, NULL);

   AddZ80AWritePort(0xAA, 0xAA, StopZ80Mode2,			NULL);
   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,			NULL);	// <bad reads>
   AddZ80AWritePort(-1, -1, NULL, NULL);



   // CPU2
   if(!load_rom_index(2,ROM2,0x08000)) return;

   memcpy(RAM2, ROM2, 0x08000);

   /* SPEED HACK */
   RAM2[0x00b4] = 0xD3;		// OUTA (AAh)
   RAM2[0x00b5] = 0xAA;
   SetStopZ80BMode2(0x00b4);

   AddZ80BROMBase(RAM2, 0x0038, 0x0066);

   AddZ80BReadByte(0xC000, 0xC000, sw_soundlatch,		NULL);		// soundlatch
   AddZ80BReadByte(0x0000, 0x7FFF, NULL,				RAM2+0x0000);	// ROM
   AddZ80BReadByte(0x8000, 0x87FF, NULL,				RAM2+0x8000);	// RAM
   AddZ80BReadByte(0x0000, 0xFFFF, DefBadReadZ80,		NULL);		// <bad reads>
   AddZ80BReadByte(-1, -1, NULL, NULL);

   AddZ80BWriteByte(0x0000, 0x7FFF, NULL,				NULL);		// ROM
   AddZ80BWriteByte(0x8000, 0x87FF, NULL,				RAM2+0x8000);	// RAM
   AddZ80BWriteByte(0xA000, 0xA000, sw_ym3812_control_port,	NULL);		// sound control port
   AddZ80BWriteByte(0xA001, 0xA001, sw_ym3812_write_port,	NULL);		// sound write port
   AddZ80BWriteByte(0xC000, 0xC000, sw_adpcm_start,		NULL);		// sample start
   AddZ80BWriteByte(0xC400, 0xC400, sw_adpcm_end,		NULL);		// sample end
   AddZ80BWriteByte(0xC800, 0xC800, sw_adpcm_volume,		NULL);		// sample volume
   AddZ80BWriteByte(0xCC00, 0xCC00, NULL,				NULL);		// NMI ack.
   AddZ80BWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,		NULL);	// <bad writes>
   AddZ80BWriteByte(-1, -1, NULL, NULL);

   AddZ80BReadPort(0x00, 0xFF, DefBadReadZ80,			NULL);	// <bad reads>
   AddZ80BReadPort(-1, -1, NULL, NULL);

   AddZ80BWritePort(0xAA, 0xAA, StopZ80BMode2,			NULL);
   AddZ80BWritePort(0x00, 0xFF, DefBadWriteZ80,			NULL);	// <bad reads>
   AddZ80BWritePort(-1, -1, NULL, NULL);


   // ADPCM SAMPLES
  // if(!load_rom("silkworm.1", ADPCM, 0x08000)) return;
   
   


   // GFX
   GFX_CHAR  = GFX + (0x20000 * 0);
   GFX_SPR   = GFX + (0x20000 * 1);
   GFX_TILE  = GFX + (0x20000 * 5);

   RAM_VIDEO_BG = RAM1+0xC000;
   RAM_COLOR_BG = RAM1+0xC200;
   RAM_VIDEO_FG = RAM1+0xC400;
   RAM_COLOR_FG = RAM1+0xC600;
   RAM_VIDEO_TX = RAM1+0xC800;
   RAM_COLOR_TX = RAM1+0xCC00;
   RAM_VIDEO_SPR= RAM1+0xE000;

   memset(GFX, 0x00, 0x1A0000);

   DecodeSilkwormGfx_8x8("silkworm.2", 0, 0x08000);		// characters

   DecodeSilkwormGfx_8x8("silkworm.6", 1, 0x10000);		// sprites
   DecodeSilkwormGfx_8x8("silkworm.7", 2, 0x10000);		// sprites
   DecodeSilkwormGfx_8x8("silkworm.8", 3, 0x10000);		// sprites
   DecodeSilkwormGfx_8x8("silkworm.9", 4, 0x10000);		// sprites

   DecodeSilkwormGfx_16x16("silkworm.10", 5, 0x10000);	// tiles 1
   DecodeSilkwormGfx_16x16("silkworm.11", 6, 0x10000);	// tiles 1
   DecodeSilkwormGfx_16x16("silkworm.12", 7, 0x10000);	// tiles 1
   DecodeSilkwormGfx_16x16("silkworm.13", 8, 0x10000);	// tiles 1

   DecodeSilkwormGfx_16x16("silkworm.14", 9, 0x10000);	// tiles 2
   DecodeSilkwormGfx_16x16("silkworm.15", 10, 0x10000);	// tiles 2
   DecodeSilkwormGfx_16x16("silkworm.16", 11, 0x10000);	// tiles 2
   DecodeSilkwormGfx_16x16("silkworm.17", 12, 0x10000);	// tiles 2

   // Setup palette
   InitPaletteMap(RAM_PALETTE, 0x40, 0x10, 0x8000);
   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);

   // make solid mask
   GFX_CHAR_SOLID = make_solid_mask_8x8(   GFX_CHAR, 0x08000 / 32  );
   GFX_SPR_SOLID  = make_solid_mask_8x8(   GFX_SPR,  0x40000 / 32 );
   GFX_TILE_SOLID = make_solid_mask_16x16( GFX_TILE, 0x80000 / 128 );

   FreeMem(TMP);

   AddZ80AInit();
   AddZ80BInit();
}



/************************************************************************/
/*	GAME CLEAR										*/
/************************************************************************/
void ClearSilkworm(void)
{
}


/************************************************************************/
/*	GAME EXECUTE									*/
/************************************************************************/
void ExecuteSilkwormFrame(void)
{
   cpu_execute_cycles(CPU_Z80_0, CPU_FRAME_MHz(6,60));	// Main Z80 6MHz (60fps)
   cpu_interrupt(CPU_Z80_0, 0x38);					// vbl interrupt

   cpu_execute_cycles(CPU_Z80_1, CPU_FRAME_MHz(2,60));	// Sub Z80 4MHz (60fps)
   cpu_interrupt(CPU_Z80_1, 0x38);					// Sound interrupt

   cpu_execute_cycles(CPU_Z80_1, CPU_FRAME_MHz(2,60));	// Sub Z80 4MHz (60fps)
   cpu_interrupt(CPU_Z80_1, 0x38);					// Sound interrupt

  if (nmi_trigger != 0)
	cpu_int_nmi(CPU_Z80_1);

}



/************************************************************************/
/*	SPRITE DRAW										*/
/************************************************************************/
static void silkworm_draw_sprites(int _priority){
   const UINT8 layout[8][8] =
   {
	{0,1,4,5,16,17,20,21},
	{2,3,6,7,18,19,22,23},
	{8,9,12,13,24,25,28,29},
	{10,11,14,15,26,27,30,31},
	{32,33,36,37,48,49,52,53},
	{34,35,38,39,50,51,54,55},
	{40,41,44,45,56,57,60,61},
	{42,43,46,47,58,59,62,63}
   };
   UINT8 *map;
   int offs;
   int code;
   int flags;
   int x,y;
   int sx,sy,sc;
   int xpos,ypos;
   int flip,flipx,flipy;
   int priority;
   int size;
   int bank;


   for(offs=0; offs<0x800; offs+=8){
   if(RAM[0x40000+offs]==0){

	flags		= RAM_VIDEO_SPR[offs+3];	
	bank		= RAM_VIDEO_SPR[offs+0];
	priority	= flags>>6;

	if( (priority==_priority) && (bank&0x04) ){

		RAM[0x40000+offs] = 1;

		size  = RAM_VIDEO_SPR[offs+2] & 0x03;
		code	= RAM_VIDEO_SPR[offs+1] + ((bank & 0xF8) << 5);
		code  &= ~((1 << (size<<1)) - 1);
		size  = 1 << size;

		xpos	= RAM_VIDEO_SPR[offs+5] - ((flags & 0x10) << 4);
		ypos 	= RAM_VIDEO_SPR[offs+4] - ((flags & 0x20) << 3);
		flip  = bank & 0x03;


		if(flip_screen){
			xpos 	= 256 - (size<<3) - xpos;
			ypos	= 256 - (size<<3) - ypos;
			flip = ~flip;
		}
		flipx = flip & 0x01;
		flipy = flip & 0x02;
		xpos += 32;
		ypos += 32;
		MAP_PALETTE_MAPPED_NEW(flags & 0x0F, 16, map);

		for(y=0; y<size; y++){
			for(x=0; x<size; x++){
				sx = xpos + ((flipx?(size-1-x):x)<<3);
				sy = ypos + ((flipy?(size-1-y):y)<<3);
				sc = code+layout[y][x];

			      if(GFX_SPR_SOLID[sc] && (sx>24) && (sy>14) && (sx<320) && (sy<280) ){			
			            if(GFX_SPR_SOLID[sc]==1 && _priority!=3){			// Some pixels; trans
				            switch(flip & 0x03){
				               case 0x00: Draw8x8_Trans_Mapped(&GFX_SPR[sc<<6], sx, sy, map); break;
				               case 0x01: Draw8x8_Trans_Mapped_FlipY(&GFX_SPR[sc<<6], sx, sy, map);  break;
			      	         case 0x02: Draw8x8_Trans_Mapped_FlipX(&GFX_SPR[sc<<6], sx, sy, map);  break;
			            	   case 0x03: Draw8x8_Trans_Mapped_FlipXY(&GFX_SPR[sc<<6], sx, sy, map); break;
						}
				      }else{	
					    	switch(flip & 0x03){		// all pixels; solid
				               case 0x00: Draw8x8_Mapped(&GFX_SPR[sc<<6], sx, sy, map); break;
				               case 0x01: Draw8x8_Mapped_FlipY(&GFX_SPR[sc<<6], sx, sy, map);  break;
			      	         case 0x02: Draw8x8_Mapped_FlipX(&GFX_SPR[sc<<6], sx, sy, map);  break;
			            	   case 0x03: Draw8x8_Mapped_FlipXY(&GFX_SPR[sc<<6], sx, sy, map); break;
						}
			            }
				}
			}
		}
	}
   }
   }
}



/************************************************************************/
/*	GAME DRAW										*/
/************************************************************************/
void DrawSilkworm(void)
{
   UINT8 *map;
   int color;
   int code;
   SCROLL_REGS;

   ClearPaletteMap();
   clear_game_screen(0);

   memset(RAM+0x40000, 0x00, 0x800);

   /***************************/
   /* BACKGROUND LAYER		*/
   /***************************/
   silkworm_draw_sprites(3);

   MAKE_SCROLL_512x256_4_16(
	RAM1[0xF803] + (RAM1[0xF804]<<8) + 48,
	RAM1[0xF805] - 240
   );

   START_SCROLL_512x256_4_16(32,16,256,240);

	color = RAM_COLOR_BG[zz>>2];
	code  = RAM_VIDEO_BG[zz>>2] + ((color & 0x07)<<8);
	if(GFX_TILE_SOLID[2048 + code]){
		MAP_PALETTE_MAPPED_NEW( 48+(color>>4), 16, map);
		if(GFX_TILE_SOLID[2048+code]==1)	Draw16x16_Trans_Mapped(&GFX_TILE[0x80000+(code<<8)], x, 32+y, map);
		else						Draw16x16_Mapped(&GFX_TILE[0x80000+(code<<8)], x, 32+y, map);
	}

   END_SCROLL_512x256_4_16();


   /***************************/
   /* FOREGROUND LAYER		*/
   /***************************/
   silkworm_draw_sprites(2);

   MAKE_SCROLL_512x256_4_16(
	RAM1[0xF800] + (RAM1[0xF801]<<8) + 48,
	RAM1[0xF802] - 240
   );

   START_SCROLL_512x256_4_16(32,16,256,240);

	color = RAM_COLOR_FG[zz>>2];
	code  = RAM_VIDEO_FG[zz>>2] + ((color & 0x07)<<8);
	if(GFX_TILE_SOLID[code]){
		MAP_PALETTE_MAPPED_NEW( 32+(color>>4), 16, map);
		if(GFX_TILE_SOLID[code]==1)	Draw16x16_Trans_Mapped(&GFX_TILE[code<<8], x, 32+y, map);
		else					Draw16x16_Mapped(&GFX_TILE[code<<8], x, 32+y, map);
	}

   END_SCROLL_512x256_4_16();

   /***************************/
   /* TEXT LAYER			*/
   /***************************/
   silkworm_draw_sprites(1);

   for(x=0;x!=960;x++){
	code = RAM_VIDEO_TX[x] + ((RAM_COLOR_TX[x]&0x03)<<8);
	if(GFX_CHAR_SOLID[code]){
		MAP_PALETTE_MAPPED_NEW(16 + (RAM_COLOR_TX[x]>>4), 16, map);
		if(GFX_CHAR_SOLID[code]==1)	Draw8x8_Trans_Mapped(&GFX_CHAR[code<<6], 32 + ((x<<3)&0xF8), 32 + ((x>>2)&0xF8), map);
		else					Draw8x8_Mapped(&GFX_CHAR[code<<6], 32 + ((x<<3)&0xF8), 32 + ((x>>2)&0xF8), map);
	}
   }

   silkworm_draw_sprites(0);

}
