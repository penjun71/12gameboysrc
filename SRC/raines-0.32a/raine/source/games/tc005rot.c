/******************************************************************************/
/*                                                                            */
/*                 tc0005rot: TAITO ROTATION LAYER (F2-SYSTEM)                */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "tc005rot.h"

/*

'tc0005rot' is a made up name, until someone can give me the more board info.
Or this layer could be another part of tc0100scn (but I guess it isn't).

TC0005ROT RAM
-------------

--------------+---------------
00000 - 01FFF | BG ROT RAM
02000 - 0200F | SCROLL RAM
--------------+---------------

BG ROT RAM
----------

-----+--------+---------------------
Byte | Bit(s) | Info
-----+76543210+---------------------
  0  |xx......| Colour Bank
  0  |..xxxxxx| Tile Number (high)
  1  |xxxxxxxx| Tile Number (low)
-----+--------+---------------------

SCROLL RAM
----------

-----+--------+---------------------
Byte | Bit(s) | Info
-----+76543210+---------------------
  0  |.......x| ?
  0  |......x.| ?
  2  |......xx| BG0 Scroll X (high)
  3  |xxxxxxxx| BG0 Scroll X (low)
  4  |......xx| BG1 Scroll X (high)
  5  |xxxxxxxx| BG1 Scroll X (low)
  6  |......xx| BG0 Scroll Y (high)
  7  |xxxxxxxx| BG0 Scroll Y (low)
  8  |......xx| BG1 Scroll Y (high)
  9  |xxxxxxxx| BG1 Scroll Y (low)
-----+--------+---------------------

0000.0000.000X.XXXX
XXXX.xxxx.xxxx.xxxx
SXXX.xxxx.xxxx.xxxx
SXXX.xxxx.xxxx.xxxx
0000.0000.000Y.YYYY
YYYY.yyyy.yyyy.yyyy
SYYY.yyyy.yyyy.yyyy
SYYY.yyyy.yyyy.yyyy

Not implemented:

- Various things.
- Priority with sprites.

Normal Games:

- Camel Try      - OK

R180 Games:

- Don Doko Don   - OK

R270 Games:

- Drift Out      - OK

*/

static UINT8 *RAM_BG;
static UINT8 *RAM_SCR;
static UINT8 *GFX_BG;

static UINT8 rot_pens;
static UINT8 rot_map[64];

static BITMAP *pixel_bitmap;

void init_tc0005rot(int mode)
{
   int ta;

   RAM_BG = tc0005rot.RAM;
   RAM_SCR= tc0005rot.RAM_SCR;
   GFX_BG = tc0005rot.GFX_ROT;

   rot_pens = Reserve_Pens(64);

   for(ta=0;ta<64;ta++){
      rot_map[ta] = rot_pens+ta;
   }

   if(mode){
      rot_map[0] = 255;
      rot_map[16] = 255;
      rot_map[32] = 255;
      rot_map[48] = 255;
   }

   if(pixel_bitmap) destroy_bitmap(pixel_bitmap);

   pixel_bitmap = create_bitmap_ex(8,512,512);

   clear(pixel_bitmap);
}

static BITMAP *bitmap_store;

void tc0005rot_set_bitmap(void)
{
   bitmap_store = GameBitmap;
   GameBitmap = pixel_bitmap;
   init_spr8x8asm();
}

void tc0005rot_unset_bitmap(void)
{
   GameBitmap = bitmap_store;
   init_spr8x8asm();
}

void tc0005rot_bg0_ww(UINT32 address, UINT16 data)
{
   if(ReadWord(&RAM_BG[address&0x1FFF])!=data){

   // Update RAM
   // ----------

   WriteWord(&RAM_BG[address&0x1FFF],data);

   // Update BG0 Buffer
   // -----------------

   if(RefreshBuffers==0){
      Draw8x8_Mapped(&GFX_BG[(data&0x3FFF)<<6],(address&0x7E)<<2,((address&0x1F80)>>4),&rot_map[(data>>10)&0x30]);
   }

   }
}

void tc0005rot_refresh_buffer(void)
{
   UINT32 address;
   UINT16 data;

   tc0005rot_set_bitmap();

   for(address=0;address<0x2000;address+=2){

      data = ReadWord(&RAM_BG[address]);

      Draw8x8_Mapped(&GFX_BG[(data&0x3FFF)<<6],(address&0x7E)<<2,((address&0x1F80)>>4),&rot_map[(data>>10)&0x30]);

   }

   tc0005rot_unset_bitmap();
}

static UINT32 tmp_f1x,tmp_f1y,tmp_f1xd,tmp_f1yd,tmp_f2xd,tmp_f2yd;
static UINT32 tmp_src,tmp_bit;

static void RotateF2SystemBG0(fixed f1x, fixed f1y, fixed f1xd,fixed f1yd,fixed f2xd,fixed f2yd)
{
#if !defined(RAINE_DOS) && !defined(RAINE_UNIX)
  fixed f2x,f2y;
  int dx,dy;
  UINT8 ta;
#endif   
  UINT8 *BIT;
  UINT8 *SRC;

   // Init Stuff
   // ----------

   f1x+=(16*f2xd);
   f1y+=(16*f2yd);

   f1x+=(16*f1xd);
   f1y+=(16*f1yd);

   BIT=GameViewBitmap->line[0];
   SRC=pixel_bitmap->line[0];

   // Draw the rotated data
   // ---------------------

#if !defined(RAINE_DOS) && !defined(RAINE_UNIX)

   dy=224;
   do{
      dx=320;
      f2x=f1x;
      f2y=f1y;
      do{
         ta=SRC[((f2x>>16)&0x1FF)|((f2y>>7)&0x3FE00)];
         if(ta!=255) *BIT=ta;
         f2x+=f2xd;
         f2y+=f2yd;
         BIT++;
      }while(--dx);
      BIT+=(64);
      f1x+=f1xd;
      f1y+=f1yd;
   }while(--dy);

#else

   tmp_f1x=f1x;
   tmp_f1y=f1y;
   tmp_f1xd=f1xd;
   tmp_f1yd=f1yd;
   tmp_f2xd=f2xd;
   tmp_f2yd=f2yd;
   tmp_src=(UINT32) &SRC[0];
   tmp_bit=(UINT32) &BIT[0];

   if(((tmp_f1yd>>5)==0x0800)&&((tmp_f1xd>>4)==0x0000)&&((tmp_f2yd>>5)==0x0000)&&((tmp_f2xd>>4)==0x1000)){

     tmp_f1x=(tmp_f1x>>16)&0x1FF;
     tmp_f1y=((tmp_f1y>>16)&0x1FF)<<9;
     

   if((tmp_f1x&3)==0){

#ifdef RAINE_DOS
     asm("
	pushl	%%ebp
	movl	_tmp_f1x,%%eax
	movl	_tmp_src,%%ebx
	movl	%%eax,SMC4-4
	movl	%%ebx,SMC5-4

	movl	_tmp_f1y,%%ebx
	movl	_tmp_bit,%%edi
	movb	$255,%%ch

	movl	$224,%%edx
1:
	movl	$320/4,%%ebp
	movl	$0x2164334A,%%eax
SMC4:
	movl	%%ebx,%%esi
	addl	$0x2164334A,%%esi
SMC5:
2:
	movl	(%%esi,%%eax),%%ecx
	testl	%%ecx,%%ecx		\n// Skip Blank Lines
	jz	5f

        testb	$0x80,%%cl
	jnz	8f
	movb	%%cl,(%%edi)
8:
        testb	$0x80,%%ch
	jnz	8f
	movb	%%ch,1(%%edi)
8:
	roll	$16,%%ecx

        testb	$0x80,%%cl
	jnz	8f
	movb	%%cl,2(%%edi)
8:
        testb	$0x80,%%ch
	jnz	8f
	movb	%%ch,3(%%edi)
8:
5:
	addl	$4,%%eax
	addl	$4,%%edi
	andl	$0x001FF,%%eax

	decl	%%ebp
	jnz	2b

	addl	$0x00200,%%ebx
	addl	$(64),%%edi
	andl	$0x3FE00,%%ebx

	decl	%%edx
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi" , "%ebp"
   );
#else // RAINE_UNIX
     asm("
	pushl	%%ebp
	movl	tmp_f1y,%%ebx
	movl	tmp_bit,%%edi
	movb	$255,%%ch

	movl	$224,%%edx
1:
	movl	$320/4,%%ebp
        movl    (tmp_f1x),%%eax
	movl	%%ebx,%%esi
        addl    tmp_src,%%esi
2:
	movl	(%%esi,%%eax),%%ecx
	testl	%%ecx,%%ecx		\n// Skip Blank Lines
	jz	5f

        testb	$0x80,%%cl
	jnz	8f
	movb	%%cl,(%%edi)
8:
        testb	$0x80,%%ch
	jnz	8f
	movb	%%ch,1(%%edi)
8:
	roll	$16,%%ecx

        testb	$0x80,%%cl
	jnz	8f
	movb	%%cl,2(%%edi)
8:
        testb	$0x80,%%ch
	jnz	8f
	movb	%%ch,3(%%edi)
8:
5:
	addl	$4,%%eax
	addl	$4,%%edi
	andl	$0x001FF,%%eax

	decl	%%ebp
	jnz	2b

	addl	$0x00200,%%ebx
	addl	$(64),%%edi
	andl	$0x3FE00,%%ebx

	decl	%%edx
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi" , "%ebp"
   );
#endif     
     return;
   }
   else{

#ifdef RAINE_DOS     
   asm("
	pushl	%%ebp
	movl	_tmp_f1x,%%eax
	movl	_tmp_src,%%ebx
	movl	%%eax,SMC1-4
	movl	%%ebx,SMC2-4

	movl	_tmp_f1y,%%ebx
	movl	_tmp_bit,%%edi
					// Mask colour is 255 (skip them)
	movb	$255,%%ch
					// Screen is 224 high
	movl	$224,%%edx
1:
	movl	$320,%%ebp		\n// Screen is 320 wide
	movl	$0x2164334A,%%eax
SMC1:
	movl	%%ebx,%%esi
	addl	$0x2164334A,%%esi
SMC2:
2:
	movb	(%%esi,%%eax,1),%%cl
	incl	%%eax
        cmpb	%%ch,%%cl
	jz	8f
	movb	%%cl,(%%edi)
8:
	incl	%%edi
	andl	$0x001FF,%%eax

	decl	%%ebp
	jnz	2b

	addl	$0x00200,%%ebx
	addl	$(64),%%edi
	andl	$0x3FE00,%%ebx

	decl	%%edx
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi" , "%ebp"
   );
#else // RAINE_UNIX
   asm("
	pushl	%%ebp
	movl	tmp_f1y,%%ebx
	movl	tmp_bit,%%edi
					// Mask colour is 255 (skip them)
	movb	$255,%%ch
					// Screen is 224 high
	movl	$224,%%edx
1:
	movl	$320,%%ebp		\n// Screen is 320 wide
	movl	(tmp_f1x),%%eax
	movl	%%ebx,%%esi
	addl	tmp_src,%%esi
2:
	movb	(%%esi,%%eax,1),%%cl
	incl	%%eax
        cmpb	%%ch,%%cl
	jz	8f
	movb	%%cl,(%%edi)
8:
	incl	%%edi
	andl	$0x001FF,%%eax

	decl	%%ebp
	jnz	2b

	addl	$0x00200,%%ebx
	addl	$(64),%%edi
	andl	$0x3FE00,%%ebx

	decl	%%edx
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi" , "%ebp"
   );
#endif   
   return;
   }

   }
   else{

#ifdef RAINE_DOS
     asm("
	pushl	%%ebp
	movl	_tmp_f2xd,%%eax		\n// Poke in the data for the main loops
	movl	_tmp_f2yd,%%ebx		\n// <self modifying code>
	movl	%%eax,Hack1+1
	movl	%%ebx,Hack2+2
	movl	_tmp_f1xd,%%eax
	movl	_tmp_f1yd,%%ebx
	movl	%%eax,Hack3+1
	movl	%%ebx,Hack4+2

	movl	_tmp_src,%%eax		\n// source buffer (512 x 512 x 1byte/pixel)
	movl	%%eax,Hack5+2

	movl	$224,%%ebp		\n// dest size y
	movl	_tmp_f1x,%%eax
	movl	_tmp_f1y,%%ebx
	movl	_tmp_bit,%%edi		\n// destination buffer
1:
	pushl	%%ebp
	pushl	%%eax
	pushl	%%ebx

	movl	$320,%%ebp		\n// dest size x
	movl	%%eax,%%ecx
	movl	%%ebx,%%edx
2:
	shrl	$16,%%ecx
	shrl	$7,%%edx
	andl	$0x001FF,%%ecx		\n// wrap around source x
	andl	$0x3FE00,%%edx		\n// wrap around source y

Hack1:	addl	$0x2164334A,%%eax
	orl	%%edx,%%ecx

Hack2:	addl	$0x2164334A,%%ebx
Hack5:	movb	0x2164334A(%%ecx),%%dl

	movl	%%eax,%%ecx

        testb	$0x80,%%dl		\n// bit 7 used for transparent mask
	jnz	8f
	movb	%%dl,(%%edi)
8:
	movl	%%ebx,%%edx

	incl	%%edi

	decl	%%ebp
	jnz	2b

	popl	%%ebx
	popl	%%eax
	popl	%%ebp

	addl	$(64),%%edi
Hack3:
	addl	$0x2164334A,%%eax
Hack4:
	addl	$0x2164334A,%%ebx

	decl	%%ebp
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi", "%ebp"
   );
#else
     asm("
	pushl	%%ebp
	movl	$224,%%ebp		\n// dest size y
	movl	tmp_f1x,%%eax
	movl	tmp_f1y,%%ebx
	movl	tmp_bit,%%edi		\n// destination buffer
        movl    tmp_src,%%esi
1:
	pushl	%%ebp
	pushl	%%eax
	pushl	%%ebx

	movl	$320,%%ebp		\n// dest size x
	movl	%%eax,%%ecx
	movl	%%ebx,%%edx
2:
	shrl	$16,%%ecx
	shrl	$7,%%edx
	andl	$0x001FF,%%ecx		\n// wrap around source x
	andl	$0x3FE00,%%edx		\n// wrap around source y

	addl	(tmp_f2xd),%%eax
	orl	%%edx,%%ecx

	addl	(tmp_f2yd),%%ebx
	movb	(%%esi,%%ecx),%%dl

	movl	%%eax,%%ecx

        testb	$0x80,%%dl		\n// bit 7 used for transparent mask
	jnz	8f
	movb	%%dl,(%%edi)
8:
	movl	%%ebx,%%edx

	incl	%%edi

	decl	%%ebp
	jnz	2b

	popl	%%ebx
	popl	%%eax
	popl	%%ebp

	addl	$(64),%%edi
	addl	(tmp_f1xd),%%eax
	addl	(tmp_f1yd),%%ebx

	decl	%%ebp
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi", "%ebp"
   );
#endif     
   }

#endif
}

static void RotateBG0(fixed f1x, fixed f1y, fixed f1xd,fixed f1yd,fixed f2xd,fixed f2yd)
{
#if !defined(RAINE_DOS) && !defined(RAINE_UNIX)
   fixed f2x,f2y;
   int dx,dy;
#endif
   UINT8 *BIT;
   UINT8 *SRC;

   // Init Stuff
   // ----------

   f1x+=(319*f2xd);
   f1y+=(319*f2yd);

   BIT=GameViewBitmap->line[0];
   SRC=pixel_bitmap->line[0];

   // Draw the rotated data
   // ---------------------
#if !defined(RAINE_DOS) && !defined(RAINE_UNIX)
   for(dy=0;dy<320;dy++){
      f2x=f1x;
      f2y=f1y;
      for(dx=0;dx<224;dx++){
         BIT[0]=SRC[((f2x>>16)&0x1FF)|((f2y>>7)&0x3FE00)];
         f2x+=f1xd;
         f2y+=f1yd;
         BIT++;
      }
      BIT+=(64);
      f1x-=f2xd;
      f1y-=f2yd;
   }
#else
   tmp_src=(UINT32) &SRC[0];
   tmp_bit=(UINT32) &BIT[0];
   tmp_f1xd=f1xd;
   tmp_f1yd=f1yd;
   tmp_f2xd=f2xd;
   tmp_f2yd=f2yd;
   tmp_f1x=f1x;
   tmp_f1y=f1y;

#ifdef RAINE_DOS
   asm("
	pushl	%%ebp
	movl	_tmp_f1xd,%%eax		\n// Poke in the data for the main loops
	movl	_tmp_f1yd,%%ebx		\n// <self modifying code>
	movl	%%eax,Hck1+1
	movl	%%ebx,Hck2+2

	movl	_tmp_f2xd,%%eax
	movl	_tmp_f2yd,%%ebx
	movl	%%eax,Hck3+1
	movl	%%ebx,Hck4+2

	movl	_tmp_src,%%eax
	movl	%%eax,Hck5+2

	movl	$320,%%ebp
	movl	_tmp_f1x,%%eax
	movl	_tmp_f1y,%%ebx
	movl	_tmp_bit,%%edi
1:
	pushl	%%ebp
	pushl	%%eax
	pushl	%%ebx

	movl	$224,%%ebp
2:
	movl	%%eax,%%ecx
	movl	%%ebx,%%edx
	shrl	$16,%%ecx
	shrl	$7,%%edx
	andl	$0x001FF,%%ecx
	andl	$0x3FE00,%%edx
	orl	%%ecx,%%edx
Hck5:	movb	0x2164334A(%%edx),%%cl

Hck1:	addl	$0x2164334A,%%eax
	movb	%%cl,(%%edi)
Hck2:	addl	$0x2164334A,%%ebx
	incl	%%edi

	decl	%%ebp
	jnz	2b

	popl	%%ebx
	popl	%%eax
	popl	%%ebp

	addl	$(64),%%edi
Hck3:
	subl	$0x2164334A,%%eax
Hck4:
	subl	$0x2164334A,%%ebx

	decl	%%ebp
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi", "%ebp"
   );
#else // RAINE_UNIX
   asm("
	pushl	%%ebp
	movl	$320,%%ebp
	movl	tmp_f1x,%%eax
	movl	tmp_f1y,%%ebx
	movl	tmp_bit,%%edi
        movl    tmp_src,%%esi
1:
	pushl	%%ebp
	pushl	%%eax
	pushl	%%ebx

	movl	$224,%%ebp
2:
	movl	%%eax,%%ecx
	movl	%%ebx,%%edx
	shrl	$16,%%ecx
	shrl	$7,%%edx
	andl	$0x001FF,%%ecx
	andl	$0x3FE00,%%edx
	orl	%%ecx,%%edx
	movb	(%%esi,%%edx),%%cl
	addl	(tmp_f1xd),%%eax
	movb	%%cl,(%%edi)
	addl	(tmp_f1yd),%%ebx
	incl	%%edi

	decl	%%ebp
	jnz	2b

	popl	%%ebx
	popl	%%eax
	popl	%%ebp

	addl	$(64),%%edi
	subl	(tmp_f2xd),%%eax
	subl	(tmp_f2yd),%%ebx

	decl	%%ebp
	jnz	1b
	popl	%%ebp
	"
	:
	:
	: "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi", "%ebp"
   );
#endif   

#endif
}

void tc0005rot_draw_rot_r270(int col_ofs)
{
  //int x,y,zz;
  fixed px,py,ang1,ang2,ang3,ang4;

   /*if(RefreshBuffers){

   zz=0;
   for(y=512;y<1024;y+=8){
      for(x=0;x<512;x+=8){
         Draw8x8_Mapped(&GFX_BG[(ReadWord(&RAM_BG[zz])&0x3FFF)<<6],x,y,&rot_map[(ReadWord(&RAM_BG[zz])>>10)&0x30]);
         zz+=2;
      }
   }

   }*/

   Set_Pens_15bit_xRGB(rot_pens, col_ofs<<4, 64);

   // Copy and Rotate BG0
   // -------------------

   px=((ReadWord(&RAM_SCR[0x00])<<20)|(ReadWord(&RAM_SCR[0x02])<<4));

   py=((ReadWord(&RAM_SCR[0x08])<<20)|(ReadWord(&RAM_SCR[0x0A])<<4));

   ang2=ReadWord(&RAM_SCR[0x04]);	// -8000 to 7FF0 (?)

   if((ang2&0x8000)!=0){ang2=0-(((ang2^0xFFFF)+1)<<4);}
   else{ang2=ang2<<4;}

   ang1=ReadWord(&RAM_SCR[0x06]);

   if((ang1&0x8000)!=0){ang1=0-(((ang1^0xFFFF)+1)<<4);}
   else{ang1=ang1<<4;}

   ang4=ReadWord(&RAM_SCR[0x0C]);

   if((ang4&0x8000)!=0){ang4=0-(((ang4^0xFFFF)+1)<<4);}
   else{ang4=ang4<<4;}

   ang3=ReadWord(&RAM_SCR[0x0E]);

   if((ang3&0x8000)!=0){ang3=0-(((ang3^0xFFFF)+1)<<4);}
   else{ang3=ang3<<4;}

   RotateBG0(px,py,ang1,ang2,ang3,ang4);
}

void tc0005rot_draw_rot(int col_ofs)
{
  //int x,y,zz;
   fixed px,py,ang1,ang2,ang3,ang4;

   /*if(RefreshBuffers){

   zz=0;
   for(y=512;y<1024;y+=8){
      for(x=0;x<512;x+=8){
         Draw8x8_Mapped(&GFX_BG[(ReadWord(&RAM_BG[zz])&0x3FFF)<<6],x,y,&rot_map[(ReadWord(&RAM_BG[zz])>>10)&0x30]);
         zz+=2;
      }
   }

   }*/

   Set_Pens_12bit_RGBx(rot_pens, col_ofs<<4, 64);

   // Copy and Rotate BG0
   // -------------------

   px=((ReadWord(&RAM_SCR[0x00])<<20)|(ReadWord(&RAM_SCR[0x02])<<4));

   py=((ReadWord(&RAM_SCR[0x08])<<20)|(ReadWord(&RAM_SCR[0x0A])<<4));

   ang2=ReadWord(&RAM_SCR[0x04]);	// -8000 to 7FF0 (?)

   if((ang2&0x8000)!=0){ang2=0-(((ang2^0xFFFF)+1));}

   ang1=ReadWord(&RAM_SCR[0x06]);

   if((ang1&0x8000)!=0){ang1=0-(((ang1^0xFFFF)+1));}

   ang4=ReadWord(&RAM_SCR[0x0C]);

   if((ang4&0x8000)!=0){ang4=0-(((ang4^0xFFFF)+1));}

   ang3=ReadWord(&RAM_SCR[0x0E]);

   if((ang3&0x8000)!=0){ang3=0-(((ang3^0xFFFF)+1));}

   RotateF2SystemBG0(px,py,ang1<<4,ang2<<5,ang3<<4,ang4<<5);
}


