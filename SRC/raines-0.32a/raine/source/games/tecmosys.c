/******************************************************************************/
/*                                                                            */
/*                       NINJA GAIDEN (C)1988 TECMO                           */
/*                                                                            */
/*                       TECMO KNIGHT (C)1989 TECMO                           */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "tecmosys.h"
#include "taitosnd.h"
#include "2203intf.h"
#include "sasound.h"		// sample support routines
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

static struct DIR_INFO ninja_gaiden_dirs[] =
{
   { "ninja_gaiden", },
   { "gaiden", },
   { NULL, },
};

static struct ROM_INFO ninja_gaiden_roms[] =
{
   {    "gaiden.10", 0x00020000, 0xa6451dec, 0, 0, 0, },
   {    "gaiden.11", 0x00020000, 0x7fbfdf5e, 0, 0, 0, },
   {    "gaiden.12", 0x00020000, 0x90f1e13a, 0, 0, 0, },
   {    "gaiden.13", 0x00020000, 0x7d9f5c5e, 0, 0, 0, },
   {     "gaiden.2", 0x00020000, 0x454f7314, 0, 0, 0, },
   {     "gaiden.3", 0x00010000, 0x75fd3e6a, 0, 0, 0, },
   {     "gaiden.4", 0x00020000, 0xb0e0faf9, 0, 0, 0, },
   {     "gaiden.6", 0x00020000, 0xe7ccdf9f, 0, 0, 0, },
   {     "gaiden.7", 0x00020000, 0x016bec95, 0, 0, 0, },
   {     "gaiden.8", 0x00020000, 0x7ef7f880, 0, 0, 0, },
   {     "gaiden.9", 0x00020000, 0x6e9b7fd3, 0, 0, 0, },
   {       "18.bin", 0x00020000, 0x3fadafd6, 0, 0, 0, },
   {       "19.bin", 0x00020000, 0xddae9d5b, 0, 0, 0, },
   {     "gaiden.1", 0x00020000, 0xe037ff7c, 0, 0, 0, },
   {       "20.bin", 0x00020000, 0x08cf7a93, 0, 0, 0, },
   {       "21.bin", 0x00020000, 0x1ac892f5, 0, 0, 0, },
   {     "gaiden.5", 0x00010000, 0x8d4035f7, 0, 0, 0, },
   {       "14.bin", 0x00020000, 0x1ecfddaa, 0, 0, 0, },
   {       "15.bin", 0x00020000, 0x1291a696, 0, 0, 0, },
   {       "16.bin", 0x00020000, 0x140b47ca, 0, 0, 0, },
   {       "17.bin", 0x00020000, 0x7638cccb, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO ninja_gaiden_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x01A000, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x01A000, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x01A000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x01A002, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x01A002, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x01A002, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x01A002, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x01A002, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x01A002, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x01A002, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x01A000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x01A003, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x01A003, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x01A003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x01A003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x01A003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x01A003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x01A003, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_ninja_gaiden_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT4,           0x08, 0x02 },
   { MSG_OFF,                 0x08, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO ninja_gaiden_dsw[] =
{
   { 0x01A005, 0xFF, dsw_data_ninja_gaiden_0 },
   { 0x01A004, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO ninja_gaiden_video =
{
   DrawTecmoSys,
   256,
   224,
   64,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
};

static struct YM2203interface ym2203_interface =
{
  2,				// 2 x YM2203
  4000000,			// Rate - Guessed
  { 0x00ff20c0,	0x00ff20c0 },	// Volume Control
  { 0,		0 },
  { 0,		0 },
  { 0,		0 },
  { 0,		0 },
  { NULL,	NULL }
};

static struct OKIM6295interface m6295_interface =
{
   1,					// 1 chip
   { 15700/2 },				// rate
   { 0 },		// rom list
   { 100 },
};

static struct SOUND_INFO ninja_gaiden_sound[] =
{
   { SOUND_YM2203,  &ym2203_interface,    },
   { SOUND_M6295,   &m6295_interface,     },
   { 0,             NULL,                 },
};

struct GAME_MAIN game_ninja_gaiden =
{
   ninja_gaiden_dirs,
   ninja_gaiden_roms,
   ninja_gaiden_inputs,
   ninja_gaiden_dsw,
   NULL,

   LoadNGaiden,
   ClearNGaiden,
   &ninja_gaiden_video,
   ExecuteTecmoSysFrame,
   "gaiden",
   "Ninja Gaiden",
   "�E�җ����`",
   COMPANY_ID_TECMO,
   NULL,
   1988,
   ninja_gaiden_sound,
   GAME_BEAT,
};

static struct DIR_INFO tecmo_knight_dirs[] =
{
   { "tecmo_knight", },
   { "tknight", },
   { NULL, },
};

static struct ROM_INFO tecmo_knight_roms[] =
{
   {    "tkni1.bin", 0x00020000, 0x9121daa8, 0, 0, 0, },
   {    "tkni2.bin", 0x00020000, 0x6669cd87, 0, 0, 0, },
   {    "tkni3.bin", 0x00010000, 0x15623ec7, 0, 0, 0, },
   {    "tkni4.bin", 0x00020000, 0xa7a1dbcf, 0, 0, 0, },
   {    "tkni5.bin", 0x00010000, 0x5ed15896, 0, 0, 0, },
   {    "tkni6.bin", 0x00080000, 0xf68fafb1, 0, 0, 0, },
   {    "tkni7.bin", 0x00080000, 0x4b4d4286, 0, 0, 0, },
   {    "tkni8.bin", 0x00080000, 0x4931b184, 0, 0, 0, },
   {    "tkni9.bin", 0x00080000, 0xd22f4239, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO tecmo_knight_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x01A000, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x01A000, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x01A000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x01A002, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x01A002, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x01A002, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x01A002, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x01A002, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x01A002, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x01A002, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x01A000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x01A003, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x01A003, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x01A003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x01A003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x01A003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x01A003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x01A003, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_tecmo_knight_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT4,           0x08, 0x02 },
   { MSG_OFF,                 0x08, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO tecmo_knight_dsw[] =
{
   { 0x01A005, 0xFF, dsw_data_tecmo_knight_0 },
   { 0x01A004, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

struct GAME_MAIN game_tecmo_knight =
{
   tecmo_knight_dirs,
   tecmo_knight_roms,
   tecmo_knight_inputs,
   tecmo_knight_dsw,
   NULL,

   LoadTKnight,
   ClearTKnight,
   &ninja_gaiden_video,
   ExecuteTecmoSysFrame,
   "tknight",
   "Tecmo Knight",
   "���C���h�t�@���O",
   COMPANY_ID_TECMO,
   NULL,
   1989,
   ninja_gaiden_sound,
   GAME_BEAT,
};

static int sport=0;

static void CtrlWriteB(UINT32 offset,UINT8 data)
{

   if((offset==0x7A802)||(offset==0x7A803)){
      sport=data&0xFF;
#ifdef RAINE_DEBUG
      print_debug("68000 Sends $%02x\n",sport);
#endif
      cpu_int_nmi(CPU_Z80_0);
   }

   offset-=0x60000;
   offset&=0x1FFFF;

   RAM[offset^1]=data;
}

static UINT16 SoundReadZ80(UINT16 address)
{
   #ifdef RAINE_DEBUG
   print_debug("Z80 Receives $%02x\n",sport);
   #endif
   return sport;
}

void AddTecmoSound(UINT32 p1, UINT32 p2, UINT32 romsize, UINT8 *adpcm, UINT32 adpcmsize)
{

   // Apply Speed Patch
   // -----------------

   Z80ROM[p1+0]=0xD3;	// OUTA (AAh)
   Z80ROM[p1+1]=0xAA;	//

   SetStopZ80Mode2( (UINT16) p2);

   // Setup Z80 memory map
   // --------------------

   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);

   AddZ80AReadByte(0x0000, 0xF7FF, NULL,			Z80ROM+0x0000);	// Z80 ROM/RAM
   AddZ80AReadByte(0xF800, 0xF800, M6295_A_Read_Z80,		NULL);		// ADPCM
   //AddZ80AReadByte(0xF810, 0xF811, YM2203AReadZ80,		NULL);		// YM2203 A
   //AddZ80AReadByte(0xF820, 0xF821, YM2203BReadZ80,		NULL);		// YM2203 B
   AddZ80AReadByte(0xFC20, 0xFC20, SoundReadZ80,		NULL);		// 68000 COMM
   AddZ80AReadByte(0x0000, 0xFFFF, NULL,			Z80ROM+0x0000);	// <test>
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,		NULL);		// <bad reads>
   AddZ80AReadByte(-1, -1, NULL, NULL);

   AddZ80AWriteByte(0xF000, 0xF7FF, NULL,			Z80ROM+0xF000);	// Z80 RAM
   AddZ80AWriteByte(0xF800, 0xF800, M6295_A_Write_Z80,		NULL);		// ADPCM CHANNEL A
   AddZ80AWriteByte(0xF810, 0xF811, YM2203AWriteZ80,		NULL);		// YM2203 A
   AddZ80AWriteByte(0xF820, 0xF821, YM2203BWriteZ80,		NULL);		// YM2203 B
   AddZ80AWriteByte(0x0000, 0xFFFF, NULL,			Z80ROM+0x0000);	// <test>
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,		NULL);		// <bad writes>
   AddZ80AWriteByte(-1, -1, NULL, NULL);

   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,           NULL);
   AddZ80AReadPort(  -1,   -1, NULL,                    NULL);

   AddZ80AWritePort(0xAA, 0xAA, StopZ80Mode2,           NULL);
   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,         NULL);
   AddZ80AWritePort(  -1,   -1, NULL,                   NULL);

   AddZ80AInit();

   ADPCMSetBuffers(((struct ADPCMinterface*)&m6295_interface),adpcm,adpcmsize);
   
}

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG1;
static UINT8 *GFX_FG0;
static UINT8 *GFX_SPR;

static UINT8 *GFX_BG0_SOLID;
static UINT8 *GFX_BG1_SOLID;
static UINT8 *GFX_FG0_SOLID;
static UINT8 *GFX_SPR_SOLID;

void LoadNGaiden(void)
{
   int ta,tb;

   if(!(ROM=AllocateMem(0x040000))) return;
   if(!(RAM=AllocateMem(0x080000))) return;
   if(!(GFX=AllocateMem(0x420000))) return;

   GFX_BG0=GFX+0x220000;
   GFX_BG1=GFX+0x320000;
   GFX_FG0=GFX+0x000000;
   GFX_SPR=GFX+0x020000;

   if(!load_rom("gaiden.1", RAM, 0x20000)) return;	// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("gaiden.2", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   tb=0;
   if(!load_rom("gaiden.5", RAM, 0x10000)) return;	// 8x8 FG0 GFX
   for(ta=0;ta<0x10000;ta++){
      GFX_FG0[tb++]=RAM[ta]>>4;
      GFX_FG0[tb++]=RAM[ta]&15;
   }

   tb=0;
   if(!load_rom("gaiden.6",  RAM+0x00000, 0x20000)) return;	// 16x16 SPRITES
   if(!load_rom("gaiden.8",  RAM+0x20000, 0x20000)) return;
   if(!load_rom("gaiden.10", RAM+0x40000, 0x20000)) return;
   if(!load_rom("gaiden.12", RAM+0x60000, 0x20000)) return;
   for(ta=0;ta<0x80000;ta+=2){
      GFX_SPR[tb+0]=RAM[ta]>>4;
      GFX_SPR[tb+1]=RAM[ta]&15;
      GFX_SPR[tb+4]=RAM[ta+1]>>4;
      GFX_SPR[tb+5]=RAM[ta+1]&15;
      tb+=8;
   }

   tb=0;
   if(!load_rom("gaiden.7",  RAM+0x00000, 0x20000)) return;	// 16x16 SPRITES
   if(!load_rom("gaiden.9",  RAM+0x20000, 0x20000)) return;
   if(!load_rom("gaiden.11", RAM+0x40000, 0x20000)) return;
   if(!load_rom("gaiden.13", RAM+0x60000, 0x20000)) return;
   for(ta=0;ta<0x80000;ta+=2){
      GFX_SPR[tb+2]=RAM[ta]>>4;
      GFX_SPR[tb+3]=RAM[ta]&15;
      GFX_SPR[tb+6]=RAM[ta+1]>>4;
      GFX_SPR[tb+7]=RAM[ta+1]&15;
      tb+=8;
   }

   tb=0;
   if(!load_rom("14.bin", RAM+0x00000, 0x20000)) return;	// 16x16 BG0
   if(!load_rom("15.bin", RAM+0x20000, 0x20000)) return;
   if(!load_rom("16.bin", RAM+0x40000, 0x20000)) return;
   if(!load_rom("17.bin", RAM+0x60000, 0x20000)) return;
   for(ta=0;ta<0x80000;ta+=4){
      GFX_BG0[tb+0]=RAM[ta+0]>>4;
      GFX_BG0[tb+1]=RAM[ta+0]&15;
      GFX_BG0[tb+2]=RAM[ta+1]>>4;
      GFX_BG0[tb+3]=RAM[ta+1]&15;
      GFX_BG0[tb+4]=RAM[ta+2]>>4;
      GFX_BG0[tb+5]=RAM[ta+2]&15;
      GFX_BG0[tb+6]=RAM[ta+3]>>4;
      GFX_BG0[tb+7]=RAM[ta+3]&15;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=0x8;}}
   }

   tb=0;
   if(!load_rom("18.bin", RAM+0x00000, 0x20000)) return;	// 16x16 BG1
   if(!load_rom("19.bin", RAM+0x20000, 0x20000)) return;
   if(!load_rom("20.bin", RAM+0x40000, 0x20000)) return;
   if(!load_rom("21.bin", RAM+0x60000, 0x20000)) return;
   for(ta=0;ta<0x80000;ta+=4){
      GFX_BG1[tb+0]=RAM[ta+0]>>4;
      GFX_BG1[tb+1]=RAM[ta+0]&15;
      GFX_BG1[tb+2]=RAM[ta+1]>>4;
      GFX_BG1[tb+3]=RAM[ta+1]&15;
      GFX_BG1[tb+4]=RAM[ta+2]>>4;
      GFX_BG1[tb+5]=RAM[ta+2]&15;
      GFX_BG1[tb+6]=RAM[ta+3]>>4;
      GFX_BG1[tb+7]=RAM[ta+3]&15;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=0x8;}}
   }

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x100000 >> 8);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, 0x100000 >> 8);
   GFX_FG0_SOLID = make_solid_mask_8x8(GFX_FG0, 0x020000 >> 6);
   GFX_SPR_SOLID = make_solid_mask_8x8(GFX_SPR, 0x200000 >> 6);

   FreeMem(RAM);

   RAMSize=0x24000+0x10000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   /*-------[SOUND SYSTEM INIT]-------*/

   Z80ROM=RAM+0x24000;
   if(!load_rom("gaiden.3", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM = AllocateMem(0x20000))) return;
   if(!load_rom("gaiden.4", PCMROM, 0x20000)) return;	// ADPCM ROM

   AddTecmoSound(0x00BD, 0x00BD, 0x10000, PCMROM, 0x20000);

   /*---------------------------------*/

   memset(RAM+0x00000,0x00,0x24000);
   memset(RAM+0x1A000,0xFF,0x01000);

   // 68000 Speed hack
   // ----------------

   WriteWord68k(&ROM[0x1338],0x4EF9);		// jmp $000300
   WriteLong68k(&ROM[0x133A],0x00000300);

   WriteWord68k(&ROM[0x300],0x41F9);		// lea #$001344,a0
   WriteLong68k(&ROM[0x302],0x00001344);
   WriteWord68k(&ROM[0x306],0x4EB9);		// jsr $0011D8
   WriteLong68k(&ROM[0x308],0x000011D8);
   WriteLong68k(&ROM[0x30C],0x13FC0000);	// move.w #$0000,$AA0000
   WriteLong68k(&ROM[0x310],0x00AA0000);	//
   WriteWord68k(&ROM[0x314],0x60EA);		// bra <loop>

   // Fix 68000 Checksum
   // ------------------

   ROM[0x54A]=0x60;
   ROM[0x552]=0x60;

   InitPaletteMap(RAM+0x18000, 0x40, 0x10, 0x1000);

   set_colour_mapper(&col_map_xxxx_bbbb_gggg_rrrr);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x20000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x060000, 0x07FFFF, NULL, RAM+0x000000);			// *ALL* RAM
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x060000, 0x07FFFF, NULL, RAM+0x000000);			// *ALL* RAM
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x060000, 0x079FFF, NULL, RAM+0x000000);		// *ALL* RAM
   AddWriteByte(0x07A000, 0x07FFFF, CtrlWriteB, NULL);			// *ALL* RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000,  NULL);			// Trap Idle 68000
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x060000, 0x07FFFF, NULL, RAM+0x000000);		// *ALL* RAM
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers...
}

void LoadTKnight(void)
{
   int ta,tb;

   if(!(ROM=AllocateMem(0x40000))) return;
   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(GFX=AllocateMem(0x420000))) return;

   GFX_BG0=GFX+0x220000;
   GFX_BG1=GFX+0x320000;
   GFX_FG0=GFX+0x000000;
   GFX_SPR=GFX+0x020000;

   if(!load_rom("TKNI1.BIN", RAM, 0x20000)) return;	// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("TKNI2.BIN", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   tb=0;
   if(!load_rom("TKNI5.BIN", RAM, 0x10000)) return;	// 8x8 FG0 GFX
   for(ta=0;ta<0x10000;ta++){
      GFX_FG0[tb++]=RAM[ta]>>4;
      GFX_FG0[tb++]=RAM[ta]&15;
   }

   tb=0;
   if(!load_rom("TKNI9.BIN", RAM, 0x80000)) return;	// 16x16 SPRITES
   for(ta=0;ta<0x80000;ta+=2){
      GFX_SPR[tb+0]=RAM[ta]>>4;
      GFX_SPR[tb+1]=RAM[ta]&15;
      GFX_SPR[tb+4]=RAM[ta+1]>>4;
      GFX_SPR[tb+5]=RAM[ta+1]&15;
      tb+=8;
   }
   tb=0;
   if(!load_rom("TKNI8.BIN", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta+=2){
      GFX_SPR[tb+2]=RAM[ta]>>4;
      GFX_SPR[tb+3]=RAM[ta]&15;
      GFX_SPR[tb+6]=RAM[ta+1]>>4;
      GFX_SPR[tb+7]=RAM[ta+1]&15;
      tb+=8;
   }

   tb=0;
   if(!load_rom("TKNI7.BIN", RAM, 0x80000)) return;		// 16x16 BACKGROUND
   for(ta=0;ta<0x80000;ta+=4){
      GFX_BG0[tb+0]=RAM[ta+0]>>4;
      GFX_BG0[tb+1]=RAM[ta+0]&15;
      GFX_BG0[tb+2]=RAM[ta+1]>>4;
      GFX_BG0[tb+3]=RAM[ta+1]&15;
      GFX_BG0[tb+4]=RAM[ta+2]>>4;
      GFX_BG0[tb+5]=RAM[ta+2]&15;
      GFX_BG0[tb+6]=RAM[ta+3]>>4;
      GFX_BG0[tb+7]=RAM[ta+3]&15;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=0x8;}}
   }
   tb=0;
   if(!load_rom("TKNI6.BIN", RAM, 0x80000)) return;		// 16x16 BACKGROUND
   for(ta=0;ta<0x80000;ta+=4){
      GFX_BG1[tb+0]=RAM[ta+0]>>4;
      GFX_BG1[tb+1]=RAM[ta+0]&15;
      GFX_BG1[tb+2]=RAM[ta+1]>>4;
      GFX_BG1[tb+3]=RAM[ta+1]&15;
      GFX_BG1[tb+4]=RAM[ta+2]>>4;
      GFX_BG1[tb+5]=RAM[ta+2]&15;
      GFX_BG1[tb+6]=RAM[ta+3]>>4;
      GFX_BG1[tb+7]=RAM[ta+3]&15;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=0x8;}}
   }

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x100000 >> 8);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, 0x100000 >> 8);
   GFX_FG0_SOLID = make_solid_mask_8x8(GFX_FG0, 0x020000 >> 6);
   GFX_SPR_SOLID = make_solid_mask_8x8(GFX_SPR, 0x200000 >> 6);

   FreeMem(RAM);

   RAMSize=0x24000+0x10000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   /*-------[SOUND SYSTEM INIT]-------*/

   Z80ROM=RAM+0x24000;
   if(!load_rom("TKNI3.BIN", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM = AllocateMem(0x20000))) return;
   if(!load_rom("TKNI4.BIN", PCMROM, 0x20000)) return;	// ADPCM ROM

   AddTecmoSound(0x00D2, 0x00D2, 0x10000, PCMROM, 0x20000);

   /*---------------------------------*/

   memset(RAM+0x00000,0x00,0x24000);
   memset(RAM+0x1A000,0xFF,0x01000);

   // Protected JSR's hack
   // --------------------

   WriteLong68k(&ROM[0x1AA76],0x0240001F);	// andi.w #$001F,d0
   WriteWord68k(&ROM[0x1AA7A],0xE348);		// lsl.w #1,d0
   WriteLong68k(&ROM[0x1AA7C],0x4BF824D2);	// lea $24d2.w,a5
   WriteLong68k(&ROM[0x1AA80],0x30350000);	// move.w 0(a5,d0.w),d0
   WriteWord68k(&ROM[0x1AA84],0x4E75);		// rts

   // 68000 Speed hack
   // ----------------

   WriteLong68k(&ROM[0x0AAE],0x13FC0000);	// move.w #$0000,$AA0000
   WriteLong68k(&ROM[0x0AB2],0x00AA0000);	//

   InitPaletteMap(RAM+0x18000, 0x40, 0x10, 0x1000);

   set_colour_mapper(&col_map_xxxx_bbbb_gggg_rrrr);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x20000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x060000, 0x07FFFF, NULL, RAM+0x000000);			// *ALL* RAM
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x060000, 0x07FFFF, NULL, RAM+0x000000);			// *ALL* RAM
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x060000, 0x079FFF, NULL, RAM+0x000000);		// *ALL* RAM
   AddWriteByte(0x07A000, 0x07FFFF, CtrlWriteB, NULL);			// *ALL* RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000,  NULL);			// Trap Idle 68000
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x060000, 0x07FFFF, NULL, RAM+0x000000);		// *ALL* RAM
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers...
}

void ClearTecmoSys(void)
{
   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x040000,1);
      //save_debug("RAM.bin",RAM,0x020000,1);
   #endif
}

void ExecuteTecmoSysFrame(void)
{
   RAM[0x1A005] = get_dsw(0);
   RAM[0x1A004] = get_dsw(1);

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(10,60));	// M68000 10MHz (60fps)
   cpu_interrupt(CPU_68K_0, 5);

   cpu_execute_cycles(CPU_Z80_0, CPU_FRAME_MHz(8,60));
   cpu_interrupt(CPU_Z80_0, 0x38);
}

static void draw_tecmo_object(int pri)
{
   static int spr_off[4 * 4 * 2] =
   {
      0,  0, 16,  0,  0, 16, 16, 16,
     32,  0, 48,  0, 32, 16, 48, 16,
      0, 32, 16, 32,  0, 48, 16, 48,
     32, 32, 48, 32, 32, 48, 48, 48,
   };

   int x, y, ta, zz, dx, dy, i;
   UINT8 *MAP;

   for(zz=0x16000;zz<0x16800;zz+=16){			// SPRITES (HIGH PRIORITY)

   if((RAM[zz]&0x84)==pri){

   y=(ReadWord(&RAM[zz+6])+48)&0x1FF;
   x=(ReadWord(&RAM[zz+8])+64)&0x1FF;

   if((x>0)&&(y>0)&&(x<256+64)&&(y<224+64)){

   ta=ReadWord(&RAM[zz+2]);
   if(ta!=0){

      MAP_PALETTE_MAPPED_NEW(
         RAM[zz+4]>>4,
         16,
         MAP
      );

      switch(RAM[zz]&3)
      {
         case 0:
            switch(RAM[zz+4]&0x03){
            case 0:			// 8x8
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[ta<<6],x,y,MAP);
            break;
            case 1:			// 16x16
               ta &= 0xFFFC;
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+0)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+1)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+2)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+3)<<6],x+8,y+8,MAP);
            break;
            case 2:			// 32x32
               dx = x;
               dy = y;
               ta &= 0xFFF0;
               for(i = 0; i < 4; i ++){
               x = dx + spr_off[(i<<1) + 0];
               y = dy + spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+0)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+1)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+2)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+3)<<6],x+8,y+8,MAP);
               ta+=4;
               }
            break;
            case 3:			// 64x64
               dx = x;
               dy = y;
               ta &= 0xFFC0;
               for(i = 0; i < 16; i ++){
               x = dx + spr_off[(i<<1) + 0];
               y = dy + spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+0)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+1)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+2)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_Rot(&GFX_SPR[(ta+3)<<6],x+8,y+8,MAP);
               ta+=4;
               }
            break;
            }
         break;
         case 1:
            switch(RAM[zz+4]&0x03){
            case 0:			// 8x8
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[ta<<6],x,y,MAP);
            break;
            case 1:			// 16x16
               ta &= 0xFFFC;
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+0)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+1)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+2)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+3)<<6],x+0,y+8,MAP);
            break;
            case 2:			// 32x32
               dx = x + 16;
               dy = y;
               ta &= 0xFFF0;
               for(i = 0; i < 4; i ++){
               x = dx - spr_off[(i<<1) + 0];
               y = dy + spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+0)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+1)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+2)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+3)<<6],x+0,y+8,MAP);
               ta+=4;
               }
            break;
            case 3:			// 64x64
               dx = x + 48;
               dy = y;
               ta &= 0xFFC0;
               for(i = 0; i < 16; i ++){
               x = dx - spr_off[(i<<1) + 0];
               y = dy + spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+0)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+1)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+2)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipY_Rot(&GFX_SPR[(ta+3)<<6],x+0,y+8,MAP);
               ta+=4;
               }
            break;
            }
         break;
         case 2:
            switch(RAM[zz+4]&0x03){
            case 0:			// 8x8
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[ta<<6],x,y,MAP);
            break;
            case 1:			// 16x16
               ta &= 0xFFFC;
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+0)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+1)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+2)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+3)<<6],x+8,y+0,MAP);
            break;
            case 2:			// 32x32
               dx = x;
               dy = y + 16;
               ta &= 0xFFF0;
               for(i = 0; i < 4; i ++){
               x = dx + spr_off[(i<<1) + 0];
               y = dy - spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+0)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+1)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+2)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+3)<<6],x+8,y+0,MAP);
               ta+=4;
               }
            break;
            case 3:			// 64x64
               dx = x;
               dy = y + 48;
               ta &= 0xFFC0;
               for(i = 0; i < 16; i ++){
               x = dx + spr_off[(i<<1) + 0];
               y = - spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+0)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+1)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+2)<<6],x+0,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipX_Rot(&GFX_SPR[(ta+3)<<6],x+8,y+0,MAP);
               ta+=4;
               }
            break;
            }
         break;
         case 3:
            switch(RAM[zz+4]&0x03){
            case 0:			// 8x8
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[ta<<6],x,y,MAP);
            break;
            case 1:			// 16x16
               ta &= 0xFFFC;
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+0)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+1)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+2)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+3)<<6],x+0,y+0,MAP);
            break;
            case 2:			// 32x32
               dx = x + 16;
               dy = y + 16;
               ta &= 0xFFF0;
               for(i = 0; i < 4; i ++){
               x = dx - spr_off[(i<<1) + 0];
               y = dy - spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+0)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+1)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+2)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+3)<<6],x+0,y+0,MAP);
               ta+=4;
               }
            break;
            case 3:			// 64x64
               dx = x + 48;
               dy = y + 48;
               ta &= 0xFFC0;
               for(i = 0; i < 16; i ++){
               x = dx - spr_off[(i<<1) + 0];
               y = dy - spr_off[(i<<1) + 1];
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+0)<<6],x+8,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+1)<<6],x+0,y+8,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+2)<<6],x+8,y+0,MAP);
               Draw8x8_Trans_Mapped_FlipXY_Rot(&GFX_SPR[(ta+3)<<6],x+0,y+0,MAP);
               ta+=4;
               }
            break;
            }
         break;
      }

   }

   }

   }
   }
}

void DrawTecmoSys(void)
{
   int x,y,ta,zz,zzz,zzzz,x16,y16;
   UINT8 *MAP,bcol;

   ClearPaletteMap();

      MAP_PALETTE_MAPPED_NEW(
         0x20,
         16,
         MAP
      );

   bcol=MAP[0];

   /*

   BG0

   */

   MAKE_SCROLL_1024x512_2_16(
      ReadWord(&RAM[0x1A30C]),
      ReadWord(&RAM[0x1A304])+16
   );

   START_SCROLL_1024x512_2_16(64,64,256,224);

      MAP_PALETTE_MAPPED_NEW(
         (RAM[0x14000+zz]>>4)|0x30,
         16,
         MAP
      );

      MAP[0]=bcol;

      Draw16x16_Mapped_Rot(&GFX_BG0[(ReadWord(&RAM[0x15000+zz])&0xFFF)<<8],x,y,MAP);

   END_SCROLL_1024x512_2_16();

   /*

   OBJECT LOW

   */

   draw_tecmo_object(0x84);

   /*

   BG1

   */

   MAKE_SCROLL_1024x512_2_16(
      ReadWord(&RAM[0x1A20C]),
      ReadWord(&RAM[0x1A204])+16
   );

   START_SCROLL_1024x512_2_16(64,64,256,224);

      ta=ReadWord(&RAM[0x13000+zz])&0xFFF;
      if(GFX_BG1_SOLID[ta]){				// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            (RAM[0x12000+zz]>>4)|0x20,
            16,
            MAP
         );

         if(GFX_BG1_SOLID[ta]==1)			// Some pixels; trans
            Draw16x16_Trans_Mapped_Rot(&GFX_BG1[ta<<8],x,y,MAP);
         else						// all pixels; solid
            Draw16x16_Mapped_Rot(&GFX_BG1[ta<<8],x,y,MAP);

      }

   END_SCROLL_1024x512_2_16();

   /*

   OBJECT HIGH

   */

   draw_tecmo_object(0x04);

   /*

   FG0

   */

   MAKE_SCROLL_256x256_2_8(
      ReadWord(&RAM[0x1A10C]),
      ReadWord(&RAM[0x1A104])+16
   );

   START_SCROLL_256x256_2_8(64,64,256,224);

      ta=ReadWord(&RAM[0x10800+zz])&0x7FF;
      if(GFX_FG0_SOLID[ta]){				// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            (RAM[0x10000+zz]>>4)|0x10,
            16,
            MAP
         );

         if(GFX_FG0_SOLID[ta]==1)			// Some pixels; trans
            Draw8x8_Trans_Mapped_Rot(&GFX_FG0[ta<<6],x,y,MAP);
         else						// all pixels; solid
            Draw8x8_Mapped_Rot(&GFX_FG0[ta<<6],x,y,MAP);
      }

   END_SCROLL_256x256_2_8();

}

/*

TECMO 68000 SYSTEM (C)1988-89 TECMO
===================================

Information compiled by [J3d!]

Main CPU......68000
Sound CPUs....Z80, YM2203, YM2203
Screen........256x224

68000 MEMORY MAP
================

-------+--------+--------------------------
Start  | End    | Description
-------+--------+--------------------------
000000 | 03FFFF | 68000 ROM
060000 | 063FFF | 68000 RAM
070000 | 0707FF | FG0 RAM A
070800 | 070FFF | FG0 RAM B
072000 | 072FFF | BG1 RAM A
073000 | 073FFF | BG1 RAM B
074000 | 074FFF | BG0 RAM A
075000 | 075FFF | BG0 RAM B
076000 | 0767FF | SPRITE RAM
078000 | 0787FF | COLOUR RAM
-------+--------+--------------------------

COLOR RAM
=========

- 64 banks of 16 colours
- Format is 12-bit bgr
- 1024 colours onscreen from palette of 4096

------+------------------------------------
Banks | Description
------+------------------------------------
00-0F | Sprites
10-1F | FG0 Plans
20-2F | BG1 Plane
30-3F | BG0 Plane
------+------------------------------------

SPRITE RAM
==========

- 16 bytes/sprite entry
- 128 entries
- Sprites are infact 8x8, but chained to form
  8x8, 16x16, 32x32 and 64x64. 32x32 is most
  common, followed by 16x16, others are rarely
  needed, but sometimes used...

-----+--------+----------------------------
Byte | Bit(s) | Description
-----+76543210+----------------------------
  0  |........| <Garbage?>
  1  |.......x| Flip Y-Axis
  1  |......x.| Flip X-Axis
  1  |.....x..| Sprite Enabled
  1  |.x......| Sprite:BG0 Priority?
  1  |x.......| Sprite:BG1 Priority (0=Sprites Infront)
  2  |xxxxxxxx| Sprite Number (high)
  3  |xxxxxxxx| Sprite Number (low)
  4  |........| <Garbage?>
  5  |......xx| Sprite Size (0=8x8; 1=16x16; 2=32x32; 3=64x64)
  5  |xxxx....| Palette Bank
  6  |xxxxxxxx| Y Pos (high)
  7  |xxxxxxxx| Y Pos (low)
  8  |xxxxxxxx| X Pos (high)
  9  |xxxxxxxx| X Pos (low)
10-15|........| <Unused>
-----+--------+----------------------------

*/
