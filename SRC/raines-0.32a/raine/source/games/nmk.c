/******************************************************************************/
/*                                                                            */
/*                  NMK - NIHON MAICON KAIHATSU  UPL - ???                    */
/*   CPU: M68000                                                              */
/* SOUND: M6295x2                                                             */
/* VIDEO: VARIES [1/2 BG0; 1 OBJECT; 1 FG0]                                   */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "nmk.h"
#include "taitosnd.h"
#include "decode.h"
#include "3812intf.h"
#include "debug.h"
#include "sasound.h"		// sample support routines
#include "mame/handlers.h"
/*

About:

 NMK is an abbreviation of "Nihon Maicon Kaihatsu".
 "Nihon" means "Japan",
 "Maicon" is an abbrev. of "Micro Computer", with a Japanese accent,
 "Kaihatsu" means "Development".

 However, NMK no longer develop videogames.

Supported romsets:

0 - Bomb Jack Twin         - NMK [NMK]  9th Mar 1992 / Youichi Koyama
1 - Hacha Mecha Fighter    - NMK [NMK] 19th Sep 1991 / Youichi Koyama
2 - Bio Ship Paladin       - NMK [UPL]          1990 / Itsam Matarca
3 - Mustang                - NMK [UPL] 25th May 1990 / Youichi Koyama
4 - Task Force Harrier     - NMK [SAM]          1989 / ?
5 - Thunder Dragon         - NMK [TCM]  4th Jun 1991 / Youichi Koyama
6 - Thunder Dragon Bootleg - NMK [TCM]  4th Jun 1991 / Youichi Koyama
7 - Strahl                 - NMK [UPL]          1992 / ?
8 - Macross 2              - NMK [BAN] 11th Jun 1992 / Youichi Koyama

Saboten Bombers - NMK - (c) 1992 <-- Undumped??
	Saboten Bombers, Hacha Meche Fighter and Thunder Dragon all use the same
	Dipswitch settings.  Raine reads and uses these in reverse order.
	Bit1 of DSW1 (bit8 in the manual) is unused _most_ of the time. - BaT

Todo:

Hacha Mecha Fighter is protected, has no sound
Bio Ship Paladin is protected, has no sound
Mustang is protected/broken ingame, has no sound
Task Force Harrier is protected, has no sound
Thunder Dragon and bootleg have no sound
Strahl has no sound, possible eeprom in test mode

Atomic Robo Kid     UPL-88013
Omega Fighter       UPL-89016
Task Force Harrier  UPL-89050
USAAF Mustang       UPL-90058
Bio Ship Paladin    UPL-90062

*/

#define ROM_COUNT       9

static int romset;

static struct DIR_INFO bio_ship_paladin_dirs[] =
{
   { "bio_ship_paladin", },
   { "bioship", },
   { "bshippal", },
   { NULL, },
};

static struct ROM_INFO bio_ship_paladin_roms[] =
{
   {            "1", 0x00020000, 0x820ef303, 0, 0, 0, },
   {            "2", 0x00020000, 0xacf56afb, 0, 0, 0, },
   {            "6", 0x00010000, 0x5f39a980, 0, 0, 0, },
   {            "7", 0x00010000, 0x2f3f5a10, 0, 0, 0, },
   {            "8", 0x00010000, 0x75a46fea, 0, 0, 0, },
   {            "9", 0x00010000, 0xd91448ee, 0, 0, 0, },
   {     "sbs-g.01", 0x00080000, 0x21302e78, 0, 0, 0, },
   {     "sbs-g.02", 0x00080000, 0xf31eb668, 0, 0, 0, },
   {     "sbs-g.03", 0x00080000, 0x60e00d7b, 0, 0, 0, },
   {     "sbs-g.04", 0x00080000, 0x7c74cc4e, 0, 0, 0, },
   {     "sbs-g.05", 0x00080000, 0xf0a782e3, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO nmk_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x010000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x010000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x010000, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x010000, 0x04, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x010000, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x010002, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x010002, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x010002, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x010002, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x010002, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x010002, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x010002, 0x40, BIT_ACTIVE_1 },
   { KB_DEF_P1_B4,        MSG_P1_B4,               0x010002, 0x80, BIT_ACTIVE_1 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x010000, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x010003, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x010003, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x010003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x010003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x010003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x010003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x010003, 0x40, BIT_ACTIVE_1 },
   { KB_DEF_P2_B4,        MSG_P2_B4,               0x010003, 0x80, BIT_ACTIVE_1 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_bio_ship_paladin_0[] =
{
   { MSG_SCREEN,              0x01, 0x02 },
   { MSG_NORMAL,              0x01, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_DIFFICULTY,          0x06, 0x04 },
   { MSG_NORMAL,              0x06, 0x00 },
   { MSG_HARD,                0x02, 0x00 },
   { MSG_HARDEST,             0x04, 0x00 },
   { MSG_EASY,                0x00, 0x00 },
   { MSG_TEST_MODE,           0x08, 0x02 },
   { MSG_OFF,                 0x08, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x20, 0x02 },
   { MSG_ON,                  0x20, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { "Lives",                 0xC0, 0x04 },
   { "3",                     0xC0, 0x00 },
   { "5",                     0x40, 0x00 },
   { "4",                     0x80, 0x00 },
   { "2",                     0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_bio_ship_paladin_1[] =
{
   { MSG_COIN2,               0x1C, 0x08 },
   { MSG_1COIN_1PLAY,         0x1C, 0x00 },
   { MSG_1COIN_2PLAY,         0x0C, 0x00 },
   { MSG_1COIN_3PLAY,         0x14, 0x00 },
   { MSG_1COIN_4PLAY,         0x04, 0x00 },
   { MSG_2COIN_1PLAY,         0x18, 0x00 },
   { MSG_3COIN_1PLAY,         0x08, 0x00 },
   { MSG_4COIN_1PLAY,         0x10, 0x00 },
   { MSG_5COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN1,               0xE0, 0x08 },
   { MSG_1COIN_1PLAY,         0xE0, 0x00 },
   { MSG_1COIN_2PLAY,         0x60, 0x00 },
   { MSG_1COIN_3PLAY,         0xA0, 0x00 },
   { MSG_1COIN_4PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0xC0, 0x00 },
   { MSG_3COIN_1PLAY,         0x40, 0x00 },
   { MSG_4COIN_1PLAY,         0x80, 0x00 },
   { MSG_5COIN_1PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO bio_ship_paladin_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_bio_ship_paladin_0 },
   { 0x01000A, 0xFF, dsw_data_bio_ship_paladin_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO nmk_video =
{
   DrawNMK,
   256,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
};

static struct OKIM6295interface m6295_interface =
{
   2,					// 2 chips
   { 22050,
     22050 },				// rate
   { 0,
     0 },		// rom list
   { 100,100 }, // unused volumes
};

static struct SOUND_INFO bomb_jack_twin_sound[] =
{
   { SOUND_M6295,   &m6295_interface,     },
   { 0,             NULL,                 },
};

struct GAME_MAIN game_bio_ship_paladin =
{
   bio_ship_paladin_dirs,
   bio_ship_paladin_roms,
   nmk_inputs,
   bio_ship_paladin_dsw,
   NULL,

   LoadBattleShipP,
   ClearBattleShipP,
   &nmk_video,
   ExecuteNMKFrame,
   "bioship",
   "Bio Ship Paladin",
   "�F�����̓S����",
   COMPANY_ID_UPL,
   "UPL-90062",
   1990,
   NULL, //bomb_jack_twin_sound,
   GAME_SHOOT,
};

static struct DIR_INFO bomb_jack_twin_dirs[] =
{
   { "bomb_jack_twin", },
   { "bjtwin", },
   { "bjt", },
   { NULL, },
};

static struct ROM_INFO bomb_jack_twin_roms[] =
{
   {      "bjt.100", 0x00100000, 0xbb06245d, 0, 0, 0, },
   {      "bjt.127", 0x00100000, 0x8da67808, 0, 0, 0, },
   {      "bjt.130", 0x00100000, 0x372d46dd, 0, 0, 0, },
   {       "bjt.32", 0x00100000, 0x8a4f26d0, 0, 0, 0, },
   {       "bjt.35", 0x00010000, 0xaa13df7c, 0, 0, 0, },
   {       "bjt.76", 0x00040000, 0x7cd4e72a, 0, 0, 0, },
   {       "bjt.77", 0x00040000, 0x7830a465, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct DSW_DATA dsw_data_bomb_jack_twin_0[] =
{
   { MSG_SCREEN,              0x01, 0x02 },
   { MSG_NORMAL,              0x01, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { "Start Level",           0x0E, 0x08 },
   { "Japan",                 0x0E, 0x00 },
   { "Nevada",                0x0C, 0x00 },
   { "England",               0x0A, 0x00 },
   { "Germany",               0x08, 0x00 },
   { "Korea",                 0x06, 0x00 },
   { "Thailand",              0x04, 0x00 },
   { "Hong Kong",             0x02, 0x00 },
   { "China",                 0x00, 0x00 },
   { MSG_DIFFICULTY,          0x30, 0x04 },
   { MSG_NORMAL,              0x30, 0x00 },
   { MSG_EASY,                0x20, 0x00 },
   { MSG_HARD,                0x10, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Lives",                 0xC0, 0x04 },
   { "3",                     0xC0, 0x00 },
   { "4",                     0x80, 0x00 },
   { "2",                     0x40, 0x00 },
   { "1",                     0x00, 0x00 },
   { NULL,                    0,    0,   },
};

struct DSW_DATA dsw_data_bomb_jack_twin_1[] =
{
   { MSG_DEMO_SOUND,          0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_COIN2,               0x1C, 0x08 },
   { MSG_1COIN_1PLAY,         0x1C, 0x00 },
   { MSG_1COIN_2PLAY,         0x0C, 0x00 },
   { MSG_1COIN_3PLAY,         0x14, 0x00 },
   { MSG_1COIN_4PLAY,         0x04, 0x00 },
   { MSG_2COIN_1PLAY,         0x18, 0x00 },
   { MSG_3COIN_1PLAY,         0x08, 0x00 },
   { MSG_4COIN_1PLAY,         0x10, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { MSG_COIN1,               0xE0, 0x08 },
   { MSG_1COIN_1PLAY,         0xE0, 0x00 },
   { MSG_1COIN_2PLAY,         0x60, 0x00 },
   { MSG_1COIN_3PLAY,         0xA0, 0x00 },
   { MSG_1COIN_4PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0xC0, 0x00 },
   { MSG_3COIN_1PLAY,         0x40, 0x00 },
   { MSG_4COIN_1PLAY,         0x80, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO bomb_jack_twin_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_bomb_jack_twin_0 },
   { 0x01000A, 0xFF, dsw_data_bomb_jack_twin_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO bomb_jack_twin_video =
{
   DrawBombJackTwin,
   384,
   224,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_bomb_jack_twin =
{
   bomb_jack_twin_dirs,
   bomb_jack_twin_roms,
   nmk_inputs,
   bomb_jack_twin_dsw,
   NULL,

   LoadBombJackTwin,
   ClearBombJackTwin,
   &bomb_jack_twin_video,
   ExecuteNMKFrame,
   "bjtwin",
   "Bomb Jack Twin",
   "�{���W���b�N�c�C��",
   COMPANY_ID_NMK,
   NULL,
   1993,
   bomb_jack_twin_sound,
   GAME_PLATFORM,
};

static struct DIR_INFO saboten_bombers_dirs[] =
{
   { "saboten_bombers", },
   { "sabotenb", },
   { NULL, },
};

static struct ROM_INFO saboten_bombers_roms[] =
{
   {    "ic100.sb5", 0x00200000, 0xb20f166e, 0, 0, 0, },
   {     "ic27.sb7", 0x00100000, 0x43e33a7e, 0, 0, 0, },
   {     "ic30.sb6", 0x00100000, 0x288407af, 0, 0, 0, },
   {     "ic32.sb4", 0x00200000, 0x24c62205, 0, 0, 0, },
   {     "ic35.sb3", 0x00010000, 0xeb7bc99d, 0, 0, 0, },
   {     "ic75.sb2", 0x00040000, 0x367e87b7, 0, 0, 0, },
   {     "ic76.sb1", 0x00040000, 0xb2b0b2cf, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct DSW_DATA dsw_data_saboten_bombers_0[] =
{
   { MSG_SCREEN,              0x01, 0x02 },
   { MSG_NORMAL,              0x01, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { "Language",              0x02, 0x02 },
   { "Japanese",              0x02, 0x00 },
   { "English",               0x00, 0x00 },
   { MSG_DIFFICULTY,          0x0c, 0x04 },
   { MSG_NORMAL,              0x0c, 0x00 },
   { MSG_EASY,                0x08, 0x00 },
   { MSG_HARD,                0x04, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Lives",                 0xC0, 0x04 },
   { "3",                     0xC0, 0x00 },
   { "2",                     0x80, 0x00 },
   { "4",                     0x40, 0x00 },
   { "1",                     0x00, 0x00 },
   { NULL,                    0,    0,   },
};

struct DSW_DATA dsw_data_saboten_bombers_1[] =
{
   { MSG_DEMO_SOUND,          0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_COIN1,               0xE0, 0x08 }, // Looks ugly and out of order, but it's correct!
   { MSG_1COIN_1PLAY,         0xE0, 0x00 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x40, 0x00 },
   { MSG_1COIN_4PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x60, 0x00 },
   { MSG_3COIN_1PLAY,         0xA0, 0x00 },
   { MSG_4COIN_1PLAY,         0x20, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { MSG_COIN2,               0x1C, 0x08 },
   { MSG_1COIN_1PLAY,         0x1C, 0x00 },
   { MSG_1COIN_2PLAY,         0x18, 0x00 },
   { MSG_1COIN_3PLAY,         0x08, 0x00 },
   { MSG_1COIN_4PLAY,         0x10, 0x00 },
   { MSG_2COIN_1PLAY,         0x0c, 0x00 },
   { MSG_3COIN_1PLAY,         0x14, 0x00 },
   { MSG_4COIN_1PLAY,         0x04, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO saboten_bombers_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_saboten_bombers_0 },
   { 0x01000A, 0xFF, dsw_data_saboten_bombers_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO saboten_bombers_video =
{
   DrawBombJackTwin,
   384,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_saboten_bombers =
{
   saboten_bombers_dirs,
   saboten_bombers_roms,
   nmk_inputs,
   saboten_bombers_dsw,
   NULL,

   LoadSabotenBombers,
   ClearBombJackTwin,
   &saboten_bombers_video,
   ExecuteNMKFrame,
   "sabotenb",
   "Saboten Bombers",
   "",
   COMPANY_ID_NMK,
   NULL,
   1992,
   bomb_jack_twin_sound,
   GAME_PLATFORM | GAME_PARTIALLY_WORKING,
};

static struct DIR_INFO macross_2_dirs[] =
{
   { "macross", },
   { "macross2", },
   { "sdf_macross", },
   { NULL, },
};

static struct ROM_INFO macross_2_roms[] =
{
   {       "921a01", 0x00020000, 0xbbd8242d, 0, 0, 0, },
   {       "921a02", 0x00010000, 0x77c082c7, 0, 0, 0, },
   {       "921a03", 0x00080000, 0x33318d55, 0, 0, 0, },
   {       "921a04", 0x00200000, 0x4002e4bb, 0, 0, 0, },
   {       "921a05", 0x00080000, 0xd5a1eddd, 0, 0, 0, },
   {       "921a06", 0x00080000, 0x89461d0f, 0, 0, 0, },
   {       "921a07", 0x00200000, 0x7d2bf112, 0, 0, 0, },
   {       "921a08", 0x00000100, 0xcfdbb86c, 0, 0, 0, },
   {       "921a09", 0x00000100, 0x633ab1c9, 0, 0, 0, },
   {       "921a10", 0x00000020, 0x8371e42d, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct DSW_DATA dsw_data_macross_2_0[] =
{
   { "Service",              0x01, 0x02 },
   { MSG_OFF,                0x01, 0x00 },
   { MSG_ON,                 0x00, 0x00 },
 
 	 { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
 
 	 { MSG_DSWA_BIT3,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
 
 	 { "Langage",               0x08, 0x02 },
   { "Japenese",                 0x08, 0x00 },
   { "English",                  0x00, 0x00 },
 
 	 { MSG_DSWA_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
 
 	 { MSG_DSWA_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
 
 	 { "Lives",                 0xC0, 0x04 },
   { "3",                     0xC0, 0x00 },
   { "4",                     0x80, 0x00 },
 
 	 { "2",                     0x40, 0x00 },
   { "1",                     0x00, 0x00 },
   { NULL,                    0,    0,   },
};

struct DSW_DATA dsw_data_macross_2_1[] =
{
	{ MSG_COIN1,               0xf0, 0x0f },
   { MSG_1COIN_1PLAY,         0xF0, 0x00 },
   { MSG_1COIN_2PLAY,         0x70, 0x00 },
	 { MSG_1COIN_3PLAY,         0xB0, 0x00 },
   { MSG_1COIN_4PLAY,         0x30, 0x00 },
   { MSG_1COIN_5PLAY,         0xD0, 0x00 },
	 { MSG_1COIN_6PLAY,         0x50, 0x00 },
	 { MSG_1COIN_7PLAY,         0x90, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
	 { MSG_2COIN_3PLAY,         0xE0, 0x00 },
   { MSG_2COIN_5PLAY,         0x60, 0x00 },
   { MSG_3COIN_1PLAY,         0xA0, 0x00 },
	 { MSG_3COIN_2PLAY,         0x20, 0x00 },
   { MSG_3COIN_4PLAY,         0xC0, 0x00 },
   { MSG_4COIN_1PLAY,         0x40, 0x00 },
	 { MSG_4COIN_3PLAY,         0x80, 0x00 },
	 { MSG_FREE_PLAY,              0x00, 0x00 },

	 { MSG_COIN2,               0x0f, 0x0f },
 	 { MSG_1COIN_1PLAY,         0x0F, 0x00 },
   { MSG_1COIN_2PLAY,         0x07, 0x00 },
   { MSG_1COIN_3PLAY,         0x0B, 0x00 },
 	 { MSG_1COIN_4PLAY,         0x03, 0x00 },
   { MSG_1COIN_5PLAY,         0x0D, 0x00 },
   { MSG_1COIN_6PLAY,         0x05, 0x00 },
 	 { MSG_1COIN_7PLAY,         0x09, 0x00 },
   { MSG_2COIN_1PLAY,         0x01, 0x00 },
   { MSG_2COIN_3PLAY,         0x0E, 0x00 },
 	 { MSG_2COIN_5PLAY,         0x06, 0x00 },
   { MSG_3COIN_1PLAY,         0x0A, 0x00 },
   { MSG_3COIN_2PLAY,         0x02, 0x00 },
 	 { MSG_3COIN_4PLAY,         0x0C, 0x00 },
   { MSG_4COIN_1PLAY,         0x04, 0x00 },
   { MSG_4COIN_3PLAY,         0x08, 0x00 },
 	 { MSG_FREE_PLAY,              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO macross_2_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_macross_2_0 },
   { 0x01000A, 0xFF, dsw_data_macross_2_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO macross_2_video =
{
   draw_macross_2,
   256,
   224,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_macross_2 =
{
   macross_2_dirs,
   macross_2_roms,
   nmk_inputs,
   macross_2_dsw,
   NULL,

   load_macross_2,
   clear_macross_2,
   &macross_2_video,
   ExecuteNMKFrame,
   "macross",
   "SDF Macross",
   NULL,
   COMPANY_ID_NMK,
   NULL,
   1992,
   NULL, // bomb_jack_twin_sound,
   GAME_SHOOT,
};

static struct DIR_INFO hacha_mecha_fighter_dirs[] =
{
   { "hacha_mecha_fighter", },
   { "hachamf", },
   { NULL, },
};

static struct ROM_INFO hacha_mecha_fighter_roms[] =
{
   {   "hmf_02.rom", 0x00080000, 0x3f1e67f2, 0, 0, 0, },
   {   "hmf_01.rom", 0x00010000, 0x9e6f48fc, 0, 0, 0, },
   {   "hmf_03.rom", 0x00080000, 0xb25ed93b, 0, 0, 0, },
   {   "hmf_04.rom", 0x00080000, 0x05a624e3, 0, 0, 0, },
   {   "hmf_05.rom", 0x00020000, 0x29fb04a2, 0, 0, 0, },
   {   "hmf_06.rom", 0x00020000, 0xde6408a0, 0, 0, 0, },
   {   "hmf_07.rom", 0x00020000, 0x9d847c31, 0, 0, 0, },
   {   "hmf_08.rom", 0x00100000, 0x7fd0f556, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct DSW_DATA dsw_data_hacha_mecha_fighter_0[] =
{
   { MSG_SCREEN,              0x01, 0x02 },
   { MSG_NORMAL,              0x01, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { "Language",              0x02, 0x02 },
   { "Japanese",              0x02, 0x00 },
   { "English",               0x00, 0x00 },
   { MSG_DIFFICULTY,          0x0c, 0x04 },
   { MSG_NORMAL,              0x0c, 0x00 },
   { MSG_EASY,                0x08, 0x00 },
   { MSG_HARD,                0x04, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Lives",                 0xC0, 0x04 },
   { "3",                     0xC0, 0x00 },
   { "2",                     0x80, 0x00 },
   { "4",                     0x40, 0x00 },
   { "1",                     0x00, 0x00 },
   { NULL,                    0,    0,   },
};

struct DSW_DATA dsw_data_hacha_mecha_fighter_1[] =
{
   { MSG_DEMO_SOUND,          0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_COIN1,               0xE0, 0x08 }, // Looks ugly and out of order, but it's correct!
   { MSG_1COIN_1PLAY,         0xE0, 0x00 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x40, 0x00 },
   { MSG_1COIN_4PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x60, 0x00 },
   { MSG_3COIN_1PLAY,         0xA0, 0x00 },
   { MSG_4COIN_1PLAY,         0x20, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { MSG_COIN2,               0x1C, 0x08 },
   { MSG_1COIN_1PLAY,         0x1C, 0x00 },
   { MSG_1COIN_2PLAY,         0x18, 0x00 },
   { MSG_1COIN_3PLAY,         0x08, 0x00 },
   { MSG_1COIN_4PLAY,         0x10, 0x00 },
   { MSG_2COIN_1PLAY,         0x0c, 0x00 },
   { MSG_3COIN_1PLAY,         0x14, 0x00 },
   { MSG_4COIN_1PLAY,         0x04, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { NULL,                    0,    0,   },
};


static struct DSW_INFO hacha_mecha_fighter_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_hacha_mecha_fighter_0 },
   { 0x010009, 0xFF, dsw_data_hacha_mecha_fighter_1 },
   { 0,        0,    NULL,      },
};

struct GAME_MAIN game_hacha_mecha_fighter =
{
   hacha_mecha_fighter_dirs,
   hacha_mecha_fighter_roms,
   nmk_inputs,
   hacha_mecha_fighter_dsw,
   NULL,

   LoadHachaMechaFighter,
   ClearHachaMechaFighter,
   &nmk_video,
   ExecuteNMKFrame,
   "hachamf",
   "Hacha Mecha Fighter",
   NULL,
   COMPANY_ID_NMK,
   NULL,
   1991,
   bomb_jack_twin_sound,
   GAME_NOT_WORKING,
};

static struct DIR_INFO mustang_dirs[] =
{
   { "mustang", },
   { NULL, },
};

static struct ROM_INFO mustang_roms[] =
{
   {   "mustang.01", 0x00020000, 0xd13f0722, 0, 0, 0, },
   {   "mustang.02", 0x00020000, 0x87c1fb43, 0, 0, 0, },
   {   "mustang.03", 0x00020000, 0x23d03ad5, 0, 0, 0, },
   {   "mustang.04", 0x00020000, 0xa62b2f87, 0, 0, 0, },
   {   "mustang.05", 0x00020000, 0x932d3e33, 0, 0, 0, },
   {   "mustang.06", 0x00020000, 0x54773f95, 0, 0, 0, },
   {   "mustang.07", 0x00020000, 0x42a6cfc2, 0, 0, 0, },
   {   "mustang.08", 0x00020000, 0x9d3bee66, 0, 0, 0, },
   {   "mustang.09", 0x00020000, 0x5f8fdfb1, 0, 0, 0, },
   {   "mustang.10", 0x00020000, 0xb3dd5243, 0, 0, 0, },
   {   "mustang.11", 0x00020000, 0xc6c9752f, 0, 0, 0, },
   {   "mustang.12", 0x00020000, 0x39757d6a, 0, 0, 0, },
   {   "mustang.13", 0x00020000, 0xd8ccce31, 0, 0, 0, },
   {   "mustang.14", 0x00020000, 0x13c6363b, 0, 0, 0, },
   {   "mustang.15", 0x00020000, 0x81ccfcad, 0, 0, 0, },
   {   "mustang.16", 0x00010000, 0x99ee7505, 0, 0, 0, },
   {   "mustang.17", 0x00010000, 0xf6f6c4bf, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct DSW_INFO mustang_dsw[] =
{
   { 0x010005, 0xFF, dsw_data_default_0 },
   { 0x010004, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO mustang_video =
{
   DrawMustang,
   256,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_mustang =
{
   mustang_dirs,
   mustang_roms,
   nmk_inputs,
   mustang_dsw,
   NULL,

   LoadMustang,
   ClearMustang,
   &mustang_video,
   ExecuteNMKFrame,
   "mustang",
   "USAAF Mustang",
   NULL,
   COMPANY_ID_UPL,
   "UPL-90058",
   1990,
   NULL,
   GAME_SHOOT | GAME_NOT_WORKING,
};

static struct DIR_INFO strahl_dirs[] =
{
   { "strahl", },
   { NULL, },
};

static struct ROM_INFO strahl_roms[] =
{
   { "str6b1w1.776", 0x00080000, 0xbb1bb155, 0, 0, 0, },
   { "str7b2r0.275", 0x00040000, 0x5769e3e1, 0, 0, 0, },
   { "str8pmw1.540", 0x00080000, 0x01d6bb6a, 0, 0, 0, },
   { "str9pew1.639", 0x00080000, 0x6bb3eb9f, 0, 0, 0, },
   {  "strahl-1.83", 0x00020000, 0xafc3c4d6, 0, 0, 0, },
   {  "strahl-2.82", 0x00020000, 0xc9d008ae, 0, 0, 0, },
   {  "strahl-3.73", 0x00010000, 0x2273b33e, 0, 0, 0, },
   {  "strahl-4.66", 0x00010000, 0x60a799c4, 0, 0, 0, },
   {  "strl3-01.32", 0x00080000, 0xd8337f15, 0, 0, 0, },
   {  "strl4-02.57", 0x00080000, 0x2a38552b, 0, 0, 0, },
   {  "strl5-03.58", 0x00080000, 0xa0e7d210, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct DSW_DATA dsw_data_strahl_1[] =
{
   { MSG_DSWB_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT3,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT4,           0x08, 0x02 },
   { MSG_OFF,                 0x08, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO strahl_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_default_0 },
   { 0x01000A, 0xFF, dsw_data_strahl_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO strahl_video =
{
   DrawStrahl,
   256,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_strahl =
{
   strahl_dirs,
   strahl_roms,
   nmk_inputs,
   strahl_dsw,
   NULL,

   LoadStrahl,
   ClearStrahl,
   &strahl_video,
   ExecuteNMKFrame,
   "strahl",
   "Strahl",
   "�V���g���[��",
   COMPANY_ID_UPL,
   NULL,
   1992,
   NULL,
   GAME_SHOOT,
};

static struct DIR_INFO task_force_harrier_dirs[] =
{
   { "task_force_harrier", },
   { "tforceh", },
   { "tskfrceh", },
   { NULL, },
};

static struct ROM_INFO task_force_harrier_roms[] =
{
   {            "1", 0x00010000, 0xc7402e4a, 0, 0, 0, },
   {            "2", 0x00020000, 0x78923aaa, 0, 0, 0, },
   {            "3", 0x00020000, 0x99cea259, 0, 0, 0, },
   {           "12", 0x00010000, 0xb959f837, 0, 0, 0, },
   {      "89050-4", 0x00080000, 0x64d7d687, 0, 0, 0, },
   {      "89050-8", 0x00080000, 0x11ee4c39, 0, 0, 0, },
   {     "89050-10", 0x00080000, 0x893552ab, 0, 0, 0, },
   {     "89050-13", 0x00080000, 0x24db3fa4, 0, 0, 0, },
   {     "89050-17", 0x00080000, 0x7f715421, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct DSW_INFO task_force_harrier_dsw[] =
{
   { 0x010005, 0xFF, dsw_data_default_0 },
   { 0x010004, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO task_force_harrier_video =
{
   DrawTaskForceHarrier,
   256,
   224,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_task_force_harrier =
{
   task_force_harrier_dirs,
   task_force_harrier_roms,
   nmk_inputs,
   task_force_harrier_dsw,
   NULL,

   LoadTaskForceHarrier,
   ClearTaskForceHarrier,
   &task_force_harrier_video,
   ExecuteNMKFrame,
   "tforceh",
   "Task Force Harrier",
   NULL,
   COMPANY_ID_UPL,
   "UPL-89050",
   1989,
   NULL,
   GAME_SHOOT | GAME_NOT_WORKING,
};

static struct DIR_INFO thunder_dragon_dirs[] =
{
   { "thunder_dragon", },
   { "tdragon", },
   { "thunderd", },
   { NULL, },
};

static struct ROM_INFO thunder_dragon_roms[] =
{
   {      "thund.1", 0x00010000, 0xbf493d74, 0, 0, 0, },
   {      "thund.2", 0x00080000, 0xecfea43e, 0, 0, 0, },
   {      "thund.3", 0x00080000, 0xae6875a8, 0, 0, 0, },
   {      "thund.4", 0x00100000, 0x3eedc2fe, 0, 0, 0, },
   {      "thund.5", 0x00100000, 0xd0bde826, 0, 0, 0, },
   {      "thund.6", 0x00020000, 0xfe365920, 0, 0, 0, },
   {      "thund.7", 0x00020000, 0x52192fe5, 0, 0, 0, },
   {      "thund.8", 0x00020000, 0xedd02831, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct DSW_DATA dsw_data_thunder_dragon_0[] =
{
   { MSG_SCREEN,              0x01, 0x02 },
   { MSG_NORMAL,              0x01, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { "Language",              0x02, 0x02 },
   { "Japanese",              0x02, 0x00 },
   { "English",               0x00, 0x00 },
   { MSG_DIFFICULTY,          0x0c, 0x04 },
   { MSG_NORMAL,              0x0c, 0x00 },
   { MSG_EASY,                0x08, 0x00 },
   { MSG_HARD,                0x04, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Lives",                 0xC0, 0x04 },
   { "3",                     0xC0, 0x00 },
   { "2",                     0x80, 0x00 },
   { "4",                     0x40, 0x00 },
   { "1",                     0x00, 0x00 },
   { NULL,                    0,    0,   },
};

struct DSW_DATA dsw_data_thunder_dragon_1[] =
{
   { MSG_DEMO_SOUND,          0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_COIN1,               0xE0, 0x08 }, // Looks ugly and out of order, but it's correct!
   { MSG_1COIN_1PLAY,         0xE0, 0x00 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x40, 0x00 },
   { MSG_1COIN_4PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x60, 0x00 },
   { MSG_3COIN_1PLAY,         0xA0, 0x00 },
   { MSG_4COIN_1PLAY,         0x20, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { MSG_COIN2,               0x1C, 0x08 },
   { MSG_1COIN_1PLAY,         0x1C, 0x00 },
   { MSG_1COIN_2PLAY,         0x18, 0x00 },
   { MSG_1COIN_3PLAY,         0x08, 0x00 },
   { MSG_1COIN_4PLAY,         0x10, 0x00 },
   { MSG_2COIN_1PLAY,         0x0c, 0x00 },
   { MSG_3COIN_1PLAY,         0x14, 0x00 },
   { MSG_4COIN_1PLAY,         0x04, 0x00 },
   { MSG_FREE_PLAY,              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO thunder_dragon_dsw[] =
{
   { 0x010008, 0xFF, dsw_data_thunder_dragon_0 },
   { 0x01000A, 0xFF, dsw_data_thunder_dragon_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO thunder_dragon_video =
{
   DrawThunderDragon,
   256,
   224,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE,
};

struct GAME_MAIN game_thunder_dragon =
{
   thunder_dragon_dirs,
   thunder_dragon_roms,
   nmk_inputs,
   thunder_dragon_dsw,
   NULL,

   LoadThunderDragon,
   ClearThunderDragon,
   &thunder_dragon_video,
   ExecuteNMKFrame,
   "tdragon",
   "Thunder Dragon",
   "����",
   COMPANY_ID_NMK,
   NULL,
   1991,
   NULL,
   GAME_SHOOT,
};

static struct DIR_INFO thunder_dragon_bl_dirs[] =
{
   { "thunder_dragon_bl", },
   { "thndrdbl", },
   { "tdragonb", },
   { "thunderd", },
   { ROMOF("tdragon"), },
   { CLONEOF("tdragon"), },
   { NULL, },
};

static struct ROM_INFO thunder_dragon_bl_roms[] =
{
   {    "td_01.bin", 0x00010000, 0xf6f6c4bf, 0, 0, 0, },
   {    "td_02.bin", 0x00010000, 0x99ee7505, 0, 0, 0, },
   {    "td_03.bin", 0x00020000, 0x2fa1aa04, 0, 0, 0, },
   {    "td_04.bin", 0x00020000, 0xe8a62d3e, 0, 0, 0, },
   {    "td_06.bin", 0x00080000, 0xc1be8a4d, 0, 0, 0, },
   {    "td_07.bin", 0x00080000, 0x2c3e371f, 0, 0, 0, },
   {    "td_08.bin", 0x00020000, 0x5144dc69, 0, 0, 0, },
   {    "td_09.bin", 0x00080000, 0xb6e074eb, 0, 0, 0, },
   {    "td_10.bin", 0x00080000, 0xbfd0ec5d, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

/*
static struct YM3812interface ym3812_interface =
{
   1,		// 1 chip
   3000000,	// 3 MHz  (emu only)
   { 255 },	// Volume (emu only)
   { NULL }
};

static struct SOUND_INFO thunder_dragon_bl_sound[] =
{
   { SOUND_YM3812,  &ym3812_interface,    },
   { 0,             NULL,                 },
};
*/

struct GAME_MAIN game_thunder_dragon_bl =
{
   thunder_dragon_bl_dirs,
   thunder_dragon_bl_roms,
   nmk_inputs,
   thunder_dragon_dsw,
   NULL,

   LoadThunderDragonBl,
   ClearThunderDragonBl,
   &thunder_dragon_video,
   ExecuteNMKFrame,
   "tdragonb",
   "Thunder Dragon (bootleg)",
   "���� (bootleg)",
   COMPANY_ID_BOOTLEG,
   NULL,
   1991,
   NULL, // Sound does not work and triggers loads of warnings !!!
   //   thunder_dragon_bl_sound,
   GAME_SHOOT,
};

static int scr_x;
static int scr_y;

static UINT8 *RAM_COL;

static UINT8 *GFX_FG0;
static UINT8 *FG0_Mask;

static UINT8 *GFX_SPR;
static UINT8 *SPR_Mask;

static UINT8 *GFX_BG1;
static UINT8 *BG1_Mask;

static UINT8 *GFX_BG0;
static UINT8 *BG0_Mask;

/**********************************************************/

static int sport;

static void WriteSound68k(UINT32 address, UINT8 data)
{
   if(data&0xFF){
   sport=data&0xFF;
   #ifdef RAINE_DEBUG
      print_debug("68000 Sends:%02x\n",data);
   #endif
   cpu_int_nmi(CPU_Z80_0);
   }
}

static UINT16 TDSoundReadZ80(UINT16 offset)
{
   UINT8 ta;

   switch(offset&0x1F){
      case 0x08:		// YM3812 TimerA/B
         return 0x60;
      break;
      case 0x10:
      case 0x11:
      case 0x12:
      //   return 0xFE;
      //break;
      case 0x13:
         #ifdef RAINE_DEBUG
         print_debug("Z80 Receives:%02x\n",sport);
         #endif
         ta = sport;
         sport = 0;
         return ta;
      break;
      default:
         ta = 0xFF;
      break;
   }

   #ifdef RAINE_DEBUG
   print_debug("Z80Read:%04x,%02x [%04x]\n",offset,ta,z80pc);
   #endif
   return ta;
}

static void TDSoundWriteZ80(UINT16 offset, UINT8 data)
{
   switch(offset&0x1F){
      case 0x08:
      case 0x09:
         YM3812WriteZ80( (UINT16) (offset&1), data);
      break;
      default:
         #ifdef RAINE_DEBUG
         print_debug("Z80Write:%04x,%02x [%04x]\n",offset,data,z80pc);
         #endif
      break;
   }
}

/**********************************************************/

static UINT8 input_mode;

WRITE_HANDLER( bjtwin_oki6295_bankswitch_w )
{
  if ((data & 0x00ff0000) == 0)
	{
	  switch (offset&0xf)
		{
			case 0x00:	OKIM6295_set_bank_base(0, 0, (data & 0x0f) * 0x10000);	break;
			case 0x02:	OKIM6295_set_bank_base(0, 1, (data & 0x0f) * 0x10000);	break;
			case 0x04:	OKIM6295_set_bank_base(0, 2, (data & 0x0f) * 0x10000);	break;
			case 0x06:	OKIM6295_set_bank_base(0, 3, (data & 0x0f) * 0x10000);	break;
			case 0x08:	OKIM6295_set_bank_base(1, 0, (data & 0x0f) * 0x10000);	break;
			case 0x0a:	OKIM6295_set_bank_base(1, 1, (data & 0x0f) * 0x10000);	break;
			case 0x0c:	OKIM6295_set_bank_base(1, 2, (data & 0x0f) * 0x10000);	break;
			case 0x0e:	OKIM6295_set_bank_base(1, 3, (data & 0x0f) * 0x10000);	break;
		}
	}
}

static void NMKVideoWW(UINT32 addr, UINT16 data)
{
   static UINT8 command[2];
   addr &= 0x7FFF;

   WriteWord(&RAM[0x10000+addr],data);

   if((addr>=0x4000)&&(addr<0x4100)){

   data &= 0x00FF;

   switch(addr){
      case 0x4000:
         if(!(command[0]&0x80)){
            command[0] = 0x80;
         }
         else{
            if(data&0x80){
               command[0] = data&0x7F;
               data &= ~0x60;
            }
         }
	 //fprintf(stderr,"BAD\n");
	 OKIM6295_data_0_w(0,data & 0xff);
         //M6295buffer_request(0,data&0xFF);
      break;
      case 0x4010:
         if(!(command[1]&0x80)){
            command[1] = 0x80;
         }
         else{
            if(data&0x80){
               command[1] = data&0x7F;
               data &= ~0x60;
            }
         }
	 //fprintf(stderr,"BAD\n");
	 OKIM6295_data_1_w(1,data & 0xff);
         //M6295buffer_request(1,data&0xFF);
      break;
      default:
	if (addr>=0x4020 && addr<=0x402f)
	  bjtwin_oki6295_bankswitch_w(addr,data);
	  //PCMROM[((addr&0x0008)>>3)*0x100000+((addr&0x0006)>>1)*0x10000]=data&0xf;
	//m6295_bank[(addr&0x0008)>>3][(addr&0x0006)>>1] = data & 0x0F;
      break;
   }

   }
   if(romset==6){
      if(addr==0x001E){
               #ifdef RAINE_DEBUG
               print_ingame(60,"sound_write:%04x",data);
               print_debug("sound_write:%04x\n",data);
               #endif
               WriteSound68k(addr, (UINT8) (data&0xFF) );
      }
   }
}

static void NMKVideoWB(UINT32 addr, UINT8 data)
{
   addr &= 0x7FFF;

   RAM[0x10000+(addr^1)]=data;

   if(romset==4){
      if(addr==0x0013){
         switch(data&0x1F){
            case 0x11:
               input_mode = 0;
            break;
            case 0x12:
               input_mode = 1;
            break;
            case 0x13:
               input_mode = 2;
            break;
            case 0x14:
               input_mode = 3;
            break;
            default:
               #ifdef RAINE_DEBUG
               print_debug("input_write:%02x\n",data&0x1F);
               #endif
            break;
         }
      }
   }
   if(romset==6){
      if(addr==0x001F){
               #ifdef RAINE_DEBUG
               print_ingame(60,"sound_write:%02x",data);
               print_debug("sound_write:%02x\n",data);
               #endif
               WriteSound68k(addr, (UINT8) (data&0xFF) );
      }
   }
}

static UINT16 NMKVideoRW(UINT32 addr)
{
   addr&=0x7FFF;

   if(romset==4){
      if(addr==0x0002){
         switch(input_mode){
            case 0:
               return ~ReadWord(&RAM[0x10004]);	// DSW
            break;
            case 1:
               return ~ReadWord(&RAM[0x10002]);	// PLAYER
            break;
            case 2:
               return ~ReadWord(&RAM[0x10000]);	// COIN
            break;
            case 3:
               return ~ReadWord(&RAM[0x10002]);	// PLAYER
            break;
            default:
               return 0;
            break;
         }
      }
   }

   switch(addr){
      case 0x4000:
	//fprintf(stderr,"BAD\n");
	return OKIM6295_status_0_r( 0 );
	//return M6295buffer_status(0);
         //return 0;
      break;
      case 0x4010:
	//fprintf(stderr,"BAD\n");
	return OKIM6295_status_1_r( 1 );
	//return M6295buffer_status(1);
         //return 0;
      break;
      default:
         //if(addr>0x4000) print_debug("RW:%06x\n",addr);
         return ReadWord(&RAM[0x10000+addr]);
      break;
   }
   return 0;
}

int NMKDecodeFG0(UINT8 *src, UINT32 size)
{
   UINT32 ta,tb;

   if(!(GFX_FG0=AllocateMem(0x40000))) return 0;
   memset(GFX_FG0,0x00,0x40000);

   tb=0;
   for(ta=0;ta<size;ta++,tb+=2){
      GFX_FG0[tb+0]=(src[ta]>>4)^15;
      GFX_FG0[tb+1]=(src[ta]&15)^15;
   }

   FG0_Mask = make_solid_mask_8x8(GFX_FG0, 0x1000);

   return 1;
}

int NMKDecodeSPR(UINT8 *src, UINT32 size)
{
   UINT32 ta,tb;

   if(!(GFX_SPR=AllocateMem(size<<1))) return 0;

   tb=0;
   for(ta=0;ta<size;ta+=4){
      GFX_SPR[tb+0]=(src[ta+0]>>4)^15;
      GFX_SPR[tb+1]=(src[ta+0]&15)^15;
      GFX_SPR[tb+2]=(src[ta+1]>>4)^15;
      GFX_SPR[tb+3]=(src[ta+1]&15)^15;
      GFX_SPR[tb+4]=(src[ta+2]>>4)^15;
      GFX_SPR[tb+5]=(src[ta+2]&15)^15;
      GFX_SPR[tb+6]=(src[ta+3]>>4)^15;
      GFX_SPR[tb+7]=(src[ta+3]&15)^15;
      tb+=16;
      if((tb&0xFF)==0){tb-=0xF8;}
      else{if((tb&0xFF)==8){tb-=8;}}
   }


   SPR_Mask = make_solid_mask_pad_16x16(GFX_SPR, (size<<1)>>8, 0x10000);

   return 1;
}

int NMKDecodeSPR_Mustang(UINT8 *src, UINT32 size)
{
   int ta,tb,ss;

   ss=size/2;

   if(!(GFX_SPR=AllocateMem(size<<1))) return 0;

   tb=0;
   for(ta=0;ta<ss;ta+=2){
      GFX_SPR[tb+0]=(src[   ta+0]>>4)^15;
      GFX_SPR[tb+1]=(src[   ta+0]&15)^15;
      GFX_SPR[tb+2]=(src[ss+ta+0]>>4)^15;
      GFX_SPR[tb+3]=(src[ss+ta+0]&15)^15;
      GFX_SPR[tb+4]=(src[   ta+1]>>4)^15;
      GFX_SPR[tb+5]=(src[   ta+1]&15)^15;
      GFX_SPR[tb+6]=(src[ss+ta+1]>>4)^15;
      GFX_SPR[tb+7]=(src[ss+ta+1]&15)^15;
      tb+=16;
      if((tb&0xFF)==0){tb-=0xF8;}
      else{if((tb&0xFF)==8){tb-=8;}}
   }

   SPR_Mask = make_solid_mask_pad_16x16(GFX_SPR, (size<<1)>>8, 0x10000);

   return 1;
}

int NMKDecodeBG1(UINT8 *src, UINT32 size)
{
   UINT32 ta,tb;

   if(!(GFX_BG1=AllocateMem(0x100000))) return(0);
   memset(GFX_BG1,0x00,0x100000);

   tb=0;
   for(ta=0;ta<size;ta+=4){
      GFX_BG1[tb+0]=(src[ta+0]>>4)^15;
      GFX_BG1[tb+1]=(src[ta+0]&15)^15;
      GFX_BG1[tb+2]=(src[ta+1]>>4)^15;
      GFX_BG1[tb+3]=(src[ta+1]&15)^15;
      GFX_BG1[tb+4]=(src[ta+2]>>4)^15;
      GFX_BG1[tb+5]=(src[ta+2]&15)^15;
      GFX_BG1[tb+6]=(src[ta+3]>>4)^15;
      GFX_BG1[tb+7]=(src[ta+3]&15)^15;
      tb+=16;
      if((tb&0xFF)==0){tb-=0xF8;}
      else{if((tb&0xFF)==8){tb-=8;}}
   }

   BG1_Mask = make_solid_mask_16x16(GFX_BG1, 0x1000);

   return 1;
}

int NMKDecodeBG0(UINT8 *src, UINT32 size)
{
   UINT32 ta,tb;

   if(size <= 0x100000){
   if(!(GFX_BG0=AllocateMem(0x100000))) return(0);
   memset(GFX_BG0,0x00,0x100000);
   }
   else{
   if(!(GFX_BG0=AllocateMem(size * 2))) return(0);
   memset(GFX_BG0,0x00,size * 2);
   }

   tb=0;
   for(ta=0;ta<size;ta+=4){
      GFX_BG0[tb+0]=(src[ta+0]>>4)^15;
      GFX_BG0[tb+1]=(src[ta+0]&15)^15;
      GFX_BG0[tb+2]=(src[ta+1]>>4)^15;
      GFX_BG0[tb+3]=(src[ta+1]&15)^15;
      GFX_BG0[tb+4]=(src[ta+2]>>4)^15;
      GFX_BG0[tb+5]=(src[ta+2]&15)^15;
      GFX_BG0[tb+6]=(src[ta+3]>>4)^15;
      GFX_BG0[tb+7]=(src[ta+3]&15)^15;
      tb+=16;
      if((tb&0xFF)==0){tb-=0xF8;}
      else{if((tb&0xFF)==8){tb-=8;}}
   }

   if(size <= 0x100000)
   BG0_Mask = make_solid_mask_16x16(GFX_BG0, 0x1000);
   else
   BG0_Mask = make_solid_mask_16x16(GFX_BG0, size/0x80);

   return 1;
}

int NMKDecodeBG0_BJT(UINT8 *src, UINT32 size)
{
   UINT32 ta,tb;

   if(!(GFX_BG0=AllocateMem(size * 2))) return(0);
   memset(GFX_BG0,0x00,size * 2);

   tb=0;
   for(ta=0;ta<size;ta++){
      GFX_BG0[tb+0]=(src[ta+0]>>4)^15;
      GFX_BG0[tb+1]=(src[ta+0]&15)^15;
      tb+=2;
   }

   BG0_Mask = make_solid_mask_8x8(GFX_BG0, size/0x20);

   return 1;
}

static void EmptySoundFrame(void)
{
}

static void AddNMKControls(void)
{
   // Set Sound Only Emulation Frame
   // ------------------------------

   ExecuteSoundFrame=&EmptySoundFrame;

   memset(RAM+0x00000,0x00,0x30000);
   memset(RAM+0x10000,0xFF,0x00010);

   if(romset==7)
      RAM_COL=RAM+0x1C000;
   else
      RAM_COL=RAM+0x18000;

   InitPaletteMap(RAM_COL, 0x40, 0x10, 0x8000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_rgbx_rev);
}

void AddNMKMainCPU(UINT32 ram, UINT32 vram)
{
   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);      // 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);                 // 68000 ROM
   AddReadByte(ram,    ram+0xFFFF, NULL, RAM+0x000000);                 // 68000 RAM
   //AddReadByte(vram+0x4000,vram+0x4001,OKIM6295_status_0_r,NULL);
   //AddReadByte(vram+0x4010,vram+0x4011,OKIM6295_status_1_r,NULL);
   
   AddReadByte(vram, vram+0x1FFFF, NULL, RAM+0x010000);                 // SCREEN RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);               // <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);                 // 68000 ROM
   AddReadWord(ram,    ram+0xFFFF, NULL, RAM+0x000000);                 // 68000 RAM
   AddReadWord(vram+0x8000,vram+0x1FFFF, NULL, RAM+0x018000);           // SCREEN RAM
   //AddReadWord(vram+0x4000,vram+0x4001,OKIM6295_status_0_r,NULL);
   //AddReadWord(vram+0x4010,vram+0x4011,OKIM6295_status_1_r,NULL);
   AddReadWord(vram, vram+0x07FFF, NMKVideoRW, NULL);                   // MISC SCREEN RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);               // <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(ram,    ram+0xFFFF, NULL, RAM+0x000000);                // 68000 RAM
   AddWriteByte(vram+0x8000, vram+0x1FFFF, NULL, RAM+0x018000);         // SCREEN RAM
   //AddWriteByte(vram+0x4000, vram+0x04001,OKIM6295_data_0_w , NULL);                  // MISC SCREEN RAM
   //AddWriteByte(vram+0x4010, vram+0x04011,OKIM6295_data_1_w , NULL);                  // MISC SCREEN RAM
   //AddWriteByte(vram+0x4020, vram+0x0402f,bjtwin_oki6295_bankswitch_w , NULL);                  // MISC SCREEN RAM
   AddWriteByte(vram, vram+0x07FFF, NMKVideoWB, NULL);                  // MISC SCREEN RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);                   // Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);             // <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(ram,    ram+0xFFFF, NULL, RAM+0x000000);                // 68000 RAM
   AddWriteWord(vram+0x8000, vram+0x1FFFF, NULL, RAM+0x018000);         // SCREEN RAM
   //AddWriteWord(vram+0x4000, vram+0x04001,OKIM6295_data_0_w , NULL);                  // MISC SCREEN RAM
   //AddWriteWord(vram+0x4010, vram+0x04011,OKIM6295_data_1_w , NULL);                  // MISC SCREEN RAM
   //AddWriteWord(vram+0x4020, vram+0x402f, bjtwin_oki6295_bankswitch_w , NULL);                  // MISC SCREEN RAM
   AddWriteWord(vram, vram+0x07FFF, NMKVideoWW, NULL);                  // MISC SCREEN RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);             // <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();     // Set Starscream mem pointers... 
}

void LoadBombJackTwin(void)
{
   int ta;

   romset=0;

   if(!(ROM=AllocateMem(0x100000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("bjt.35", ROM, 0x10000)) return;   // 8x8 FG0 TILES (OK)
   if(!NMKDecodeFG0(ROM,0x10000))return;

   if(!load_rom("bjt.100", ROM, 0x100000)) return; // 16x16 SPRITES (OK)
   DecodeBombJackTwin_OBJ(ROM,0x100000);
//   DecodeBombJackTwin_OBJ(ROM); kath
   if(!NMKDecodeSPR(ROM,0x100000))return;

   if(!load_rom("bjt.32", ROM, 0x100000)) return;  // 16x16 TILES (OK)
   DecodeBombJackTwin_BG0(ROM,0x100000);
//   DecodeBombJackTwin_BG0(ROM); kath
   if(!NMKDecodeBG0_BJT(ROM,0x100000))return;

   if(!load_rom("bjt.77", RAM+0x00000, 0x40000)) return;   // MAIN 68000
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("bjt.76", RAM+0x00000, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   if(!(PCMROM = AllocateMem(0x200000))) return;
   if(!load_rom("bjt.130",PCMROM+0x000000,0x100000)) return;
   if(!load_rom("bjt.127",PCMROM+0x100000,0x100000)) return;
   ADPCMSetBuffers(((struct ADPCMinterface*)&m6295_interface),PCMROM,0x100000);

/*    for(ta=0;ta<0x10;ta++){ */
/*       m6295_romlist_chip_a[ta].data = PCMROM+0x000000+(ta*0x10000); */
/*       m6295_romlist_chip_a[ta].size = 0x10000; */
/*       m6295_romlist_chip_b[ta].data = PCMROM+0x100000+(ta*0x10000); */
/*       m6295_romlist_chip_b[ta].size = 0x10000; */
/*    } */

   /*-----------------------*/

   RAMSize=0x40000;

   scr_x = 384;
   scr_y = 224;

   // 68000 Speed hack

   WriteLong68k(&ROM[0x096EA],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x096EE],0x00AA0000);       //

   // Input hack, so test mode is available

   WriteLong68k(&ROM[0x08F64],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F68],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F6C],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F70],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F74],0x4E714E71);      //

   // Checksum hack

   ROM[0x09172] = 0x60;

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

void LoadSabotenBombers(void)
{
   int ta;

   romset=0;

   if(!(ROM=AllocateMem(0x200000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("ic35.sb3", ROM, 0x10000)) return;   // 8x8 FG0 TILES (OK)
   if(!NMKDecodeFG0(ROM,0x10000))return;

   if(!load_rom("ic100.sb5", ROM, 0x200000)) return; // 16x16 SPRITES (OK)
   DecodeBombJackTwin_OBJ(ROM,0x200000);
//   DecodeBombJackTwin_OBJ(ROM); kath
   if(!NMKDecodeSPR(ROM,0x200000))return;

   if(!load_rom("ic32.sb4", ROM, 0x200000)) return;  // 16x16 TILES (OK)
   DecodeBombJackTwin_BG0(ROM,0x200000);
   if(!NMKDecodeBG0_BJT(ROM,0x200000))return;

   if(!load_rom("ic76.sb1", RAM+0x00000, 0x40000)) return;   // MAIN 68000
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("ic75.sb2", RAM+0x00000, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   if(!(PCMROM = AllocateMem(0x200000))) return;
   if(!load_rom("ic30.sb6",PCMROM+0x000000,0x100000)) return;
   if(!load_rom("ic27.sb7",PCMROM+0x100000,0x100000)) return;
   ADPCMSetBuffers(((struct ADPCMinterface*)&m6295_interface),PCMROM,0x100000);

/*    for(ta=0;ta<0x10;ta++){ */
/*       m6295_romlist_chip_a[ta].data = PCMROM+0x000000+(ta*0x10000); */
/*       m6295_romlist_chip_a[ta].size = 0x10000; */
/*       m6295_romlist_chip_b[ta].data = PCMROM+0x100000+(ta*0x10000); */
/*       m6295_romlist_chip_b[ta].size = 0x10000; */
/*    } */

   /*-----------------------*/

   RAMSize=0x40000;

   scr_x = 384;
   scr_y = 224;

   // 68000 Speed hack

//   WriteLong68k(&ROM[0x096EA],0x13FC0000);       // move.b #$00,$AA0000
//   WriteLong68k(&ROM[0x096EE],0x00AA0000);       //

   // Input hack, so test mode is available

//   WriteLong68k(&ROM[0x08F64],0x4E714E71);      //
//   WriteLong68k(&ROM[0x08F68],0x4E714E71);      //
//   WriteLong68k(&ROM[0x08F6C],0x4E714E71);      //
//   WriteLong68k(&ROM[0x08F70],0x4E714E71);      //
//   WriteLong68k(&ROM[0x08F74],0x4E714E71);      //

   // Checksum hack

//   ROM[0x09172] = 0x60;

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

void load_macross_2(void)
{
  //int ta;

   romset=8;

   if(!(RAM=AllocateMem(0x60000))) return;
   if(!(ROM=AllocateMem(0x200000))) return;

   if(!load_rom("921a01", ROM, 0x20000)) return;   // 8x8 FG0 TILES (OK)
   if(!NMKDecodeFG0(ROM,0x20000))return;

   if(!load_rom("921a07", ROM, 0x200000)) return;  // 16x16 SPRITES (OK)
   DecodeBombJackTwin_OBJ(ROM,0x200000);
//   DecodeBombJackTwin_OBJ(ROM); kath
   if(!NMKDecodeSPR(ROM,0x200000))return;

   if(!load_rom("921a04", ROM, 0x200000)) return;  // 16x16 TILES (OK)
   DecodeBombJackTwin_BG0(ROM,0x200000);
//   DecodeBombJackTwin_BG0(ROM); kath
   if(!NMKDecodeBG0(ROM,0x200000))return;

   FreeMem(ROM);
   if(!(ROM=AllocateMem(0x80000))) return;

   if(!load_rom("921a03", ROM+0x00000, 0x80000)) return;   // MAIN 68000
   ByteSwap(ROM,0x80000);

   /*-----[Sound Setup]-----*/

   if(!(PCMROM = AllocateMem(0x100000))) return;
   if(!load_rom("921a05",PCMROM+0x000000,0x080000)) return;
   if(!load_rom("921a06",PCMROM+0x080000,0x080000)) return;
   ADPCMSetBuffers(((struct ADPCMinterface*)&m6295_interface),PCMROM,0x80000);

/*    for(ta=0;ta<0x04;ta++){ */
/*       m6295_romlist_chip_a[ta].data = PCMROM+0x000000+(ta*0x20000); */
/*       m6295_romlist_chip_a[ta].size = 0x20000; */
/*       m6295_romlist_chip_b[ta].data = PCMROM+0x080000+(ta*0x20000); */
/*       m6295_romlist_chip_b[ta].size = 0x20000; */
/*    } */

   /*-----------------------*/

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   // 68000 Speed hack

   WriteLong68k(&ROM[0x052AA],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x052AE],0x00AA0000);       //

   WriteLong68k(&ROM[0x052CA],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x052CE],0x00AA0000);       //
/*
   // Input hack, so test mode is available

   WriteLong68k(&ROM[0x08F64],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F68],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F6C],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F70],0x4E714E71);      //
   WriteLong68k(&ROM[0x08F74],0x4E714E71);      //

   // Checksum hack

   ROM[0x09172] = 0x60;
*/
   // Protection Hack

   WriteWord68k(&ROM[0x00BEC8],0x4EF9);
   WriteLong68k(&ROM[0x00BECA],0x00004FD0);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

static struct ROM_PATCH hacha_mecha_fighter_patch_0[] =
{
  { 0x0000C0, 0x5279000F, 0x90240839 },
  { 0x0000C8, 0x0000000F, 0x9025670C },
  { 0x0000D0, 0x5279000F, 0xEFFE5279 },
  { 0x0000D8, 0x000FEFFC, 0x4E714E71 },
  { 0x0000E0, 0x4E714EF9, 0x0003F200 },
  { 0x000100, 0x4DF9000F, 0x90034A16 },
  { 0x000108, 0x660EDDFC, 0x000055FE },
  { 0x000110, 0x0C160005, 0x67025216 },
  { 0x000118, 0x33FC0001, 0x000C0018 },
  { 0x000120, 0x4E750000, 0x00000000 },
  { 0x000130, 0x00000000, 0x33FC0001 },
  { 0x000138, 0x000BEF04, 0x5379000F },
  { 0x000140, 0xEF004E75, 0x4279000F },
  { 0x000148, 0x7B2A60F0, 0x4279000F },
  { 0x000150, 0x90081039, 0x000C0001 },
  { 0x000158, 0x4640C07C, 0x000313C0 },
  { 0x000160, 0x000BE703, 0x4E7591FC },
  { 0x000168, 0x00010000, 0x0C100020 },
  { 0x000170, 0x6C0A0C28, 0x00150100 },
  { 0x000178, 0x6C024E75, 0xD1FC0000 },
  { 0x000180, 0x6BB40C10, 0x000566F2 },
  { 0x000188, 0x0C280006, 0x000267EA },
  { 0x000190, 0x42B9000F, 0xFFF042B9 },
  { 0x000198, 0x000FFFF8, 0x4E750000 },

  { 0x0001A0, 0x0C790001, 0x000F9002 },
  { 0x0001A8, 0x670E08F9, 0x0000000F },
  { 0x0001B0, 0x90004EF9, 0x0000C63C },
  { 0x0001B8, 0x4EF90000, 0xC5E20000 },

  { 0x000210, 0x093E1400, 0x00000000 },		// DATA
  { 0x000220, 0x5A596956, 0x73790000 },
  { 0x000228, 0x0000565B, 0x735B0000 },
  { 0x000250, 0x001D1715, 0x00190014 },
  { 0x000260, 0x347C0200, 0x4A03671C },		// CODE
  { 0x000268, 0x72053403, 0xC47C00F0 },
  { 0x000270, 0xC67C000F, 0x70A0B400 },
  { 0x000278, 0x6710D07C, 0x001051C9 },
  { 0x000280, 0xFFF64E75, 0x343C82FF },
  { 0x000288, 0x600EE949, 0x86411632 },
  { 0x000290, 0x3000343C, 0x80008443 },
  { 0x000298, 0x33C20008, 0x001E45F9 },		// C001E -> 8001E
  { 0x0002A0, 0x000FE000, 0x0C2A0005 },		// BE000 -> FE000
  { 0x0002A8, 0x060166D6, 0x4A39000F },		// B9003 -> F9003
  { 0x0002B0, 0x900366CE, 0x95FC0000 },
  { 0x0002B8, 0x7000082A, 0x00040A4D },
  { 0x0002C0, 0x660A082A, 0x00050B4D },
  { 0x0002C8, 0x66024E75, 0xD5FC0000 },
  { 0x0002D0, 0xC000082A, 0x00070007 },
  { 0x0002D8, 0x660C95FC, 0x00003010 },
  { 0x0002E0, 0x42AA0006, 0x600A082A },
  { 0x0002E8, 0x00010009, 0x66EC4E75 },
  { 0x0002F0, 0x41F90008, 0x4000303C },		// C4000 -> 84000
  { 0x0002F8, 0xC0004258, 0x51C8FFFC },
  { 0x000300, 0x0C390003, 0x00044023 },
  { 0x000308, 0x66C04EF9, 0x0000D518 },

  { 0x000400, 0x4EF90000, 0x917841F9 },		// Input fix
  { 0x000408, 0x000FFFFC, 0x30A80004 },
  { 0x000410, 0x31680006, 0x000290FC },
  { 0x000418, 0x85AF6100, 0xFD4A207C },		// 85AF = ?

  { 0x000498, 0x66124E75, 0x3639000F },		// Some hack

  { 0x0004A8, 0x67024E75, 0x36194EB9 },		// Some jump to $260 hack
  { 0x0004B0, 0x00000260, 0x33C3000F },

/*
  { 0x008DA0, 0x0000D518, 0x4EB90000 },
  { 0x008DA8, 0x8E7C4EB9, 0x0000D558 },
  { 0x008DB0, 0x4EB90000, 0x96DA4EB9 },
  { 0x008DB8, 0x0000A062, 0x4EB90000 },
  { 0x008DC0, 0xA1544EB9, 0x0000A57A },
  { 0x008DC8, 0x4EB90000, 0x9E224EB9 },
  { 0x008DD0, 0x0000AA0A, 0x4EB90000 },
  { 0x008DD8, 0xAC486500, 0x01EA4EB9 },
  { 0x008DE0, 0x0000B110, 0x4EB90000 },
  { 0x008DE8, 0xB9B24EB9, 0x0000BB4C },
  { 0x008DF0, 0x4EB90000, 0xAFA64EB9 },
  { 0x008DF8, 0x0000C6A4, 0x60000060 },
  { 0x008E00, 0x4EB90000, 0x03004EB9 },
  { 0x008E08, 0x00008E7C, 0x4EB90000 },
  { 0x008E10, 0xD5584EB9, 0x000096DA },
  { 0x008E18, 0x4EB90000, 0xA0624EB9 },
  { 0x008E20, 0x0000A154, 0x4EB90000 },
  { 0x008E28, 0xA57A4EB9, 0x00009E22 },
  { 0x008E30, 0x4EB90000, 0xAA0A4EB9 },
  { 0x008E38, 0x0000AC48, 0x65000188 },
  { 0x008E40, 0x4EB90000, 0xB1104EB9 },
  { 0x008E48, 0x0000B9B2, 0x4EB90000 },
  { 0x008E50, 0xBB4C4EB9, 0x0000AFA6 },
  { 0x008E58, 0x4EB90000, 0xC6A45279 },

  { 0x0093F8, 0x4279000B, 0xEF024279 },
  { 0x009400, 0x000BEF00, 0x4E714E71 },
  { 0x0094B0, 0x000092F4, 0x60FE3200 },
  { 0x009578, 0xFFFE4EB8, 0x00C04E71 },

  { 0x010930, 0x3E10E34F, 0x4E7141E8 },

  { 0x03F000, 0x4EF90000, 0x9E220000 },
  { 0x03F008, 0x00000000, 0x00004007 },
  { 0x03F010, 0x4EF90000, 0xAA0A198B },
  { 0x03F018, 0x198B198B, 0x198B4038 },
  { 0x03F020, 0x4EF90000, 0xAC48198B },
  { 0x03F028, 0x198B198B, 0x198B4019 },
  { 0x03F030, 0x4EF90000, 0xB110198B },
  { 0x03F038, 0x198B198B, 0x198B402A },
  { 0x03F040, 0x4EF90000, 0xB9B2198B },
  { 0x03F048, 0x198B198B, 0x198B400B },
  { 0x03F050, 0x4EF90000, 0xBB4C0000 },
  { 0x03F058, 0x00000000, 0x0000403C },
  { 0x03F060, 0x4EF90000, 0xAFA60000 },
  { 0x03F068, 0x00000000, 0x0000401D },
  { 0x03F070, 0x4EF90000, 0xC6A40000 },
  { 0x03F078, 0x00000000, 0x0000402E },
  { 0x03F080, 0x4EF90000, 0xA0620000 },
  { 0x03F088, 0x00000000, 0x00004004 },
  { 0x03F090, 0x4EF90000, 0xD5180000 },
  { 0x03F098, 0x00000000, 0x00004030 },
  { 0x03F0A0, 0x4EF90000, 0x8E7C0000 },
  { 0x03F0A8, 0x00000000, 0x00004011 },
  { 0x03F0B0, 0x4EF90000, 0xD5580000 },
  { 0x03F0B8, 0x00000000, 0x00004022 },
  { 0x03F0C0, 0x4EF90000, 0x96DA0000 },
  { 0x03F0C8, 0x00000000, 0x00004003 },
  { 0x03F0D0, 0x4EF90000, 0xA0620000 },
  { 0x03F0D8, 0x00000000, 0x00004034 },
  { 0x03F0E0, 0x4EF90000, 0xA1540000 },
  { 0x03F0E8, 0x00000000, 0x00004015 },
  { 0x03F0F0, 0x4EF90000, 0xA57A0000 },
  { 0x03F0F8, 0x00000000, 0x00004026 },
  { 0x03F100, 0x4EF90000, 0xD5180000 },
  { 0x03F108, 0x00000000, 0x00000000 },
  { 0x03F110, 0x4EF90000, 0x8E7C198B },
  { 0x03F118, 0x198B198B, 0x198B0000 },
  { 0x03F120, 0x4EF90000, 0xD558198B },
  { 0x03F128, 0x198B198B, 0x198B0000 },
  { 0x03F130, 0x4EF90000, 0x96DA198B },
  { 0x03F138, 0x198B198B, 0x198B0000 },
  { 0x03F140, 0x4EF90000, 0xA062198B },
  { 0x03F148, 0x198B198B, 0x198B0000 },
  { 0x03F150, 0x4EF90000, 0xA1540000 },
  { 0x03F158, 0x00000000, 0x00000000 },
  { 0x03F160, 0x4EF90000, 0xA57A0000 },
  { 0x03F168, 0x00000000, 0x00000000 },
  { 0x03F170, 0x4EF90000, 0x9E220000 },
  { 0x03F178, 0x00000000, 0x00000000 },
  { 0x03F180, 0x4EF90000, 0xAA0A0000 },
  { 0x03F188, 0x00000000, 0x00000000 },
  { 0x03F190, 0x4EF90000, 0xAC480000 },
  { 0x03F198, 0x00000000, 0x00000000 },
  { 0x03F1A0, 0x4EF90000, 0xB1100000 },
  { 0x03F1A8, 0x00000000, 0x00000000 },
  { 0x03F1B0, 0x4EF90000, 0xB9B20000 },
  { 0x03F1B8, 0x00000000, 0x00000000 },
  { 0x03F1C0, 0x4EF90000, 0xBB4C0000 },
  { 0x03F1C8, 0x00000000, 0x00000000 },
  { 0x03F1D0, 0x4EF90000, 0xAFA60000 },
  { 0x03F1D8, 0x00000000, 0x00000000 },
  { 0x03F1E0, 0x4EF90000, 0xC6A40000 },
  { 0x03F1E8, 0x00000000, 0x00000000 },
  { 0x03F1F0, 0x4EF90000, 0xA57A0000 },
  { 0x03F1F8, 0x00000000, 0x00000000 },
  { 0x03F200, 0x4279000B, 0xEF024A39 },
  { 0x03F208, 0x000BEF05, 0x67184239 },
  { 0x03F210, 0x000BEF05, 0x04390001 },
  { 0x03F218, 0x000BEF01, 0x6A000008 },
  { 0x03F220, 0x4239000B, 0xEF0148E7 },
  { 0x03F228, 0xF0001039, 0x000C0001 },
  { 0x03F230, 0x12000200, 0x00030A00 },
  { 0x03F238, 0x00036736, 0x08010000 },
  { 0x03F240, 0x67164A39, 0x000BA001 },
  { 0x03F248, 0x660000A8, 0x13FC0001 },
  { 0x03F250, 0x000BA001, 0x6000009C },
  { 0x03F258, 0x4A39000B, 0xA0006608 },
  { 0x03F260, 0x13FC0001, 0x000BA000 },
  { 0x03F268, 0x08010001, 0x67D46000 },
  { 0x03F270, 0x00824A39, 0x000BA000 },
  { 0x03F278, 0x672E4239, 0x000BA000 },
  { 0x03F280, 0x06390001, 0x000BEF08 },
  { 0x03F288, 0x1439000B, 0xEF09B439 },
  { 0x03F290, 0x000BEF08, 0x66124239 },
  { 0x03F298, 0x000BEF08, 0x1439000B },
  { 0x03F2A0, 0xEF0BD539, 0x000BEF01 },
  { 0x03F2A8, 0x4A39000B, 0xA001672E },
  { 0x03F2B0, 0x4239000B, 0xA0010639 },
  { 0x03F2B8, 0x0001000B, 0xEF0C1439 },
  { 0x03F2C0, 0x000BEF0D, 0xB439000B },
  { 0x03F2C8, 0xEF0C6612, 0x4239000B },
  { 0x03F2D0, 0xEF0C1439, 0x000BEF0F },
  { 0x03F2D8, 0xD539000B, 0xEF011039 },
  { 0x03F2E0, 0x000BEF01, 0x0C00000A },
  { 0x03F2E8, 0x650813FC, 0x0009000B },
  { 0x03F2F0, 0xEF010839, 0x0006000B },
  { 0x03F2F8, 0x90006600, 0x00C04A39 },
  { 0x03F300, 0x000BEF01, 0x6604123C },
  { 0x03F308, 0x00181439, 0x000B9000 },
  { 0x03F310, 0x02020083, 0x16020A02 },
  { 0x03F318, 0x00836722, 0x14030A02 },
  { 0x03F320, 0x00826706, 0x08010003 },
  { 0x03F328, 0x675A0A03, 0x00816708 },
  { 0x03F330, 0x08010004, 0x67000068 },
  { 0x03F338, 0x4239000B, 0xA00223FC },
  { 0x03F340, 0x0008E294, 0x000BE000 },
  { 0x03F348, 0x33F9000C, 0x0002000B },
  { 0x03F350, 0xE3BA23FC, 0x0008E3BA },
  { 0x03F358, 0x000BE004, 0x4A39000B },
  { 0x03F360, 0xDFFF670A, 0x363C00F4 },
  { 0x03F368, 0x4EB90003, 0xF8000201 },
  { 0x03F370, 0x001813C1, 0x000BE295 },
  { 0x03F378, 0x4279000B, 0x90084CDF },
  { 0x03F380, 0x000F4E75, 0x1039000B },
  { 0x03F388, 0xA0020800, 0x00036600 },
  { 0x03F390, 0x00240039, 0x0008000B },
  { 0x03F398, 0xA0026000, 0x00181039 },
  { 0x03F3A0, 0x000BA002, 0x08000004 },
  { 0x03F3A8, 0x66940039, 0x0010000B },
  { 0x03F3B0, 0xA002608A, 0x08010004 },
  { 0x03F3B8, 0x67E46082, 0x6000FF7A },
  { 0x03F3C0, 0x08010004, 0x67E46086 },
  { 0x03F3C8, 0x6000FF74, 0x67C46000 },
  { 0x03F3D0, 0xFF4C4E76, 0x4E764E76 },
  { 0x03F800, 0x08390007, 0x000B9000 },
  { 0x03F808, 0x66140839, 0x0006000C },
  { 0x03F810, 0x000B660A, 0x13FC0078 },
  { 0x03F818, 0x000C001F, 0x4E7548E7 },
  { 0x03F820, 0x2E803A03, 0x383C0021 },
  { 0x03F828, 0x41F90003, 0xF98E1C18 },
  { 0x03F830, 0xBC036700, 0x008C51CC },
  { 0x03F838, 0xFFF61A39, 0x000BDFFF },
  { 0x03F840, 0xCA056622, 0x4CDF0174 },
  { 0x03F848, 0x4E7513FC, 0x0078000C },
  { 0x03F850, 0x001F33FC, 0x0000000B },
  { 0x03F858, 0xDFFE60E8, 0x1C39000B },
  { 0x03F860, 0xDFFFBC05, 0x664A1C39 },
  { 0x03F868, 0x000C000F, 0x08060000 },
  { 0x03F870, 0x66D20405, 0x00A141FA },
  { 0x03F878, 0x0028DA45, 0x3A305000 },
  { 0x03F880, 0x00458010, 0x380513C5 },
  { 0x03F888, 0x000C0009, 0xE04C13C4 },
  { 0x03F890, 0x000C001F, 0x06050005 },
  { 0x03F898, 0x13C5000C, 0x001F60A4 },
  { 0x03F8A0, 0x1A001B00, 0x1C001D00 },
  { 0x03F8A8, 0x1E011F02, 0x20032104 },
  { 0x03F8B0, 0x13FC0010, 0x000C001F },
  { 0x03F8B8, 0x13C5000B, 0xDFFF60B2 },
  { 0x03F8C0, 0x024500FF, 0x67840C05 },
  { 0x03F8C8, 0x00B06D90, 0x1C39000B },
  { 0x03F8D0, 0xDFFEBC05, 0x13C5000B },
  { 0x03F8D8, 0xDFFE0405, 0x00D041FA },
  { 0x03F8E0, 0x00760285, 0x000000FF },
  { 0x03F8E8, 0xDA05D1C5, 0x1A180005 },
  { 0x03F8F0, 0x00801410, 0x1839000C },
  { 0x03F8F8, 0x000F0804, 0x00016614 },
  { 0x03F900, 0x13C5000C, 0x001F0002 },
  { 0x03F908, 0x002013C2, 0x000C001F },
  { 0x03F910, 0x6000FF32, 0x08040002 },
  { 0x03F918, 0x661413C5, 0x000C001F },
  { 0x03F920, 0x00020040, 0x13C2000C },
  { 0x03F928, 0x001F6000, 0xFF180804 },
  { 0x03F930, 0x00036614, 0x13C5000C },
  { 0x03F938, 0x001F0002, 0x008013C2 },
  { 0x03F940, 0x000C001F, 0x6000FEFE },
  { 0x03F948, 0x13FC0020, 0x000C001F },
  { 0x03F950, 0x4E714E71, 0x609E0106 },
  { 0x03F958, 0x02000301, 0x04020607 },
  { 0x03F960, 0x06000700, 0x00000800 },
  { 0x03F968, 0x09000A00, 0x00000C03 },
  { 0x03F970, 0x0A0F0000, 0x00000E00 },
  { 0x03F978, 0x13001003, 0x11071205 },
  { 0x03F980, 0x13001400, 0x15001600 },
  { 0x03F988, 0x17041803, 0x1903D0D1 },
  { 0x03F990, 0xD2D3D4D5, 0xD6D8D9DA },
  { 0x03F998, 0xDBDCDDE0, 0xE1E2E3E4 },
  { 0x03F9A0, 0xE5E6E7E8, 0xE9EAEBA1 },
  { 0x03F9A8, 0xA2A3A4A5, 0xA6A7A800 },
*/
  { -1,       0,          0          },
};

void LoadHachaMechaFighter(void)
{
   int ta;

   romset=1;

   if(!(ROM=AllocateMem(0x100000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("hmf_05.rom", RAM, 0x20000)) return;           // 8x8 FG0 TILES
   if(!NMKDecodeFG0(RAM,0x20000))return;

   if(!load_rom("hmf_08.rom", ROM, 0x100000)) return;   	// 16x16 SPRITES
   ByteSwap(ROM,0x100000);
   if(!NMKDecodeSPR(ROM,0x100000))return;

   if(!load_rom("hmf_04.rom", ROM+0x00000, 0x80000)) return;   // 16x16 TILES
   if(!NMKDecodeBG1(ROM,0x80000))return;

   if(!load_rom("hmf_04.rom", ROM+0x00000, 0x80000)) return;   // 16x16 TILES
   if(!NMKDecodeBG0(ROM,0x80000))return;

   if(!load_rom("hmf_07.rom", RAM+0x00000, 0x20000)) return;   // MAIN 68000
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("hmf_06.rom", RAM+0x00000, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   if(!(PCMROM = AllocateMem(0x80000))) return;
   if(!load_rom("hmf_03.rom",PCMROM+0x00000,0x40000)) return;
   if(!load_rom("hmf_02.rom",PCMROM+0x40000,0x40000)) return;
   ADPCMSetBuffers(((struct ADPCMinterface*)&m6295_interface),PCMROM,0x40000);

/*    m6295_romlist_chip_a[0].data = PCMROM+0x00000; */
/*    m6295_romlist_chip_a[0].size = 0x40000; */
/*    m6295_romlist_chip_a[1].data = NULL; */
/*    m6295_romlist_chip_b[0].data = PCMROM+0x40000; */
/*    m6295_romlist_chip_b[0].size = 0x40000; */
/*    m6295_romlist_chip_b[1].data = NULL; */

   /*-----------------------*/

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   // Protection

   patch_rom(ROM,hacha_mecha_fighter_patch_0);

   WriteWord68k(&ROM[0x07E82],0x4279);		//
   WriteLong68k(&ROM[0x07E84],0x000FEF00);	//
   WriteLong68k(&ROM[0x07E88],0x4E714E71);	//

   WriteLong68k(&ROM[0x07F2C],0x00007DC2);	//

   WriteWord68k(&ROM[0x143B0],0x4E41);		//

   // 68000 Speed hack

   WriteLong68k(&ROM[0x0810E],0x13FC0000);      // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x08112],0x00AA0000);      //

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

void LoadBattleShipP(void)
{
   int ta;

   romset=2;

   if(!(ROM=AllocateMem(0xA0000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("7", RAM, 0x10000)) return;               // 8x8 FG0 TILES
   if(!NMKDecodeFG0(RAM,0x10000))return;

   if(!load_rom("sbs-g.03", ROM, 0x80000)) return;        // 16x16 SPRITES
   if(!NMKDecodeSPR(ROM,0x80000))return;

   if(!load_rom("sbs-g.01", ROM, 0x80000)) return;        // 16x16 TILES
   if(!NMKDecodeBG0(ROM,0x80000))return;

   if(!load_rom("sbs-g.02", ROM, 0x80000)) return;        // 16x16 TILES
   if(!NMKDecodeBG1(ROM,0x80000))return;

   if(!load_rom("2", RAM+0x00000, 0x20000)) return;       // 68000 ROM
   if(!load_rom("8", RAM+0x20000, 0x10000)) return;	  // DATA ROM (BG0)
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+0]=RAM[ta];
   }
   if(!load_rom("1", RAM+0x00000, 0x20000)) return;       // 68000 ROM
   if(!load_rom("9", RAM+0x20000, 0x10000)) return;	  // DATA ROM (BG0)
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   if(!(PCMROM = AllocateMem(0x100000))) return;
   if(!load_rom( "sbs-g.05", PCMROM+0x00000,0x80000)) return;
   if(!load_rom( "sbs-g.04", PCMROM+0x80000,0x80000)) return;
   ADPCMSetBuffers(((struct ADPCMinterface*)&m6295_interface),PCMROM,0x80000);

/*    m6295_romlist_chip_a[0].data = PCMROM+0x00000; */
/*    m6295_romlist_chip_a[0].size = 0x80000; */
/*    m6295_romlist_chip_a[1].data = NULL; */
/*    m6295_romlist_chip_b[0].data = PCMROM+0x80000; */
/*    m6295_romlist_chip_b[0].size = 0x80000; */
/*    m6295_romlist_chip_b[1].data = NULL; */

   /*-----------------------*/

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   // Fix Checksum
   // ------------

   WriteWord68k(&ROM[0x0E84C],0x4E75);

   // Fix something (protection/eeprom)
   // ---------------------------------

   WriteWord68k(&ROM[0x0F63C],0x4E75);

   // 68000 Speed hack
   // ----------------

   WriteWord68k(&ROM[0x0498],0x4EF9);           // jmp    $300
   WriteLong68k(&ROM[0x049A],0x00000300);       //

   WriteLong68k(&ROM[0x0300],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x0304],0x00AA0000);       //
   WriteWord68k(&ROM[0x0308],0x4879);           // pea    $00000498
   WriteLong68k(&ROM[0x030A],0x00000498);       //
   WriteWord68k(&ROM[0x030E],0x4EF9);           // jmp    $49E
   WriteLong68k(&ROM[0x0310],0x0000049E);       //

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

void LoadMustang(void)
{
   int ta;

   romset=3;

   if(!(ROM=AllocateMem(0x100000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("mustang.15", RAM, 0x20000)) return;       // 8x8 FG0 TILES
   if(!NMKDecodeFG0(RAM,0x20000))return;

   if(!load_rom("mustang.01", ROM+0x00000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.02", ROM+0x20000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.03", ROM+0x40000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.07", ROM+0x60000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.06", ROM+0x80000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.05", ROM+0xA0000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.04", ROM+0xC0000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.08", ROM+0xE0000, 0x20000)) return;        // 16x16 TILES
   if(!NMKDecodeSPR_Mustang(ROM,0x100000))return;

   if(!load_rom("mustang.09", ROM+0x00000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.12", ROM+0x20000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.11", ROM+0x40000, 0x20000)) return;        // 16x16 TILES
   if(!load_rom("mustang.10", ROM+0x60000, 0x20000)) return;        // 16x16 TILES
   if(!NMKDecodeBG0(ROM,0x80000))return;

   if(!load_rom("mustang.14", RAM+0x00000, 0x20000)) return;       // 68000 ROM
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+0]=RAM[ta];
   }
   if(!load_rom("mustang.13", RAM+0x00000, 0x20000)) return;
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   // Speed Hack

   WriteLong68k(&ROM[0x006F0],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x006F4],0x00AA0000);       //

   WriteLong68k(&ROM[0x00706],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x0070A],0x00AA0000);       //

   // Scroll Hack

   WriteLong68k(&ROM[0x00644],0x0008C000);       //
   WriteLong68k(&ROM[0x00652],0x0008C002);       //
   WriteLong68k(&ROM[0x00662],0x0008C004);       //
   WriteLong68k(&ROM[0x00672],0x0008C006);       //

/*
   WriteWord68k(&ROM[0x008BC],0x4E71);       //
   WriteWord68k(&ROM[0x008C2],0x4E71);       //
   WriteWord68k(&ROM[0x008D0],0x4E71);       //

   ROM[0x008F0]=0x60;
*/
   // Make Test Mode Easier
/*
   WriteLong68k(&ROM[0x03B14],0x4E714E71);
*/

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

void LoadTaskForceHarrier(void)
{
   int ta;

   romset=4;

   if(!(ROM=AllocateMem(0x100000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("1", RAM, 0x20000)) return;   		   	 // 8x8 FG0 TILES
   if(!NMKDecodeFG0(RAM,0x20000))return;

   if(!load_rom("89050-13", ROM+0x00000, 0x80000)) return;       // 16x16 TILES
   if(!load_rom("89050-17", ROM+0x80000, 0x80000)) return;       // 16x16 TILES
   if(!NMKDecodeSPR_Mustang(ROM,0x100000))return;

   if(!load_rom("89050-4", ROM+0x00000, 0x80000)) return;        // 16x16 TILES
   if(!NMKDecodeBG0(ROM,0x80000))return;

   if(!load_rom("2", RAM+0x00000, 0x20000)) return;   		 // 68000 ROM
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+0]=RAM[ta];
   }
   if(!load_rom("3", RAM+0x00000, 0x20000)) return;
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   //WriteWord68k(&ROM[0x06CD8],0x4E71);

   // Protection against nop's

   WriteLong68k(&ROM[0x06E44],0x4E714E71);

   WriteWord68k(&ROM[0x064D0],0x4E71);
   WriteLong68k(&ROM[0x064D2],0x4E714E71);

   WriteWord68k(&ROM[0x06A9C],0x4E71);
   WriteLong68k(&ROM[0x06A9E],0x4E714E71);

   // Problems from protection

   WriteWord68k(&ROM[0x00422],0x6004);

   WriteWord68k(&ROM[0x00416],0x4E71);
   WriteWord68k(&ROM[0x00436],0x4E71);
   WriteWord68k(&ROM[0x00444],0x4E71);
   WriteWord68k(&ROM[0x00450],0x4E71);
   WriteWord68k(&ROM[0x00460],0x4E71);
   WriteWord68k(&ROM[0x0046E],0x4E71);
   WriteWord68k(&ROM[0x0047A],0x4E71);
   WriteWord68k(&ROM[0x0048A],0x4E71);

   WriteLong68k(&ROM[0x00494],0x4E714E71);

   WriteLong68k(&ROM[0x021E0],0x4E714E71);

   // Protection

   WriteWord68k(&ROM[0x007D4],0x4E71);
   WriteLong68k(&ROM[0x007E0],0x4EF808E6);

   WriteLong68k(&ROM[0x00824],0x4EF808E6);

   WriteLong68k(&ROM[0x00880],0x4EF808E6);

   // Speed Hack

   WriteLong68k(&ROM[0x00738],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x0073C],0x00AA0000);       //

   WriteLong68k(&ROM[0x00722],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x00726],0x00AA0000);       //

   // Make Test Mode Easier
/*
   WriteLong68k(&ROM[0x06DCE],0x4E714E71);
*/
/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

static struct ROM_PATCH thunder_dragon_patch_0[] =
{
  { 0x0000C0, 0x5279000B, 0x90240839 },
  { 0x0000C8, 0x0000000B, 0x9025670C },
  { 0x0000D0, 0x5279000B, 0xEFFE5279 },
  { 0x0000D8, 0x000BEFFC, 0x4E714E71 },
  { 0x0000E0, 0x4E714EF9, 0x0003F200 },
  { 0x000100, 0x4DF9000B, 0x90034A16 },
  { 0x000108, 0x660EDDFC, 0x000055FE },
  { 0x000110, 0x0C160005, 0x67025216 },
  { 0x000118, 0x33FC0001, 0x000C0018 },
  { 0x000120, 0x4E750000, 0x00000000 },
  { 0x000130, 0x00000000, 0x33FC0001 },
  { 0x000138, 0x000BEF04, 0x5379000B },
  { 0x000140, 0xEF004E75, 0x4279000B },
  { 0x000148, 0x7B2A60F0, 0x4279000B },
  { 0x000150, 0x90081039, 0x000C0001 },
  { 0x000158, 0x4640C07C, 0x000313C0 },
  { 0x000160, 0x000BE703, 0x4E7591FC },
  { 0x000168, 0x00010000, 0x0C100020 },
  { 0x000170, 0x6C0A0C28, 0x00150100 },
  { 0x000178, 0x6C024E75, 0xD1FC0000 },
  { 0x000180, 0x6BB40C10, 0x000566F2 },
  { 0x000188, 0x0C280006, 0x000267EA },
  { 0x000190, 0x42B9000B, 0xFFF042B9 },
  { 0x000198, 0x000BFFF8, 0x4E750000 },
  { 0x0001A0, 0x0C790001, 0x000B9002 },
  { 0x0001A8, 0x670E08F9, 0x0000000B },
  { 0x0001B0, 0x90004EF9, 0x0000C63C },
  { 0x0001B8, 0x4EF90000, 0xC5E20000 },
  { 0x000210, 0x093E1400, 0x00000000 },
  { 0x000220, 0x5A596956, 0x73790000 },
  { 0x000228, 0x0000565B, 0x735B0000 },
  { 0x000250, 0x001D1715, 0x00190014 },
  { 0x000260, 0x347C0200, 0x4A03671C },
  { 0x000268, 0x72053403, 0xC47C00F0 },
  { 0x000270, 0xC67C000F, 0x70A0B400 },
  { 0x000278, 0x6710D07C, 0x001051C9 },
  { 0x000280, 0xFFF64E75, 0x343C82FF },
  { 0x000288, 0x600EE949, 0x86411632 },
  { 0x000290, 0x3000343C, 0x80008443 },
  { 0x000298, 0x33C2000C, 0x001E45F9 },
  { 0x0002A0, 0x000BE000, 0x0C2A0005 },
  { 0x0002A8, 0x060166D6, 0x4A39000B },
  { 0x0002B0, 0x900366CE, 0x95FC0000 },
  { 0x0002B8, 0x7000082A, 0x00040A4D },
  { 0x0002C0, 0x660A082A, 0x00050B4D },
  { 0x0002C8, 0x66024E75, 0xD5FC0000 },
  { 0x0002D0, 0xC000082A, 0x00070007 },
  { 0x0002D8, 0x660C95FC, 0x00003010 },
  { 0x0002E0, 0x42AA0006, 0x600A082A },
  { 0x0002E8, 0x00010009, 0x66EC4E75 },
  { 0x0002F0, 0x41F9000C, 0x4000303C },
  { 0x0002F8, 0xC0004258, 0x51C8FFFC },
  { 0x000300, 0x0C390003, 0x00044023 },
  { 0x000308, 0x66C04EF9, 0x0000D518 },

  { 0x000400, 0x4EF90000, 0xA89C41F9 },
  { 0x000408, 0x000BFFFC, 0x30A80004 },
  { 0x000410, 0x31680006, 0x000290FC },
  { 0x000418, 0x85AF6100, 0xFD4A207C },
  { 0x000498, 0x66124E75, 0x3639000B },
  { 0x0004A8, 0x67024E75, 0x36194EB9 },
  { 0x0004B0, 0x00000260, 0x33C3000B },

  { 0x008DA0, 0x0000D518, 0x4EB90000 },
  { 0x008DA8, 0x8E7C4EB9, 0x0000D558 },
  { 0x008DB0, 0x4EB90000, 0x96DA4EB9 },
  { 0x008DB8, 0x0000A062, 0x4EB90000 },
  { 0x008DC0, 0xA1544EB9, 0x0000A57A },
  { 0x008DC8, 0x4EB90000, 0x9E224EB9 },
  { 0x008DD0, 0x0000AA0A, 0x4EB90000 },
  { 0x008DD8, 0xAC486500, 0x01EA4EB9 },
  { 0x008DE0, 0x0000B110, 0x4EB90000 },
  { 0x008DE8, 0xB9B24EB9, 0x0000BB4C },
  { 0x008DF0, 0x4EB90000, 0xAFA64EB9 },
  { 0x008DF8, 0x0000C6A4, 0x60000060 },
  { 0x008E00, 0x4EB90000, 0x03004EB9 },
  { 0x008E08, 0x00008E7C, 0x4EB90000 },
  { 0x008E10, 0xD5584EB9, 0x000096DA },
  { 0x008E18, 0x4EB90000, 0xA0624EB9 },
  { 0x008E20, 0x0000A154, 0x4EB90000 },
  { 0x008E28, 0xA57A4EB9, 0x00009E22 },
  { 0x008E30, 0x4EB90000, 0xAA0A4EB9 },
  { 0x008E38, 0x0000AC48, 0x65000188 },
  { 0x008E40, 0x4EB90000, 0xB1104EB9 },
  { 0x008E48, 0x0000B9B2, 0x4EB90000 },
  { 0x008E50, 0xBB4C4EB9, 0x0000AFA6 },
  { 0x008E58, 0x4EB90000, 0xC6A45279 },
/*
  { 0x009378, 0x4EB90003, 0xFC003401 },		// DSW bit reverse
*/
  { 0x0093F8, 0x4279000B, 0xEF024279 },
  { 0x009400, 0x000BEF00, 0x4E714E71 },
  { 0x0094B0, 0x000092F4, 0x60FE3200 },
  { 0x009578, 0xFFFE4EB8, 0x00C04E71 },
/*
  { 0x00C1F8, 0x000B9074, 0x4E714E71 },		// Title screen / partial
  { 0x00C258, 0xF38A4E71, 0x4E715279 },		// NMK logo
  { 0x00C630, 0x000466AE, 0x4EF801A0 },
  { 0x00C638, 0x4E719000, 0x08F90000 },
  { 0x00D3E8, 0x00404E75, 0xFFF04E75 },
*/
  { 0x010930, 0x3E10E34F, 0x4E7141E8 },

  { 0x03F000, 0x4EF90000, 0x9E220000 },
  { 0x03F008, 0x00000000, 0x00004007 },
  { 0x03F010, 0x4EF90000, 0xAA0A198B },
  { 0x03F018, 0x198B198B, 0x198B4038 },
  { 0x03F020, 0x4EF90000, 0xAC48198B },
  { 0x03F028, 0x198B198B, 0x198B4019 },
  { 0x03F030, 0x4EF90000, 0xB110198B },
  { 0x03F038, 0x198B198B, 0x198B402A },
  { 0x03F040, 0x4EF90000, 0xB9B2198B },
  { 0x03F048, 0x198B198B, 0x198B400B },
  { 0x03F050, 0x4EF90000, 0xBB4C0000 },
  { 0x03F058, 0x00000000, 0x0000403C },
  { 0x03F060, 0x4EF90000, 0xAFA60000 },
  { 0x03F068, 0x00000000, 0x0000401D },
  { 0x03F070, 0x4EF90000, 0xC6A40000 },
  { 0x03F078, 0x00000000, 0x0000402E },
  { 0x03F080, 0x4EF90000, 0xA0620000 },
  { 0x03F088, 0x00000000, 0x00004004 },
  { 0x03F090, 0x4EF90000, 0xD5180000 },
  { 0x03F098, 0x00000000, 0x00004030 },
  { 0x03F0A0, 0x4EF90000, 0x8E7C0000 },
  { 0x03F0A8, 0x00000000, 0x00004011 },
  { 0x03F0B0, 0x4EF90000, 0xD5580000 },
  { 0x03F0B8, 0x00000000, 0x00004022 },
  { 0x03F0C0, 0x4EF90000, 0x96DA0000 },
  { 0x03F0C8, 0x00000000, 0x00004003 },
  { 0x03F0D0, 0x4EF90000, 0xA0620000 },
  { 0x03F0D8, 0x00000000, 0x00004034 },
  { 0x03F0E0, 0x4EF90000, 0xA1540000 },
  { 0x03F0E8, 0x00000000, 0x00004015 },
  { 0x03F0F0, 0x4EF90000, 0xA57A0000 },
  { 0x03F0F8, 0x00000000, 0x00004026 },
  { 0x03F100, 0x4EF90000, 0xD5180000 },
  { 0x03F108, 0x00000000, 0x00000000 },
  { 0x03F110, 0x4EF90000, 0x8E7C198B },
  { 0x03F118, 0x198B198B, 0x198B0000 },
  { 0x03F120, 0x4EF90000, 0xD558198B },
  { 0x03F128, 0x198B198B, 0x198B0000 },
  { 0x03F130, 0x4EF90000, 0x96DA198B },
  { 0x03F138, 0x198B198B, 0x198B0000 },
  { 0x03F140, 0x4EF90000, 0xA062198B },
  { 0x03F148, 0x198B198B, 0x198B0000 },
  { 0x03F150, 0x4EF90000, 0xA1540000 },
  { 0x03F158, 0x00000000, 0x00000000 },
  { 0x03F160, 0x4EF90000, 0xA57A0000 },
  { 0x03F168, 0x00000000, 0x00000000 },
  { 0x03F170, 0x4EF90000, 0x9E220000 },
  { 0x03F178, 0x00000000, 0x00000000 },
  { 0x03F180, 0x4EF90000, 0xAA0A0000 },
  { 0x03F188, 0x00000000, 0x00000000 },
  { 0x03F190, 0x4EF90000, 0xAC480000 },
  { 0x03F198, 0x00000000, 0x00000000 },
  { 0x03F1A0, 0x4EF90000, 0xB1100000 },
  { 0x03F1A8, 0x00000000, 0x00000000 },
  { 0x03F1B0, 0x4EF90000, 0xB9B20000 },
  { 0x03F1B8, 0x00000000, 0x00000000 },
  { 0x03F1C0, 0x4EF90000, 0xBB4C0000 },
  { 0x03F1C8, 0x00000000, 0x00000000 },
  { 0x03F1D0, 0x4EF90000, 0xAFA60000 },
  { 0x03F1D8, 0x00000000, 0x00000000 },
  { 0x03F1E0, 0x4EF90000, 0xC6A40000 },
  { 0x03F1E8, 0x00000000, 0x00000000 },
  { 0x03F1F0, 0x4EF90000, 0xA57A0000 },
  { 0x03F1F8, 0x00000000, 0x00000000 },
  { 0x03F200, 0x4279000B, 0xEF024A39 },
  { 0x03F208, 0x000BEF05, 0x67184239 },
  { 0x03F210, 0x000BEF05, 0x04390001 },
  { 0x03F218, 0x000BEF01, 0x6A000008 },
  { 0x03F220, 0x4239000B, 0xEF0148E7 },
  { 0x03F228, 0xF0001039, 0x000C0001 },
  { 0x03F230, 0x12000200, 0x00030A00 },
  { 0x03F238, 0x00036736, 0x08010000 },
  { 0x03F240, 0x67164A39, 0x000BA001 },
  { 0x03F248, 0x660000A8, 0x13FC0001 },
  { 0x03F250, 0x000BA001, 0x6000009C },
  { 0x03F258, 0x4A39000B, 0xA0006608 },
  { 0x03F260, 0x13FC0001, 0x000BA000 },
  { 0x03F268, 0x08010001, 0x67D46000 },
  { 0x03F270, 0x00824A39, 0x000BA000 },
  { 0x03F278, 0x672E4239, 0x000BA000 },
  { 0x03F280, 0x06390001, 0x000BEF08 },
  { 0x03F288, 0x1439000B, 0xEF09B439 },
  { 0x03F290, 0x000BEF08, 0x66124239 },
  { 0x03F298, 0x000BEF08, 0x1439000B },
  { 0x03F2A0, 0xEF0BD539, 0x000BEF01 },
  { 0x03F2A8, 0x4A39000B, 0xA001672E },
  { 0x03F2B0, 0x4239000B, 0xA0010639 },
  { 0x03F2B8, 0x0001000B, 0xEF0C1439 },
  { 0x03F2C0, 0x000BEF0D, 0xB439000B },
  { 0x03F2C8, 0xEF0C6612, 0x4239000B },
  { 0x03F2D0, 0xEF0C1439, 0x000BEF0F },
  { 0x03F2D8, 0xD539000B, 0xEF011039 },
  { 0x03F2E0, 0x000BEF01, 0x0C00000A },
  { 0x03F2E8, 0x650813FC, 0x0009000B },
  { 0x03F2F0, 0xEF010839, 0x0006000B },
  { 0x03F2F8, 0x90006600, 0x00C04A39 },
  { 0x03F300, 0x000BEF01, 0x6604123C },
  { 0x03F308, 0x00181439, 0x000B9000 },
  { 0x03F310, 0x02020083, 0x16020A02 },
  { 0x03F318, 0x00836722, 0x14030A02 },
  { 0x03F320, 0x00826706, 0x08010003 },
  { 0x03F328, 0x675A0A03, 0x00816708 },
  { 0x03F330, 0x08010004, 0x67000068 },
  { 0x03F338, 0x4239000B, 0xA00223FC },
  { 0x03F340, 0x0008E294, 0x000BE000 },
  { 0x03F348, 0x33F9000C, 0x0002000B },
  { 0x03F350, 0xE3BA23FC, 0x0008E3BA },
  { 0x03F358, 0x000BE004, 0x4A39000B },
  { 0x03F360, 0xDFFF670A, 0x363C00F4 },
  { 0x03F368, 0x4EB90003, 0xF8000201 },
  { 0x03F370, 0x001813C1, 0x000BE295 },
  { 0x03F378, 0x4279000B, 0x90084CDF },
  { 0x03F380, 0x000F4E75, 0x1039000B },
  { 0x03F388, 0xA0020800, 0x00036600 },
  { 0x03F390, 0x00240039, 0x0008000B },
  { 0x03F398, 0xA0026000, 0x00181039 },
  { 0x03F3A0, 0x000BA002, 0x08000004 },
  { 0x03F3A8, 0x66940039, 0x0010000B },
  { 0x03F3B0, 0xA002608A, 0x08010004 },
  { 0x03F3B8, 0x67E46082, 0x6000FF7A },
  { 0x03F3C0, 0x08010004, 0x67E46086 },
  { 0x03F3C8, 0x6000FF74, 0x67C46000 },
  { 0x03F3D0, 0xFF4C4E76, 0x4E764E76 },
  { 0x03F800, 0x08390007, 0x000B9000 },
  { 0x03F808, 0x66140839, 0x0006000C },
  { 0x03F810, 0x000B660A, 0x13FC0078 },
  { 0x03F818, 0x000C001F, 0x4E7548E7 },
  { 0x03F820, 0x2E803A03, 0x383C0021 },
  { 0x03F828, 0x41F90003, 0xF98E1C18 },
  { 0x03F830, 0xBC036700, 0x008C51CC },
  { 0x03F838, 0xFFF61A39, 0x000BDFFF },
  { 0x03F840, 0xCA056622, 0x4CDF0174 },
  { 0x03F848, 0x4E7513FC, 0x0078000C },
  { 0x03F850, 0x001F33FC, 0x0000000B },
  { 0x03F858, 0xDFFE60E8, 0x1C39000B },
  { 0x03F860, 0xDFFFBC05, 0x664A1C39 },
  { 0x03F868, 0x000C000F, 0x08060000 },
  { 0x03F870, 0x66D20405, 0x00A141FA },
  { 0x03F878, 0x0028DA45, 0x3A305000 },
  { 0x03F880, 0x00458010, 0x380513C5 },
  { 0x03F888, 0x000C0009, 0xE04C13C4 },
  { 0x03F890, 0x000C001F, 0x06050005 },
  { 0x03F898, 0x13C5000C, 0x001F60A4 },
  { 0x03F8A0, 0x1A001B00, 0x1C001D00 },
  { 0x03F8A8, 0x1E011F02, 0x20032104 },
  { 0x03F8B0, 0x13FC0010, 0x000C001F },
  { 0x03F8B8, 0x13C5000B, 0xDFFF60B2 },
  { 0x03F8C0, 0x024500FF, 0x67840C05 },
  { 0x03F8C8, 0x00B06D90, 0x1C39000B },
  { 0x03F8D0, 0xDFFEBC05, 0x13C5000B },
  { 0x03F8D8, 0xDFFE0405, 0x00D041FA },
  { 0x03F8E0, 0x00760285, 0x000000FF },
  { 0x03F8E8, 0xDA05D1C5, 0x1A180005 },
  { 0x03F8F0, 0x00801410, 0x1839000C },
  { 0x03F8F8, 0x000F0804, 0x00016614 },
  { 0x03F900, 0x13C5000C, 0x001F0002 },
  { 0x03F908, 0x002013C2, 0x000C001F },
  { 0x03F910, 0x6000FF32, 0x08040002 },
  { 0x03F918, 0x661413C5, 0x000C001F },
  { 0x03F920, 0x00020040, 0x13C2000C },
  { 0x03F928, 0x001F6000, 0xFF180804 },
  { 0x03F930, 0x00036614, 0x13C5000C },
  { 0x03F938, 0x001F0002, 0x008013C2 },
  { 0x03F940, 0x000C001F, 0x6000FEFE },
  { 0x03F948, 0x13FC0020, 0x000C001F },
  { 0x03F950, 0x4E714E71, 0x609E0106 },
  { 0x03F958, 0x02000301, 0x04020607 },
  { 0x03F960, 0x06000700, 0x00000800 },
  { 0x03F968, 0x09000A00, 0x00000C03 },
  { 0x03F970, 0x0A0F0000, 0x00000E00 },
  { 0x03F978, 0x13001003, 0x11071205 },
  { 0x03F980, 0x13001400, 0x15001600 },
  { 0x03F988, 0x17041803, 0x1903D0D1 },
  { 0x03F990, 0xD2D3D4D5, 0xD6D8D9DA },
  { 0x03F998, 0xDBDCDDE0, 0xE1E2E3E4 },
  { 0x03F9A0, 0xE5E6E7E8, 0xE9EAEBA1 },
  { 0x03F9A8, 0xA2A3A4A5, 0xA6A7A800 },
/*
  { 0x03FC00, 0x7000E249, 0x640408C0 },		// DSW bit reverse
  { 0x03FC08, 0x000FE249, 0x640408C0 },
  { 0x03FC10, 0x000EE249, 0x640408C0 },
  { 0x03FC18, 0x000DE249, 0x640408C0 },
  { 0x03FC20, 0x000CE249, 0x640408C0 },
  { 0x03FC28, 0x000BE249, 0x640408C0 },
  { 0x03FC30, 0x000AE249, 0x640408C0 },
  { 0x03FC38, 0x0009E249, 0x640408C0 },
  { 0x03FC40, 0x0008E249, 0x640408C0 },
  { 0x03FC48, 0x0007E249, 0x640408C0 },
  { 0x03FC50, 0x0006E249, 0x640408C0 },
  { 0x03FC58, 0x0005E249, 0x640408C0 },
  { 0x03FC60, 0x0004E249, 0x640408C0 },
  { 0x03FC68, 0x0003E249, 0x640408C0 },
  { 0x03FC70, 0x0002E249, 0x640408C0 },
  { 0x03FC78, 0x0001E249, 0x640408C0 },
  { 0x03FC80, 0x0000E158, 0x320033C1 },
  { 0x03FC88, 0x000B9006, 0x4E754E76 },
*/
  { -1,       0,          0          },
};

void LoadThunderDragon(void)
{
   int ta;

   romset=5;

   if(!(ROM=AllocateMem(0x100000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("thund.6", RAM, 0x20000)) return;    	// 8x8 FG0 TILES
   if(!NMKDecodeFG0(RAM,0x20000))return;

   if(!load_rom("thund.4", ROM, 0x100000)) return;      // 16x16 TILES
   ByteSwap(ROM,0x100000);
   if(!NMKDecodeSPR(ROM,0x100000))return;

   if(!load_rom("thund.5", ROM, 0x100000)) return;      // 16x16 TILES
   if(!NMKDecodeBG0(ROM+0x00000,0x80000))return;
   if(!NMKDecodeBG1(ROM+0x80000,0x80000))return;

   if(!load_rom("thund.8", RAM+0x00000, 0x20000)) return;  	 // 68000 ROM
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+0]=RAM[ta];
   }
   if(!load_rom("thund.7", RAM+0x00000, 0x20000)) return;
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   // Protection

   patch_rom(ROM,thunder_dragon_patch_0);

   // Missing Sprite Problem

   WriteWord68k(&ROM[0x00308],0x4E71);

   // Speed Hack

   WriteLong68k(&ROM[0x09628],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x0962C],0x00AA0000);       //

   WriteLong68k(&ROM[0x0963E],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x09642],0x00AA0000);       //

   // Make Test Mode Easier
/*
   WriteLong68k(&ROM[0x0053A],0x4E714E71);
*/
/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x30000);

   AddNMKMainCPU(0x0B0000, 0x0C0000);

   AddNMKControls();
}

void LoadThunderDragonBl(void)
{
   int ta;

   romset=6;

   if(!(ROM=AllocateMem(0x100000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("td_08.bin", RAM, 0x20000)) return;   	   	 // 8x8 FG0 TILES
   if(!NMKDecodeFG0(RAM,0x20000))return;

   if(!load_rom("td_10.bin", ROM+0x00000, 0x80000)) return;      // 16x16 TILES
   if(!load_rom("td_09.bin", ROM+0x80000, 0x80000)) return;      // 16x16 TILES
   DecodeThunderDragon_OBJ(ROM);
   if(!NMKDecodeSPR_Mustang(ROM,0x100000))return;

   if(!load_rom("td_06.bin", ROM+0x00000, 0x80000)) return;        // 16x16 TILES
   DecodeThunderDragon_OBJ(ROM);
   if(!NMKDecodeBG0(ROM,0x80000))return;

   if(!load_rom("td_07.bin", ROM+0x00000, 0x80000)) return;        // 16x16 TILES
   DecodeThunderDragon_OBJ(ROM);
   if(!NMKDecodeBG1(ROM,0x80000))return;

   if(!load_rom("td_04.bin", RAM+0x00000, 0x20000)) return;  	 // 68000 ROM
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+0]=RAM[ta];
   }
   if(!load_rom("td_03.bin", RAM+0x00000, 0x20000)) return;
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   DecodeThunderDragon_ROM(ROM);

   RAMSize=0x40000;

   /*-------[SOUND SYSTEM INIT]-------*/

   Z80ROM=RAM+0x30000;
   if(!load_rom("td_02.bin", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   Z80ROM[0x0038]=0xCD;	//	CALL	$0010
   Z80ROM[0x0039]=0x10;	//
   Z80ROM[0x003A]=0x00;	//
   Z80ROM[0x003B]=0xED;	//	RETI
   Z80ROM[0x003C]=0x4D;	//

   Z80ROM[0x0066]=0xCD;	//	CALL	$0018
   Z80ROM[0x0067]=0x18;	//
   Z80ROM[0x0068]=0x00;	//
   Z80ROM[0x0069]=0xED;	//	RETN
   Z80ROM[0x006A]=0x45;	//

   // Apply Speed Patch
   // -----------------

   Z80ROM[0x0123]=0xD3;	// OUTA (AAh)
   Z80ROM[0x0124]=0xAA;	//

   SetStopZ80Mode2(0x011A);

   // Setup Z80 memory map
   // --------------------

   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);

   AddZ80AReadByte(0x0000, 0x27FF, NULL,			NULL);	// Z80 ROM/RAM
   //AddZ80AReadByte(0x400E, 0x400F, YM2151ReadZ80,		NULL);	// YM2151 I/O
   //AddZ80AReadByte(0x4000, 0x401F, TDSoundReadZ80,		NULL);	// 68000 + OTHER I/O
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,		NULL);	// <bad reads>
   AddZ80AReadByte(-1, -1, NULL, NULL);

   AddZ80AWriteByte(0x2000, 0x27FF, NULL,			NULL);	// Z80 RAM
   //AddZ80AWriteByte(0x400E, 0x400F, YM2151WriteZ80,		NULL);	// YM2151 I/O
   //AddZ80AWriteByte(0x4000, 0x401F, TDSoundWriteZ80,		NULL);	// 68000 + OTHER I/O
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,		NULL);	// <bad writes>
   AddZ80AWriteByte(-1, -1, NULL, NULL);

   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,           NULL);
   AddZ80AReadPort(  -1,   -1, NULL,                    NULL);

   AddZ80AWritePort(0xAA, 0xAA, StopZ80Mode2,           NULL);
   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,         NULL);
   AddZ80AWritePort(  -1,   -1, NULL,                   NULL);

   AddZ80AInit();

   /*---------------------------------*/

   scr_x = 256;
   scr_y = 224;

   // Missing Sprite Problem

   WriteWord68k(&ROM[0x00308],0x4E71);

   // DSW reverse unwanted

   WriteWord68k(&ROM[0x09378],0x33C1);
   WriteLong68k(&ROM[0x0937A],0x000B9006);

   // Scroll Sync

   //WriteWord68k(&ROM[0x0959C],0x660C);

   // Speed Hack

   WriteLong68k(&ROM[0x09628],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x0962C],0x00AA0000);       //

   WriteLong68k(&ROM[0x0963E],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x09642],0x00AA0000);       //

   // Make Test Mode Easier
/*
   WriteLong68k(&ROM[0x0053A],0x4E714E71);
*/

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x30000);

   AddNMKMainCPU(0x0B0000, 0x0C0000);

   AddNMKControls();
}

void LoadStrahl(void)
{
   int ta;

   romset=7;

   if(!(ROM=AllocateMem(0x180000))) return;

   if(!load_rom("strahl-3.73", ROM, 0x10000)) return;               // 8x8 FG0 TILES
   if(!NMKDecodeFG0(ROM,0x10000))return;

   if(!load_rom("strl3-01.32", ROM+0x000000, 0x80000)) return;      // 16x16 TILES
   if(!load_rom("strl4-02.57", ROM+0x080000, 0x80000)) return;      // 16x16 TILES
   if(!load_rom("strl5-03.58", ROM+0x100000, 0x80000)) return;      // 16x16 TILES
   if(!NMKDecodeSPR(ROM,0x180000))return;

   if(!load_rom("str6b1w1.776", ROM+0x00000, 0x80000)) return;      // 16x16 TILES
   if(!NMKDecodeBG0(ROM,0x80000))return;

   if(!load_rom("str7b2r0.275", ROM+0x00000, 0x40000)) return;      // 16x16 TILES
   if(!NMKDecodeBG1(ROM,0x40000))return;

   FreeMem(ROM);
   if(!(ROM=AllocateMem(0x80000))) return;
   if(!(RAM=AllocateMem(0x60000))) return;

   if(!load_rom("strahl-2.82", RAM+0x00000, 0x20000)) return;       // 68000 ROM
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+0]=RAM[ta];
   }
   if(!load_rom("strahl-1.83", RAM+0x00000, 0x20000)) return;
   memset(RAM+0x20000,0xFF,0x10000);
   for(ta=0;ta<0x30000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   RAMSize=0x40000;

   scr_x = 256;
   scr_y = 224;

   // Fix Checksum

   WriteWord68k(&ROM[0x008B6],0x4E71);
   WriteWord68k(&ROM[0x0093E],0x4E71);

   // Protection (like task force harrier)

   WriteWord68k(&ROM[0x01CDE],0x6004);
   WriteWord68k(&ROM[0x01D0C],0x4E71);
   WriteWord68k(&ROM[0x01D36],0x4E71);

   WriteLong68k(&ROM[0x01D42],0x44FC0000);
   WriteWord68k(&ROM[0x01D46],0x4E75);

   WriteLong68k(&ROM[0x01D96],0x44FC0000);
   WriteWord68k(&ROM[0x01D9A],0x4E75);

   // Speed Hack

   WriteLong68k(&ROM[0x01462],0x13FC0000);       // move.b #$00,$AA0000
   WriteLong68k(&ROM[0x01466],0x00AA0000);       //

   // Scroll Hack

   WriteWord68k(&ROM[0x0B1EC],0x003E + 0x00);
   WriteWord68k(&ROM[0x0B21C],0x0042 + 0x00);
   WriteWord68k(&ROM[0x0B264],0x0046 + 0x00);
   WriteWord68k(&ROM[0x0B294],0x004A + 0x00);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddNMKMainCPU(0x0F0000, 0x080000);

   AddNMKControls();
}

void clear_nmk(void)
{
   #ifdef RAINE_DEBUG
      if(romset==8){
      save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x060000,1);
      }
   #endif
}

// Int#1 Timer (BPS).
// Need to hand tune this for Bomb Jack Twin music looping

static int int_rate[ROM_COUNT]=
{
   102,
   60,
   60,
   60,
   60,
   60,
   60,
   60,
   60,
};

static int tick;

void ExecuteNMKFrame(void)
{

   if(romset==1){

   // Protection :(

   WriteWord(&RAM[0x0E000],0x0008);	// INPUT ADDRESS
   WriteWord(&RAM[0x0E002],0x0000);
   WriteWord(&RAM[0x0E004],0x0008);	// INPUT ADDRESS
   WriteWord(&RAM[0x0E006],0x0002);
   WriteWord(&RAM[0x0E008],0x0008);	// INPUT ADDRESS
   WriteWord(&RAM[0x0E00A],0x0008);

   WriteWord(&RAM[0x0EFC0],ReadWord(&RAM[0x10000]));	// INPUT MIRROR

   }

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(16,60));
   #ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif
   cpu_interrupt(CPU_68K_0, 4);
   cpu_interrupt(CPU_68K_0, 2);

   tick += int_rate[romset];

   while(tick>=60){
      cpu_interrupt(CPU_68K_0, 1);
//      cpu_interrupt(CPU_68K_0, 2);
      tick-=60;
   }
/*
   #ifdef RAINE_DEBUG
   if(key[KEY_G]) int_rate[romset]--;
   if(key[KEY_H]) int_rate[romset]++;

   print_ingame(60,"Int#1 @ %d bps",int_rate[romset]);
   #endif
*/

   //print_ingame(60,"0x%04X 0x%04X",ReadWord(&RAM[0x10000]),ReadWord(&RAM[0x10002]));

   if(romset==6){
   cpu_execute_cycles(CPU_Z80_0, CPU_FRAME_MHz(4,60));	// Z80 4MHz (60fps)
   #ifdef RAINE_DEBUG
      print_debug("Z80PC0:%04x\n",z80pc);
   #endif
   cpu_interrupt(CPU_Z80_0, 0x38);
   //cpu_int_nmi(CPU_Z80_0);
   }

   ExecuteSoundFrame();
}

/*

-----+--------+--------------------
Byte | Bit(s) | Use
-----+76543210+--------------------
  1  |.......x| Sprite Enable?
  2  |......x.| Flip X Axis alt?
  2  |.......x| Flip Y Axis alt?
  3  |xxxx....| Chain X Axis
  3  |....xxxx| Chain Y Axis
  6  |x.......| Flip X Axis
  6  |.x......| Flip Y Axis
  6  |...xxxxx| Tile Number (high)
  7  |xxxxxxxx| Tile Number (low)
  8  |.......x| X (high)
  9  |xxxxxxxx| X (low)
 12  |.......x| Y (high)
 13  |xxxxxxxx| Y (low)
 15  |...xxxxx| Colour Bank
-----+--------+--------------------

*/

static void RenderNMKSprites(int pri)
{
   int x,y,zz,r1,ta,xx,yy,xp,xxp;
   UINT8 *MAP;
   UINT8 col_bank;
   UINT8 x_ofs;

   if(romset==7)
      zz=0xF000;
   else
      zz=0x8000;

   if(romset==2)
      col_bank = 0x20;
   else
      col_bank = 0x10;

   r1 = 255;

   if(romset==0)
      x_ofs = 96;
   else
      x_ofs = 32;

   do {
      if((ReadWord(&RAM[zz]))!=0){		// Strahl - Check other games are ok

      x=(ReadWord(&RAM[zz+ 8])+x_ofs)&0x1FF;
      y=(ReadWord(&RAM[zz+12])+16)&0x1FF;

            ta=ReadWord(&RAM[zz+ 6])&0x3FFF;

            MAP_PALETTE_MAPPED_NEW(
               (RAM[zz+14]&0x1F)+col_bank,
               16,
               MAP
            );

            xx=(RAM[zz+2]&0x0F)+1;
            yy=(RAM[zz+2]>>4)+1;

            xp=x;
            xxp=xx;

            do{
            do{

            if(SPR_Mask[ta]!=0){                      // No pixels; skip

            if((x>16)&&(y>16)&&(x<scr_x+32)&&(y<scr_y+32)){

            if(SPR_Mask[ta]==1){                      // Some pixels; trans

            switch(RAM[zz+3]&0x03){
            case 0x00: Draw16x16_Trans_Mapped_Rot(&GFX_SPR[ta<<8],x,y,MAP);        break;
            case 0x01: Draw16x16_Trans_Mapped_FlipY_Rot(&GFX_SPR[ta<<8],x,y,MAP);  break;
            case 0x02: Draw16x16_Trans_Mapped_FlipX_Rot(&GFX_SPR[ta<<8],x,y,MAP);  break;
            case 0x03: Draw16x16_Trans_Mapped_FlipXY_Rot(&GFX_SPR[ta<<8],x,y,MAP); break;
            }

            }
            else{                                     // all pixels; solid

            switch(RAM[zz+3]&0x03){
            case 0x00: Draw16x16_Mapped_Rot(&GFX_SPR[ta<<8],x,y,MAP);        break;
            case 0x01: Draw16x16_Mapped_FlipY_Rot(&GFX_SPR[ta<<8],x,y,MAP);  break;
            case 0x02: Draw16x16_Mapped_FlipX_Rot(&GFX_SPR[ta<<8],x,y,MAP);  break;
            case 0x03: Draw16x16_Mapped_FlipXY_Rot(&GFX_SPR[ta<<8],x,y,MAP); break;
            }

            }

            }

            }

            ta++;
            x=(x+16)&0x1FF;

            }while(--xx);

            x=xp;
            xx=xxp;
            y=(y+16)&0x1FF;

            }while(--yy);
      }
      zz+=16;
   } while(--r1);
}

static UINT8 bg_order[ROM_COUNT][3]={
      {0,1,2},                          // 0 - Bomb Jack Twin
      {0,1,2},                          // 1 - Hacha Mecha Fighter
      {0,1,2},                          // 2 - Battle Ship Paladin
      {0,1,2},                          // 3 - Mustang
      {0,1,2},                          // 4 - Task Force Harrier
      {0,1,2},                          // 5 - Thunder Dragon
      {0,1,2},                          // 6 - Thunder Dragon Bootleg
      {0,1,2},                          // 7 - Strahl
      {0,1,2},                          // 8 - Macross
};

static int NMKLayerCount;

typedef struct NMKLAYER
{
   UINT8 *RAM;
   UINT8 *GFX16;
   UINT8 *GFX8;
   UINT8 *MSK16;
   UINT8 *MSK8;
   UINT8 *SCR;
   UINT8 PAL;
} NMKLAYER;

static struct NMKLAYER NMKLayers[3];

void RenderNMKLayer(int layer)
{
   UINT8 *RAM_BG,*SCR_BG,*GFX_BG16,*MSK_BG16;
   UINT8 *GFX_BG8,*MSK_BG8,PAL_BG;
   UINT8 *MAP;
   int x,y,x16,y16,zz,zzz,zzzz,ta;

   layer = bg_order[romset][layer];

   RAM_BG       =NMKLayers[layer].RAM;
   SCR_BG       =NMKLayers[layer].SCR;
   GFX_BG16     =NMKLayers[layer].GFX16;
   MSK_BG16     =NMKLayers[layer].MSK16;
   GFX_BG8      =NMKLayers[layer].GFX8;
   MSK_BG8      =NMKLayers[layer].MSK8;
   PAL_BG       =NMKLayers[layer].PAL;

   if((ReadWord(&SCR_BG[4])&0x0010)==0){        // 16x16

   if(GFX_BG16!=NULL){                          // HAVE GFX

   if(NMKLayerCount==0){                     // **** SOLID ****

   switch(ReadWord(&SCR_BG[4])&0x0003){

   case 0x00:                                   // <<<<$1000x$200>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0100)>>4)<<9;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x0FF0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz&=0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

      Draw16x16_Mapped_Rot(&GFX_BG16[(ReadWord(&RAM_BG[zz])&0xFFF)<<8],x,y,MAP);
   zz+=2;
   if((zz&0x1F)==0){zz+=0x1FE0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0x1FE0)==0){zzzz-=0x2000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x01:                                   // <<<<$800x$400>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0300)>>4)<<8;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x07F0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

      Draw16x16_Mapped_Rot(&GFX_BG16[(ReadWord(&RAM_BG[zz])&0xFFF)<<8],x,y,MAP);
   zz+=2;
   if((zz&0x1F)==0){zz+=0xFE0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0xFE0)==0){zzzz-=0x1000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x02:                                   // <<<<$400x$800>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0700)>>4)<<7;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x03F0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

      Draw16x16_Mapped_Rot(&GFX_BG16[(ReadWord(&RAM_BG[zz])&0xFFF)<<8],x,y,MAP);
   zz+=2;
   if((zz&0x1F)==0){zz+=0x7E0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0x7E0)==0){zzzz-=0x800;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x03:                                   // <<<<$200x$1000>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0F00)>>4)<<6;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x01F0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

      Draw16x16_Mapped_Rot(&GFX_BG16[(ReadWord(&RAM_BG[zz])&0xFFF)<<8],x,y,MAP);
   zz+=2;
   if((zz&0x1F)==0){zz+=0x3E0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0x3E0)==0){zzzz-=0x400;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   }
   NMKLayerCount++;
   }                                            // END SOLID
   else{                                        // **** TRANSPARENT ****

   switch(ReadWord(&SCR_BG[4])&3){

   case 0x00:                                   // <<<<$1000x$200>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0100)>>4)<<9;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x0FF0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG16[ta]!=0){                      // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG16[ta]==1){                   // Some pixels; trans
            Draw16x16_Trans_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw16x16_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x1F)==0){zz+=0x1FE0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0x1FE0)==0){zzzz-=0x2000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x01:                                   // <<<<$800x$400>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0300)>>4)<<8;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x07F0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG16[ta]!=0){                      // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG16[ta]==1){                   // Some pixels; trans
            Draw16x16_Trans_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw16x16_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x1F)==0){zz+=0xFE0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0xFE0)==0){zzzz-=0x1000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x02:                                   // <<<<$400x$800>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0700)>>4)<<7;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x03F0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG16[ta]!=0){                      // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG16[ta]==1){                   // Some pixels; trans
            Draw16x16_Trans_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw16x16_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x1F)==0){zz+=0x7E0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0x7E0)==0){zzzz-=0x800;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x03:                                   // <<<<$200x$1000>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F0)>>4)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0F00)>>4)<<6;                  // Y Offset (256-nn)
   y16=zzz&15;                                  // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x01F0)>>4)<<5;                  // X Offset (16-nn)
   x16=zzz&15;                                  // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=16){
   for(y=(32-y16);y<(scr_y+32);y+=16){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG16[ta]!=0){                      // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (RAM_BG[1+zz]>>4)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG16[ta]==1){                   // Some pixels; trans
            Draw16x16_Trans_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw16x16_Mapped_Rot(&GFX_BG16[ta<<8],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x1F)==0){zz+=0x3E0;zz&=0x3FFF;}
   }
   zzzz+=0x20;
   if((zzzz&0x3E0)==0){zzzz-=0x400;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   }
   NMKLayerCount++;
   }                                            // END TRANSPARENT
   }                                            // END HAVE GFX
   }                                            // END 16x16
   else{                                        // 8x8

   if(GFX_BG8!=NULL){                           // HAVE GFX

   if(NMKLayerCount==0){                     // **** SOLID ****

   zz=4;
   if(romset==0) zz=4+(64*2);

   for(x=32;x<scr_x+32;x+=8,zz+=8){
   for(y=32;y<scr_y+32;y+=8,zz+=2){

      MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM_BG[zz])>>12)|PAL_BG,
               16,
               MAP
            );

      Draw8x8_Mapped_Rot(&GFX_BG8[(ReadWord(&RAM_BG[zz])&0xFFF)<<6],x,y,MAP);
   }
   }
   NMKLayerCount++;
   }                                            // END SOLID
   else{                                        // **** TRANSPARENT ****

   if(ReadLong(&SCR_BG[0])==0){

   zz=4;
   if(romset==0) zz=4+(64*2);

   for(x=32;x<scr_x+32;x+=8,zz+=8){
   for(y=32;y<scr_y+32;y+=8,zz+=2){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG8[ta]!=0){

            MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM_BG[zz])>>12)|PAL_BG,
               16,
               MAP
            );

         Draw8x8_Trans_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
      }
   }
   }

   }
   else{

   //print_ingame(60,"%04x",ReadWord(&SCR_BG[4])&3);

   switch(ReadWord(&SCR_BG[4])&3){

   case 0x00:                                   // <<<<$800x$100>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F8)>>3)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0000)>>3)<<9;                  // Y Offset (256-nn)
   y16=zzz&7;                                   // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x07F8)>>3)<<6;                  // X Offset (16-nn)
   x16=zzz&7;                                   // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=8){
   for(y=(32-y16);y<(scr_y+32);y+=8){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG8[ta]!=0){                       // No pixels; skip

            MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM_BG[zz])>>12)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG8[ta]==1){                    // Some pixels; trans
            Draw8x8_Trans_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw8x8_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x3F)==0){zz+=0x3FC0;zz&=0x3FFF;}
   }
   zzzz+=0x40;
   if((zzzz&0x3FC0)==0){zzzz-=0x4000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x02:                                   // <<<<$400x$200>>>> (yes, this is reversed)
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F8)>>3)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0100)>>3)<<8;                  // Y Offset (256-nn)
   y16=zzz&7;                                   // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x03F8)>>3)<<6;                  // X Offset (16-nn)
   x16=zzz&7;                                   // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=8){
   for(y=(32-y16);y<(scr_y+32);y+=8){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG8[ta]!=0){                       // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM_BG[zz])>>12)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG8[ta]==1){                    // Some pixels; trans
            Draw8x8_Trans_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw8x8_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x3F)==0){zz+=0x1FC0;zz&=0x3FFF;}
   }
   zzzz+=0x40;
   if((zzzz&0x1FC0)==0){zzzz-=0x2000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x01:                                   // <<<<$200x$400>>>> (yes, this is reversed)
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F8)>>3)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0300)>>3)<<7;                  // Y Offset (256-nn)
   y16=zzz&7;                                   // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x01F8)>>3)<<6;                  // X Offset (16-nn)
   x16=zzz&7;                                   // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=8){
   for(y=(32-y16);y<(scr_y+32);y+=8){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG8[ta]!=0){                       // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM_BG[zz])>>12)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG8[ta]==1){                    // Some pixels; trans
            Draw8x8_Trans_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw8x8_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x3F)==0){zz+=0xFC0;zz&=0x3FFF;}
   }
   zzzz+=0x40;
   if((zzzz&0xFC0)==0){zzzz-=0x1000;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   case 0x03:                                   // <<<<$100x$800>>>>
   zzz=(ReadWord(&SCR_BG[2])+16);
   zzzz =((zzz&0x00F8)>>3)<<1;                  // Y Offset (16-255)
   zzzz+=((zzz&0x0700)>>3)<<6;                  // Y Offset (256-nn)
   y16=zzz&7;                                   // Y Offset (0-15)
   zzz=ReadWord(&SCR_BG[0]);
   zzzz+=((zzz&0x00F8)>>3)<<6;                  // X Offset (16-nn)
   x16=zzz&7;                                   // X Offset (0-15)

   zzzz=zzzz&0x3FFF;
   zz=zzzz;
   for(x=(32-x16);x<(scr_x+32);x+=8){
   for(y=(32-y16);y<(scr_y+32);y+=8){
      ta=ReadWord(&RAM_BG[zz])&0xFFF;
      if(MSK_BG8[ta]!=0){                       // No pixels; skip

      MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM_BG[zz])>>12)|PAL_BG,
               16,
               MAP
            );

         if(MSK_BG8[ta]==1){                    // Some pixels; trans
            Draw8x8_Trans_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
         else{                                  // all pixels; solid
            Draw8x8_Mapped_Rot(&GFX_BG8[ta<<6],x,y,MAP);
         }
      }
   zz+=2;
   if((zz&0x3F)==0){zz+=0x7C0;zz&=0x3FFF;}
   }
   zzzz+=0x40;
   if((zzzz&0x7C0)==0){zzzz-=0x800;}
   zzzz&=0x3FFF;
   zz=zzzz;
   }
   break;
   }

   }

   NMKLayerCount++;
   }                                            // END TRANSPARENT
   }                                            // END HAVE GFX

   }                                            // END 8x8
}

void DrawNMK(void)
{
   int ta,tb;

   ClearPaletteMap();

   NMKLayerCount=0;

   if(RefreshBuffers){
   NMKLayers[0].RAM          =RAM+0x22000;
   NMKLayers[0].GFX16        =GFX_BG1;
   NMKLayers[0].GFX8         =NULL;
   NMKLayers[0].MSK16        =BG1_Mask;
   NMKLayers[0].MSK8         =NULL;
   NMKLayers[0].SCR          =RAM+0x14208;
   NMKLayers[0].PAL          =0x00;

   NMKLayers[1].RAM          =RAM+0x20000;
   NMKLayers[1].GFX16        =GFX_BG0;
   NMKLayers[1].GFX8         =NULL;
   NMKLayers[1].MSK16        =BG0_Mask;
   NMKLayers[1].MSK8         =NULL;
   NMKLayers[1].SCR          =RAM+0x14200;
   NMKLayers[1].PAL          =0x10;

   NMKLayers[2].RAM          =RAM+0x2C000;
   NMKLayers[2].GFX16        =NULL;
   NMKLayers[2].GFX8         =GFX_FG0;
   NMKLayers[2].MSK16        =NULL;
   NMKLayers[2].MSK8         =FG0_Mask;
   NMKLayers[2].SCR          =RAM+0x14008;
   NMKLayers[2].PAL          =0x30;

   if(romset==1){
   NMKLayers[2].PAL          =0x20;
   }

   }

   for(tb=0;tb<16;tb+=2){
      ta = (RAM[0x1C001+tb+tb]<<8) | (RAM[0x1C003+tb+tb]);
      WriteWord(&RAM[0x14200+tb], ta);
   }

   WriteLong(&RAM[0x14008],0x00000000);
   WriteLong(&RAM[0x1400C],0xFFFFFFFF);

   if(romset!=1){
   //ta = (ReadWord(&RAM[0x000F0])&0x0F)<<13;
   ta = (ReadWord(&RAM[0x000F2])&0x07)<<13;
   NMKLayers[0].RAM = ROM+0x40000+ta;
   }

   RenderNMKLayer(0);

   RenderNMKSprites(0);

   RenderNMKLayer(1);

   RenderNMKLayer(2);
}

void DrawMustang(void)
{
   int ta,tb;

   ClearPaletteMap();

   NMKLayerCount=0;

   if(RefreshBuffers){
   NMKLayers[1].RAM          =RAM+0x20000;
   NMKLayers[1].GFX16        =GFX_BG0;
   NMKLayers[1].GFX8         =NULL;
   NMKLayers[1].MSK16        =BG0_Mask;
   NMKLayers[1].MSK8         =NULL;
   NMKLayers[1].SCR          =RAM+0x14200;
   NMKLayers[1].PAL          =0x00;			// OK

   NMKLayers[2].RAM          =RAM+0x2C000;
   NMKLayers[2].GFX16        =NULL;
   NMKLayers[2].GFX8         =GFX_FG0;
   NMKLayers[2].MSK16        =NULL;
   NMKLayers[2].MSK8         =FG0_Mask;
   NMKLayers[2].SCR          =RAM+0x14008;
   NMKLayers[2].PAL          =0x20;
   }

   for(tb=0;tb<16;tb+=2){
      ta = (RAM[0x1C000+tb+tb]<<8) | (RAM[0x1C002+tb+tb]);
      WriteWord(&RAM[0x14200+tb], ta);
   }

   WriteLong(&RAM[0x14008],0x00000000);
   WriteLong(&RAM[0x1400C],0xFFFFFFFF);

   RenderNMKLayer(1);

   RenderNMKSprites(0);

   RenderNMKLayer(2);
}

void DrawBombJackTwin(void)
{
   int ta,x,y,zz;
   UINT8 *map;
   UINT8 *GFX_BG;

   ClearPaletteMap();

   GFX_BG = GFX_BG0 + ((ReadWord(&RAM[0x24000])&0x0F)*0x20000);

   zz = 0x2CE00+4;
   for(x=32;x<(scr_x+32);x+=8){
   for(y=32;y<(scr_y+32);y+=8){

      ta = ReadWord(&RAM[zz]);

      MAP_PALETTE_MAPPED_NEW(
         ta>>12,
         16,
         map
      );

      if((ta&0x800)==0)
         Draw8x8_Mapped_Rot(&GFX_FG0[(ta&0x7FF)<<6],x,y,map);
      else
         Draw8x8_Mapped_Rot(&GFX_BG[(ta&0x7FF)<<6],x,y,map);

      zz+=2;
   }
   zz+=8;
   }

   RenderNMKSprites(0);
}

void DrawTaskForceHarrier(void)
{
   ClearPaletteMap();

   NMKLayerCount=0;

   if(RefreshBuffers){
   NMKLayers[0].RAM          =RAM+0x20000;
   NMKLayers[0].GFX16        =GFX_BG0;
   NMKLayers[0].GFX8         =NULL;
   NMKLayers[0].MSK16        =BG0_Mask;
   NMKLayers[0].MSK8         =NULL;
   NMKLayers[0].SCR          =RAM+0x14200;
   NMKLayers[0].PAL          =0x00;

   NMKLayers[2].RAM          =RAM+0x2D000;
   NMKLayers[2].GFX16        =NULL;
   NMKLayers[2].GFX8         =GFX_FG0;
   NMKLayers[2].MSK16        =NULL;
   NMKLayers[2].MSK8         =FG0_Mask;
   NMKLayers[2].SCR          =RAM+0x14008;
   NMKLayers[2].PAL          =0x00;
   }
/*
   for(tb=0;tb<16;tb+=2){
      ta = (RAM[0x1C000+tb+tb]<<8) | (RAM[0x1C002+tb+tb]);
      WriteWord(&RAM[0x14200+tb], ta);
   }
*/
   WriteLong(&RAM[0x14008],0x00000000);
   WriteLong(&RAM[0x1400C],0xFFFFFFFF);

   RenderNMKLayer(0);

   RenderNMKSprites(0);

   RenderNMKLayer(2);
}

void DrawThunderDragon(void)
{
   int ta,tb;

   ClearPaletteMap();

   NMKLayerCount=0;

   if(RefreshBuffers){
   NMKLayers[0].RAM          =RAM+0x1C000;
   NMKLayers[0].GFX16        =GFX_BG0;
   NMKLayers[0].GFX8         =NULL;
   NMKLayers[0].MSK16        =BG0_Mask;
   NMKLayers[0].MSK8         =NULL;
   NMKLayers[0].SCR          =RAM+0x14800;
   NMKLayers[0].PAL          =0x00;

   NMKLayers[2].RAM          =RAM+0x20000;
   NMKLayers[2].GFX16        =NULL;
   NMKLayers[2].GFX8         =GFX_FG0;
   NMKLayers[2].MSK16        =NULL;
   NMKLayers[2].MSK8         =FG0_Mask;
   NMKLayers[2].SCR          =RAM+0x14A00;
   NMKLayers[2].PAL          =0x20;

   }

   // GFX BG0 bank switch

   if((ReadWord(&RAM[0x10018])&0x0001)==0){
   NMKLayers[0].GFX16 = GFX_BG0;
   NMKLayers[0].MSK16 = BG0_Mask;
   }
   else{
   NMKLayers[0].GFX16 = GFX_BG1;
   NMKLayers[0].MSK16 = BG1_Mask;
   }

   // Scroll BG0 fix

   for(tb=0;tb<16;tb+=2){
      ta = (RAM[0x14000+tb+tb]<<8) | (RAM[0x14002+tb+tb]);
      WriteWord(&RAM[0x14800+tb], ta);
   }

   WriteWord(&RAM[0x14800], ReadWord(&RAM[0x090E0]));
   WriteWord(&RAM[0x14802], ReadWord(&RAM[0x090E4]));

   // Scroll FG0 fix

   WriteLong(&RAM[0x14A00],0x00000000);
   WriteLong(&RAM[0x14A04],0xFFFFFFFF);

   RenderNMKLayer(0);

   RenderNMKSprites(0);

   RenderNMKLayer(2);
}

void DrawStrahl(void)
{
   int ta,tb;

   ClearPaletteMap();

   NMKLayerCount=0;

   if(RefreshBuffers){
   NMKLayers[0].RAM          =RAM+0x20000;
   NMKLayers[0].GFX16        =GFX_BG1;
   NMKLayers[0].GFX8         =NULL;
   NMKLayers[0].MSK16        =BG1_Mask;
   NMKLayers[0].MSK8         =NULL;
   NMKLayers[0].SCR          =RAM+0x14100;
   NMKLayers[0].PAL          =0x30;

   NMKLayers[1].RAM          =RAM+0x24000;
   NMKLayers[1].GFX16        =GFX_BG0;
   NMKLayers[1].GFX8         =NULL;
   NMKLayers[1].MSK16        =BG0_Mask;
   NMKLayers[1].MSK8         =NULL;
   NMKLayers[1].SCR          =RAM+0x18100;
   NMKLayers[1].PAL          =0x20;

   NMKLayers[2].RAM          =RAM+0x2C000;
   NMKLayers[2].GFX16        =NULL;
   NMKLayers[2].GFX8         =GFX_FG0;
   NMKLayers[2].MSK16        =NULL;
   NMKLayers[2].MSK8         =FG0_Mask;
   NMKLayers[2].SCR          =RAM+0x14200;
   NMKLayers[2].PAL          =0x00;
   }

   for(tb=0;tb<16;tb+=2){
      ta = (RAM[0x14000+tb+tb]<<8) | (RAM[0x14002+tb+tb]);
      WriteWord(&RAM[0x14100+tb], ta);
      ta = (RAM[0x18000+tb+tb]<<8) | (RAM[0x18002+tb+tb]);
      WriteWord(&RAM[0x18100+tb], ta);
   }

   WriteLong(&RAM[0x14200],0x00000000);
   WriteLong(&RAM[0x14204],0xFFFFFFFF);

   RenderNMKLayer(0);

   RenderNMKLayer(1);

   RenderNMKSprites(0);

   RenderNMKLayer(2);
}

void draw_macross_2(void)
{
   int ta,tb;

   ClearPaletteMap();

   NMKLayerCount=0;

   if(RefreshBuffers){
   NMKLayers[0].RAM          =RAM+0x20000;
   NMKLayers[0].GFX16        =GFX_BG0;
   NMKLayers[0].GFX8         =NULL;
   NMKLayers[0].MSK16        =BG0_Mask;
   NMKLayers[0].MSK8         =NULL;
   NMKLayers[0].SCR          =RAM+0x1C800;
   NMKLayers[0].PAL          =0x00;

   NMKLayers[2].RAM          =RAM+0x2C000;
   NMKLayers[2].GFX16        =NULL;
   NMKLayers[2].GFX8         =GFX_FG0;
   NMKLayers[2].MSK16        =NULL;
   NMKLayers[2].MSK8         =FG0_Mask;
   NMKLayers[2].SCR          =RAM+0x1CA00;
   NMKLayers[2].PAL          =0x20;

   }

   // GFX BG0 bank switch

   ta = ReadWord(&RAM[0x10018]) & 0x0003;

   NMKLayers[0].GFX16 = GFX_BG0 + (ta * 0x100000);
   NMKLayers[0].MSK16 = BG0_Mask + (ta * 0x1000);

   // Scroll BG0 fix

   for(tb=0;tb<16;tb+=2){
      ta = (RAM[0x1C000+tb+tb]<<8) | (RAM[0x1C002+tb+tb]);
      WriteWord(&RAM[0x1C800+tb], ta);
   }

   WriteWord(&RAM[0x1C800], ReadWord(&RAM[0x090E0]));
   WriteWord(&RAM[0x1C802], ReadWord(&RAM[0x090E4]));

   // Scroll FG0 fix

   WriteLong(&RAM[0x1CA00],0x00000000);
   WriteLong(&RAM[0x1CA04],0xFFFFFFFF);


   RenderNMKLayer(0);

   RenderNMKSprites(0);

   RenderNMKLayer(2);
}
