/******************************************************************************/
/*                                                                            */
/*                  MAHJONG QUEST (C) 1990 TAITO CORPORATION                  */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "mjnquest.h"
#include "tc100scn.h"
#include "tc110pcr.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "taitosnd.h"
#include "savegame.h"
#include "sasound.h"		// sample support routines

static struct DIR_INFO mahjong_quest_dirs[] =
{
   { "mahjong_quest", },
   { "mjnquest", },
   { NULL, },
};

static struct ROM_INFO mahjong_quest_roms[] =
{
   {       "c77-01", 0x00100000, 0x5ba51205, 0, 0, 0, },
   {       "c77-02", 0x00100000, 0x6a6f3040, 0, 0, 0, },
   {       "c77-03", 0x00080000, 0x312f17b1, 0, 0, 0, },
   {       "c77-04", 0x00080000, 0xc2e7e038, 0, 0, 0, },
   {       "c77-05", 0x00080000, 0xc5a54678, 0, 0, 0, },
   {       "c77-08", 0x00020000, 0x4244f775, 0, 0, 0, },
   {       "c77-09", 0x00020000, 0x0a005d01, 0, 0, 0, },
   {       "c77-10", 0x00010000, 0xf16b2c1e, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO mahjong_quest_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03A302, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03A302, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03A302, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03A302, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x03A300, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x03A300, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x03A300, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x03A300, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x03A300, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x03A300, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x03A300, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x03A300, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x03A301, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x03A301, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x03A301, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x03A301, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x03A301, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x03A301, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x03A301, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x03A301, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_mahjong_quest_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DSWA_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO mahjong_quest_dsw[] =
{
   { 0x03A100, 0xFF, dsw_data_mahjong_quest_0 },
   { 0x03A102, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO mahjong_quest_video =
{
   draw_mahjong_quest,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_mahjong_quest =
{
   mahjong_quest_dirs,
   mahjong_quest_roms,
   mahjong_quest_inputs,
   mahjong_quest_dsw,
   NULL,

   load_mahjong_quest,
   clear_mahjong_quest,
   &mahjong_quest_video,
   execute_mahjong_quest_frame,
   "mjnquest",
   "Mahjong Quest",
   NULL,
   COMPANY_ID_TAITO,
   "C77",
   1990,
   taito_ym2610_sound,
   GAME_PUZZLE | GAME_PARTIALLY_WORKING,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_COLOUR;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BANK;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static UINT32 input_ofs;

static void mahjong_ioc_ww(UINT32 offset, UINT16 data)
{
   switch(data){
      case 0x0040:
         input_ofs = 0;
      break;
      case 0x0080:
         input_ofs = 1;
      break;
      case 0x0100:
         input_ofs = 2;
      break;
      case 0x0200:
         input_ofs = 3;
      break;
      case 0x0400:
         input_ofs = 4;
      break;
   }
}

static UINT16 mahjong_ioc_rw(UINT32 offset)
{
   return RAM[0x3A300+input_ofs];
}

void load_mahjong_quest(void)
{
   int ta,tb;

   if(!(GFX=AllocateMem(0x400000+0x100000))) return;
   if(!(RAM=AllocateMem(0x100000))) return;

   GFX_BG0	=GFX+0x000000;
   GFX_SPR	=GFX+0x400000;

   tb=0;
   if(!load_rom("c77-01", RAM, 0x100000)) return;		// 8x8 TILES
   for(ta=0;ta<0x100000;ta+=2,tb+=4){
      GFX_BG0[tb+3]=RAM[ta]&15;
      GFX_BG0[tb+2]=RAM[ta]>>4;
      GFX_BG0[tb+1]=RAM[ta+1]&15;
      GFX_BG0[tb+0]=RAM[ta+1]>>4;
   }
   if(!load_rom("c77-02", RAM, 0x100000)) return;		// 8x8 TILES
   for(ta=0;ta<0x100000;ta+=2,tb+=4){
      GFX_BG0[tb+3]=RAM[ta]&15;
      GFX_BG0[tb+2]=RAM[ta]>>4;
      GFX_BG0[tb+1]=RAM[ta+1]&15;
      GFX_BG0[tb+0]=RAM[ta+1]>>4;
   }

   tb=0;
   if(!load_rom("c77-05", RAM, 0x80000)) return;		// 16x16 SPRITES ($1000)
   for(ta=0;ta<0x80000;ta++,tb+=2){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }

   FreeMem(RAM);

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0xC0000))) return;

   if(!load_rom("c77-09", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("c77-08", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("c77-04", ROM+0x40000, 0x80000)) return;

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x48000;
   if(!load_rom("c77-10", Z80ROM, 0x10000)) return;			// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("c77-03", PCMROM, 0x80000)) return;			// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x80000, 0x80000);

   AddTaitoYM2610(0x0338, 0x02A5, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x20000;
   RAM_SCROLL = RAM+0x3A000;
   RAM_OBJECT = RAM+0x10000;
   RAM_COLOUR = RAM+0x30000;
   RAM_INPUT  = RAM+0x3A100;
   GFX_FG0    = RAM+0x40000;
   GFX_BANK   = RAM+0x3A200;

   RAMSize=0x58000;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x10000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x1000);

   tc0110pcr_init(RAM_COLOUR, 0);

   set_colour_mapper(&col_map_xbbb_bbgg_gggr_rrrr);
   InitPaletteMap(RAM_COLOUR, 0x100, 0x10, 0x8000);

   // 68000 Speed Hack
   // ----------------

   WriteLong68k(&ROM[0x004A4],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x004A8],0x00AA0000);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX_BG0;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x7FFF;
   tc0100scn[0].layer[0].scr_x	=17;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX_BG0;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x7FFF;
   tc0100scn[0].layer[1].scr_x	=17;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=17;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT+0x0000;
   tc0200obj.RAM_B	= RAM_OBJECT+0x8000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x0FFF;
   tc0200obj.ofs_x	= -93;
   tc0200obj.ofs_y	= -16;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x0BFFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x0BFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x110000, 0x11FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x500000, 0x50FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x300000, 0x30000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x360000, 0x360003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x0BFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x110000, 0x11FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x500000, 0x50FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x200000, 0x200007, tc0110pcr_rw, NULL);			// COLOR RAM
   AddReadWord(0x300000, 0x30000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x310000, 0x310001, mahjong_ioc_rw, NULL);		// KEYBOARD
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x110000, 0x11FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x500000, 0x50FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x406000, 0x406FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x420000, 0x42000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteByte(0x360000, 0x360003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x300000, 0x30000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x110000, 0x11FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x500000, 0x50FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x406000, 0x406FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x400000, 0x40FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x200000, 0x200007, tc0110pcr_ww, NULL);		// COLOR RAM
   AddWriteWord(0x420000, 0x42000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x300000, 0x30000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x380000, 0x380001, NULL, GFX_BANK);			// BANK SWITCH
   AddWriteWord(0x320000, 0x320001, mahjong_ioc_ww, NULL);		// KEYBOARD
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void clear_mahjong_quest(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("RAM.bin",RAM,0x040000,1);
   #endif
}

void execute_mahjong_quest_frame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);

   Taito2610_Frame();			// Z80 and YM2610
}

void draw_mahjong_quest(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   if(ReadWord(&GFX_BANK[0])==0){
      tc0100scn[0].layer[0].GFX  = GFX_BG0;
      tc0100scn[0].layer[0].MASK = GFX_BG0_SOLID;
      tc0100scn[0].layer[1].GFX  = GFX_BG0;
      tc0100scn[0].layer[1].MASK = GFX_BG0_SOLID;
   }
   else{
      tc0100scn[0].layer[0].GFX  = GFX_BG0 + (0x8000 * 0x40);
      tc0100scn[0].layer[0].MASK = GFX_BG0_SOLID + (0x8000);
      tc0100scn[0].layer[1].GFX  = GFX_BG0 + (0x8000 * 0x40);
      tc0100scn[0].layer[1].MASK = GFX_BG0_SOLID + (0x8000);
   }

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // OBJECT
   // ------

   render_tc0200obj_mapped();

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);
}
