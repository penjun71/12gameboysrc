
void load_american_horseshoes(void);
void clear_american_horseshoes(void);

void load_cachat(void);
void clear_cachat(void);

void LoadPlotting(void);
void ClearPlotting(void);

void LoadRaimais(void);
void ClearRaimais(void);

void LoadChampionWr(void);
void ClearChampionWr(void);

void LoadFightingHawk(void);
void ClearFightingHawk(void);

void LoadKuriKinton(void);
void LoadKuriKinton_alt(void);
void LoadKuriKinton_jap(void);
void ClearKuriKinton(void);

void LoadPuzznic(void);
void ClearPuzznic(void);

void LoadPlgirls(void);
void ClearPlgirls(void);

void LoadPlgirls2(void);
void ClearPlgirls2(void);

void LoadPalamedes(void);
void ClearPalamedes(void);

void load_cuby_bop(void);
void clear_cuby_bop(void);

void load_tube_it(void);
void clear_tube_it(void);

void ExecuteLSystemFrame(void);

void DrawLSystem(void);
void DrawLSystem_R270(void);
