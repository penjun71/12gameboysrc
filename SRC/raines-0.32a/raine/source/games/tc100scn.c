/******************************************************************************/
/*                                                                            */
/*             tc0100scn: TAITO SCREEN LAYER CHIP (BG0/BG1/FG0)               */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "tc100scn.h"

extern int layer_id_data[MAX_CHIP][3];

/*

SCREEN RAM
----------

----------+-----------------------------
 Range    | Use
----------+-----------------------------
0000-3FFF | BG0 Layer [512x512x8: 4 bytes]
4000-5FFF | FG0 Layer [512x512x8: 2 bytes]
6000-7FFF | FG0 GFX
8000-BFFF | BG1 Layer [512x512x8: 4 bytes]
C000-C7FF | Line-Line Scroll? (AquaJack)
E000-E0FF | ? (AquaJack)
----------+-----------------------------

SCROLL RAM
----------

-----+--------+-------------------------
Byte | Bit(s) | Use
-----+76543210+-------------------------
  0  |.......x| BG0 Scroll X
  1  |xxxxxxxx| BG0 Scroll X
  2  |.......x| BG1 Scroll X
  3  |xxxxxxxx| BG1 Scroll X
  4  |.......x| FG0 Scroll X
  5  |xxxxxxxx| FG0 Scroll X
  6  |.......x| BG0 Scroll Y
  7  |xxxxxxxx| BG0 Scroll Y
  8  |.......x| BG1 Scroll Y
  9  |xxxxxxxx| BG1 Scroll Y
  A  |.......x| FG0 Scroll Y
  B  |xxxxxxxx| FG0 Scroll Y
  D  |.......x| BG0 Disable
  D  |......x.| BG1 Disable
  D  |.....x..| FG0 Disable
  D  |....x...| Swap BG0:BG1
  F  |.......x| Screen Flip
  F  |..x.....| Priority for 2 chips?
-----+--------+-------------------------

Not implemented:

- FG0 seems to be offset differently in some games.
- The variable stuff is slower, unless it can use self
  modifying code, so need to convert to asm.
- Need to add support for different screen orientations
  (screen flip Y-Axis, screen rotate 90 degrees).
- Line-line scroll is used occasionally (Chase HQ).
- FG0 gfx mask is missing (might be faster, might not make
  much difference).

Direct Mapped Games:

Bonze Adventure/Jigoku	- OK
Cadash/Cadash Italian	- OK (title screen might use line-line scroll)
Mega Blast		- OK
Camel Try		- OK (only need FG0)
Growl			- OK (fg0 is different offset in test mode)
Ninja Kids		- OK
Thunder Fox		- OK (priorities between dual tc0100scn and sprites wrong)
Operation Wolf		- OK (Old/Split scroll ram)
Rastan			- ? (Old/Split scroll ram)
Rainbow Islands		- ? (Old/Split scroll ram)

Mapped Games:

Final Blow		- OK
Chase HQ		- OK (uses line-line scroll)
Pulirula		- OK
AquaJack		- OK
Dino Rex		- OK
Night Striker		- OK (uses line-line scroll)
Chase HQ 2		- ?
Continental Circus	- ?

Mapped rotate 270� games:

Maze of Flott		- OK
Earth Joker		- OK
Asuka & Asuka		- OK (don't need FG0)
Gun Frontier		- OK

Direct Mapped rotate 180� games:

Liquid Kids		- OK
Don Doko Don		- OK

Mapped Flip-Y Axis games:

Space Gun		- OK (hiscores use line-line scroll?)
Operation Thunderbolt   - OK
Battle Shark		- OK

*/

/******************************************************************************/

void render_bg0_solid_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8(bmp_x,bmp_y,bmp_w,bmp_h);

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);        break;
         case 0x40: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0x80: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map); break;
         }

      END_SCROLL_512x512_4_8();
}

void render_bg0_transparent_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8(bmp_x,bmp_y,bmp_w,bmp_h);

      ta=ReadWord(&RAM_BG[2+zz])&tile_mask;
      if(RAM_MSK[ta]!=0){			// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         if(RAM_MSK[ta]==1){			// Some pixels; trans
            switch(RAM_BG[1+zz]&0xC0){
               case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x40: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }
         else{					// all pixels; solid
            switch(RAM_BG[1+zz]&0xC0){
               case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x40: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }

      }

      END_SCROLL_512x512_4_8();
}


void render_fg0_solid_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(y=bmp_y;(UINT32)y<(bmp_h+bmp_y);y+=8,zz+=48){
      for(x=bmp_x;(UINT32)x<(bmp_w+bmp_x);x+=8,zz+=2){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);        break;
         case 0x40: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0x80: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map); break;
         }
      }
      }
}


void render_fg0_transparent_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,z;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(y=bmp_y;(UINT32)y<(bmp_h+bmp_y);y+=8,zz+=48){
      for(x=bmp_x;(UINT32)x<(bmp_w+bmp_x);x+=8,zz+=2){
         if((z=RAM_BG[zz])!=0){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[z<<6],x,y,map);        break;
         case 0x40: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0x80: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[z<<6],x,y,map); break;
         }

         }
      }
      }
}


void render_fg0scr_transparent_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   //UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   //UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   //RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   //tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_2_8(bmp_x,bmp_y,bmp_w,bmp_h);

         if((ta=RAM_BG[zz])!=0){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
         case 0x40: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
         case 0x80: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
         }

         }

      END_SCROLL_512x512_2_8();
}

void render_fg0scr_transparent_mapped_2(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   //UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   //tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_2_8(bmp_x,bmp_y,bmp_w,bmp_h);

      ta=RAM_BG[zz];

      if(RAM_MSK[ta]!=0){			// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,
            map
         );

         if(RAM_MSK[ta]==1){			// Some pixels; trans
            switch(RAM_BG[1+zz]&0xC0){
               case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x40: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }
         else{					// all pixels; solid
            switch(RAM_BG[1+zz]&0xC0){
               case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x40: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }

      }

      END_SCROLL_512x512_2_8();
}

void render_tc0100scn_layer_mapped(int chip, int layer)
{
   TC0100SCN *tchip;

   if(!check_layer_enabled(layer_id_data[chip][layer]))
       return;

   tchip = &tc0100scn[chip];

   // Check BG0/BG1 flip
   // ------------------

   if(((tchip->ctrl & 0x08)!=0)&&(layer<2)) layer^=1;

   // Check Layer Disable bits
   // ------------------------

   if((tchip->ctrl & (1<<layer))!=0) return;

   if(tc0100scn_layer_count==0){		// [SOLID]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_solid_mapped(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_solid_mapped(&tchip->layer[layer]);
      break;
   }

   }
   else{					// [TRANSPARENT]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_transparent_mapped(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_transparent_mapped(&tchip->layer[layer]);
      break;
      case SCN_FG0_SCROLL:			// [FG0: 512x512; 8x8 TILE RAM + SCROLLING]
      render_fg0scr_transparent_mapped(&tchip->layer[layer]);
      break;
      case SCN_FG0_SCROLL_2:		// [FG0: 512x512; 8x8 TILE RAM + SCROLLING]
      tc0100scn_0_update_gfx_fg0();
      render_fg0scr_transparent_mapped_2(&tchip->layer[layer]);
      break;
   }

   }

   tc0100scn_layer_count++;
}

/******************************************************************************/

void render_bg0_solid_mapped_r270(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8_R270(bmp_x,bmp_y,bmp_w,bmp_h);

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);        break;
         case 0x80: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0x40: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map); break;
         }

      END_SCROLL_512x512_4_8();
}


void render_bg0_transparent_mapped_r270(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8_R270(bmp_x,bmp_y,bmp_w,bmp_h);

      ta=ReadWord(&RAM_BG[2+zz])&tile_mask;
      if(RAM_MSK[ta]!=0){			// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         if(RAM_MSK[ta]==1){			// Some pixels; trans
            switch(RAM_BG[1+zz]&0xC0){
               case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x80: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x40: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }
         else{					// all pixels; solid
            switch(RAM_BG[1+zz]&0xC0){
               case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x80: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x40: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }

      }

      END_SCROLL_512x512_4_8();
}


void render_fg0_solid_mapped_r270(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(x=bmp_x;(UINT32)x<(bmp_w+bmp_x);x+=8,zz+=48){
      for(y=(bmp_h+bmp_y-8);(UINT32)y>=bmp_y;y-=8,zz+=2){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Mapped_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);        break;
         case 0x80: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0x40: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map); break;
         }
      }
      }
}


void render_fg0_transparent_mapped_r270(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,z;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(x=bmp_x;(UINT32)x<(bmp_w+bmp_x);x+=8,zz+=48){
      for(y=(bmp_h+bmp_y-8);(UINT32)y>=bmp_y;y-=8,zz+=2){
         if((z=RAM_BG[zz])!=0){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[z<<6],x,y,map);        break;
         case 0x80: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0x40: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[z<<6],x,y,map); break;
         }

         }
      }
      }
}


void render_fg0scr_transparent_mapped_r270(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   //UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   //UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   //RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   //tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_2_8_R270(bmp_x,bmp_y,bmp_w,bmp_h);

         if((ta=RAM_BG[zz])!=0){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x00: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
         case 0x80: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
         case 0x40: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
         }

         }

      END_SCROLL_512x512_2_8();
}


void render_tc0100scn_layer_mapped_r270(int chip, int layer)
{
   TC0100SCN *tchip;

   if(!check_layer_enabled(layer_id_data[chip][layer]))
       return;

   tchip = &tc0100scn[chip];

   // Check BG0/BG1 flip
   // ------------------

   if(((tchip->ctrl & 0x08)!=0)&&(layer<2)) layer^=1;

   // Check Layer Disable bits
   // ------------------------

   if((tchip->ctrl & (1<<layer))!=0) return;

   if(tc0100scn_layer_count==0){		// [SOLID]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_solid_mapped_r270(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_solid_mapped_r270(&tchip->layer[layer]);
      break;
   }

   }
   else{					// [TRANSPARENT]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_transparent_mapped_r270(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_transparent_mapped_r270(&tchip->layer[layer]);
      break;
      case SCN_FG0_SCROLL:			// [FG0: 512x512; 8x8 TILE RAM + SCROLLING]
      render_fg0scr_transparent_mapped_r270(&tchip->layer[layer]);
      break;
   }

   }

   tc0100scn_layer_count++;
}

/******************************************************************************/

void render_bg0_solid_mapped_flipy(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8_FLIPY(bmp_x,bmp_y,bmp_w,bmp_h);

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x40: Draw8x8_Mapped_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);        break;
         case 0x00: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0x80: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map); break;
         }

      END_SCROLL_512x512_4_8();
}

void render_bg0_transparent_mapped_flipy(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8_FLIPY(bmp_x,bmp_y,bmp_w,bmp_h);

      ta=ReadWord(&RAM_BG[2+zz])&tile_mask;
      if(RAM_MSK[ta]!=0){			// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         if(RAM_MSK[ta]==1){			// Some pixels; trans
            switch(RAM_BG[1+zz]&0xC0){
               case 0x40: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x00: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }
         else{					// all pixels; solid
            switch(RAM_BG[1+zz]&0xC0){
               case 0x40: Draw8x8_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x00: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }

      }

      END_SCROLL_512x512_4_8();
}


void render_fg0_solid_mapped_flipy(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(y=bmp_y;(UINT32)y<(bmp_h+bmp_y);y+=8,zz+=48){
      for(x=(bmp_w+bmp_x-8);(UINT32)x>=bmp_x;x-=8,zz+=2){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x40: Draw8x8_Mapped_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);        break;
         case 0x00: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0x80: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map); break;
         }
      }
      }
}

void render_fg0_transparent_mapped_flipy(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,z;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(y=bmp_y;(UINT32)y<(bmp_h+bmp_y);y+=8,zz+=48){
      for(x=(bmp_w+bmp_x-8);(UINT32)x>=bmp_x;x-=8,zz+=2){
         if((z=RAM_BG[zz])!=0){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0x40: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[z<<6],x,y,map);        break;
         case 0x00: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0xC0: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0x80: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[z<<6],x,y,map); break;
         }

         }
      }
      }
}

void render_fg0scr_transparent_mapped_flipy_2(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   //UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   //tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_2_8_FLIPY(bmp_x,bmp_y,bmp_w,bmp_h);

      ta=RAM_BG[zz];

      if(RAM_MSK[ta]!=0){			// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,
            map
         );

         if(RAM_MSK[ta]==1){			// Some pixels; trans
            switch(RAM_BG[1+zz]&0xC0){
               case 0x40: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x00: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }
         else{					// all pixels; solid
            switch(RAM_BG[1+zz]&0xC0){
               case 0x40: Draw8x8_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x00: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0xC0: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x80: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }

      }

      END_SCROLL_512x512_2_8();
}

void render_tc0100scn_layer_mapped_flipy(int chip, int layer)
{
   TC0100SCN *tchip;

   if(!check_layer_enabled(layer_id_data[chip][layer]))
       return;

   tchip = &tc0100scn[chip];

   // Check BG0/BG1 flip
   // ------------------

   if(((tchip->ctrl & 0x08)!=0)&&(layer<2)) layer^=1;

   // Check Layer Disable bits
   // ------------------------

   if((tchip->ctrl & (1<<layer))!=0) return;

   if(tc0100scn_layer_count==0){		// [SOLID]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_solid_mapped_flipy(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_solid_mapped_flipy(&tchip->layer[layer]);
      break;
   }

   }
   else{					// [TRANSPARENT]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_transparent_mapped_flipy(&tchip->layer[layer]);
      break;
      case SCN_FG0_SCROLL_2:			// [FG0: 512x512; 8x8 TILE RAM + SCROLLING]
      tc0100scn_0_update_gfx_fg0();
      render_fg0scr_transparent_mapped_flipy_2(&tchip->layer[layer]);
      break;
   }

   }

   tc0100scn_layer_count++;
}

/******************************************************************************/

void render_bg0_solid_r180_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8_R180(bmp_x,bmp_y,bmp_w,bmp_h);

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0xC0: Draw8x8_Mapped_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);        break;
         case 0x80: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0x40: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map);  break;
         case 0x00: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[(ReadWord(&RAM_BG[2+zz])&tile_mask)<<6],x,y,map); break;
         }

      END_SCROLL_512x512_4_8();
}


void render_bg0_transparent_r180_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,ta;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   UINT8 *RAM_MSK;
   mapper_direct *pal_map;
   UINT32 tile_mask;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   RAM_MSK = tc_layer->MASK;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   tile_mask = tc_layer->tile_mask;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_4_8(
         scr_x-(ReadWord(RAM_SCR+0)),
         scr_y-(ReadWord(RAM_SCR+6))
      );

      START_SCROLL_512x512_4_8_R180(bmp_x,bmp_y,bmp_w,bmp_h);

      ta=ReadWord(&RAM_BG[2+zz])&tile_mask;
      if(RAM_MSK[ta]!=0){			// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[zz],
            16,
            map
         );

         if(RAM_MSK[ta]==1){			// Some pixels; trans
            switch(RAM_BG[1+zz]&0xC0){
               case 0xC0: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x80: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x40: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x00: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }
         else{					// all pixels; solid
            switch(RAM_BG[1+zz]&0xC0){
               case 0xC0: Draw8x8_Mapped_Rot(&RAM_GFX[ta<<6],x,y,map);        break;
               case 0x80: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x40: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[ta<<6],x,y,map);  break;
               case 0x00: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[ta<<6],x,y,map); break;
            }
         }

      }

      END_SCROLL_512x512_4_8();
}


void render_fg0_solid_r180_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(y=(bmp_h+bmp_y-8);(UINT32)y>=bmp_y;y-=8,zz+=48){
      for(x=(bmp_w+bmp_x-8);(UINT32)x>=bmp_x;x-=8,zz+=2){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0xC0: Draw8x8_Mapped_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);        break;
         case 0x80: Draw8x8_Mapped_FlipY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0x40: Draw8x8_Mapped_FlipX_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map);  break;
         case 0x00: Draw8x8_Mapped_FlipXY_Rot(&RAM_GFX[RAM_BG[zz]<<6],x,y,map); break;
         }
      }
      }
}


void render_fg0_transparent_r180_mapped(TC0100SCN_LAYER *tc_layer)
{
   int x,y,x16,y16,zzzz,zzz,zz,z;
   UINT8 *map;

   UINT8 *RAM_BG;
   UINT8 *RAM_SCR;
   UINT8 *RAM_GFX;
   mapper_direct *pal_map;
   UINT32 bmp_x,bmp_y,bmp_w,bmp_h;
   UINT32 scr_x,scr_y;

   RAM_BG  = tc_layer->RAM;
   RAM_SCR = tc_layer->SCR;
   RAM_GFX = tc_layer->GFX;
   bmp_x = tc_layer->bmp_x;
   bmp_y = tc_layer->bmp_y;
   bmp_w = tc_layer->bmp_w;
   bmp_h = tc_layer->bmp_h;
   pal_map = tc_layer->mapper;
   scr_x = tc_layer->scr_x;
   scr_y = tc_layer->scr_y;

      MAKE_SCROLL_512x512_2_8(
         scr_x,		//-(ReadWord(RAM_SCR+0)),
         scr_y		//-(ReadWord(RAM_SCR+6))
      );

      zz=zzzz;
      for(y=(bmp_h+bmp_y-8);(UINT32)y>=bmp_y;y-=8,zz+=48){
      for(x=(bmp_w+bmp_x-8);(UINT32)x>=bmp_x;x-=8,zz+=2){
         if((z=RAM_BG[zz])!=0){

         MAP_PALETTE_MAPPED_NEW(
            RAM_BG[1+zz]&0x3F,
            4,				// 4 Colours
            map
         );

         switch(RAM_BG[1+zz]&0xC0){
         case 0xC0: Draw8x8_Trans_Mapped_Rot(&RAM_GFX[z<<6],x,y,map);        break;
         case 0x80: Draw8x8_Trans_Mapped_FlipY_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0x40: Draw8x8_Trans_Mapped_FlipX_Rot(&RAM_GFX[z<<6],x,y,map);  break;
         case 0x00: Draw8x8_Trans_Mapped_FlipXY_Rot(&RAM_GFX[z<<6],x,y,map); break;
         }

         }
      }
      }
}


void render_tc0100scn_layer_r180_mapped(int chip, int layer)
{
   TC0100SCN *tchip;

   if(!check_layer_enabled(layer_id_data[chip][layer]))
       return;

   tchip = &tc0100scn[chip];

   // Check BG0/BG1 flip
   // ------------------

   if(((tchip->ctrl & 0x08)!=0)&&(layer<2)) layer^=1;

   // Check Layer Disable bits
   // ------------------------

   if((tchip->ctrl & (1<<layer))!=0) return;

   if(tc0100scn_layer_count==0){		// [SOLID]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_solid_r180_mapped(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_solid_r180_mapped(&tchip->layer[layer]);
      break;
   }

   }
   else{					// [TRANSPARENT]

   switch(tchip->layer[layer].type){
      case SCN_BG0:				// [BG0/BG1: 512x512; 8x8 TILE ROM]
      render_bg0_transparent_r180_mapped(&tchip->layer[layer]);
      break;
      case SCN_FG0:				// [FG0: 512x512; 8x8 TILE RAM]
      render_fg0_transparent_r180_mapped(&tchip->layer[layer]);
      break;
   }

   }

   tc0100scn_layer_count++;
}

/******************************************************************************/
