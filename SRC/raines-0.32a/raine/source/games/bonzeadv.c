/******************************************************************************/
/*                                                                            */
/*             BONZE ADVENTURE/JIGOKU (C) 1988 TAITO CORPORATION              */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "bonzeadv.h"
#include "tc100scn.h"
#include "tc110pcr.h"
#include "tc002obj.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

/*******************
   BONZE ADVENTURE
 *******************/

static struct DIR_INFO bonze_adventure_dirs[] =
{
   { "bonze_adventure", },
   { "bonzeadv", },
   { NULL, },
};

static struct ROM_INFO bonze_adventure_roms[] =
{
   {       "b41-01", 0x00080000, 0x5d072fa4, 0, 0, 0, },
   {       "b41-02", 0x00080000, 0x29f205d9, 0, 0, 0, },
   {       "b41-03", 0x00080000, 0x736d35d0, 0, 0, 0, },
   {       "b41-04", 0x00080000, 0xc668638f, 0, 0, 0, },
   {     "b41-09-1", 0x00010000, 0xaf821fbc, 0, 0, 0, },
   {       "b41-10", 0x00010000, 0x4ca94d77, 0, 0, 0, },
   {     "b41-11-1", 0x00010000, 0x823fff00, 0, 0, 0, },
   {       "b41-15", 0x00010000, 0xaed7a0d0, 0, 0, 0, },
   {       "b41-13", 0x00010000, 0x9e464254, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO bonze_adventure_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x040009, 0x01, BIT_ACTIVE_1 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x040009, 0x02, BIT_ACTIVE_1 },
   { KB_DEF_TILT,         MSG_TILT,                0x04000B, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x040007, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x040007, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x04000B, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x04000B, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x04000B, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x04000B, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x04000B, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x04000B, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x040007, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x04000D, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x04000D, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x04000D, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x04000D, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P1_B1,               0x04000D, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x04000D, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_bonze_adventure_0[] =
{
   { MSG_CABINET,             0x01, 0x02 },
   { MSG_TABLE,               0x01, 0x00 },
   { MSG_UPRIGHT,             0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_bonze_adventure_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { MSG_EXTRA_LIFE,          0x0C, 0x04 },
   { "50k and 150k",          0x0C, 0x00 },
   { "40k and 100k",          0x08, 0x00 },
   { "60k and 200k",          0x04, 0x00 },
   { "80k and 250k",          0x00, 0x00 },
   { MSG_LIVES,               0x30, 0x04 },
   { "3",                     0x30, 0x00 },
   { "2",                     0x20, 0x00 },
   { "4",                     0x10, 0x00 },
   { "5",                     0x00, 0x00 },
   { MSG_CONTINUE_PLAY,       0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO bonze_adventure_dsw[] =
{
   { 0x020000, 0xFF, dsw_data_bonze_adventure_0 },
   { 0x020020, 0xBF, dsw_data_bonze_adventure_1 },
   { 0,        0,    NULL,      },
};

/*
static struct ROMSW_DATA romsw_data_bonze_adventure_0[] =
{
   { "Taito Japan (Japanese)", 0x00 },
   { "Taito America",          0x01 },
   { "Taito Japan",            0x02 },
   { NULL,                     0    },
};

static struct ROMSW_INFO bonze_adventure_romsw[] =
{
   { 0x03FFFF, 0x02, romsw_data_bonze_adventure_0 },
   { 0,        0,    NULL },
};
*/

static struct VIDEO_INFO bonze_adventure_video =
{
   DrawBonzeAdv,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL,
};

struct GAME_MAIN game_bonze_adventure =
{
   bonze_adventure_dirs,
   bonze_adventure_roms,
   bonze_adventure_inputs,
   bonze_adventure_dsw,
   NULL,

   LoadBonzeAdv,
   ClearBonzeAdv,
   &bonze_adventure_video,
   ExecuteBonzeAdvFrame,
   "bonzeadv",
   "Bonze's Adventure",
   "�n�������� American",
   COMPANY_ID_TAITO,
   "B41",
   1988,
   taito_ym2610_sound,
   GAME_PLATFORM,
};

/*****************
   JIGOKU MEGURI
 *****************/

static struct DIR_INFO jigoku_meguri_dirs[] =
{
   { "jigoku_meguri", },
   { "jigoku", },
   { "jigkmgri", },	/* Steph 2001.04.20 - added this name because of MAME driver's name */
   { ROMOF("bonzeadv"), },
   { CLONEOF("bonzeadv"), },
   { NULL, },
};

static struct ROM_INFO jigoku_meguri_roms[] =
{
   {       "b41-01", 0x00080000, 0x5d072fa4, 0, 0, 0, },
   {       "b41-02", 0x00080000, 0x29f205d9, 0, 0, 0, },
   {       "b41-03", 0x00080000, 0x736d35d0, 0, 0, 0, },
   {       "b41-04", 0x00080000, 0xc668638f, 0, 0, 0, },
   {     "b41-09-1", 0x00010000, 0xaf821fbc, 0, 0, 0, },
   {       "b41-10", 0x00010000, 0x4ca94d77, 0, 0, 0, },
   {     "b41-11-1", 0x00010000, 0x823fff00, 0, 0, 0, },
   {       "b41-12", 0x00010000, 0x40d9c1fc, 0, 0, 0, },
   {       "b41-13", 0x00010000, 0x9e464254, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct DSW_DATA dsw_data_jigoku_meguri_0[] =
{
   { MSG_CABINET,             0x01, 0x02 },
   { MSG_TABLE,               0x01, 0x00 },
   { MSG_UPRIGHT,             0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_1COIN_2PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_1COIN_2PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO jigoku_meguri_dsw[] =
{
   { 0x020000, 0xFF, dsw_data_jigoku_meguri_0 },
   { 0x020020, 0xBF, dsw_data_bonze_adventure_1 },
   { 0,        0,    NULL,      },
};

/*
static struct ROMSW_INFO jigoku_meguri_romsw[] =
{
   { 0x03FFFF, 0x00, romsw_data_bonze_adventure_0 },
   { 0,        0,    NULL },
};
*/

struct GAME_MAIN game_jigoku_meguri =
{
   jigoku_meguri_dirs,
   jigoku_meguri_roms,
   bonze_adventure_inputs,
   jigoku_meguri_dsw,
   NULL,

   LoadJigoku,
   ClearBonzeAdv,
   &bonze_adventure_video,
   ExecuteBonzeAdvFrame,
   "jigkmgri",
   "Jigoku Meguri",
   "�n��������",
   COMPANY_ID_TAITO,
   "B41",
   1988,
   taito_ym2610_sound,
   GAME_PLATFORM,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *RAM_OBJECT;
static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

typedef struct LEVEL_INFO
{
   int MapXStart;
   int MapYStart;
   int ScrollXStart;
   int ScrollYStart;
   int ScrollYMin;
   int ScrollYMax;
   int ScrollXMin;
   int ScrollXMax;
   int MapYMin;
   int MapYMax;
   int MapXMin;
   int MapXMax;
   int LevelTime;
} LEVEL_INFO;

struct LEVEL_INFO CLEV[32];

static int CChipLoaded=0;

static void LoadCChip(void)
{
   int ta;
   char str[256];

   raine_push_config_state();

   sprintf(str,"%sconfig/bonzchip.cfg",dir_cfg.exe_path);
   raine_set_config_file(str);

   for(ta=0;ta<32;ta++){
      sprintf(str,"Level%02x",ta);
      CLEV[ta].MapXStart=	raine_get_config_hex(str,"MapXStart",0x0020);
      CLEV[ta].MapYStart=	raine_get_config_hex(str,"MapYStart",0x0018);
      CLEV[ta].ScrollXStart=	raine_get_config_hex(str,"ScrollXStart",0x0090);
      CLEV[ta].ScrollYStart=	raine_get_config_hex(str,"ScrollYStart",0x00A0);
      CLEV[ta].ScrollYMin=	raine_get_config_hex(str,"ScrollYMin",0x0020);
      CLEV[ta].ScrollYMax=	raine_get_config_hex(str,"ScrollYMax",0x00C0);
      CLEV[ta].ScrollXMin=	raine_get_config_hex(str,"ScrollXMin",0x0040);
      CLEV[ta].ScrollXMax=	raine_get_config_hex(str,"ScrollXMax",0x00C0);
      CLEV[ta].MapYMin=		raine_get_config_hex(str,"MapYMin",0x0000);
      CLEV[ta].MapYMax=		raine_get_config_hex(str,"MapYMax",0x7FFF);
      CLEV[ta].MapXMin=		raine_get_config_hex(str,"MapXMin",0x0000);
      CLEV[ta].MapXMax=		raine_get_config_hex(str,"MapXMax",0x7FFF);
      CLEV[ta].LevelTime=	raine_get_config_hex(str,"LevelTime",0x6000);
   }
   CChipLoaded=1;

   raine_pop_config_state();
}

static UINT8 *GFX_BG0_SOLID;

static UINT8 *CBANK[8];
static int CChip_Bank=0;

static int CChip_ID=0x01;

static int CChipReadB(UINT32 address)
{
   int i;

   i=address&0x0FFF;

   switch(i){
      case 0x803:
         return(CChip_ID);
      break;
      case 0xC01:
         return(CChip_Bank);
      break;
      default:
         /*#ifdef RAINE_DEBUG
            if(i>0x20){
            print_debug("CCRB[%02x][%03x](%02x)\n",CChip_Bank,i,CBANK[CChip_Bank][i]);
            print_ingame(60,"CCRB[%02x][%03x](%02x)\n",CChip_Bank,i,CBANK[CChip_Bank][i]);
            }
         #endif*/
         return(CBANK[CChip_Bank][i]);
      break;
   }
}

static int CChipReadW(UINT32 address)
{
   return(CChipReadB(address+1));
}

static void CChipWriteB(UINT32 address, int data)
{
   int i;
   int ta;

   ta=CBANK[0][0x21];
   i=address&0x0FFF;
   data&=0xFF;

   switch(i){
      case 0x11:				// cchip[0][0x011]: COIN LEDS
         CBANK[CChip_Bank][i]=data;
         switch_led(0,(data>>4)&1);		// Coin A [Coin Inserted]
         switch_led(1,(data>>5)&1);		// Coin B [Coin Inserted]
         switch_led(2,((data>>6)&1)^1);		// Coin A [Ready for coins]
         //switch_led(3,((data>>7)&1)^1);	// Coin B [Ready for coins]
      break;
      case 0x1D:				// cchip[0][0x01D]: GENERATE LEVEL RESTART POS
         if(data==0x55){
#ifdef RAINE_DEBUG
            print_debug("LEVELRESTART(%02x)\n",ta);
            print_ingame(60,"LEVELRESTART(%02x)",ta);
#endif
         CBANK[0][0x01D]=0x00;
         }
      break;
      case 0x1F:				// cchip[0][0x01F]: GENERATE LEVEL START POS
         if(data==0x55){
         #ifdef RAINE_DEBUG
            print_debug("LEVELSTART(%02x)\n",ta);
            print_ingame(60,"LEVELSTART(%02x)",ta);
         #endif
         CBANK[0][0x01F]=0x00;
	   if(ta<32){
	   CBANK[0][0x023]=CLEV[ta].MapXStart&0xFF;
	   CBANK[0][0x025]=(CLEV[ta].MapXStart>>8)&0xFF;
	
	   CBANK[0][0x027]=CLEV[ta].MapYStart&0xFF;
	   CBANK[0][0x029]=(CLEV[ta].MapYStart>>8)&0xFF;
	
	   CBANK[0][0x02B]=CLEV[ta].ScrollXStart&0xFF;
	   CBANK[0][0x02D]=(CLEV[ta].ScrollXStart>>8)&0xFF;
	
	   CBANK[0][0x02F]=CLEV[ta].ScrollYStart&0xFF;
	   CBANK[0][0x031]=(CLEV[ta].ScrollYStart>>8)&0xFF;
	
	   CBANK[0][0x033]=CLEV[ta].ScrollYMin&0xFF;
	   CBANK[0][0x035]=(CLEV[ta].ScrollYMin>>8)&0xFF;
	   CBANK[0][0x037]=CLEV[ta].ScrollYMax&0xFF;
	   CBANK[0][0x039]=(CLEV[ta].ScrollYMax>>8)&0xFF;
	
	   CBANK[0][0x03B]=CLEV[ta].ScrollXMin&0xFF;
	   CBANK[0][0x03D]=(CLEV[ta].ScrollXMin>>8)&0xFF;
	   CBANK[0][0x03F]=CLEV[ta].ScrollXMax&0xFF;
	   CBANK[0][0x041]=(CLEV[ta].ScrollXMax>>8)&0xFF;
	
	   CBANK[0][0x043]=CLEV[ta].MapYMin&0xFF;
	   CBANK[0][0x045]=(CLEV[ta].MapYMin>>8)&0xFF;
	   CBANK[0][0x047]=CLEV[ta].MapYMax&0xFF;
	   CBANK[0][0x049]=(CLEV[ta].MapYMax>>8)&0xFF;
	
	   CBANK[0][0x04B]=CLEV[ta].MapXMin&0xFF;
	   CBANK[0][0x04D]=(CLEV[ta].MapXMin>>8)&0xFF;
	   CBANK[0][0x04F]=CLEV[ta].MapXMax&0xFF;
	   CBANK[0][0x051]=(CLEV[ta].MapXMax>>8)&0xFF;
	
	   CBANK[0][0x053]=CLEV[ta].LevelTime&0xFF;
	   CBANK[0][0x055]=(CLEV[ta].LevelTime>>8)&0xFF;
	   }
         }
      break;
      case 0x21:	// LEVEL NUMBER
         CBANK[CChip_Bank][i]=data;
      break;
      case 0x803:	// C-CHIP ID
      break;
      case 0xC01:	// C-CHIP BANK SELECT
         CChip_Bank=data&7;
      break;
      default:
         CBANK[CChip_Bank][i]=data;
      break;
   }
}

static void CChipWriteW(UINT32 address, int data)
{
   CChipWriteB(address+1,data&0xFF);
}

static void load_actual(int romset)
{
   int ta,tb;
   (void)(romset);

   RAMSize=0x54000;

   if(!(ROM=AllocateMem(0xC0000))) return;
   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(GFX=AllocateMem(0x204000))) return;

   GFX_SPR	= GFX+0x100000;

   CBANK[0]=RAM+0x40000;	// C-CHIP BANKS	($800000-$800FFF)
   CBANK[1]=RAM+0x40800;
   CBANK[2]=RAM+0x41000;
   CBANK[3]=RAM+0x41800;
   CBANK[4]=RAM+0x42000;
   CBANK[5]=RAM+0x42800;
   CBANK[6]=RAM+0x43000;
   CBANK[7]=RAM+0x43800;

   if(!load_rom_index(2, ROM, 0x80000)) return;		// 8x8 TILES
   tb=0;
   for(ta=0;ta<0x80000;ta++){
      GFX[tb++]=ROM[ta^1]>>4;
      GFX[tb++]=ROM[ta^1]&15;
   }
   if(!load_rom_index(1, ROM, 0x80000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x80000;ta++){
      GFX[tb++]=ROM[ta^1]>>4;
      GFX[tb++]=ROM[ta^1]&15;
   }

   if(!load_rom_index(4, RAM+0x00000, 0x10000)) return;	// 68000 ROM
   if(!load_rom_index(5, RAM+0x10000, 0x10000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom_index(6, RAM+0x00000, 0x10000)) return;
   if(!load_rom_index(7, RAM+0x10000, 0x10000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   if(!load_rom_index(0, ROM+0x40000, 0x80000)) return;	// LEVEL DATA

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x44000;
   if(!load_rom_index(8, Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom_index(3, PCMROM, 0x80000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x080000, 0x080000);

   AddTaitoYM2610(0x02EE, 0x028D, 0x10000);

   /*-----------------------*/

   RAM_VIDEO  = RAM+0x04000;
   RAM_SCROLL = RAM+0x20060;
   RAM_OBJECT = RAM+0x14000;
   GFX_FG0    = GFX+0x200000;

   GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x4000);

   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x1000);

   // Fix SOUND ERROR

   ROM[0x26568]=0x60;

   WriteWord68k(&ROM[0x26558],0x4E71);

   WriteWord68k(&ROM[0x26540],0x4E71);

   // Fix BAD HARDWARE

   ROM[0x07526]=0x60;

   // 68000 Speed Hack

   WriteLong68k(&ROM[0x013DE],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x013E2],0x00AA0000);	//
   WriteWord68k(&ROM[0x013D6],0x60E6-8);	//	bra.s	loop

   memset(RAM+0x00000,0x00,0x44000);

   LoadCChip();

   tc0110pcr_init(RAM+0x16000, 1);

   set_colour_mapper(&col_map_xbbb_bbgg_gggr_rrrr);
   InitPaletteMap(RAM+0x16000, 0x100, 0x10, 0x8000);


   // Init tc0002obj emulation
   // ------------------------

   tc0002obj.RAM	= RAM_OBJECT;
   tc0002obj.GFX	= GFX_SPR;
   tc0002obj.MASK	= GFX_SPR_SOLID;
   tc0002obj.bmp_x	= 32;
   tc0002obj.bmp_y	= 32;
   tc0002obj.bmp_w	= 320;
   tc0002obj.bmp_h	= 224;
   tc0002obj.mapper	= &Map_15bit_xBGR;
   tc0002obj.tile_mask	= 0x0FFF;
   tc0002obj.ofs_x	= 0;
   tc0002obj.ofs_y	= -16;

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	= RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	= GFX;
   tc0100scn[0].layer[0].MASK	= GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	= RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	= 0;
   tc0100scn[0].layer[0].bmp_x	= 32;
   tc0100scn[0].layer[0].bmp_y	= 32;
   tc0100scn[0].layer[0].bmp_w	= 320;
   tc0100scn[0].layer[0].bmp_h	= 224;
   tc0100scn[0].layer[0].mapper	= &Map_15bit_xBGR;
   tc0100scn[0].layer[0].tile_mask= 0x3FFF;
   tc0100scn[0].layer[0].scr_x	= 16;
   tc0100scn[0].layer[0].scr_y	= 16;

   tc0100scn[0].layer[1].RAM	= RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	= GFX;
   tc0100scn[0].layer[1].MASK	= GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	= RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	= 0;
   tc0100scn[0].layer[1].bmp_x	= 32;
   tc0100scn[0].layer[1].bmp_y	= 32;
   tc0100scn[0].layer[1].bmp_w	= 320;
   tc0100scn[0].layer[1].bmp_h	= 224;
   tc0100scn[0].layer[1].mapper	= &Map_15bit_xBGR;
   tc0100scn[0].layer[1].tile_mask= 0x3FFF;
   tc0100scn[0].layer[1].scr_x	= 16;
   tc0100scn[0].layer[1].scr_y	= 16;

   tc0100scn[0].layer[2].RAM	= RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	= GFX_FG0;
   tc0100scn[0].layer[2].SCR	= RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	= 1;
   tc0100scn[0].layer[2].bmp_x	= 32;
   tc0100scn[0].layer[2].bmp_y	= 32;
   tc0100scn[0].layer[2].bmp_w	= 320;
   tc0100scn[0].layer[2].bmp_h	= 224;
   tc0100scn[0].layer[2].mapper	= &Map_15bit_xBGR;
   tc0100scn[0].layer[2].scr_x	= 16;
   tc0100scn[0].layer[2].scr_y	= 16;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);
   tc0100scn_0_copy_gfx_fg0(ROM+0x011A92, 0x1000);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);		// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x10C000, 0x10FFFF, NULL, RAM+0x000000);			// MAIN RAM
   AddReadByte(0x080000, 0x0FFFFF, NULL, ROM+0x040000);			// DATA ROM
   AddReadByte(0xC00000, 0xC0FFFF, NULL, RAM+0x004000);			// SCREEN RAM
   AddReadByte(0xD00000, 0xD01FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x390000, 0x39000F, NULL, RAM+0x020000);			// DSWA
   AddReadByte(0x3B0000, 0x3B000F, NULL, RAM+0x020020);			// DSWB
   AddReadByte(0x3E0000, 0x3E0003, tc0140syt_read_main_68k, NULL);	// SOUND
   AddReadByte(0x800000, 0x800FFF, CChipReadB, NULL);			// C-CHIP
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x10C000, 0x10FFFF, NULL, RAM+0x000000);			// MAIN RAM
   AddReadWord(0x080000, 0x0FFFFF, NULL, ROM+0x040000);			// DATA ROM
   AddReadWord(0xC00000, 0xC0FFFF, NULL, RAM+0x004000);			// SCREEN RAM
   AddReadWord(0xD00000, 0xD01FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x200000, 0x200007, tc0110pcr_rw, NULL);			// COLOR RAM
   AddReadWord(0x390000, 0x39000F, NULL, RAM+0x020000);			// DSWA
   AddReadWord(0x3B0000, 0x3B000F, NULL, RAM+0x020020);			// DSWB
   AddReadWord(0x3D0000, 0x3D000F, NULL, RAM+0x020040);			// ???
   AddReadWord(0x800000, 0x800FFF, CChipReadW, NULL);			// C-CHIP
   AddReadWord(-1, -1, NULL, NULL);

   AddWriteByte(0x10C000, 0x10FFFF, NULL, RAM+0x000000);		// MAIN RAM
   AddWriteByte(0xC00000, 0xC0FFFF, NULL, RAM+0x004000);		// SCREEN RAM
   AddWriteByte(0xD00000, 0xD01FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x3A0000, 0x3A0001, NULL, RAM+0x020010);		// ???
   AddWriteByte(0x3E0000, 0x3E0003, tc0140syt_write_main_68k, NULL);	// SOUND
   AddWriteByte(0x800000, 0x800FFF, CChipWriteB, NULL);			// C-CHIP
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x10C000, 0x10FFFF, NULL, RAM+0x000000);		// MAIN RAM
   AddWriteWord(0xC00000, 0xC0FFFF, NULL, RAM+0x004000);		// SCREEN RAM
   AddWriteWord(0xD00000, 0xD01FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x200000, 0x200007, tc0110pcr_ww, NULL);		// COLOR RAM
   AddWriteWord(0xC20000, 0xC2000F, NULL, RAM+0x020060);		// SCROLL RAM
   AddWriteWord(0x3C0000, 0x3C000F, NULL, RAM+0x020030);		// ???
   AddWriteWord(0x800000, 0x800FFF, CChipWriteW, NULL);			// C-CHIP
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void LoadBonzeAdv(void)
{
   load_actual(0);
}

void LoadJigoku(void)
{
   load_actual(1);
}

void ClearBonzeAdv(void)
{
   RemoveTaitoYM2610();
}

void ExecuteBonzeAdvFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 4);

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawBonzeAdv(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // Init tc0002obj emulation
   // ------------------------

   tc0002obj.ctrl = ReadWord(&RAM[0x20010]);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1+OBJECT
   // ----------

   if((tc0002obj.ctrl & 0x2000)==0){
      render_tc0100scn_layer_mapped(0,1);
      render_tc0002obj_mapped();
   }
   else{
      render_tc0002obj_mapped();
      render_tc0100scn_layer_mapped(0,1);
   }

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);
}


