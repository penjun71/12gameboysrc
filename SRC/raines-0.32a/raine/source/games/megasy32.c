/******************************************************************************/
/*                                                                            */
/*                  TETRIS 2 PLUS/MEGA SYSTEM-32 (C) JALECO                   */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "megasy32.h"
#include "debug.h"

static struct DIR_INFO tetris_2_plus_dirs[] =
{
   { "tetris_2_plus", },
   { "tetris2p", },
   { "tetrisp2", },
   { NULL, },
};

static struct ROM_INFO tetris_2_plus_roms[] =
{
   {   "t2p_04.rom", 0x00080000, 0xe67f9c51, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {   "t2p_01.rom", 0x00080000, 0x5020a4ed, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "t2p_10.rom", 0x00080000, 0x34dd1bad, REGION_GFX1, 0x000000, LOAD_NORMAL, },
   {  "t2p_m01.rom", 0x00400000, 0x91dd84f1, 0, 0, 0, },
   {  "t2p_m02.rom", 0x00400000, 0x13241976, 0, 0, 0, },
   {  "t2p_m04.rom", 0x00100000, 0xb849dec9, 0, 0, 0, },
   {  "t2p_m06.rom", 0x00200000, 0xf186924e, 0, 0, 0, },
   {  "t2p_m07.rom", 0x00200000, 0x5c8e217c, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO tetris_2_plus_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x098004, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x098004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x098004, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x098004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x098002, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x098002, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x098002, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x098002, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x098002, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x098002, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x098002, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_B4,        MSG_P1_B4,               0x098002, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x098004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x098003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x098003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x098003, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x098003, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x098003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x098003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x098003, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_B4,        MSG_P2_B4,               0x098003, 0x80, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_tetris_2_plus_0[] =
{
   { MSG_COIN1,               0x07, 0x08 },
   { MSG_1COIN_1PLAY,         0x07, 0x00 },
   { MSG_1COIN_2PLAY,         0x06, 0x00 },
   { MSG_1COIN_3PLAY,         0x05, 0x00 },
   { MSG_1COIN_4PLAY,         0x04, 0x00 },
   { MSG_2COIN_1PLAY,         0x03, 0x00 },
   { MSG_3COIN_1PLAY,         0x02, 0x00 },
   { MSG_4COIN_1PLAY,         0x01, 0x00 },
   { MSG_5COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0x38, 0x08 },
   { MSG_1COIN_1PLAY,         0x38, 0x00 },
   { MSG_1COIN_2PLAY,         0x30, 0x00 },
   { MSG_1COIN_3PLAY,         0x28, 0x00 },
   { MSG_1COIN_4PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x18, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x08, 0x00 },
   { MSG_5COIN_1PLAY,         0x00, 0x00 },
   { MSG_FREE_PLAY,             0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_tetris_2_plus_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_HARDEST,             0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_EASY,                0x00, 0x00 },
   { "Vs Mode Rounds",        0x04, 0x02 },
   { "3",                     0x04, 0x00 },
   { "1",                     0x00, 0x00 },
   { "Language",              0x08, 0x02 },
   { "Japanese",              0x08, 0x00 },
   { "English",               0x00, 0x00 },
   { "FBI Logo",              0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "Voice",                 0x20, 0x02 },
   { MSG_ON,                  0x20, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x40, 0x02 },
   { MSG_ON,                  0x40, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_SCREEN,              0x80, 0x02 },
   { MSG_NORMAL,              0x80, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO tetris_2_plus_dsw[] =
{
   { 0x098008, 0xFF, dsw_data_tetris_2_plus_0 },
   { 0x098009, 0xFF, dsw_data_tetris_2_plus_1 },
   { 0,        0,    NULL,      },
};

static struct GFX_LAYOUT layout_8x8x8 =
{
   8,8,
   RGN_FRAC(1,1),
   8,
   {0,1,2,3,4,5,6,7},
   {STEP8(0,8)},
   {STEP8(0,8*8)},
   8*8*8
};

static struct GFX_LIST tetris_2_plus_gfx[] =
{
   { REGION_GFX1, &layout_8x8x8,  },
   { 0,           NULL,           },
};

static struct VIDEO_INFO tetris_2_plus_video =
{
   DrawTetris2Plus,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
   tetris_2_plus_gfx,
};

struct GAME_MAIN game_tetris_2_plus =
{
   tetris_2_plus_dirs,
   tetris_2_plus_roms,
   tetris_2_plus_inputs,
   tetris_2_plus_dsw,
   NULL,

   LoadTetris2Plus,
   ClearTetris2Plus,
   &tetris_2_plus_video,
   ExecuteTetris2PlusFrame,
   "tetrisp2",
   "Tetris 2 Plus",
   NULL,
   COMPANY_ID_JALECO,
   NULL,
   1996,
   NULL,
   GAME_PUZZLE | GAME_NOT_WORKING,
};

static UINT8 *GFX_FG0;
static UINT8 *GFX_FG0_SOLID;

static UINT8 *GFX_UNK;
//static UINT8 *GFX_UNK_SOLID;

void LoadTetris2Plus(void)
{
   int ta,tb;
   UINT8 *TMP;

   if(!(GFX=AllocateMem(0x800000+0x200000))) return;
   if(!(TMP=AllocateMem(0x400000))) return;

//   GFX_FG0 = GFX+0x800000;
   GFX_UNK = GFX+0x800000;

   tb=0;
   if(!load_rom("t2p_m01.rom", TMP, 0x400000)) return;	// 8x8 ??? TILES
   for(ta=0;ta<0x400000;ta+=2,tb+=4){
      WriteWord(&GFX[tb],ReadWord(&TMP[ta]));
   }
   tb=2;
   if(!load_rom("t2p_m02.rom", TMP, 0x400000)) return;	// 8x8 ??? TILES
   for(ta=0;ta<0x400000;ta+=2,tb+=4){
      WriteWord(&GFX[tb],ReadWord(&TMP[ta]));
   }
   FreeMem(TMP);

//   if(!load_rom("t2p_10.rom", GFX_FG0, 0x004000)) return;	// 8x8 FG0 TILES

   if(!load_rom("t2p_m04.rom", GFX_UNK, 0x100000)) return;	// 16x16 ??? TILES

   GFX_FG0 = load_region[REGION_GFX1];
   GFX_FG0_SOLID = make_solid_mask_8x8(GFX_FG0, 0x2000);

   RAMSize=0x98010;

   if(!(RAM=AllocateMem(RAMSize))) return;
   memset(RAM+0x00000,0x00,0x98000);
   memset(RAM+0x98000,0xFF,0x00010);

   WriteWord68k(&ROM[0x02D4A],0x4E71);

   WriteWord68k(&ROM[0x02D54],0x4E71);

   WriteLong68k(&ROM[0x02D98],0x4E714E71);

   WriteWord68k(&ROM[0x02DA4],0x6000);

   InitPaletteMap(RAM+0x60000, 0x80, 0x100, 0x8000);

   set_colour_mapper(&col_map_bbbb_bggg_ggrr_rrrx_xxxx_xxxx_xxxx_xxxx);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x080000);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x0FFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x400000, 0x40FFFF, NULL, RAM+0x010000);			// FG0/BG0 RAM
   AddReadByte(0x200000, 0x23FFFF, NULL, RAM+0x020000);			// ? RAM
   AddReadByte(0x300000, 0x31FFFF, NULL, RAM+0x060000);			// COLOR RAM
   AddReadByte(0x500000, 0x507FFF, NULL, RAM+0x080000);			// ? RAM
   AddReadByte(0x600000, 0x60FFFF, NULL, RAM+0x088000);			// ? RAM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x0FFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x400000, 0x40FFFF, NULL, RAM+0x010000);			// FG0/BG0 RAM
   AddReadWord(0x200000, 0x23FFFF, NULL, RAM+0x020000);			// ? RAM
   AddReadWord(0x300000, 0x31FFFF, NULL, RAM+0x060000);			// COLOR RAM
   AddReadWord(0x500000, 0x507FFF, NULL, RAM+0x080000);			// ? RAM
   AddReadWord(0x600000, 0x60FFFF, NULL, RAM+0x088000);			// ? RAM
   AddReadWord(0xBE0000, 0xBE000F, NULL, RAM+0x098000);			// ? RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x400000, 0x40FFFF, NULL, RAM+0x010000);		// FG0/BG0 RAM
   AddWriteByte(0x200000, 0x23FFFF, NULL, RAM+0x020000);		// ? RAM
   AddWriteByte(0x300000, 0x31FFFF, NULL, RAM+0x060000);		// COLOR RAM
   AddWriteByte(0x500000, 0x507FFF, NULL, RAM+0x080000);		// ? RAM
   AddWriteByte(0x600000, 0x60FFFF, NULL, RAM+0x088000);		// ? RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x400000, 0x40FFFF, NULL, RAM+0x010000);		// FG0/BG0 RAM
   AddWriteWord(0x200000, 0x23FFFF, NULL, RAM+0x020000);		// ? RAM
   AddWriteWord(0x300000, 0x31FFFF, NULL, RAM+0x060000);		// COLOR RAM
   AddWriteWord(0x500000, 0x507FFF, NULL, RAM+0x080000);		// ? RAM
   AddWriteWord(0x600000, 0x60FFFF, NULL, RAM+0x088000);		// ? RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearTetris2Plus(void)
{
   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x100000,1);
      save_debug("RAM.bin",RAM,0x080000,1);
      //save_debug("GFX.bin",GFX,0x800000,0);
   #endif
}

void ExecuteTetris2PlusFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)

#ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
      //print_ingame(60,"%04x %04x %04x %04x",ReadWord(&RAM[0x0800]),ReadWord(&RAM[0x0802]),ReadWord(&RAM[0x0804]),ReadWord(&RAM[0x0806]));
#endif

   cpu_interrupt(CPU_68K_0, 4);
   cpu_interrupt(CPU_68K_0, 2);
   cpu_interrupt(CPU_68K_0, 1);
}

static int tcol;
static int bg1flip,bg2flip;

void DrawTetris2Plus(void)
{
   int x,y,ta,xx,yy;
   int z,zz;
   UINT8 *map;

   ClearPaletteMap();

   MAP_PALETTE_MAPPED_NEW(
      tcol,
      256,
      map
   );

#if 0
   zz=0;
   for(x=32;x<320+32;x+=16){
   for(y=32;y<224+32;y+=16){

      z=ReadWord(&RAM[0x14000+zz]);
/*
      ta = ((z&0x1FFF)<<8);

      Draw16x16_Mapped(&GFX_UNK[ta], x, y, map);
*/
      ta = ((z&0x000F)<<7) | ((z&0x1FF0)<<8);

      Draw8x8_Mapped_Rot(&GFX[ta+0x400000], x,   y,   map);
      Draw8x8_Mapped_Rot(&GFX[ta+0x400040], x+8, y,   map);
      Draw8x8_Mapped_Rot(&GFX[ta+0x400800], x,   y+8, map);
      Draw8x8_Mapped_Rot(&GFX[ta+0x400840], x+8, y+8, map);

      zz+=4;
   }
   zz+=(256-((224/16)*4));
   }
#endif

/*

   0x0000 0x0100 0x0200 0x0300 | 0x0010 | 0x0020 | 0x0030
   0x0004 0x0104 0x0204 0x0304
   0x0008 0x0108 0x0208 0x0308
   0x000C 0x010C 0x020C 0x030C

*/

   yy=0;
   for(y=32;y<224+32;y+=16,yy++){
   xx=0;
   for(x=32;x<320+32;x+=16,xx++){

      zz = (((xx&0x07)>>0)<<8)
         | (((yy&0x0F)>>0)<<2);

//         | (((xx&0x0C)>>2)<<4);

      zz = (xx * 4) + (yy * 256);


      z=ReadWord(&RAM[0x14000+zz]);

//      ta = ((z&0x000F)<<7) | ((z&0x1FF0)<<8);
//      ta = (z & 0x1FFF) << 10;

//      ta = (((z >> 0) & 0x000F) * (0x40 * 0x02)) |
//           (((z >> 4) & 0x01FF) * (0x40 * 0x40));

      ta = (((z >> 0) & 0x0007) * (0x40 * 0x04)) |
           (((z >> 3) & 0x03FF) * (0x40 * 0x80));

      ta &= 0x7FFFFF;

      Draw8x8_Mapped_Rot(&GFX[ta+0x000000], x,   y,   map);
      Draw8x8_Mapped_Rot(&GFX[ta+0x000080], x+8, y,   map);
      Draw8x8_Mapped_Rot(&GFX[ta+0x001000], x,   y+8, map);
      Draw8x8_Mapped_Rot(&GFX[ta+0x001080], x+8, y+8, map);

//      ta = (z & 0x1FFF) << 7;
//      Draw16x16_Mapped_Rot(&GFX_UNK[ta], x, y, map);

   }
   }

   // FG0
   // ---

   zz=0;
   for(y=32;y<224+32;y+=8){
   for(x=32;x<320+32;x+=8){
      ta=RAM[0x10000+zz];
      if(GFX_FG0_SOLID[ta]!=0){				// No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            (RAM[0x10002+zz]&0x0F)|0x60,
            256,
            map
         );

         if(GFX_FG0_SOLID[ta]==1){			// Some pixels; trans
            Draw8x8_Trans_Mapped_Rot(&GFX_FG0[ta<<6],x,y,map);
         }
         else{						// all pixels; solid
            Draw8x8_Mapped_Rot(&GFX_FG0[ta<<6],x,y,map);
         }

      }

      zz+=4;
   }
   zz+=(256-(40*4));
   }

   if(key[KEY_F7]){
   if(bg1flip==0){
      tcol--;
      tcol&=0x7F;
      bg1flip=1;
      print_ingame(60,"P:%02x",tcol);
   }
   }
   else{
      bg1flip=0;
   }
   if(key[KEY_F8]){
   if(bg2flip==0){
      tcol++;
      tcol&=0x7F;
      bg2flip=1;
      print_ingame(60,"P:%02x",tcol);
   }
   }
   else{
      bg2flip=0;
   }

}

/*

JALECO MEGA SYSTEM-32
---------------------

CPU: 68000 (68020?)

-------+--------+-------------------
Start  | End    | Contents
-------+--------+-------------------
000000 | 0FFFFF | PROGRAM  ROM
108000 | 10FFFF | SCRATCH  RAM
400000 | 403FFF | ASCII    RAM (FG0)
404000 | 407FFF | SCROLL   RAM (BG0)
500000 | 507FFF | ROTATE   RAM
100000 | 103FFF | OBJECT   RAM
300000 | 301FFF | COLOR    RAM
304000 | 305FFF | COLOR    RAM
308000 | 30BFFF | COLOR    RAM
318000 | 31BFFF | COLOR    RAM (FG0)
200000 | 23FFFF | PRIORITY RAM
-------+--------+-------------------

FG0 RAM
-------

- 8x8 tiles
- 4 bytes per tile
- 64x64 tiles; 512x512 layer

-----+--------+---------------------
Byte | Bit(s) | Use
-----+76543210+---------------------
  1  |xxxxxxxx| Tile Number
  3  |....xxxx| Colour Bank
-----+--------+---------------------

BG0 RAM
-------

- 16x16 tiles
- 4 bytes per tile
- 64x64 tiles; 1024x1024 layer

-----+--------+---------------------
Byte | Bit(s) | Use
-----+76543210+---------------------
  0  |...xxxxx| Tile Number (high)
  1  |xxxxxxxx| Tile Number (low)
  3  |....xxxx| Colour Bank
-----+--------+---------------------

*/

