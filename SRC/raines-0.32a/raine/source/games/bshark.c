/******************************************************************************/
/*                                                                            */
/*                  BATTLE SHARK (C) 1989 TAITO CORPORATION                   */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "bshark.h"
#include "tc100scn.h"
#include "tc110pcr.h"
#include "tc150rod.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

static struct DIR_INFO battle_shark_dirs[] =
{
   { "battle_shark", },
   { "bshark", },
   { NULL, },
};

static struct ROM_INFO battle_shark_roms[] =
{
   { "bshark03.bin", 0x00080000, 0xa18eab78, 0, 0, 0, },
   { "bshark05.bin", 0x00080000, 0x596b83da, 0, 0, 0, },
   { "bshark07.bin", 0x00080000, 0xedb07808, 0, 0, 0, },
   { "bshark09.bin", 0x00080000, 0xc4cbc907, 0, 0, 0, },
   { "bshark01.bin", 0x00080000, 0x3ebe8c63, 0, 0, 0, },
   { "bshark70.bin", 0x00020000, 0xd77d81e2, 0, 0, 0, },
   { "bshark72.bin", 0x00020000, 0xc09c0f91, 0, 0, 0, },
   { "bshark74.bin", 0x00020000, 0x6869fa99, 0, 0, 0, },
   { "bshark75.bin", 0x00020000, 0x6ba65542, 0, 0, 0, },
   { "bshark02.bin", 0x00080000, 0x8488ba10, 0, 0, 0, },
   { "bshark04.bin", 0x00080000, 0x2446b0da, 0, 0, 0, },
   { "bshark06.bin", 0x00080000, 0xd200b6eb, 0, 0, 0, },
   { "bshark08.bin", 0x00020000, 0x1c79e69c, 0, 0, 0, },
   { "bshark18.bin", 0x00010000, 0x7245a6f6, 0, 0, 0, },
   { "bshark67.bin", 0x00020000, 0x39307c74, 0, 0, 0, },
   { "bshark69.bin", 0x00020000, 0xa54c137a, 0, 0, 0, },
   { "bshark71.bin", 0x00020000, 0xdf1fa629, 0, 0, 0, },
   { "bshark73.bin", 0x00020000, 0xf2fe62b5, 0, 0, 0, },
   { "bshark19.bin", 0x00000100, 0x2ee9c404, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO battle_shark_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x027014, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x027014, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x027014, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x027014, 0x04, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x02701E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x02701E, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x02701E, 0x02, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_battle_shark_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DSWA_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO battle_shark_dsw[] =
{
   { 0x027010, 0xFF, dsw_data_battle_shark_0 },
   { 0x027012, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_battle_shark_0[] =
{
   { "Taito 0",               0x00 },
   { "Taito 1",               0x01 },
   { "Taito 2",               0x02 },
   { "Taito 3",               0x03 },
   { NULL,                    0    },
};

static struct ROMSW_INFO battle_shark_romsw[] =
{
   { 0x07FFFF, 0x02, romsw_data_battle_shark_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO battle_shark_video =
{
   DrawBattleShark,
   320,
   240,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_battle_shark =
{
   battle_shark_dirs,
   battle_shark_roms,
   battle_shark_inputs,
   battle_shark_dsw,
   battle_shark_romsw,

   LoadBattleShark,
   ClearBattleShark,
   &battle_shark_video,
   ExecuteBattleSharkFrame,
   "bshark",
   "Battle Shark",
   NULL,
   COMPANY_ID_TAITO,
   "C34",
   1989,
   taito_ym2610b_sound,
   GAME_SHOOT,
};

#define OBJ_A_COUNT	(0x75C8)

// OBJECT TILE MAPS

static UINT8 *OBJECT_MAP;

// 16x16 OBJECT TILES BANK A

static UINT8 *GFX_OBJ_A;
static UINT8 *GFX_OBJ_A_SOLID;

static UINT8 *zoom16_ofs;
static UINT8 *zoom8_ofs;

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_LINES;

static UINT16 YM2610Read68k(UINT32 address)
{
   return YM2610ReadZ80( (UINT16) ((address&7)>>1) );
}

static void YM2610Write68k(UINT32 address, UINT16 data)
{
   YM2610WriteZ80( (UINT16) ((address&7)>>1), (UINT8) data);
}

void LoadBattleShark(void)
{
   int ta,tb,tc;
   UINT8 *TMP;

   if(!(GFX=AllocateMem(0x100000))) return;

   if(!(GFX_LINES=AllocateMem(0x200000))) return;

   if(!(GFX_OBJ_A=AllocateMem(OBJ_A_COUNT*0x80))) return;

   if(!(OBJECT_MAP=AllocateMem(0x80000))) return;

   if(!(TMP=AllocateMem(0x80000))) return;

   if(!load_rom("bshark07.bin", TMP, 0x80000)) return;	// 1024x1 ROAD LINES
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_LINES[tb+0] =((tc&0x8000)>>(14));
      GFX_LINES[tb+1] =((tc&0x4000)>>(13));
      GFX_LINES[tb+2] =((tc&0x2000)>>(12));
      GFX_LINES[tb+3] =((tc&0x1000)>>(11));
      GFX_LINES[tb+4] =((tc&0x0800)>>(10));
      GFX_LINES[tb+5] =((tc&0x0400)>>( 9));
      GFX_LINES[tb+6] =((tc&0x0200)>>( 8));
      GFX_LINES[tb+7] =((tc&0x0100)>>( 7));
      GFX_LINES[tb+0]|=((tc&0x0080)>>( 7));
      GFX_LINES[tb+1]|=((tc&0x0040)>>( 6));
      GFX_LINES[tb+2]|=((tc&0x0020)>>( 5));
      GFX_LINES[tb+3]|=((tc&0x0010)>>( 4));
      GFX_LINES[tb+4]|=((tc&0x0008)>>( 3));
      GFX_LINES[tb+5]|=((tc&0x0004)>>( 2));
      GFX_LINES[tb+6]|=((tc&0x0002)>>( 1));
      GFX_LINES[tb+7]|=((tc&0x0001)>>( 0));
      tb+=8;
   }

   if(!load_rom("bshark01.bin", TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] =((tc&0x8000)>>(15));
      GFX_OBJ_A[tb+1] =((tc&0x4000)>>(14));
      GFX_OBJ_A[tb+2] =((tc&0x2000)>>(13));
      GFX_OBJ_A[tb+3] =((tc&0x1000)>>(12));
      GFX_OBJ_A[tb+4] =((tc&0x0800)>>(11));
      GFX_OBJ_A[tb+5] =((tc&0x0400)>>(10));
      GFX_OBJ_A[tb+6] =((tc&0x0200)>>( 9));
      GFX_OBJ_A[tb+7] =((tc&0x0100)>>( 8));
      GFX_OBJ_A[tb+8] =((tc&0x0080)>>( 7));
      GFX_OBJ_A[tb+9] =((tc&0x0040)>>( 6));
      GFX_OBJ_A[tb+10]=((tc&0x0020)>>( 5));
      GFX_OBJ_A[tb+11]=((tc&0x0010)>>( 4));
      GFX_OBJ_A[tb+12]=((tc&0x0008)>>( 3));
      GFX_OBJ_A[tb+13]=((tc&0x0004)>>( 2));
      GFX_OBJ_A[tb+14]=((tc&0x0002)>>( 1));
      GFX_OBJ_A[tb+15]=((tc&0x0001)>>( 0));
      tb+=16;
   }
   if(!load_rom("bshark02.bin", TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] |=((tc&0x8000)>>(14));
      GFX_OBJ_A[tb+1] |=((tc&0x4000)>>(13));
      GFX_OBJ_A[tb+2] |=((tc&0x2000)>>(12));
      GFX_OBJ_A[tb+3] |=((tc&0x1000)>>(11));
      GFX_OBJ_A[tb+4] |=((tc&0x0800)>>(10));
      GFX_OBJ_A[tb+5] |=((tc&0x0400)>>( 9));
      GFX_OBJ_A[tb+6] |=((tc&0x0200)>>( 8));
      GFX_OBJ_A[tb+7] |=((tc&0x0100)>>( 7));
      GFX_OBJ_A[tb+8] |=((tc&0x0080)>>( 6));
      GFX_OBJ_A[tb+9] |=((tc&0x0040)>>( 5));
      GFX_OBJ_A[tb+10]|=((tc&0x0020)>>( 4));
      GFX_OBJ_A[tb+11]|=((tc&0x0010)>>( 3));
      GFX_OBJ_A[tb+12]|=((tc&0x0008)>>( 2));
      GFX_OBJ_A[tb+13]|=((tc&0x0004)>>( 1));
      GFX_OBJ_A[tb+14]|=((tc&0x0002)>>( 0));
      GFX_OBJ_A[tb+15]|=((tc&0x0001)<<( 1));
      tb+=16;
   }
   if(!load_rom("bshark03.bin", TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] |=((tc&0x8000)>>(13));
      GFX_OBJ_A[tb+1] |=((tc&0x4000)>>(12));
      GFX_OBJ_A[tb+2] |=((tc&0x2000)>>(11));
      GFX_OBJ_A[tb+3] |=((tc&0x1000)>>(10));
      GFX_OBJ_A[tb+4] |=((tc&0x0800)>>( 9));
      GFX_OBJ_A[tb+5] |=((tc&0x0400)>>( 8));
      GFX_OBJ_A[tb+6] |=((tc&0x0200)>>( 7));
      GFX_OBJ_A[tb+7] |=((tc&0x0100)>>( 6));
      GFX_OBJ_A[tb+8] |=((tc&0x0080)>>( 5));
      GFX_OBJ_A[tb+9] |=((tc&0x0040)>>( 4));
      GFX_OBJ_A[tb+10]|=((tc&0x0020)>>( 3));
      GFX_OBJ_A[tb+11]|=((tc&0x0010)>>( 2));
      GFX_OBJ_A[tb+12]|=((tc&0x0008)>>( 1));
      GFX_OBJ_A[tb+13]|=((tc&0x0004)>>( 0));
      GFX_OBJ_A[tb+14]|=((tc&0x0002)<<( 1));
      GFX_OBJ_A[tb+15]|=((tc&0x0001)<<( 2));
      tb+=16;
   }
   if(!load_rom("bshark04.bin", TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] |=((tc&0x8000)>>(12));
      GFX_OBJ_A[tb+1] |=((tc&0x4000)>>(11));
      GFX_OBJ_A[tb+2] |=((tc&0x2000)>>(10));
      GFX_OBJ_A[tb+3] |=((tc&0x1000)>>( 9));
      GFX_OBJ_A[tb+4] |=((tc&0x0800)>>( 8));
      GFX_OBJ_A[tb+5] |=((tc&0x0400)>>( 7));
      GFX_OBJ_A[tb+6] |=((tc&0x0200)>>( 6));
      GFX_OBJ_A[tb+7] |=((tc&0x0100)>>( 5));
      GFX_OBJ_A[tb+8] |=((tc&0x0080)>>( 4));
      GFX_OBJ_A[tb+9] |=((tc&0x0040)>>( 3));
      GFX_OBJ_A[tb+10]|=((tc&0x0020)>>( 2));
      GFX_OBJ_A[tb+11]|=((tc&0x0010)>>( 1));
      GFX_OBJ_A[tb+12]|=((tc&0x0008)>>( 0));
      GFX_OBJ_A[tb+13]|=((tc&0x0004)<<( 1));
      GFX_OBJ_A[tb+14]|=((tc&0x0002)<<( 2));
      GFX_OBJ_A[tb+15]|=((tc&0x0001)<<( 3));
      tb+=16;
   }

   if(!load_rom("bshark06.bin", OBJECT_MAP, 0x80000)) return; // TILE MAPPING

   if(!load_rom("bshark05.bin", TMP, 0x80000)) return;	// 8x8 BG0 TILES
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX[tb+0] = ((tc&0xF000)>>12);
      GFX[tb+1] = ((tc&0x0F00)>> 8);
      GFX[tb+2] = ((tc&0x00F0)>> 4);
      GFX[tb+3] = ((tc&0x000F)>> 0);
      tb+=4;
   }

   FreeMem(TMP);

   GFX_OBJ_A_SOLID = make_solid_mask_16x8(GFX_OBJ_A, OBJ_A_COUNT);
   GFX_BG0_SOLID   = make_solid_mask_8x8 (GFX,       0x4000);

   RAMSize=0x80000;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0x100000))) return;

   if(!load_rom("bshark71.bin", RAM, 0x20000)) return;	// 68000 MAIN ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("bshark69.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("bshark70.bin", RAM, 0x20000)) return;	// 68000 MAIN ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("bshark67.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   if(!load_rom("bshark74.bin", RAM, 0x20000)) return;	// 68000 SUB ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x80000]=RAM[ta];
   }
   if(!load_rom("bshark72.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x80001]=RAM[ta];
   }
   if(!load_rom("bshark75.bin", RAM, 0x20000)) return;	// 68000 SUB ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0xC0000]=RAM[ta];
   }
   if(!load_rom("bshark73.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0xC0001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   if(!(PCMROM=AllocateMem(0x0A0000))) return;
   if(!load_rom("bshark09.bin",PCMROM+0x00000,0x80000)) return;	// ADPCM A rom
   if(!load_rom("bshark08.bin",PCMROM+0x80000,0x20000)) return;	// ADPCM B rom (rom too small?)
   YM2610SetBuffers(PCMROM, PCMROM+0x80000, 0x80000, 0x20000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x14000;
   RAM_SCROLL = RAM+0x27000;
   RAM_INPUT  = RAM+0x27010;
   GFX_FG0    = RAM+0x3C000;

   set_colour_mapper(&col_map_xbbb_bbgg_gggr_rrrr);
   InitPaletteMap(RAM+0x25000, 0x100, 0x10, 0x8000);

   // CPU Communication
   // -----------------

   ROM[0x0BD52]=0x60;
   WriteLong68k(&ROM[0x0BD6A],0x4E714E71);	//	nop
   WriteLong68k(&ROM[0x0BD72],0x4E714E71);	//	nop
   ROM[0x0BEB6]=0x60;
   ROM[0x0BEE6]=0x60;
   ROM[0x0BF0E]=0x60;
   WriteLong68k(&ROM[0x0BF24],0x4E714E71);	//	nop
   WriteWord68k(&ROM[0x0BF3A],0x4E71);		//	nop

   // Control Hack
   // ------------

   WriteWord68k(&ROM[0x07FDE],0x1B79);		//	X
   WriteLong68k(&ROM[0x07FE0],0x00800001);	//
   WriteWord68k(&ROM[0x07FE4],0xB902);		//

   WriteWord68k(&ROM[0x07FE6],0x1B79);		//	Y
   WriteLong68k(&ROM[0x07FE8],0x00800003);	//
   WriteWord68k(&ROM[0x07FEC],0xB903);		//

   WriteWord68k(&ROM[0x07FEE],0x3E1F);		//	move	(a7)+,D7
   WriteWord68k(&ROM[0x07FF0],0x4E73);		//	rte

   // Main 68000 Speed Hack
   // ---------------------

   WriteWord68k(&ROM[0x0304C],0x4EF9);		//	jmp	$2F00
   WriteLong68k(&ROM[0x0304E],0x00002F00);	//

   WriteLong68k(&ROM[0x02F00],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x02F04],0x00AA0000);	//
   WriteWord68k(&ROM[0x02F08],0x4EB9);		//	jsr	<random_gen>
   WriteLong68k(&ROM[0x02F0A],0x000004C6);	//
   WriteWord68k(&ROM[0x02F0E],0x4EF9);		//	jmp	$3052
   WriteLong68k(&ROM[0x02F10],0x00003052);	//

   // Sub 68000 Speed Hack
   // ---------------------

   WriteLong68k(&ROM[0x8093C],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x80940],0x00AA0000);	//
   WriteWord68k(&ROM[0x80944],0x6100-10);	//	bra.s	loop

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0150rod emulation
   // ------------------------

   tc0150rod.RAM  	= RAM + 0x34000;
   tc0150rod.GFX  	= GFX_LINES;
   tc0150rod.PAL  	= NULL;
   tc0150rod.mapper	= &Map_15bit_xBGR;
   tc0150rod.bmp_x	= 32;
   tc0150rod.bmp_y	= 32;
   tc0150rod.bmp_w	= 320;
   tc0150rod.bmp_h	= 240;
   tc0150rod.scr_x	= 0;
   tc0150rod.scr_y	= -14;

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=240;
   tc0100scn[0].layer[0].mapper	=&Map_15bit_xBGR;
   tc0100scn[0].layer[0].tile_mask=0x3FFF;
   tc0100scn[0].layer[0].scr_x	=16;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=240;
   tc0100scn[0].layer[1].mapper	=&Map_15bit_xBGR;
   tc0100scn[0].layer[1].tile_mask=0x3FFF;
   tc0100scn[0].layer[1].scr_x	=16;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=240;
   tc0100scn[0].layer[2].mapper	=&Map_15bit_xBGR;
   tc0100scn[0].layer[2].scr_x	=16;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   init_16x8_zoom();
   zoom16_ofs = make_16x16_zoom_ofs_type1zz();
   zoom8_ofs  = make_16x8_zoom_ofs_type1zz();

   GameMouse = 1;

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x113FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0xD00000, 0xD0FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0xC00000, 0xC00FFF, NULL, RAM+0x024000);			// OBJECT RAM
   AddReadByte(0xA00000, 0xA01FFF, NULL, RAM+0x025000);			// COLOR RAM
   AddReadByte(0x400000, 0x40000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x800000, 0x80000F, NULL, RAM+0x027020);			// CURSOR
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x113FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0xD00000, 0xD0FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0xC00000, 0xC00FFF, NULL, RAM+0x024000);			// OBJECT RAM
   AddReadWord(0xA00000, 0xA01FFF, NULL, RAM+0x025000);			// COLOR RAM
   AddReadWord(0x400000, 0x40000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x113FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0xD06000, 0xD06FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0xD00000, 0xD0FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0xC00000, 0xC00FFF, NULL, RAM+0x024000);		// OBJECT RAM
   AddWriteByte(0xA00000, 0xA01FFF, NULL, RAM+0x025000);		// COLOR RAM
   AddWriteByte(0x400000, 0x40000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x113FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0xD06000, 0xD06FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0xD00000, 0xD0FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0xC00000, 0xC00FFF, NULL, RAM+0x024000);		// OBJECT RAM
   AddWriteWord(0xA00000, 0xA01FFF, NULL, RAM+0x025000);		// COLOR RAM
   AddWriteWord(0xD20000, 0xD2000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x400000, 0x40000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 

   AddMemFetchMC68000B(0x000000, 0x07FFFF, ROM+0x080000-0x000000);	// 68000 ROM
   AddMemFetchMC68000B(-1, -1, NULL);

   AddReadByteMC68000B(0x000000, 0x07FFFF, NULL, ROM+0x080000);		// 68000 ROM
   AddReadByteMC68000B(0x108000, 0x10BFFF, NULL, RAM+0x030000);		// MAIN RAM
   AddReadByteMC68000B(0x110000, 0x113FFF, NULL, RAM+0x010000);		// COMMON RAM
   AddReadByteMC68000B(0x000000, 0xFFFFFF, DefBadReadByte, NULL);	// <Bad Reads>
   AddReadByteMC68000B(-1, -1, NULL, NULL);

   AddReadWordMC68000B(0x000000, 0x07FFFF, NULL, ROM+0x080000);		// 68000 ROM
   AddReadWordMC68000B(0x108000, 0x10BFFF, NULL, RAM+0x030000);		// MAIN RAM
   AddReadWordMC68000B(0x110000, 0x113FFF, NULL, RAM+0x010000);		// COMMON RAM
   AddReadWordMC68000B(0x600000, 0x600007, YM2610Read68k, NULL);	// YM2610
   AddReadWordMC68000B(0x000000, 0xFFFFFF, DefBadReadWord, NULL);	// <Bad Reads>
   AddReadWordMC68000B(-1, -1, NULL, NULL);

   AddWriteByteMC68000B(0x108000, 0x10BFFF, NULL, RAM+0x030000);	// MAIN RAM
   AddWriteByteMC68000B(0x110000, 0x113FFF, NULL, RAM+0x010000);	// COMMON RAM
   AddWriteByteMC68000B(0xAA0000, 0xAA0001, Stop68000, NULL);		// Trap Idle 68000
   AddWriteByteMC68000B(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);	// <Bad Writes>
   AddWriteByteMC68000B(-1, -1, NULL, NULL);

   AddWriteWordMC68000B(0x108000, 0x10BFFF, NULL, RAM+0x030000);	// MAIN RAM
   AddWriteWordMC68000B(0x110000, 0x113FFF, NULL, RAM+0x010000);	// COMMON RAM
   AddWriteWordMC68000B(0x800000, 0x801FFF, NULL, RAM+0x034000);	// ROADSCROLL RAM
   AddWriteWordMC68000B(0x600000, 0x600007, YM2610Write68k, NULL);	// YM2610
   AddWriteWordMC68000B(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);	// <Bad Writes>
   AddWriteWordMC68000B(-1, -1, NULL, NULL);

   AddInitMemoryMC68000B();	// Set Starscream mem pointers... 
}

void ClearBattleShark(void)
{
   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x100000,1);
      save_debug("RAM.bin",RAM,0x040000,1);
      //save_debug("GFX.bin",GFX,0x100000,0);
   #endif
}

static int x1,myy1,x11,myy11;

void ExecuteBattleSharkFrame(void)
{
   /*------[Mouse Hack]-------*/

   set_mouse_range(0,0,319,239);

   x11=319-mouse_x;
   myy11=mouse_y;

   x1=(x11*106)/320;
   myy1=(myy11*98)/240;

   if(x1<0){x1=0;}
   if(x1>127){x1=127;}
   if(myy1<0){myy1=0;}
   if(myy1>127){myy1=127;}

   RAM[0x27020]=x1-0x40+12;
   RAM[0x27022]=myy1-0x40+18;

   if(mouse_b&1) RAM[0x2701E] &= ~0x40;

   /*------[CPU Execution]------*/

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(10,60));	// M68000 12MHz (60fps)
#ifdef RAINE_DEBUG
      print_debug("PC0:%06x SR0:%04x\n",s68000context.pc,s68000context.sr);
#endif
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 4);

   cpu_execute_cycles(CPU_68K_1, CPU_FRAME_MHz(10,60));	// M68000 12MHz (60fps)
   #ifdef RAINE_DEBUG
      print_debug("PC1:%06x SR1:%04x\n",s68000context.pc,s68000context.sr);
   #endif
   cpu_interrupt(CPU_68K_1, 4);
}

static void render_z_system_sprites(int pri)
{
   int x,y,ta,tb,zz;
   int zx,zy,rx,ry,xx,zzx,zzy;
   UINT8 *map,*SPR_MAP;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;
/*
   if(pri!=0)
      pri=0x00;
   else
      pri=0x80;
*/
   // OBJECT Z-SYSTEM
   // ---------------

   for(zz=0x24000;zz<0x25000;zz+=8){

      //if((RAM[zz+2]&0x80)==pri){

      zx=(RAM[zz+2]>>0)&0x3F;
      zy=(RAM[zz+1]>>1)&0x3F;
      if((zx!=0)&&(zy!=0)){

         ta = ReadWord(&RAM[zz+6])&0xFFF;
         if(ta!=0){

            x=((320+32)-(ReadWord(&RAM[zz+4])+zx))&0x1FF;

            //y=(32+ReadWord(&RAM[zz+0])+(0x3F-(zy>>1)))&0x1FF;

            y=(((64-16)+32+7)+(ReadWord(&RAM[zz+0])-zy))&0x1FF;

            MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM[zz+2])>>7)&0xFF,
               16,
               map
            );

            zoom_dat_x = zoom16_ofs+(zx<<2);
            zoom_dat_y = zoom8_ofs+(zy<<3);

            SPR_MAP = OBJECT_MAP + (ta<<6);

            switch(RAM[zz+5]&0xC0){
            case 0x40:

            xx=x;

            for(ry=0;ry<8;ry++){
            zzy = zoom_dat_y[ry];
            if((y>16)&&(y<240+32)){
            ta=0;
            for(rx=0;rx<4;rx++){
            zzx = zoom_dat_x[rx];

            if((x>16)&&(x<320+32)){
               tb=ReadWord(&SPR_MAP[ta])&0x7FFF;
               if(GFX_OBJ_A_SOLID[tb]!=0){			// No pixels; skip
                  if(GFX_OBJ_A_SOLID[tb]==1)			// Some pixels; trans
                     Draw16x8_Trans_Mapped_ZoomXY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
                  else						// all pixels; solid
                     Draw16x8_Mapped_ZoomXY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
               }
            }
            ta+=2;
            x = (x+zzx)&0x1FF;
            }
            }
            y = (y+zzy)&0x1FF;
            SPR_MAP+=8;
            x=xx;
            }

            break;
            case 0x00:

            x+=(zx+1);
            xx=x;

            for(ry=0;ry<8;ry++){
            zzy = zoom_dat_y[ry];
            if((y>16)&&(y<240+32)){
            ta=0;
            for(rx=0;rx<4;rx++){
            zzx = zoom_dat_x[rx];
            x = (x-zzx)&0x1FF;

            if((x>16)&&(x<320+32)){
               tb=ReadWord(&SPR_MAP[ta])&0x7FFF;
               if(GFX_OBJ_A_SOLID[tb]!=0){			// No pixels; skip
                  if(GFX_OBJ_A_SOLID[tb]==1)			// Some pixels; trans
                     Draw16x8_Trans_Mapped_ZoomXY_FlipY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
                  else						// all pixels; solid
                     Draw16x8_Mapped_ZoomXY_FlipY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
               }
            }
            ta+=2;
            }
            }
            y = (y+zzy)&0x1FF;
            SPR_MAP+=8;
            x=xx;
            }

            break;
            case 0xC0:

            y+=zy;
            xx=x;

            for(ry=0;ry<8;ry++){
            zzy = zoom_dat_y[ry];
            y = (y-zzy)&0x1FF;
            if((y>16)&&(y<240+32)){
            ta=0;
            for(rx=0;rx<4;rx++){
            zzx = zoom_dat_x[rx];

            if((x>16)&&(x<320+32)){
               tb=ReadWord(&SPR_MAP[ta])&0x7FFF;
               if(GFX_OBJ_A_SOLID[tb]!=0){			// No pixels; skip
                  if(GFX_OBJ_A_SOLID[tb]==1)			// Some pixels; trans
                     Draw16x8_Trans_Mapped_ZoomXY_FlipX(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
                  else						// all pixels; solid
                     Draw16x8_Mapped_ZoomXY_FlipX(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
               }
            }
            ta+=2;
            x = (x+zzx)&0x1FF;
            }
            }
            SPR_MAP+=8;
            x=xx;
            }

            break;
            case 0x80:

            x+=(zx+1);
            y+=zy;
            xx=x;

            for(ry=0;ry<8;ry++){
            zzy = zoom_dat_y[ry];
            y = (y-zzy)&0x1FF;
            if((y>16)&&(y<240+32)){
            ta=0;
            for(rx=0;rx<4;rx++){
            zzx = zoom_dat_x[rx];
            x = (x-zzx)&0x1FF;

            if((x>16)&&(x<320+32)){
               tb=ReadWord(&SPR_MAP[ta])&0x7FFF;
               if(GFX_OBJ_A_SOLID[tb]!=0){			// No pixels; skip
                  if(GFX_OBJ_A_SOLID[tb]==1)			// Some pixels; trans
                     Draw16x8_Trans_Mapped_ZoomXY_FlipXY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
                  else						// all pixels; solid
                     Draw16x8_Mapped_ZoomXY_FlipXY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
               }
            }
            ta+=2;
            }
            }
            SPR_MAP+=8;
            x=xx;
            }

            break;
            }

         }
      }
   //}
   }
}

void DrawBattleShark(void)
{
   int x,y;

   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped_flipy(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped_flipy(0,1);

   // 3D-ROAD Z-SYSTEM
   // ----------------

   tc0150rod_init_palette();
   tc0150rod_render_flipy(0x000,0x800);

   // OBJECT
   // ------

   render_z_system_sprites(1);

   // FG0
   // ---

   render_tc0100scn_layer_mapped_flipy(0,2);

   // CURSOR
   // ------

   x=(32+(319-x11))-7;
   y=(32+myy11)-7;
   if((x>16)&&(y>16)&&(x<320+32)&&(y<240+32)){
      Draw16x16_Trans(SpriteGun1,x,y,0);
   }

   pal[254].r=0;
   pal[254].g=0;
   pal[254].b=0;

   pal[253].r=31;
   pal[253].g=31;
   pal[253].b=63;

   pal[252].r=63;
   pal[252].g=31;
   pal[252].b=31;
}

/*

BATTLE SHARK
------------

- Taito; 1989; Z-System
- 68000; 68000; YM2610

-------------+--------------------------------
File         | Contents
-------------+--------------------------------
BSHARK01.BIN | Sprite Tiles (plane0)
BSHARK02.BIN | Sprite Tiles (plane1)
BSHARK03.BIN | Sprite Tiles (plane2)
BSHARK04.BIN | Sprite Tiles (plane3)
BSHARK05.BIN | 8x8 BG0 Tiles
BSHARK06.BIN | Sprite Table
BSHARK07.BIN | tc0150rod gfx
BSHARK08.BIN | YM2610 ADPCM
BSHARK09.BIN | YM2610 ADPCM
BSHARK18.BIN | ? (Zoom table data/Roadscroll)
BSHARK19.BIN | ? (256 bytes)
BSHARK67.BIN | Main 68000 block#1 (odd)
BSHARK69.BIN | Main 68000 block#0 (odd)
BSHARK70.BIN | Main 68000 block#1 (even)
BSHARK71.BIN | Main 68000 block#0 (even)
BSHARK72.BIN | Sub 68000 block#0 (odd)
BSHARK73.BIN | Sub 68000 block#1 (odd)
BSHARK74.BIN | Sub 68000 block#0 (even)
BSHARK75.BIN | Sub 68000 block#1 (even)
-------------+--------------------------------

OBJECT RAM
----------

-----+--------+-------------------------------
Byte | Bit(s) | Description
-----+76543210+-------------------------------
  0  |.xxxxxx.| Zoom Y Axis (0-63)
  0  |.......x| Y Position (High)
  1  |xxxxxxxx| Y Position (Low)
  2  |.xxxxxxx| Colour Bank (High)
  3  |x.......| Colour Bank (Low)
  3  |..xxxxxx| Zoom X Axis (0-63)
  4  |x.......| Flip X Axis
  4  |.x......| Flip Y Axis
  4  |.......x| X Position (High)
  5  |xxxxxxxx| X Position (Low)
  6  |....xxxx| Sprite Tile (High)
  7  |xxxxxxxx| Sprite Tile (Low)
-----+--------+-------------------------------

- Colour ram looks like it can switch access mode:

A00004 == 0000: Normal access (A00000-A01FFF)
A00004 == 0001: Port access   (A00000-A00003)

- Maybe other taito games can switch like this.
- Maybe they just left some old code in the rom.

*/

