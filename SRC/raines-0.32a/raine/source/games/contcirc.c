/******************************************************************************/
/*                                                                            */
/*            CONTINENTAL CIRCUS (C) 1987/1989 TAITO CORPORATION              */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "contcirc.h"
#include "tc100scn.h"
#include "tc110pcr.h"
#include "tc150rod.h"
#include "tc220ioc.h"
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif
#include "sasound.h"		// sample support routines

static struct DIR_INFO continental_circus_dirs[] =
{
   { "continental_circus", },
   { "contcirc", },
   { "ctcircus", },
   { NULL, },
};

static struct ROM_INFO continental_circus_roms[] =
{
   {  "cc_2_03.bin", 0x00080000, 0xf11f2be8, 0, 0, 0, },
   {  "cc_2_57.bin", 0x00080000, 0xf6fb3ba2, 0, 0, 0, },
   {  "cc_3_01.bin", 0x00080000, 0x4f6c36d9, 0, 0, 0, },
   {  "cc_3_02.bin", 0x00080000, 0x8df866a2, 0, 0, 0, },
   {  "cc_3_06.bin", 0x00080000, 0xbddf9eea, 0, 0, 0, },
   {  "cc_3_07.bin", 0x00080000, 0x2cb40599, 0, 0, 0, },
   {  "cc_3_64.bin", 0x00080000, 0x151e1f52, 0, 0, 0, },
   {  "cc_1_19.bin", 0x00080000, 0xcaa1c4c8, 0, 0, 0, },
   {  "cc_1_18.bin", 0x00080000, 0x1e6724b5, 0, 0, 0, },
   {  "cc_1_17.bin", 0x00080000, 0xe9ce03ab, 0, 0, 0, },
   {  "cc_1_11.bin", 0x00010000, 0xd8746234, 0, 0, 0, },
   {  "cc_2_25.bin", 0x00020000, 0xf5c92e42, 0, 0, 0, },
   {  "cc_2_26.bin", 0x00020000, 0x1345ebe6, 0, 0, 0, },
   {  "cc_2_35.bin", 0x00020000, 0x16522f2d, 0, 0, 0, },
   {  "cc_2_36.bin", 0x00020000, 0xa1732ea5, 0, 0, 0, },
   {  "cc_3_97.bin", 0x00010000, 0xdccb0c7f, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO continental_circus_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x022804, 0x08, BIT_ACTIVE_1 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x022804, 0x04, BIT_ACTIVE_1 },
   { KB_DEF_TILT,         MSG_TILT,                0x022806, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x022804, 0x10, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x022806, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x022818, 0xFF, BIT_ACTIVE_1 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x02281A, 0xFF, BIT_ACTIVE_1 },

   { KB_DEF_P1_B1,        "Accelerate",            0x022820, 0xFF, BIT_ACTIVE_1 },	// Accelerate - Analogue uses bit#567
   { KB_DEF_P1_B2,        "Brake",                 0x022822, 0xFF, BIT_ACTIVE_1 },	// Brake      - Analogue uses bit#567 (not available in digital mode!)
   { KB_DEF_P1_B3,        "Gear Shift",            0x022824, 0xFF, BIT_ACTIVE_1 },	// Gear Shift - High/Low

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_continental_circus_0[] =
{
   { "Acceleration",          0x01, 0x02 },
   { "Analogue",              0x01, 0x00 },
   { "Digital",               0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },	// Untested
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },	// Untested
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_continental_circus_1[] =
{
   { MSG_DSWB_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT3,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT4,           0x08, 0x02 },
   { MSG_OFF,                 0x08, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "3D Double Image",       0x20, 0x02 },
   { MSG_ON,                  0x20, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DSWB_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO continental_circus_dsw[] =
{
   { 0x022800, 0xFF, dsw_data_continental_circus_0 },
   { 0x022802, 0xDF, dsw_data_continental_circus_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_continental_circus_0[] =
{
   { "Taito Japan",                     0x01 },
   { "Taito America (US Navy License)", 0x02 },
   { "Taito Japan (US Navy License)",   0x03 },
   { NULL,                    0    },
};

static struct ROMSW_INFO continental_circus_romsw[] =
{
   { 0x07FFFF, 0x03, romsw_data_continental_circus_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO continental_circus_video =
{
   draw_continental_circus,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_continental_circus =
{
   continental_circus_dirs,
   continental_circus_roms,
   continental_circus_inputs,
   continental_circus_dsw,
   continental_circus_romsw,

   load_continental_circus,
   clear_continental_circus,
   &continental_circus_video,
   execute_continental_circus_frame,
   "contcirc",
   "Continental Circus",
   NULL,
   COMPANY_ID_TAITO,
   "B33",
   1989,
   taito_ym2610_sound,
   GAME_RACE,
};

static struct DIR_INFO continental_circus_alt_dirs[] =
{
   { "continental_circus_alt", },
   { "continental_circus_alternate", },
   { "contcalt", },
   { ROMOF("contcirc"), },
   { CLONEOF("contcirc"), },
   { NULL, },
};

static struct ROM_INFO continental_circus_alt_roms[] =
{
   {  "cc_2_03.bin", 0x00080000, 0xf11f2be8, 0, 0, 0, },
   {  "cc_2_57.bin", 0x00080000, 0xf6fb3ba2, 0, 0, 0, },
   {  "cc_3_01.bin", 0x00080000, 0x4f6c36d9, 0, 0, 0, },
   {  "cc_3_02.bin", 0x00080000, 0x8df866a2, 0, 0, 0, },
   {  "cc_3_06.bin", 0x00080000, 0xbddf9eea, 0, 0, 0, },
   {  "cc_3_07.bin", 0x00080000, 0x2cb40599, 0, 0, 0, },
   {  "cc_3_64.bin", 0x00080000, 0x151e1f52, 0, 0, 0, },
   {  "cc_1_19.bin", 0x00080000, 0xcaa1c4c8, 0, 0, 0, },
   {  "cc_1_18.bin", 0x00080000, 0x1e6724b5, 0, 0, 0, },
   {  "cc_1_17.bin", 0x00080000, 0xe9ce03ab, 0, 0, 0, },
   {  "cc_1_11.bin", 0x00010000, 0xd8746234, 0, 0, 0, },
   {  "cc_2_25.bin", 0x00020000, 0xf5c92e42, 0, 0, 0, },
   {         "ic26", 0x00020000, 0xe7c1d1fa, 0, 0, 0, },
   {  "cc_2_35.bin", 0x00020000, 0x16522f2d, 0, 0, 0, },
   {         "ic36", 0x00020000, 0xd6741e33, 0, 0, 0, },
   {  "cc_3_97.bin", 0x00010000, 0xdccb0c7f, 0, 0, 0, },
   {       "b14_31", 0x00002000, 0x5c6b013d, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct ROMSW_INFO continental_circus_alt_romsw[] =
{
   { 0x07FFFF, 0x02, romsw_data_continental_circus_0 },
   { 0,        0,    NULL },
};

struct GAME_MAIN game_continental_circus_alt =
{
   continental_circus_alt_dirs,
   continental_circus_alt_roms,
   continental_circus_inputs,
   continental_circus_dsw,
   continental_circus_alt_romsw,

   load_continental_circus_alt,
   clear_continental_circus,
   &continental_circus_video,
   execute_continental_circus_frame,
   "contcalt",
   "Continental Circus (alternate)",
   NULL,
   COMPANY_ID_TAITO,
   "B33",
   1987,
   taito_ym2610_sound,
   GAME_RACE,
};

#define OBJ_A_COUNT	(0x4D61)

// OBJECT TILE MAPS

static UINT8 *OBJECT_MAP;

// 16x16 OBJECT TILES BANK A

static UINT8 *GFX_OBJ_A;
static UINT8 *GFX_OBJ_A_SOLID;

static UINT8 *zoom16_ofs;
static UINT8 *zoom8_ofs;

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_LINES;

/* road - line - left - right */

static UINT8 PAL_MAP[0x100] =
{
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
   0xC, 0x9, 0xA, 0xB, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 
};

static void load_actual(int romset)
{
   int ta,tb,tc;
   UINT8 *TMP;

   if(!(GFX=AllocateMem(0x100000))) return;

   if(!(GFX_LINES=AllocateMem(0x200000))) return;

   if(!(GFX_OBJ_A=AllocateMem(OBJ_A_COUNT*0x80))) return;

   if(!(OBJECT_MAP=AllocateMem(0x80000))) return;

   if(!(TMP=AllocateMem(0x80000))) return;

   if(!load_rom_index(0, TMP, 0x80000)) return;	// 1024x1 ROAD LINES
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_LINES[tb+0] =((tc&0x8000)>>(14));
      GFX_LINES[tb+1] =((tc&0x4000)>>(13));
      GFX_LINES[tb+2] =((tc&0x2000)>>(12));
      GFX_LINES[tb+3] =((tc&0x1000)>>(11));
      GFX_LINES[tb+4] =((tc&0x0800)>>(10));
      GFX_LINES[tb+5] =((tc&0x0400)>>( 9));
      GFX_LINES[tb+6] =((tc&0x0200)>>( 8));
      GFX_LINES[tb+7] =((tc&0x0100)>>( 7));
      GFX_LINES[tb+0]|=((tc&0x0080)>>( 7));
      GFX_LINES[tb+1]|=((tc&0x0040)>>( 6));
      GFX_LINES[tb+2]|=((tc&0x0020)>>( 5));
      GFX_LINES[tb+3]|=((tc&0x0010)>>( 4));
      GFX_LINES[tb+4]|=((tc&0x0008)>>( 3));
      GFX_LINES[tb+5]|=((tc&0x0004)>>( 2));
      GFX_LINES[tb+6]|=((tc&0x0002)>>( 1));
      GFX_LINES[tb+7]|=((tc&0x0001)>>( 0));
      tb+=8;
   }

   if(!load_rom_index(2, TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] =((tc&0x8000)>>(15));
      GFX_OBJ_A[tb+1] =((tc&0x4000)>>(14));
      GFX_OBJ_A[tb+2] =((tc&0x2000)>>(13));
      GFX_OBJ_A[tb+3] =((tc&0x1000)>>(12));
      GFX_OBJ_A[tb+4] =((tc&0x0800)>>(11));
      GFX_OBJ_A[tb+5] =((tc&0x0400)>>(10));
      GFX_OBJ_A[tb+6] =((tc&0x0200)>>( 9));
      GFX_OBJ_A[tb+7] =((tc&0x0100)>>( 8));
      GFX_OBJ_A[tb+8] =((tc&0x0080)>>( 7));
      GFX_OBJ_A[tb+9] =((tc&0x0040)>>( 6));
      GFX_OBJ_A[tb+10]=((tc&0x0020)>>( 5));
      GFX_OBJ_A[tb+11]=((tc&0x0010)>>( 4));
      GFX_OBJ_A[tb+12]=((tc&0x0008)>>( 3));
      GFX_OBJ_A[tb+13]=((tc&0x0004)>>( 2));
      GFX_OBJ_A[tb+14]=((tc&0x0002)>>( 1));
      GFX_OBJ_A[tb+15]=((tc&0x0001)>>( 0));
      tb+=16;
   }
   if(!load_rom_index(3, TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] |=((tc&0x8000)>>(14));
      GFX_OBJ_A[tb+1] |=((tc&0x4000)>>(13));
      GFX_OBJ_A[tb+2] |=((tc&0x2000)>>(12));
      GFX_OBJ_A[tb+3] |=((tc&0x1000)>>(11));
      GFX_OBJ_A[tb+4] |=((tc&0x0800)>>(10));
      GFX_OBJ_A[tb+5] |=((tc&0x0400)>>( 9));
      GFX_OBJ_A[tb+6] |=((tc&0x0200)>>( 8));
      GFX_OBJ_A[tb+7] |=((tc&0x0100)>>( 7));
      GFX_OBJ_A[tb+8] |=((tc&0x0080)>>( 6));
      GFX_OBJ_A[tb+9] |=((tc&0x0040)>>( 5));
      GFX_OBJ_A[tb+10]|=((tc&0x0020)>>( 4));
      GFX_OBJ_A[tb+11]|=((tc&0x0010)>>( 3));
      GFX_OBJ_A[tb+12]|=((tc&0x0008)>>( 2));
      GFX_OBJ_A[tb+13]|=((tc&0x0004)>>( 1));
      GFX_OBJ_A[tb+14]|=((tc&0x0002)>>( 0));
      GFX_OBJ_A[tb+15]|=((tc&0x0001)<<( 1));
      tb+=16;
   }
   if(!load_rom_index(4, TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] |=((tc&0x8000)>>(13));
      GFX_OBJ_A[tb+1] |=((tc&0x4000)>>(12));
      GFX_OBJ_A[tb+2] |=((tc&0x2000)>>(11));
      GFX_OBJ_A[tb+3] |=((tc&0x1000)>>(10));
      GFX_OBJ_A[tb+4] |=((tc&0x0800)>>( 9));
      GFX_OBJ_A[tb+5] |=((tc&0x0400)>>( 8));
      GFX_OBJ_A[tb+6] |=((tc&0x0200)>>( 7));
      GFX_OBJ_A[tb+7] |=((tc&0x0100)>>( 6));
      GFX_OBJ_A[tb+8] |=((tc&0x0080)>>( 5));
      GFX_OBJ_A[tb+9] |=((tc&0x0040)>>( 4));
      GFX_OBJ_A[tb+10]|=((tc&0x0020)>>( 3));
      GFX_OBJ_A[tb+11]|=((tc&0x0010)>>( 2));
      GFX_OBJ_A[tb+12]|=((tc&0x0008)>>( 1));
      GFX_OBJ_A[tb+13]|=((tc&0x0004)>>( 0));
      GFX_OBJ_A[tb+14]|=((tc&0x0002)<<( 1));
      GFX_OBJ_A[tb+15]|=((tc&0x0001)<<( 2));
      tb+=16;
   }
   if(!load_rom_index(5, TMP, OBJ_A_COUNT*0x10)) return;	// 16x16 OBJ A
   tb=0;
   for(ta=0;ta<OBJ_A_COUNT*0x10;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX_OBJ_A[tb+0] |=((tc&0x8000)>>(12));
      GFX_OBJ_A[tb+1] |=((tc&0x4000)>>(11));
      GFX_OBJ_A[tb+2] |=((tc&0x2000)>>(10));
      GFX_OBJ_A[tb+3] |=((tc&0x1000)>>( 9));
      GFX_OBJ_A[tb+4] |=((tc&0x0800)>>( 8));
      GFX_OBJ_A[tb+5] |=((tc&0x0400)>>( 7));
      GFX_OBJ_A[tb+6] |=((tc&0x0200)>>( 6));
      GFX_OBJ_A[tb+7] |=((tc&0x0100)>>( 5));
      GFX_OBJ_A[tb+8] |=((tc&0x0080)>>( 4));
      GFX_OBJ_A[tb+9] |=((tc&0x0040)>>( 3));
      GFX_OBJ_A[tb+10]|=((tc&0x0020)>>( 2));
      GFX_OBJ_A[tb+11]|=((tc&0x0010)>>( 1));
      GFX_OBJ_A[tb+12]|=((tc&0x0008)>>( 0));
      GFX_OBJ_A[tb+13]|=((tc&0x0004)<<( 1));
      GFX_OBJ_A[tb+14]|=((tc&0x0002)<<( 2));
      GFX_OBJ_A[tb+15]|=((tc&0x0001)<<( 3));
      tb+=16;
   }

   if(!load_rom_index(6, OBJECT_MAP, 0x80000)) return; // TILE MAPPING

   if(!load_rom_index(1, TMP, 0x80000)) return;	// 8x8 BG0 TILES
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      tc=ReadWord(&TMP[ta]);
      GFX[tb+0] = ((tc&0xF000)>>12);
      GFX[tb+1] = ((tc&0x0F00)>> 8);
      GFX[tb+2] = ((tc&0x00F0)>> 4);
      GFX[tb+3] = ((tc&0x000F)>> 0);
      tb+=4;
   }

   FreeMem(TMP);

   GFX_OBJ_A_SOLID = make_solid_mask_16x8(GFX_OBJ_A, OBJ_A_COUNT);
   GFX_BG0_SOLID   = make_solid_mask_8x8 (GFX,       0x4000);

   RAMSize=0x80000;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0x80000))) return;

   if(!load_rom_index(11, RAM+0x00000, 0x20000)) return;	// 68000 MAIN ROM
   if(!load_rom_index(13, RAM+0x20000, 0x20000)) return;	// 68000 SUB ROM
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom_index(12, RAM+0x00000, 0x20000)) return;
   if(!load_rom_index(14, RAM+0x20000, 0x20000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x50000;
   if(!load_rom_index(10, Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x180000))) return;
   if(!load_rom_index(7,PCMROM+0x000000,0x80000)) return; // ADPCM A rom
   if(!load_rom_index(9,PCMROM+0x080000,0x80000)) return; // ADPCM B rom
   if(!load_rom_index(8,PCMROM+0x100000,0x80000)) return; // ADPCM B rom
   YM2610SetBuffers(PCMROM, PCMROM+0x080000, 0x080000, 0x100000);

   AddTaitoYM2610(0x034D, 0x02BF, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);
   memset(RAM+0x22800,0xFF,0x00800);

   RAM_VIDEO  = RAM+0x10000;
   RAM_SCROLL = RAM+0x29000;
   GFX_FG0    = RAM+0x3C000;
   RAM_INPUT  = RAM+0x22800;

   tc0110pcr_init_typeb(RAM+0x20000, 1, 0);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);
   InitPaletteMap(RAM+0x20000, 0x100, 0x10, 0x8000);

   // Main Checksum
   // -------------

   WriteLong68k(&ROM[0x00AB4],0x4E714E71);	//	nop; nop
   WriteLong68k(&ROM[0x00AB8],0x4E714E71);	//	nop; nop

   // Main Checksum
   // -------------

   WriteLong68k(&ROM[0x409AC],0x4E714E71);	//	nop; nop
   WriteLong68k(&ROM[0x409B0],0x4E714E71);	//	nop; nop
   WriteWord68k(&ROM[0x409B4],0x4E71);		//	nop

   // Main 68000 Speed Hack
   // ---------------------

   WriteLong68k(&ROM[0x00844],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x00848],0x00AA0000);	//
   WriteWord68k(&ROM[0x0084C],0x6100-10);	//	bra.s	<loop>

   // Sub 68000 Speed Hack
   // --------------------

   WriteLong68k(&ROM[0x40850],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x40854],0x00AA0000);	//
   WriteWord68k(&ROM[0x40858],0x6100-26);	//	bra.s	<loop>

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0150rod emulation
   // ------------------------

   tc0150rod.RAM  	= RAM + 0x38000;
   tc0150rod.GFX  	= GFX_LINES;
   tc0150rod.PAL  	= PAL_MAP;
   tc0150rod.mapper	= &Map_15bit_xRGB;
   tc0150rod.bmp_x	= 32;
   tc0150rod.bmp_y	= 32;
   tc0150rod.bmp_w	= 320;
   tc0150rod.bmp_h	= 224;
   tc0150rod.scr_x	= 0;
   tc0150rod.scr_y	= 0 - (15 + 6);

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
   tc0100scn[0].layer[0].mapper	=&Map_15bit_xRGB;
   tc0100scn[0].layer[0].tile_mask=0x3FFF;
   tc0100scn[0].layer[0].scr_x	=16;
   tc0100scn[0].layer[0].scr_y	=16;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
   tc0100scn[0].layer[1].mapper	=&Map_15bit_xRGB;
   tc0100scn[0].layer[1].tile_mask=0x3FFF;
   tc0100scn[0].layer[1].scr_x	=16;
   tc0100scn[0].layer[1].scr_y	=16;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
   tc0100scn[0].layer[2].mapper	=&Map_15bit_xRGB;
   tc0100scn[0].layer[2].scr_x	=16;
   tc0100scn[0].layer[2].scr_y	=16;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   init_16x8_zoom();
   zoom16_ofs = make_16x16_zoom_ofs_type1z();
   zoom8_ofs  = make_16x8_zoom_ofs_type1z();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x080000, 0x087FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x200000, 0x20FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x300000, 0x301FFF, NULL, RAM+0x038000);			// ROOT RAM
   AddReadByte(0x400000, 0x4007FF, NULL, RAM+0x022000);			// OBJECT RAM
   AddReadByte(0x420000, 0x420003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x080000, 0x087FFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x200000, 0x20FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x400000, 0x4007FF, NULL, RAM+0x022000);			// OBJECT RAM
   AddReadWord(0x100000, 0x100007, tc0110pcr_rw, NULL);			// COLOR RAM
   AddReadWord(0x300000, 0x301FFF, NULL, RAM+0x038000);			// ROOT RAM
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x080000, 0x087FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x206000, 0x206FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x200000, 0x20FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x300000, 0x301FFF, NULL, RAM+0x038000);		// ROOT RAM
   AddWriteByte(0x400000, 0x4007FF, NULL, RAM+0x022000);		// OBJECT RAM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x080000, 0x087FFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x206000, 0x206FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x200000, 0x20FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x300000, 0x301FFF, NULL, RAM+0x038000);		// ROOT RAM [ROADSCROLL]
   AddWriteWord(0x400000, 0x4007FF, NULL, RAM+0x022000);		// OBJECT RAM
   AddWriteWord(0x220000, 0x22000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x100000, 0x100007, tc0110pcr_ww, NULL);		// COLOR RAM
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 

   AddMemFetchMC68000B(0x000000, 0x03FFFF, ROM+0x040000-0x000000);	// 68000 ROM
   AddMemFetchMC68000B(-1, -1, NULL);

   AddReadByteMC68000B(0x000000, 0x03FFFF, NULL, ROM+0x040000);			// 68000 ROM
   AddReadByteMC68000B(0x080000, 0x083FFF, NULL, RAM+0x008000);			// MAIN RAM
   AddReadByteMC68000B(0x084000, 0x087FFF, NULL, RAM+0x004000);			// COMMON RAM
   AddReadByteMC68000B(0x100000, 0x100003, tc0220ioc_rb_port, NULL);		// INPUT
   AddReadByteMC68000B(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByteMC68000B(-1, -1, NULL, NULL);

   AddReadWordMC68000B(0x000000, 0x03FFFF, NULL, ROM+0x040000);			// 68000 ROM
   AddReadWordMC68000B(0x080000, 0x083FFF, NULL, RAM+0x008000);			// MAIN RAM
   AddReadWordMC68000B(0x084000, 0x087FFF, NULL, RAM+0x004000);			// COMMON RAM
   AddReadWordMC68000B(0x200000, 0x200003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadWordMC68000B(0x100000, 0x100003, tc0220ioc_rw_port, NULL);		// INPUT
   AddReadWordMC68000B(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWordMC68000B(-1, -1, NULL, NULL);

   AddWriteByteMC68000B(0x080000, 0x083FFF, NULL, RAM+0x008000);		// MAIN RAM
   AddWriteByteMC68000B(0x084000, 0x087FFF, NULL, RAM+0x004000);		// COMMON RAM
   AddWriteByteMC68000B(0x100000, 0x100003, tc0220ioc_wb_port, NULL);		// INPUT
   AddWriteByteMC68000B(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByteMC68000B(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByteMC68000B(-1, -1, NULL, NULL);

   AddWriteWordMC68000B(0x080000, 0x083FFF, NULL, RAM+0x008000);		// MAIN RAM
   AddWriteWordMC68000B(0x084000, 0x087FFF, NULL, RAM+0x004000);		// COMMON RAM
   AddWriteWordMC68000B(0x200000, 0x200003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteWordMC68000B(0x100000, 0x100003, tc0220ioc_ww_port, NULL);		// INPUT
   AddWriteWordMC68000B(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWordMC68000B(-1, -1, NULL, NULL);

   AddInitMemoryMC68000B();	// Set Starscream mem pointers... 
}

void load_continental_circus(void)
{
   load_actual(0);
}

void load_continental_circus_alt(void)
{
   load_actual(1);
}

void clear_continental_circus(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x040000,1);
   #endif
}

void execute_continental_circus_frame(void)
{
   static int gear=0;
   static int gearflip=1;

   // Gear Control Hack

   if(RAM_INPUT[0x24]){
      if(!gearflip){
         gearflip=1;
         gear^=1;
         if(gear)
            print_ingame(60,"Changed to High Gear");
         else
            print_ingame(60,"Changed to Low Gear");
      }
   }
   else{
      gearflip=0;
   }

   if(gear)
      RAM_INPUT[0x06] &= ~0x10;
   else
      RAM_INPUT[0x06] |=  0x10;

   RAM_INPUT[0x04] &= 0x1F;
   RAM_INPUT[0x06] &= 0x1F;

   if(RAM_INPUT[0x20])
      RAM_INPUT[0x04] |= 0x80;

   if(RAM_INPUT[0x22])
      RAM_INPUT[0x06] |= 0x80;

   // Wheel Hack

   RAM_INPUT[0x10] = 0x00;
   RAM_INPUT[0x12] = 0x00;

   if(RAM_INPUT[0x18]){
   RAM_INPUT[0x10] = 0x50;
   RAM_INPUT[0x12] = 0x00;
   }
   if(RAM_INPUT[0x1A]){
   RAM_INPUT[0x10] = 0xB0;
   RAM_INPUT[0x12] = 0xFF;
   }

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   #ifdef RAINE_DEBUG
      print_debug("PC0:%06x SR0:%04x\n",s68000context.pc,s68000context.sr);
   #endif
   cpu_interrupt(CPU_68K_0, 6);

   cpu_execute_cycles(CPU_68K_1, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   #ifdef RAINE_DEBUG
      print_debug("PC1:%06x SR1:%04x\n",s68000context.pc,s68000context.sr);
   #endif
   cpu_interrupt(CPU_68K_1, 6);

   Taito2610_Frame();			// Z80 and YM2610
}

/*

---+-------------------+--------------
 0 | xxxxxxx. ........ | zoom y
 0 | .......x xxxxxxxx | sprite y
 2 | .....xxx xxxxxxxx | tile number
 4 | x....... ........ | pri???
 4 | .x...... ........ | flip x
 4 | .......x xxxxxxxx | sprite x
 6 | xxxxxxxx ........ | colour bank
 6 | ........ .xxxxxxx | zoom x
---+-------------------+--------------

*/

static void render_z_system_sprites(int pri)
{
   int x,y,ta,tb,zz;
   int zx,zy,rx,ry,xx,zzx,zzy;
   UINT8 *map,*SPR_MAP;
   UINT8 *zoom_dat_x;
   UINT8 *zoom_dat_y;

   if(pri==0)
      pri = 0x80;
   else
      pri = 0x00;

   // OBJECT Z-SYSTEM
   // ---------------

   for(zz=0x227F8;zz>=0x22000;zz-=8){

      if((RAM[zz+5]&0x80)==pri){

      zx=(ReadWord(&RAM[zz+6])>>0)&0x7F;
      zy=(ReadWord(&RAM[zz+0])>>9)&0x7F;
      if((zx!=0)&&(zy!=0)){

         ta = ReadWord(&RAM[zz+2])&0x7FF;
         if(ta!=0){

            x=(0+32+ReadWord(&RAM[zz+4]))&0x1FF;

            y=(16+128+ReadWord(&RAM[zz+0])-zy)&0x1FF;

            MAP_PALETTE_MAPPED_NEW(
               (ReadWord(&RAM[zz+6])>>8)&0xFF,
               16,
               map
            );

            zoom_dat_x = zoom16_ofs+(zx<<3);
            zoom_dat_y = zoom8_ofs+(zy<<4);

            SPR_MAP = OBJECT_MAP + (ta<<8);

            switch(RAM[zz+5]&0x40){
            case 0x00:

            xx=x;

            for(ry=0;ry<16;ry++){
            zzy = zoom_dat_y[ry];
            if((y>16)&&(y<240+32)){
            ta=0;
            for(rx=0;rx<8;rx++){
            zzx = zoom_dat_x[rx];

            if((x>16)&&(x<320+32)){
               tb=ReadWord(&SPR_MAP[ta])&0x7FFF;
               if(GFX_OBJ_A_SOLID[tb]!=0){			// No pixels; skip
                  if(GFX_OBJ_A_SOLID[tb]==1)			// Some pixels; trans
                     Draw16x8_Trans_Mapped_ZoomXY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
                  else						// all pixels; solid
                     Draw16x8_Mapped_ZoomXY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
               }
            }
            ta+=2;
            x = (x+zzx)&0x1FF;
            }
            }
            y = (y+zzy)&0x1FF;
            SPR_MAP+=16;
            x=xx;
            }

            break;
            case 0x40:

            x+=(zx+1);
            xx=x;

            for(ry=0;ry<16;ry++){
            zzy = zoom_dat_y[ry];
            if((y>16)&&(y<240+32)){
            ta=0;
            for(rx=0;rx<8;rx++){
            zzx = zoom_dat_x[rx];
            x = (x-zzx)&0x1FF;

            if((x>16)&&(x<320+32)){
               tb=ReadWord(&SPR_MAP[ta])&0x7FFF;
               if(GFX_OBJ_A_SOLID[tb]!=0){			// No pixels; skip
                  if(GFX_OBJ_A_SOLID[tb]==1)			// Some pixels; trans
                     Draw16x8_Trans_Mapped_ZoomXY_FlipY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
                  else						// all pixels; solid
                     Draw16x8_Mapped_ZoomXY_FlipY(&GFX_OBJ_A[tb<<7],x,y,map,zzx,zzy);
               }
            }
            ta+=2;
            }
            }
            y = (y+zzy)&0x1FF;
            SPR_MAP+=16;
            x=xx;
            }

            break;
            }

         }
      }
   }

   }
}

void draw_continental_circus(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // Z-SYSTEM OBJECT
   // ---------------

   render_z_system_sprites(0);

   // 3D-ROAD Z-SYSTEM
   // ----------------

   tc0150rod_init_palette();
   tc0150rod_render(0x000,0x800);

   // Z-SYSTEM OBJECT
   // ---------------

   render_z_system_sprites(1);

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);

   //tc0150rod_show_palette();
}


