
void LoadInsectorX(void);
void ClearInsectorX(void);

void LoadTNZSExtra(void);
void ClearTNZSExtra(void);

void LoadArk2Doh(void);
void ClearArk2Doh(void);

void LoadTNZS(void);
void ClearTNZS(void);

void LoadTNZSBL(void);
void ClearTNZSBL(void);

void LoadArk2DohUS(void);
void ClearArk2DohUS(void);

void LoadArk2DohJP(void);

void LoadExtermination(void);
void ClearExtermination(void);

void load_kageki(void);
void clear_kageki(void);

void load_chuka_taisen(void);
void clear_chuka_taisen(void);

void LoadDrToppel(void);
void ClearDrToppel(void);

void LoadPlumpPop(void);
void ClearPlumpPop(void);

void DrawTNZSSystem(void);

void ExecuteTNZSSystemFrame(void);
void ExecuteInsectorXFrame(void);
void ExecuteDrToppelFrame(void);

