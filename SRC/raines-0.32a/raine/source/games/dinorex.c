/******************************************************************************/
/*                                                                            */
/*                    DINO REX (C) 1992 TAITO CORPORATION                     */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "dinorex.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

/******************
   DINO REX WORLD
 ******************/

static struct DIR_INFO dino_rex_dirs[] =
{
   { "dino_rex", },
   { "dinorex", },
   { NULL, },
};

static struct ROM_INFO dino_rex_roms[] =
{
   { "drex_06m.rom", 0x00100000, 0x52f62835, 0, 0, 0, },
   { "drex_02m.rom", 0x00200000, 0x6c304403, 0, 0, 0, },
   { "drex_03m.rom", 0x00200000, 0xfc9cdab4, 0, 0, 0, },
   { "drex_04m.rom", 0x00100000, 0x3800506d, 0, 0, 0, },
   { "drex_05m.rom", 0x00100000, 0xe2ec3b5d, 0, 0, 0, },
   { "drex_01m.rom", 0x00200000, 0xd10e9c7d, 0, 0, 0, },
   { "drex_07m.rom", 0x00100000, 0x28262816, 0, 0, 0, },
   { "drex_08m.rom", 0x00080000, 0x377b8b7b, 0, 0, 0, },
   {  "drex_12.rom", 0x00010000, 0x8292c7c1, 0, 0, 0, },
   {  "drex_14.rom", 0x00080000, 0xe6aafdac, 0, 0, 0, },
   {  "drex_16.rom", 0x00080000, 0xcedc8537, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO dino_rex_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x04300E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x04300E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x04300E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x04300E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x043004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x043004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x043004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x043004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x043004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x043004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x043004, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x043006, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x043006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x043006, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x043006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x043006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x043006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x043006, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_dino_rex_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_dino_rex_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Damage",                0x0C, 0x04 },
   { MSG_NORMAL,              0x0C, 0x00 },
   { "Small",                 0x08, 0x00 },
   { "Big",                   0x04, 0x00 },
   { "Biggest",               0x00, 0x00 },
   { "Timer",                 0x10, 0x02 },
   { MSG_NORMAL,              0x10, 0x00 },
   { "Fast",                  0x00, 0x00 },
   { "Match Type",            0x20, 0x02 },
   { "Best of 3",             0x20, 0x00 },
   { "Single",                0x00, 0x00 },
   { "2 Player Mode",         0x40, 0x02 },
   { "Upright",               0x40, 0x00 },
   { "Cocktail",              0x00, 0x00 },
   { "Upright Controls",      0x80, 0x02 },
   { "Dual",                  0x80, 0x00 },
   { "Single",                0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO dino_rex_dsw[] =
{
   { 0x043000, 0xFF, dsw_data_dino_rex_0 },
   { 0x043002, 0xFF, dsw_data_dino_rex_1 },
   { 0,        0,    NULL,      },
};

/*
static struct ROMSW_DATA romsw_data_dino_rex_0[] =
{
   { "Taito Japan (Japanese)", 0x01 },
   { "Taito America",          0x02 },
   { "Taito Worldwide",        0x03 },
   { NULL,                     0    },
};

static struct ROMSW_INFO dino_rex_romsw[] =
{
   { 0x07FFFF, 0x03, romsw_data_dino_rex_0 },
   { 0,        0,    NULL },
};
*/

static struct VIDEO_INFO dino_rex_video =
{
   DrawDinoRex,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_dino_rex =
{
   dino_rex_dirs,
   dino_rex_roms,
   dino_rex_inputs,
   dino_rex_dsw,
   NULL,

   LoadDinoRex,
   ClearDinoRex,
   &dino_rex_video,
   ExecuteDinoRexFrame,
   "dinorex",
   "Dino Rex",
   "ダイノレックス",
   COMPANY_ID_TAITO,
   "D39",
   1992,
   taito_ym2610_sound,
   GAME_BEAT,
};

/***************
   DINO REX US
 ***************/

static struct DIR_INFO dino_rex_us_dirs[] =
{
   { "dino_rex_us", },
   { "dinorexu", },
   { ROMOF("dinorex"), },
   { CLONEOF("dinorex"), },
   { NULL, },
};

static struct ROM_INFO dino_rex_us_roms[] =
{
   { "drex_06m.rom", 0x00100000, 0x52f62835, 0, 0, 0, },
   { "drex_02m.rom", 0x00200000, 0x6c304403, 0, 0, 0, },
   { "drex_03m.rom", 0x00200000, 0xfc9cdab4, 0, 0, 0, },
   { "drex_04m.rom", 0x00100000, 0x3800506d, 0, 0, 0, },
   { "drex_05m.rom", 0x00100000, 0xe2ec3b5d, 0, 0, 0, },
   { "drex_01m.rom", 0x00200000, 0xd10e9c7d, 0, 0, 0, },
   { "drex_07m.rom", 0x00100000, 0x28262816, 0, 0, 0, },
   { "drex_08m.rom", 0x00080000, 0x377b8b7b, 0, 0, 0, },
   {  "drex_12.rom", 0x00010000, 0x8292c7c1, 0, 0, 0, },
   {  "drex_14.rom", 0x00080000, 0xe6aafdac, 0, 0, 0, },
   { "drex_16u.rom", 0x00080000, 0xfe96723b, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct DSW_DATA dsw_data_dino_rex_us_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { "Price to Continue",     0xC0, 0x04 },
   { "Same as Start",         0xC0, 0x00 },
   { MSG_1COIN_1PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO dino_rex_us_dsw[] =
{
   { 0x043000, 0xFF, dsw_data_dino_rex_us_0 },
   { 0x043002, 0xFF, dsw_data_dino_rex_1 },
   { 0,        0,    NULL,      },
};

struct GAME_MAIN game_dino_rex_us =
{
   dino_rex_us_dirs,
   dino_rex_us_roms,
   dino_rex_inputs,
   dino_rex_us_dsw,
   NULL,

   LoadDinoRex,
   ClearDinoRex,
   &dino_rex_video,
   ExecuteDinoRexFrame,
   "dinorexu",
   "Dino Rex (US)",
   "ダイノレックス (US)",
   COMPANY_ID_TAITO,
   "D39",
   1992,
   taito_ym2610_sound,
   GAME_BEAT,
};

/******************
   DINO REX JAPAN
 ******************/

static struct DIR_INFO dino_rex_jp_dirs[] =
{
   { "dino_rex_jp", },
   { "dinorexj", },
   { ROMOF("dinorex"), },
   { CLONEOF("dinorex"), },
   { NULL, },
};

static struct ROM_INFO dino_rex_jp_roms[] =
{
   { "drex_06m.rom", 0x00100000, 0x52f62835, 0, 0, 0, },
   { "drex_02m.rom", 0x00200000, 0x6c304403, 0, 0, 0, },
   { "drex_03m.rom", 0x00200000, 0xfc9cdab4, 0, 0, 0, },
   { "drex_04m.rom", 0x00100000, 0x3800506d, 0, 0, 0, },
   { "drex_05m.rom", 0x00100000, 0xe2ec3b5d, 0, 0, 0, },
   { "drex_01m.rom", 0x00200000, 0xd10e9c7d, 0, 0, 0, },
   { "drex_07m.rom", 0x00100000, 0x28262816, 0, 0, 0, },
   { "drex_08m.rom", 0x00080000, 0x377b8b7b, 0, 0, 0, },
   {  "drex_12.rom", 0x00010000, 0x8292c7c1, 0, 0, 0, },
   {  "drex_14.rom", 0x00080000, 0xe6aafdac, 0, 0, 0, },
   {   "d39_13.rom", 0x00080000, 0xae496b2f, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct DSW_DATA dsw_data_dino_rex_jp_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_1COIN_2PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_1COIN_2PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO dino_rex_jp_dsw[] =
{
   { 0x043000, 0xFF, dsw_data_dino_rex_jp_0 },
   { 0x043002, 0xFF, dsw_data_dino_rex_1 },
   { 0,        0,    NULL,      },
};

struct GAME_MAIN game_dino_rex_jp =
{
   dino_rex_jp_dirs,
   dino_rex_jp_roms,
   dino_rex_inputs,
   dino_rex_jp_dsw,
   NULL,

   LoadDinoRex,
   ClearDinoRex,
   &dino_rex_video,
   ExecuteDinoRexFrame,
   "dinorexj",
   "Dino Rex (Japan)",
   "ダイノレックス (Japan)",
   COMPANY_ID_TAITO,
   "D39",
   1992,
   taito_ym2610_sound,
   GAME_BEAT,
};

static UINT8 *RAM_INPUT;
static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

void LoadDinoRex(void)
{
   int ta,tb,tc;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0x300000))) return;
   if(!(GFX=AllocateMem(0x200000+0xC00000))) return;

   GFX_BG0 = GFX + 0x000000;
   GFX_SPR = GFX + 0x200000;

   if(!load_rom("drex_06m.rom", ROM, 0x100000)) return;
   tb = 0;
   for(ta=0;ta<0x100000;ta+=2,tb+=4){
      tc=ReadWord(&ROM[ta]);
      GFX_BG0[tb+0] = ((tc&0xF000)>>12);
      GFX_BG0[tb+1] = ((tc&0x0F00)>> 8);
      GFX_BG0[tb+2] = ((tc&0x00F0)>> 4);
      GFX_BG0[tb+3] = ((tc&0x000F)>> 0);
   }

   tb = 0;
   if(!load_rom("drex_01m.rom", ROM, 0x200000)) return;
   for(ta=0;ta<0x200000;ta++){
      GFX_SPR[tb++]=ROM[ta]&15;
      GFX_SPR[tb++]=ROM[ta]>>4;
   }
   if(!load_rom("drex_02m.rom", ROM, 0x200000)) return;
   for(ta=0;ta<0x200000;ta++){
      GFX_SPR[tb++]=ROM[ta]&15;
      GFX_SPR[tb++]=ROM[ta]>>4;
   }
   if(!load_rom("drex_03m.rom", ROM, 0x200000)) return;
   for(ta=0;ta<0x200000;ta++){
      GFX_SPR[tb++]=ROM[ta]&15;
      GFX_SPR[tb++]=ROM[ta]>>4;
   }

   // Load 68000 PROGRAM ROMS

   if(!load_rom("drex_14.rom", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom_index(10, RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   // Load 68000 DATA ROMS (already byteswapped)

   if(!load_rom("drex_04m.rom", ROM+0x100000, 0x100000)) return;
   if(!load_rom("drex_05m.rom", ROM+0x200000, 0x100000)) return;

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x54000;
   if(!load_rom("drex_12.rom", Z80ROM, 0x10000)) return;		// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x180000))) return;
   if(!load_rom("drex_08m.rom",PCMROM,0x80000)) return;			// ADPCM A rom
   if(!load_rom("drex_07m.rom",PCMROM+0x80000,0x100000)) return;	// ADPCM B rom
   YM2610SetBuffers(PCMROM, PCMROM+0x80000, 0x080000, 0x100000);

   AddTaitoYM2610(0x0211, 0x017A, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x50000);

   RAM_VIDEO  = RAM+0x30000;
   RAM_SCROLL = RAM+0x43100;
   RAM_INPUT  = RAM+0x43000;
   GFX_FG0    = RAM+0x50000;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x08000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x0C000);

   set_colour_mapper(&col_Map_15bit_RRRRGGGGBBBBRGBx);
   InitPaletteMap(RAM+0x40000, 0x100, 0x10, 0x8000);

   WriteWord68k(&ROM[0x01084],0x4EF9);		// JMP $7FF00
   WriteLong68k(&ROM[0x01086],0x0007FF00);

   WriteWord68k(&ROM[0x7FF00],0x4EB9);
   WriteLong68k(&ROM[0x7FF02],0x000032FC);
   WriteLong68k(&ROM[0x7FF06],0x13FC0000);	// Stop 68000
   WriteLong68k(&ROM[0x7FF0A],0x00AA0000);
   WriteWord68k(&ROM[0x7FF0E],0x6100-16);	// Loop

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
   //tc0100scn[0].layer[0].mapper	=&Map_15bit_RRRRGGGGBBBBRGBx;
   tc0100scn[0].layer[0].tile_mask=0x7FFF;
   tc0100scn[0].layer[0].scr_x	=19;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
   //tc0100scn[0].layer[1].mapper	=&Map_15bit_RRRRGGGGBBBBRGBx;
   tc0100scn[0].layer[1].tile_mask=0x7FFF;
   tc0100scn[0].layer[1].scr_x	=19;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
   //tc0100scn[0].layer[2].mapper	=&Map_15bit_RRRRGGGGBBBBRGBx;
   tc0100scn[0].layer[2].scr_x	=19;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);
/*
   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM+0x10000;
   tc0200obj.RAM_B	= RAM+0x18000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
   tc0200obj.mapper	= &Map_15bit_RRRRGGGGBBBBRGBx;
   tc0200obj.tile_mask	= 0xFFFF;
   tc0200obj.ofs_x	= 0;	//0-0x2E;
   tc0200obj.ofs_y	= 0;	//0-0x1F;

   tc0200obj.cols	= 16;

   tc0200obj.RAM_TILE	= RAM+0x20000;
   tc0200obj.RAM_TILE_B	= RAM+0x20800;

   init_tc0200obj();
*/
/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x48000);

   AddMemFetch(0x000000, 0x2FFFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x2FFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x600000, 0x60FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddReadByte(0x400000, 0x400FFF, NULL, RAM+0x020000);			// EXT OBJECT RAM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x300000, 0x30001F, NULL, RAM+0x043000);			// INPUT
   AddReadByte(0xA00000, 0xA00003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x610000, 0x61FFFF, NULL, RAM+0x000000);			// 68000 RAM [MIRROR/BAD CODING]
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x2FFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x600000, 0x60FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM+0x010000);			// OBJECT RAM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x400000, 0x400FFF, NULL, RAM+0x020000);			// EXT OBJECT RAM
   AddReadWord(0x500000, 0x501FFF, NULL, RAM+0x040000);			// COLOR RAM
   AddReadWord(0x300000, 0x30001F, NULL, RAM+0x043000);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x600000, 0x60FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM+0x010000);		// OBJECT RAM
   AddWriteByte(0x906000, 0x906FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x400000, 0x400FFF, NULL, RAM+0x020000);		// EXT OBJECT RAM
   AddWriteByte(0xA00000, 0xA00003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x300000, 0x30001F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x600000, 0x60FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM+0x010000);		// OBJECT RAM
   AddWriteWord(0x906000, 0x906FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x500000, 0x501FFF, NULL, RAM+0x040000);		// COLOR RAM
   AddWriteWord(0x400000, 0x400FFF, NULL, RAM+0x020000);		// EXT OBJECT RAM
   AddWriteWord(0x700000, 0x7000FF, NULL, RAM+0x042000);		// ??? RAM
   AddWriteWord(0x920000, 0x92000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x300000, 0x30001F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearDinoRex(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      //save_debug("RAM.bin",RAM,0x080000,1);
   #endif
}

void ExecuteDinoRexFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(6,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(6,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 5);

   Taito2610_Frame();			// Z80 and YM2610
}


void DrawDinoRex(void)
{
   int x,y,ta,tb,sb,zz;
   int xn,yn,td,th;
   int relx,rely,relx2,rely2;
   UINT8 *map;

   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   x=0;
   y=0;
   xn=0;
   yn=0;
   tb=0;

   if((ReadWord(&RAM[0x1800A])&0x0001)==0){
      th=0x10000;
      sb=0x20000;
   }
   else{
      th=0x18000;
      sb=0x20800;
   }

   relx2=ReadWord(&RAM[th+0x14])&0x7FF;
   rely2=ReadWord(&RAM[th+0x16])&0x7FF;

   relx=relx2;
   rely=rely2;

   for(zz=th;zz<(th+0x4000);zz+=16,sb+=2){

      if(RAM[zz+11]!=0){zz=th+0x4000;}

      td=RAM[zz+9];

      if((td&0x40)==0){
         if((td&0x80)==0){			// 0x04
         x=((16-83)+ReadWord(&RAM[zz+4])+relx)&0x7FF;
         xn=x;
         }
         else{
         x=xn;
         }
      }
      else{
         if((td&0x80)!=0){
            x=(x+16)&0x7FF;
         }
      }

      if((td&0x10)==0){
         if((td&0x04)==0){
         y=(16+ReadWord(&RAM[zz+6])+rely)&0x7FF;
         yn=y;
         }
         else{
         y=yn;
         }
      }
      else{
         if((td&0x20)!=0){
            y=(y+16)&0x7FF;
         }
      }

         if((ReadWord(&RAM[zz+4])&0x4000)!=0){
            relx=ReadWord(&RAM[zz+4])&0x7FF;
            rely=ReadWord(&RAM[zz+6])&0x7FF;
            relx+=relx2;
            rely+=rely2;
         }

      if((td&0x04)==0){
         tb=RAM[zz+8];
      }

      if((x>16)&&(y>16)&&(x<320+32)&&(y<224+32)){

      ta=((RAM[sb]<<16)|(RAM[zz]<<8));
      if(ta!=0){

      MAP_PALETTE_MAPPED_NEW(
         tb,
         16,
         map
      );

      switch(td&0x03){
      case 0x00: Draw16x16_Trans_Mapped_Rot(&GFX[ta+0x200000],x,y,map); break;
      case 0x01: Draw16x16_Trans_Mapped_FlipY_Rot(&GFX[ta+0x200000],x,y,map); break;
      case 0x02: Draw16x16_Trans_Mapped_FlipX_Rot(&GFX[ta+0x200000],x,y,map); break;
      case 0x03: Draw16x16_Trans_Mapped_FlipXY_Rot(&GFX[ta+0x200000],x,y,map); break;
      }

      }

      }

   }

//render_tc0200obj_mapped_opwolf3();

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);

}

