
void load_astyanax(void);
void load_the_lord_of_king(void);

void load_rodland(void);
void load_rodland_japanese(void);

void Load64thStreet(void);
void Load64thStreetJ(void);

void LoadP47USA(void);
void LoadP47J(void);

void load_iga_ninjyutsuden(void);
void load_soldam(void);
void load_chimera_beast(void);
void LoadSaintDragon(void);
void LoadPhantasm(void);
void LoadKickOff(void);
void LoadHachoo(void);
void LoadPlusAlpha(void);
void LoadAvengingSpirit(void);
void LoadCybattler(void);
void LoadEarthDefForce(void);
void LoadShingen(void);
void LoadLegendOfMakaj(void);
void LoadPeekABoo(void);

void DrawLegendOfMakaj(void);
void DrawPeekABoo(void);

void DrawMegaSystem1(void);
void DrawMegaSystem1Vertical(void);

void DrawMegaSystem2(void);
void DrawMegaSystem2Vertical(void);

void ExecuteMegaSystem1Frame(void);
void ExecuteBSPFrame(void);
void ExecuteMegaSystem1FrameKO(void);

void ExecuteMegaSystem2Frame(void);

void ClearMS1(void);

#define clear_chimera_beast	ClearMS1
#define clear_rodland_japanese	ClearMS1
#define clear_soldam		ClearMS1
#define ClearSaintDragon	ClearMS1
#define ClearP47J		ClearMS1
#define ClearPhantasm		ClearMS1
#define ClearKickOff		ClearMS1
#define ClearHachoo		ClearMS1
#define ClearPlusAlpha		ClearMS1
#define ClearAvengingSpirit	ClearMS1
#define ClearCybattler		ClearMS1
#define Clear64thStreet		ClearMS1
#define Clear64thStreetJ	ClearMS1
#define ClearEarthDefForce	ClearMS1
#define ClearShingen		ClearMS1
#define ClearLegendOfMakaj	ClearMS1
#define ClearAstyanax		ClearMS1
#define ClearP47USA		ClearMS1
#define clear_rodland		ClearMS1
#define ClearPeekABoo		ClearMS1


