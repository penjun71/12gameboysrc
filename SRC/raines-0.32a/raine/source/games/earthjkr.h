
void LoadEarthJoker(void);
void LoadMazeOfFlott(void);
void LoadGalmedes(void);

void ClearEarthJoker(void);

#define ClearMazeOfFlott	ClearEarthJoker
#define ClearGalmedes		ClearEarthJoker

void DrawEarthJoker(void);
void ExecuteEarthJokerFrame(void);
