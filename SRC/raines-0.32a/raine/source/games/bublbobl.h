void load_bubble_bobble(void);
void load_bubble_bobble_romstar(void);
void load_bobble_bobble(void);
void load_super_bobble_bobble(void);

void clear_bubble_bobble(void);

void draw_bubble_bobble(void);

void execute_bubble_bobble_frame(void);
