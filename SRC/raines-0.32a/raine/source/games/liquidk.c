/******************************************************************************/
/*                                                                            */
/*                    LIQUID KIDS (C) 1990 TAITO CORPORATION                  */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "liquidk.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

static struct DIR_INFO liquid_kids_dirs[] =
{
   { "liquid_kids", },
   { "liquidk", },
   { NULL, },
};

static struct ROM_INFO liquid_kids_roms[] =
{
   {  "lk_obj0.bin", 0x00080000, 0x67cc3163, 0, 0, 0, },
   {  "lk_obj1.bin", 0x00080000, 0xd2400710, 0, 0, 0, },
   {   "lk_scr.bin", 0x00080000, 0xc3364f9b, 0, 0, 0, },
   {   "lk_snd.bin", 0x00080000, 0x474d45a4, 0, 0, 0, },
   {     "lq09.bin", 0x00020000, 0x6ae09eb9, 0, 0, 0, },
   {     "lq10.bin", 0x00020000, 0x50bef2e0, 0, 0, 0, },
   {     "lq11.bin", 0x00020000, 0x42d2be6e, 0, 0, 0, },
   {     "lq12.bin", 0x00020000, 0xcb16bad5, 0, 0, 0, },
   {     "lq08.bin", 0x00010000, 0x413c310c, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO liquid_kids_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03210E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03210E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03210E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03210E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x032104, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x032104, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x032104, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x032104, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x032104, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x032104, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x032104, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x032106, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x032106, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x032106, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x032106, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x032106, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x032106, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x032106, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_liquid_kids_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COINAGE,               0x30, 0x04 },
   { "1/1; 1/2",              0x30, 0x00 },
   { "1/2; 1/1",              0x20, 0x00 },
   { "2/1; 1/1",              0x10, 0x00 },
   { "3/1; 1/1",              0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_liquid_kids_1[] =
{
   { MSG_DSWB_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "Bonus at",              0x0C, 0x04 },
   { "30k, 100k",             0x0C, 0x00 },
   { "30k, 150k",             0x08, 0x00 },
   { "50k, 250k",             0x04, 0x00 },
   { "50k, 350k",             0x00, 0x00 },
   { "Extra Lives",           0x30, 0x04 },
   { "2",                     0x30, 0x00 },
   { "1",                     0x20, 0x00 },
   { "3",                     0x10, 0x00 },
   { "4",                     0x00, 0x00 },
   { "Continue Play",         0x40, 0x02 },
   { MSG_ON,                  0x40, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DSWB_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO liquid_kids_dsw[] =
{
   { 0x032100, 0xFF, dsw_data_liquid_kids_0 },
   { 0x032102, 0xFF, dsw_data_liquid_kids_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_liquid_kids_0[] =
{
   { "Taito Japan (Japanese)", 0x01 },
   { "Taito America",          0x02 },
   { "Taito Japan",            0x03 },
   { NULL,                     0    },
};

static struct ROMSW_INFO liquid_kids_romsw[] =
{
   { 0x07FFFF, 0x03, romsw_data_liquid_kids_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO liquid_kids_video =
{
   DrawLiquidKids,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_liquid_kids =
{
   liquid_kids_dirs,
   liquid_kids_roms,
   liquid_kids_inputs,
   liquid_kids_dsw,
   liquid_kids_romsw,

   LoadLiquidKids,
   ClearLiquidKids,
   &liquid_kids_video,
   ExecuteLiquidKidsFrame,
   "liquidk",
   "Liquid Kids",
   "�~�Y�o�N��`��",
   COMPANY_ID_TAITO,
   "C49",
   1990,
   taito_ym2610_sound,
   GAME_PLATFORM,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static void BadWriteWord(UINT32 address, UINT16 data)
{
#ifdef RAINE_DEBUG
      if(address!=0x340000) print_debug("Ww(%06x,%04x) [%06x]\n",address,data,s68000context.pc);
#endif
}

void LoadLiquidKids(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x80000))) return;
   RAMSize=0x50000;

   if(!(GFX=AllocateMem(0x100000+0x200000))) return;

   GFX_BG0 = GFX+0x000000;
   GFX_SPR = GFX+0x100000;

   tb=0;
   if(!load_rom("lk_scr.bin", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta+=2){
      GFX[tb+0]=RAM[ta+1]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+2]=RAM[ta+0]>>4;
      GFX[tb+3]=RAM[ta+0]&15;
      tb+=4;
   }

   tb=0;
   if(!load_rom("lk_obj0.bin", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta++){
      GFX_SPR[tb++]=RAM[ta]&15;
      GFX_SPR[tb++]=RAM[ta]>>4;
   }
   if(!load_rom("lk_obj1.bin", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta++){
      GFX_SPR[tb++]=RAM[ta]&15;
      GFX_SPR[tb++]=RAM[ta]>>4;
   }
   for(ta=0x1D3100;ta<0x1D3400;ta++){			// Sprite Mask things (yuck!)
      GFX_SPR[ta]=0;
   }
   for(ta=0x011900;ta<0x011A00;ta++){			// Sprite Mask things (yuck!)
      GFX_SPR[ta]=0;
   }

   if(!(RAM=AllocateMem(0x40000+0x10000))) return;
   if(!(ROM=AllocateMem(0x80000))) return;

   if(!load_rom("lq09.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("lq11.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("lq10.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("lq12.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("lq08.bin", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("lk_snd.bin",PCMROM,0x80000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x080000, 0x080000);

   AddTaitoYM2610(0x01DD, 0x0189, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x10000;
   RAM_SCROLL = RAM+0x32000;
   RAM_OBJECT = RAM+0x20000;
   RAM_INPUT  = RAM+0x32100;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x4000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   GFX_FG0    = RAM+0x34000;

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x30000, 0x100, 0x10, 0x1000);


   // 68000 Speed Hack
   // ----------------

   WriteLong68k(&ROM[0x89F6],0x4E714E71);
   WriteLong68k(&ROM[0x89FA],0x4E714E71);

   // Fix ROM Checksum
   // ----------------

   WriteWord68k(&ROM[0x1DAE],0x4E75);

   // Fix Long Sound Wait
   // -------------------

   WriteWord68k(&ROM[0x017A],0x4E71);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x3FFF;
   tc0100scn[0].layer[0].scr_x	=16-3;
   tc0100scn[0].layer[0].scr_y	=8+16;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x3FFF;
   tc0100scn[0].layer[1].scr_x	=16-3;
   tc0100scn[0].layer[1].scr_y	=8+16;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=1;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=0;
   tc0100scn[0].layer[2].scr_y	=16;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT+0x0000;
   tc0200obj.RAM_B	= RAM_OBJECT+0x8000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x1FFF;
   tc0200obj.ofs_x	= 0;
   tc0200obj.ofs_y	= -16;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x34000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x300000, 0x30001F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x320000, 0x320003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x400000, 0x40000F, NULL, RAM+0x032200);			// ???
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x200000, 0x201FFF, NULL, RAM+0x030000);			// COLOR RAM
   AddReadWord(0x300000, 0x30001F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x300000, 0x30001F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0x400000, 0x40000F, NULL, RAM+0x032200);		// ???
   AddWriteByte(0x320000, 0x320003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x200000, 0x201FFF, NULL, RAM+0x030000);		// COLOR RAM
   AddWriteWord(0x300000, 0x30001F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x380000, 0x38000F, NULL, RAM+0x032180);		// ???
   AddWriteWord(0x600000, 0x60000F, NULL, RAM+0x032280);		// ???
   AddWriteWord(0xB00000, 0xB000FF, NULL, RAM+0x032300);		// ???
   AddWriteWord(0x000000, 0xFFFFFF, BadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearLiquidKids(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x080000,1);
      //save_debug("RAM.bin",RAM,0x034000,1);
      //save_debug("GFX.bin",GFX,0x600000,0);
   #endif
}

static int last_scr,last_scr2;

void ExecuteLiquidKidsFrame(void)
{
   last_scr=ReadLong(&RAM_OBJECT[0x24]); // Keep Sprites and Scrolling in sync (sprites are 1 frame behind)

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 5);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);

   Taito2610_FrameCameltry();
   //Taito2610_Frame();				// Z80 and YM2610
}

void DrawLiquidKids(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_r180_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_r180_mapped(0,1);

   // OBJECT
   // ------

   last_scr2=ReadLong(&RAM_OBJECT[0x24]);	// [Store]
   WriteLong(&RAM_OBJECT[0x24],last_scr);	// Delay Scrolling 1 frame

   render_tc0200obj_r180_mapped();

   WriteLong(&RAM_OBJECT[0x24],last_scr2);	// [Restore]

   // FG0
   // ---

   render_tc0100scn_layer_r180_mapped(0,2);
}

