/******************************************************************************/
/*                                                                            */
/*  'Default' driver, used to init some stuff with default values mainly...   */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "arpro.h"
#include "default.h"
#include "savegame.h"
#include "sasound.h"		// sample support routines

void LoadDefault(void)
{
   ResetMemoryPool();				// Init game memory pool

   reset_arpro();				// Init action replay

   ExecuteSoundFrame=NULL;			// Default execution for 1 frame (sound cpu execution)

   Clear68000List();				// Clear M68000 memory lists
   StarScreamEngine=0;				// No M68000 by default

   ClearZ80List();				// Clear Z80 memory lists
   MZ80Engine=0;				// No Z80 by default

   #ifdef HAVE_6502
   ClearM6502List();				// Clear M6502 memory lists
   M6502Engine=0;				// No M6502 by default
   #endif

   GameSound=0;					// No Sound emulation by default
   GameMouse=0;					// No Mouse driver by default

   raine_cfg.req_pause_game = 0;
   raine_cfg.req_save_screen = 0;

   dir_cfg.last_screenshot_num = 0;

   SaveSlot=0;					// Reset save slot to 0
   SaveDataCount=0;				// No save data
   SaveCallbackCount=0;				// No save callbacks

   RAMSize=0x80000;				// Size of RAM memory allocated (should be 0 here really)

   clear_eeprom_list();				// No EEPROMs mapped

   if(raine_cfg.show_fps_mode==2) raine_cfg.show_fps_mode=0;			// Don't like it running from time=0

   PCMROM=NULL;					// Might as well clear these too
   Z80ROM=NULL;
   ROM=NULL;
   RAM=NULL;
   GFX=NULL;

   tile_list_count = 0;

   set_white_pen(255);

   current_cmap_func = NULL;
   current_colour_mapper = NULL;

   reset_layer_switches();
}

void ClearDefault(void)
{
   int i;

#ifndef NO020
   MC68020 = 0;
#endif
   reset_arpro();				// Free action replay

   saDestroySound(1);				// Free all sound resources

   destroy_palette_map();			// Free colour mapping resources

   if(raine_cfg.show_fps_mode==2) raine_cfg.show_fps_mode=0;			// Don't like it running from time=0

   FreeMemoryPool();				// Free anything left in the memory pool

   InputCount = 0;

   for(i = 0; i < MAX_DIPSWITCHES; i ++)
      dipswitch[i].statcount = 0;

   LanguageSw.Count = 0;
}

