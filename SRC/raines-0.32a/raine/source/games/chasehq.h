
void load_chase_hq(void);
void load_chase_hq_japanese(void);
void clear_chase_hq(void);

void LoadNightStr(void);
void ClearNightStr(void);

void DrawChaseHQ(void);
void ExecuteChaseHQFrame(void);
void ExecuteNightStrFrame(void);
