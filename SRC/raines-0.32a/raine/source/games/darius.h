void LoadDarius(void);
void LoadDariusExtra(void);
void LoadDariusAlt(void);

void ClearDarius(void);
void DrawDarius(void);
void ExecuteDariusFrame(void);
