/******************************************************************************/
/*                                                                            */
/*                        CAVE / ATLUS GAMES (C) 1998                         */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "cave.h"
#include "cat93c46.h"
#include "mame/handlers.h"
#include "mame/eeprom.h"
#include "sasound.h"		// sample support routines
#include "debug.h"
#include "savegame.h"
#include "lspr16x16.h"

#undef RAINE_DEBUG

/******************************************************************************/

#define MAX_PRI         16              // 16 levels of priority

/* Definitions for priorities :
 * bits 0-1 : tile number
 *            0 = sprites (OBJ)
 *            1-3 : bg layers 0-2
 * bits 2-3 : priority (from 0 to 3)
 */

#define MAX_TILES       0x8000          // 0x4000*0x14=0xA0000 (640kb)

static struct TILE_Q
{
   UINT32 tile;                          // Tile number
   UINT16 x,y;                           // X,Y position
   UINT8 *map;                          // Colour map data
   UINT8 flip;                          // Flip X/Y Axis
   UINT16 ww,hh;
   UINT16 zoomx,zoomy;
   struct TILE_Q *next;                 // Next item with equal priority
} TILE_Q;

static struct TILE_QS
{
   UINT32 tile;                          // Tile number
   INT16 x,y;                           // X,Y position
   UINT8 *map;                          // Colour map data
   UINT8 flip;                          // Flip X/Y Axis
   UINT8 *dy;                           // offset for linescroll (adr)
  struct TILE_QS *next;                 // Next item with equal priority
} TILE_QS;

static struct TILE_Q *TileQueue;               // full list
static struct TILE_Q *last_tile;               // last tile in use
static struct TILE_Q *next_tile[MAX_PRI];      // next tile for each priority
static struct TILE_Q *first_tile[MAX_PRI];     // first tile for each priority

static struct TILE_QS *TileQueue_scroll;               // full list
static struct TILE_QS *last_tile_scroll;               // last tile in use
static struct TILE_QS *next_tile_scroll[MAX_PRI];      // next tile for each priority
static struct TILE_QS *first_tile_scroll[MAX_PRI];     // first tile for each priority

/******************************************************************************/

// Fake dsw for guwange
static struct DSW_DATA dsw_guwange_0[] =
{

   { "Zoom",           0x01, 0x02 },
   { MSG_ON,           0x00, 0x00 },
   { MSG_OFF,          0x01, 0x00 },
};

static struct DSW_INFO guwange_dsw[] =
{
   { 0x005, 0xFE, dsw_guwange_0 },
};

static UINT32 gfx_bg0_count;
static UINT32 gfx_bg1_count;
static UINT32 gfx_bg2_count;
static UINT32 gfx_obj_count;

static const int *scrolldx,*scrolldx_flip;
int raster_bg;
static int latch;
static int limitx, gamex;

const int
  ddonpach_scrolldx_flip[] = { 0x57, 0x56, 0x55 -7 +1 },
  ddonpach_scrolldx[]      = { 0x6c, 0x6d, 0x6e + 7 },

  donpachi_scrolldx_flip[] = { 0x57, 0x56, 0x55 -7 +1+2 },
  donpachi_scrolldx[]      = { 0x6c, 0x6d, 0x6e +7-2 },

  feveron_scrolldx_flip[] = { 0x54, 0x53, 0 },
  feveron_scrolldx[]      = { 0x6c, 0x6d, 0 },

  uopoko_scrolldx_flip[] = { 0x54, 0, 0 },
  uopoko_scrolldx[]      = { 0x6d, 0, 0 },

  esprade_scrolldx_flip[] = { 0x57, 0x56, 0x55 },
  esprade_scrolldx[]      = { 0x6c, 0x6d, 0x6e }, // also hotdogst

  hotdogst_scrolldx_flip[]= { 0x57-0x40, 0x56-0x40, 0x55-0x40 },

  guwange_scrolldx_flip[] = { 0x57+2, 0x56+2, 0x55+2 },
  guwange_scrolldx[]      = { 0x6c-2, 0x6d-2, 0x6e -2 };
  
    
static short vblank_irq,sound_irq,romset;
// romset is 1 for ddonpach (bg2 and register places different...)
// 2 for uopoko (layer alignement different)
// 3 for dfeveron (little slide in flipped bg0...)
static unsigned long sprite_size; // To prevent overflows...
static short flipx,flipy; // global flipping (sprites & layers)

// Nb of sprites / rows for a tile
#define DIM_NX 0x20 
// _ / col
#define DIM_NY 0x20 

/* Update the IRQ state based on all possible causes */
static int irq_counter = 0;
static UINT8 *dsw_buffer = &input_buffer[5];

static void setup_cave_game() {
  limitx = current_game->video_info->screen_x + current_game->video_info->border_size+16;
  gamex = current_game->video_info->screen_x;
  if(!(TileQueue = (struct TILE_Q *) AllocateMem(sizeof(TILE_Q)*MAX_TILES)))return;
   memset(RAM+0x00000,0x00,0x80000);
   AddMemFetch(-1, -1, NULL);
   
   AddRWBW(-1, -1, NULL, NULL);
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);      // <Bad Reads> 
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);      // <Bad Reads>
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);      // <Bad Writes>
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);      // <Bad Writes>
   AddInitMemory();   // Set Starscream mem pointers... 
}

static void update_irq_state(void)
{
  //  fprintf(stderr,"vb %d so %d\n",vblank_irq,sound_irq);
  if ((vblank_irq || sound_irq) && irq_counter++<3)
	  cpu_interrupt(CPU_68K_0, 1);
}


/* Called by the YMZ280B to set the IRQ state */
static void sound_irq_gen(int state)
{
	sound_irq = (state != 0);  
        update_irq_state();  
}

static void irqhandler(int irq)
{
  if (irq){
    cpu_interrupt(CPU_Z80_0, 0x38);
  }
}

static struct YMZ280Binterface ymz280b_intf =
{
   1,
   { 16934400 },
   { REGION_SMP1 },
   { YM3012_VOL(255,MIXER_PAN_LEFT,255,MIXER_PAN_RIGHT) },   
   { sound_irq_gen }
};

static struct OKIM6295interface donpachi_okim6295_interface =
{
	2,
	{ 8330, 16000 },
	{ REGION_SMP1, REGION_SMP2 },
	{ 225, 225 }
};

static struct YM2203interface hotdogst_ym2203_interface =
{
	1,			/* 1 chip */
	3000000,	/* 3 MHz ??? */
	{ YM2203_VOL(75,75) },
	{ 0 },
	{ 0 },
	{ 0 },
	{ 0 },
	{ irqhandler }
};

static struct OKIM6295interface hotdogst_okim6295_interface =
{
	1,                  /* 1 chip */
	{ 8000 },           /* 8000Hz frequency? */
	{ REGION_SMP1 },	/* memory region */
	{ 200 }
};

struct SOUND_INFO donpachi_sound[] =
{
   { SOUND_M6295,  &donpachi_okim6295_interface,  },
   { 0,             NULL,               },
};

struct SOUND_INFO cave_sound[] =
{
   { SOUND_YMZ280B,  &ymz280b_intf,  },
   { 0,             NULL,               },
};

struct SOUND_INFO hotdogst_sound[] =
{
   { SOUND_YM2203,  &hotdogst_ym2203_interface,  },
   { SOUND_M6295, &hotdogst_okim6295_interface,},
};

static struct DIR_INFO esprade_dirs[] =
{
   { "esprade", },
   { NULL, },
};

static struct DIR_INFO hotdogst_dirs[] =
{
   { "hotdogst", },
   { "HotDogStorm" },
   { NULL, },
};

static struct DIR_INFO donpachi_dirs[] =
{
   { "donpachi", },
   { NULL, },
};

static struct DIR_INFO guwange_dirs[] =
{
   { "guwange", },
   { NULL, },
};

static struct DIR_INFO ddonpach_dirs[] =
{
  { "ddonpach", },
  { NULL, },
};

static struct ROM_INFO esprade_roms[] =
{
   {      "u42.bin", 0x00080000, 0x0718c7e5,REGION_ROM1,0,LOAD_8_16 },
   {      "u41.bin", 0x00080000, 0xdef30539,REGION_ROM1,1,LOAD_8_16 },

   {      "u54.bin", 0x00400000, 0xe7ca6936,REGION_GFX1,0x000000,LOAD8X8_16X16 },
   {      "u55.bin", 0x00400000, 0xf53bd94f,REGION_GFX1,0x400000,LOAD8X8_16X16 },

   {      "u52.bin", 0x00400000, 0xe7abe7b4,REGION_GFX2,0x000000,LOAD8X8_16X16 },
   {      "u53.bin", 0x00400000, 0x51a0f391,REGION_GFX2,0x400000,LOAD8X8_16X16 },

   {      "u51.bin", 0x00400000, 0x0b9b875c,REGION_GFX3,0x000000,LOAD8X8_16X16 },
   
   {      "u63.bin", 0x00400000, 0x2f2fe92c,REGION_GFX4,0x000000,LOAD_8_16S },
   {      "u64.bin", 0x00400000, 0x491a3da4,REGION_GFX4,0x000001,LOAD_8_16S },
   {      "u65.bin", 0x00400000, 0x06563efe,REGION_GFX4,0x800000,LOAD_8_16S },
   {      "u66.bin", 0x00400000, 0x7bbe4cfc,REGION_GFX4,0x800001,LOAD_8_16S },

   {      "u19.bin", 0x00400000, 0xf54b1cab,REGION_SMP1,0,LOAD_NORMAL },
   {           NULL,          0,          0, },
};

static struct ROM_INFO donpachi_roms[] =
{
   {      "prg.u29", 0x00080000, 0x6be14af6,REGION_ROM1,0,LOAD_SWAP_16 },

   {      "atdp.u54", 0x100000, 0x6bda6b66,REGION_GFX1,0x000000, LOAD_NORMAL },

   {      "atdp.u57", 0x100000, 0x0a0e72b9,REGION_GFX2,0x000000,LOAD_NORMAL },

   {      "u58.bin", 0x00040000, 0x285379ff,REGION_GFX3,0x000000,LOAD_NORMAL },
   
   {      "atdp.u44",0x00200000, 0x7189e953,REGION_GFX4,0x000000,LOAD_SWAP_16 },
   {      "atdp.u45",0x00200000, 0x6984173f,REGION_GFX4,0x200000,LOAD_SWAP_16 },

   {      "atdp.u33", 0x00200000, 0xd749de00,REGION_SMP1,0x40000,LOAD_NORMAL },

   {      "atdp.u32", 0x00100000, 0x0d89fcca,REGION_SMP2,0x40000,LOAD_NORMAL },
   {      "atdp.u33", 0x00200000, 0xd749de00,REGION_SMP2,0x140000,LOAD_NORMAL },   
   {           NULL,          0,          0, },
};

static struct ROM_INFO guwange_roms[] =
{
  { "gu-u0127.bin", 0x00080000, 0xf86b5293, REGION_ROM1, 0x0000000, LOAD_8_16,   },
  { "gu-u0129.bin", 0x00080000, 0x6c0e3b93, REGION_ROM1, 0x0000001, LOAD_8_16,   },

  {     "u101.bin", 0x00800000, 0x0369491f, REGION_GFX1, 0x0000000, LOAD8X8_16X16, },

  {   "u10102.bin", 0x00400000, 0xe28d6855, REGION_GFX2, 0x0000000, LOAD8X8_16X16, },
   {   "u10103.bin", 0x00400000, 0x0fe91b8e, REGION_GFX3, 0x0000000, LOAD8X8_16X16, },

  {     "u083.bin", 0x00800000, 0xadc4b9c4, REGION_GFX4, 0x0000000, LOAD_8_16S, },
   {     "u082.bin", 0x00800000, 0x3d75876c, REGION_GFX4, 0x0000001, LOAD_8_16S, },
   {     "u086.bin", 0x00400000, 0x188e4f81, REGION_GFX4, 0x1000000, LOAD_8_16S, },
   {     "u085.bin", 0x00400000, 0xa7d5659e, REGION_GFX4, 0x1000001, LOAD_8_16S, },
   {    "u0462.bin", 0x00400000, 0xb3d75691, REGION_SMP1, 0, LOAD_NORMAL, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct ROM_INFO ddonpach_roms[] =
{
   {      "u27.bin", 0x00080000, 0x2432ff9b, REGION_ROM1, 0x0000000, LOAD_8_16,    },
   {      "u26.bin", 0x00080000, 0x4f3a914a, REGION_ROM1, 0x0000001, LOAD_8_16,    },

   {      "u60.bin", 0x00200000, 0x903096a7, REGION_GFX1, 0x0000000, LOAD_NORMAL,  },

   {      "u61.bin", 0x00200000, 0xd89b7631, REGION_GFX2, 0x0000000, LOAD_NORMAL,  },

   {      "u62.bin", 0x00200000, 0x292bfb6b, REGION_GFX3, 0x0000000, LOAD_NORMAL,  },

   {      "u50.bin", 0x00200000, 0x14b260ec, REGION_GFX4, 0x0000000, LOAD_SWAP_16, },
   {      "u51.bin", 0x00200000, 0xe7ba8cce, REGION_GFX4, 0x0200000, LOAD_SWAP_16, },
   {      "u52.bin", 0x00200000, 0x02492ee0, REGION_GFX4, 0x0400000, LOAD_SWAP_16, },
   {      "u53.bin", 0x00200000, 0xcb4c10f0, REGION_GFX4, 0x0600000, LOAD_SWAP_16, },
   {       "u6.bin", 0x00200000, 0x9dfdafaf, REGION_SMP1, 0x0000000, LOAD_NORMAL, },
   {       "u7.bin", 0x00200000, 0x795b17d5, REGION_SMP1, 0x0200000, LOAD_NORMAL, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct ROM_INFO hotdogst_roms[] =
{
   {       "mp3u29", 0x00080000, 0x1f4e5479,REGION_ROM1,0,LOAD_8_16 },
   {       "mp4u28", 0x00080000, 0x6f1c3c4b,REGION_ROM1,1,LOAD_8_16 },

   {       "mp2u19", 0x00040000, 0xff979ebe,REGION_ROM2,0,LOAD_NORMAL },

   {       "mp7u56", 0x080000, 0x87c21c50,REGION_GFX1,0x000000, LOAD_NORMAL },

   {       "mp6u61", 0x080000, 0x4dafb288,REGION_GFX2,0x000000,LOAD_NORMAL },

   {       "mp5u64",0x0080000, 0x9b26458c,REGION_GFX3,0x000000,LOAD_NORMAL },
   
   {      "mp9u55",0x00200000, 0x258d49ec,REGION_GFX4,0x000000, LOAD_NORMAL },
   {      "mp8u54",0x00200000, 0xbdb4d7b8,REGION_GFX4,0x200000, LOAD_NORMAL },

   {      "mp1u65", 0x00080000, 0x4868be1b,REGION_SMP1,0x00000,LOAD_NORMAL },

   {           NULL,          0,          0, },
};
   

static struct INPUT_INFO cave_68k_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x000000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x000002, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_TEST,         MSG_TEST,                0x000000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x000002, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x000001, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x000001, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x000001, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x000001, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x000001, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x000001, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x000001, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x000001, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x000003, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x000003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x000003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x000003, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x000003, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x000003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x000003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x000003, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct INPUT_INFO guwange_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x000003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x000003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_TEST,         MSG_TEST,                0x000003, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x000003, 0x08, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x000001, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x000001, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x000001, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x000001, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x000001, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x000001, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x000001, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x000001, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x000000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x000000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x000000, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x000000, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x000000, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x000000, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x000000, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x000000, 0x80, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

/* 8x8x4 tiles */
static struct GFX_LAYOUT layout_8x8x4 =
{
   8,8,
   RGN_FRAC(1,1),
   4,
   {0,1,2,3},
   {STEP8(0,4)},
   {STEP8(0,8*4)},
   8*8*4
};

/* 8x8x8 tiles */
static struct GFX_LAYOUT layout_8x8x8 =
{
   8,8,
   RGN_FRAC(1,1),
   8,
   {8,9,10,11, 0,1,2,3},
   {0*4,1*4,4*4,5*4,8*4,9*4,12*4,13*4},
   {0*64,1*64,2*64,3*64,4*64,5*64,6*64,7*64},
   8*8*8
};


/* 16x16x4 tiles */
static struct GFX_LAYOUT layout_16x16x4 =
{
   16,16,
   RGN_FRAC(1,1),
   4,
   {0,1,2,3},
   {0*4,1*4,2*4,3*4,4*4,5*4,6*4,7*4,
    64*4,65*4,66*4,67*4,68*4,69*4,70*4,71*4},
   {0*32,1*32,2*32,3*32,4*32,5*32,6*32,7*32,
    16*32,17*32,18*32,19*32,20*32,21*32,22*32,23*32},
   16*16*4
};

/* 16x16x8 tiles */
static struct GFX_LAYOUT layout_16x16x8 =
{
   16,16,
   RGN_FRAC(1,1),
   8,
   {8,9,10,11, 0,1,2,3},
   {0*4,1*4,4*4,5*4,8*4,9*4,12*4,13*4,
    128*4,129*4,132*4,133*4,136*4,137*4,140*4,141*4},
   {0*64,1*64,2*64,3*64,4*64,5*64,6*64,7*64,
    16*64,17*64,18*64,19*64,20*64,21*64,22*64,23*64},
   16*16*8
};

/* 16x16x4 objects */
static struct GFX_LAYOUT cave_gfx_object =
{
   16,16,
   RGN_FRAC(1,1),
   4,
   {0,1,2,3},
   {  1*4,  0*4,  3*4,  2*4,  5*4,  4*4,  7*4,  6*4,
      9*4,  8*4, 11*4, 10*4, 13*4, 12*4, 15*4, 14*4 },
   {STEP16(0,16*4)},
   16*16*4
};

/* 16x16x4 objects */
static struct GFX_LAYOUT ddonpach_gfx_object =
{
   16,16,
   RGN_FRAC(1,1),
   4,
   {0,1,2,3},
   {STEP16(0,4)},
   {STEP16(0,16*4)},
   16*16*4
};

static struct GFX_LIST cave_gfx[] = // feveron only ???
{
   { REGION_GFX1, &layout_16x16x4,  },
   { REGION_GFX2, &layout_16x16x4,  },
   { REGION_GFX3, &layout_16x16x4,  },
   { REGION_GFX4, &cave_gfx_object, },
   { 0,           NULL,             },
};

static struct GFX_LIST donpachi_gfx[] =
{
   { REGION_GFX1, &layout_16x16x4,  },
   { REGION_GFX2, &layout_16x16x4,  },
   { REGION_GFX3, &layout_8x8x4,  },
   { REGION_GFX4, &ddonpach_gfx_object, },
   { 0,           NULL,             },
};

static struct GFX_LIST esprade_gfx[] =
{
   { REGION_GFX1, NULL,  },
   { REGION_GFX2, NULL,  },
   { REGION_GFX3, NULL,  },
   { REGION_GFX4, NULL, },
   { 0,           NULL,             },
};

static struct GFX_LIST ddonpach_gfx[] =
{
   { REGION_GFX1, &layout_16x16x4,      },
   { REGION_GFX2, &layout_16x16x4,      },
   { REGION_GFX3, &layout_8x8x8,        },
   { REGION_GFX4, &ddonpach_gfx_object, },
   { 0,           NULL,                 },
};

static struct GFX_LIST uopoko_gfx[] =
{
   { REGION_GFX1, &layout_16x16x8,  },
   { REGION_GFX2, &layout_16x16x8,  },
   { REGION_GFX3, &layout_16x16x8,  },
   { REGION_GFX4, &cave_gfx_object, },
   { 0,           NULL,             },
};

static struct VIDEO_INFO cave_68k_video_r270 =
{
   draw_cave_68k,
   320,
   240,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE | VIDEO_NEEDS_16BPP,
   cave_gfx,
};

static struct VIDEO_INFO esprade_video_r270 =
{
   draw_cave_68k,
   320,
   240,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE | VIDEO_NEEDS_16BPP,
   esprade_gfx,
};

static struct VIDEO_INFO donpachi_video_r270 =
{
   draw_cave_68k,
   320,
   240,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE | VIDEO_NEEDS_16BPP,
   donpachi_gfx,
};

static struct VIDEO_INFO hotdogst_video_r270 =
{
   draw_cave_68k,
   384,
   240,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE | VIDEO_NEEDS_16BPP,
   cave_gfx,
};

static struct VIDEO_INFO ddonpach_video =
{
   draw_cave_68k,
   320,
   240,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE | VIDEO_NEEDS_16BPP,
   ddonpach_gfx,
};

static struct VIDEO_INFO uopoko_video =
{
   draw_cave_68k,
   320,
   240,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE | VIDEO_NEEDS_16BPP,
   uopoko_gfx,
};

struct GAME_MAIN game_esprade =
{
   esprade_dirs,
   esprade_roms,
   cave_68k_inputs,
   NULL,
   NULL,

   load_esprade,
   clear_cave_68k,
   &esprade_video_r270,
   execute_cave_68k_frame,
   "esprade",
   "Esprade",
   "CV-04",
   COMPANY_ID_CAVE,
   NULL,
   1998,
   cave_sound,
   GAME_SHOOT,
};

struct GAME_MAIN game_guwange =
{
   guwange_dirs,
   guwange_roms,
   guwange_inputs,
   guwange_dsw,
   NULL,

   load_guwange,
   clear_cave_68k,
   &esprade_video_r270,
   execute_cave_68k_frame,
   "guwange",
   "Guwange",
   "CV-04",
   COMPANY_ID_CAVE,
   NULL,
   1999,
   cave_sound,
   GAME_SHOOT,
};

struct GAME_MAIN game_ddonpach =
{
   ddonpach_dirs,
   ddonpach_roms,
   cave_68k_inputs,
   NULL,
   NULL,

   load_ddonpach,
   clear_cave_68k,
   &ddonpach_video,
   execute_cave_68k_frame,
   "ddonpach",
   "Dodonpachi (Japan)",
   "CV-04",
   COMPANY_ID_CAVE,
   NULL,
   1997,
   cave_sound,
   GAME_SHOOT,
};

struct GAME_MAIN game_donpachi =
{
   donpachi_dirs,
   donpachi_roms,
   cave_68k_inputs,
   NULL,
   NULL,

   load_donpachi,
   clear_cave_68k,
   &donpachi_video_r270,
   execute_cave_68k_frame,
   "donpachi",
   "DonPachi (Japan)",
   "CV-04",
   COMPANY_ID_CAVE,
   NULL,
   1995,
   donpachi_sound,
   GAME_SHOOT,
};

static struct DIR_INFO uo_poko_dirs[] =
{
   { "uo_poko", },
   { "uopoko", },
   { NULL, },
};

static struct ROM_INFO uo_poko_roms[] =
{
   {     "u26j.bin", 0x00080000, 0xe7eec050, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {     "u25j.bin", 0x00080000, 0x68cb6211, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {      "u49.bin", 0x00400000, 0x12fb11bb, REGION_GFX1, 0x000000, LOAD_NORMAL, },
   {      "u33.bin", 0x00400000, 0x5d142ad2, REGION_GFX4, 0x000000, LOAD_NORMAL, },
   {       "u4.bin", 0x00200000, 0xa2d0d755, REGION_SMP1, 0x000000, LOAD_NORMAL, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_uo_poko =
{
   uo_poko_dirs,
   uo_poko_roms,
   cave_68k_inputs,
   NULL,
   NULL,

   load_uo_poko,
   clear_cave_68k,
   &uopoko_video,
   execute_cave_68k_frame,
   "uopoko",
   "Uo Poko",
   NULL,
   COMPANY_ID_CAVE,
   "CV-02",
   1998,
   cave_sound,
   GAME_PUZZLE,
};

static struct DIR_INFO feveron_dirs[] =
{
   { "feveron", },
   { "dfeveron", },
   { "dangun_feveron", },
   { NULL, },
};

static struct ROM_INFO feveron_roms[] =
{
   {      "u34.bin", 0x00080000, 0xbe87f19d, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {      "u33.bin", 0x00080000, 0xe53a7db3, REGION_ROM1, 0x000001, LOAD_8_16,   },

   {      "u50.bin", 0x00200000, 0x7a344417, REGION_GFX1, 0x000000, LOAD_NORMAL, },

   {      "u49.bin", 0x00200000, 0xd21cdda7, REGION_GFX2, 0x000000, LOAD_NORMAL, },

   {      "u25.bin", 0x00400000, 0xa6f6a95d, REGION_GFX4, 0x000000, LOAD_NORMAL, },
   {      "u26.bin", 0x00400000, 0x32edb62a, REGION_GFX4, 0x400000, LOAD_NORMAL, },
   {      "u19.bin", 0x00400000, 0x5f5514da, REGION_SMP1,        0, LOAD_NORMAL, },
   {           NULL,          0,          0,           0,        0,           0, },
};

struct GAME_MAIN game_feveron =
{
   feveron_dirs,
   feveron_roms,
   cave_68k_inputs,
   NULL,
   NULL,

   load_feveron,
   clear_cave_68k,
   &cave_68k_video_r270,
   execute_cave_68k_frame,
   "dfeveron",
   "Dangun Feveron",
   NULL,
   COMPANY_ID_CAVE,
   "CV-05",
   1998,
   cave_sound,
   GAME_SHOOT,
};

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG1;
static UINT8 *GFX_BG2;

static UINT8 *GFX_OBJ_A;
static UINT8 *GFX_OBJ_B;
static UINT8 *GFX_OBJ_SIZE;

static UINT8 *GFX_BG0_SOLID;
static UINT8 *GFX_BG1_SOLID;
static UINT8 *GFX_BG2_SOLID;

static UINT8 *GFX_OBJ_SOLID;

static UINT8 *RAM_BG0;
static UINT8 *RAM_BG1;
static UINT8 *RAM_BG2;

static UINT8 *RAM_BG0_CTRL;
static UINT8 *RAM_BG1_CTRL;
static UINT8 *RAM_BG2_CTRL;

static UINT8 *RAM_CTRL;

static UINT32 gfx_colours;

static struct EEPROM_interface eeprom_interface =
{
   6,            /* address bits */
   16,            /* data bits */
   "0110",         /* read command */
   "0101",         /* write command */
   0,            /* erase command */
   0,            /* lock command */
   "0100110000"    /* unlock command */
};

static struct EEPROM_interface hotdogst_eeprom_interface =
{
	6,				// address bits
	16,				// data bits
	"0110",			// read command
	"0101",			// write command
	0,				// erase command
	0,				// lock command
	"10100110000" 	// unlock command,
};

//WRITE_HANDLER( cave_eeprom_w )
static void cave_eeprom_w(UINT32 offset, UINT16 data)
{
	(void)(offset);
   if ( (data & 0xFF000000) == 0 )  /* even address */
   {
      /* latch the bit */
     //     print_ingame(60,"%d\n",data); 
     EEPROM_write_bit(data & 0x0800);

     /* reset line asserted: reset. */
     EEPROM_set_cs_line((data & 0x0200) ? CLEAR_LINE : ASSERT_LINE );
     
     /* clock line asserted: write latch or select next bit to read */
     EEPROM_set_clock_line((data & 0x0400) ? ASSERT_LINE : CLEAR_LINE );

   }
}

//WRITE16_HANDLER( guwange_eeprom_w )
static void guwange_eeprom_w(UINT32 offset, UINT16 data)
{
	(void)(offset);
  //  print_ingame(60,"gu %d %x\n",offset,data);
  if ( (data & 0x00FF0000) == 0 )  /* odd address */
   {
     //     print_ingame(60,"eepr ok\n");
     // latch the bit

     EEPROM_write_bit(data & 0x80);

      // reset line asserted: reset.
     EEPROM_set_cs_line((data & 0x20) ? CLEAR_LINE : ASSERT_LINE );

     // clock line asserted: write latch or select next bit to read
     EEPROM_set_clock_line((data & 0x40) ? ASSERT_LINE : CLEAR_LINE );
   }
}

/* Handles writes to the YMZ280B */
static WRITE16_HANDLER( cave_sound_w )
{
  if (offset & 2)
    YMZ280B_data_0_w(offset, data & 0xff);
  else
    YMZ280B_register_0_w(offset, data & 0xff);
  //   }
}

/* Handles reads from the YMZ280B */
static READ16_HANDLER( cave_sound_r )
{
  int r =YMZ280B_status_0_r(offset);
  //fprintf(stderr,"cave_sound_r %x -> %x\n",offset,r);
  return r;
}

static UINT8 esp_input_rb(UINT32 offset)
{
  short val;
  offset &= 3;
  switch(offset){
  case 0x00:
    val = (input_buffer[0] & 0x03) | 0xFC;
    //   print_ingame(60,"inp0 : %x\n",val);
    break;
  case 0x01:
    val = input_buffer[1];
    break;
  case 0x02:
    val = (input_buffer[2] & 0x03) | 0xF4 |
      ( (EEPROM_read_bit() & 0x01) << 3 );
    break;
  case 0x03:
    val = input_buffer[3];
    break;
  default:
    val = 0;
    break;
  }
  return val;
}

//AD16_HANDLER( guwange_inputs_r )
static UINT8 guwange_input_rb(UINT32 offset)
{
  short val;
  offset &= 3;
  switch(offset){
  case 0x00:
    val = (input_buffer[0] & 0x03) | 0xFC;
    break;
  case 0x01:
    val = input_buffer[1];
    break;
  case 0x02:
    val = input_buffer[2];
    //     (eeprom_93c46_rb_cave() << 3);
    //   val = input_buffer[2] | ((eeprom_93c46_rb_cave() & 0x1) << 3);
    break;
      case 0x03:
   val = (input_buffer[3] & 0x7f) | ( (EEPROM_read_bit() & 0x01) << 7 );
   break;
  default:
    val = 0;
    break;
  }
  return val;
}

static UINT16 esp_input_rw(UINT32 offset)
{
   UINT16 val= (esp_input_rb(offset+0) << 8) |
          (esp_input_rb(offset+1) << 0);
   //   if (offset == 0) {
   //     print_ingame(60,"offs %x -> %x\n",offset,val);
     //   }
   return val;
}

static UINT16 guwange_input_rw(UINT32 offset)
{
   UINT16 val= (guwange_input_rb(offset+0) << 8) |
          (guwange_input_rb(offset+1) << 0);
   //   if (offset == 0) {
   //     print_ingame(60,"%x\n",val);
   //   }
   return val;
}

static void esp_input_wb(UINT32 offset, UINT8 data)
{
#ifdef RAINE_DEBUG
      print_debug("esp_input_wb(%02x,%02x)\n", offset & 3, data);
#endif
}

static void esp_input_ww(UINT32 offset, UINT16 data)
{
   esp_input_wb(offset+0, data >> 8);
   esp_input_wb(offset+1, data >> 0);
}

static UINT8 ddonpach_irq_cause_r(UINT32 offset)
{

    int result = 0x0007;

    if (vblank_irq)      result ^= 0x01; 
/*    if (unknown_irq)   result ^= 0x02; */

    if ((offset&0xe) == 0)   vblank_irq = 0; 
/*    if (offset == 6)   unknown_irq = 0; */
   update_irq_state(); 

    return result; 
  
}

static UINT16 cave_irq_cause_r(UINT32 offset)
{

    int result = 0x0003;
    offset &= 0xf;
    if (vblank_irq)      result ^= 0x01; 

    if (offset == 4 || offset==5)   vblank_irq = 0; 
    update_irq_state(); 

    return result; 
  
}

static int layer_id_data[4];

static char *layer_id_name[4] =
{
   "BG0", "BG1", "BG2", "OBJECT",
};

void load_ddonpach(void)
{
   romset = 1;
   scrolldx_flip = ddonpach_scrolldx_flip;
   scrolldx = ddonpach_scrolldx;
  raster_bg = 0;
   
   sprite_size = 0x10000;

   GFX_BG0   = load_region[REGION_GFX1];
   GFX_BG1   = load_region[REGION_GFX2];
   GFX_BG2   = load_region[REGION_GFX3];
   GFX_OBJ_A = load_region[REGION_GFX4];

   gfx_bg0_count = 0x04000;
   gfx_bg1_count = 0x04000;
   gfx_bg2_count = 0x0200000/0x40;
   gfx_obj_count = 0x10000;

   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(gfx_obj_count))) return;
   memset(GFX_OBJ_SIZE,0x00,gfx_obj_count);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, gfx_bg0_count);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, gfx_bg1_count);
   GFX_BG2_SOLID = make_solid_mask_8x8(GFX_BG2, gfx_bg2_count);
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, gfx_obj_count);

   gfx_colours = 256;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   RAM_BG0 = RAM + 0x020000;
   RAM_BG1 = RAM + 0x028000;
   RAM_BG2 = RAM + 0x030000;

   RAM_BG0_CTRL = RAM + 0x040000;
   RAM_BG1_CTRL = RAM + 0x040006;
   RAM_BG2_CTRL = RAM + 0x04000c;

   RAM_CTRL = RAM + 0x50100;

/*
   WriteWord68k(&ROM[0x02D4A],0x4E71);
*/
   InitPaletteMap(RAM+0x40012, 0x80, 0x100, 0x8000);

   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = add_layer_info(layer_id_name[2]);
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

/*
 *  StarScream Stuff follows
 */

   // 68000 speed hack

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x050012);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadBW(0x000000, 0x0FFFFF, NULL, ROM+0x000000);   // 68000 ROM
   AddRWBW(0x100000, 0x10FFFF, NULL, RAM+0x000000);   // 68000 RAM

   AddReadWord(0x300002,0x300003,cave_sound_r,NULL);
   AddWriteWord(0x300000, 0x300003, cave_sound_w,NULL);

   AddRWBW(0x400000, 0x40FFFF, NULL, RAM+0x010000);   // OBJECT RAM
   AddRWBW(0x500000, 0x507FFF, NULL, RAM+0x020000);   // BG0 RAM
   AddRWBW(0x600000, 0x607FFF, NULL, RAM+0x028000);   // BG1 RAM
   AddRWBW(0x700000, 0x703FFF, NULL, RAM+0x030000);   // BG2 RAM
   AddRWBW(0x704000, 0x707FFF, NULL, RAM+0x030000);   // BG2 RAM [mirror]
   AddRWBW(0x708000, 0x70BFFF, NULL, RAM+0x030000);   // BG2 RAM [mirror]
   AddRWBW(0x70C000, 0x70FFFF, NULL, RAM+0x030000);   // BG2 RAM [mirror]

   AddReadWord(0x800000, 0x800007, ddonpach_irq_cause_r, NULL);// ?
   //AddWriteByte(0x800000, 0x80007F, esp_800000_wb, NULL);// ?
   //AddWriteWord(0x800000, 0x80007F, esp_800000_ww, NULL);// ?
   AddWriteBW(0x800000, 0x80007F, NULL, RAM_CTRL+0);

   AddRWBW(0x900000, 0x900005, NULL, RAM+0x40000);      // bg0 ctrl
   AddRWBW(0xa00000, 0xa00005, NULL, RAM+0x40006);      // bg1 ctrl

   AddRWBW(0xb00000, 0xb00005, NULL, RAM+0x4000c);      // bg2 ctrl
   AddRWBW(0xC00000, 0xC0FFFF, NULL, RAM+0x40012);   // COLOR RAM

   AddReadByte(0xD00000, 0xD00003, esp_input_rb, NULL);   // INPUT
   AddReadWord(0xD00000, 0xD00003, esp_input_rw, NULL);   // INPUT

   AddReadBW(0xe00000, 0xe00001, NULL, RAM+0x50012);   // INPUT
   AddWriteWord(0xE00000, 0xE00001, cave_eeprom_w, NULL);// EEPROM

   setup_cave_game();

}   

static WRITE16_HANDLER( nmk_oki6295_bankswitch_w )
{
  /* The OKI6295 ROM space is divided in four banks, each one indepentently
     controlled. The sample table at the beginning of the addressing space is
     divided in four pages as well, banked together with the sample data. */
  
#define TABLESIZE 0x100
#define BANKSIZE 0x10000
  
  int chip,banknum,size,bankaddr;
  unsigned char *rom;
  offset -= 0xb00020;
  offset >>=1;
  
  chip	=	offset / 4;
  banknum	=offset % 4;
  
  rom = load_region[REGION_SMP1 + chip];
  size	= get_region_size(REGION_SMP1 + chip) - 0x40000;
  
  bankaddr =	(data * BANKSIZE) % size;	// % used: size is not a power of 2
  
  /* copy the samples */
  memcpy(rom + banknum * BANKSIZE,rom + 0x40000 + bankaddr,BANKSIZE);
      
  /* and also copy the samples address table (only for chip #1) */
  if (chip==1)
    {
      rom += banknum * TABLESIZE;
      memcpy(rom,rom + 0x40000 + bankaddr,TABLESIZE);
    }
}

void load_donpachi(void)
{
  romset =1; // like ddonpach
   
  sprite_size = 0x8000;

  scrolldx_flip = donpachi_scrolldx_flip;
  scrolldx = donpachi_scrolldx;
  raster_bg = 0;

  GFX_BG0   = load_region[REGION_GFX1];
  GFX_BG1   = load_region[REGION_GFX2];
  GFX_BG2   = load_region[REGION_GFX3];
  GFX_OBJ_A = load_region[REGION_GFX4];
  
  gfx_bg0_count = 0x02000; // 16x16x4
  gfx_bg1_count = 0x02000;
  gfx_bg2_count = 0x02000; // 8x8x4
  gfx_obj_count = 0x08000;

   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(gfx_obj_count))) return;
   memset(GFX_OBJ_SIZE,0x00,gfx_obj_count);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, gfx_bg0_count);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, gfx_bg1_count);
   GFX_BG2_SOLID = make_solid_mask_8x8(GFX_BG2, gfx_bg2_count);
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, gfx_obj_count);

   gfx_colours = 16;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   RAM_BG0 = RAM + 0x028000;
   RAM_BG1 = RAM + 0x020000;
   RAM_BG2 = RAM + 0x030000;

   RAM_BG1_CTRL = RAM + 0x038000;
   RAM_BG0_CTRL = RAM + 0x038006;
   RAM_BG2_CTRL = RAM + 0x03800c;

   RAM_CTRL = RAM + 0x38020;

/*
   WriteWord68k(&ROM[0x02D4A],0x4E71);
*/
   InitPaletteMap(RAM+0x38100, 0x80, 0x10, 0x8000);

   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = add_layer_info(layer_id_name[2]);
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

/*
 *  StarScream Stuff follows
 */

   // 68000 speed hack

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x050012);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadBW(0x000000, 0x07FFFF, NULL, ROM+0x000000);   // 68000 ROM
   AddRWBW(0x100000, 0x10FFFF, NULL, RAM+0x000000);   // 68000 RAM
   AddRWBW(0x200000, 0x207FFF, NULL, RAM+0x020000);   // BG1
   AddRWBW(0x300000, 0x307FFF, NULL, RAM+0x028000);   // BG0
   AddRWBW(0x400000, 0x403FFF, NULL, RAM+0x030000);   // BG2
   AddRWBW(0x404000, 0x407FFF, NULL, RAM+0x030000);   // BG2 (mirror)
   AddRWBW(0x500000, 0x50fFFF, NULL, RAM+0x010000);   // Sprites

   AddRWBW(0x600000, 0x600005, NULL, RAM+0x38000);      // bg1 ctrl
   AddRWBW(0x700000, 0x700005, NULL, RAM+0x38006);      // bg0 ctrl
   AddRWBW(0x800000, 0x800005, NULL, RAM+0x3800c);      // bg2 ctrl
   AddRWBW(0x900000, 0x90007f, NULL, RAM+0x38020);      // sprites ctrl

   AddRWBW(0xa08000, 0xa08FFF, NULL, RAM+0x38100);   // COLOR RAM

   AddReadWord(0xb00000, 0xb00001, OKIM6295_status_0_r, NULL);
   AddReadWord(0xb00010, 0xb00011, OKIM6295_status_1_r, NULL);
   AddWriteWord(0xb00000, 0xb00001, OKIM6295_data_0_w, NULL);
   AddWriteWord(0xb00010, 0xb00011, OKIM6295_data_1_w, NULL);
   AddWriteWord(0xb00020, 0xb0002f, nmk_oki6295_bankswitch_w, NULL);

   AddReadByte(0xc00000, 0xc00003, esp_input_rb, NULL);   // INPUT
   AddReadWord(0xc00000, 0xc00003, esp_input_rw, NULL);   // INPUT

   AddWriteWord(0xd00000, 0xd00001, cave_eeprom_w, NULL);// EEPROM

   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);   // Trap Idle 68000

   setup_cave_game();
}   

static UINT8 soundlatch_lo_r(UINT32 offset) {
  return latch & 0xff;
}

static UINT8 soundlatch_hi_r(UINT32 offset) {
  return latch >> 8;
}

static WRITE16_HANDLER( hotdogst_soundcmd_w )
{
  latch = data;
  cpu_int_nmi(CPU_Z80_0);
}

static WRITE_HANDLER( hotdogst_soundcmd_wb )
{
  // Debugging only (not called)
  if (offset & 1)
    latch = (latch & 0xff00) | data;
  else
    latch = (latch & 0xff) | (data << 8);
  cpu_int_nmi(CPU_Z80_0);
}

void load_hotdogst(void)
{
  UINT8 *Z80RAM;
  
  romset =4; // sprites placement, and layer activation bit...
  sprite_size = 0x8000;

  scrolldx_flip = hotdogst_scrolldx_flip;
  scrolldx = esprade_scrolldx;
  raster_bg = 0;

  GFX_BG0   = load_region[REGION_GFX1];
  GFX_BG1   = load_region[REGION_GFX2];
  GFX_BG2   = load_region[REGION_GFX3];
  GFX_OBJ_A = load_region[REGION_GFX4];

  gfx_bg0_count = 0x01000; // 16x16x4
  gfx_bg1_count = 0x01000;
  gfx_bg2_count = 0x01000; // 8x8x4
  gfx_obj_count = 0x08000;

   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(gfx_obj_count))) return;
   memset(GFX_OBJ_SIZE,0x00,gfx_obj_count);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, gfx_bg0_count);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, gfx_bg1_count);
   GFX_BG2_SOLID = make_solid_mask_16x16(GFX_BG2, gfx_bg2_count);
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, gfx_obj_count);

   gfx_colours = 16;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   RAM_BG0 = RAM + 0x028000;
   RAM_BG1 = RAM + 0x020000;
   RAM_BG2 = RAM + 0x030000;

   RAM_BG1_CTRL = RAM + 0x038000;
   RAM_BG0_CTRL = RAM + 0x038006;
   RAM_BG2_CTRL = RAM + 0x03800c;

   RAM_CTRL = RAM + 0x38020;

/*
   WriteWord68k(&ROM[0x02D4A],0x4E71);
*/
   InitPaletteMap(RAM+0x38100, 0x80, 0x10, 0x8000);
   init_16x16_zoom_64(); // Should have zoom...

   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&hotdogst_eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = add_layer_info(layer_id_name[2]);
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

   Z80ROM=load_region[REGION_ROM2];
   Z80RAM=RAM+0x50000; // Too much ram for sure !
   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066); // ???!

   AddZ80AReadByte(0x0000, 0x7fFF, NULL,  Z80ROM+0x0000); // Z80 ROM
   AddZ80AReadByte(0xe000, 0xFFFF, NULL,  Z80RAM+0X0000); // RAM
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,       NULL);
   AddZ80AReadByte(    -1,     -1, NULL,                NULL);

   AddZ80AWriteByte(0xe000, 0xffFF, NULL, Z80RAM+0x0000); // Z80 RAM
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,     NULL);
   AddZ80AWriteByte(    -1,     -1, NULL,               NULL);

   AddZ80AReadPort(0x30, 0x30, soundlatch_lo_r,  NULL);
   AddZ80AReadPort(0x40, 0x40, soundlatch_hi_r,  NULL);
   AddZ80AReadPort(0x50, 0x50, YM2203_status_port_0_r, NULL);
   AddZ80AReadPort(0x51, 0x51, YM2203_read_port_0_r, NULL);
   AddZ80AReadPort(0x60, 0x60, OKIM6295_status_0_r, NULL);
   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,           NULL);
   AddZ80AReadPort(  -1,   -1, NULL,                    NULL);

   AddZ80AWritePort(0x50, 0x50, YM2203_control_port_0_w, NULL);
   AddZ80AWritePort(0x51, 0x51, YM2203_write_port_0_w, NULL);
   AddZ80AWritePort(0x60, 0x60, OKIM6295_data_0_w, NULL);
   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,         NULL);
   AddZ80AWritePort(  -1,   -1, NULL,                   NULL);

   AddZ80AInit();

/*
 *  StarScream Stuff follows
 */
   
   // 68000 speed hack

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x040000);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadBW(0x000000, 0x0fFFFF, NULL, ROM+0x000000);   // 68000 ROM
   AddRWBW(0x300000, 0x30FFFF, NULL, RAM+0x000000);   // 68000 RAM
   AddRWBW(0x880000, 0x887FFF, NULL, RAM_BG0);   // BG0
   AddRWBW(0x900000, 0x907FFF, NULL, RAM_BG1);   // BG1
   AddRWBW(0x980000, 0x987FFF, NULL, RAM_BG2);   // BG2

   AddWriteWord(0xa8006e, 0xa8006f, hotdogst_soundcmd_w, NULL);
   AddWriteByte(0xa8006e, 0xa8006f, hotdogst_soundcmd_wb, NULL);

   AddReadWord(0xa80000, 0xa80007, cave_irq_cause_r, NULL);
   AddWriteBW( 0xa80000, 0xa8007f, NULL, RAM_CTRL+0);
   AddRWBW(0xf00000, 0xf0fFFF, NULL, RAM+0x010000);   // Sprites

   AddRWBW(0xb00000, 0xb00005, NULL, RAM_BG0_CTRL);      // bg0 ctrl
   AddRWBW(0xb80000, 0xb80005, NULL, RAM_BG1_CTRL);      // bg1 ctrl
   AddRWBW(0xc00000, 0xc00005, NULL, RAM_BG2_CTRL);      // bg2 ctrl

   AddRWBW(0x408000, 0x408FFF, NULL, RAM+0x38100);   // COLOR RAM

   AddReadByte(0xc80000, 0xc80003, esp_input_rb, NULL);   // INPUT
   AddReadWord(0xc80000, 0xc80003, esp_input_rw, NULL);   // INPUT

   AddWriteWord(0xd00000, 0xd00001, cave_eeprom_w, NULL);// EEPROM
   AddWriteWord(0xd00002, 0xd00003, NULL, RAM+0x380a0); // ???
   
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);   // Trap Idle 68000

   setup_cave_game();
}   

void load_esprade(void)
{
  romset = 0;
  scrolldx_flip = esprade_scrolldx_flip;
  scrolldx = esprade_scrolldx;
  raster_bg = 0;

   sprite_size = 0x10000;

   GFX_BG0   = load_region[REGION_GFX1];
   GFX_BG1   = load_region[REGION_GFX2];
   GFX_BG2   = load_region[REGION_GFX3];
   GFX_OBJ_A = load_region[REGION_GFX4];

   if (!GFX_BG0 || !GFX_BG1 || !GFX_BG2 || !GFX_OBJ_A)
     return;
   
   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(sprite_size))) return;
   memset(GFX_OBJ_SIZE,0x00,sprite_size);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x0800000/0x100);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, 0x0800000/0x100);
   GFX_BG2_SOLID = make_solid_mask_16x16(GFX_BG2, 0x0400000/0x100);
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, sprite_size);

   gfx_colours = 256;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;


   RAM_BG0 = RAM + 0x020000;
   RAM_BG1 = RAM + 0x028000;
   RAM_BG2 = RAM + 0x030000;

   RAM_BG0_CTRL = RAM + 0x048080;
   RAM_BG1_CTRL = RAM + 0x048088;
   RAM_BG2_CTRL = RAM + 0x048090;

   RAM_CTRL = RAM + 0x48100;

   // 68000 speed hack

   WriteLong68k(&ROM[0x4f156],0x13FC0000);
   WriteLong68k(&ROM[0x4f15a],0x00AA0000);

   InitPaletteMap(RAM+0x38000, 0x80, 0x100, 0x8000);
   init_16x16_zoom_64();

   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = add_layer_info(layer_id_name[2]);
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x048000);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadByte(0x000000, 0x0FFFFF, NULL, ROM+0x000000);         // 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);         // 68000 RAM
   AddReadWord(0x300002,0x300003,cave_sound_r,NULL);
   AddWriteWord(0x300000, 0x300003, cave_sound_w,NULL);

   AddReadByte(0x400000, 0x40FFFF, NULL, RAM+0x010000);         // OBJECT RAM
   AddReadByte(0x500000, 0x507FFF, NULL, RAM+0x020000);         // BG0 RAM
   AddReadByte(0x600000, 0x607FFF, NULL, RAM+0x028000);         // BG1 RAM
   AddReadByte(0x700000, 0x707FFF, NULL, RAM+0x030000);         // BG2 RAM
   AddReadBW(0x800000, 0x800007, cave_irq_cause_r, NULL);      // ?
   AddReadByte(0xC00000, 0xC0FFFF, NULL, RAM+0x038000);         // COLOR RAM
   AddReadByte(0xD00000, 0xD00003, esp_input_rb, NULL);         // INPUT

   AddReadWord(0x000000, 0x0FFFFF, NULL, ROM+0x000000);         // 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);         // 68000 RAM
   AddReadWord(0x400000, 0x40FFFF, NULL, RAM+0x010000);         // OBJECT RAM
   AddReadWord(0x500000, 0x507FFF, NULL, RAM+0x020000);         // BG0 RAM
   AddReadWord(0x600000, 0x607FFF, NULL, RAM+0x028000);         // BG1 RAM
   AddReadWord(0x700000, 0x707FFF, NULL, RAM+0x030000);         // BG2 RAM
   AddReadWord(0x900000, 0x900007, NULL, RAM+0x048080);      // BG0 CTRL RAM
   AddReadWord(0xA00000, 0xA00007, NULL, RAM+0x048088);      // BG1 CTRL RAM
   AddReadWord(0xB00000, 0xB00007, NULL, RAM+0x048090);      // BG2 CTRL RAM
   AddReadWord(0xC00000, 0xC0FFFF, NULL, RAM+0x038000);         // COLOR RAM
   AddReadWord(0xD00000, 0xD00003, esp_input_rw, NULL);         // INPUT

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);      // 68000 RAM
   AddWriteByte(0x400000, 0x40FFFF, NULL, RAM+0x010000);      // OBJECT RAM
   AddWriteByte(0x500000, 0x507FFF, NULL, RAM+0x020000);      // BG0 RAM
   AddWriteByte(0x600000, 0x607FFF, NULL, RAM+0x028000);      // BG1 RAM
   AddWriteByte(0x700000, 0x707FFF, NULL, RAM+0x030000);      // BG2 RAM
   //AddWriteByte(0x800000, 0x80007F, esp_800000_wb, NULL);      // ?
   AddWriteByte(0xC00000, 0xC0FFFF, NULL, RAM+0x038000);      // COLOR RAM
   AddWriteByte(0xD00000, 0xD00003, esp_input_wb, NULL);      // INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);         // Trap Idle 68000
   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);      // 68000 RAM
   AddWriteWord(0x400000, 0x40FFFF, NULL, RAM+0x010000);      // OBJECT RAM
   AddWriteWord(0x500000, 0x507FFF, NULL, RAM+0x020000);      // BG0 RAM
   AddWriteWord(0x600000, 0x607FFF, NULL, RAM+0x028000);      // BG1 RAM
   AddWriteWord(0x700000, 0x707FFF, NULL, RAM+0x030000);      // BG2 RAM
   AddWriteWord(0xC00000, 0xC0FFFF, NULL, RAM+0x038000);      // COLOR RAM
   AddWriteWord(0xD00000, 0xD00003, esp_input_ww, NULL);      // INPUT
   AddWriteWord(0xE00000, 0xE00001, cave_eeprom_w, NULL);      // EEPROM
   //AddWriteWord(0x800000, 0x80007F, esp_800000_ww, NULL);      // ?
   AddWriteBW(0x800000, 0x80007F, NULL, RAM_CTRL+0);
   AddWriteWord(0x900000, 0x900007, NULL, RAM+0x048080);      // BG0 CTRL RAM
   AddWriteWord(0xA00000, 0xA00007, NULL, RAM+0x048088);      // BG1 CTRL RAM
   AddWriteWord(0xB00000, 0xB00007, NULL, RAM+0x048090);      // BG2 CTRL RAM
   setup_cave_game();
}

void load_guwange(void)
{
   // Guwange has exactly the same gfx handling as esprade...
   romset = 0;
  scrolldx_flip = guwange_scrolldx_flip;
  scrolldx = guwange_scrolldx;
  raster_bg = 0;

   sprite_size = 0x18000;

   GFX_BG0   = load_region[REGION_GFX1];
   GFX_BG1   = load_region[REGION_GFX2];
   GFX_BG2   = load_region[REGION_GFX3];
   GFX_OBJ_A = load_region[REGION_GFX4];

   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(sprite_size))) return;
   memset(GFX_OBJ_SIZE,0x00,sprite_size);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x0800000/0x100);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, 0x0400000/0x100);
   GFX_BG2_SOLID = make_solid_mask_16x16(GFX_BG2, 0x0400000/0x100);
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, sprite_size);

   gfx_colours = 256;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   RAM_BG0 = RAM + 0x020000;
   RAM_BG1 = RAM + 0x028000;
   RAM_BG2 = RAM + 0x030000;

   RAM_BG0_CTRL = RAM + 0x048080;
   RAM_BG1_CTRL = RAM + 0x048088;
   RAM_BG2_CTRL = RAM + 0x048090;

   RAM_CTRL = RAM + 0x48100;

   InitPaletteMap(RAM+0x38000, 0x80, 0x100, 0x8000);
   init_16x16_zoom_64();

   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = add_layer_info(layer_id_name[2]);
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x048500);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadBW(0x000000, 0x0FFFFF, NULL, ROM+0x000000);         // 68000 ROM
   AddRWBW(0x200000, 0x20FFFF, NULL, RAM+0x000000);         // 68000 RAM
   AddReadBW(0x300000,0x300007,cave_irq_cause_r,NULL);
   
   //AddWriteByte(0x300000, 0x30007f, esp_800000_wb,NULL);
   //AddWriteWord(0x300000, 0x30007f, esp_800000_ww,NULL);
   AddWriteBW(0x300000, 0x30007F, NULL, RAM_CTRL+0);

   AddRWBW(0x400000, 0x40FFFF, NULL, RAM+0x010000);         // OBJECT RAM
   AddRWBW(0x500000, 0x507FFF, NULL, RAM+0x020000);         // BG0 RAM
   AddRWBW(0x600000, 0x607FFF, NULL, RAM+0x028000);         // BG1 RAM
   AddRWBW(0x700000, 0x707FFF, NULL, RAM+0x030000);         // BG2 RAM
   AddReadBW(0x800002, 0x800003, cave_sound_r, NULL);      // ?
   AddWriteWord(0x800000, 0x800003, cave_sound_w,NULL);
   AddWriteByte(0x800000, 0x800003, cave_sound_w, NULL);      // ?
   AddRWBW(0x900000, 0x900007, NULL, RAM+0x048080);      // BG0 CTRL RAM
   AddRWBW(0xA00000, 0xA00007, NULL, RAM+0x048088);      // BG1 CTRL RAM

   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);         // Trap Idle 68000
   AddRWBW(0xB00000, 0xB00007, NULL, RAM+0x048090);      // BG2 CTRL RAM
   AddWriteWord(0xB00000, 0xB00007, NULL, RAM+0x048090);      // BG2 CTRL RAM
   AddRWBW(0xC00000, 0xC0FFFF, NULL, RAM+0x038000);         // COLOR RAM
   AddReadByte(0xD00010, 0xD00013, guwange_input_rb, NULL);         // INPUT
   AddReadWord(0xD00010, 0xD00013, guwange_input_rw, NULL);         // INPUT
   AddWriteWord(0xD00010, 0xD00011, guwange_eeprom_w, NULL);         // INPUT
   setup_cave_game();
}

void load_feveron(void)
{
   romset = 3;
   scrolldx_flip = feveron_scrolldx_flip;
   scrolldx = feveron_scrolldx;
   raster_bg = 0;

   sprite_size = 0x10000;

   GFX_BG0   = load_region[REGION_GFX1];
   GFX_BG1   = load_region[REGION_GFX2];
   GFX_BG2   = NULL;
   GFX_OBJ_A = load_region[REGION_GFX4];

   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(sprite_size))) return;
   memset(GFX_OBJ_SIZE,0x00,sprite_size);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x0400000/0x100);
   GFX_BG1_SOLID = make_solid_mask_16x16(GFX_BG1, 0x0400000/0x100);
   GFX_BG2_SOLID = NULL;
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, sprite_size);

   if(!(TileQueue_scroll = (struct TILE_QS *) AllocateMem(sizeof(TILE_QS)*MAX_TILES)))return;

   gfx_colours = 16;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   RAM_BG0 = RAM + 0x020000;
   RAM_BG1 = RAM + 0x028000;
   RAM_BG2 = NULL;

   RAM_BG0_CTRL = RAM + 0x048080;
   RAM_BG1_CTRL = RAM + 0x048088;
   RAM_BG2_CTRL = NULL;

   RAM_CTRL = RAM + 0x48100;

/*
   WriteWord68k(&ROM[0x02D4A],0x4E71);
*/

   InitPaletteMap(RAM+0x38000, 0x80, 0x10, 0x8000);
   init_16x16_zoom_64();
   
   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = 0;
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x048000);
   
   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadBW(0x000000, 0x0FFFFF, NULL, ROM+0x000000);         // 68000 ROM
   AddRWBW(0x100000, 0x10FFFF, NULL, RAM+0x000000);         // 68000 RAM
   AddReadBW(0x300002,0x300003,cave_sound_r,NULL);
   AddWriteBW(0x300000, 0x300003, cave_sound_w,NULL);

   AddRWBW(0x400000, 0x40FFFF, NULL, RAM+0x010000);         // OBJECT RAM
   AddRWBW(0x500000, 0x507FFF, NULL, RAM+0x020000);         // BG0 RAM
   AddRWBW(0x600000, 0x607FFF, NULL, RAM+0x028000);         // BG1 RAM
   AddRWBW(0x708000, 0x708FFF, NULL, RAM+0x038000);         // COLOR RAM
   //AddReadBW(0x708000, 0x708FFF, NULL, RAM+0x038000);         // COLOR RAM
   //AddWriteByte(0x708000, 0x708FFF, feveron_pal_wb, NULL);         // COLOR RAM
   //AddWriteWord(0x708000, 0x708FFF, feveron_pal_ww, NULL);         // COLOR RAM

   AddRWBW(0x710000, 0x710FFF, NULL, RAM+0x038000);         // COLOR RAM [MIRROR]

   AddReadWord(0x800000, 0x800007, cave_irq_cause_r, NULL);      // ?
   //AddWriteByte(0x800000, 0x80007f, esp_800000_wb, NULL);      // ?
   //AddWriteWord(0x800000, 0x80007f, esp_800000_ww, NULL);      // ?
   AddWriteBW(0x800000, 0x80007F, NULL, RAM_CTRL+0);
   
   AddWriteWord(0x900000, 0x900007, NULL, RAM+0x048080);      // BG0 CTRL RAM

   AddWriteWord(0xA00000, 0xA00007, NULL, RAM+0x048088);      // BG1 CTRL RAM

   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);         // Trap Idle 68000

   AddReadWord(0xB00000, 0xB00003, esp_input_rw, NULL);         // INPUT
   AddReadByte(0xB00000, 0xB00003, esp_input_rb, NULL);         // INPUT

   AddWriteWord(0xC00000, 0xC00001, cave_eeprom_w, NULL);      // EEPROM
   setup_cave_game();
}

void load_uo_poko(void)
{
   romset = 2;

   sprite_size = 0x08000;
   scrolldx = uopoko_scrolldx;
   scrolldx_flip = uopoko_scrolldx_flip;
  raster_bg = 1;

   GFX_BG0   = load_region[REGION_GFX1];
   GFX_BG1   = NULL;
   GFX_BG2   = NULL;
   GFX_OBJ_A = load_region[REGION_GFX4];

   if(!(GFX_OBJ_B   =AllocateMem(sprite_size<<8))) return;
   memcpy(GFX_OBJ_B,GFX_OBJ_A,sprite_size<<8);

   if(!(GFX_OBJ_SIZE=AllocateMem(sprite_size))) return;
   memset(GFX_OBJ_SIZE,0x00,sprite_size);

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x0400000/0x100);
   GFX_BG1_SOLID = NULL;
   GFX_BG2_SOLID = NULL;
   GFX_OBJ_SOLID = make_solid_mask_16x16(GFX_OBJ_B, sprite_size);

   if(!(TileQueue_scroll = (struct TILE_QS *) AllocateMem(sizeof(TILE_QS)*MAX_TILES)))return;

   gfx_colours = 256;

   RAMSize=0x80000;
   if(!(RAM=AllocateMem(RAMSize))) return;

   RAM_BG0 = RAM + 0x020000;
   RAM_BG1 = NULL;
   RAM_BG2 = NULL;

   RAM_BG0_CTRL = RAM + 0x048080;
   RAM_BG1_CTRL = NULL;
   RAM_BG2_CTRL = NULL;

   RAM_CTRL = RAM + 0x48100;

/*
   WriteWord68k(&ROM[0x02D4A],0x4E71);
*/
   InitPaletteMap(RAM+0x38000, 0x80, 0x100, 0x8000);
   init_16x16_zoom_64();

   set_colour_mapper(&col_map_xggg_ggrr_rrrb_bbbb);

   EEPROM_init(&eeprom_interface);
   load_eeprom();

   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = 0;
   layer_id_data[2] = 0;
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x100000);
   ByteSwap(RAM,0x048000);

   AddMemFetch(0x000000, 0x0FFFFF, ROM+0x000000-0x000000);      // 68000 ROM

   AddReadByte(0x000000, 0x0FFFFF, NULL, ROM+0x000000);         // 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);         // 68000 RAM
   AddReadBW(0x300002,0x300003,cave_sound_r,NULL);
   AddWriteBW(0x300000, 0x300003, cave_sound_w,NULL);

   AddReadByte(0x400000, 0x40FFFF, NULL, RAM+0x010000);         // OBJECT RAM
   AddReadByte(0x500000, 0x507FFF, NULL, RAM+0x020000);         // BG0 RAM
   AddReadWord(0x600000, 0x60007F, cave_irq_cause_r, NULL);      // ?
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM+0x038000);         // COLOR RAM
   AddReadByte(0x900000, 0x900003, esp_input_rb, NULL);         // INPUT

   AddReadWord(0x000000, 0x0FFFFF, NULL, ROM+0x000000);         // 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);         // 68000 RAM
   AddReadWord(0x400000, 0x40FFFF, NULL, RAM+0x010000);         // OBJECT RAM
   AddReadWord(0x500000, 0x507FFF, NULL, RAM+0x020000);         // BG0 RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM+0x038000);         // COLOR RAM
   AddReadWord(0x900000, 0x900003, esp_input_rw, NULL);         // INPUT

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);      // 68000 RAM
   AddWriteByte(0x400000, 0x40FFFF, NULL, RAM+0x010000);      // OBJECT RAM
   AddWriteByte(0x500000, 0x507FFF, NULL, RAM+0x020000);      // BG0 RAM
   //AddWriteByte(0x600000, 0x60007F, esp_800000_wb, NULL);      // ?
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM+0x038000);      // COLOR RAM
   AddWriteByte(0x900000, 0x900003, esp_input_wb, NULL);      // INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);         // Trap Idle 68000
   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);      // 68000 RAM
   AddWriteWord(0x400000, 0x40FFFF, NULL, RAM+0x010000);      // OBJECT RAM
   AddWriteWord(0x500000, 0x507FFF, NULL, RAM+0x020000);      // BG0 RAM
   //AddWriteWord(0x600000, 0x60007F, esp_800000_ww, NULL);      // ?
   AddWriteBW(0x600000, 0x60007F, NULL, RAM_CTRL+0);
   
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM+0x038000);      // COLOR RAM
   AddWriteWord(0x900000, 0x900003, esp_input_ww, NULL);      // INPUT
   AddWriteWord(0xA00000, 0xA00001, cave_eeprom_w, NULL);      // EEPROM
   AddWriteWord(0x700000, 0x700007, NULL, RAM+0x048080);      // BG0 CTRL RAM
   setup_cave_game();
}

void execute_hotdogst_frame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(28,60));   // M68000 28MHz (60fps)

#ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
      debug_cave_vcu();
#endif

   cpu_execute_cycles(CPU_Z80_0, CPU_FRAME_MHz(4,60));        // Sound Z80
      
   vblank_irq = 1;
   irq_counter = 0;
   update_irq_state();
}

struct GAME_MAIN game_hotdogst =
{
   hotdogst_dirs,
   hotdogst_roms,
   cave_68k_inputs,
   NULL,
   NULL,

   load_hotdogst,
   clear_cave_68k,
   &hotdogst_video_r270,
   execute_hotdogst_frame,
   "hotdogst",
   "Hotdog Storm",
   "?",
   COMPANY_ID_MARBLE,
   NULL,
   1996,
   hotdogst_sound,
   GAME_SHOOT,
};

void clear_cave_68k(void)
{
   save_eeprom();

#ifdef RAINE_DEBUG
      save_debug("RAM.bin",RAM,RAMSize,1);
      save_debug("ROM.bin",ROM,get_region_size(REGION_ROM1),1);
#endif
}

#ifdef RAINE_DEBUG
void debug_cave_vcu(void)
{

   clear_ingame_message_list();

   if(RAM_BG0_CTRL)

      print_ingame(60,"BG0: (%04x,%04x) %04x", ReadWord(RAM_BG0_CTRL+0), ReadWord(RAM_BG0_CTRL+2), ReadWord(RAM_BG0_CTRL+4));

   if(RAM_BG1_CTRL)

      print_ingame(60,"BG1: (%04x,%04x) %04x", ReadWord(RAM_BG1_CTRL+0), ReadWord(RAM_BG1_CTRL+2), ReadWord(RAM_BG1_CTRL+4));

   if(RAM_BG2_CTRL)

      print_ingame(60,"BG2: (%04x,%04x) %04x", ReadWord(RAM_BG2_CTRL+0), ReadWord(RAM_BG2_CTRL+2), ReadWord(RAM_BG2_CTRL+4));


   print_ingame(60,"CTRL: %04x %04x %04x %04x %04x %04x %04x %04x", ReadWord(RAM_CTRL+0x00), ReadWord(RAM_CTRL+0x02), ReadWord(RAM_CTRL+0x04), ReadWord(RAM_CTRL+0x06), ReadWord(RAM_CTRL+0x08), ReadWord(RAM_CTRL+0x0A), ReadWord(RAM_CTRL+0x0C), ReadWord(RAM_CTRL+0x0E));

}
#endif

void execute_cave_68k_frame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(28,60));   // M68000 28MHz (60fps)

#ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
      debug_cave_vcu();
#endif

   vblank_irq = 1;
   irq_counter = 0;
   update_irq_state();
}


static void ClearTileQueue(void)
{
   int ta;

   last_tile = TileQueue;
   last_tile_scroll = TileQueue_scroll;
   for(ta=0; ta<MAX_PRI; ta++){
      first_tile[ta] = last_tile;
      next_tile[ta]  = last_tile;
      last_tile      = last_tile+1;

      if (raster_bg) {
	first_tile_scroll[ta] = last_tile_scroll;
	next_tile_scroll[ta]  = last_tile_scroll;
	last_tile_scroll      = last_tile_scroll+1;
      }
   }
}

static void TerminateTileQueue(void)
{
   int ta;
   struct TILE_Q *curr_tile;
   struct TILE_QS *curr_tile_scroll;

   for(ta=0; ta<MAX_PRI; ta++){

      curr_tile = next_tile[ta];

      curr_tile->next = NULL;

      if (raster_bg) {
	curr_tile_scroll = next_tile_scroll[ta];
	
	curr_tile_scroll->next = NULL;
      }
   }

}

static DEF_INLINE void QueueTile(UINT32 tile, int x, int y, UINT8 *map, UINT8 flip, UINT32 pri,UINT16 ww,UINT16 hh)
{
   struct TILE_Q *curr_tile;
   curr_tile = next_tile[pri];

   curr_tile->tile = tile;
   curr_tile->x    = x;
   curr_tile->y    = y;
   curr_tile->map  = map;
   curr_tile->flip = flip;
   curr_tile->next = last_tile;
   curr_tile->ww   = ww;
   curr_tile->hh   = hh;
   
   next_tile[pri]  = last_tile;
   last_tile       = last_tile+1;
}

static DEF_INLINE void QueueTileZoom(UINT32 tile, int x, int y, UINT8 *map, UINT8 flip, UINT32 pri,UINT16 ww,UINT16 hh,UINT16 zoomx, UINT16 zoomy)
{
   struct TILE_Q *curr_tile;
   curr_tile = next_tile[pri];

   curr_tile->tile = tile;
   curr_tile->x    = x;
   curr_tile->y    = y;
   curr_tile->map  = map;
   curr_tile->flip = flip;
   curr_tile->next = last_tile;
   curr_tile->ww   = ww;
   curr_tile->hh   = hh;
   curr_tile->zoomx= zoomx;
   curr_tile->zoomy= zoomy;
   
   next_tile[pri]  = last_tile;
   last_tile       = last_tile+1;
}

static DEF_INLINE void QueueTileScroll(UINT32 tile, int x, int y, UINT8 *map, UINT8 flip, UINT32 pri,UINT8 *dy)
{
   struct TILE_QS *curr_tile;
   curr_tile = next_tile_scroll[pri];

   curr_tile->tile = tile;
   curr_tile->x    = x;
   curr_tile->y    = y;
   curr_tile->map  = map;
   curr_tile->flip = flip;
   curr_tile->next = last_tile_scroll;
   curr_tile->dy   = dy;
   
   next_tile_scroll[pri]  = last_tile_scroll;
   last_tile_scroll       = last_tile_scroll+1;
}

static DEF_INLINE void make_tile_new(UINT32 num, UINT32 width)
{
   UINT32 x,y;
   UINT8 *src,*dst;
   
   src = GFX_OBJ_A + (num << 8);
   dst = GFX_OBJ_B + (num << 8);
   
   for(y = 0; y < (0x010 * 0x010); y += 0x010){
   for(x = 0; x < (width * 0x100); x += 0x100){

      dst[x + tile_16x16_map[y +  0]] = src[ 0];
      dst[x + tile_16x16_map[y +  1]] = src[ 1];
      dst[x + tile_16x16_map[y +  2]] = src[ 2];
      dst[x + tile_16x16_map[y +  3]] = src[ 3];
      dst[x + tile_16x16_map[y +  4]] = src[ 4];
      dst[x + tile_16x16_map[y +  5]] = src[ 5];
      dst[x + tile_16x16_map[y +  6]] = src[ 6];
      dst[x + tile_16x16_map[y +  7]] = src[ 7];
      dst[x + tile_16x16_map[y +  8]] = src[ 8];
      dst[x + tile_16x16_map[y +  9]] = src[ 9];
      dst[x + tile_16x16_map[y + 10]] = src[10];
      dst[x + tile_16x16_map[y + 11]] = src[11];
      dst[x + tile_16x16_map[y + 12]] = src[12];
      dst[x + tile_16x16_map[y + 13]] = src[13];
      dst[x + tile_16x16_map[y + 14]] = src[14];
      dst[x + tile_16x16_map[y + 15]] = src[15];

      src += 16;

   }
   }

   for(x = 0; x < width; x ++){
      GFX_OBJ_SIZE[num + x] = width;
      GFX_OBJ_SOLID[num + x] = check_tile_solid(GFX_OBJ_B + ((num + x) << 8), 0x100);
   }

}

static void DrawTileQueue(void)
{
  struct TILE_Q *tile_ptr;
  struct TILE_QS *tile_ptr_scroll;
  UINT32 ta,pri;
  UINT8 *GFX_BG = 0;
  UINT8 *GFX_BG_SOLID = 0;
  UINT8 which,*map,flip;
  UINT16 x,y,w,xx,ww,hh,zoomx,zoomy;

  TerminateTileQueue();

  for(pri=0;pri<MAX_PRI;pri++){
    tile_ptr = first_tile[pri];
    tile_ptr_scroll = first_tile_scroll[pri];
    which = pri & 3;
      
    switch(which){
    case 0:  // sprites
      GFX_BG = GFX_OBJ_B;
      GFX_BG_SOLID = GFX_OBJ_SOLID;
      break;
    case 1: // bg0
      GFX_BG = GFX_BG0;
      GFX_BG_SOLID = GFX_BG0_SOLID;
      break;
    case 2: // bg1
      GFX_BG = GFX_BG1;
      GFX_BG_SOLID = GFX_BG1_SOLID;
      break;
    case 3: // bg2
      GFX_BG = GFX_BG2;
      GFX_BG_SOLID = GFX_BG2_SOLID;
      break;
	default:
	  return;
	  break;
    }

      
    switch(which){
      
    case 3:
      if (romset==1) { // ddonpach has a 8x8 bg2
	while(tile_ptr->next){
	  ta = tile_ptr->tile;
	  if(GFX_BG_SOLID[ta]==1)                     // Some pixels; trans
	    Draw8x8_Trans_Mapped_Rot(&GFX_BG[ta<<6], tile_ptr->x, tile_ptr->y, tile_ptr->map);
	  else                                        // all pixels; solid
	    Draw8x8_Mapped_Rot(&GFX_BG[ta<<6], tile_ptr->x, tile_ptr->y, tile_ptr->map);
	  tile_ptr = tile_ptr->next;
	}
	break;
      }

    case 1:        // BG0 BG1 [no flipping] and bg2 (except ddonpach)
    case 2:
      while(tile_ptr->next){
	ta = tile_ptr->tile;
	if(GFX_BG_SOLID[ta]==1)                     // Some pixels; trans
	  Draw16x16_Trans_Mapped_Rot(&GFX_BG[ta<<8], tile_ptr->x, tile_ptr->y, tile_ptr->map);
	else                                        // all pixels; solid
	  Draw16x16_Mapped_Rot(&GFX_BG[ta<<8], tile_ptr->x, tile_ptr->y, tile_ptr->map);
	tile_ptr = tile_ptr->next;
      }

      // This is the only place where it makes sense : uopoko for now !
      if (raster_bg) {
	while(tile_ptr_scroll->next){
	  ta = tile_ptr_scroll->tile;
	  if(GFX_BG_SOLID[ta]==1)                     // Some pixels; trans
	    ldraw16x16_Trans_Mapped_Rot(&GFX_BG[ta<<8], tile_ptr_scroll->x, tile_ptr_scroll->y, tile_ptr_scroll->map,tile_ptr_scroll->dy);
	  else                                        // all pixels; solid
	    ldraw16x16_Mapped_Rot(&GFX_BG[ta<<8], tile_ptr_scroll->x, tile_ptr_scroll->y, tile_ptr_scroll->map,tile_ptr_scroll->dy);
	  tile_ptr_scroll = tile_ptr_scroll->next;
	}
      }
      break;
    case 0:        // OBJECT [flipping]
      while(tile_ptr->next){
	ta = tile_ptr->tile;
	x   = tile_ptr->x;
	y   = tile_ptr->y;
	ww  = tile_ptr->ww;
	flip= tile_ptr->flip;
	hh  = tile_ptr->hh;
	if (*dsw_buffer & 1){
	  zoomx = zoomy = 16;
	} else {
	  zoomx= tile_ptr->zoomx;
	  zoomy= tile_ptr->zoomy;
	}
	map = tile_ptr->map;
	
	switch(flip){
	case 0:
	  do{
	    if(GFX_OBJ_SIZE[ta] != ww)
	      make_tile_new(ta, ww);
	    xx = x;
	    w = ww;
	    
	    do{
	      if((xx > zoomx) && (xx < limitx-zoomx) && (y > zoomy) && (y < 240+32-zoomy+16)){
		  
		if(GFX_OBJ_SOLID[ta]){
		  if(GFX_BG_SOLID[ta]==1)      // Some pixels; trans
		    Draw16x16_64_Trans_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 0);
		  else                         // all pixels; solid
		    Draw16x16_64_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 0);
		}
	      }
	      xx = (xx + zoomx) & 0x3FF;
	      ta++; //ta = (ta + 1) & 0xFFFF;
		
	    }while(--w);

	    y = (y + zoomy) & 0x3FF;

	  }while(--hh);
	  break;
	case 1:
	  x = (x + ((ww - 1) * 16)) & 0x3FF;
	  do{
	     
	    if(GFX_OBJ_SIZE[ta] != ww)
	      make_tile_new(ta, ww);
	    xx = x;
	    w = ww;
	     
	    do{
	       
	      if(GFX_OBJ_SOLID[ta]){
		if((xx > zoomx) && (xx < limitx-zoomx) && (y > zoomy) && (y < 240+32-zoomy+16)){
		  
		  if(GFX_BG_SOLID[ta]==1)      // Some pixels; trans
		    Draw16x16_64_Trans_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 1);
		  else                         // all pixels; solid
		    Draw16x16_64_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 1);
		}
	      }
	       
	      xx = (xx - zoomx) & 0x3FF;
	      ta++; //(code + 1) & 0xFFFF;
	    }while(--w);
	    
	    y = (y + zoomy) & 0x3FF;
	     
	  }while(--hh);
	  break;
	case 2:
	  y = (y + ((hh - 1) * 16)) & 0x3FF;
	  do{
	     	     
	    if(GFX_OBJ_SIZE[ta] != ww)
	      make_tile_new(ta, ww);
	    xx = x;
	    w = ww;
	     
	    do{
	       
	      if(GFX_OBJ_SOLID[ta]){
		if((xx > zoomx) && (xx < limitx-zoomx) && (y > zoomy) && (y < 240+32-zoomy+16)){
		  if(GFX_BG_SOLID[ta]==1)      // Some pixels; trans
		    Draw16x16_64_Trans_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 2);
		  else                         // all pixels; solid
		    Draw16x16_64_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 2);
		}
	      }
	       
	      xx = (xx + zoomx) & 0x3FF;
	      ta++; //(code + 1) & 0xFFFF;
	       
	    }while(--w);
	     
	    y = (y - zoomy) & 0x3FF;
	     
	  }while(--hh);
	  break;
	case 3:
	  x = (x + ((ww - 1) * 16)) & 0x3FF;
	  y = (y + ((hh - 1) * 16)) & 0x3FF;
	  do{
	    
	    if(GFX_OBJ_SIZE[ta] != ww)
	      make_tile_new(ta, ww);
	    xx = x;
	    w = ww;
	    
	    do{
	      
	      if((xx > zoomx) && (xx < limitx-zoomx) && (y > zoomy) && (y < 240+32-zoomy+16)){
		
		if(GFX_OBJ_SOLID[ta]){
		  if(GFX_BG_SOLID[ta]==1)      // Some pixels; trans
		    Draw16x16_64_Trans_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 3);
		  else                         // all pixels; solid
		    Draw16x16_64_Mapped_ZoomXY_flip_Rot(&GFX_BG[ta<<8], xx, y, map, zoomx, zoomy, 3);
		}
	      }
	      xx = (xx - zoomx) & 0x3FF;
	      ta++; // = (code + 1) & 0xFFFF;
	      
	    }while(--w);
	    
	    y = (y - zoomy) & 0x3FF;
	    
	  }while(--hh);
	  break;
	}	 
	tile_ptr = tile_ptr->next;
      } // while (tile_ptr->next)
      break;
    }
  }
}

void draw_cave_layer(int num)
{
   int x,y,ta;
   int zz;
   int x16,y16,zzz,zzzz;
   UINT8 *map;
   int scrollx, scrolly;
   
   UINT8 *RAM_BG = NULL;
   UINT8 *SCR_BG = NULL;
   UINT8 *MSK_BG = NULL;
   int tile_num = num+1;
   
   if(! check_layer_enabled(layer_id_data[num]))

      return;

   switch(num)
   {
      case 0:
         RAM_BG = RAM_BG0;
         SCR_BG = RAM_BG0_CTRL;
         MSK_BG = GFX_BG0_SOLID;
      break;
      case 1:
         RAM_BG = RAM_BG1;
         SCR_BG = RAM_BG1_CTRL;
         MSK_BG = GFX_BG1_SOLID;
      break;
      case 2:
         RAM_BG = RAM_BG2;
         SCR_BG = RAM_BG2_CTRL;
         MSK_BG = GFX_BG2_SOLID;
      break;
	  default:
	     return;
	  break;
   }

   if (!SCR_BG) return;
   if ((!(ReadWord(SCR_BG+4) & 1)) && romset != 4){ // Layer active ?
// romset == 4 -> hotdogst ignores activation bit...
#ifdef RAINE_DEBUG
     print_ingame(60,"layer %d inactive\n",num);
#endif     
       return;
   } 

 scrollx = ReadWord(SCR_BG+0);
 scrolly = ReadWord(SCR_BG+2);

 if (flipx) {
   scrollx += scrolldx_flip[num];
 } else {
   scrollx += scrolldx[num];
 }

#if 0
if (raster_bg && (num==0 || romset==3)) // Feveron is the only game
   if ((scrollx & 0x4000))
     scrollx += ReadWord(RAM_BG+0x1000);
#endif
 
 if (flipy) 
   scrolly = 0x100 + scrolly;
 else
   scrolly = 0x11 + scrolly;


 if((num==2) && (romset==1))// bg2 is 8x8 in ddonpach
   {

   MAKE_SCROLL_512x512_4_8(scrollx, scrolly);

   START_SCROLL_512x512_4_8(32,32,gamex,240);

      ta = ReadWord(&RAM_BG[zz+2]);
      if(MSK_BG[ta])
      {
         MAP_PALETTE_MAPPED_NEW(
            (RAM_BG[zz+1] & 0x3F) | 0x40,
            gfx_colours,
            map
         );

         QueueTile(ta, x, y, map, 0, (2+1) | ((RAM_BG[zz+1] & 0xC0) >> 4),0,0);
      }

   END_SCROLL_512x512_4_8();
   }
   else
   {

   MAKE_SCROLL_512x512_4_16(scrollx, scrolly);

   if (raster_bg && (num==0 || romset==3)) // Feveron is the only game
     if (scrollx & 0x4000) {
       // We have a linescroll effect !
       if (romset == 2) {
	 // For now I only handle uopoko this is the only one with no
	 // column scroll, and with offsets <= 32
	 //int starty=32-y16;
	 UINT8 *dy;
	 //START_SCROLL_512x512_4_16(32,32,gamex,240);
	 // I am obliged to modify all the scroll primitives... (for speed)
	 zzzz+=4;
	 zz=zzzz;
	 // This code is very dirty. It was a quick hack to see if it is
	 // possible. Now that I know it is, it would be nice to rewrite it
	 // a better way : it should scan the whole 512x512 window normally...
	 for(y=(32-y16);(UINT32)y<(240+32);y+=16){
	   dy = RAM_BG+0x1000;
	   for(x=(-8-x16);x<(320);x+=16){
	     //for(x=(32-x16);x<(320);x+=16){
	     ta = ReadWord(&RAM_BG[zz+2])&0x7FFF;
	     
	     if(MSK_BG[ta])
	       {
		 MAP_PALETTE_MAPPED_NEW(
			       //((ReadWord(&RAM_BG[zz])>>8) | 0x40) & 0x7F,
			       (RAM_BG[zz+1] & 0x3F) | 0x40,
			       gfx_colours,
			       map
			       );
		 
		 QueueTileScroll(ta, x, y, map, 0, ((RAM_BG[zz+1] & 0xC0) >> 4) | tile_num,dy);
	       }
	  END_SCROLL_512x512_4_16();
	  return;
	}
	 }
	 
	 if (romset==4) {
	   // hotdog storm shares its palette between sprites & layers...
	   START_SCROLL_512x512_4_16(32,32,gamex,240);

	   ta = ReadWord(&RAM_BG[zz+2])&0x7FFF;
	   if(MSK_BG[ta])
	     {
	       MAP_PALETTE_MAPPED_NEW(
                (ReadWord(&RAM_BG[zz])>>8) & 0x7f, // | 0x40) & 0x7F,
		gfx_colours,
		map
		);
	       QueueTile(ta, x, y, map, 0, ((RAM_BG[zz+1] & 0xC0) >> 4) | tile_num,0,0);
	     }

	   END_SCROLL_512x512_4_16();
	 } else {
	   START_SCROLL_512x512_4_16(32,32,gamex,240);

	   ta = ReadWord(&RAM_BG[zz+2])&0x7FFF;
	   if(MSK_BG[ta])
	     {
	       MAP_PALETTE_MAPPED_NEW(
                ((ReadWord(&RAM_BG[zz])>>8) | 0x40) & 0x7F,
		gfx_colours,
		map
		);
	       QueueTile(ta, x, y, map, 0, ((RAM_BG[zz+1] & 0xC0) >> 4) | tile_num,0,0);
	     }

	   END_SCROLL_512x512_4_16();
	 }
       }
     }

static UINT8 flip_map[4] =
{
   0, 2, 1, 3
};

void draw_cave_68k(void)
{
   int x,y,ta,size,code,priority,zoomx,zoomy;
   int ww,hh,flip,attr;
   UINT8 *map, *RAM_OBJ;
   
   ClearPaletteMap();

   MAP_PALETTE_MAPPED_NEW(
      0x40,
      gfx_colours,
      map
   );

   clear_game_screen(map[0]);
   ClearTileQueue();

   flipx = ReadWord(RAM_CTRL+0) & 0x8000;
   flipy = ReadWord(RAM_CTRL+2) & 0x8000;

   if(! (ReadWord(RAM_CTRL+8) & 1))
      RAM_OBJ = RAM + 0x10000;
   else
      RAM_OBJ = RAM + 0x14000;

   /*

   OBJECT

   */

   if(check_layer_enabled(layer_id_data[3])) { // sprites enabled
     
     for(ta=0; ta < 0x4000; ta+=16){
       if (romset==1) {
	 attr = ReadWord(&RAM_OBJ[0+ta]);
	 size = ReadWord(&RAM_OBJ[8+ta]);
	 zoomx=16;
	 zoomy=16;
       } else {
	 attr = ReadWord(&RAM_OBJ[4+ta]);
	 zoomx= ReadWord(&RAM_OBJ[8+ta])/0x10;
	 zoomy= ReadWord(&RAM_OBJ[10+ta])/0x10;
	 size = ReadWord(&RAM_OBJ[12+ta]);
	 if (zoomx>64)
	   zoomx=64;
	 if (zoomy>64)
	   zoomy=64;
       }

       priority = ((attr & 0x0030) >> 2);// | 3;

       ww = (size >> 8) & 0x1F; 
       hh = (size >> 0) & 0x1F;

       if((ww) && (hh)){
	 if (romset==1) { // donpachi / ddonpach
	   code = ReadWord(&RAM_OBJ[2+ta]);
	   x = ReadWord(&RAM_OBJ[4+ta])<<6;
	   y = ReadWord(&RAM_OBJ[6+ta])<<6;
	 } else {
	   code = ReadWord(&RAM_OBJ[6+ta]);
	   x = ReadWord(&RAM_OBJ[0+ta]);
	   y = ReadWord(&RAM_OBJ[2+ta]);
	 }
	 
	 if (romset == 4) { // Hotdogst
	   x = (x + 32) & 0x3FF;
	   y = (y + 32) & 0x3FF;
	 } else {
	   x = ((x >> 6) + 32) & 0x3FF;
	   y = ((y >> 6) + 32) & 0x3FF;
	 }
	 x -= (zoomx-16)/2;
	 y -= (zoomy-16)/2;
	 
	 code += (attr & 3) << 16;

	 if (((unsigned long)(code + ww*hh) > sprite_size) || y<0){ // overflow (happens in guwange...)
#ifdef RAINE_DEBUG
	   print_ingame(60,"sprite overflow\n");
#endif
	   continue;
	 }
	 flip = flip_map[(attr >>2) & 0x03];

	 MAP_PALETTE_MAPPED_NEW(
				(attr>>8) & 0x3F,
				gfx_colours,
				map
				);
	   // Really short, uh ? All the flipping code is in DrawTileQueue !
	 QueueTileZoom(code, x, y, map, flip, priority,ww,hh, zoomx, zoomy);
       }
     }
   }
   
   /*
     
     BG0
     
   */
   
   draw_cave_layer(0);
   
   /*
     
     BG1
     
   */
   
   draw_cave_layer(1);
   
   /*
     
     BG2
     
   */
   draw_cave_layer(2);
   
   DrawTileQueue();
}

/*

bg ram 0x0000-0x0FFF:

byte |     bit(s)     | use
-----+FEDCBA9876543210+-----------
 0-1 |..xxxxxx........| colour
 2-3 |.xxxxxxxxxxxxxxx| tile

bg ram 0x1000-0x1FFF:

byte |     bit(s)     | use
-----+FEDCBA9876543210+-----------
 0-1 |..........xxxxxx| ?
 2-3 |................|

bg ctrl ram:

byte |     bit(s)     | use
-----+FEDCBA9876543210+-----------
 0-1 |xxx.............| x offset
 0-1 |.......xxxxxxxxx| x pos
 2-3 |xxx.............| y offset
 2-3 |......x.........| priority ?
 2-3 |.......xxxxxxxxx| y pos

object ram:

byte |     bit(s)     | use
-----+FEDCBA9876543210+-----------
 0-1 |xxxxxxxxxx......| x pos
 2-3 |xxxxxxxxxx......| y pos
 4-5 |..xxxxxx........| colour
 4-5 |............x...| flip x axis
 4-5 |.............x..| flip y axis
 6-7 |xxxxxxxxxxxxxxxx| tile
 8-9 |................|
 A-B |................|
 C-D |....xxxx........| x chain
 C-D |............xxxx| y chain
 E-F |................|

ctrl ram:

byte |     bit(s)     | use
-----+FEDCBA9876543210+-----------
08-09|...............x| object double buffer

*/
