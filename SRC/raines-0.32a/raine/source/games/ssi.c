/******************************************************************************/
/*                                                                            */
/*         SUPER SPACE INVADERS 91/MJ12 (C)1991 TAITO CORPORATION             */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "ssi.h"
#include "tc220ioc.h"
#include "tc200obj.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO majestic_twelve_dirs[] =
{
   { "majestic_twelve", },
   { "mj12", },
   { "majest12", },
   { ROMOF("ssi"), },
   { CLONEOF("ssi"), },
   { NULL, },
};

static struct ROM_INFO majestic_twelve_roms[] =
{
   {  "ssi_m01.rom", 0x00100000, 0xa1b4f486, REGION_GFX2, 0x000000, LOAD_NORMAL, },
   {  "ssi_m02.rom", 0x00020000, 0x3cb0b907, REGION_SMP1, 0x000000, LOAD_NORMAL, },
   {   "c64-07.bin", 0x00020000, 0xf29ed5c9, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {   "c64-08.bin", 0x00020000, 0xddfd33d5, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "c64-06.bin", 0x00020000, 0x18dc71ac, REGION_ROM1, 0x040000, LOAD_8_16,   },
   {   "c64-05.bin", 0x00020000, 0xb61866c0, REGION_ROM1, 0x040001, LOAD_8_16,   },
   {   "ssi_09.rom", 0x00010000, 0x88d7f65c, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {           NULL,          0,          0, 0,           0,        0,           },
};

static struct INPUT_INFO majestic_twelve_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x02200E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x02200E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x02200E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x02200E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x022004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x022004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x022004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x022004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x022004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x022004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x022004, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x022006, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x022006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x022006, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x022006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x022006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x022006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x022006, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_majestic_twelve_0[] =
{
   { "Cabinet",               0x01, 0x02 },
   { "Table",                 0x01, 0x00 },
   { "Upright",               0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_majestic_twelve_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Shields",               0x0C, 0x04 },
   { "1",                     0x0C, 0x00 },
   { "3",                     0x04, 0x00 },
   { "2",                     0x08, 0x00 },
   { "0",                     0x00, 0x00 },
   { "Lives",                 0x10, 0x02 },
   { "3",                     0x10, 0x00 },
   { "2",                     0x00, 0x00 },
   { "2 Players",             0x20, 0x02 },
   { "Simultaneous",          0x20, 0x00 },
   { "Alternate",             0x00, 0x00 },
   { "Continue Game",         0x40, 0x02 },
   { MSG_ON,                  0x40, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { "Screen Rotation",       0x80, 0x02 },
   { MSG_ON,                  0x80, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO majestic_twelve_dsw[] =
{
   { 0x022000, 0xFF, dsw_data_majestic_twelve_0 },
   { 0x022002, 0xFF, dsw_data_majestic_twelve_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_majestic_twelve_0[] =
{
   { "Taito Japan (MJ12)",    0x01 },
   { "Taito America (MJ12)",  0x02 },
   { "Taito Japan (SSI)",     0x03 },
   { NULL,                    0    },
};

static struct ROMSW_INFO majestic_twelve_romsw[] =
{
   { 0x07FFFF, 0x01, romsw_data_majestic_twelve_0 },
   { 0,        0,    NULL },
};

static struct GFX_LAYOUT tilelayout =
{
	16,16,	/* 16*16 sprites */
	RGN_FRAC(1,1),
	4,	/* 4 bits per pixel */
	{ 0, 1, 2, 3 },
	{ 1*4, 0*4, 3*4, 2*4, 5*4, 4*4, 7*4, 6*4, 9*4, 8*4, 11*4, 10*4, 13*4, 12*4, 15*4, 14*4 },
	{ 0*64, 1*64, 2*64, 3*64, 4*64, 5*64, 6*64, 7*64, 8*64, 9*64, 10*64, 11*64, 12*64, 13*64, 14*64, 15*64 },
	128*8	/* every sprite takes 128 consecutive bytes */
};

static struct GFX_LAYOUT charlayout =
{
	8,8,	/* 8*8 characters */
	RGN_FRAC(1,1),
	4,	/* 4 bits per pixel */
	{ 0, 1, 2, 3 },
	{ 2*4, 3*4, 0*4, 1*4, 6*4, 7*4, 4*4, 5*4 },
	{ 0*32, 1*32, 2*32, 3*32, 4*32, 5*32, 6*32, 7*32 },
	32*8	/* every sprite takes 32 consecutive bytes */
};

static struct GFX_LIST taito_f2_gfx[] =
{
   { REGION_GFX1, &charlayout, },
   { REGION_GFX2, &tilelayout, },
   { 0,           NULL,        },
};

static struct VIDEO_INFO majestic_twelve_video =
{
   draw_super_space_invaders,
   320,
   224,
   32,
   VIDEO_ROTATE_270 |
   VIDEO_ROTATABLE,
   taito_f2_gfx,
};

struct GAME_MAIN game_majestic_twelve =
{
   majestic_twelve_dirs,
   majestic_twelve_roms,
   majestic_twelve_inputs,
   majestic_twelve_dsw,
   majestic_twelve_romsw,

   load_super_space_invaders,
   clear_super_space_invaders,
   &majestic_twelve_video,
   execute_super_space_invaders_frame,
   "majest12",
   "Majestic Twelve",
   "マジェスティックトウェルブ",
   COMPANY_ID_TAITO,
   "C64",
   1990,
   taito_ym2610_sound,
   GAME_SHOOT,
};

static struct DIR_INFO super_space_invaders_91_dirs[] =
{
   { "super_space_invaders_91", },
   { "ssi", },
   { "space_invaders_91", },
   { NULL, },
};

static struct ROM_INFO super_space_invaders_91_roms[] =
{
   {  "ssi_m01.rom", 0x00100000, 0xa1b4f486, REGION_GFX2, 0x000000, LOAD_NORMAL, },
   {  "ssi_m02.rom", 0x00020000, 0x3cb0b907, REGION_SMP1, 0x000000, LOAD_NORMAL, },
   { "ssi_15-1.rom", 0x00040000, 0xce9308a6, REGION_ROM1, 0x000000, LOAD_8_16,   },
   { "ssi_16-1.rom", 0x00040000, 0x470a483a, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "ssi_09.rom", 0x00010000, 0x88d7f65c, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {           NULL,          0,          0, 0,           0,        0,           },
};

struct GAME_MAIN game_super_space_invaders_91 =
{
   super_space_invaders_91_dirs,
   super_space_invaders_91_roms,
   majestic_twelve_inputs,
   majestic_twelve_dsw,
   majestic_twelve_romsw,

   load_super_space_invaders,
   clear_super_space_invaders,
   &majestic_twelve_video,
   execute_super_space_invaders_frame,
   "ssi",
   "Super Space Invaders 91",
   "スーパースペースインベーダー’９１",
   COMPANY_ID_TAITO,
   "C64",
   1990,
   taito_ym2610_sound,
   GAME_SHOOT,
};

static UINT8 *RAM_OBJECT;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

void load_super_space_invaders(void)
{
   RAMSize=0x40000+0x10000;

   if(!(RAM=AllocateMem(RAMSize))) return;

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;

   memcpy(Z80ROM, load_region[REGION_ROM2], 0x10000);
   PCMROM = load_region[REGION_SMP1];

   YM2610SetBuffers(PCMROM, PCMROM, 0x20000, 0x20000);
   AddTaitoYM2610(0x0217, 0x018F, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_INPUT  = RAM+0x22000;
   RAM_OBJECT = RAM+0x10000;

   GFX_SPR = load_region[REGION_GFX2];
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x20000, 0x100, 0x10, 0x1000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);

   WriteWord68k(&ROM[0x01FB4],0x4EF9);
   WriteLong68k(&ROM[0x01FB6],0x000000C0);

   WriteWord68k(&ROM[0x000C0],0x4EB9);		//	jsr	<???>
   if(is_current_game("ssi"))
      WriteLong68k(&ROM[0x000C2],0x0000932A);
   else
      WriteLong68k(&ROM[0x000C2],0x0000929A);

   WriteWord68k(&ROM[0x000C6],0x4EB9);		//	jsr	<random gen>
   WriteLong68k(&ROM[0x000C8],0x00001BE6);

   WriteLong68k(&ROM[0x000CC],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x000D0],0x00AA0000);

   WriteWord68k(&ROM[0x000D4],0x6100-0x16);	//	bra.s	<loop>

   if(is_current_game("ssi"))
   {
      WriteLong68k(&ROM[0x09CFC],0x00000100);

      WriteWord68k(&ROM[0x00100],0x4EB9);
      WriteLong68k(&ROM[0x00102],0x0000B3E6);
      WriteLong68k(&ROM[0x00106],0x13FC0000);	//	move.b	#$00,$AA0000
      WriteLong68k(&ROM[0x0010A],0x00AA0000);
      WriteWord68k(&ROM[0x0010E],0x4E75);
   }
   else
   {
      WriteLong68k(&ROM[0x09C6C],0x00000100);

      WriteWord68k(&ROM[0x00100],0x4EB9);
      WriteLong68k(&ROM[0x00102],0x0000B41C);
      WriteLong68k(&ROM[0x00106],0x13FC0000);	//	move.b	#$00,$AA0000
      WriteLong68k(&ROM[0x0010A],0x00AA0000);
      WriteWord68k(&ROM[0x0010E],0x4E75);
   }

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT;
   tc0200obj.RAM_B	= NULL;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x1FFF;
   tc0200obj.ofs_x	= 0;
   tc0200obj.ofs_y	= 0;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x100000, 0x10000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x400000, 0x400003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM_OBJECT);			// SPRITE
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM_OBJECT);			// SPRITE
   AddReadWord(0x300000, 0x301FFF, NULL, RAM+0x020000);			// COLOR RAM
   AddReadWord(0x100000, 0x10000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x200000, 0x20FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM_OBJECT);			// SPRITE
   AddWriteByte(0x400000, 0x400003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x500000, 0x5000FF, NULL, RAM+0x022200);		// ???
   AddWriteByte(0x100000, 0x10000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x200000, 0x20FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM_OBJECT);			// SPRITE
   AddWriteWord(0x300000, 0x301FFF, NULL, RAM+0x020000);		// COLOR RAM
   AddWriteWord(0x500000, 0x5000FF, NULL, RAM+0x022200);		// ???
   AddWriteWord(0x100000, 0x10000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void clear_super_space_invaders(void)
{
   RemoveTaitoYM2610();
}

void execute_super_space_invaders_frame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 5);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(6,60));	// Overflow for sprite sync

   Taito2610_Frame();			// Z80 and YM2610
}

void draw_super_space_invaders(void)
{
   ClearPaletteMap();

   clear_game_screen(0);

   render_tc0200obj_mapped_r270_b_rot();
}


