/******************************************************************************/
/*                                                                            */
/*                    PULIRULA (C) 1991 TAITO CORPORATION                     */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "pulirula.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

/********************
   PULIRULA (WORLD)
 ********************/

static struct DIR_INFO pulirula_dirs[] =
{
   { "pulirula", },
   { NULL, },
};

static struct ROM_INFO pulirula_roms[] =
{
   {   "c98-06.rom", 0x00020000, 0x64a71b45, 0, 0, 0, },
   {   "c98-02.rom", 0x00100000, 0x4a2ad2b3, 0, 0, 0, },
   {   "c98-03.rom", 0x00100000, 0x589a678f, 0, 0, 0, },
   {   "c98-04.rom", 0x00100000, 0x0e1fe3b2, 0, 0, 0, },
   {   "c98-05.rom", 0x00080000, 0x9ddd9c39, 0, 0, 0, },
   {   "c98-01.rom", 0x00100000, 0x197f66f5, 0, 0, 0, },
   {   "c98-07.rom", 0x00020000, 0x90195bc0, 0, 0, 0, },
   {   "c98-12.rom", 0x00040000, 0x816d6cde, 0, 0, 0, },
   {   "c98-14.rom", 0x00020000, 0xa858e17c, 0, 0, 0, },
   {   "c98-16.rom", 0x00040000, 0x59df5c77, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO pulirula_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03A10E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03A10E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03A10E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03A10E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x03A104, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x03A104, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x03A104, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x03A104, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x03A104, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x03A104, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x03A104, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x03A104, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x03A106, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x03A106, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x03A106, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x03A106, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x03A106, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x03A106, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x03A106, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x03A106, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_pulirula_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_pulirula_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Magic",                 0x0C, 0x04 },
   { "3",                     0x0C, 0x00 },
   { "4",                     0x08, 0x00 },
   { "5",                     0x04, 0x00 },
// { "5",                     0x00, 0x00 },
   { MSG_LIVES,               0x30, 0x04 },
   { "3",                     0x30, 0x00 },
   { "2",                     0x20, 0x00 },
   { "4",                     0x10, 0x00 },
   { "5",                     0x00, 0x00 },
   { MSG_DSWB_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "Upright Controls",      0x80, 0x02 },
   { "Dual",                  0x80, 0x00 },
   { "Single",                0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO pulirula_dsw[] =
{
   { 0x03A100, 0xFF, dsw_data_pulirula_0 },
   { 0x03A102, 0xFF, dsw_data_pulirula_1 },
   { 0,        0,    NULL,      },
};

/*
static struct ROMSW_DATA romsw_data_pulirula_0[] =
{
   { "Taito (Japanese)",      0x01 },
   { "Taito America",         0x02 },
   { "Taito Japan",           0x03 },
   { NULL,                    0    },
};

static struct ROMSW_INFO pulirula_romsw[] =
{
   { 0x07FFFF, 0x01, romsw_data_pulirula_0 },
   { 0,        0,    NULL },
};
*/

static struct VIDEO_INFO pulirula_video =
{
   draw_pulirula,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_pulirula =
{
   pulirula_dirs,
   pulirula_roms,
   pulirula_inputs,
   pulirula_dsw,
   NULL,

   load_pulirula,
   clear_pulirula,
   &pulirula_video,
   execute_pulirula_frame,
   "pulirula",
   "Pulirula",
   "プリルラ",
   COMPANY_ID_TAITO,
   "C98",
   1991,
   taito_ym2610_sound,
   GAME_BEAT,
};

/********************
   PULIRULA (JAPAN)
 ********************/

static struct DIR_INFO pulirula_jp_dirs[] =
{
   { "pulirula_jp", },
   { "pulirulj", },
   { ROMOF("pulirula"), },
   { CLONEOF("pulirula"), },
   { NULL, },
};

static struct ROM_INFO pulirula_jp_roms[] =
{
   {   "c98-06.rom", 0x00020000, 0x64a71b45, 0, 0, 0, },
   {   "c98-02.rom", 0x00100000, 0x4a2ad2b3, 0, 0, 0, },
   {   "c98-03.rom", 0x00100000, 0x589a678f, 0, 0, 0, },
   {   "c98-04.rom", 0x00100000, 0x0e1fe3b2, 0, 0, 0, },
   {   "c98-05.rom", 0x00080000, 0x9ddd9c39, 0, 0, 0, },
   {   "c98-01.rom", 0x00100000, 0x197f66f5, 0, 0, 0, },
   {   "c98-07.rom", 0x00020000, 0x90195bc0, 0, 0, 0, },
   {   "c98-12.rom", 0x00040000, 0x816d6cde, 0, 0, 0, },
   {   "c98-14.rom", 0x00020000, 0xa858e17c, 0, 0, 0, },
   {   "c98-13",     0x00040000, 0xb7d13d5b, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct DSW_DATA dsw_data_pulirula_jp_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_1COIN_2PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_1COIN_2PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_3COIN_1PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO pulirula_jp_dsw[] =
{
   { 0x03A100, 0xFF, dsw_data_pulirula_jp_0 },
   { 0x03A102, 0xFF, dsw_data_pulirula_1 },
   { 0,        0,    NULL,      },
};

struct GAME_MAIN game_pulirula_jp =
{
   pulirula_jp_dirs,
   pulirula_jp_roms,
   pulirula_inputs,
   pulirula_jp_dsw,
   NULL,

   load_pulirula,
   clear_pulirula,
   &pulirula_video,
   execute_pulirula_frame,
   "pulirulj",
   "Pulirula (Japan)",
   "プリルラ",
   COMPANY_ID_TAITO,
   "C98",
   1991,
   taito_ym2610_sound,
   GAME_BEAT,
};


static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_COLOUR;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static void BadWriteWord(UINT32 address, UINT16 data)
{
#ifdef RAINE_DEBUG
      if(address!=0xA00000) print_debug("Ww(%06x,%04x) [%06x]\n",address,data,s68000context.pc);
#endif
}

void load_pulirula(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x100000))) return;
   if(!(GFX=AllocateMem(0x300000+0x400000))) return;

   GFX_BG0	=GFX+0x000000;
   GFX_SPR	=GFX+0x300000;

   tb=0;
   if(!load_rom("c98-04.rom", RAM, 0x100000)) return;		// 8x8 TILES
   for(ta=0;ta<0x100000;ta+=2,tb+=4){
      GFX[tb+3]=RAM[ta]&15;
      GFX[tb+2]=RAM[ta]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+0]=RAM[ta+1]>>4;
   }
   if(!load_rom("c98-05.rom", RAM, 0x80000)) return;		// 8x8 TILES
   for(ta=0;ta<0x80000;ta++,tb+=2){
      GFX[tb+0]=RAM[ta]>>4;
      GFX[tb+1]=RAM[ta]&15;
   }

   tb=0;
   if(!load_rom("c98-02.rom", RAM, 0x100000)) return;		// 16x16 SPRITES ($2000)
   for(ta=0;ta<0x100000;ta++,tb+=2){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }
   if(!load_rom("c98-03.rom", RAM, 0x100000)) return;		// 16x16 SPRITES ($2000)
   for(ta=0;ta<0x100000;ta++,tb+=2){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }

   for(ta=0;ta<0x100;ta++){		// Remove Masking Sprite
      GFX_SPR[ta+0x100]=0;
   }

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0xC0000))) return;

   if(!load_rom("c98-12.rom", RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom_index(9, RAM, 0x40000)) return;
   for(ta=0;ta<0x40000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("c98-06.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x80000]=RAM[ta];
   }
   if(!load_rom("c98-07.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x80001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x48000;
   if(!load_rom("c98-14.rom", Z80ROM, 0x20000)) return;			// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x180000))) return;
   if(!load_rom("c98-05.rom", PCMROM+0x000000, 0x080000)) return;	// ADPCM A rom
   if(!load_rom("c98-01.rom", PCMROM+0x080000, 0x100000)) return;	// ADPCM B rom
   YM2610SetBuffers(PCMROM, PCMROM+0x80000, 0x080000, 0x100000);

   AddTaitoYM2610(0x01A4, 0x0150, 0x20000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x20000;
   RAM_SCROLL = RAM+0x3A000;
   RAM_OBJECT = RAM+0x10000;
   RAM_COLOUR = RAM+0x30000;
   RAM_INPUT  = RAM+0x3A100;
   GFX_FG0    = RAM+0x40000;

   RAMSize=0x68000;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x10000);	// Only 0xC000, but mask is 0xFFFF
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x4000);

   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb);
   InitPaletteMap(RAM_COLOUR, 0x100, 0x10, 0x8000);


   // 68000 Speed Hack
   // ----------------

   WriteWord68k(&ROM[0x084C],0x4EF9);		// jmp $300
   WriteLong68k(&ROM[0x084E],0x00000300);
   WriteLong68k(&ROM[0x00300],0x526DABF2);	// jsr <random gen>
   WriteLong68k(&ROM[0x00304],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x00308],0x00AA0000);
   WriteWord68k(&ROM[0x0030C],0x6100-14);	// bra.s <loop>

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX_BG0;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0xFFFF;
   tc0100scn[0].layer[0].scr_x	=17;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX_BG0;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0xFFFF;
   tc0100scn[0].layer[1].scr_x	=17;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=3;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=17;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT+0x0000;
   tc0200obj.RAM_B	= RAM_OBJECT+0x8000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x3FFF;
   tc0200obj.ofs_x	= -93;
   tc0200obj.ofs_y	= -16;

   tc0200obj.RAM_TILE	= RAM+0x36000;
   tc0200obj.RAM_TILE_B	= RAM+0x36800;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0xC0000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x0BFFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x0BFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x300000, 0x30FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x700000, 0x701FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadByte(0x400000, 0x403FFF, NULL, RAM+0x032000);			// ??? RAM
   AddReadByte(0xB00000, 0xB0000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x200000, 0x200003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x0BFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x300000, 0x30FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x700000, 0x701FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadWord(0x400000, 0x403FFF, NULL, RAM+0x032000);			// ??? RAM
   AddReadWord(0xB00000, 0xB0000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x300000, 0x30FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x700000, 0x701FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteByte(0x400000, 0x403FFF, NULL, RAM+0x032000);		// ??? RAM
   AddWriteByte(0x200000, 0x200003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0xB00000, 0xB0000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x300000, 0x30FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x700000, 0x701FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteWord(0x400000, 0x403FFF, NULL, RAM+0x032000);		// ??? RAM
   AddWriteWord(0x600000, 0x603FFF, NULL, RAM+0x036000);		// OBJECT EXTRA RAM
   AddWriteWord(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0xB00000, 0xB0000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, BadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void clear_pulirula(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      //save_debug("RAM.bin",RAM,0x040000,1);
   #endif
}

void execute_pulirula_frame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);

   Taito2610_Frame();			// Z80 and YM2610
}

void draw_pulirula(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // OBJECT
   // ------

   render_tc0200obj_mapped_pulirula();

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);
}
