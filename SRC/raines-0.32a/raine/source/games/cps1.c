/*****************************************************************************/
/*                                                                           */
/*                   CAPCOM SYSTEM 1 / CPS1 (C) 1990 CAPCOM                  */
/* Based on the mame source, but lots of things were rewritten...            */
/* Thnaks to the mame team to show how all this works anyway !               */
/* See cps1drv.c for the games related data...                               */
/*****************************************************************************/

#include "gameinc.h"
#include "cps1.h"
#include "taitosnd.h"
#include "mame/eeprom.h"
#include "savegame.h"
#include "sasound.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

/* Output ports */
#define CPS1_OBJ_BASE		0x00    /* Base address of objects */
#define CPS1_SCROLL1_BASE       0x01    /* Base address of scroll 1 */
#define CPS1_SCROLL2_BASE       0x02    /* Base address of scroll 2 */
#define CPS1_SCROLL3_BASE       0x03    /* Base address of scroll 3 */
#define CPS1_OTHER_BASE		0x04    /* Base address of other video */
#define CPS1_PALETTE_BASE       0x05    /* Base address of palette */
#define CPS1_SCROLL1_SCROLLX    0x06    /* Scroll 1 X */
#define CPS1_SCROLL1_SCROLLY    0x07    /* Scroll 1 Y */
#define CPS1_SCROLL2_SCROLLX    0x08    /* Scroll 2 X */
#define CPS1_SCROLL2_SCROLLY    0x09    /* Scroll 2 Y */
#define CPS1_SCROLL3_SCROLLX    0x0a    /* Scroll 3 X */
#define CPS1_SCROLL3_SCROLLY    0x0b    /* Scroll 3 Y */
#define CPS1_STARS1_SCROLLX     0x0c    /* Stars 1 X */
#define CPS1_STARS1_SCROLLY     0x0d    /* Stars 1 Y */
#define CPS1_STARS2_SCROLLX     0x0e    /* Stars 2 X */
#define CPS1_STARS2_SCROLLY     0x0f    /* Stars 2 Y */

#define CPS1_ROWSCROLL_OFFS     0x10    /* base of row scroll offsets in other RAM */

#define CPS1_SCROLL2_WIDTH      0x40
#define CPS1_SCROLL2_HEIGHT     0x40

#define qsound_rom (Z80RAM+0x4000)
#define qsound_decode (Z80RAM+0xc000)

const int srcwidth = CPS1_SCROLL2_WIDTH * 0x10;
const int srcheight = CPS1_SCROLL2_HEIGHT * 0x10;

const int cps1_scroll1_size=0x4000;
const int cps1_scroll2_size=0x4000;
const int cps1_scroll3_size=0x4000;
const int cps1_obj_size    =0x0800;
const int cps1_other_size  =0x0800;
#define cps1_palette_entries (32*8)  /* Number colour schemes in palette */
const int cps1_palette_size=cps1_palette_entries*32; /* Size of palette RAM */

static UINT8 *RAM_SPR,*Z80RAM,*qsound_sharedram1,*qsound_sharedram2;
static UINT8 *GFX_SPR,*GFX_SPR16,*GFX_SPR32;
static UINT8 *GFX_SPR_SOLID;
static UINT8 *GFX_SPR_SOLID16,*GFX_SPR_SOLID32;
static UINT8 *cps1_gfxram,*cps1_palette;
static UINT8 *cps1_buffered_obj;
static int cps1_last_sprite_offset;     /* Offset of the last sprite */
static int cps1_layer_enabled[4];       /* Layer enabled [Y/N] */
static int cps1_stars_enabled;          /* Layer enabled [Y/N] */
static int cps1_flip_screen;    /* Flip screen on / off */
static int base2,base1,base3,scrwidth,scrheight,spacechar;
static int scrlx,scrly,kludge,size_code2;
static UINT32 max_sprites,max_sprites32,max_sprites8;

/********************************************************************
*
*  EEPROM
*  ======
*
*   The EEPROM is accessed by a serial protocol using the register
*   0xf1c006
*
********************************************************************/

static struct EEPROM_interface qsound_eeprom_interface =
{
	7,		/* address bits */
	8,		/* data bits */
	"0110",	/*  read command */
	"0101",	/* write command */
	"0111"	/* erase command */
};

static struct EEPROM_interface pang3_eeprom_interface =
{
	6,		/* address bits */
	16,		/* data bits */
	"0110",	/*  read command */
	"0101",	/* write command */
	"0111"	/* erase command */
};

UINT16 cps1_eeprom_port_r(UINT32 offset)
{
  return EEPROM_read_bit();
}

void cps1_eeprom_port_w(UINT32 offset,UINT16 data)
{
  /*
    bit 0 = data
    bit 6 = clock
    bit 7 = cs
  */
  EEPROM_write_bit(data & 0x01);
  EEPROM_set_cs_line((data & 0x80) ? CLEAR_LINE : ASSERT_LINE);
  EEPROM_set_clock_line((data & 0x40) ? ASSERT_LINE : CLEAR_LINE);
}

// Z80 Bankswitch

#define MAX_BANKS 0x20

static UINT8 *ROM_BANK,*bank[MAX_BANKS];
static UINT16 nb_banks,current_bank;

void init_banks(UINT8 *rombase) 
{
  UINT32 size,n;
  nb_banks = (get_region_size(REGION_ROM2) - 0x8000)/0x4000;
  size = nb_banks * (0x8000 + 0x4000); // must copy the rom for each bank
  if (nb_banks > MAX_BANKS) {
    allegro_message("nb_banks (%d) > MAX_BANKS",nb_banks);
    exit(1);
  }

  ROM_BANK = AllocateMem(size);
  if (!ROM_BANK) return;
  
  for (n=0; n<nb_banks; n++) {
    UINT8 *dst = ROM_BANK + n * 0xc000;
    bank[n] = dst;
    memcpy(dst, rombase, 0x8000); // ROM
    memcpy(dst+0x8000, Z80ROM+0x8000+(n*0x4000), 0x4000); // bank
  }
  
  current_bank = -1;
}

void cps1_set_bank(UINT16 offset, UINT8 data)
{
  if (data != current_bank && data < nb_banks) {
    current_bank = data;
    Z80ASetBank(bank[data]);
  } 
}

// Sound

static UINT8 cps1_sound_fade_timer,latch;

static void qsound_banksw_w(UINT16 offset,UINT8 data)
{
  data &= 0xf;
  
  if (data != current_bank && data < nb_banks) {
    current_bank = data;
    Z80ASetBank(bank[data]);
  }  
}


/******************************************************************************/
/*                                                                            */
/*                        PROTECTION DEVICES KTNXMAME                         */
/*                                                                            */
/******************************************************************************/

/* Game specific data */
/* Game specific data */
struct CPS1config
{
	char *name;             /* game driver name */

	/* Some games interrogate a couple of registers on bootup. */
	/* These are CPS1 board B self test checks. They wander from game to */
	/* game. */
	int cpsb_addr;        /* CPS board B test register address */
	int cpsb_value;       /* CPS board B test register expected value */

	/* some games use as a protection check the ability to do 16-bit multiplies */
	/* with a 32-bit result, by writing the factors to two ports and reading the */
	/* result from two other ports. */
	/* It looks like this feature was introduced with 3wonders (CPSB ID = 08xx) */
	int mult_factor1;
	int mult_factor2;
	int mult_result_lo;
	int mult_result_hi;

	int layer_control;
	int priority0;
	int priority1;
	int priority2;
	int priority3;
	int control_reg;  /* Control register? seems to be always 0x3f */

	/* ideally, the layer enable masks should consist of only one bit, */
	/* but in many cases it is unknown which bit is which. */
	int scrl1_enable_mask;
	int scrl2_enable_mask;
	int scrl3_enable_mask;
	int stars_enable_mask;

	int bank_scroll1;
	int bank_scroll2;
	int bank_scroll3;

	/* Some characters aren't visible */
	const int start_scroll2;
	const int end_scroll2;
	const int start_scroll3;
	const int end_scroll3;

	int kludge;  /* Ghouls n Ghosts sprite kludge */
};

struct CPS1config *cps1_game_config;

/*                 CPSB ID    multiply protection  ctrl    priority masks  unknwn     layer enable    */
#define CPS_B_01 0x00,0x0000, 0,0,0,0, /* n/a */   0x66,0x68,0x6a,0x6c,0x6e,0x70, 0x02,0x04,0x08,0x30
#define UNKNW_02 0x00,0x0000, 0,0,0,0, /* n/a */   0x6c,0x6a,0x68,0x66,0x64,0x62, 0x02,0x04,0x08,0x00
#define UNKNW_03 0x00,0x0000, 0,0,0,0, /* n/a */   0x70,0x6e,0x6c,0x6a,0x68,0x66, 0x20,0x10,0x08,0x00
#define CPS_B_04 0x60,0x0004, 0,0,0,0, /* n/a */   0x6e,0x66,0x70,0x68,0x72,0x6a, 0x02,0x0c,0x0c,0x00
#define CPS_B_05 0x60,0x0005, 0,0,0,0, /* n/a */   0x68,0x6a,0x6c,0x6e,0x70,0x72, 0x02,0x08,0x20,0x14
#define CPS_B_11 0x00,0x0000, 0,0,0,0, /* n/a */   0x66,0x68,0x6a,0x6c,0x6e,0x70, 0x20,0x10,0x08,0x00
#define CPS_B_12 0x60,0x0402, 0,0,0,0, /* n/a */   0x6c,0x00,0x00,0x00,0x00,0x62, 0x02,0x04,0x08,0x00
#define CPS_B_13 0x6e,0x0403, 0,0,0,0, /* n/a */   0x62,0x64,0x66,0x68,0x6a,0x6c, 0x20,0x04,0x02,0x00
#define CPS_B_14 0x5e,0x0404, 0,0,0,0, /* n/a */   0x52,0x54,0x56,0x58,0x5a,0x5c, 0x08,0x30,0x30,0x00
#define CPS_B_15 0x4e,0x0405, 0,0,0,0, /* n/a */   0x42,0x44,0x46,0x48,0x4a,0x4c, 0x04,0x22,0x22,0x00
#define CPS_B_16 0x40,0x0406, 0,0,0,0, /* n/a */   0x4c,0x4a,0x48,0x46,0x44,0x42, 0x10,0x0a,0x0a,0x00
#define CPS_B_17 0x48,0x0407, 0,0,0,0, /* n/a */   0x54,0x52,0x50,0x4e,0x4c,0x4a, 0x08,0x10,0x02,0x00
#define CPS_B_18 0xd0,0x0408, 0,0,0,0, /* n/a */   0xdc,0xda,0xd8,0xd6,0xd4,0xd2, 0x10,0x0a,0x0a,0x00
#define NOBATTRY 0x00,0x0000, 0x40,0x42,0x44,0x46, 0x66,0x68,0x6a,0x6c,0x6e,0x70, 0x02,0x04,0x08,0x00
#define BATTRY_1 0x72,0x0800, 0x4e,0x4c,0x4a,0x48, 0x68,0x66,0x64,0x62,0x60,0x70, 0x20,0x04,0x08,0x12
#define BATTRY_2 0x00,0x0000, 0x5e,0x5c,0x5a,0x58, 0x60,0x6e,0x6c,0x6a,0x68,0x70, 0x30,0x08,0x30,0x00
#define BATTRY_3 0x00,0x0000, 0x46,0x44,0x42,0x40, 0x60,0x6e,0x6c,0x6a,0x68,0x70, 0x20,0x12,0x12,0x00
#define BATTRY_4 0x00,0x0000, 0x46,0x44,0x42,0x40, 0x68,0x66,0x64,0x62,0x60,0x70, 0x20,0x10,0x02,0x00
#define BATTRY_5 0x00,0x0000, 0x00,0x00,0x00,0x00, 0x6e,0x66,0x70,0x68,0x72,0x6a, 0x02,0x0c,0x0c,0x00
#define BATTRY_6 0x00,0x0000, 0x4e,0x4c,0x4a,0x48, 0x60,0x6e,0x6c,0x6a,0x68,0x70, 0x20,0x06,0x06,0x00
#define BATTRY_7 0x00,0x0000, 0x00,0x00,0x00,0x00, 0x60,0x6e,0x6c,0x6a,0x68,0x70, 0x20,0x14,0x14,0x00
#define BATTRY_8 0x00,0x0000, 0x00,0x00,0x00,0x00, 0x6c,0x00,0x00,0x00,0x00,0x52, 0x14,0x02,0x14,0x00
#define QSOUND_1 0x00,0x0000, 0x00,0x00,0x00,0x00, 0x62,0x64,0x66,0x68,0x6a,0x6c, 0x10,0x08,0x04,0x00
#define QSOUND_2 0x00,0x0000, 0x00,0x00,0x00,0x00, 0x4a,0x4c,0x4e,0x40,0x42,0x44, 0x16,0x16,0x16,0x00
#define QSOUND_3 0x4e,0x0c00, 0x00,0x00,0x00,0x00, 0x52,0x54,0x56,0x48,0x4a,0x4c, 0x04,0x02,0x20,0x00
#define QSOUND_4 0x6e,0x0c01, 0x00,0x00,0x00,0x00, 0x56,0x40,0x42,0x68,0x6a,0x6c, 0x04,0x08,0x10,0x00
#define QSOUND_5 0x5e,0x0c02, 0x00,0x00,0x00,0x00, 0x6a,0x6c,0x6e,0x70,0x72,0x5c, 0x04,0x08,0x10,0x00


static struct CPS1config cps1_config_table[]=
{
	/* name       CPSB    banks        tile limits            kludge */
	{"forgottn",CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 7 },
	{"lostwrld",CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 7 },
	{"ghouls",  CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 1 },
	{"ghoulsu", CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 1 },
	{"daimakai",CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 1 },
	{"strider", CPS_B_01, 1,0,1, 0x0000,0xffff,0x0000,0xffff },
	{"striderj",CPS_B_01, 1,0,1, 0x0000,0xffff,0x0000,0xffff },
	{"stridrja",CPS_B_01, 1,0,1, 0x0000,0xffff,0x0000,0xffff },
	{"dwj",     UNKNW_02, 0,1,1, 0x0000,0xffff,0x0000,0xffff },
	{"willow",  UNKNW_03, 0,1,0, 0x0000,0xffff,0x0000,0xffff },
	{"willowj", UNKNW_03, 0,1,0, 0x0000,0xffff,0x0000,0xffff },
	{"unsquad", CPS_B_11, 0,0,0, 0x0000,0xffff,0x0001,0xffff },
	{"area88",  CPS_B_11, 0,0,0, 0x0000,0xffff,0x0001,0xffff },
	{"ffight",  CPS_B_04, 0,0,0, 0x0001,0xffff,0x0001,0xffff },
	{"ffightu", CPS_B_01, 0,0,0, 0x0001,0xffff,0x0001,0xffff },
	{"ffightj", CPS_B_04, 0,0,0, 0x0001,0xffff,0x0001,0xffff },
	{"1941",    CPS_B_05, 0,0,0, 0x0000,0xffff,0x0400,0x07ff },
	{"1941j",   CPS_B_05, 0,0,0, 0x0000,0xffff,0x0400,0x07ff },
	{"mercs",   CPS_B_12, 0,0,0, 0x0600,0x5bff,0x0700,0x17ff, 4 },	/* (uses port 74) */
	{"mercsu",  CPS_B_12, 0,0,0, 0x0600,0x5bff,0x0700,0x17ff, 4 },	/* (uses port 74) */
	{"mercsj",  CPS_B_12, 0,0,0, 0x0600,0x5bff,0x0700,0x17ff, 4 },	/* (uses port 74) */
	{"msword",  CPS_B_13, 0,0,0, 0x2800,0x37ff,0x0000,0xffff },	/* CPSB ID not checked, but it's the same as sf2j */
	{"mswordu", CPS_B_13, 0,0,0, 0x2800,0x37ff,0x0000,0xffff },	/* CPSB ID not checked, but it's the same as sf2j */
	{"mswordj", CPS_B_13, 0,0,0, 0x2800,0x37ff,0x0000,0xffff },	/* CPSB ID not checked, but it's the same as sf2j */
	{"mtwins",  CPS_B_14, 0,0,0, 0x0000,0x3fff,0x0e00,0xffff },
	{"chikij",  CPS_B_14, 0,0,0, 0x0000,0x3fff,0x0e00,0xffff },
	{"nemo",    CPS_B_15, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"nemoj",   CPS_B_15, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"cawing",  CPS_B_16, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"cawingj", CPS_B_16, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"sf2",     CPS_B_17, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ua",   CPS_B_17, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ub",   CPS_B_17, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ue",   CPS_B_18, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ui",   CPS_B_14, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2j",    CPS_B_13, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ja",   CPS_B_17, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2jc",   CPS_B_12, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	/* from here onwards the CPS-B board has suicide battery and multiply protection */
	{"3wonders",BATTRY_1, 0,1,1, 0x0000,0xffff,0x0000,0xffff, 2 },
	{"3wonderu",BATTRY_1, 0,1,1, 0x0000,0xffff,0x0000,0xffff, 2 },
	{"wonder3", BATTRY_1, 0,1,1, 0x0000,0xffff,0x0000,0xffff, 2 },
	{"kod",     BATTRY_2, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"kodu",    BATTRY_2, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"kodj",    BATTRY_2, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"kodb",    BATTRY_2, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* bootleg, doesn't use multiply protection */
	{"captcomm",BATTRY_3, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"captcomu",BATTRY_3, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"captcomj",BATTRY_3, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"knights", BATTRY_4, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 3 },
	{"knightsu",BATTRY_4, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 3 },
	{"knightsj",BATTRY_4, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 3 },
	{"sf2ce",   NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ceua", NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2ceub", NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2cej",  NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2rb",   NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2rb2",  NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2red",  NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2v004", NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2accp2",NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"varth",   BATTRY_5, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* CPSB test has been patched out (60=0008) */
	{"varthu",  BATTRY_5, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* CPSB test has been patched out (60=0008) */
	{"varthj",  BATTRY_6, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* CPSB test has been patched out (72=0001) */
	{"cworld2j",BATTRY_7, 0,0,0, 0x0000,0xffff,0x0000,0xffff },  /* The 0x76 priority values are incorrect values */
	{"wof",     CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* bootleg? */
	{"wofa",    CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* bootleg? */
	{"wofu",    QSOUND_1, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"wofj",    QSOUND_1, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"dino",    QSOUND_2, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* layer enable never used */
	{"dinoj",   QSOUND_2, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* layer enable never used */
	{"punisher",QSOUND_3, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"punishru",QSOUND_3, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"punishrj",QSOUND_3, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"slammast",QSOUND_4, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"slammasu",QSOUND_4, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"mbomberj",QSOUND_4, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"mbombrd", QSOUND_5, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"mbombrdj",QSOUND_5, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"sf2t",    NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"sf2tj",   NOBATTRY, 2,2,2, 0x0000,0xffff,0x0000,0xffff },
	{"qad",     BATTRY_8, 0,0,0, 0x0000,0xffff,0x0000,0xffff },	/* TODO: layer enable */
	{"qadj",    NOBATTRY, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"qtono2",  NOBATTRY, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"megaman", CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"rockmanj",CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"pnickj",  CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	{"pang3",   CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 5 },	/* EEPROM port is among the CPS registers */
	{"pang3j",  CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff, 5 },	/* EEPROM port is among the CPS registers */
	#ifdef MESS
	{"sfzch",   CPS_B_01, 0,0,0, 0x0000,0xffff,0x0000,0xffff },
	#endif

    /* CPS2 games */
	{"cps2",    NOBATTRY, 4,4,4, 0x0000,0xffff,0x0000,0xffff },
	{"ssf2",    NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff },
	{"ssf2a",   NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff },
	{"ssf2j",   NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff },
	{"ssf2jr1", NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff },
	{"ssf2jr2", NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff },
	{"ssf2t",   NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff, 9 },
	{"ssf2tu",  NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff, 9 },
	{"ssf2ta",  NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff, 9 },
	{"ssf2xj",  NOBATTRY, 4,4,0, 0x0000,0xffff,0x0000,0xffff, 9 },
	{"xmcota",  NOBATTRY, 4,4,4, 0x0000,0xffff,0x0000,0xffff, 8 },
	{"xmcotaj", NOBATTRY, 4,4,4, 0x0000,0xffff,0x0000,0xffff, 8 },
	{"xmcotaj1",NOBATTRY, 4,4,4, 0x0000,0xffff,0x0000,0xffff, 8 },
	{0}		/* End of table */
};

static UINT16 cps1_output[0x100],scroll2x,scroll2y;

static void cps1_find_last_sprite(void)    /* Find the offset of last sprite */
{
    int offset=0;
	/* Locate the end of table marker */
    while (offset < cps1_obj_size)
	{
        int colour=ReadWord(&cps1_buffered_obj[offset+6]);
	
		if (colour == 0xff00)
		{
			/* Marker found. This is the last sprite. */
            cps1_last_sprite_offset=offset-8;
			return;
		}
        offset+=8;
	}
	/* Sprites must use full sprite RAM */
    cps1_last_sprite_offset=cps1_obj_size-8;
}

INLINE UINT8 *cps1_base(int offset,int boundary)
{
	int base=cps1_output[offset]*256;
	/*
	The scroll RAM must start on a 0x4000 boundary.
	Some games do not do this.
	For example:
	   Captain commando     - continue screen will not display
	   Muscle bomber games  - will animate garbage during gameplay
	Mask out the irrelevant bits.
	*/
	base &= ~(boundary-1);
 	return &cps1_gfxram[(base&0x3ffff)];
}

static UINT16 oldx,oldy,oldx2,oldy2;

static void cps1_init_machine(void)
{
   struct CPS1config *pCFG=&cps1_config_table[0];
   memset(input_buffer,0xff,0x20);   
   cps1_sound_fade_timer = latch = 0xff;
   size_code2 = get_region_size(REGION_ROM2);
   
   oldx2 = oldx = 0xffff; // To have scroll1x != oldx
   
   while(pCFG->name)
   {
      if (is_current_game(pCFG->name))
      {
         break;
      }
      pCFG++;
   }
   cps1_game_config=pCFG;

   kludge=cps1_game_config->kludge;
   
   if (kludge == 7) // forgottn has digital input
     GameMouse=1;

   if (is_current_game("sf2rb"))
   {
      /* Patch out protection check */
      UINT16 *rom = (UINT16 *)load_region[REGION_ROM1];
      WriteWord68k(&rom[0xe5464/2], 0x6012);
   }

   if (is_current_game("sf2accp2"))
   {
      /* Patch out a odd branch which would be incorrectly interpreted
         by the cpu core as a 32-bit branch. This branch would make the
         game crash (address error, since it would branch to an odd address)
         if location 180ca6 (outside ROM space) isn't 0. Protection check? */

      UINT16 *rom = (UINT16 *)load_region[REGION_ROM1];
      WriteWord68k(&rom[0x11756/2],0x4e71);
   }

   base2 = cps1_game_config->bank_scroll2*0x04000;
   base1 = cps1_game_config->bank_scroll1*0x08000;
   base3 = cps1_game_config->bank_scroll3*0x01000;

   /* knights; the real space is 0x8820 */
   if (kludge == 3)
     spacechar = 0xf020;
   else spacechar = 0x20;
   spacechar += base1;
   /* The spacechar case is a weird case of lazyness : scroll1 is sometimes */
   /* filled with a sprite not drawn : spacechar. For most games, this */
   /* sprite is empty and is detected by GFX_SPR_SOLID. But for some like */
   /* strider, it really contains something !!! */

   scrwidth = current_game->video_info->screen_x+32;
   scrheight= current_game->video_info->screen_y+32;
   /* Put in some defaults */
   /* Apparently some games do not initialise completely and need these */
   /* defaults (captcomm)  */
   cps1_output[CPS1_OBJ_BASE]     = 0x9200;
   cps1_output[CPS1_SCROLL1_BASE] = 0x9000;
   cps1_output[CPS1_SCROLL2_BASE] = 0x9040;
   cps1_output[CPS1_SCROLL3_BASE] = 0x9080;
   cps1_output[CPS1_OTHER_BASE]   = 0x9100;
   cps1_output[CPS1_PALETTE_BASE] = 0x90c0;
}


static UINT16 protection_rw(UINT32 offset)
{
#if VERBOSE
  if (offset >= 0x18/2) logerror("PC %06x: read output port %02x\n",cpu_get_pc(),offset*2);
#endif
  /* Some games interrogate a couple of registers on bootup. */
  /* These are CPS1 board B self test checks. They wander from game to */
  /* game. */
  
  if (offset && offset == cps1_game_config->cpsb_addr/2)
    return cps1_game_config->cpsb_value;

   /* some games use as a protection check the ability to do 16-bit multiplies */
   /* with a 32-bit result, by writing the factors to two ports and reading the */
   /* result from two other ports. */
   if (offset && offset == cps1_game_config->mult_result_lo/2)
      return (cps1_output[cps1_game_config->mult_factor1/2] *
            cps1_output[cps1_game_config->mult_factor2/2]) & 0xffff;
   if (offset && offset == cps1_game_config->mult_result_hi/2)
      return (cps1_output[cps1_game_config->mult_factor1/2] *
            cps1_output[cps1_game_config->mult_factor2/2]) >> 16;

   /* Pang 3 EEPROM interface */
   if (kludge == 5 && offset == 0x7a/2)
      return cps1_eeprom_port_r(0);

   return cps1_output[offset];
}

static void protection_ww(UINT32 offset, UINT16 data)
{
   /* Pang 3 EEPROM interface */
   if (kludge == 5 && offset == 0x7a/2)
   {
      cps1_eeprom_port_w(0,data);
      return;
   }

   cps1_output[offset] = data;
   //data = COMBINE_DATA(&cps1_output[offset]);

#ifdef MAME_DEBUG
if (cps1_game_config->control_reg && offset == cps1_game_config->control_reg/2 && data != 0x3f)
   usrintf_showmessage("control_reg = %04x",data);
#endif
#if VERBOSE
if (offset >= 0x22/2 &&
      offset != cps1_game_config->layer_control/2 &&
      offset != cps1_game_config->priority0/2 &&
      offset != cps1_game_config->priority1/2 &&
      offset != cps1_game_config->priority2/2 &&
      offset != cps1_game_config->priority3/2 &&
      offset != cps1_game_config->control_reg/2)
   logerror("PC %06x: write %02x to output port %02x\n",cpu_get_pc(),data,offset*2);

#ifdef MAME_DEBUG
if (offset == 0x22/2 && (data & ~0x8001) != 0x0e)
   usrintf_showmessage("port 22 = %02x",data);
if (cps1_game_config->priority0 && offset == cps1_game_config->priority0/2 && data != 0x00)
   usrintf_showmessage("priority0 %04x",data);
#endif
#endif
}

static int dial[2]; // forgottn stuff

static void cps1_ioc_wb(UINT32 offset, UINT8 data)
{
   offset &= 0x1FF;
   if (offset == 0x189) {
     //fprintf(stderr,"sound_fade time %x\n",data);
     cps1_sound_fade_timer = data;
   } else if (offset == 0x181) {
     //fprintf(stderr,"latch %x\n",data);
     latch = data;
   }
}

static void cps1_ioc_ww(UINT32 offset, UINT16 data)
{
   offset &= 0x1FF;
   if (offset == 0x188) {
     cps1_sound_fade_timer = data & 0xff;
   } else if (offset == 0x180) {
     //fprintf(stderr,"latch %x\n",data);
     latch = data & 0xff;
   } else if (offset >= 0x100)
      protection_ww((offset - 0x100)>>1, data);
   else if (offset == 0x40) 
     dial[0] = input_buffer[5*2];
   else if (offset == 0x48)
     dial[1] = input_buffer[6*2];
#ifdef RAINE_DEBUG
   else
     print_debug("cps1_ioc_ww unmapped %x\n",offset);
#endif
}

static UINT16 cps1_input2_r(UINT32 offset) 
{
  int buttons=input_buffer[5*2];
  
  return buttons << 8 | buttons;
}

static UINT8 cps1_input2_rb(UINT32 offset) 
{
  return input_buffer[5*2];
}

static UINT16 cps1_input3_r(UINT32 offset) 
{
  int buttons=input_buffer[6*2];
  return buttons << 8 | buttons;
}

static UINT8 cps1_input3_rb(UINT32 offset) 
{
  return input_buffer[6*2];
}

static UINT8 cps1_ioc_rb(UINT32 offset)
{
   offset &= 0x1FF;
   if (offset >= 0x18 && offset <= 0x22) {
     UINT8 input;
     offset = (offset - 0x18) & 0xfe;
     input = input_buffer[offset];
     //fprintf(stderr,"read input %x,%x\n",offset,input);
     
     return input;
   } else if (offset == 0x177 || offset == 0x1fd)
     return input_buffer[10];
   else if (offset >= 0x100) {
     int ret = protection_rw((offset - 0x100)>>1);
     if (offset & 1) return ret>>8;
     else return ret & 0xff;
#ifdef RAINE_DEBUG
   } else {
     print_debug("cps1_ioc_rb unmapped %x\n",offset);
#endif     
   }
   return 0xFF;
}

static UINT16 cps1_ioc_rw(UINT32 offset)
{
   offset &= 0x1FF;
   if(offset < 0x100) {
     if (offset >= 0x18 && offset<=0x22) {
       UINT8 input;
       offset = (offset - 0x18) & 0xfe;
       input = input_buffer[offset];
       return input | (input << 8);
     } else if (offset == 0 || offset == 0x10)
       fprintf(stderr,"port0_rw\n");
     else if (offset == 0x52)
	return (ReadWord(&input_buffer[5*2]) - dial[0]) & 0xff;
     else if (offset == 0x54)
	return ((ReadWord(&input_buffer[5*2]) - dial[0]) >> 8) & 0xff;
     
#ifdef RAINE_DEBUG
   else
     print_debug("cps1_ioc_rw unmapped %x\n",offset);
#endif     
     return 0xFFFF;
   } else
      return protection_rw((offset - 0x100)>>1);
}

void cps1_reset_handler(void)
{
#ifdef RAINE_DEBUG
   print_debug("cps1_reset_handler()\n");
#endif
}

static UINT8 *RAM_SCROLL1,*RAM_SCROLL2,*RAM_SCROLL3;

static int layer_id_data[4];

static char *layer_id_name[4] =
{
   "OBJECT", "SCROLL1", "SCROLL2", "SCROLL3",
};

void finish_conf_cps1()
{
   AddZ80AInit();
   cps1_set_bank(0,0);

   AddMemFetch(-1, -1, NULL);
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);               // <Bad Reads>
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);               // <Bad Reads>
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);             // <Bad Writes>
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);             // <Bad Writes>
   AddRWBW(-1, -1, NULL, NULL);

   AddInitMemory();     // Set Starscream mem pointers...
   RAM_SCROLL1 = NULL; // Init after the 1st frame...
}

static void cps1_set_z80() 
{
   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);

   init_banks(Z80ROM);
   
   AddZ80AReadByte(0x0000, 0xbfFF, NULL, NULL); // Z80 ROM + BANKS
   AddZ80AReadByte(0xd000, 0xd7ff, NULL, Z80RAM);
   AddZ80AReadByte(0xf001, 0xf001, YM2151_status_port_0_r, NULL);
   AddZ80AReadByte(0xf002, 0xf002, OKIM6295_status_0_r, NULL);
   AddZ80AReadByte(0xf008, 0xf008, NULL, &latch);
   AddZ80AReadByte(0xf00a, 0xf00a, NULL, &cps1_sound_fade_timer);
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,               NULL);
   AddZ80AReadByte(    -1,     -1, NULL,                        NULL);

   AddZ80AWriteByte(0xd000, 0xd7FF, NULL,                       Z80RAM);
   //   if (kludge != 7) { // forgottn has a broken music which crashes raine !
     AddZ80AWriteByte(0xf000, 0xf000, YM2151_register_port_0_w,   NULL);
     AddZ80AWriteByte(0xf001, 0xf001, YM2151_data_port_0_w,   NULL);
     //}
   
   AddZ80AWriteByte(0xf002, 0xf002, OKIM6295_data_0_w,   NULL);
   AddZ80AWriteByte(0xf004, 0xf004, cps1_set_bank,   NULL);
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,             NULL);
   AddZ80AWriteByte(    -1,     -1, NULL,                       NULL);
}

static void qsound_set_z80() 
{
  qsound_sharedram1 = Z80RAM;
  qsound_sharedram2 = Z80RAM + 0x1000;
  
  init_banks(qsound_decode);
  
   AddZ80AROMBase(qsound_decode, 0x0038, 0x0066);

   AddZ80AReadByte(0x0000, 0xbfFF, NULL, NULL); // Z80 ROM/RAM

   AddZ80AReadByte(0xc000, 0xcfff, NULL, Z80RAM);
   AddZ80AReadByte(0xd007, 0xd007, qsound_status_r, NULL);
   AddZ80AReadByte(0xf000, 0xffff, NULL, Z80RAM+0x1000);
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,               NULL);
   AddZ80AReadByte(    -1,     -1, NULL,                        NULL);

   AddZ80AWriteByte(0xc000, 0xcfFF, NULL, Z80RAM);
   AddZ80AWriteByte(0xd000, 0xd000, qsound_data_h_w,   NULL);
   AddZ80AWriteByte(0xd001, 0xd001, qsound_data_l_w,   NULL);
   AddZ80AWriteByte(0xd002, 0xd002, qsound_cmd_w,   NULL);
   AddZ80AWriteByte(0xd003, 0xd003, qsound_banksw_w,   NULL);
   AddZ80AWriteByte(0xf000, 0xffff, NULL,   Z80RAM+0x1000);
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,             NULL);
   AddZ80AWriteByte(    -1,     -1, NULL,                       NULL);
}

void load_common(void)
{
   UINT32 ta,size;
   UINT32 *dest;
   int i,j,size_code;
   UINT8 *cps1_gfx;
   
   RAMSize=0x80000+0x10000;
   
   if(!(RAM=AllocateMem(RAMSize))) return;
     
   cps1_gfxram = RAM+0x010000;
   Z80ROM=load_region[REGION_ROM2];
   Z80RAM=RAM+0x70000;

   memset(Z80RAM, 0x00, 0x10000);   

/*      AddTaitoSoundBanking(load_region[REGION_ROM2], */
/*         get_region_size(REGION_ROM2)); */
/*      TaitoSoundSetBank(0,0); */
   // No ports !
   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,           NULL);
   AddZ80AReadPort(  -1,   -1, NULL,                    NULL);

   AddZ80AWritePort(0xAA, 0xAA, StopZ80Mode2,           NULL);
   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,         NULL);
   AddZ80AWritePort(  -1,   -1, NULL,                   NULL);

   RAM_SPR = RAM+0x004000;
   GFX_SPR = load_region[REGION_GFX1];
   size = get_region_size(REGION_GFX1); // size of packed region
   
   cps1_gfx = GFX_SPR;
   
	for (i = 0;i < size/4;i++)
	{
		UINT32 src = cps1_gfx[4*i] + (cps1_gfx[4*i+1]<<8) + (cps1_gfx[4*i+2]<<16) + (cps1_gfx[4*i+3]<<24);
		UINT32 dwval = 0;
		int penusage = 0;

		for (j = 0;j < 8;j++)
		{
			int n = 0;
			UINT32 mask = (0x80808080 >> j) & src;

			if (mask & 0x000000ff) n |= 1;
			if (mask & 0x0000ff00) n |= 2;
			if (mask & 0x00ff0000) n |= 4;
			if (mask & 0xff000000) n |= 8;

			dwval |= n << (j * 4);
			penusage |= 1 << n;
		}
		cps1_gfx[4*i  ] = dwval>>0;
		cps1_gfx[4*i+1] = dwval>>8;
		cps1_gfx[4*i+2] = dwval>>16;
		cps1_gfx[4*i+3] = dwval>>24;
		
	}

	
   GFX_SPR16 = AllocateMem(size*2);
   if (!GFX_SPR16) return;
   
   for(ta=0;ta<size;ta++)
   {
      GFX_SPR[ta] ^= 0xff;
   // Unpack 16x16 sprites (and 32x32)
	  GFX_SPR16[(ta<<1)] = GFX_SPR[ta] & 0xf;
	  GFX_SPR16[(ta<<1)+1] = GFX_SPR[ta] >> 4;
   }

   /* Need to separate 32x32 sprites for rotation... */
   /* This is a terrible waste of memory, but at least it allows more speed */
   GFX_SPR32 = AllocateMem(size*2);
   /* We could avoid copying the whole buffer if we knew where the 32x32 */
   /* sprites are in the buffer. But we know that only once the game has */
   /* started... */
   if (!GFX_SPR32) return;
   memcpy(GFX_SPR32,GFX_SPR16,size*2);
   
   /* rebuild 8x8 sprites */
   GFX_SPR = AllocateMem(size);
   dest= (UINT32*)GFX_SPR;
   
   for (ta=8; ta <size*2; ta+=16) {
     *dest++ = ReadLong(&GFX_SPR16[ta]);
     *dest++ = ReadLong(&GFX_SPR16[ta+4]);
   }
   
   max_sprites = size*2 / 0x100;
   max_sprites8= size / 0x40;
   max_sprites32=max_sprites/4;
   
   GFX_SPR_SOLID = make_solid_mask_8x8(GFX_SPR, max_sprites8);
   GFX_SPR_SOLID16 = make_solid_mask_16x16(GFX_SPR16, max_sprites);
   GFX_SPR_SOLID32 = make_solid_mask_32x32(GFX_SPR32, max_sprites32);
       
   layer_id_data[0] = add_layer_info(layer_id_name[0]);
   layer_id_data[1] = add_layer_info(layer_id_name[1]);
   layer_id_data[2] = add_layer_info(layer_id_name[2]);
   layer_id_data[3] = add_layer_info(layer_id_name[3]);

   memset(RAM+0x00000,0x00,0x80000);

   cps1_init_machine();

/*
 *  StarScream Main 68000 Setup
 */
   size_code = get_region_size(REGION_ROM1);
   
   ByteSwap(ROM, size_code );
   ByteSwap(RAM,0x070000);

   AddResetHandler(&cps1_reset_handler);

   AddMemFetch(0x000000, size_code-1, ROM+0x000000-0x000000);
   AddReadBW(0x000000, size_code-1, NULL, ROM+0x000000);                 // 68000 ROM

   AddReadBW(0x800000, 0x800001, NULL, &input_buffer[4*2]); // port 4 (ctrl)
   AddReadBW(0x800010, 0x800011, NULL, &input_buffer[4*2]); // port 4 (ctrl)

   AddReadWord(0x800176, 0x800177, cps1_input2_r, NULL);
   AddReadWord(0x8001fc, 0x8001fd, cps1_input2_r, NULL);
   AddReadWord(0xf1c000, 0xf1c001, cps1_input2_r, NULL);
   AddReadByte(0xf1c001, 0xf1c001, cps1_input2_rb, NULL);
   AddReadWord(0xf1c002, 0xf1c003, cps1_input3_r, NULL);
   AddReadByte(0xf1c003, 0xf1c003, cps1_input3_rb, NULL);
   
   AddRWBW(0xFF0000, 0xFFFFFF, NULL, RAM+0x000000);                 // 68000 RAM
   AddReadByte(0x800000, 0x8001FF, cps1_ioc_rb, NULL);                  // IOC

   AddReadWord(0x800000, 0x8001FF, cps1_ioc_rw, NULL);                  // IOC
   AddReadWord(0xf1c006, 0xf1c007, cps1_eeprom_port_r, NULL); // eeprom

   AddWriteByte(0x800000, 0x8001FF, cps1_ioc_wb, NULL);                 // IOC
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);                   // Trap Idle 68000

   AddWriteWord(0x800000, 0x8001FF, cps1_ioc_ww, NULL);                 // IOC
   AddWriteWord(0xf1c006, 0xf1c007, cps1_eeprom_port_w, NULL);
}

extern int (*cpupos)(int len);
extern int cputick, onef_cycle, calc_temp,cpu_calc(int);

void load_cps1() 
{
   cpupos=cpu_calc;

   onef_cycle=300000; // 2151 freq
   if (is_current_game("pang3") || is_current_game("pang3j")) {
     // pang3 arrives in a wrong format because of the rom_continue...
     // luckily it's a little rom, we can fix it here with no big overhead...
     UINT8 *tmp;
     int n;
     int size = get_region_size(REGION_GFX1);
     GFX_SPR = load_region[REGION_GFX1];
     tmp = AllocateMem(size);

     for (n=0; n<0x100000/2; n++){
       WriteWord(&tmp[n<<3],ReadWord(&GFX_SPR[n<<1]));
       WriteWord(&tmp[(n<<3)+2],ReadWord(&GFX_SPR[(n<<1)+0x200000]));
       WriteWord(&tmp[(n<<3)+4],ReadWord(&GFX_SPR[(n<<1)+0x100000]));
       WriteWord(&tmp[(n<<3)+6],ReadWord(&GFX_SPR[(n<<1)+0x300000]));
     }
     memcpy(GFX_SPR,tmp,size);
     
     FreeMem(tmp);
     EEPROM_init(&pang3_eeprom_interface);
     load_eeprom();
   }
  load_common();

  cps1_set_z80();
  
  AddMemFetch(0x910000, 0x92ffff, cps1_gfxram+0x010000-0x000000);
  AddRWBW(0x900000, 0x92FFFF, NULL, cps1_gfxram);
  finish_conf_cps1();
}

// Deocding stuff for qsound... Thanks mame team !

static int bitswap1(int src,int key,int select)
{
  if (select & (1 << ((key >> 0) & 7)))
    src = (src & 0xfc) | ((src & 0x01) << 1) | ((src & 0x02) >> 1);
  if (select & (1 << ((key >> 4) & 7)))
    src = (src & 0xf3) | ((src & 0x04) << 1) | ((src & 0x08) >> 1);
  if (select & (1 << ((key >> 8) & 7)))
    src = (src & 0xcf) | ((src & 0x10) << 1) | ((src & 0x20) >> 1);
  if (select & (1 << ((key >>12) & 7)))
    src = (src & 0x3f) | ((src & 0x40) << 1) | ((src & 0x80) >> 1);
  
  return src;
}

static int bitswap2(int src,int key,int select)
{
  if (select & (1 << ((key >>12) & 7)))
    src = (src & 0xfc) | ((src & 0x01) << 1) | ((src & 0x02) >> 1);
  if (select & (1 << ((key >> 8) & 7)))
    src = (src & 0xf3) | ((src & 0x04) << 1) | ((src & 0x08) >> 1);
  if (select & (1 << ((key >> 4) & 7)))
    src = (src & 0xcf) | ((src & 0x10) << 1) | ((src & 0x20) >> 1);
  if (select & (1 << ((key >> 0) & 7)))
    src = (src & 0x3f) | ((src & 0x40) << 1) | ((src & 0x80) >> 1);
  
  return src;
}

static int bytedecode(int src,int swap_key1,int swap_key2,int xor_key,int select)
{
  src = bitswap1(src,swap_key1 & 0xffff,select & 0xff);
  src = ((src & 0x7f) << 1) | ((src & 0x80) >> 7);
  src = bitswap2(src,swap_key1 >> 16,select & 0xff);
  src ^= xor_key;
  src = ((src & 0x7f) << 1) | ((src & 0x80) >> 7);
  src = bitswap2(src,swap_key2 & 0xffff,select >> 8);
  src = ((src & 0x7f) << 1) | ((src & 0x80) >> 7);
  src = bitswap1(src,swap_key2 >> 16,select >> 8);
  return src;
}

static void kabuki_decode(unsigned char *src,unsigned char *dest_op,unsigned char *dest_data,
		int base_addr,int length,int swap_key1,int swap_key2,int addr_key,int xor_key)
{
  int A;
  int select;

  for (A = 0;A < length;A++)
    {
      /* decode opcodes */
      select = (A + base_addr) + addr_key;
      dest_op[A] = bytedecode(src[A],swap_key1,swap_key2,xor_key,select);
      
      /* decode data */
      select = ((A + base_addr) ^ 0x1fc0) + addr_key + 1;
      dest_data[A] = bytedecode(src[A],swap_key1,swap_key2,xor_key,select);
    }
}

static void cps1_decode(int swap_key1,int swap_key2,int addr_key,int xor_key)
{
  unsigned char *rom = load_region[REGION_ROM2];
  unsigned char *backup = qsound_rom;
  
  /* the main CPU can read the ROM and checksum it to verify that it hasn't been */
  /* replaced with a decrypted one. */
  if (backup)
      memcpy(backup,rom,0x8000);
  
  kabuki_decode(rom,qsound_decode,rom,0x0000,0x8000, swap_key1,swap_key2,addr_key,xor_key);
}

static UINT16 qsound_rom_r(UINT32 offset) 
{
  offset -= 0xf00000;
  
  return ReadByte(qsound_rom+(offset>>1)) | 0xff00;
}

static UINT16 qsound_sharedram1_r(UINT32 offset)
{
  int ret;
  offset &= 0x1fff;
  
  ret  = qsound_sharedram1[offset>>1] | 0xff00;
  return ret;

}

static UINT8 qsound_sharedram1_rb(UINT32 offset)
{
  int ret;
  offset &= 0x1fff;

/*    if (offset != 0x1fff) */
/*      fprintf(stderr,"rb %x\n",offset); */
  ret = qsound_sharedram1[offset>>1];
  return ret;
}

static void qsound_sharedram1_w(UINT32 offset, UINT16 data)
{
  offset &= 0x1fff;
  //fprintf(stderr,"sound cmd %x\n",data);
  qsound_sharedram1[offset>>1] = data & 0xff;
}

static void qsound_sharedram1_wb(UINT32 offset, UINT8 data)
{
  offset &= 0x1fff;
/*    if (data != 0xff)  */
/*      fprintf(stderr,"sound cmdb %x\n",data); */
  qsound_sharedram1[offset>>1] = data;
}

static UINT16 qsound_sharedram2_r(UINT32 offset)
{
  offset &= 0x1fff;
  //fprintf(stderr,"rw2\n");
  
  return qsound_sharedram2[offset>>1] | 0xff00;
}

static UINT8 qsound_sharedram2_rb(UINT32 offset)
{
  offset &= 0x1fff;
  return qsound_sharedram2[offset>>1];
}

static void qsound_sharedram2_w(UINT32 offset, UINT16 data)
{
  offset &= 0x1fff;
  //fprintf(stderr,"sound cmd %x\n",data);
  
  qsound_sharedram2[offset>>1] = data & 0xff;
}

static void qsound_sharedram2_wb(UINT32 offset, UINT8 data)
{
  offset &= 0x1fff;
  //fprintf(stderr,"sound cmdb %x\n",data);
  qsound_sharedram2[offset>>1] = data;
}

void load_qsound() 
{
  load_common();

  if (!strncmp(current_game->main_name,"wof",3)) 
    cps1_decode(0x01234567,0x54163072,0x5151,0x51);
  else if (!strncmp(current_game->main_name,"dino",4)) 
    cps1_decode(0x76543210,0x24601357,0x4343,0x43);
  else if (!strncmp(current_game->main_name,"slammas",7))
    cps1_decode(0x54321076,0x65432107,0x3131,0x19);
  else if (!strncmp(current_game->main_name,"punish",6)) 
    cps1_decode(0x67452103,0x75316024,0x2222,0x22);

  qsound_set_z80();
  EEPROM_init(&qsound_eeprom_interface);
  load_eeprom();

  AddRWBW(0x900000, 0x92FFFF, NULL, cps1_gfxram);
  AddReadWord(0xf00000, 0xf0ffff, qsound_rom_r, NULL);

  AddReadWord(0xf18000, 0xf19fff, qsound_sharedram1_r, NULL);
  AddReadByte(0xf18000, 0xf19fff, qsound_sharedram1_rb, NULL);
  AddWriteWord(0xf18000, 0xf19fff, qsound_sharedram1_w, NULL);
  AddWriteByte(0xf18000, 0xf19fff, qsound_sharedram1_wb, NULL);

  AddReadWord(0xf1e000, 0xf1ffff, qsound_sharedram2_r, NULL);
  AddReadByte(0xf1e000, 0xf1ffff, qsound_sharedram2_rb, NULL);
  AddWriteWord(0xf1e000, 0xf1ffff, qsound_sharedram2_w, NULL);
  AddWriteByte(0xf1e000, 0xf1ffff, qsound_sharedram2_wb, NULL);
  
  finish_conf_cps1();
}

void clear_cps1(void)
{
   if (is_current_game("pang3") || is_current_game("pang3j")) {
     save_eeprom();
   }
   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,get_region_size(REGION_ROM1),1);
      save_debug("RAM.bin",RAM,0x080000,1);
   #endif
}

void clear_qsound() 
{
  save_eeprom();
}

#define SLICES 4
#define Z80_FRAME CPU_FRAME_MHz(4,60)/SLICES

void execute_cps1_frame(void)
{
  int n,mx,my;
  
  if (GameMouse) {
    get_mouse_mickeys(&mx,&my);
    WriteWord(&input_buffer[5*2],mx & 0xfff);
    if (mouse_b & 1) input_buffer[8] &= 0xef;
    if (mouse_b & 2) input_buffer[8] &= 0xdf;
  }
  
  cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(10,60));       // Main 68000
   #ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n", s68000context.pc, s68000context.sr);
   #endif
      
   cpu_interrupt(CPU_68K_0, 2);

   if (RaineSoundCard) {
     for (n=0; n< SLICES; n++) {
       cpu_execute_cycles(CPU_Z80_0, Z80_FRAME );        // Sound Z80
#ifdef RAINE_DEBUG
       print_debug("Z80:%04x\n",z80pc);
#endif
       cpu_interrupt(CPU_Z80_0, 0x38);
     }
   }
}

void execute_sf2_frame(void)
{
  int n;
  cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));       // Main 68000
   #ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n", s68000context.pc, s68000context.sr);
   #endif
      if (s68000context.pc > 0x900000){
	//ByteSwap(cps1_gfxram,0x20000);
	save_file("RAM.bin",cps1_gfxram+(s68000context.pc-0x900000),0x100);
	exit(1);
      }
   cpu_interrupt(CPU_68K_0, 2);

   if (RaineSoundCard) {
     for (n=0; n< SLICES; n++) {
       cpu_execute_cycles(CPU_Z80_0, Z80_FRAME );        // Sound Z80
#ifdef RAINE_DEBUG
       print_debug("Z80:%04x\n",z80pc);
#endif
       cpu_interrupt(CPU_Z80_0, 0x38);
     }
   }
}

#undef SLICES
#undef Z80_FRAME
#define SLICES 4
#define Z80_FRAME CPU_FRAME_MHz(4,60)/SLICES

void execute_qsound_frame(void)
{
  cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(10,60));       // Main 68000
   #ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n", s68000context.pc, s68000context.sr);
   #endif
      
   cpu_interrupt(CPU_68K_0, 2);
   
   qsound_sharedram1[0xfff] = 0x77;
   //fprintf(stderr,"%x\n",qsound_sharedram1[0xfff]);
   
#if 0
   for (n=0; n< SLICES; n++) {
     cpu_execute_cycles(CPU_Z80_0, Z80_FRAME );        // Sound Z80
     cpu_interrupt(CPU_Z80_0, 0x38);
   }
#ifdef RAINE_DEBUG
   print_debug("Z80:%04x\n",z80pc);
#endif
#endif
}

static void render_scroll1(int solid) 
{
   int offs,offsy,offsx,sx,sy,code,n,x,y;
   static int scrlxrough,scrlyrough,ytop;
   UINT16 scroll1x = cps1_output[CPS1_SCROLL1_SCROLLX],
     scroll1y = cps1_output[CPS1_SCROLL1_SCROLLY], colour;
   UINT8 *map;

   if (oldx != scroll1x || oldy != scroll1y) {
     scrlxrough=(scroll1x>>3)+4;
     scrlyrough=(scroll1y>>3);
     ytop=-(scroll1y&0x07)+16;

     //fprintf(stderr,"scroll1x %x %x\n",scroll1x,scroll1y);
     oldx = scroll1x;
     oldy = scroll1y;
   }
   
   /* After a bit of fight with the MAKE_SCROLL_xxx macros, I gave up and took the mame method : */
   /* the good old for loops. At least you know where you are everytime !!! */

   /* Here is SCROLL1. Maybe the sprites should be before ? */

   sx=-(scroll1x&0x07);
   
   if (solid) {
     for (x=0; x<0x35; x++)
       {
	 sy=ytop;
	 offsx=((scrlxrough+x)*0x80)&0x1fff;
	 n=scrlyrough;
	 for (y=0; y<0x20; y++,n++)
	   {
	     
	     offsy=( (n&0x1f)*4 | ((n&0x20)*0x100)) & 0x3fff;
	     offs=offsy+offsx;
	     offs &= 0x3fff;
	     
	     code  =ReadWord(&RAM_SCROLL1[offs]) + base1;
	     colour=ReadWord(&RAM_SCROLL1[(offs+2)]);

	     if (code < max_sprites8 &&
	       code != spacechar) {
	       
	       //if (code != 0x5420)
	       //fprintf(stderr,"%d,%d,%x\n",sx,sy,code);
	       MAP_PALETTE_MAPPED_NEW(
				      (colour&0x1F) | 0x20,
				      16,
				      map
				      );
	       
	       Draw8x8_Mapped_flip_Rot(&GFX_SPR[code<<6],sx,sy,map,(colour & 0x60)>>5);
	     } 
	     sy+=8;
	   }
	 sx+=8;
       }
   } else { // normal layer (not solid)
     for (x=0; x<0x35; x++)
       {
	 sy=ytop;
	 offsx=((scrlxrough+x)*0x80)&0x1fff;
	 n=scrlyrough;
	 for (y=0; y<0x20; y++,n++)
	   {
	   
	     offsy=( (n&0x1f)*4 | ((n&0x20)*0x100)) & 0x3fff;
	     offs=offsy+offsx;
	     offs &= 0x3fff;
		
	     code  =ReadWord(&RAM_SCROLL1[offs]) + base1;
	     colour=ReadWord(&RAM_SCROLL1[(offs+2)]);

	     if (code < max_sprites8 && GFX_SPR_SOLID[code] &&
		 code != spacechar) {
	       
	       //if (code != 0x5420)
	       //fprintf(stderr,"%d,%d,%x\n",sx,sy,code);
	       MAP_PALETTE_MAPPED_NEW(
				      (colour&0x1F) | 0x20,
				      16,
				      map
				      );
	     
	       if (GFX_SPR_SOLID[code]==1) // Some pixels transp
		 Draw8x8_Trans_Mapped_flip_Rot(&GFX_SPR[code<<6],sx,sy,map,(colour & 0x60)>>5);
	       else // all solid
		 Draw8x8_Mapped_flip_Rot(&GFX_SPR[code<<6],sx,sy,map,(colour & 0x60)>>5);
	     } 
	     sy+=8;
	   }
	 sx+=8;
       }
   } // if solid
}

static void render_sprites(int solid) 
{
   /* Draw the sprites */
  int i;

  UINT8 *base,*map;

  if (solid) {
    // Sprites layer CAN'T be solid... That's the only case where we must
    // clear the screen explicitly...
    clear_game_screen(0);
  }

  cps1_buffered_obj = cps1_base(CPS1_OBJ_BASE, cps1_obj_size);

  cps1_find_last_sprite();
   base = cps1_buffered_obj + (cps1_last_sprite_offset/8)*8; // Not buffered yet... Is it really necessary?
   /* Why are the sprites in the wrong order ??? */
   /* mame draws them from 0 to the end, and I must do the contrary !!! */
   
   //fprintf(stderr,"render_sprites %x\n",cps1_last_sprite_offset);
   
   for (i=cps1_last_sprite_offset; i>=0; i-=8)
     {
       unsigned int x=ReadWord(base);
       unsigned int y=ReadWord(base+2);
       if (x && y)
	 {
	   unsigned int code=ReadWord(base+4);
	   int colour=ReadWord(base+6);
	   int col=colour&0x1f;
	   // fprintf(stderr,"x %x y %x code %x i %x (%x)\n",x,y,code,i,cps1_last_sprite_offset);
	   //fprintf(stderr,"%d,%d,%x,%x\n",x,y,code,colour);
	   
	   y &= 0x1ff;
	   //if (y > 450) y -= 0x200;
	   
	   /* in cawing, skyscrapers parts on level 2 have all the top bits of the */
	   /* x coordinate set. Does this have a special meaning? */
	   x &= 0x1ff;
	   //if (x > 450) x -= 0x200;
	   
	   x-=0x20;
	   y+=0x10;
	   
	   if (kludge == 7)
	     {
	       code += 0x4000;
	     }
	   if (kludge == 1 && code >= 0x01000)
	     {
	       code += 0x4000;
	     }
	   if (kludge == 2 && code >= 0x02a00)
	     {
	       code += 0x4000;
	     }

	   if (colour & 0xff00 )
	     {
	       /* handle blocked sprites */
	       int nx=(colour & 0x0f00) >> 8;
	       int ny=(colour & 0xf000) >> 12;
	       unsigned int nxs,nys,sx,sy;

	       nx++;
	       ny++;
	   
	       if (colour & 0x40) {
		 /* Y flip */
		 if (colour &0x20){
		   for (nys=0; nys<ny; nys++)
		     {
		       for (nxs=0; nxs<nx; nxs++)
			 {
			   int code2 = (code+(nx-1)-nxs+0x10*(ny-1-nys));
			   sx = (x+nxs*16) & 0x1ff;
			   sy = (y+nys*16) & 0x1ff;
			   if (sx < scrwidth && sy < scrheight && code2 <= max_sprites) {
			     MAP_PALETTE_MAPPED_NEW(
						    col,
						    16,
						    map
						    );
			       
			     Draw16x16_Trans_Mapped_flip_Rot(&GFX_SPR16[code2<<8],sx, sy, map,3);
			   }
			 }
		     }
		 } else
		   for (nys=0; nys<ny; nys++)
		     {
		       for (nxs=0; nxs<nx; nxs++)
			 {
			   int code2 = (code+nxs+0x10*(ny-1-nys));
			   sx = (x+nxs*16) & 0x1ff;
			   sy = (y+nys*16) & 0x1ff;

			   if (sx < scrwidth && sy < scrheight && code2 <= max_sprites) {
			     MAP_PALETTE_MAPPED_NEW(
						    col,
						    16,
						    map
						    );
			       
			     Draw16x16_Trans_Mapped_flip_Rot(&GFX_SPR16[code2<<8],sx,sy,map,2 );
			   }
			 }
		     }
	       }
	       else
		 {
		   if (colour &0x20) // flipy !!!
		     {
		       for (nys=0; nys<ny; nys++)
			 {
			   for (nxs=0; nxs<nx; nxs++)
			     {
			       int code2 = (code+(nx-1)-nxs+0x10*nys);
			       sx = (x+nxs*16) & 0x1ff;
			       sy = (y+nys*16) & 0x1ff;
				 
			       if (sx < scrwidth && sy < scrheight && code2 <= max_sprites) {
				 MAP_PALETTE_MAPPED_NEW(
							col,
							16,
							map
							);
				   
				 Draw16x16_Trans_Mapped_flip_Rot(&GFX_SPR16[code2<<8], sx,sy,map,1);
			       }
			     }
			 } 
		     }
		   else
		     {
		       for (nys=0; nys<ny; nys++)
			 {
			   for (nxs=0; nxs<nx; nxs++)
			     {
			       int code2 = (code+nxs+0x10*nys);
			       sx = (x+nxs*16) & 0x1ff;
			       sy = (y+nys*16) & 0x1ff;

			       if (sx < scrwidth && sy < scrheight && code2 <= max_sprites) {
				 MAP_PALETTE_MAPPED_NEW(
							col,
							16,
							map
							);

				 Draw16x16_Trans_Mapped_flip_Rot(&GFX_SPR16[code2<<8],sx,sy, map,0);
			       }
			     }
			 }
		     }
		 }
	     }
	   else
	     {
	       /* Simple case... 1 sprite does it happen ??? */
	       if (x < scrwidth && y < scrheight && code <= max_sprites) {
		 MAP_PALETTE_MAPPED_NEW(
					col,
					16,
					map
					);

		 Draw16x16_Trans_Mapped_flip_Rot(&GFX_SPR16[code<<8], x, y, map,(colour & 0x60)>>5);
	       }
	     }
	 } // if x && y
       base -= 8;
     } // for i
}

static void cps1_render_scroll2_bitmap(int solid) 
{
  /* Expect lots of trouble with this layer. Mame renders it via a bitmap, */
  /* and we do it directly. It's faster, but there are risks... */
  int sx, sy;
  int ny=(scroll2y>>4);	  /* Rough Y */
  const int startcode=cps1_game_config->start_scroll2;
  const int endcode=cps1_game_config->end_scroll2;
  UINT8 *map;

  if (solid) {
    
    for (sx=CPS1_SCROLL2_WIDTH-1; sx>=0; sx--)
      {
	int n=ny;
      
	for (sy=0x09*2-1; sy>=0; sy--) {
	  int offsy, offsx, offs, colour, code;
	
	  n&=0x3f;
	  offsy  = ((n&0x0f)*4 | ((n&0x30)*0x100))&0x3fff;
	  offsx=(sx*0x040)&0xfff;
	  offs=offsy+offsx;
	
	  colour=ReadWord(&RAM_SCROLL2[offs+2]);
	
	  code=ReadWord(&RAM_SCROLL2[offs]);
	
	  if ( code >= startcode && code <= endcode
	       /*
		 MERCS has an gap in the scroll 2 layout
		 (bad tiles at start of level 2)*/
	       &&	!(kludge == 4 && (code >= 0x1e00 && code < 0x5400))
	       )
	    {
	      code += base2;
	    
	      if (code < max_sprites) {
		int myx = 16*sx - scrlx;
		int myy = 16*n - scrly;

		// We must warp around the bitmap... What a mess...
		if (myx < 0) myx += srcwidth;
		if (myy < 0) myy += srcheight;
	      
		if (myx > 0 && myx <= scrwidth && myy> 0 && myy <= scrheight) {
		
		  MAP_PALETTE_MAPPED_NEW(
					 (colour&0x1F) | 0x40,
					 16,
					 map
					 );
	     
		  Draw16x16_Mapped_flip_Rot(&GFX_SPR16[code<<8],myx,myy,map,(colour & 0x60)>>5);
		}
	      }
	    }
	  n++;
	}
      }
  } else { // normal layer
    for (sx=CPS1_SCROLL2_WIDTH-1; sx>=0; sx--)
      {
	int n=ny;
      
	for (sy=0x09*2-1; sy>=0; sy--) {
	  int offsy, offsx, offs, colour, code;
	
	  n&=0x3f;
	  offsy  = ((n&0x0f)*4 | ((n&0x30)*0x100))&0x3fff;
	  offsx=(sx*0x040)&0xfff;
	  offs=offsy+offsx;
	
	  colour=ReadWord(&RAM_SCROLL2[offs+2]);
	
	  code=ReadWord(&RAM_SCROLL2[offs]);
	
	  if ( code >= startcode && code <= endcode
	       /*
		 MERCS has an gap in the scroll 2 layout
		 (bad tiles at start of level 2)*/
	       &&	!(kludge == 4 && (code >= 0x1e00 && code < 0x5400))
	       )
	    {
	      code += base2;
	    
	      if (code < max_sprites && GFX_SPR_SOLID16[code]) {
		int myx = 16*sx - scrlx;
		int myy = 16*n - scrly;

		// We must warp around the bitmap... What a mess...
		if (myx < 0) myx += srcwidth;
		if (myy < 0) myy += srcheight;
	      
		if (myx > 0 && myx <= scrwidth && myy> 0 && myy <= scrheight) {
		
		  MAP_PALETTE_MAPPED_NEW(
					 (colour&0x1F) | 0x40,
					 16,
					 map
					 );
	     
		  if (GFX_SPR_SOLID16[code]==1) // Some pixels transp
		    Draw16x16_Trans_Mapped_flip_Rot(&GFX_SPR16[code<<8],myx,myy,map,(colour & 0x60)>>5);
		  else // all solid
		    Draw16x16_Mapped_flip_Rot(&GFX_SPR16[code<<8],myx,myy,map,(colour & 0x60)>>5);
		}
	      }
	    }
	  n++;
	}
      }
  } // if (solid)
}
  
static void cps1_render_scroll2_low(int solid)
{
  scroll2x=cps1_output[CPS1_SCROLL2_SCROLLX];
  scroll2y=cps1_output[CPS1_SCROLL2_SCROLLY];
  if (scroll2x != oldx2 || scroll2y != oldy2) {
    // fprintf(stderr,"scroll2x %x scroll2y %x\n",scroll2x,scroll2y);
    oldx2 = scroll2x;
    oldy2 = scroll2y;
    scrly=-(scroll2y-0x20);
    scrlx=-(scroll2x+0x40-0x20);
    if (cps1_flip_screen)
      {
	fprintf(stderr,"fliping screen\n");
	scrly=(CPS1_SCROLL2_HEIGHT*16)-scrly;
      }

    if (scrlx < 0) scrlx = (-scrlx) % srcwidth;
    else scrlx = srcwidth - scrlx % srcwidth;
    
    if (scrly < 0) scrly = (-scrly) % srcheight;
    else scrly = srcheight - (scrly % srcheight);
    scrly += 16;
  }

  cps1_render_scroll2_bitmap(solid);
}

static void render_scroll3(int priority,int solid)
{
  int sx,sy;
  UINT16 scroll3x=cps1_output[CPS1_SCROLL3_SCROLLX];
  UINT16 scroll3y=cps1_output[CPS1_SCROLL3_SCROLLY];
  UINT8 *map;
  int nxoffset=scroll3x&0x1f;
  int nyoffset=scroll3y&0x1f;
  int nx=(scroll3x>>5)+1;
  int ny=(scroll3y>>5)-1;
  const int startcode=cps1_game_config->start_scroll3;
  const int endcode=cps1_game_config->end_scroll3;
  
  if (solid) { // this layer does not render properly when solid...
    clear_game_screen(0);
  } 

  for (sx=1; sx<0x32/4+2; sx++)
    {
      for (sy=1; sy<0x20/4+2; sy++)
	{
	  int offsy, offsx, offs, colour, code;
	  int n;
	  n=ny+sy;
	  offsy  = ((n&0x07)*4 | ((n&0xf8)*0x0100))&0x3fff;
	  offsx=((nx+sx)*0x020)&0x7ff;
	  offs=offsy+offsx;
	  offs &= 0x3fff;
	  
	  code=ReadWord(&RAM_SCROLL3[offs]);
	  
	  if (code >= startcode && code <= endcode)
	    {
	      code+=base3;
	      if (kludge == 2 && code >= 0x01500)
		{
		  code -= 0x1000;
		}
	      else if (kludge == 8 && code >= 0x05800)
		{
		  code -= 0x4000;
		}
	      else if (kludge == 9 && code < 0x05600)
		{
		  code += 0x4000;
		}

	      colour=ReadWord(&RAM_SCROLL3[(offs+2)]);
	      if (code <= max_sprites32) {
		
		if (GFX_SPR_SOLID32[code]) {
		  int myx = 32*sx-nxoffset;
		  int myy = 32*sy-nyoffset-16;
		  if (myx >=0 && myx<=scrwidth && myy>=0 && myy <= scrheight) {
		
	      
		    //if (code != 0x40ff)
		    //fprintf(stderr,"draw layer2 %x %x %x\n",code,sx*16,n*16);
		
		    MAP_PALETTE_MAPPED_NEW(
					   (colour&0x1F) | 0x60,
					   16,
					   map
					   );
	     
		    // _Rot functions are not ready... yet !
		    // fprintf(stderr,"draw %x,%x,%x\n",myx,myy,code);
	    
		    if (GFX_SPR_SOLID32[code]==1) // Some pixels transp
		      Draw32x32_Trans_Mapped_flip_Rot(&GFX_SPR32[code<<10],myx,myy,map,(colour & 0x60)>>5);
		    else // all solid
		      Draw32x32_Mapped_flip_Rot(&GFX_SPR32[code<<10],myx,myy,map,(colour & 0x60)>>5);
		  }
	      
		}
	      }
	      
	    }
	}
    }
}

static int render_layer(int layer,int distort,int solid) 
{
  if (cps1_layer_enabled[layer] && check_layer_enabled(layer_id_data[layer])){
      
    switch(layer) {
    case 0: render_sprites(solid); break;
    case 1: render_scroll1(solid); break;
    case 2: 
#ifdef RAINE_DEBUG
      /*        if (distort) */
      /*  	//cps1_render_scroll2_distort(bitmap); */
      /*  	fprintf(stderr,"distort not supported on layer2\n"); */
#endif
      cps1_render_scroll2_low(solid);
      break;
    case 3: render_scroll3(0,solid); break;
    }
    return 0; // next layer is NOT solid
  }
  return 1;
}

void draw_cps1(void)
{
   int videocontrol=cps1_output[0x11];
   int layercontrol = cps1_output[cps1_game_config->layer_control/2];
   int distort_scroll2 = videocontrol & 0x01;
   int l0 = (layercontrol >> 0x06) & 03,
     l1 = (layercontrol >> 0x08) & 03,
     l2 = (layercontrol >> 0x0a) & 03,
     l3 = (layercontrol >> 0x0c) & 03,
     solid;
   cps1_flip_screen=videocontrol&0x8000;

   if (!RAM_SCROLL1) {
     if (l0 == l1 || l0==l2 || l0==l3) {
#ifdef RAINE_DEBUG
       fprintf(stderr,"l0 id %x %x %x %x\n",l0,l1,l2,l3);
#endif
       return;
     } else if (l1 == l2 || l1==l3){
#ifdef RAINE_DEBUG
       fprintf(stderr,"l1 id %x %x %x %x\n",l0,l1,l2,l3);
#endif
       return;
     } else if (l2 == l3) {
#ifdef RAINE_DEBUG
       fprintf(stderr,"l3 id %x %x %x %x\n",l0,l1,l2,l3);
#endif
       return;
     }
     RAM_SCROLL1 = cps1_base(CPS1_SCROLL1_BASE,cps1_scroll1_size);
     RAM_SCROLL2 = cps1_base(CPS1_SCROLL2_BASE,cps1_scroll2_size);
     RAM_SCROLL3 = cps1_base(CPS1_SCROLL3_BASE,cps1_scroll3_size);
     cps1_palette=cps1_base(CPS1_PALETTE_BASE,cps1_palette_size);
     if (cps1_palette == RAM_SCROLL1 || cps1_palette==RAM_SCROLL2 ||
	 cps1_palette == RAM_SCROLL3 || RAM_SCROLL1 == RAM_SCROLL2
	 || RAM_SCROLL2 == RAM_SCROLL3) {
       RAM_SCROLL1 = NULL;
       return;
     }
     InitPaletteMap(cps1_palette, 0x80, 0x10, 0x10000);

     set_colour_mapper(&col_map_nnnn_rrrr_gggg_bbbb);
     
#ifdef RAINE_DEBUG
     fprintf(stderr,"scroll2 (diff) %x\n",RAM_SCROLL2-cps1_gfxram);
     fprintf(stderr,"scroll3 (diff) %x\n",RAM_SCROLL3-cps1_gfxram);
     fprintf(stderr,"scroll1 (diff) %x\n",RAM_SCROLL1-cps1_gfxram);
     fprintf(stderr,"palette (diff) %x\n",cps1_palette-cps1_gfxram);
#endif
   }

   cps1_layer_enabled[0]=1;
   cps1_layer_enabled[1]=layercontrol & cps1_game_config->scrl1_enable_mask;
   cps1_layer_enabled[2]=layercontrol & cps1_game_config->scrl2_enable_mask;
   cps1_layer_enabled[3]=layercontrol & cps1_game_config->scrl3_enable_mask;
   cps1_stars_enabled   =layercontrol & cps1_game_config->stars_enable_mask;

   ClearPaletteMap();
   
   solid = render_layer(l0,distort_scroll2,1); // solid layer
   solid = render_layer(l1,distort_scroll2,solid);
   solid = render_layer(l2,distort_scroll2,solid);
   render_layer(l3,distort_scroll2,solid);
}

