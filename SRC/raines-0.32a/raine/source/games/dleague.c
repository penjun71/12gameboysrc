/******************************************************************************/
/*                                                                            */
/*                DYNAMITE LEAGUE (C) 1990 TAITO CORPORATION                  */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "dleague.h"
#include "tc004vcu.h"
#include "tc140syt.h"
#include "tc220ioc.h"
#include "taitosnd.h"
#ifdef RAINE_DEBUG
#include "debug.h"
#endif
#include "sasound.h"		// sample support routines

static struct DIR_INFO dynamite_league_dirs[] =
{
   { "dynamite_league", },
   { "dleague", },
   { NULL, },
};

// Each LOAD_8_16 in GFX1 is doubled to make padding...
static struct ROM_INFO dynamite_leaguem_roms[] =
{
  {"c02-19a.33", 0x00020000, 0x7e904e45, REGION_ROM1, 0x000000, LOAD_8_16,   },
  {"c02-21a.36", 0x00020000, 0x18c8a32b, REGION_ROM1, 0x000001, LOAD_8_16,   },
  {"c02-20.34",  0x00010000, 0xcdf593f3, REGION_ROM1, 0x040000, LOAD_8_16,   },
  {"c02-22.37",  0x00010000, 0xf50db2d7, REGION_ROM1, 0x040001, LOAD_8_16,   },

  {"c02-23.40", 0x00010000, 0x5632ee49, REGION_ROM2, 0x000000, LOAD_NORMAL, },

  {"c02-02.15", 0x00080000, 0xb273f854, REGION_GFX1, 0x000000, LOAD_NORMAL, },
  {"c02-06.6",  0x00020000, 0xb8473c7b, REGION_GFX1, 0x080000, LOAD_8_16, },
  {"c02-10.14", 0x00020000, 0x50c02f0f, REGION_GFX1, 0x080001, LOAD_8_16, },
  {"c02-06.6",  0x00020000, 0xb8473c7b, REGION_GFX1, 0x080000, LOAD_8_16, },
  {"c02-10.14", 0x00020000, 0x50c02f0f, REGION_GFX1, 0x080001, LOAD_8_16, },

  {"c02-03.17", 0x00080000, 0xc3fd0dcd, REGION_GFX1, 0x100000, LOAD_NORMAL, },
  {"c02-07.7",  0x00020000, 0x8c1e3296, REGION_GFX1, 0x180000, LOAD_8_16, },
  {"c02-11.16", 0x00020000, 0xfbe548b8, REGION_GFX1, 0x180001, LOAD_8_16, },
  {"c02-06.6",  0x00020000, 0xb8473c7b, REGION_GFX1, 0x080000, LOAD_8_16, },
  {"c02-10.14", 0x00020000, 0x50c02f0f, REGION_GFX1, 0x080001, LOAD_8_16, },
  
  {"c02-24.19", 0x00080000, 0x18ef740a, REGION_GFX1, 0x200000, LOAD_NORMAL, },
  {"c02-08.8",  0x00020000, 0x1a3c2f93, REGION_GFX1, 0x280000, LOAD_8_16, },
  {"c02-12.18", 0x00020000, 0xb1c151c5, REGION_GFX1, 0x280001, LOAD_8_16, },
  {"c02-06.6",  0x00020000, 0xb8473c7b, REGION_GFX1, 0x080000, LOAD_8_16, },
  {"c02-10.14", 0x00020000, 0x50c02f0f, REGION_GFX1, 0x080001, LOAD_8_16, },
  
  {"c02-05.21", 0x00080000, 0xfe3a5179, REGION_GFX1, 0x300000, LOAD_NORMAL, },
  {"c02-09.9",  0x00020000, 0xa614d234, REGION_GFX1, 0x380000, LOAD_8_16, },
  {"c02-13.20", 0x00020000, 0x8eb3194d, REGION_GFX1, 0x380001, LOAD_8_16, },
  {"c02-06.6",  0x00020000, 0xb8473c7b, REGION_GFX1, 0x080000, LOAD_8_16, },
  {"c02-10.14", 0x00020000, 0x50c02f0f, REGION_GFX1, 0x080001, LOAD_8_16, },
  

  {"c02-18.22", 0x00002000, 0xc88f0bbe, REGION_GFX2, 0x000000, LOAD_NORMAL, },

  {"c02-01.31", 0x00080000, 0xd5a3d1aa, REGION_SMP1,0x00000, LOAD_NORMAL, },
  
  {           NULL,          0,          0,           0,        0,           0, },
};

static struct ROM_INFO dynamite_league_roms[] =
{
   {  "dl_ic06.bin", 0x00020000, 0xb8473c7b, 0, 0, 0, },
   {  "dl_ic07.bin", 0x00020000, 0x8c1e3296, 0, 0, 0, },
   {  "dl_ic08.bin", 0x00020000, 0x1a3c2f93, 0, 0, 0, },
   {  "dl_ic09.bin", 0x00020000, 0xa614d234, 0, 0, 0, },
   {  "dl_ic14.bin", 0x00020000, 0x50c02f0f, 0, 0, 0, },
   {  "dl_ic15.bin", 0x00080000, 0xb273f854, 0, 0, 0, },
   {  "dl_ic16.bin", 0x00020000, 0xfbe548b8, 0, 0, 0, },
   {  "dl_ic17.bin", 0x00080000, 0xc3fd0dcd, 0, 0, 0, },
   {  "dl_ic18.bin", 0x00020000, 0xb1c151c5, 0, 0, 0, },
   {  "dl_ic19.bin", 0x00080000, 0x18ef740a, 0, 0, 0, },
   {  "dl_ic20.bin", 0x00020000, 0x8eb3194d, 0, 0, 0, },
   {  "dl_ic21.bin", 0x00080000, 0xfe3a5179, 0, 0, 0, },
   {  "dl_ic33.bin", 0x00020000, 0x7e904e45, 0, 0, 0, },
   {  "dl_ic34.bin", 0x00010000, 0xcdf593f3, 0, 0, 0, },
   {  "dl_ic36.bin", 0x00020000, 0x18c8a32b, 0, 0, 0, },
   {  "dl_ic37.bin", 0x00010000, 0xf50db2d7, 0, 0, 0, },
   {  "dl_ic40.bin", 0x00010000, 0x5632ee49, 0, 0, 0, },
   {   "c02-01.rom", 0x00080000, 0xd5a3d1aa, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO dynamite_league_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x032004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x032004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x032004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x032004, 0x01, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x032004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x032006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x032006, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x032006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x032006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x03200E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x03200E, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x03200E, 0x04, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x032004, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x032006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x032006, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x032006, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x032006, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x03200E, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x03200E, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x03200E, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_dynamite_league_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DSWA_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT6,           0x20, 0x02 },
   { MSG_OFF,                 0x20, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO dynamite_league_dsw[] =
{
   { 0x032000, 0xFF, dsw_data_dynamite_league_0 },
   { 0x032002, 0xFF, dsw_data_default_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_dynamite_league_0[] =
{
   { "Taito Japan (Japanese)", 0x00 },
   { "Taito America",          0x01 },
   { "Taito Japan",            0x02 },
   { NULL,                     0    },
};

static struct ROMSW_INFO dynamite_league_romsw[] =
{
   { 0x05FFFF, 0x00, romsw_data_dynamite_league_0 },
   { 0,        0,    NULL },
};

static struct GFX_LAYOUT tilelayout =
{
	16,16,	/* 16x16 pixels */
	32768,	/* 32768 tiles */
	4,
	{ 0, 1, 2, 3 },
	{ 4, 0, 12, 8, 0x100000*8+4, 0x100000*8, 0x100000*8+12, 0x100000*8+8,
	    0x200000*8+4, 0x200000*8, 0x200000*8+12, 0x200000*8+8, 0x300000*8+4, 0x300000*8, 0x300000*8+12, 0x300000*8+8 },
	{ 0*16, 1*16, 2*16, 3*16, 4*16, 5*16, 6*16, 7*16,
		8*16, 9*16, 10*16, 11*16, 12*16, 13*16, 14*16, 15*16 },
	16*16
};

static struct GFX_LAYOUT charlayout =
{
	8, 8,	/* 8x8 pixels */
	256,	/* 256 chars */
	4,		/* 4 bit per pixel */
	{ 0x1000*8 + 8, 0x1000*8, 8, 0 },
	{ 0, 1, 2, 3, 4, 5, 6, 7 },
	{ 16*0, 16*1, 16*2, 16*3, 16*4, 16*5, 16*6, 16*7 },
	16*8
};

static struct GFX_LIST dleague_gfx[] =
{
	{ REGION_GFX1, &tilelayout },
	{ REGION_GFX2, &charlayout },
	{ 0,           NULL        },
};

static struct VIDEO_INFO dynamite_league_video =
{
   DrawDLeague,
   320,
   240,
   64,
   VIDEO_ROTATE_NORMAL,
};

static struct VIDEO_INFO dynamite_leaguem_video =
{
   DrawDLeague,
   320,
   240,
   64,
   VIDEO_ROTATE_NORMAL,
   dleague_gfx,
};

struct GAME_MAIN game_dynamite_league =
{
   dynamite_league_dirs,
   dynamite_league_roms,
   dynamite_league_inputs,
   dynamite_league_dsw,
   dynamite_league_romsw,

   LoadDLeague,
   ClearDLeague,
   &dynamite_league_video,
   ExecuteDLeagueFrame,
   "dleague",
   "Dynamite League",
   NULL,
   COMPANY_ID_TAITO,
   "C02",
   1990,
   taito_ym2610_sound,
   GAME_SPORTS | GAME_PARTIALLY_WORKING,
};

struct GAME_MAIN game_dynamite_leaguem =
{
   dynamite_league_dirs,
   dynamite_leaguem_roms,
   dynamite_league_inputs,
   dynamite_league_dsw,
   dynamite_league_romsw,

   LoadDLeaguem,
   ClearDLeague,
   &dynamite_leaguem_video,
   ExecuteDLeagueFrame,
   "dleaguem",
   "Dynamite League (Mame)",
   NULL,
   COMPANY_ID_TAITO,
   "C02",
   1990,
   taito_ym2610_sound,
   GAME_SPORTS | GAME_PARTIALLY_WORKING | GAME_PRIVATE,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_COLOUR;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_FG0;

static void setup_dleague() {
   YM2610SetBuffers(PCMROM, PCMROM, 0x80000, 0x80000);

   AddTaitoYM2610(0x033A, 0x02A7, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   GFX_FG0   = RAM+0x40000;
   RAM_VIDEO = RAM+0x10000;
   RAM_COLOUR= RAM+0x31000+0x800;
   RAM_INPUT = RAM+0x32000;

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x5000);

   InitPaletteMap(RAM_COLOUR, 0x40, 0x10, 0x8000);

   set_colour_mapper(&col_map_xbbb_bbgg_gggr_rrrr);

   // 68000 Speed Hack
   // ----------------

   WriteWord68k(&ROM[0x0464],0x4EF9);		//	jmp	$300
   WriteLong68k(&ROM[0x0466],0x00000300);

   WriteLong68k(&ROM[0x0300],0x46FC2000);	//	move	#$2000,SR
   WriteLong68k(&ROM[0x0304],0x13FC0000);	// 	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x0308],0x00AA0000);
   WriteWord68k(&ROM[0x030C],0x6100-10);	//	bra.s	<loop>

   // Init tc0004vcu emulation
   // ------------------------

   tc0004vcu.RAM	= RAM_VIDEO;
   tc0004vcu.GFX_BG0	= GFX_BG0;
   tc0004vcu.GFX_BG0_MSK= GFX_BG0_SOLID;
   tc0004vcu.GFX_FG0	= GFX_FG0;
   tc0004vcu.tile_mask	= 0x7FFF;
   tc0004vcu.bmp_x	= 64;
   tc0004vcu.bmp_y	= 64;
   tc0004vcu.bmp_w	= 320;
   tc0004vcu.bmp_h	= 240;
   tc0004vcu.scr_x	= 16-1;
   tc0004vcu.scr_y	= 32-1;

   tc0004vcu_init();

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x60000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x05FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x05FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x400000, 0x420FFF, NULL, RAM+0x010000);			// SCREEN RAM
   AddReadByte(0x500000, 0x500FFF, NULL, RAM+0x031000);			// COLOR RAM
   AddReadByte(0x200000, 0x20000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x300000, 0x300003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x05FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x400000, 0x420FFF, NULL, RAM+0x010000);			// SCREEN RAM
   AddReadWord(0x500000, 0x500FFF, NULL, RAM+0x031000);			// COLOR RAM
   AddReadWord(0x200000, 0x20000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x400000, 0x420FFF, NULL, RAM+0x010000);		// SCREEN RAM
   AddWriteByte(0x500000, 0x500FFF, NULL, RAM+0x031000);		// COLOR RAM
   AddWriteByte(0x200000, 0x20000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0x300000, 0x300003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x400000, 0x420FFF, NULL, RAM+0x010000);		// SCREEN RAM
   AddWriteWord(0x500000, 0x500FFF, NULL, RAM+0x031000);		// COLOR RAM
   AddWriteWord(0x200000, 0x20000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void LoadDLeaguem(void)
{
   if(!(RAM=AllocateMem(0x80000))) return;
   GFX = load_region[REGION_GFX1];
   GFX_BG0 = GFX+0x000000;

   RAMSize=0x48000+0x10000;

   /*-----[Sound Setup]-----*/

   if(!(Z80ROM=AllocateMem(0x1c000))) return;
   memcpy(Z80ROM+0x00000,load_region[REGION_ROM2]+0x0,0x4000);
   memcpy(Z80ROM+0x10000,load_region[REGION_ROM2]+0x4000,0xc000); 
  
   PCMROM=load_region[REGION_SMP1];
   setup_dleague();
}

void LoadDLeague(void)
{
   int ta,tb,tc;

   if(!(RAM=AllocateMem(0x80000))) return;
   if(!(ROM=AllocateMem(0x60000))) return;
   if(!(GFX=AllocateMem(0x400000+0x200000))) return;

   GFX_BG0 = GFX+0x000000;

   if(!load_rom("dl_ic06.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x400000;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic07.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x400004;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic08.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x400008;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic09.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x40000C;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic14.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x400002;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic16.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x400006;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic18.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x40000A;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic20.bin", RAM+0x00000, 0x20000)) return;	// GFX
   tb=0x40000E;
   for(ta=0;ta<0x20000;ta+=16){
      for(tc=0;tc<16;tc++){
         GFX[tb+(tc<<4)]=RAM[ta+tc]&15;
         GFX[tb+(tc<<4)+1]=RAM[ta+tc]>>4;
      }
      tb+=256;
   }
   if(!load_rom("dl_ic15.bin", RAM+0x00000, 0x80000)) return;	// GFX
   tb=0;
   for(ta=0;ta<0x80000;ta+=2){
      GFX[tb+0]=RAM[ta]&15;
      GFX[tb+1]=RAM[ta]>>4;
      GFX[tb+2]=RAM[ta+1]&15;
      GFX[tb+3]=RAM[ta+1]>>4;
      tb+=16;
   }
   if(!load_rom("dl_ic17.bin", RAM+0x00000, 0x80000)) return;	// GFX
   tb=4;
   for(ta=0;ta<0x80000;ta+=2){
      GFX[tb+0]=RAM[ta]&15;
      GFX[tb+1]=RAM[ta]>>4;
      GFX[tb+2]=RAM[ta+1]&15;
      GFX[tb+3]=RAM[ta+1]>>4;
      tb+=16;
   }
   if(!load_rom("dl_ic19.bin", RAM+0x00000, 0x80000)) return;	// GFX
   tb=8;
   for(ta=0;ta<0x80000;ta+=2){
      GFX[tb+0]=RAM[ta]&15;
      GFX[tb+1]=RAM[ta]>>4;
      GFX[tb+2]=RAM[ta+1]&15;
      GFX[tb+3]=RAM[ta+1]>>4;
      tb+=16;
   }
   if(!load_rom("dl_ic21.bin", RAM+0x00000, 0x80000)) return;	// GFX
   tb=12;
   for(ta=0;ta<0x80000;ta+=2){
      GFX[tb+0]=RAM[ta]&15;
      GFX[tb+1]=RAM[ta]>>4;
      GFX[tb+2]=RAM[ta+1]&15;
      GFX[tb+3]=RAM[ta+1]>>4;
      tb+=16;
   }

   if(!load_rom("dl_ic33.bin", RAM, 0x20000)) return;		// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("dl_ic36.bin", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("dl_ic34.bin", RAM, 0x10000)) return;
   for(ta=0;ta<0x10000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("dl_ic37.bin", RAM, 0x10000)) return;
   for(ta=0;ta<0x10000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   RAMSize=0x48000+0x10000;

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x48000;
   if(!load_rom("dl_ic40.bin", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("c02-01.rom",PCMROM,0x80000)) return;		// ADPCM A rom
   //memset(PCMROM,0x80,0x80000);
   setup_dleague();
}

void ClearDLeague(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x060000,1);
      save_debug("RAM.bin",RAM,0x040000,1);
      //save_debug("GFX.bin",GFX,0x500000,0);
   #endif
}

void ExecuteDLeagueFrame(void)
{
   //print_ingame(60,"%04x",ReadWord(&RAM[0x30800]));

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   #ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif
   cpu_interrupt(CPU_68K_0, 1);
   cpu_interrupt(CPU_68K_0, 3);

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawDLeague(void)
{
   ClearPaletteMap();

   // Init tc0180vcu emulation
   // ------------------------

   tc0004vcu_layer_count = 0;

   // BG0
   // ---

   tc0004vcu_render_bg0();

   // BG1
   // ---

   tc0004vcu_render_bg1();

   // OBJECT
   // ------

   tc0004vcu_render_obj();

   // FG0
   // ---

   //tc0004vcu_render_fg0();
}
