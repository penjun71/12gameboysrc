/******************************************************************************/
/*                                                                            */
/*                 GUN FRONTIER (C) 1990 TAITO CORPORATION                    */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "gunfront.h"
#include "tc100scn.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "taitosnd.h"
#include "sasound.h"		// sample support routines

static struct DIR_INFO gun_frontier_dirs[] =
{
   { "gun_frontier", },
   { "gunfront", },
   { NULL, },
};

static struct ROM_INFO gun_frontier_roms[] =
{
   {   "c71-10.rom", 0x00020000, 0xf39c0a06, 0, 0, 0, },
   {   "c71-02.rom", 0x00100000, 0x2a600c92, 0, 0, 0, },
   {   "c71-03.rom", 0x00100000, 0x9133c605, 0, 0, 0, },
   {   "c71-08.rom", 0x00020000, 0xc17dc0a0, 0, 0, 0, },
   {   "c71-09.rom", 0x00020000, 0x10a544a2, 0, 0, 0, },
   {   "c71-01.rom", 0x00100000, 0x0e73105a, 0, 0, 0, },
   {   "c71-12.rom", 0x00010000, 0x0038c7f8, 0, 0, 0, },
   {   "c71-14.rom", 0x00020000, 0x312da036, 0, 0, 0, },
   {   "c71-15.rom", 0x00020000, 0xdf3e00bb, 0, 0, 0, },
   {   "c71-16.rom", 0x00020000, 0x1bbcc2d4, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO gun_frontier_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03200E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03200E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03200E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03200E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x032004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x032004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x032004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x032004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x032004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x032004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x032004, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x032006, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x032006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x032006, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x032006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x032006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x032006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x032006, 0x20, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_gun_frontier_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_2PLAY,         0xC0, 0x00 },
   { MSG_1COIN_3PLAY,         0x80, 0x00 },
   { MSG_1COIN_4PLAY,         0x40, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_gun_frontier_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { MSG_DSWB_BIT3,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT4,           0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { "Lives",                 0x30, 0x04 },
   { "3",                     0x30, 0x00 },
   { "1",                     0x20, 0x00 },
   { "2",                     0x10, 0x00 },
   { "5",                     0x00, 0x00 },
   { "Continue Mode",         0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "Simultaneous Play",     0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO gun_frontier_dsw[] =
{
   { 0x032000, 0xFF, dsw_data_gun_frontier_0 },
   { 0x032002, 0xFF, dsw_data_gun_frontier_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_gun_frontier_0[] =
{
   { "Taito 1",               0x01 },
   { "Taito 2",               0x02 },
   { "Taito 3",               0x03 },
   { "Taito 4",               0x04 },
   { "Taito 5",               0x05 },
   { "Taito 6",               0x06 },
   { NULL,                    0    },
};

static struct ROMSW_INFO gun_frontier_romsw[] =
{
   { 0x07FFFF, 0x03, romsw_data_gun_frontier_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO gun_frontier_video =
{
   DrawGunFront,
   224,
   320,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_gun_frontier =
{
   gun_frontier_dirs,
   gun_frontier_roms,
   gun_frontier_inputs,
   gun_frontier_dsw,
   gun_frontier_romsw,

   LoadGunFront,
   ClearGunFront,
   &gun_frontier_video,
   ExecuteGunFrontFrame,
   "gunfront",
   "Gun Frontier",
   "ガンフロンティア",
   COMPANY_ID_TAITO,
   "C71",
   1990,
   taito_ym2610_sound,
   GAME_SHOOT,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

void LoadGunFront(void)
{
   int ta,tb;

   if(!(RAM=AllocateMem(0x100000))) return;
   if(!(GFX=AllocateMem(0x200000+0x200000))) return;

   GFX_BG0 = GFX+0x000000;
   GFX_SPR = GFX+0x200000;

   tb=0;
   if(!load_rom("c71-02.rom", RAM, 0x100000)) return;		// 8x8 TILES
   for(ta=0;ta<0x100000;ta+=2){
      GFX[tb+0]=RAM[ta+1]>>4;
      GFX[tb+1]=RAM[ta+1]&15;
      GFX[tb+2]=RAM[ta+0]>>4;
      GFX[tb+3]=RAM[ta+0]&15;
      tb+=4;
   }
   tb=0;
   if(!load_rom("c71-03.rom", RAM, 0x100000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x100000;ta++){
      GFX_SPR[tb++]=RAM[ta]&15;
      GFX_SPR[tb++]=RAM[ta]>>4;
   }

   FreeMem(RAM);

   Rotate8x8(GFX,0x8000);
   Flip8x8_X(GFX,0x8000);
   Rotate16x16(GFX_SPR,0x2000);
   Flip16x16_X(GFX_SPR,0x2000);

   RAMSize=0x48000;

   if(!(RAM=AllocateMem(RAMSize))) return;
   if(!(ROM=AllocateMem(0xC0000))) return;

   if(!load_rom("c71-09.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("c71-08.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("c71-10.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("c71-14.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }
   if(!load_rom("c71-16.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x80000]=RAM[ta];
   }
   if(!load_rom("c71-15.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x80001]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x38000;
   if(!load_rom("c71-12.rom", Z80ROM, 0x10000)) return;	// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x100000))) return;
   if(!load_rom("c71-01.rom",PCMROM,0x100000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x100000, 0x100000);

   AddTaitoYM2610(0x01A4, 0x0150, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x34000);

   RAM_VIDEO  = RAM+0x10000;
   RAM_SCROLL = RAM+0x32100;
   RAM_OBJECT = RAM+0x20000;
   RAM_INPUT  = RAM+0x32000;
   GFX_FG0    = RAM+0x34000;

   GFX_BG0_SOLID = make_solid_mask_8x8  (GFX_BG0, 0x8000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);
   InitPaletteMap(RAM+0x30000, 0x100, 0x10, 0x1000);


   // speed hack

   WriteWord68k(&ROM[0x151DE],0x4EF9);			//	jmp	$0000C0
   WriteLong68k(&ROM[0x151E0],0x000000C0);

   WriteLong68k(&ROM[0x000C0],0x13FC0000);		//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x000C4],0x00AA0000);

   WriteWord68k(&ROM[0x000C8],0x082D);			//	btst	#2,-32762(a5)
   WriteLong68k(&ROM[0x000CA],0x00028006);

   WriteWord68k(&ROM[0x000CE],0x6700+(0x100-0x10)); 	//	beq.s	<loop>

   WriteWord68k(&ROM[0x000D0],0x082D);			//	btst	#5,-1223(a5)
   WriteLong68k(&ROM[0x000D2],0x0005FB39);

   WriteWord68k(&ROM[0x000D6],0x6600+(0x100-0x18)); 	//	bne.s	<loop>

   WriteWord68k(&ROM[0x000D8],0x4EF9);			//	jmp	$0151EE
   WriteLong68k(&ROM[0x000DA],0x000151EE);

   // scroll hack

   WriteLong68k(&ROM[0x10DFC],0x4EB80100);		//	jsr	$100.w

   WriteWord68k(&ROM[0x00100],0x4EB9);			//	jsr	$1101C
   WriteLong68k(&ROM[0x00102],0x0001101C);

   WriteLong68k(&ROM[0x00106],0x13FC0000);		//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x0010A],0x00AA0000);

   WriteWord68k(&ROM[0x0010E],0x4E75);			//	rts

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;	//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=SCN_BG0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=224;
   tc0100scn[0].layer[0].bmp_h	=320;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x7FFF;
   tc0100scn[0].layer[0].scr_x	=0x0013;
   tc0100scn[0].layer[0].scr_y	=0x0008;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=SCN_BG1;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=224;
   tc0100scn[0].layer[1].bmp_h	=320;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x7FFF;
   tc0100scn[0].layer[1].scr_x	=0x0013;
   tc0100scn[0].layer[1].scr_y	=0x0008;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=SCN_FG0_SCROLL;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=224;
   tc0100scn[0].layer[2].bmp_h	=320;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=0x0013;
   tc0100scn[0].layer[2].scr_y	=0x0008;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT+0x0000;
   tc0200obj.RAM_B	= RAM_OBJECT+0x8000;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 224;
   tc0200obj.bmp_h	= 320;
// Mapper disabled
   tc0200obj.tile_mask	= 0x1FFF;
   tc0200obj.ofs_x	= 0;
   tc0200obj.ofs_y	= 0;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0xC0000);
   ByteSwap(RAM,0x2C000);

   AddMemFetch(0x000000, 0x0BFFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x0BFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadByte(0x300000, 0x30000F, tc0220ioc_rb_bswap, NULL);		// INPUT
   AddReadByte(0x320000, 0x320003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x0BFFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x200000, 0x201FFF, NULL, RAM+0x030000);			// COLOR RAM
   AddReadWord(0x300000, 0x30000F, tc0220ioc_rw_bswap, NULL);		// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1, NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb_r270, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x320000, 0x320003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x300000, 0x30000F, tc0220ioc_wb_bswap, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww_r270, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x80FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x200000, 0x201FFF, NULL, RAM+0x030000);		// COLOR RAM
   AddWriteWord(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0xB00000, 0xB0000F, NULL, RAM+0x032200);		// ??? RAM
   AddWriteWord(0x300000, 0x30000F, tc0220ioc_ww_bswap, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearGunFront(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x0C0000,1);
      //save_debug("RAM.bin",RAM,0x048000,1);
      //save_debug("GFX.bin",GFX,0x500000,0);
   #endif
}

void ExecuteGunFrontFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(20,60) - s68000context.odometer);
   /*#ifdef RAINE_DEBUG
      print_debug("PC:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif*/
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);
   s68000context.odometer = 0;
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(20,60));

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawGunFront(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped_r270(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped_r270(0,1);

   // OBJECT
   // ------

   render_tc0200obj_mapped_r270();

   // FG0
   // ---

   render_tc0100scn_layer_mapped_r270(0,2);
}

