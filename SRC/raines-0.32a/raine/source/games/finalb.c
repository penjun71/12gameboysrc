/******************************************************************************/
/*                                                                            */
/*                    FINAL BLOW (C) 1989 TAITO CORPORATION                   */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "finalb.h"
#include "tc100scn.h"
#include "tc110pcr.h"
#include "tc200obj.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO final_blow_dirs[] =
{
   { "final_blow", },
   { "finalb", },
   { "finalbl", },
   { NULL, },
};

static struct ROM_INFO final_blow_roms[] =
{
   {    "fb_09.rom", 0x00020000, 0x632f1ecd, 0, 0, 0, },
   {    "fb_10.rom", 0x00010000, 0xa38aaaed, 0, 0, 0, },
   {    "fb_17.rom", 0x00020000, 0xe91b2ec9, 0, 0, 0, },
   {   "fb_m01.rom", 0x00080000, 0xf0eb6846, 0, 0, 0, },
   {   "fb_m02.rom", 0x00080000, 0x5dd06bdd, 0, 0, 0, },
   {   "fb_m03.rom", 0x00080000, 0xdaa11561, 0, 0, 0, },
   {   "fb_m04.rom", 0x00080000, 0x6346f98e, 0, 0, 0, },
   {   "fb_m05.rom", 0x00080000, 0xaa90b93a, 0, 0, 0, },
   {   "fb_m06.rom", 0x00020000, 0xfc450a25, 0, 0, 0, },
   {   "fb_m07.rom", 0x00020000, 0xec3df577, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO final_blow_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x03C00E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x03C00E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x03C00E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x03C00E, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x03C004, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x03C004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x03C004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x03C004, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x03C004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x03C004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x03C004, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x03C004, 0x40, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x03C006, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x03C006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x03C006, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x03C006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x03C006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x03C006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x03C006, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x03C006, 0x40, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_final_blow_0[] =
{
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_1COIN_2PLAY,         0x20, 0x00 },
   { MSG_2COIN_1PLAY,         0x10, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { MSG_1COIN_1PLAY,         0xC0, 0x00 },
   { MSG_1COIN_2PLAY,         0x80, 0x00 },
   { MSG_2COIN_1PLAY,         0x40, 0x00 },
   { MSG_2COIN_3PLAY,         0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_final_blow_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO final_blow_dsw[] =
{
   { 0x03C000, 0xFF, dsw_data_final_blow_0 },
   { 0x03C002, 0xFF, dsw_data_final_blow_1 },
   { 0,        0,    NULL,      },
};

static struct ROMSW_DATA romsw_data_final_blow_0[] =
{
   { "Taito Japan (Notice)",  0x00 },
   { "Taito America",         0x01 },
   { "Taito Japan",           0x02 },
   { NULL,                    0    },
};

static struct ROMSW_INFO final_blow_romsw[] =
{
   { 0x03FFFF, 0x02, romsw_data_final_blow_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO final_blow_video =
{
   DrawFinalBlow,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL| VIDEO_ROTATABLE,
};

struct GAME_MAIN game_final_blow =
{
   final_blow_dirs,
   final_blow_roms,
   final_blow_inputs,
   final_blow_dsw,
   final_blow_romsw,

   LoadFinalBlow,
   ClearFinalBlow,
   &final_blow_video,
   ExecuteFinalBFrame,
   "finalb",
   "Final Blow",
   "ファイナルブロー",
   COMPANY_ID_TAITO,
   "B82",
   1988,
   taito_ym2610_sound,
   GAME_BEAT,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_SCROLL;
static UINT8 *RAM_OBJECT;
static UINT8 *RAM_INPUT;

static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

void LoadFinalBlow(void)
{
   int ta,tb;

   if(!(GFX=AllocateMem(0x280000))) return;
   if(!(RAM=AllocateMem(0x80000))) return;

   GFX_SPR = GFX+0x80000;

   tb=0;
   if(!load_rom("fb_m07.rom", RAM, 0x20000)) return;		// 8x8 BG TILES
   for(ta=0;ta<0x20000;ta+=2){
      GFX[tb+1]=RAM[ta+0]&15;
      GFX[tb+0]=RAM[ta+0]>>4;
      GFX[tb+5]=RAM[ta+1]&15;
      GFX[tb+4]=RAM[ta+1]>>4;
      tb+=8;
   }
   tb=0;
   if(!load_rom("fb_m06.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta+=2){
      GFX[tb+3]=RAM[ta+0]&15;
      GFX[tb+2]=RAM[ta+0]>>4;
      GFX[tb+7]=RAM[ta+1]&15;
      GFX[tb+6]=RAM[ta+1]>>4;
      tb+=8;
   }

   tb=0;
   if(!load_rom("fb_m03.rom", RAM, 0x80000)) return;		// 16x16 SPRITES
   for(ta=0;ta<0x80000;ta++,tb+=4){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }
   tb=2;
   if(!load_rom("fb_m04.rom", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta++,tb+=4){
      GFX_SPR[tb+0]=RAM[ta]&15;
      GFX_SPR[tb+1]=RAM[ta]>>4;
   }
   tb=0;
   if(!load_rom("fb_m05.rom", RAM, 0x80000)) return;
   for(ta=0;ta<0x80000;ta++,tb+=4){
      GFX_SPR[tb+0]|=((RAM[ta] & 0x3)>>0)<<4;
      GFX_SPR[tb+1]|=((RAM[ta] & 0xC)>>2)<<4;
      GFX_SPR[tb+2]|=((RAM[ta] & 0x30)>>4)<<4;
      GFX_SPR[tb+3]|=((RAM[ta] & 0xC0)>>6)<<4;
   }

   RAMSize=0x40000+0x10000+0x4000;

   if(!(ROM=AllocateMem(0x40000))) return;
   if(!(RAM=AllocateMem(RAMSize))) return;

   if(!load_rom("fb_09.rom", RAM, 0x20000)) return;		// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("fb_17.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x40000;
   if(!load_rom("fb_10.rom", Z80ROM, 0x10000)) return;		// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x100000))) return;
   if(!load_rom("fb_m01.rom",PCMROM,0x80000)) return;		// ADPCM A rom
   if(!load_rom("fb_m02.rom",PCMROM+0x80000,0x80000)) return;	// ADPCM A rom
   if(ReadLong(&PCMROM[0])==0){
      FreeMem(PCMROM);
      YM2610SetBuffers(NULL, NULL, 0, 0);
   }
   else{
      YM2610SetBuffers(PCMROM, PCMROM+0x80000, 0x80000, 0x80000);
   }
   AddTaitoYM2610(0x033A, 0x02A7, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x40000);

   RAM_VIDEO  = RAM+0x18000;
   RAM_SCROLL = RAM+0x3C100;
   RAM_OBJECT = RAM+0x10000;
   RAM_INPUT  = RAM+0x3C000;
   GFX_FG0    = RAM+0x50000;

   GFX_BG0_SOLID = make_solid_mask_8x8(GFX, 0x2000);
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x2000);

   tc0110pcr_init(RAM+0x38000, 0);

   set_colour_mapper(&col_map_xbbb_bbgg_gggr_rrrr);
   InitPaletteMap(RAM+0x38000, 0x100, 0x10, 0x8000);


   // Fix Int#6
   // ---------

   //WriteLong68k(&ROM[0x00610],0x4E714E71);	//	nop

   // Speed Hack
   // ----------

   WriteLong68k(&ROM[0x00744],0x13FC0000);	//	move.b	#$00,$AA0000
   WriteLong68k(&ROM[0x00748],0x00AA0000);
   WriteWord68k(&ROM[0x0074C],0x6100-16);

   //WriteLong68k(&ROM[0x00618],0x13FC0000);	//	move.b	#$00,$AA0000
   //WriteLong68k(&ROM[0x0061C],0x00AA0000);

   // Fix Colour ram error
   // --------------------

   WriteWord68k(&ROM[0x022E6],0x4E71);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn[0].layer[0].RAM	=RAM_VIDEO+0x0000;
   tc0100scn[0].layer[0].GFX	=GFX;
   tc0100scn[0].layer[0].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[0].SCR	=RAM_SCROLL+0;
   tc0100scn[0].layer[0].type	=0;
   tc0100scn[0].layer[0].bmp_x	=32;
   tc0100scn[0].layer[0].bmp_y	=32;
   tc0100scn[0].layer[0].bmp_w	=320;
   tc0100scn[0].layer[0].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[0].tile_mask=0x1FFF;
   tc0100scn[0].layer[0].scr_x	=16;
   tc0100scn[0].layer[0].scr_y	=8;

   tc0100scn[0].layer[1].RAM	=RAM_VIDEO+0x8000;
   tc0100scn[0].layer[1].GFX	=GFX;
   tc0100scn[0].layer[1].MASK	=GFX_BG0_SOLID;
   tc0100scn[0].layer[1].SCR	=RAM_SCROLL+2;
   tc0100scn[0].layer[1].type	=0;
   tc0100scn[0].layer[1].bmp_x	=32;
   tc0100scn[0].layer[1].bmp_y	=32;
   tc0100scn[0].layer[1].bmp_w	=320;
   tc0100scn[0].layer[1].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[1].tile_mask=0x1FFF;
   tc0100scn[0].layer[1].scr_x	=16;
   tc0100scn[0].layer[1].scr_y	=8;

   tc0100scn[0].layer[2].RAM	=RAM_VIDEO+0x4000;
   tc0100scn[0].layer[2].GFX	=GFX_FG0;
   tc0100scn[0].layer[2].SCR	=RAM_SCROLL+4;
   tc0100scn[0].layer[2].type	=1;
   tc0100scn[0].layer[2].bmp_x	=32;
   tc0100scn[0].layer[2].bmp_y	=32;
   tc0100scn[0].layer[2].bmp_w	=320;
   tc0100scn[0].layer[2].bmp_h	=224;
// Mapper disabled
   tc0100scn[0].layer[2].scr_x	=16;
   tc0100scn[0].layer[2].scr_y	=8;

   tc0100scn[0].RAM     = RAM_VIDEO;
   tc0100scn[0].GFX_FG0 = GFX_FG0;

   init_tc0100scn(0);

   // Init tc0200obj emulation
   // ------------------------

   tc0200obj.RAM	= RAM_OBJECT;
   tc0200obj.GFX	= GFX_SPR;
   tc0200obj.MASK	= GFX_SPR_SOLID;
   tc0200obj.bmp_x	= 32;
   tc0200obj.bmp_y	= 32;
   tc0200obj.bmp_w	= 320;
   tc0200obj.bmp_h	= 224;
// Mapper disabled
   tc0200obj.tile_mask	= 0x1FFF;
   tc0200obj.ofs_x	= -96;
   tc0200obj.ofs_y	= -16;

   init_tc0200obj();

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x40000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x300000, 0x30000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x320000, 0x320003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x800000, 0x81FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x900000, 0x907FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddReadWord(0x200000, 0x200007, tc0110pcr_rw, NULL);			// COLOR RAM
   AddReadWord(0x300000, 0x30000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_wb, NULL);	// FG0 GFX RAM
   AddWriteByte(0x800000, 0x81FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x900000, 0x907FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteByte(0x320000, 0x320003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x300000, 0x30000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x10FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x806000, 0x806FFF, tc0100scn_0_gfx_fg0_ww, NULL);	// FG0 GFX RAM
   AddWriteWord(0x800000, 0x81FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x900000, 0x907FFF, NULL, RAM_OBJECT);			// OBJECT RAM
   AddWriteWord(0x200000, 0x200007, tc0110pcr_ww, NULL);		// COLOR RAM
   AddWriteWord(0x820000, 0x82000F, NULL, RAM_SCROLL);			// SCROLL RAM
   AddWriteWord(0x360000, 0x36000F, NULL, RAM+0x03C300);		// ??? RAM
   AddWriteWord(0x300000, 0x30000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearFinalBlow(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      save_debug("ROM.bin",ROM,0x040000,1);
      save_debug("RAM.bin",RAM,0x040000,1);
      //save_debug("GFX.bin",GFX,0x280000,0);
   #endif
}

void ExecuteFinalBFrame(void)
{
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(10,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 6);
   cpu_interrupt(CPU_68K_0, 5);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(2,60));	// Overflow for sprite sync

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawFinalBlow(void)
{
   ClearPaletteMap();

   // Init tc0100scn emulation
   // ------------------------

   tc0100scn_layer_count = 0;
   tc0100scn[0].ctrl = ReadWord(RAM_SCROLL+12);

   // BG0
   // ---

   render_tc0100scn_layer_mapped(0,0);

   // BG1
   // ---

   render_tc0100scn_layer_mapped(0,1);

   // OBJECT
   // ------

   render_tc0200obj_mapped_64();

   // FG0
   // ---

   render_tc0100scn_layer_mapped(0,2);
}
