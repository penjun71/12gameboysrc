/******************************************************************************/
/*                                                                            */
/*                               GUNBIRD (C) 1994 PSIKYO                      */
/*                                                                            */
/*                             SENGOKU ACE (C) 1993 PSIKYO                    */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "gunbird.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"
#include "memory.h"

static struct DIR_INFO Gunbird_dirs[] =
{
   { "gunbird", },
   { NULL, },
};

static struct DIR_INFO btlkrodj_dirs[] =
{
   { "btlkrodj", },
   //   { "gunbird", },
   { NULL, },
};

//static int sport = 0;
static int ack_latch,latch;
//static int read_debug;
static int  oldmem13;
static UINT8 *z80_8200; /* Fast bankswitch for z80 */


READ_HANDLER(soundlatch_r)
{
  // Read port 8
  //  fprintf(stderr,"soundlatch_r %d et %d\n",RAM[0x30012],RAM[0x30013]);
  return latch;
}

/* In realt� c'� una funzione Z80ASetbank */
WRITE_HANDLER(gunbird_sound_bankswitch_w)
{
  int bank = ((data>>4)&3);
	/* The banked rom is seen at 8200-ffff, so the last 0x200 bytes
	   of the rom not reachable. */

  z80_8200 = Z80ROM+0x10200+bank*0x8000;  
}

UINT16 z80_read8200b(UINT16 offset) {
  offset -= 0x8200;
  //  fprintf(stderr,"offset : %x\n",offset);
  return z80_8200[offset];
}

WRITE_HANDLER(sngkace_sound_bankswitch_w)
{
  int bank = (data&3);
  z80_8200 = Z80ROM+0x10000+bank*0x8000;  
}

UINT16 z80_read8000b(UINT16 offset) {
  offset -= 0x8000;
  //  fprintf(stderr,"offset : %x\n",offset);
  return z80_8200[offset];
}

WRITE_HANDLER( psikyo_ack_latch_w )
{
  //    fprintf(stderr,"SoundWrite %d:%d\n",address,data);
  //    RAM[0x30003]&=0x7f;
    //    YM2610WriteZ80(0,data);
  ack_latch = 0;
}

static struct ROMSW_DATA romsw_btlkrodj[] =
{
  { "Psikyo (Japan)", 0xf},
  { "Psikyo (Honk Kong)",0xa},
  { "Psikyo (Korea)", 0xc},
  { "Psikyo (Taiwan)",0x6},
  { "Jaleco+Psikyo (USA & Canada)",0xe},
  { "Psikyo",0x0},
  { NULL,                    0    },  
};

static struct ROMSW_INFO btlkrodj_romsw[] =
{
   { 0xc00007, 0x0e, romsw_btlkrodj },
   { 0,        0,    NULL },
};
  
static struct ROM_INFO Gunbird_roms[] =
{
   {    "1-u46.bin",   0x040000, 0x474abd69, 0, 0, 0, },
   {    "2-u39.bin",   0x040000, 0x3e3e661f, 0, 0, 0, },
   {    "3-u71.bin",   0x020000, 0x2168e4ba, 0, 0, 0, },
   {      "u14.bin",   0x200000, 0x7d7e8a00, 0, 0, 0, },
   {      "u24.bin",   0x200000, 0x5e3ffc9d, 0, 0, 0, },
   {      "u15.bin",   0x200000, 0xa827bfb5, 0, 0, 0, },
   {      "u25.bin",   0x100000, 0xef652e0c, 0, 0, 0, },
   {      "u33.bin",   0x200000, 0x54494e6b, 0, 0, 0, },
   {      "u64.bin",   0x080000, 0xe187ed4f, 0, 0, 0, },
   {      "u56.bin",   0x100000, 0x9e07104d, 0, 0, 0, },
   {       "u3.bin",   0x040000, 0x0905aeb2, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct ROM_INFO btlkrodj_roms[] =
{
   {    "4-u46.bin",   0x040000, 0x8a7a28b4, 0, 0, 0, },
   {    "5-u39.bin",   0x040000, 0x933561fa, 0, 0, 0, },
   {    "3-u71.bin",   0x020000, 0x22411fab, 0, 0, 0, },
   {      "u14.bin",   0x200000, 0x282d89c3, 0, 0, 0, },
   {      "u24.bin",   0x200000, 0xbbe9d3d1, 0, 0, 0, },
   {      "u15.bin",   0x200000, 0xd4d1b07c, 0, 0, 0, },
   //   {      "u25.bin",       0x100000,   0xef652e0c  }, Not present
   {      "u33.bin",   0x200000, 0x4c8577f1, 0, 0, 0, },
   {      "u64.bin",   0x080000, 0x0f33049f, 0, 0, 0, },
   {      "u56.bin",   0x100000, 0x51d73682, 0, 0, 0, },
   {       "u3.bin",   0x040000, 0x30d541ed, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};


static struct INPUT_INFO Gunbird_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x30003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x30003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x30003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_TEST,         MSG_TEST,                0x30003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x30003, 0x40, BIT_ACTIVE_0 },
   // 0x80 from sound CPU

   { KB_DEF_P1_START,     MSG_P1_START,            0x30000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x30000, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x30000, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x30000, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x30000, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x30000, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x30000, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x30000, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x30001, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x30001, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x30001, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x30001, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x30001, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x30001, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x30001, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x30001, 0x02, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct INPUT_INFO btlkrodj_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x30003, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x30003, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x30003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_TEST,         MSG_TEST,                0x30003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x30003, 0x40, BIT_ACTIVE_0 },
   // 0x80 from sound CPU

   { KB_DEF_P1_START,     MSG_P1_START,            0x30000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x30000, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x30000, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x30000, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x30000, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x30000, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x30000, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x30000, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_B4,        MSG_P1_B4,               0x30002, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_B5,        MSG_P1_B5,               0x30002, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_B6,        MSG_P1_B6,               0x30002, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x30001, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x30001, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x30001, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x30001, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x30001, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x30001, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x30001, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x30001, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_B4,        MSG_P2_B4,               0x30002, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B5,        MSG_P2_B5,               0x30002, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_B6,        MSG_P2_B6,               0x30002, 0x02, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};


static struct DSW_DATA dsw_data_Gunbird_0[] =
{
   { "Flip screen" ,	      0x01, 0x02 },
   { MSG_OFF,		      0x01, 0x00 },
   { MSG_ON,		      0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DIFFICULTY,          0x0C, 0x04 },  // Is it really the difficulty level ?
   { MSG_EASY,                0x08, 0x00 },
   { MSG_NORMAL,              0x0C, 0x00 },
   { MSG_HARD,                0x04, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Lives",                 0x30, 0x04 },
   { "1",                     0x20, 0x00 },
   { "2",                     0x10, 0x00 },
   { "3",                     0x30, 0x00 },
   { "4",                     0x00, 0x00 },
   { "Bonus life",            0x40, 0x02 },
   { "400k",                  0x40, 0x00 },
   { "600k",                  0x00, 0x00 },
   { MSG_SERVICE,             0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};


static struct DSW_DATA dsw_data_Gunbird_1[] =
{
   { "Credits/Coinage",       0x01, 0x02 },
   { "A+B/A,B",               0x01, 0x00 },
   { "A,B/A",                 0x00, 0x00 }, 
   { "Coinage A",             0x0E, 0x08 },	
   { MSG_3COIN_1PLAY,         0x0A, 0x00 },
   { MSG_2COIN_1PLAY,         0x0C, 0x00 },
   { MSG_1COIN_1PLAY,         0x0E, 0x00 },
   { MSG_1COIN_2PLAY,         0x08, 0x00 },
   { MSG_1COIN_3PLAY,         0x06, 0x00 },
   { MSG_1COIN_4PLAY,         0x04, 0x00 },
   { MSG_1COIN_5PLAY,         0x02, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { "Coinage B",             0x70, 0x08 },	
   { MSG_3COIN_1PLAY,         0x50, 0x00 },
   { MSG_2COIN_1PLAY,         0x60, 0x00 },
   { MSG_1COIN_1PLAY,         0x70, 0x00 },
   { MSG_1COIN_2PLAY,         0x40, 0x00 },
   { MSG_1COIN_3PLAY,         0x30, 0x00 },
   { MSG_1COIN_4PLAY,         0x20, 0x00 },
   { MSG_1COIN_5PLAY,         0x10, 0x00 },
   { MSG_1COIN_6PLAY,         0x00, 0x00 },
   { "2 Coins/1 Credit",      0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};


static struct DSW_INFO Gunbird_dsw[] =
{
   { 0x30004, 0xFF, dsw_data_Gunbird_1 },
   { 0x30005, 0xFD, dsw_data_Gunbird_0 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO Gunbird_video =
{
   DrawGunbird,
   320,         
   224,       
   32, 
   VIDEO_ROTATE_270 | VIDEO_ROTATABLE
//   VIDEO_ROTATE_NORMAL | VIDEO_ROTATABLE
};

static struct VIDEO_INFO btlkrodj_video =
{
   DrawGunbird,
   320,         
   224,       
   32, 
   //   VIDEO_ROTATE_270 | VIDEO_ROTATABLE
   VIDEO_ROTATE_NORMAL | VIDEO_ROTATABLE
};


void ClearGunbird(void)
{
}

int mycount = 0;

// The sound is too slow, but this irq does not change anything...
void sound_irq(int irq) {
  cpu_interrupt(CPU_Z80_0, 0x38);
  //cpu_interrupt(CPU_M68020_0, 1);
}
  
static struct YM2610interface ym2610_interface =
{
  1,
  8000000,
  { YM2203_VOL(255,180) },
  { 0 },
  { 0 },
  { 0 },
  { 0 },
  { NULL }, // should be sound_irq, but does no good...
  { 0 },			// hiro-shi
  { 0 },			// hiro-shi
  { YM3012_VOL(255,OSD_PAN_LEFT,255,OSD_PAN_RIGHT) },
};

struct SOUND_INFO Gunbird_sound[] =
{
   { SOUND_YM2610,  &ym2610_interface,  },
   { 0,             NULL,               },
};

struct GAME_MAIN game_gunbird =
{
   Gunbird_dirs,
   Gunbird_roms,
   Gunbird_inputs,
   Gunbird_dsw,
   NULL,

   LoadGunbird,
   ClearGunbird,
   &Gunbird_video,
   ExecuteGunbirdFrame,
   "gunbird",
   "Gunbird",
   "Gunbird",
   COMPANY_ID_PSIKYO,
   NULL,
   1994,
   Gunbird_sound,
   GAME_SHOOT,
};

struct GAME_MAIN game_btlkrodj = 
{ 
   btlkrodj_dirs,
   btlkrodj_roms,
   btlkrodj_inputs,
   Gunbird_dsw,
   btlkrodj_romsw,

   Loadbtlkrodj,
   ClearGunbird,
   &btlkrodj_video,
   ExecuteGunbirdFrame,
   "btlkrodj",
   "Battle K-Road (Japan)",
   "Battle K-Road (Japan)",
   COMPANY_ID_PSIKYO,
   NULL,
   1994,
   Gunbird_sound,
   GAME_BEAT,
};


static struct DIR_INFO Sngkace_dirs[] =
{
   { "Sengoku_Ace", },
   { "Sngkace", },
   { "sngkace", },
   { NULL, },
};


static struct ROM_INFO Sngkace_roms[] =
{
   {   "1-u127.bin",   0x040000, 0x6c45b2f8, 0, 0, 0, },
   {   "2-u126.bin",   0x040000, 0x845a6760, 0, 0, 0, },
   {    "3-u58.bin",   0x020000, 0x310f5c76, 0, 0, 0, },
   {      "u14.bin",   0x200000, 0x00a546cb, 0, 0, 0, },
   {      "u34.bin",   0x100000, 0xe6a75bd8, 0, 0, 0, },
   {      "u35.bin",   0x100000, 0xc4ca0164, 0, 0, 0, },
   {      "u68.bin",   0x100000, 0x9a7f6c34, 0, 0, 0, },
   {      "u11.bin",   0x040000, 0x11a04d91, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};


static struct INPUT_INFO Sngkace_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x30009, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x30009, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x30009, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_TEST,         MSG_TEST,                0x30009, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x30009, 0x40, BIT_ACTIVE_0 },
   // 0x80 from sound CPU

   { KB_DEF_P1_START,     MSG_P1_START,            0x30000, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x30000, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x30000, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x30000, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x30000, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x30000, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x30000, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x30000, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x30001, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x30001, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x30001, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x30001, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x30001, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x30001, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x30001, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x30001, 0x02, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};


struct GAME_MAIN game_sengoku_ace =
{
   Sngkace_dirs,
   Sngkace_roms,
   Sngkace_inputs,
   Gunbird_dsw,
   NULL,

   LoadSngkace,
   ClearGunbird,
   &Gunbird_video,
   ExecuteGunbirdFrame,
   "sngkace",
   "Sengoku Ace",
   "Sengoku Ace",
   COMPANY_ID_PSIKYO,
   NULL,
   1993,
   Gunbird_sound,
   GAME_SHOOT,
};


static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_BG1;
static UINT8 *GFX_BG1_SOLID;

static UINT8 *RAM_SPR_LST;
static UINT8 *RAM_SPR;
static UINT8 *RAM_BG0;
static UINT8 *RAM_BG1;
static UINT8 *LUT;
static UINT8 *TMP;

static int num_sprites;


static void GunbirdDecode(char *S,int tb,int size)
{
   int ta;

   if(!load_rom(S, TMP, size)) return;

   for(ta=0;ta<size;ta+=8){
      GFX[tb++]=TMP[ta+1]>>4;
      GFX[tb++]=TMP[ta+1]&15;
      GFX[tb++]=TMP[ta+0]>>4;
      GFX[tb++]=TMP[ta+0]&15;
      GFX[tb++]=TMP[ta+3]>>4;
      GFX[tb++]=TMP[ta+3]&15;
      GFX[tb++]=TMP[ta+2]>>4;
      GFX[tb++]=TMP[ta+2]&15;
      GFX[tb++]=TMP[ta+5]>>4;
      GFX[tb++]=TMP[ta+5]&15;
      GFX[tb++]=TMP[ta+4]>>4;
      GFX[tb++]=TMP[ta+4]&15;
      GFX[tb++]=TMP[ta+7]>>4;
      GFX[tb++]=TMP[ta+7]&15;
      GFX[tb++]=TMP[ta+6]>>4;
      GFX[tb++]=TMP[ta+6]&15;     
   }
}

void finish_z80() {
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,		NULL);
   AddZ80AReadByte(    -1,     -1, NULL,                NULL);
  
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,		NULL);
   AddZ80AWriteByte(    -1,     -1, NULL,               NULL);

   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,               NULL);
   AddZ80AReadPort(  -1,   -1, NULL,                        NULL);

   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,              NULL);
   AddZ80AWritePort(  -1,   -1, NULL,                        NULL);
   AddZ80AInit();
}

void setup_z80_gunbird() {
   AddZ80AReadByte(0x0000,0x7fff,NULL,Z80ROM);
   AddZ80AReadByte(0x8000,0x81ff,NULL,Z80RAM+0x8000); 
   AddZ80AReadByte(0x8200,0xffff,z80_read8200b,NULL);

   AddZ80AWriteByte(0x0000,0x7fff,DefBadWriteZ80,     NULL);
   AddZ80AWriteByte(0x8000,0x81ff,NULL,Z80RAM+0x8000);
   AddZ80AWriteByte(0x8200,0xffff,DefBadWriteZ80,     NULL);

   // Ports

   AddZ80AReadPort(0x4, 0x4,YM2610_status_port_0_A_r ,    NULL);
   AddZ80AReadPort(0x6, 0x6,YM2610_status_port_0_B_r ,    NULL);
   AddZ80AReadPort(0x8, 0x8,soundlatch_r,    NULL);

   AddZ80AWritePort(0x0, 0x0, gunbird_sound_bankswitch_w,    NULL);
   AddZ80AWritePort(0x4, 0x4, YM2610_control_port_0_A_w,    NULL);
   AddZ80AWritePort(0x5, 0x5, YM2610_data_port_0_A_w,    NULL);
   AddZ80AWritePort(0x6, 0x6, YM2610_control_port_0_B_w,    NULL);
   AddZ80AWritePort(0x7, 0x7, YM2610_data_port_0_B_w,    NULL);
   AddZ80AWritePort(0xc, 0xc, psikyo_ack_latch_w,    NULL);

   finish_z80();
   z80_8200 = Z80ROM+0x10200;
}

void setup_z80_sngkace() {
   AddZ80AReadByte(0x0000,0x77ff,NULL,Z80ROM);
   AddZ80AReadByte(0x7800,0x7fff,NULL,Z80RAM+0x7800); 
   AddZ80AReadByte(0x8000,0xffff,z80_read8000b,NULL);

   AddZ80AWriteByte(0x0000,0x77ff,DefBadWriteZ80,     NULL);
   AddZ80AWriteByte(0x7800,0x7fff,NULL,Z80RAM+0x7800);
   AddZ80AWriteByte(0x8000,0xffff,DefBadWriteZ80,     NULL);
   
   // ports

   AddZ80AReadPort(0x0, 0x0,YM2610_status_port_0_A_r ,    NULL);
   AddZ80AReadPort(0x2, 0x2,YM2610_status_port_0_B_r ,    NULL);
   AddZ80AReadPort(0x8, 0x8,soundlatch_r,    NULL);

   AddZ80AWritePort(0x0, 0x0, YM2610_control_port_0_A_w,    NULL);
   AddZ80AWritePort(0x1, 0x1, YM2610_data_port_0_A_w,    NULL);
   AddZ80AWritePort(0x2, 0x2, YM2610_control_port_0_B_w,    NULL);
   AddZ80AWritePort(0x3, 0x3, YM2610_data_port_0_B_w,    NULL);
   AddZ80AWritePort(0x4, 0x4, sngkace_sound_bankswitch_w,    NULL);
   AddZ80AWritePort(0xc, 0xc, psikyo_ack_latch_w,    NULL);

   finish_z80();
   z80_8200 = Z80ROM+0x10000;
}

void LoadGunbird(void)
{
   int ta;

   RAMSize = 0x70000;

   if(!(ROM=AllocateMem(0x80000)))     return;
   if(!(RAM=AllocateMem(RAMSize)))     return;
   if(!(GFX=AllocateMem(0x1200000)))   return;
   if(!(LUT=AllocateMem(0x40000)))     return;  // sprites lookup table 
   if(!(TMP=AllocateMem(0x200000)))    return;
   if(!(Z80ROM=AllocateMem(0x30000)))  return;
   if(!(PCMROM=AllocateMem(0x180000))) return;   
   Z80RAM = Z80ROM;
   
   /* 68020 ROMs */	
   if(!load_rom("1-u46.bin", TMP+0x00000, 0x40000)) return;
   if(!load_rom("2-u39.bin", TMP+0x40000, 0x40000)) return;   
   for(ta=0;ta<0x40000;ta+=2){
      ROM[(ta<<1)+0] = TMP[ta+1];
      ROM[(ta<<1)+1] = TMP[ta];
      ROM[(ta<<1)+2] = TMP[0x40000+ta+1];
      ROM[(ta<<1)+3] = TMP[0x40000+ta];
   }

   /* Z80 ROM + banking */
   if(!(load_rom("3-u71.bin",Z80ROM+0x00000,0x10000))) return;
   if(!(load_rom("3-u71.bin",Z80ROM+0x10000,0x20000))) return;

   /* DELTA-T samples + ADPCM samples */
   if(!(load_rom("u64.bin",PCMROM+0x00000,0x80000))) return;
   if(!(load_rom("u56.bin",PCMROM+0x80000,0x100000))) return;

   GunbirdDecode("u14.bin", 0x0000000, 0x200000);  // sprites (0xE000 tiles)	
   GunbirdDecode("u24.bin", 0x0400000, 0x200000);
   GunbirdDecode("u15.bin", 0x0800000, 0x200000); 		
   GunbirdDecode("u25.bin", 0x0C00000, 0x100000);   
   GunbirdDecode("u33.bin", 0x0E00000, 0x200000);  // layer 0 & 1 (0x4000 tiles)

   if(!load_rom("u3.bin", LUT, 0x40000)) return;   // sprites LUT
  
   for(ta=0;ta<0x1200000;ta++)
      GFX[ta]^=15;

   GFX_SPR = GFX;
   GFX_BG0 = GFX + 0x0E00000;
   GFX_BG1 = GFX + 0x1000000;   
   
   RAM_SPR = RAM + 0x00000;
   RAM_BG0 = RAM + 0x20000;
   RAM_BG1 = RAM + 0x22000;
   RAM_SPR_LST = RAM + 0x01800;

   GFX_SPR_SOLID = make_solid_mask_16x16( GFX_SPR, 0xE000  ); 
   GFX_BG0_SOLID = make_solid_mask_16x16( GFX_BG0, 0x2000  );
   GFX_BG1_SOLID = make_solid_mask_16x16( GFX_BG1, 0x2000  );

   num_sprites = 0xE000;

   InitPaletteMap(RAM+0x10000, 0x100, 0x10, 0x8000);  // not all used
   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb_rev_68k);
   
   init_16x16_zoom();

   memset(RAM+0x00000,0x00,RAMSize);
   memset(RAM+0x30000,0xFF,0x10);      

   RAM[0x30003] = 0x7f;  // bit 8 is sound related and must be 0 if no sound is used, otherwise
                         // the game hangs after the startup screen
   RAM[0x30007] = 0x7F;  // bit 8 is vblank: if 0 the games hangs at startup


   // Setup Z80 memory map
   // --------------------

   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);


   setup_z80_gunbird();

   /* ----------- */
   
   // DELTA-T, samples
   YM2610SetBuffers(PCMROM+0x00000, PCMROM+0x80000, 0x80000, 0x100000);

   // Sound (I can't make the 2 Read/Write handlers to work)
   //   AddZ80AReadByte(0x30012,0x30013,gunbird_input_r,NULL); 
   //   AddZ80AWriteByte(0x3012,0x30013,SoundWrite,NULL);
   

  // Main CPU: 68EC020

   for(ta=0;ta<0x100;ta++)
      R24[ta]=RAM+0x60000;	// SCRATCH RAM

   R24[0x40] = RAM+0x00000;     // Sprites data & Sprites list ($400000-$401FFF)
   R24[0x60] = RAM+0x10000;     // Palette ($600000-$601FFF)
   R24[0x80] = RAM+0x20000;     // Layer 0 and Layer 1 ($800000-$803FFF)
                                // RAM and Vregs ($804000-$807FFF)
   R24[0xC0] = RAM+0x30000;     // Input ports ($C00000-$C0000B) and Sound related
   R24[0xFE] = RAM+0x40000;     // RAM
   R24[0xFF] = RAM+0x50000;

   for(ta=0;ta<0x100;ta++)
      W24[ta]=R24[ta];		// copy READ -> WRITE

   for(ta=0;ta<8;ta++)
      R24[ta]=ROM+ta*0x10000;	// 68020 ROM

   init_m68k();

   FreeMem(TMP);
}

void Loadbtlkrodj(void)
{
   int ta;

   RAMSize = 0x70000;

   if(!(ROM=AllocateMem(0x80000)))     return;
   if(!(RAM=AllocateMem(RAMSize)))     return;
   if(!(GFX=AllocateMem(0x1200000)))   return;
   if(!(LUT=AllocateMem(0x40000)))     return;  // sprites lookup table 
   if(!(TMP=AllocateMem(0x200000)))    return;
   if(!(Z80ROM=AllocateMem(0x30000)))  return;
   if(!(PCMROM=AllocateMem(0x180000))) return;   
   Z80RAM = Z80ROM;
   
   /* 68020 ROMs */	
   if(!load_rom("4-u46.bin", TMP+0x00000, 0x40000)) return;
   if(!load_rom("5-u39.bin", TMP+0x40000, 0x40000)) return;   
   for(ta=0;ta<0x40000;ta+=2){
      ROM[(ta<<1)+0] = TMP[ta+1];
      ROM[(ta<<1)+1] = TMP[ta];
      ROM[(ta<<1)+2] = TMP[0x40000+ta+1];
      ROM[(ta<<1)+3] = TMP[0x40000+ta];
   }

   /* Z80 ROM + banking */
   if(!(load_rom("3-u71.bin",Z80ROM+0x00000,0x10000))) return;
   if(!(load_rom("3-u71.bin",Z80ROM+0x10000,0x20000))) return;

   /* DELTA-T samples + ADPCM samples */
   if(!(load_rom("u64.bin",PCMROM+0x00000,0x80000))) return;
   if(!(load_rom("u56.bin",PCMROM+0x80000,0x100000))) return;

   GunbirdDecode("u14.bin", 0x0000000, 0x200000);  // sprites (0xE000 tiles)	
   GunbirdDecode("u24.bin", 0x0400000, 0x200000);
   GunbirdDecode("u15.bin", 0x0800000, 0x200000); 		
   //   GunbirdDecode("u25.bin", 0x0c00000, 0x100000);  // Not present (try gunbird)
   GunbirdDecode("u33.bin", 0x0e00000, 0x200000);  // layer 0 & 1 (0x4000 tiles)

   if(!load_rom("u3.bin", LUT, 0x40000)) return;   // sprites LUT
  
   for(ta=0;ta<0x1200000;ta++)
      GFX[ta]^=15;

   GFX_SPR = GFX;
   GFX_BG0 = GFX + 0x0E00000;
   GFX_BG1 = GFX + 0x1000000;   
   
   RAM_SPR = RAM + 0x00000;
   RAM_BG0 = RAM + 0x20000;
   RAM_BG1 = RAM + 0x22000;
   RAM_SPR_LST = RAM + 0x01800;

   GFX_SPR_SOLID = make_solid_mask_16x16( GFX_SPR, 0xE000  ); 
   GFX_BG0_SOLID = make_solid_mask_16x16( GFX_BG0, 0x2000  );
   GFX_BG1_SOLID = make_solid_mask_16x16( GFX_BG1, 0x2000  );

   num_sprites = 0xE000;

   InitPaletteMap(RAM+0x10000, 0x100, 0x10, 0x8000);  // not all used
   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb_rev_68k);
   
   init_16x16_zoom();

   memset(RAM+0x00000,0x00,RAMSize);
   memset(RAM+0x30000,0xFF,0x10);      

   RAM[0x30003] = 0x7f;  // bit 8 is sound related and must be 0 if no sound is used, otherwise
                         // the game hangs after the startup screen
   RAM[0x30007] &= 0x7F;  // bit 8 is vblank: if 0 the games hangs at startup


   // Setup Z80 memory map
   // --------------------

   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);

   setup_z80_gunbird();

   /* ----------- */
   
   // DELTA-T, samples
   YM2610SetBuffers(PCMROM+0x00000, PCMROM+0x80000, 0x80000, 0x100000);

   // Sound (I can't make the 2 Read/Write handlers to work)
   //   AddZ80AReadByte(0x30012,0x30013,gunbird_input_r,NULL); 
   //   AddZ80AWriteByte(0x3012,0x30013,SoundWrite,NULL);
   

  // Main CPU: 68EC020

   for(ta=0;ta<0x100;ta++)
      R24[ta]=RAM+0x60000;	// SCRATCH RAM

   R24[0x40] = RAM+0x00000;     // Sprites data & Sprites list ($400000-$401FFF)
   R24[0x60] = RAM+0x10000;     // Palette ($600000-$601FFF)
   R24[0x80] = RAM+0x20000;     // Layer 0 and Layer 1 ($800000-$803FFF)
                                // RAM and Vregs ($804000-$807FFF)
   R24[0xC0] = RAM+0x30000;     // Input ports ($C00000-$C0000B) and Sound related
   R24[0xFE] = RAM+0x40000;     // RAM
   R24[0xFF] = RAM+0x50000;

   for(ta=0;ta<0x100;ta++)
      W24[ta]=R24[ta];		// copy READ -> WRITE

   W24[0xc0] = RAM+0x60000;	// SCRATCH RAM
   for(ta=0;ta<8;ta++)
      R24[ta]=ROM+ta*0x10000;	// 68020 ROM

   init_m68k();

   FreeMem(TMP);
}

void LoadSngkace(void)
{
   int ta;
   int i;
   //FILE *f;
   RAMSize = 0x70000;

   if(!(ROM=AllocateMem(0x80000)))     return;
   if(!(RAM=AllocateMem(RAMSize)))     return;
   if(!(GFX=AllocateMem(0x800000)))    return;
   if(!(LUT=AllocateMem(0x40000)))     return;  // sprites lookup table 
   if(!(TMP=AllocateMem(0x200000)))    return;
   if(!(Z80ROM=AllocateMem(0x30000)))  return;
   if(!(PCMROM=AllocateMem(0x100000))) return;   
   Z80RAM=Z80ROM;
   
   /* 68020 ROMs */	
   if(!load_rom("1-u127.bin", TMP+0x00000, 0x40000)) return;
   if(!load_rom("2-u126.bin", TMP+0x40000, 0x40000)) return;   
   for(ta=0;ta<0x40000;ta+=2){
      ROM[(ta<<1)+0] = TMP[ta+1];
      ROM[(ta<<1)+1] = TMP[ta];
      ROM[(ta<<1)+2] = TMP[0x40000+ta+1];
      ROM[(ta<<1)+3] = TMP[0x40000+ta];
   }
   /* Z80 ROM + banking */
   if(!(load_rom("3-u58.bin",Z80ROM+0x00000,0x10000))) return;
   if(!(load_rom("3-u58.bin",Z80ROM+0x10000,0x20000))) return;

   /* DELTA-T samples + ADPCM samples */
   if(!(load_rom("u68.bin",PCMROM+0x00000,0x100000))) return;

   /* Bit 6&7 of the samples are swapped. Naughty, naughty... */
   for (i=0;i<0x100000;i++) 
     {
       int x = PCMROM[i];
       PCMROM[i] = ((x & 0x40) << 1) | ((x & 0x80) >> 1) | (x & 0x3f);
     }

   // Setup Z80 memory map
   // --------------------

   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);

   setup_z80_sngkace();

   //   f=fopen("sound.dat","wb");
   //   fwrite(PCMROM,0x100000,1,f);
   //   fclose(f);
   YM2610SetBuffers(PCMROM+0x00000, PCMROM+0x0000, 0x100000, 0x100000);

   GunbirdDecode("u14.bin", 0x000000, 0x200000);  // sprites (0x4000 tiles)	
   GunbirdDecode("u34.bin", 0x400000, 0x100000);  // layer 0 & 1
   GunbirdDecode("u35.bin", 0x600000, 0x100000); 		
  
   if(!load_rom("u11.bin", LUT, 0x40000)) return;   // sprites LUT
  
   for(ta=0;ta<0x800000;ta++)
      GFX[ta]^=15;

   GFX_SPR = GFX;
   GFX_BG0 = GFX + 0x400000;
   GFX_BG1 = GFX + 0x600000;   
   
   RAM_SPR = RAM + 0x00000;
   RAM_BG0 = RAM + 0x20000;
   RAM_BG1 = RAM + 0x22000;
   RAM_SPR_LST = RAM + 0x01800;

   GFX_SPR_SOLID = make_solid_mask_16x16( GFX_SPR, 0x4000  ); 
   GFX_BG0_SOLID = make_solid_mask_16x16( GFX_BG0, 0x2000  );
   GFX_BG1_SOLID = make_solid_mask_16x16( GFX_BG1, 0x2000  );

   num_sprites = 0x4000;

   InitPaletteMap(RAM+0x10000, 0x100, 0x10, 0x8000);  // not all used
   set_colour_mapper(&col_map_xrrr_rrgg_gggb_bbbb_rev_68k);

   init_16x16_zoom();

   memset(RAM+0x00000,0x00,RAMSize);
   memset(RAM+0x30000,0x00,0x10);      

   RAM[0x30009] = 0x7f;  // bit 8 is sound related and must be 0 if no sound is used, otherwise
                         // the game hangs after the startup screen
   RAM[0x3000B] = 0xfe;  // bit 1 is vblank: if 0 the games hangs at startup


  // Main CPU: 68EC020

   for(ta=0;ta<0x100;ta++)
      R24[ta]=RAM+0x60000;	// SCRATCH RAM

   R24[0x40] = RAM+0x00000;     // Sprites data & Sprites list ($400000-$401FFF)
   R24[0x60] = RAM+0x10000;     // Palette ($600000-$601FFF)
   R24[0x80] = RAM+0x20000;     // Layer 0 and Layer 1 ($800000-$803FFF)
                                // RAM and Vregs ($804000-$807FFF)
   R24[0xC0] = RAM+0x30000;     // Input ports ($C00000-$C0000B) and Sound related
   R24[0xFE] = RAM+0x40000;     // RAM
   R24[0xFF] = RAM+0x50000;

   for(ta=0;ta<0x100;ta++)
      W24[ta]=R24[ta];		// copy READ -> WRITE

   for(ta=0;ta<8;ta++)
      R24[ta]=ROM+ta*0x10000;	// 68020 ROM

   init_m68k();


   FreeMem(TMP);
}

#define MAX_SLICE 50

void ExecuteGunbirdFrame(void)
{
  int slice;
  for (slice=0; slice < MAX_SLICE; slice++) {
   // DEFAULT_REAL_60HZ_VBLANK_DURATION = 2500

   // cpu_execute_cycles(cpu,cycles);
   // CPU_FRAME_MHZ(speed,slice);
   // cpu_interrupt(cpu,vector);  in pratica vector � il vblank interrupt (vedi BloodBros e WestStory)
   // CPU_FRAME_MHz(speed,slice) ((speed*1000000)/slice)


//   cpu_execute_cycles(CPU_M68020_0, 330000);
//   RAM[0x30007]^=0x80;  
//   cpu_execute_cycles(CPU_M68020_0, 2500);	// M68020 16MHz (60fps)   
//   RAM[0x30007]^=0x80;
//   cpu_interrupt(CPU_M68020_0, 1); 

    cpu_execute_cycles(CPU_M68020_0, CPU_FRAME_MHz(32,(60*MAX_SLICE)));	// M68020 16MHz (60fps)

    if (audio_sample_rate) { // The z80 is useless without sound !!!
      // The method bellow has just a problem : it can't detect 2
      // writes of the same value...
      cpu_execute_cycles(CPU_Z80_0, CPU_FRAME_MHz(4,(60*MAX_SLICE)));
      if (oldmem13 != RAM[0x30013]) {
	ack_latch = 1;
	latch = oldmem13 = RAM[0x30013];
	//       RAM[0x30013] = 0; // reset
	cpu_int_nmi(CPU_Z80_0);
      }
      cpu_interrupt(CPU_Z80_0, 0x38);
      
      //     if (want_int) {
      //       want_int=0;
      //     }
    }
  }
  cpu_interrupt(CPU_M68020_0, 1);
}

void ExecuteSengokuFrame(void)
{
   // DEFAULT_REAL_60HZ_VBLANK_DURATION = 2500

   cpu_execute_cycles(CPU_M68020_0, CPU_FRAME_MHz(32,60));	// M68020 16MHz (60fps)
   cpu_interrupt(CPU_M68020_0, 1);

}



/*
Uso di pdrawgfx in mame:

pdrawgfx(struct osd_bitmap *dest,const struct GfxElement *gfx,
        unsigned int code,unsigned int color,int flipx,int flipy,int sx,int sy,
        const struct rectangle *clip,int transparency,int transparent_color,UINT32 priority_mask)


La routine interna opera in questo modo:

   c = source[x][y];

   if( c != transparent_color )
   {
        if (((1 << priority_bitmap[x][y]) & priority_mask) == 0)
                dest[x][y] = pal[c];
        priority_bitmap[x][y] = 31;
   }

Il mame costruisce oltre alla bitmap dello schermo, un'altra bitmap dove, per ogni pixel, segna
il valore della priorit�. Consideriamo il caso in cui priority_mask = 0. Internamente il mame
esegue questa operazione: 

   priority_mask|=1<<31;

L'uso di pdraw consente di fare in modo che il primo sprite disegnato abbia la priorit� pi� 
elevata, mentre i seguenti si troveranno sotto al primo. Infatti la priority_bitmap � 
inizialemente impostata a zero. Dopo che si � disegnato uno sprite la priorit� diventa 31. Un
successivo sprite che si sovrappone al precedente non sar� disegnato per quei pixel che si 
sovrappongono.

Nel caso di gunbird si disegnao prima le tilemap che hanno priorit� 0 ed 1.
Ad esempio, l'uso di:

    tilemap_draw(bitmap, tilemap_1, (1 << 16)  );

imposta ad 1 tutti i bit della priority_bitmap che interessano il tilemap_1, indipendentemente
dal loro stato precedente.
Al termine del disegno delle tilemap, la priority_bitmap avr� valori 0 ed 1.
Con il pdraw gli sprite sono disegnati con i primi a priorit� pi� elevato e (quanto detto si
poteva realizzare anche disegnando gli sprite in ordine inverso) quelli che hanno priorit� 2
non sono disegnati 

In pratica, a parte l'ordine degli sprite, si poteva fare cos�:

    tilemap0
    sprite con priorit�
    tilemap1
    altri sprite
*/

static int zoomtable[16] = { 0/8,7/8,14/8,20/8,25/8,30/8,34/8,38/8,42/8,46/8,
			     49/8,52/8,54/8,57/8,59/8,61/8 };

void DrawSpritesP(int priority) 
{

   int zz,zzz,zzzz,x16,y16,x,y,ta,zoomx,zoomy;
   UINT8 *map;
   int attr, xstart, xend, ystart, yend, dx, dy, yinc, xinc;


   if(!(RAM_SPR[0x1FFF]&1)){

     for(zz=0;zz<0x7FE;zz+=2) {

         zzz = ReadWord68k(&RAM_SPR_LST[zz]);
         if( zzz==0xFFFF ) break;
         zzzz = (zzz%0x300) << 3;
   
         attr = ReadWord68k(&RAM_SPR[zzzz+4]);

         if( ((attr&0xC0)>>6)==priority ) {
            y = ReadWord68k(&RAM_SPR[zzzz]);
            x = ReadWord68k(&RAM_SPR[zzzz+2]);
            zzz = ReadWord68k(&RAM_SPR[zzzz+6])|((attr&1)<<16);


            y16 = (y&0xFF)-(y&0x100) + 32;
            x16 = (x&0x1FF);
            x16+= (x16>=0x180) ? (32-0x200) : 32; 

	    zoomx	=	((x & 0xf000) >> 12);
	    zoomy	=	((y & 0xf000) >> 12);
	    
	    zoomx = 16 - zoomtable[zoomx];
	    zoomy = 16 - zoomtable[zoomy];

            if(attr&0x4000){  // flipX
               xstart = (x>>9)&7;xend=-1;xinc=-1;
            } else {
               xstart = 0;xend=((x>>9)&7)+1;xinc=1;
            }
   
            if(attr&0x8000){  // flipY
               ystart = (y>>9)&7;yend=-1;yinc=-1;
            } else {
               ystart = 0;yend=((y>>9)&7)+1;yinc=1;
            }

            for(dy=ystart;dy!=yend;dy+=yinc) {
               for(dx=xstart;dx!=xend;dx+=xinc) {
                
                  y = y16 + dy*zoomy;

                  if( (y>16) && (y<224+32) ) {

                     x = x16 + dx*zoomx;

                     if( (x>16) && (x<320+32) ) {
                     
                        ta = ReadWord(&LUT[(zzz<<1)])%num_sprites;
                        
                        if(GFX_SPR_SOLID[ta]) {

                           MAP_PALETTE_MAPPED_NEW(
                              (attr>>8)&0x1F,
                              16,
                              map
                           );
                     
                           if(GFX_SPR_SOLID[ta]==1){			// Some pixels; trans
                              switch((attr>>8)&0xC0){
                                 case 0x00: Draw16x16_Trans_Mapped_ZoomXY_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy);        break;
                                 case 0x40: Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy);  break;
                                 case 0x80: Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy);  break;
                                 case 0xC0: Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy); break;
                              }
                           } else{						// all pixels; solid
                              switch((attr>>8)&0xC0){
                                 case 0x00: Draw16x16_Mapped_ZoomXY_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy);        break;
                                 case 0x40: Draw16x16_Mapped_ZoomXY_FlipY_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy);  break;
                                 case 0x80: Draw16x16_Mapped_ZoomXY_FlipX_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy);  break;
                                 case 0xC0: Draw16x16_Mapped_ZoomXY_FlipXY_Rot(&GFX_SPR[ta<<8], x, y, map, zoomx, zoomy); break;
                              }
                           }
                        }
                     }
                  }
               zzz++;
               }
            }
         }
      }
   }
}

void DrawSprites(void) 
{

   int zz,zzz,zzzz,x16,y16,x,y,ta;
   UINT8 *map;
   int attr, xstart, xend, ystart, yend, dx, dy, yinc, xinc;


   if(!(RAM_SPR[0x1FFF]&1)){

     for(zz=0;zz<0x7FE;zz+=2) {

         zzz = ReadWord68k(&RAM_SPR_LST[zz]);
         if( zzz==0xFFFF ) break;
         zzzz = (zzz%0x300) << 3;
   
         attr = ReadWord68k(&RAM_SPR[zzzz+4]);

         {
            y = ReadWord68k(&RAM_SPR[zzzz]);
            x = ReadWord68k(&RAM_SPR[zzzz+2]);
            zzz = ReadWord68k(&RAM_SPR[zzzz+6])|((attr&1)<<16);


            y16 = (y&0xFF)-(y&0x100) + 32;
            x16 = (x&0x1FF);
            x16+= (x16>=0x180) ? (32-0x200) : 32; 
	      
            if(attr&0x4000){  // flipX
               xstart = (x>>9)&7;xend=-1;xinc=-1;
            } else {
               xstart = 0;xend=((x>>9)&7)+1;xinc=1;
            }
   
            if(attr&0x8000){  // flipY
               ystart = (y>>9)&7;yend=-1;yinc=-1;
            } else {
               ystart = 0;yend=((y>>9)&7)+1;yinc=1;
            }

            for(dy=ystart;dy!=yend;dy+=yinc) {
               for(dx=xstart;dx!=xend;dx+=xinc) {
                
                  y = y16 + dy*16;

                  if( (y>16) && (y<224+32) ) {

                     x = x16 + dx*16;

                     if( (x>16) && (x<320+32) ) {
                     
                        ta = ReadWord(&LUT[(zzz<<1)])%num_sprites;
                        
                        if(GFX_SPR_SOLID[ta]) {

                           MAP_PALETTE_MAPPED_NEW(
                              (attr>>8)&0x1F,
                              16,
                              map
                           );
                     
                           if(GFX_SPR_SOLID[ta]==1){			// Some pixels; trans
                              switch((attr>>8)&0xC0){
                                 case 0x00: Draw16x16_Trans_Mapped_Rot(&GFX_SPR[ta<<8], x, y, map);        break;
                                 case 0x40: Draw16x16_Trans_Mapped_FlipY_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
                                 case 0x80: Draw16x16_Trans_Mapped_FlipX_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
                                 case 0xC0: Draw16x16_Trans_Mapped_FlipXY_Rot(&GFX_SPR[ta<<8], x, y, map); break;
                              }
                           } else{						// all pixels; solid
                              switch((attr>>8)&0xC0){
                                 case 0x00: Draw16x16_Mapped_Rot(&GFX_SPR[ta<<8], x, y, map);        break;
                                 case 0x40: Draw16x16_Mapped_FlipY_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
                                 case 0x80: Draw16x16_Mapped_FlipX_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
                                 case 0xC0: Draw16x16_Mapped_FlipXY_Rot(&GFX_SPR[ta<<8], x, y, map); break;
                              }
                           }
                        }
                     }
                  }
               zzz++;
               }
            }
         }
      }
   }
}

static void scroll_2048_bg0() {
  int zz,zzz,zzzz,x16,y16,x,y,ta;
  UINT8 *map;
  MAKE_SCROLL_512x2048_2_16(
			    ReadWord68k(&RAM[0x24406]),
			    ReadWord68k(&RAM[0x24402])
			    );

  START_SCROLL_512x2048_2_16(32,32,320,224);

  ta = ReadWord68k( &RAM_BG0[zz] ) & 0x1FFF;
      
  if( GFX_BG0_SOLID[ta] ){		
         
    MAP_PALETTE_MAPPED_NEW(
              (RAM_BG0[zz] >> 5)|0x80,
              16,
              map
            );

    if(GFX_BG0_SOLID[ta]==1)
      Draw16x16_Trans_Mapped_Rot(&GFX_BG0[ta<<8], x, y, map);  
    else
      Draw16x16_Mapped_Rot(&GFX_BG0[ta<<8], x, y, map);               
  }

  END_SCROLL_512x2048_2_16();
}

static void scroll_1024_bg0() {
  int zz,zzz,zzzz,x16,y16,x,y,ta;
  UINT8 *map;
  MAKE_SCROLL_1024x1024_2_16(
         ReadWord68k(&RAM[0x24406]),
         ReadWord68k(&RAM[0x24402])
	 );

  START_SCROLL_1024x1024_2_16(32,32,320,224);

  ta = ReadWord68k( &RAM_BG0[zz] ) & 0x1FFF;
      
  if( GFX_BG0_SOLID[ta] ){		
         
    MAP_PALETTE_MAPPED_NEW(
              (RAM_BG0[zz] >> 5)|0x80,
              16,
              map
	      );

    if(GFX_BG0_SOLID[ta]==1)
      Draw16x16_Trans_Mapped_Rot(&GFX_BG0[ta<<8], x, y, map);  
    else
      Draw16x16_Mapped_Rot(&GFX_BG0[ta<<8], x, y, map);               
  }

  END_SCROLL_1024x1024_2_16();
}  

static void scroll_2048_bg1() {
  int zz,zzz,zzzz,x16,y16,x,y,ta;
  UINT8 *map;
  MAKE_SCROLL_512x2048_2_16(
			    ReadWord68k(&RAM[0x2440E]),
			    ReadWord68k(&RAM[0x2440A])
			    );

  
  START_SCROLL_512x2048_2_16(32,32,320,224);

  ta = ReadWord68k( &RAM_BG1[zz] ) & 0x1FFF;
       
  if( GFX_BG1_SOLID[ta] ){		
         
    MAP_PALETTE_MAPPED_NEW(
              (RAM_BG1[zz] >> 5)|0xC0,
              16,
              map
            );

    if(GFX_BG1_SOLID[ta]==1)
      Draw16x16_Trans_Mapped_Rot(&GFX_BG1[ta<<8], x, y, map);  
    else
      Draw16x16_Mapped_Rot(&GFX_BG1[ta<<8], x, y, map);        
  }

  END_SCROLL_512x2048_2_16();
}

static void scroll_1024_bg1() {
  int zz,zzz,zzzz,x16,y16,x,y,ta;
  UINT8 *map;
  MAKE_SCROLL_1024x1024_2_16(
         ReadWord68k(&RAM[0x2440E]),
         ReadWord68k(&RAM[0x2440A])
      );

  
  START_SCROLL_1024x1024_2_16(32,32,320,224);

  ta = ReadWord68k( &RAM_BG1[zz] ) & 0x1FFF;
       
  if( GFX_BG1_SOLID[ta] ){		
         
    MAP_PALETTE_MAPPED_NEW(
              (RAM_BG1[zz] >> 5)|0xC0,
              16,
              map
            );

    if(GFX_BG1_SOLID[ta]==1)
      Draw16x16_Trans_Mapped_Rot(&GFX_BG1[ta<<8], x, y, map);  
    else
      Draw16x16_Mapped_Rot(&GFX_BG1[ta<<8], x, y, map);        
  }

  END_SCROLL_1024x1024_2_16();
}

void DrawGunbird(void)
{   

  ClearPaletteMap();

   clear_game_screen(0);
   

   //    BG0		
   // ----------

   if(!(RAM[0x24413]&1)) {
     if((RAM[0x24412]&1)) { // dynmaic tile changing... Pfff !
       scroll_1024_bg0();
     } else
       scroll_2048_bg0();
   }

   DrawSpritesP(1);

   //    BG1	
   // ----------

   if(!(RAM[0x24417]&1)) {
     if((RAM[0x24416]&1)) { // dynmaic tile changing... Pfff !
       scroll_1024_bg1();
     } else
       scroll_2048_bg1();
   }
//   DrawSprites();
   DrawSpritesP(0); 

}
