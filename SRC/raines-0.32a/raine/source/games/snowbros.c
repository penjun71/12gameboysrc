/******************************************************************************/
/*                                                                            */
/*                  SNOW BROS/WINTER BOBBLE (C) 1990 TOAPLAN                  */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "snowbros.h"
#include "3812intf.h"
#include "sasound.h"		// sample support routines
#ifdef RAINE_DEBUG
#include "debug.h"
#endif

static struct DIR_INFO snow_bros_dirs[] =
{
   { "snow_bros", },
   { "snowbros", },
   { NULL, },
};

static struct ROM_INFO snow_bros_roms[] =
{
   {      "sn6.bin", 0x00020000, 0x4899ddcf, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {      "sn5.bin", 0x00020000, 0xad310d3f, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "snowbros.4", 0x00008000, 0xe6eab4e4, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {          "ch0", 0x00020000, 0x36d84dfe, REGION_GFX1, 0x000000, LOAD_NORMAL, },
   {          "ch1", 0x00020000, 0x76347256, REGION_GFX1, 0x020000, LOAD_NORMAL, },
   {          "ch2", 0x00020000, 0xfdaa634c, REGION_GFX1, 0x040000, LOAD_NORMAL, },
   {          "ch3", 0x00020000, 0x34024aef, REGION_GFX1, 0x060000, LOAD_NORMAL, },
   {           NULL,          0,          0,           0,        0,           0, },
};

static struct INPUT_INFO snow_bros_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x006405, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x006405, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x006405, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P1_START,     MSG_P1_START,            0x006405, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x006401, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x006401, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x006401, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x006401, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x006401, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x006401, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_START,     MSG_P2_START,            0x006405, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x006403, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x006403, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x006403, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x006403, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x006403, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x006403, 0x20, BIT_ACTIVE_0 },
   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_snow_bros_0[] =
{
   { "Country",               0x01, 0x02 },
   { "America",               0x01, 0x00 },
   { "Europe",                0x00, 0x00 },
   { MSG_SCREEN,              0x02, 0x02 },
   { MSG_NORMAL,              0x02, 0x00 },
   { MSG_INVERT,              0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { "1/1",                   0x30, 0x00 },
   { "1/2; 2/1",              0x20, 0x00 },
   { "2/1; 3/1",              0x10, 0x00 },
   { "2/3; 4/1",              0x00, 0x00 },
   { MSG_COIN2,               0xC0, 0x04 },
   { "1/1",                   0xC0, 0x00 },
   { "1/2; 1/3",              0x80, 0x00 },
   { "2/1; 1/4",              0x40, 0x00 },
   { "2/3; 1/6",              0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_snow_bros_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { "Extra Life at",         0x0C, 0x04 },
   { "100k",                  0x0C, 0x00 },
   { "200k",                  0x08, 0x00 },
   { "100k 200k",             0x04, 0x00 },
   { "Nothing",               0x00, 0x00 },
   { "Lives",                 0x30, 0x04 },
   { "3",                     0x30, 0x00 },
   { "1",                     0x20, 0x00 },
   { "4",                     0x10, 0x00 },
   { "2",                     0x00, 0x00 },
   { "Cheat",                 0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "Continue",              0x80, 0x02 },
   { MSG_ON,                  0x80, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_INFO snow_bros_dsw[] =
{
   { 0x006400, 0xFF, dsw_data_snow_bros_0 },
   { 0x006402, 0xFF, dsw_data_snow_bros_1 },
   { 0,        0,    NULL,      },
};

static struct GFX_LAYOUT snow_bros_object =
{
   16, 16,
   RGN_FRAC(1,1),
   4,
   { 0, 1, 2, 3 },
   { STEP8(0, 4 ), STEP8(8*32,  4 ) },
   { STEP8(0, 32), STEP8(16*32, 32) },
   32*32,
};

static struct GFX_LIST snow_bros_gfx[] =
{
   { REGION_GFX1, &snow_bros_object, },
   { 0,           NULL,              },
};

static struct VIDEO_INFO snow_bros_video =
{
   draw_snow_bros,
   256,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
   snow_bros_gfx,
};

static struct YM3812interface ym3812_interface =
{
   1,              // 1 chip
   3600000,        // 3.6 MHz
   { 255 },        // Volume (emu only)
   { NULL }
};

static struct SOUND_INFO snow_bros_sound[] =
{
   { SOUND_YM3812,  &ym3812_interface,    },
   { 0,             NULL,                 },
};

struct GAME_MAIN game_snow_bros =
{
   snow_bros_dirs,
   snow_bros_roms,
   snow_bros_inputs,
   snow_bros_dsw,
   NULL,

   load_snow_bros,
   clear_snow_bros,
   &snow_bros_video,
   execute_snow_bros_frame,
   "snowbros",
   "Snow Bros",
   "スノーブラザーズ American",
   COMPANY_ID_TOAPLAN,
   NULL,
   1990,
   snow_bros_sound,
   GAME_PLATFORM,
};

static struct DIR_INFO snow_bros_alt_dirs[] =
{
   { "snow_bros_alt", },
   { "snowbroa", },
   { ROMOF("snowbros"), },
   { CLONEOF("snowbros"), },
   { NULL, },
};

static struct ROM_INFO snow_bros_alt_roms[] =
{
   {  "snowbros.3a", 0x00020000, 0x10cb37e1, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {  "snowbros.2a", 0x00020000, 0xab91cc1e, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "snowbros.4", 0x00008000, 0xe6eab4e4, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {          "ch0", 0x00020000, 0x36d84dfe, REGION_GFX1, 0x000000, LOAD_NORMAL, },
   {          "ch1", 0x00020000, 0x76347256, REGION_GFX1, 0x020000, LOAD_NORMAL, },
   {          "ch2", 0x00020000, 0xfdaa634c, REGION_GFX1, 0x040000, LOAD_NORMAL, },
   {          "ch3", 0x00020000, 0x34024aef, REGION_GFX1, 0x060000, LOAD_NORMAL, },
   {           NULL,          0,          0,           0,        0,           0, },
};

struct GAME_MAIN game_snow_bros_alt =
{
   snow_bros_alt_dirs,
   snow_bros_alt_roms,
   snow_bros_inputs,
   snow_bros_dsw,
   NULL,

   load_snow_bros,
   clear_snow_bros,
   &snow_bros_video,
   execute_snow_bros_frame,
   "snowbroa",
   "Snow Bros (alternate)",
   "スノーブラザーズ (alternate)",
   COMPANY_ID_TOAPLAN,
   NULL,
   1990,
   snow_bros_sound,
   GAME_PLATFORM,
};

static struct DIR_INFO snow_bros_japanese_dirs[] =
{
   { "snow_bros_japanese", },
   { "snowbroj", },
   { ROMOF("snowbros"), },
   { CLONEOF("snowbros"), },
   { NULL, },
};

static struct ROM_INFO snow_bros_japanese_roms[] =
{
   {   "snowbros.3", 0x00020000, 0x3f504f9e, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {   "snowbros.2", 0x00020000, 0x854b02bc, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "snowbros.4", 0x00008000, 0xe6eab4e4, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {          "ch0", 0x00020000, 0x36d84dfe, REGION_GFX1, 0x000000, LOAD_NORMAL, },
   {          "ch1", 0x00020000, 0x76347256, REGION_GFX1, 0x020000, LOAD_NORMAL, },
   {          "ch2", 0x00020000, 0xfdaa634c, REGION_GFX1, 0x040000, LOAD_NORMAL, },
   {          "ch3", 0x00020000, 0x34024aef, REGION_GFX1, 0x060000, LOAD_NORMAL, },
   {           NULL,          0,          0,           0,        0,           0, },
};

struct GAME_MAIN game_snow_bros_japanese =
{
   snow_bros_japanese_dirs,
   snow_bros_japanese_roms,
   snow_bros_inputs,
   snow_bros_dsw,
   NULL,

   load_snow_bros,
   clear_snow_bros,
   &snow_bros_video,
   execute_snow_bros_frame,
   "snowbroj",
   "Snow Bros (Japanese)",
   "スノーブラザーズ",
   COMPANY_ID_TOAPLAN,
   NULL,
   1990,
   snow_bros_sound,
   GAME_PLATFORM,
};

static struct DIR_INFO snow_bros_alt_2_dirs[] =
{
   { "snow_bros_alt_2", },
   { "snowbra2", },
   { "snowbrob", },
   { ROMOF("snowbros"), },
   { CLONEOF("snowbros"), },
   { NULL, },
};

static struct ROM_INFO snow_bros_alt_2_roms[] =
{
   {     "sbros3-a", 0x00020000, 0x301627d6, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {     "sbros2-a", 0x00020000, 0xf6689f41, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {   "snowbros.4", 0x00008000, 0xe6eab4e4, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {          "ch0", 0x00020000, 0x36d84dfe, REGION_GFX1, 0x000000, LOAD_NORMAL, },
   {          "ch1", 0x00020000, 0x76347256, REGION_GFX1, 0x020000, LOAD_NORMAL, },
   {          "ch2", 0x00020000, 0xfdaa634c, REGION_GFX1, 0x040000, LOAD_NORMAL, },
   {          "ch3", 0x00020000, 0x34024aef, REGION_GFX1, 0x060000, LOAD_NORMAL, },
   {           NULL,          0,          0, 0, 0, 0, },
};

struct GAME_MAIN game_snow_bros_alt_2 =
{
   snow_bros_alt_2_dirs,
   snow_bros_alt_2_roms,
   snow_bros_inputs,
   snow_bros_dsw,
   NULL,

   load_snow_bros,
   clear_snow_bros,
   &snow_bros_video,
   execute_snow_bros_frame,
   "snowbrob",
   "Snow Bros (alternate 2)",
   "スノーブラザーズ (alternate 2)",
   COMPANY_ID_TOAPLAN,
   "MIN16-02",
   1990,
   snow_bros_sound,
   GAME_PLATFORM,
};

static struct DIR_INFO winter_bobble_dirs[] =
{
   { "winter_bobble", },
   { "wintbob", },
   { ROMOF("snowbros"), },
   { CLONEOF("snowbros"), },
   { NULL, },
};

static struct ROM_INFO winter_bobble_roms[] =
{
   {     "wb03.bin", 0x00010000, 0xdf56e168, REGION_ROM1, 0x000000, LOAD_8_16,   },
   {     "wb01.bin", 0x00010000, 0x05722f17, REGION_ROM1, 0x000001, LOAD_8_16,   },
   {     "wb04.bin", 0x00010000, 0x53be758d, REGION_ROM1, 0x020000, LOAD_8_16,   },
   {     "wb02.bin", 0x00010000, 0xfc8e292e, REGION_ROM1, 0x020001, LOAD_8_16,   },
   {     "wb05.bin", 0x00010000, 0x53fe59df, REGION_ROM2, 0x000000, LOAD_NORMAL, },
   {     "wb06.bin", 0x00010000, 0x68204937, REGION_GFX1, 0x000001, LOAD_8_16,   },
   {     "wb07.bin", 0x00010000, 0x53f40978, REGION_GFX1, 0x020001, LOAD_8_16,   },
   {     "wb08.bin", 0x00010000, 0x9497b88c, REGION_GFX1, 0x040001, LOAD_8_16,   },
   {     "wb09.bin", 0x00010000, 0x9be718ca, REGION_GFX1, 0x060001, LOAD_8_16,   },
   {     "wb10.bin", 0x00010000, 0x5fa22b1e, REGION_GFX1, 0x060000, LOAD_8_16,   },
   {     "wb11.bin", 0x00010000, 0x41cb4563, REGION_GFX1, 0x040000, LOAD_8_16,   },
   {     "wb12.bin", 0x00010000, 0xef4e04c7, REGION_GFX1, 0x020000, LOAD_8_16,   },
   {     "wb13.bin", 0x00010000, 0x426921de, REGION_GFX1, 0x000000, LOAD_8_16,   },
   {           NULL,          0,          0,           0,        0,         0,   },
};

static struct GFX_LAYOUT winter_bobble_object =
{
   16,16,
   RGN_FRAC(1,1),
   4,
   { 0, 1, 2, 3 },
   { STEP4(3*4,-4), STEP4(7*4,-4), STEP4(11*4,-4), STEP4(15*4,-4) },
   { STEP16(0,64) },
   16*64
};

static struct GFX_LIST winter_bobble_gfx[] =
{
   { REGION_GFX1, &winter_bobble_object, },
   { 0,           NULL,                  },
};

static struct VIDEO_INFO winter_bobble_video =
{
   draw_snow_bros,
   256,
   224,
   32,
   VIDEO_ROTATE_NORMAL |
   VIDEO_ROTATABLE,
   winter_bobble_gfx,
};

struct GAME_MAIN game_winter_bobble =
{
   winter_bobble_dirs,
   winter_bobble_roms,
   snow_bros_inputs,
   snow_bros_dsw,
   NULL,

   load_snow_bros,
   clear_snow_bros,
   &winter_bobble_video,
   execute_snow_bros_frame,
   "wintbob",
   "Winter Bobble",
   "スノーブラザーズ（海ｵｯ�ﾅ）",
   COMPANY_ID_BOOTLEG,
   NULL,
   1990,
   snow_bros_sound,
   GAME_PLATFORM,
};

static UINT8 *RAM_SPR;
static UINT8 *GFX_SPR;
static UINT8 *GFX_SPR_SOLID;

static int sport=0;

static void SoundWrite(UINT32 offset, UINT8 data)
{
   sport = data;

#ifdef RAINE_DEBUG
   print_debug("68000 Sends $%02x\n",sport);
#endif
   cpu_int_nmi(CPU_Z80_0);
}

static UINT16 SoundRead(UINT32 offset)
{
   return 3;           // Z80_OK
}

void SnowBrosPort4w(UINT16 address, UINT8 data)
{
   sport=data;
}

UINT16 SnowBrosPort4r(UINT16 address)
{
   #ifdef RAINE_DEBUG
   print_debug("Z80 Receives $%02x\n",sport);
   #endif
   return(sport);
}


void load_snow_bros(void)
{
   RAMSize=0x8000+0x10000;

   if(!(RAM=AllocateMem(RAMSize))) return;

   Z80ROM=RAM+0x8000;

   memcpy(Z80ROM+0x0000, load_region[REGION_ROM2], 0x8000);
   memset(Z80ROM+0x8000, 0x00,                     0x8000);

   // Fix Checksum
   // ------------

   Z80ROM[0x0156]=0x00; // NOP
   Z80ROM[0x0157]=0x00; // NOP
   Z80ROM[0x0158]=0x00; // NOP

   // Apply Speed Patch
   // -----------------

   if(is_current_game("wintbob"))
   {
      Z80ROM[0x0181]=0xD3; // OUTA (AAh)
      Z80ROM[0x0182]=0xAA; //

      SetStopZ80Mode2(0x0180);
   }
   else
   {
      Z80ROM[0x019D]=0xD3; // OUTA (AAh)
      Z80ROM[0x019E]=0xAA; //

      SetStopZ80Mode2(0x019C);
   }

   // Setup Z80 memory map
   // --------------------

   AddZ80AROMBase(Z80ROM, 0x0038, 0x0066);

   AddZ80AReadByte(0x0000, 0x87FF, NULL,                        Z80ROM+0x0000); // Z80 ROM/RAM
   AddZ80AReadByte(0x0000, 0xFFFF, DefBadReadZ80,               NULL);
   AddZ80AReadByte(    -1,     -1, NULL,                        NULL);

   AddZ80AWriteByte(0x8000, 0x87FF, NULL,                       Z80ROM+0x8000); // Z80 RAM
   AddZ80AWriteByte(0x0000, 0xFFFF, DefBadWriteZ80,             NULL);
   AddZ80AWriteByte(    -1,     -1, NULL,                       NULL);

   AddZ80AReadPort(0x02, 0x03, YM3812ReadZ80,           NULL);
   AddZ80AReadPort(0x04, 0x04, SnowBrosPort4r,          NULL);
   AddZ80AReadPort(0x00, 0xFF, DefBadReadZ80,           NULL);
   AddZ80AReadPort(  -1,   -1, NULL,                    NULL);

   AddZ80AWritePort(0x02, 0x03, YM3812WriteZ80,         NULL);
   AddZ80AWritePort(0x04, 0x04, SnowBrosPort4w,         NULL);
   AddZ80AWritePort(0xAA, 0xAA, StopZ80Mode2,           NULL);
   AddZ80AWritePort(0x00, 0xFF, DefBadWriteZ80,         NULL);
   AddZ80AWritePort(  -1,   -1, NULL,                   NULL);

   AddZ80AInit();

   RAM_SPR = RAM+0x004000;
   GFX_SPR = load_region[REGION_GFX1];
   GFX_SPR_SOLID = make_solid_mask_16x16(GFX_SPR, 0x1000);

   memset(RAM+0x0000,0x00,0x6404);
   memset(RAM+0x6404,0xFF,0x001C);

   RAM[0x6400]=0x7F;
   RAM[0x6402]=0x7F;

   RAM[0x6412]=0x00;    // $300000: Sound Related
   RAM[0x6413]=0x03;

   // fix checksum

   WriteLong68k(&ROM[0x008EE],0x4E714E71);
   WriteLong68k(&ROM[0x008FE],0x4E714E71);

   // speed hack

   WriteLong68k(&ROM[0x003BA],0x027C7BFF);
   WriteLong68k(&ROM[0x003BE],0x13FC0000);
   WriteLong68k(&ROM[0x003C2],0x00AA0000);

   WriteLong68k(&ROM[0x00436],0x027C7AFF);
   WriteLong68k(&ROM[0x0043A],0x13FC0000);
   WriteLong68k(&ROM[0x0043E],0x00AA0000);

   WriteLong68k(&ROM[0x0045E],0x027C79FF);
   WriteLong68k(&ROM[0x00462],0x13FC0000);
   WriteLong68k(&ROM[0x00466],0x00AA0000);

   if(is_current_game("wintbob"))
   {
      WriteWord68k(&ROM[0x00DE],0x4EB9);
      WriteLong68k(&ROM[0x00E0],0x0000065A);

      WriteWord68k(&ROM[0x0142],0x4EB9);
      WriteLong68k(&ROM[0x0144],0x0000066C);

      WriteWord68k(&ROM[0x08B8],0x4EB9);
      WriteLong68k(&ROM[0x08BA],0x000009F4);
   }

   InitPaletteMap(RAM+0x06000, 0x10, 0x10, 0x8000);

   set_colour_mapper(&col_map_xbbb_bbgg_gggr_rrrr);

/*
 *  StarScream Main 68000 Setup
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x6420);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);                 // 68000 ROM
   AddReadByte(0x100000, 0x103FFF, NULL, RAM+0x000000);                 // 68000 RAM
   AddReadByte(0x700000, 0x701FFF, NULL, RAM+0x004000);                 // SPRITE RAM
   AddReadByte(0x600000, 0x6003FF, NULL, RAM+0x006000);                 // COLOUR RAM
   AddReadByte(0x500000, 0x50000F, NULL, RAM+0x006400);                 // INPUT
   AddReadByte(0x200000, 0x200001, NULL, RAM+0x006410);                 // ???
   AddReadByte(0x300000, 0x300001, SoundRead, NULL);                    // SOUND COMM
   AddReadByte(0x400000, 0x400001, NULL, RAM+0x006414);                 // ???
   AddReadByte(0x800000, 0x800001, NULL, RAM+0x006416);                 // (Interrupt Repsonse)
   AddReadByte(0x900000, 0x900001, NULL, RAM+0x006418);                 // (Interrupt Repsonse)
   AddReadByte(0xA00000, 0xA00001, NULL, RAM+0x00641A);                 // (Interrupt Repsonse)
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);      // <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);                 // 68000 ROM
   AddReadWord(0x100000, 0x103FFF, NULL, RAM+0x000000);                 // 68000 RAM
   AddReadWord(0x700000, 0x701FFF, NULL, RAM+0x004000);
   AddReadWord(0x600000, 0x6003FF, NULL, RAM+0x006000);
   AddReadWord(0x500000, 0x50000F, NULL, RAM+0x006400);
   AddReadWord(0x200000, 0x200001, NULL, RAM+0x006410);
   AddReadWord(0x300000, 0x300001, SoundRead, NULL);
   AddReadWord(0x400000, 0x400001, NULL, RAM+0x006414);
   AddReadWord(0x800000, 0x800001, NULL, RAM+0x006416);
   AddReadWord(0x900000, 0x900001, NULL, RAM+0x006418);
   AddReadWord(0xA00000, 0xA00001, NULL, RAM+0x00641A);
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);      // <Bad Reads>
   AddReadWord(-1, -1, NULL, NULL);

   AddWriteByte(0x100000, 0x103FFF, NULL, RAM+0x000000);                // 68000 RAM
   AddWriteByte(0x700000, 0x701FFF, NULL, RAM+0x004000);
   AddWriteByte(0x600000, 0x6003FF, NULL, RAM+0x006000);
   AddWriteByte(0x500000, 0x50000F, NULL, RAM+0x006400);
   AddWriteByte(0x200000, 0x200001, NULL, RAM+0x006410);
   AddWriteByte(0x300000, 0x300001, SoundWrite, NULL);
   AddWriteByte(0x400000, 0x400001, NULL, RAM+0x006414);
   AddWriteByte(0x800000, 0x800001, NULL, RAM+0x006416);
   AddWriteByte(0x900000, 0x900001, NULL, RAM+0x006418);
   AddWriteByte(0xA00000, 0xA00001, NULL, RAM+0x00641A);
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);                   // Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);      // <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x100000, 0x103FFF, NULL, RAM+0x000000);                // 68000 RAM
   AddWriteWord(0x700000, 0x701FFF, NULL, RAM+0x004000);
   AddWriteWord(0x600000, 0x6003FF, NULL, RAM+0x006000);
   AddWriteWord(0x500000, 0x50000F, NULL, RAM+0x006400);
   AddWriteWord(0x200000, 0x200001, NULL, RAM+0x006410);
   AddWriteWord(0x300000, 0x300001, SoundWrite, NULL);
   AddWriteWord(0x400000, 0x400001, NULL, RAM+0x006414);
   AddWriteWord(0x800000, 0x800001, NULL, RAM+0x006416);
   AddWriteWord(0x900000, 0x900001, NULL, RAM+0x006418);
   AddWriteWord(0xA00000, 0xA00001, NULL, RAM+0x00641A);
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);      // <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();     // Set Starscream mem pointers...
}

void clear_snow_bros(void)
{
}

void execute_snow_bros_frame(void)
{
   static int ta;

   cpu_execute_cycles(CPU_68K_0, 80000);                 // Main 68000
   cpu_interrupt(CPU_68K_0, 4);
   cpu_execute_cycles(CPU_68K_0, 80000);
   cpu_interrupt(CPU_68K_0, 3);
   cpu_execute_cycles(CPU_68K_0, 80000);
   cpu_interrupt(CPU_68K_0, 2);

   ta++;

   cpu_execute_cycles(CPU_Z80_0, CPU_FRAME_MHz(4,60));        // Sound Z80
   /*#ifdef RAINE_DEBUG
   print_debug("%04x\n",z80pc);
   #endif*/
   cpu_interrupt(CPU_Z80_0, 0x38);
   if(ta&1) cpu_interrupt(CPU_Z80_0, 0x38);
}

void draw_snow_bros(void)
{
   int x,y,ta,tb;
   int zz;
   UINT8 *map;

   ClearPaletteMap();

   clear_game_screen(0);

   x=0;
   y=0;

   for(zz=0;zz<0x1E00;zz+=16){

      tb = RAM_SPR[zz+6];

      if(tb&4){
         x = (x + ((RAM_SPR[zz+ 8]) | ((tb&1)<<8))) & 0x1FF;
         y = (y + ((RAM_SPR[zz+10]) | ((tb&2)<<7))) & 0x1FF;
      }
      else{
         x = (32 + ((RAM_SPR[zz+ 8]) | ((tb&1)<<8))) & 0x1FF;
         y = (16 + ((RAM_SPR[zz+10]) | ((tb&2)<<7))) & 0x1FF;
      }

      if((x>16)&&(y>16)&&(x<256+32)&&(y<240+32)){

         ta = ((RAM_SPR[zz+12]) | (RAM_SPR[zz+14]<<8))&0x0FFF;

         if(GFX_SPR_SOLID[ta]){            // No pixels; skip

         MAP_PALETTE_MAPPED_NEW(
            tb>>4,
            16,
            map
         );

         if(GFX_SPR_SOLID[ta]==1){         // Some pixels; trans
            switch(RAM_SPR[zz+14]&0xC0){
            case 0x00: Draw16x16_Trans_Mapped_Rot(&GFX_SPR[ta<<8], x, y, map);        break;
            case 0x80: Draw16x16_Trans_Mapped_FlipY_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
            case 0x40: Draw16x16_Trans_Mapped_FlipX_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
            case 0xC0: Draw16x16_Trans_Mapped_FlipXY_Rot(&GFX_SPR[ta<<8], x, y, map); break;
            }
         }
         else{                  // all pixels; solid
            switch(RAM_SPR[zz+14]&0xC0){
            case 0x00: Draw16x16_Mapped_Rot(&GFX_SPR[ta<<8], x, y, map);        break;
            case 0x80: Draw16x16_Mapped_FlipY_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
            case 0x40: Draw16x16_Mapped_FlipX_Rot(&GFX_SPR[ta<<8], x, y, map);  break;
            case 0xC0: Draw16x16_Mapped_FlipXY_Rot(&GFX_SPR[ta<<8], x, y, map); break;
            }
         }

         }

      }
   }

}


/*

 OBJECT RAM
 ----------

- 16 bytes per sprite definition (only odd)
- Sprites are 16x16; 16 colours

-----+--------+-----------------------------
Byte |Bit(s)  | Use
-----+76543210+-----------------------------
1/3/5|........| Unused
  7  |.......x| XPos - Sign Bit
  7  |......x.| YPos - Sign Bit
  7  |.....x..| Use Relative offsets
  7  |xxxx....| Palette Bank
  9  |xxxxxxxx| XPos
  B  |xxxxxxxx| YPos
  D  |xxxxxxxx| Sprite Number (low 8 bits)
  F  |....xxxx| Sprite Number (high 4 bits)
  F  |.x......| Flip Sprite X-Axis
  F  |x.......| Flip Sprite Y-Axis
-----+--------+-----------------------------

*/
