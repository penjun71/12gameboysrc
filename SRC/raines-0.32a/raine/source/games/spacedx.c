/******************************************************************************/
/*                                                                            */
/*               SPACE INVADERS DX (C) 1994 TAITO CORPORATION                 */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "spacedx.h"
#include "tc180vcu.h"
#include "taitosnd.h"

static struct DIR_INFO space_invaders_dx_dirs[] =
{
   { "space_invaders_dx", },
   { "spacedx", },
   { NULL, },
};

static struct ROM_INFO space_invaders_dx_roms[] =
{
   {       "d89-01", 0x00080000, 0xfffa0660, 0, 0, 0, },
   {       "d89-02", 0x00080000, 0xc36544b9, 0, 0, 0, },
   {       "d89-03", 0x00080000, 0x218f31a4, 0, 0, 0, },
   {       "d89-05", 0x00040000, 0xbe1638af, 0, 0, 0, },
   {       "d89-06", 0x00040000, 0x7122751e, 0, 0, 0, },
   {       "d89-07", 0x00010000, 0xbd743401, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO space_invaders_dx_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x072003, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x072003, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x072005, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x072005, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_TEST,         MSG_TEST,                0x072003, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x072005, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x07200F, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x07200F, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x07200F, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x07200F, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x072007, 0x01, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x072005, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x07200F, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x07200F, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x07200F, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x07200F, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x072007, 0x10, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct ROMSW_DATA romsw_data_space_invaders_dx_0[] =
{
   { "Taito Japan",           0x01 },
   { "Taito America",         0x02 },
   { NULL,                    0    },
};

static struct ROMSW_INFO space_invaders_dx_romsw[] =
{
   { 0x03FFFF, 0x02, romsw_data_space_invaders_dx_0 },
   { 0,        0,    NULL },
};

static struct VIDEO_INFO space_invaders_dx_video =
{
   DrawSpaceDX,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL,
};

struct GAME_MAIN game_space_invaders_dx =
{
   space_invaders_dx_dirs,
   space_invaders_dx_roms,
   space_invaders_dx_inputs,
   NULL,
   space_invaders_dx_romsw,

   LoadSpaceDX,
   ClearSpaceDX,
   &space_invaders_dx_video,
   ExecuteSpaceDXFrame,
   "spacedx",
   "Space Invaders DX",
   "スペースインベーダーＤＸ",
   COMPANY_ID_TAITO,
   "D89",
   1994,
   taito_ym2610_sound,
   GAME_SHOOT,
};

static UINT8 *RAM_VIDEO;
static UINT8 *RAM_COLOUR;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_BG2;
static UINT8 *GFX_BG2_SOLID;

// EEP-ROM Access Hack
// -------------------
//
// EEPROM is 128 bytes (64 words)

static void SpaceDXEEPROMAccess(UINT32 address, UINT8 data)
{
   // READ: D0=EEPROM[D0]

   if(data==0){
      s68000context.dreg[0]=ReadWord(&EEPROM[s68000context.dreg[0]<<1]);
      return;
   }

   // WRITE: EEPROM[D0]=D1

   if(data==1){
      WriteWord(&EEPROM[s68000context.dreg[0]<<1],s68000context.dreg[1]);
      return;
   }
}

static int BadReadWord(UINT32 address)
{
   #ifdef RAINE_DEBUG
       print_debug("Rw(%06x) [%06x]\n",address,s68000context.pc);
   #endif
   return(0xFFFF);
}

static void BadWriteByte(UINT32 address, UINT8 data)
{
   #ifdef RAINE_DEBUG
      if(address!=0x500000) print_debug("Wb(%06x,%02x) [%06x]\n",address,data,s68000context.pc);
   #endif
}

static void BadWriteWord(UINT32 address, UINT16 data)
{
   #ifdef RAINE_DEBUG
      if(address!=0x500000) print_debug("Ww(%06x,%04x) [%06x]\n",address,data,s68000context.pc);
   #endif
}

void LoadSpaceDX(void)
{
   int ta,tb,tc;

   if(!(ROM=AllocateMem(0x40000))) return;
   if(!(RAM=AllocateMem(0x80000+0x10000))) return;
   if(!(GFX=AllocateMem(0x140000))) return;

   GFX_BG0 = GFX+0x000000;
   GFX_BG2 = GFX+0x100000;

   if(!load_rom("d89-02", RAM, 0x40000)) return;
   tb=0;
   for(ta=0;ta<0x40000;ta+=2){
      tc=RAM[ta+0];
      GFX[tb+0]=((tc&0x80)>>7)<<3;
      GFX[tb+1]=((tc&0x40)>>6)<<3;
      GFX[tb+2]=((tc&0x20)>>5)<<3;
      GFX[tb+3]=((tc&0x10)>>4)<<3;
      GFX[tb+4]=((tc&0x08)>>3)<<3;
      GFX[tb+5]=((tc&0x04)>>2)<<3;
      GFX[tb+6]=((tc&0x02)>>1)<<3;
      GFX[tb+7]=((tc&0x01)>>0)<<3;
      tc=RAM[ta+1];
      GFX[tb+0]|=((tc&0x80)>>7)<<2;
      GFX[tb+1]|=((tc&0x40)>>6)<<2;
      GFX[tb+2]|=((tc&0x20)>>5)<<2;
      GFX[tb+3]|=((tc&0x10)>>4)<<2;
      GFX[tb+4]|=((tc&0x08)>>3)<<2;
      GFX[tb+5]|=((tc&0x04)>>2)<<2;
      GFX[tb+6]|=((tc&0x02)>>1)<<2;
      GFX[tb+7]|=((tc&0x01)>>0)<<2;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   for(ta=0;ta<0x10000;ta+=2){
      tc=RAM[ta+0];
      GFX[tb+0]=((tc&0x80)>>7)<<3;
      GFX[tb+1]=((tc&0x40)>>6)<<3;
      GFX[tb+2]=((tc&0x20)>>5)<<3;
      GFX[tb+3]=((tc&0x10)>>4)<<3;
      GFX[tb+4]=((tc&0x08)>>3)<<3;
      GFX[tb+5]=((tc&0x04)>>2)<<3;
      GFX[tb+6]=((tc&0x02)>>1)<<3;
      GFX[tb+7]=((tc&0x01)>>0)<<3;
      tc=RAM[ta+1];
      GFX[tb+0]|=((tc&0x80)>>7)<<2;
      GFX[tb+1]|=((tc&0x40)>>6)<<2;
      GFX[tb+2]|=((tc&0x20)>>5)<<2;
      GFX[tb+3]|=((tc&0x10)>>4)<<2;
      GFX[tb+4]|=((tc&0x08)>>3)<<2;
      GFX[tb+5]|=((tc&0x04)>>2)<<2;
      GFX[tb+6]|=((tc&0x02)>>1)<<2;
      GFX[tb+7]|=((tc&0x01)>>0)<<2;
      tb+=8;
   }
   if(!load_rom("d89-01", RAM, 0x40000)) return;
   tb=0;
   for(ta=0;ta<0x40000;ta+=2){
      tc=RAM[ta+0];
      GFX[tb+0]|=((tc&0x80)>>7)<<1;
      GFX[tb+1]|=((tc&0x40)>>6)<<1;
      GFX[tb+2]|=((tc&0x20)>>5)<<1;
      GFX[tb+3]|=((tc&0x10)>>4)<<1;
      GFX[tb+4]|=((tc&0x08)>>3)<<1;
      GFX[tb+5]|=((tc&0x04)>>2)<<1;
      GFX[tb+6]|=((tc&0x02)>>1)<<1;
      GFX[tb+7]|=((tc&0x01)>>0)<<1;
      tc=RAM[ta+1];
      GFX[tb+0]|=((tc&0x80)>>7)<<0;
      GFX[tb+1]|=((tc&0x40)>>6)<<0;
      GFX[tb+2]|=((tc&0x20)>>5)<<0;
      GFX[tb+3]|=((tc&0x10)>>4)<<0;
      GFX[tb+4]|=((tc&0x08)>>3)<<0;
      GFX[tb+5]|=((tc&0x04)>>2)<<0;
      GFX[tb+6]|=((tc&0x02)>>1)<<0;
      GFX[tb+7]|=((tc&0x01)>>0)<<0;
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   for(ta=0;ta<0x10000;ta+=2){
      tc=RAM[ta+0];
      GFX[tb+0]|=((tc&0x80)>>7)<<1;
      GFX[tb+1]|=((tc&0x40)>>6)<<1;
      GFX[tb+2]|=((tc&0x20)>>5)<<1;
      GFX[tb+3]|=((tc&0x10)>>4)<<1;
      GFX[tb+4]|=((tc&0x08)>>3)<<1;
      GFX[tb+5]|=((tc&0x04)>>2)<<1;
      GFX[tb+6]|=((tc&0x02)>>1)<<1;
      GFX[tb+7]|=((tc&0x01)>>0)<<1;
      tc=RAM[ta+1];
      GFX[tb+0]|=((tc&0x80)>>7)<<0;
      GFX[tb+1]|=((tc&0x40)>>6)<<0;
      GFX[tb+2]|=((tc&0x20)>>5)<<0;
      GFX[tb+3]|=((tc&0x10)>>4)<<0;
      GFX[tb+4]|=((tc&0x08)>>3)<<0;
      GFX[tb+5]|=((tc&0x04)>>2)<<0;
      GFX[tb+6]|=((tc&0x02)>>1)<<0;
      GFX[tb+7]|=((tc&0x01)>>0)<<0;
      tb+=8;
   }

   if(!load_rom("d89-06", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("d89-05", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x80000;
   if(!load_rom("d89-07", Z80ROM, 0x10000)) return;		// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x80000))) return;
   if(!load_rom("d89-03",PCMROM,0x80000)) return;		// ADPCM A rom
   YM2610SetBuffers(PCMROM, PCMROM, 0x80000, 0x80000);

   AddTaitoYM2610(0x017F, 0x0152, 0x10000);

   /*-----------------------*/

   memset(RAM+0x00000,0x00,0x80000);
   memset(RAM+0x72000,0xFF,0x00100);

   RAMSize=0x80000+0x10000;

   EEPROM=RAM+0x7F000;

   add_eeprom(EEPROM, 0x80, EPR_INVALIDATE_ON_ROM_CHANGE);
   load_eeprom();

   RAM_VIDEO  = RAM+0x50000;
   RAM_COLOUR = RAM+0x70000;

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x1000);
   GFX_BG2_SOLID = make_solid_mask_8x8  (GFX_BG2, 0x1000);

   InitPaletteMap(RAM_COLOUR, 0x100, 0x10, 0x1000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);

   // EEPROM Access Hacks

   WriteLong68k(&ROM[0x81EE],0x13FC0000);	// move.b #$00,$BB0000
   WriteLong68k(&ROM[0x81F2],0x00BB0000);	// (EEP-ROM READ HACK)
   WriteWord68k(&ROM[0x81F6],0x4E75);

   WriteLong68k(&ROM[0x826A],0x13FC0001);	// move.b #$01,$BB0000
   WriteLong68k(&ROM[0x826E],0x00BB0000);	// (EEP-ROM WRITE HACK)
   WriteWord68k(&ROM[0x8272],0x4E75);

   // Fix ROM Checksum

   WriteWord68k(&ROM[0x1AF2],0x4E75);		// rts

   // Speed Hacking

   WriteLong68k(&ROM[0x08F2],0x4E714E71);	// nop; nop
   WriteLong68k(&ROM[0x08F6],0x4E714E71);	// nop; nop
   WriteLong68k(&ROM[0x08FA],0x4E714E71);	// nop; nop

   WriteLong68k(&ROM[0x05DA],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x05DE],0x00AA0000);	// (SPEED HACK)
   WriteLong68k(&ROM[0x05E2],0x4E714E71);	// nop; nop

   // Init tc0180vcu emulation
   // ------------------------

   tc0180vcu.RAM	= RAM_VIDEO;
   tc0180vcu.RAM_2	= RAM_VIDEO+0x18000;
   tc0180vcu.GFX_BG0	= GFX_BG0;
   tc0180vcu.GFX_BG0_MSK= GFX_BG0_SOLID;
   tc0180vcu.GFX_BG2	= GFX_BG2;
   tc0180vcu.GFX_BG2_MSK= GFX_BG2_SOLID;
   tc0180vcu.tile_mask	= 0x0FFF;
   tc0180vcu.bmp_x	= 32;
   tc0180vcu.bmp_y	= 32;
   tc0180vcu.bmp_w	= 320;
   tc0180vcu.bmp_h	= 224;
   tc0180vcu.scr_x	= 0;
   tc0180vcu.scr_y	= 16;

   vcu_make_col_bankmap(0x40,0x00,0x40,0x80,0xC0);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x40000);
   ByteSwap(RAM,0x80000);

   AddMemFetch(0x000000, 0x03FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x900000, 0x90FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadByte(0x440000, 0x47FFFF, NULL, RAM+0x010000);			// SCREEN RAM
   AddReadByte(0x400000, 0x41FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x800000, 0x801FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadByte(0x500000, 0x5000FF, NULL, RAM+0x072000);			// INPUT
   AddReadByte(0x700000, 0x700003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x03FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x900000, 0x90FFFF, NULL, RAM+0x000000);			// 68000 RAM
   AddReadWord(0x440000, 0x47FFFF, NULL, RAM+0x010000);			// SCREEN RAM
   AddReadWord(0x400000, 0x41FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x800000, 0x801FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadWord(0x500000, 0x5000FF, NULL, RAM+0x072000);			// INPUT
   AddReadWord(0x000000, 0xFFFFFF, BadReadWord, NULL);			// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x900000, 0x90FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteByte(0x440000, 0x47FFFF, NULL, RAM+0x010000);		// SCREEN RAM
   AddWriteByte(0x400000, 0x41FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x800000, 0x801FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteByte(0x700000, 0x700003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0xBB0000, 0xBB0001, SpaceDXEEPROMAccess, NULL);		// EEPROM
   AddWriteByte(0x000000, 0xFFFFFF, BadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x900000, 0x90FFFF, NULL, RAM+0x000000);		// 68000 RAM
   AddWriteWord(0x440000, 0x47FFFF, NULL, RAM+0x010000);		// SCREEN RAM
   AddWriteWord(0x400000, 0x41FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x800000, 0x801FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteWord(0x000000, 0xFFFFFF, BadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearSpaceDX(void)
{
   RemoveTaitoYM2610();

   save_eeprom();

   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x040000,1);
      //save_debug("RAM.bin",RAM,0x080000,1);
      //save_debug("GFX.bin",GFX,0x140000,0);
   #endif
}

void ExecuteSpaceDXFrame(void)
{
   #ifdef RAINE_DEBUG
   vcu_debug_info();
   #endif

   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(12,60));	// M68000 12MHz (60fps)
   cpu_interrupt(CPU_68K_0, 3);

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawSpaceDX(void)
{
   static int do_clear;

   int x,y,ta,zz;
   UINT8 *bit,*map;

   ClearPaletteMap();

   // Init tc0180vcu emulation
   // ------------------------

   tc0180vcu_layer_count = 0;

   // BG0
   // ---

   vcu_render_bg0();

   // BG0
   // ---

   vcu_render_bg1();

   // PIXEL
   // -----

   if((RAM_VIDEO[0x1800F]&0x80)!=0){

   if((RAM_VIDEO[0x1800F]&0x40)!=0)
      zz=0x00000;
   else
      zz=0x20000;

   zz+=(16*512);
   zz&=0x3FFFF;

   for(ta=15;ta>=0;ta--){

      MAP_PALETTE_MAPPED(
         Map_12bit_RGBx,
         ta|0x80,
         16,
         map
      );

   }

   for(y=0;y<224;y++){
   bit = GameViewBitmap->line[y];
   zz+=0x10000;
   for(x=0;x<320;x+=2){
      if(RAM[zz]!=0){bit[x+1] = map[RAM[zz]];}
      if(RAM[zz+1]!=0){bit[x] = map[RAM[zz+1]];}
      zz+=2;
   }
   zz-=0x10000;
   zz+=(512-320);
   zz&=0x3FFFF;
   }

   do_clear=1;
   }
   else{
      if(do_clear){
         do_clear=0;
         memset(RAM+0x10000,0x00,0x40000);
      }
   }

   // BG2
   // ---

   vcu_render_bg2();

}
