/******************************************************************************/
/*                                                                            */
/*                 SILENT DRAGON (C) 1992 TAITO CORPORATION                   */
/*                                                                            */
/******************************************************************************/

#include "gameinc.h"
#include "silentd.h"
#include "tc180vcu.h"
#include "tc220ioc.h"
#include "sasound.h"		// sample support routines
#include "taitosnd.h"

static struct DIR_INFO silent_dragon_dirs[] =
{
   { "silent_dragon", },
   { "silentd", },
   { NULL, },
};

static struct ROM_INFO silent_dragon_roms[] =
{
   {   "sd_m05.rom", 0x00100000, 0xe02472c5, 0, 0, 0, },
   {   "sd_m02.rom", 0x00080000, 0xe0de5c39, 0, 0, 0, },
   {   "sd_m03.rom", 0x00100000, 0x1b9b2846, 0, 0, 0, },
   {   "sd_m04.rom", 0x00100000, 0x53237217, 0, 0, 0, },
   {   "sd_m01.rom", 0x00080000, 0xb41fff1a, 0, 0, 0, },
   {   "sd_m06.rom", 0x00100000, 0xe6e6dfa7, 0, 0, 0, },
   {    "sr_09.rom", 0x00020000, 0x2f05b14a, 0, 0, 0, },
   {    "sr_11.rom", 0x00020000, 0x35da4428, 0, 0, 0, },
   {  "sr_12-1.rom", 0x00020000, 0x5883d362, 0, 0, 0, },
   {  "sr_15-1.rom", 0x00020000, 0x8c0a72ae, 0, 0, 0, },
   {    "sr_13.rom", 0x00010000, 0x651861ab, 0, 0, 0, },
   {           NULL,          0,          0, 0, 0, 0, },
};

static struct INPUT_INFO silent_dragon_inputs[] =
{
   { KB_DEF_COIN1,        MSG_COIN1,               0x025006, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_COIN2,        MSG_COIN2,               0x025006, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_COIN3,        MSG_COIN3,               0x025060, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_COIN4,        MSG_COIN4,               0x025060, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_TILT,         MSG_TILT,                0x025006, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_SERVICE,      MSG_SERVICE,             0x025006, 0x02, BIT_ACTIVE_0 },

   { KB_DEF_P1_START,     MSG_P1_START,            0x025006, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_UP,        MSG_P1_UP,               0x02500E, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_DOWN,      MSG_P1_DOWN,             0x02500E, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_LEFT,      MSG_P1_LEFT,             0x02500E, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P1_RIGHT,     MSG_P1_RIGHT,            0x02500E, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P1_B1,        MSG_P1_B1,               0x025004, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P1_B2,        MSG_P1_B2,               0x025004, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P1_B3,        MSG_P1_B3,               0x025004, 0x04, BIT_ACTIVE_0 },

   { KB_DEF_P2_START,     MSG_P2_START,            0x025006, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_UP,        MSG_P2_UP,               0x02500E, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_DOWN,      MSG_P2_DOWN,             0x02500E, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P2_LEFT,      MSG_P2_LEFT,             0x02500E, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P2_RIGHT,     MSG_P2_RIGHT,            0x02500E, 0x80, BIT_ACTIVE_0 },
   { KB_DEF_P2_B1,        MSG_P2_B1,               0x025004, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P2_B2,        MSG_P2_B2,               0x025004, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P2_B3,        MSG_P2_B3,               0x025004, 0x20, BIT_ACTIVE_0 },

   { KB_DEF_P3_START,     MSG_P3_START,            0x025020, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P3_UP,        MSG_P3_UP,               0x025020, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P3_DOWN,      MSG_P3_DOWN,             0x025020, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P3_LEFT,      MSG_P3_LEFT,             0x025020, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P3_RIGHT,     MSG_P3_RIGHT,            0x025020, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P3_B1,        MSG_P3_B1,               0x025020, 0x20, BIT_ACTIVE_0 },
   { KB_DEF_P3_B2,        MSG_P3_B2,               0x025020, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P3_B3,        MSG_P3_B3,               0x025020, 0x80, BIT_ACTIVE_0 },

   { KB_DEF_P4_START,     MSG_P4_START,            0x025040, 0x01, BIT_ACTIVE_0 },
   { KB_DEF_P4_UP,        MSG_P4_UP,               0x025040, 0x08, BIT_ACTIVE_0 },
   { KB_DEF_P4_DOWN,      MSG_P4_DOWN,             0x025040, 0x10, BIT_ACTIVE_0 },
   { KB_DEF_P4_LEFT,      MSG_P4_LEFT,             0x025040, 0x02, BIT_ACTIVE_0 },
   { KB_DEF_P4_RIGHT,     MSG_P4_RIGHT,            0x025040, 0x04, BIT_ACTIVE_0 },
   { KB_DEF_P4_B1,        MSG_P4_B1,               0x025040, 0x28, BIT_ACTIVE_0 },
   { KB_DEF_P4_B2,        MSG_P4_B2,               0x025040, 0x40, BIT_ACTIVE_0 },
   { KB_DEF_P4_B3,        MSG_P4_B3,               0x025040, 0x80, BIT_ACTIVE_0 },

   { 0,                   NULL,                    0,        0,    0            },
};

static struct DSW_DATA dsw_data_silent_dragon_0[] =
{
   { MSG_DSWA_BIT1,           0x01, 0x02 },
   { MSG_OFF,                 0x01, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT2,           0x02, 0x02 },
   { MSG_OFF,                 0x02, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_TEST_MODE,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DEMO_SOUND,          0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_COIN1,               0x30, 0x04 },
   { MSG_1COIN_1PLAY,         0x30, 0x00 },
   { MSG_2COIN_1PLAY,         0x20, 0x00 },
   { MSG_3COIN_1PLAY,         0x10, 0x00 },
   { MSG_4COIN_1PLAY,         0x00, 0x00 },
   { MSG_DSWA_BIT7,           0x40, 0x02 },
   { MSG_OFF,                 0x40, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWA_BIT8,           0x80, 0x02 },
   { MSG_OFF,                 0x80, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { NULL,                    0,    0,   },
};

static struct DSW_DATA dsw_data_silent_dragon_1[] =
{
   { MSG_DIFFICULTY,          0x03, 0x04 },
   { MSG_NORMAL,              0x03, 0x00 },
   { MSG_EASY,                0x02, 0x00 },
   { MSG_HARD,                0x01, 0x00 },
   { MSG_HARDEST,             0x00, 0x00 },
   { MSG_DSWB_BIT3,           0x04, 0x02 },
   { MSG_OFF,                 0x04, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { MSG_DSWB_BIT4,           0x08, 0x02 },
   { MSG_ON,                  0x08, 0x00 },
   { MSG_OFF,                 0x00, 0x00 },
   { MSG_DSWB_BIT5,           0x10, 0x02 },
   { MSG_OFF,                 0x10, 0x00 },
   { MSG_ON,                  0x00, 0x00 },
   { "Coin Slots",            0x10, 0x02 },
   { "Combined",              0x10, 0x00 },
   { "Seperate",              0x00, 0x00 },
   { "Cabinet Type",          0xC0, 0x04 },
   { "3 Player",              0xC0, 0x00 },
   { "2 Player",              0x80, 0x00 },
   { "4 Player / 1 Cabinet",  0x40, 0x00 },
   { "4 Player / 2 Cabinets", 0x00, 0x00 },
   { NULL,                    0,    0,   },
};


static struct DSW_INFO silent_dragon_dsw[] =
{
   { 0x025000, 0xFF, dsw_data_silent_dragon_0 },
   { 0x025002, 0xFF, dsw_data_silent_dragon_1 },
   { 0,        0,    NULL,      },
};

static struct VIDEO_INFO silent_dragon_video =
{
   DrawSilentD,
   320,
   224,
   32,
   VIDEO_ROTATE_NORMAL,
};

struct GAME_MAIN game_silent_dragon =
{
   silent_dragon_dirs,
   silent_dragon_roms,
   silent_dragon_inputs,
   silent_dragon_dsw,
   NULL,

   LoadSilentD,
   ClearSilentD,
   &silent_dragon_video,
   ExecuteSilentDFrame,
   "silentd",
   "Silent Dragon",
   "サイレントドラゴン",
   COMPANY_ID_TAITO,
   NULL,
   1992,
   taito_ym2610_sound,
   GAME_BEAT,
};

static UINT8 *RAM_INPUT;
static UINT8 *RAM_VIDEO;
static UINT8 *RAM_COLOUR;

static UINT8 *GFX_BG0;
static UINT8 *GFX_BG0_SOLID;

static UINT8 *GFX_BG2;
static UINT8 *GFX_BG2_SOLID;

void LoadSilentD(void)
{
   int ta,tb,tc,td;

   if(!(ROM=AllocateMem(0x80000))) return;
   if(!(RAM=AllocateMem(0x100000))) return;
   if(!(GFX=AllocateMem(0x840000))) return;

   GFX_BG0 = GFX+0x000000;
   GFX_BG2 = GFX+0x800000;

   if(!load_rom("sd_m03.rom", RAM, 0x100000)) return;	// GFX
   tb=0;
   for(ta=0;ta<0x100000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]=(((tc&0x80)>>7)<<0);
      GFX[tb+1]=(((tc&0x40)>>6)<<0);
      GFX[tb+2]=(((tc&0x20)>>5)<<0);
      GFX[tb+3]=(((tc&0x10)>>4)<<0);
      GFX[tb+4]=(((tc&0x08)>>3)<<0);
      GFX[tb+5]=(((tc&0x04)>>2)<<0);
      GFX[tb+6]=(((tc&0x02)>>1)<<0);
      GFX[tb+7]=(((tc&0x01)>>0)<<0);
      GFX[tb+0]|=(((td&0x80)>>7)<<1);
      GFX[tb+1]|=(((td&0x40)>>6)<<1);
      GFX[tb+2]|=(((td&0x20)>>5)<<1);
      GFX[tb+3]|=(((td&0x10)>>4)<<1);
      GFX[tb+4]|=(((td&0x08)>>3)<<1);
      GFX[tb+5]|=(((td&0x04)>>2)<<1);
      GFX[tb+6]|=(((td&0x02)>>1)<<1);
      GFX[tb+7]|=(((td&0x01)>>0)<<1);
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   tb=0x800000;
   for(ta=0;ta<0x8000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]=(((tc&0x80)>>7)<<0);
      GFX[tb+1]=(((tc&0x40)>>6)<<0);
      GFX[tb+2]=(((tc&0x20)>>5)<<0);
      GFX[tb+3]=(((tc&0x10)>>4)<<0);
      GFX[tb+4]=(((tc&0x08)>>3)<<0);
      GFX[tb+5]=(((tc&0x04)>>2)<<0);
      GFX[tb+6]=(((tc&0x02)>>1)<<0);
      GFX[tb+7]=(((tc&0x01)>>0)<<0);
      GFX[tb+0]|=(((td&0x80)>>7)<<1);
      GFX[tb+1]|=(((td&0x40)>>6)<<1);
      GFX[tb+2]|=(((td&0x20)>>5)<<1);
      GFX[tb+3]|=(((td&0x10)>>4)<<1);
      GFX[tb+4]|=(((td&0x08)>>3)<<1);
      GFX[tb+5]|=(((td&0x04)>>2)<<1);
      GFX[tb+6]|=(((td&0x02)>>1)<<1);
      GFX[tb+7]|=(((td&0x01)>>0)<<1);
      tb+=8;
   }

   if(!load_rom("sd_m04.rom", RAM, 0x100000)) return;
   tb=0;
   for(ta=0;ta<0x100000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]|=(((tc&0x80)>>7)<<2);
      GFX[tb+1]|=(((tc&0x40)>>6)<<2);
      GFX[tb+2]|=(((tc&0x20)>>5)<<2);
      GFX[tb+3]|=(((tc&0x10)>>4)<<2);
      GFX[tb+4]|=(((tc&0x08)>>3)<<2);
      GFX[tb+5]|=(((tc&0x04)>>2)<<2);
      GFX[tb+6]|=(((tc&0x02)>>1)<<2);
      GFX[tb+7]|=(((tc&0x01)>>0)<<2);
      GFX[tb+0]|=(((td&0x80)>>7)<<3);
      GFX[tb+1]|=(((td&0x40)>>6)<<3);
      GFX[tb+2]|=(((td&0x20)>>5)<<3);
      GFX[tb+3]|=(((td&0x10)>>4)<<3);
      GFX[tb+4]|=(((td&0x08)>>3)<<3);
      GFX[tb+5]|=(((td&0x04)>>2)<<3);
      GFX[tb+6]|=(((td&0x02)>>1)<<3);
      GFX[tb+7]|=(((td&0x01)>>0)<<3);
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   tb=0x800000;
   for(ta=0;ta<0x8000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]|=(((tc&0x80)>>7)<<2);
      GFX[tb+1]|=(((tc&0x40)>>6)<<2);
      GFX[tb+2]|=(((tc&0x20)>>5)<<2);
      GFX[tb+3]|=(((tc&0x10)>>4)<<2);
      GFX[tb+4]|=(((tc&0x08)>>3)<<2);
      GFX[tb+5]|=(((tc&0x04)>>2)<<2);
      GFX[tb+6]|=(((tc&0x02)>>1)<<2);
      GFX[tb+7]|=(((tc&0x01)>>0)<<2);
      GFX[tb+0]|=(((td&0x80)>>7)<<3);
      GFX[tb+1]|=(((td&0x40)>>6)<<3);
      GFX[tb+2]|=(((td&0x20)>>5)<<3);
      GFX[tb+3]|=(((td&0x10)>>4)<<3);
      GFX[tb+4]|=(((td&0x08)>>3)<<3);
      GFX[tb+5]|=(((td&0x04)>>2)<<3);
      GFX[tb+6]|=(((td&0x02)>>1)<<3);
      GFX[tb+7]|=(((td&0x01)>>0)<<3);
      tb+=8;
   }

   if(!load_rom("sd_m05.rom", RAM, 0x100000)) return;	// GFX
   tb=0x400000;
   for(ta=0;ta<0x100000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]=(((tc&0x80)>>7)<<0);
      GFX[tb+1]=(((tc&0x40)>>6)<<0);
      GFX[tb+2]=(((tc&0x20)>>5)<<0);
      GFX[tb+3]=(((tc&0x10)>>4)<<0);
      GFX[tb+4]=(((tc&0x08)>>3)<<0);
      GFX[tb+5]=(((tc&0x04)>>2)<<0);
      GFX[tb+6]=(((tc&0x02)>>1)<<0);
      GFX[tb+7]=(((tc&0x01)>>0)<<0);
      GFX[tb+0]|=(((td&0x80)>>7)<<1);
      GFX[tb+1]|=(((td&0x40)>>6)<<1);
      GFX[tb+2]|=(((td&0x20)>>5)<<1);
      GFX[tb+3]|=(((td&0x10)>>4)<<1);
      GFX[tb+4]|=(((td&0x08)>>3)<<1);
      GFX[tb+5]|=(((td&0x04)>>2)<<1);
      GFX[tb+6]|=(((td&0x02)>>1)<<1);
      GFX[tb+7]|=(((td&0x01)>>0)<<1);
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   tb=0x820000;
   for(ta=0xF8000;ta<0x100000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]=(((tc&0x80)>>7)<<0);
      GFX[tb+1]=(((tc&0x40)>>6)<<0);
      GFX[tb+2]=(((tc&0x20)>>5)<<0);
      GFX[tb+3]=(((tc&0x10)>>4)<<0);
      GFX[tb+4]=(((tc&0x08)>>3)<<0);
      GFX[tb+5]=(((tc&0x04)>>2)<<0);
      GFX[tb+6]=(((tc&0x02)>>1)<<0);
      GFX[tb+7]=(((tc&0x01)>>0)<<0);
      GFX[tb+0]|=(((td&0x80)>>7)<<1);
      GFX[tb+1]|=(((td&0x40)>>6)<<1);
      GFX[tb+2]|=(((td&0x20)>>5)<<1);
      GFX[tb+3]|=(((td&0x10)>>4)<<1);
      GFX[tb+4]|=(((td&0x08)>>3)<<1);
      GFX[tb+5]|=(((td&0x04)>>2)<<1);
      GFX[tb+6]|=(((td&0x02)>>1)<<1);
      GFX[tb+7]|=(((td&0x01)>>0)<<1);
      tb+=8;
   }

   if(!load_rom("sd_m06.rom", RAM, 0x100000)) return;
   tb=0x400000;
   for(ta=0;ta<0x100000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]|=(((tc&0x80)>>7)<<2);
      GFX[tb+1]|=(((tc&0x40)>>6)<<2);
      GFX[tb+2]|=(((tc&0x20)>>5)<<2);
      GFX[tb+3]|=(((tc&0x10)>>4)<<2);
      GFX[tb+4]|=(((tc&0x08)>>3)<<2);
      GFX[tb+5]|=(((tc&0x04)>>2)<<2);
      GFX[tb+6]|=(((tc&0x02)>>1)<<2);
      GFX[tb+7]|=(((tc&0x01)>>0)<<2);
      GFX[tb+0]|=(((td&0x80)>>7)<<3);
      GFX[tb+1]|=(((td&0x40)>>6)<<3);
      GFX[tb+2]|=(((td&0x20)>>5)<<3);
      GFX[tb+3]|=(((td&0x10)>>4)<<3);
      GFX[tb+4]|=(((td&0x08)>>3)<<3);
      GFX[tb+5]|=(((td&0x04)>>2)<<3);
      GFX[tb+6]|=(((td&0x02)>>1)<<3);
      GFX[tb+7]|=(((td&0x01)>>0)<<3);
      tb+=16;
      if((tb&0x7F)==0){tb-=0x78;}
      else{if((tb&0x7F)==8){tb-=8;}}
   }
   tb=0x820000;
   for(ta=0xF8000;ta<0x100000;ta+=2){
      tc=RAM[ta+1];
      td=RAM[ta+0];
      GFX[tb+0]|=(((tc&0x80)>>7)<<2);
      GFX[tb+1]|=(((tc&0x40)>>6)<<2);
      GFX[tb+2]|=(((tc&0x20)>>5)<<2);
      GFX[tb+3]|=(((tc&0x10)>>4)<<2);
      GFX[tb+4]|=(((tc&0x08)>>3)<<2);
      GFX[tb+5]|=(((tc&0x04)>>2)<<2);
      GFX[tb+6]|=(((tc&0x02)>>1)<<2);
      GFX[tb+7]|=(((tc&0x01)>>0)<<2);
      GFX[tb+0]|=(((td&0x80)>>7)<<3);
      GFX[tb+1]|=(((td&0x40)>>6)<<3);
      GFX[tb+2]|=(((td&0x20)>>5)<<3);
      GFX[tb+3]|=(((td&0x10)>>4)<<3);
      GFX[tb+4]|=(((td&0x08)>>3)<<3);
      GFX[tb+5]|=(((td&0x04)>>2)<<3);
      GFX[tb+6]|=(((td&0x02)>>1)<<3);
      GFX[tb+7]|=(((td&0x01)>>0)<<3);
      tb+=8;
   }

   RAMSize=0x28000+0x10000;

   if(!(RAM=AllocateMem(RAMSize))) return;

   /*-----[Sound Setup]-----*/

   Z80ROM=RAM+0x28000;
   if(!load_rom("sr_13.rom", Z80ROM, 0x10000)) return;		// Z80 SOUND ROM

   if(!(PCMROM=AllocateMem(0x100000))) return;
   if(!load_rom("sd_m02.rom",PCMROM+0x00000,0x80000)) return;	// ADPCM A rom
   if(!load_rom("sd_m01.rom",PCMROM+0x80000,0x80000)) return;	// ADPCM A rom
   YM2610SetBuffers(PCMROM+0x00000, PCMROM+0x80000, 0x80000, 0x80000);

   AddTaitoYM2610(0x0025, 0x001F, 0x10000);

   /*-----------------------*/

   if(!load_rom("sr_12-1.rom", RAM, 0x20000)) return;		// 68000 ROM
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta]=RAM[ta];
   }
   if(!load_rom("sr_15-1.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+1]=RAM[ta];
   }
   if(!load_rom("sr_11.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40000]=RAM[ta];
   }
   if(!load_rom("sr_09.rom", RAM, 0x20000)) return;
   for(ta=0;ta<0x20000;ta++){
      ROM[ta+ta+0x40001]=RAM[ta];
   }

   memset(RAM+0x00000,0x00,0x28000);
   memset(RAM+0x25020,0xFF,0x00080);

   RAM_INPUT  = RAM+0x25000;
   RAM_VIDEO  = RAM+0x04000;
   RAM_COLOUR = RAM+0x24000;

   GFX_BG0_SOLID = make_solid_mask_16x16(GFX_BG0, 0x8000);
   GFX_BG2_SOLID = make_solid_mask_8x8  (GFX_BG2, 0x1000);

   // 68000 Speed Hack/Int Fix

   WriteWord68k(&ROM[0x00810],0x4EF9);
   WriteLong68k(&ROM[0x00812],0x00000038);

   WriteWord68k(&ROM[0x00038],0x082D);
   WriteLong68k(&ROM[0x0003A],0x0000005C);

   WriteWord68k(&ROM[0x0003E],0x6700+10);

   WriteLong68k(&ROM[0x00040],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x00044],0x00AA0000);	// Speed Hack

   WriteWord68k(&ROM[0x00048],0x6100-18);

   WriteWord68k(&ROM[0x0004A],0x4EF9);
   WriteLong68k(&ROM[0x0004C],0x00000818);

   // 68000 Speed Hack/Int Fix

   WriteWord68k(&ROM[0x00828],0x4EF9);
   WriteLong68k(&ROM[0x0082A],0x0007FF80);

   WriteWord68k(&ROM[0x7FF80],0x082D);
   WriteLong68k(&ROM[0x7FF82],0x0001005C);

   WriteWord68k(&ROM[0x7FF86],0x6600+10);

   WriteLong68k(&ROM[0x7FF88],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x7FF8C],0x00AA0000);	// Speed Hack

   WriteWord68k(&ROM[0x7FF90],0x6100-18);

   WriteWord68k(&ROM[0x7FF92],0x4E75);

   WriteLong68k(&ROM[0x7FF80],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x7FF84],0x00AA0000);	// Speed Hack

   WriteLong68k(&ROM[0x7FF88],0x13FC0000);	// move.b #$00,$AA0000
   WriteLong68k(&ROM[0x7FF8C],0x00AA0000);	// Speed Hack

   WriteWord68k(&ROM[0x7FF90],0x4E75);

   InitPaletteMap(RAM_COLOUR, 0x40, 0x10, 0x1000);

   set_colour_mapper(&col_map_rrrr_gggg_bbbb_xxxx);

   // Init tc0220ioc emulation
   // ------------------------

   tc0220ioc.RAM  = RAM_INPUT;
   tc0220ioc.ctrl = 0;		//TC0220_STOPCPU;
   reset_tc0220ioc();

   // Init tc0180vcu emulation
   // ------------------------

   tc0180vcu.RAM	= RAM_VIDEO;
   tc0180vcu.RAM_2	= RAM_VIDEO+0x18000;
   tc0180vcu.GFX_BG0	= GFX_BG0;
   tc0180vcu.GFX_BG0_MSK= GFX_BG0_SOLID;
   tc0180vcu.GFX_BG2	= GFX_BG2;
   tc0180vcu.GFX_BG2_MSK= GFX_BG2_SOLID;
   tc0180vcu.tile_mask	= 0x7FFF;
   tc0180vcu.bmp_x	= 32;
   tc0180vcu.bmp_y	= 32;
   tc0180vcu.bmp_w	= 320;
   tc0180vcu.bmp_h	= 224;
   tc0180vcu.scr_x	= 0;
   tc0180vcu.scr_y	= 16;

   vcu_make_col_bankmap(0x10,0x30,0x20,0x10,0x00);

/*
 *  StarScream Stuff follows
 */

   ByteSwap(ROM,0x80000);
   ByteSwap(RAM,0x28000);

   AddMemFetch(0x000000, 0x07FFFF, ROM+0x000000-0x000000);	// 68000 ROM
   AddMemFetch(-1, -1, NULL);

   AddReadByte(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadByte(0x400000, 0x403FFF, NULL, RAM+0x000000);			// MAIN RAM
   AddReadByte(0x500000, 0x51FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadByte(0x200000, 0x20000F, NULL, RAM_INPUT);			// INPUT
   AddReadByte(0x210000, 0x21000F, NULL, RAM+0x025020);			// P3 INPUT
   AddReadByte(0x220000, 0x22000F, NULL, RAM+0x025040);			// P3 INPUT
   AddReadByte(0x230000, 0x23000F, NULL, RAM+0x025060);			// P4 INPUT
   AddReadByte(0x240000, 0x24000F, NULL, RAM+0x025080);			// P4 INPUT
   AddReadByte(0x300000, 0x300FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadByte(0x100000, 0x100003, tc0140syt_read_main_68k, NULL);	// SOUND COMM
   AddReadByte(0x000000, 0xFFFFFF, DefBadReadByte, NULL);		// <Bad Reads>
   AddReadByte(-1, -1, NULL, NULL);

   AddReadWord(0x000000, 0x07FFFF, NULL, ROM+0x000000);			// 68000 ROM
   AddReadWord(0x400000, 0x403FFF, NULL, RAM+0x000000);			// MAIN RAM
   AddReadWord(0x500000, 0x51FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddReadWord(0x300000, 0x300FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddReadWord(0x200000, 0x20000F, NULL, RAM_INPUT);			// INPUT
   AddReadWord(0x210000, 0x21000F, NULL, RAM+0x025020);			// P3 INPUT
   AddReadWord(0x220000, 0x22000F, NULL, RAM+0x025040);			// P3 INPUT
   AddReadWord(0x230000, 0x23000F, NULL, RAM+0x025060);			// P4 INPUT
   AddReadWord(0x240000, 0x24000F, NULL, RAM+0x025080);			// P4 INPUT
   AddReadWord(0x000000, 0xFFFFFF, DefBadReadWord, NULL);		// <Bad Reads>
   AddReadWord(-1, -1,NULL, NULL);

   AddWriteByte(0x400000, 0x403FFF, NULL, RAM+0x000000);		// MAIN RAM
   AddWriteByte(0x500000, 0x51FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteByte(0x300000, 0x300FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteByte(0x100000, 0x100003, tc0140syt_write_main_68k, NULL);	// SOUND COMM
   AddWriteByte(0x200000, 0x20000F, tc0220ioc_wb, NULL);		// INPUT
   AddWriteByte(0xAA0000, 0xAA0001, Stop68000, NULL);			// Trap Idle 68000
   AddWriteByte(0x000000, 0xFFFFFF, DefBadWriteByte, NULL);		// <Bad Writes>
   AddWriteByte(-1, -1, NULL, NULL);

   AddWriteWord(0x400000, 0x403FFF, NULL, RAM+0x000000);		// MAIN RAM
   AddWriteWord(0x500000, 0x51FFFF, NULL, RAM_VIDEO);			// SCREEN RAM
   AddWriteWord(0x300000, 0x300FFF, NULL, RAM_COLOUR);			// COLOR RAM
   AddWriteWord(0x200000, 0x20000F, tc0220ioc_ww, NULL);		// INPUT
   AddWriteWord(0x000000, 0xFFFFFF, DefBadWriteWord, NULL);		// <Bad Writes>
   AddWriteWord(-1, -1, NULL, NULL);

   AddInitMemory();	// Set Starscream mem pointers... 
}

void ClearSilentD(void)
{
   RemoveTaitoYM2610();

   #ifdef RAINE_DEBUG
      //save_debug("ROM.bin",ROM,0x080000,1);
      save_debug("RAM.bin",RAM,0x028000,1);
      //save_debug("GFX0.bin",GFX_BG0,0x800000,0);
      //save_debug("GFX2.bin",GFX_BG2,0x040000,0);
   #endif
}

void ExecuteSilentDFrame(void)
{
   #ifdef RAINE_DEBUG
   vcu_debug_info();
   #endif

   //if((s68000context.pc<0x80)||(s68000context.pc>0x7FF80))
   cpu_interrupt(CPU_68K_0, 6);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(16,60));	// M68000 16MHz (60fps)
   /*#ifdef RAINE_DEBUG
      print_debug("PC_0:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif*/
   //if((s68000context.pc<0x80)||(s68000context.pc>0x7FF80))
   cpu_interrupt(CPU_68K_0, 4);
   cpu_execute_cycles(CPU_68K_0, CPU_FRAME_MHz(2,60));	// Sync Scroll
   /*#ifdef RAINE_DEBUG
      print_debug("PC_1:%06x SR:%04x\n",s68000context.pc,s68000context.sr);
   #endif*/

   Taito2610_Frame();			// Z80 and YM2610
}

void DrawSilentD(void)
{
   ClearPaletteMap();

   // Init tc0180vcu emulation
   // ------------------------

   tc0180vcu_layer_count = 0;

   // BG0
   // ---

   vcu_render_bg0();

   // BG1
   // ---

   vcu_render_bg1();

   // OBJ
   // ---

   vcu_render_obj(0x1A0);

   // BG2
   // ---

   vcu_render_bg2();
}


