
void LoadMetaFox(void);
void ClearMetaFox(void);

void LoadTwinEagle(void);
void ClearTwinEagle(void);

void LoadZingZingZip(void);
void ClearZingZingZip(void);

void LoadUSClassicGolf(void);
void ClearUSClassicGolf(void);

void DrawMetaFox(void);
void ExecuteMetaFoxFrame(void);
