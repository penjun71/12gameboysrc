/******************************************************************************/
/*                                                                            */
/*                         RAINE 16x16 TILE DRAWING                           */
/*                                                                            */
/******************************************************************************/

/*

Mapped
Transparent Mapped
Direct-Mapped
Transparent Direct-Mapped

*/

void init_spr16x16asm(void);

// 16x16 mapped sprites

void Draw16x16_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 16x16 mapped transparent sprites

void Draw16x16_Trans_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

void init_spr16x16asm_16(void);

// 16x16 mapped sprites

void Draw16x16_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 16x16 mapped transparent sprites

void Draw16x16_Trans_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

void init_spr16x16asm_32(void);

// 16x16 mapped sprites

void Draw16x16_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 16x16 mapped transparent sprites

void Draw16x16_Trans_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw16x16_Trans_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 16x16 sprites

void Draw16x16(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw16x16_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw16x16_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw16x16_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap);

// 16x16 transparent sprites

void Draw16x16_Trans(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw16x16_Trans_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw16x16_Trans_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw16x16_Trans_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap);
