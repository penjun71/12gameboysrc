
void ldraw16x16_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);

// 16x16 mapped transparent sprites

void ldraw16x16_Trans_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy);
void ldraw16x16_Trans_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Trans_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Trans_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);

// 16x16 sprites

void ldraw16x16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);

// 16x16 transparent sprites

void ldraw16x16_Trans(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);

// 16bpp
void ldraw16x16_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);

// 16x16 mapped transparent sprites

void ldraw16x16_Trans_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy);
void ldraw16x16_Trans_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Trans_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Trans_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);

// 16x16 sprites

void ldraw16x16_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipX_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipXY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);

// 16x16 transparent sprites

void ldraw16x16_Trans_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipX_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipXY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);

// 32bpp
void ldraw16x16_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);

// 16x16 mapped transparent sprites

void ldraw16x16_Trans_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy);
void ldraw16x16_Trans_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Trans_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void ldraw16x16_Trans_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);

// 16x16 sprites

void ldraw16x16_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipX_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipY_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_FlipXY_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);

// 16x16 transparent sprites

void ldraw16x16_Trans_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipX_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipY_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);
void ldraw16x16_Trans_FlipXY_32(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy);

// Column scroll

void cdraw16x16_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy);
void cdraw16x16_Trans_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy);
