#include "asmdefs.inc"

CODE_SEG
/******************************************************************************/
/*                                                                            */
/*                          RAINE 16x8 TILE DRAWING                           */
/*                                                                            */
/******************************************************************************/

/*

Mapped
Transparent Mapped

*/

FUNC(Draw16x8_Mapped)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_00:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movl	%eax,(%edi)
	movl	%ebx,4(%edi)

	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movl	%eax,8(%edi)
	movl	%ebx,12(%edi)

	addl	$16,%esi			// Next Tile Line
	addl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_00:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Mapped_FlipY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_01:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	movl	%eax,12(%edi)
	movl	%ebx,8(%edi)

	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	movl	%eax,4(%edi)
	movl	%ebx,(%edi)

	addl	$16,%esi			// Next Tile Line
	addl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_01:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Mapped_FlipX)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_02:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movl	%eax,(%edi)
	movl	%ebx,4(%edi)

	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movl	%eax,8(%edi)
	movl	%ebx,12(%edi)

	addl	$16,%esi			// Next Tile Line
	subl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_02:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Mapped_FlipXY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_03:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	movl	%eax,12(%edi)
	movl	%ebx,8(%edi)

	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	bswap	%eax
	bswap	%ebx

	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%bl,%dl
	movb	(%ecx,%edx),%bl
	movb	%ah,%dl
	movb	(%ecx,%edx),%ah
	movb	%bh,%dl
	movb	(%ecx,%edx),%bh

	movl	%eax,4(%edi)
	movl	%ebx,(%edi)

	addl	$16,%esi			// Next Tile Line
	subl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_03:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Trans_Mapped)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_04:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,4(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,1(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,5(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,6(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,3(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,7(%edi)
7:
	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,8(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,12(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,9(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,13(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,10(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,14(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,11(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15(%edi)
7:
	addl	$16,%esi			// Next Tile Line
	addl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_04:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Trans_Mapped_FlipY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_05:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-4(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-1(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-5(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-6(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-3(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-7(%edi)
7:
	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-8(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-12(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-9(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-13(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-10(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-14(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-11(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,(%edi)
7:
	addl	$16,%esi			// Next Tile Line
	addl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_05:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Trans_Mapped_FlipX)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_06:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,4(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,1(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,5(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,6(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,3(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,7(%edi)
7:
	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,8(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,12(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,9(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,13(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,10(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,14(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,11(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15(%edi)
7:
	addl	$16,%esi			// Next Tile Line
	subl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_06:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw16x8_Trans_Mapped_FlipXY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	28(%esp),%eax			// y
	xorl	%edx,%edx
	sall	$2,%eax
	movl	20(%esp),%esi			// source
	movl	0xDEADBEEF(%eax),%edi
blin_07:
	movl	32(%esp),%ecx			// cmap
	movl	$8,%ebp				// Tile Height
	addl	24(%esp),%edi			// x
9:
	movl	(%esi),%eax
	movl	4(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-4(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-1(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-5(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-6(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-3(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-7(%edi)
7:
	movl	8(%esi),%eax
	movl	12(%esi),%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-8(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-12(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-9(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-13(%edi)
7:
	shr	$16,%eax
	shr	$16,%ebx

	testb	%al,%al
	jz	7f
	movb	%al,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-10(%edi)
7:	testb	%bl,%bl
	jz	7f
	movb	%bl,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-14(%edi)
7:	testb	%ah,%ah
	jz	7f
	movb	%ah,%dl
	movb	(%ecx,%edx),%al
	movb	%al,15-11(%edi)
7:	testb	%bh,%bh
	jz	7f
	movb	%bh,%dl
	movb	(%ecx,%edx),%al
	movb	%al,(%edi)
7:
	addl	$16,%esi			// Next Tile Line
	subl	$0xDEADBEEF,%edi		// Next Screen Line
bitw_07:
	decl	%ebp
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(init_spr16x8asm)


	movl	GLOBL(GameBitmap),%eax
	movl	(%eax),%eax		// Width
	movl	%eax,bitw_00-4
	movl	%eax,bitw_01-4
	movl	%eax,bitw_02-4
	movl	%eax,bitw_03-4
	movl	%eax,bitw_04-4
	movl	%eax,bitw_05-4
	movl	%eax,bitw_06-4
	movl	%eax,bitw_07-4

	movl	GLOBL(GameBitmap),%eax
	addl	$64,%eax		// Line 0
	movl	%eax,blin_00-4
	movl	%eax,blin_01-4
	movl	%eax,blin_04-4
	movl	%eax,blin_05-4
	addl	$28,%eax		// Line 7
	movl	%eax,blin_02-4
	movl	%eax,blin_03-4
	movl	%eax,blin_06-4
	movl	%eax,blin_07-4

	ret


