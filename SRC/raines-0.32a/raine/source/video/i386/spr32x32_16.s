#include "asmdefs.inc"

CODE_SEG
/******************************************************************************/
/*                                                                            */
/*                         RAINE 32x32 TILE DRAWING                           */
/*                                                                            */
/******************************************************************************/

/*

Mapped
Transparent Mapped
Direct-Mapped
Transparent Direct-Mapped

*/

FUNC(Draw32x32_Mapped_16)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_00:
	movl	%edx,endc_00-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
	// I suppose the reads from di are some weird cache initialisation...
	// Message for the guy who wrote that :	 do u know about comments ???
	
	movb	(%edi),%dl
	movb	(%esi),%al
	movb	63(%edi),%cl
	movb	4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,(%edi)
	movw	%dx,8(%edi)

	movb	1(%esi),%al
	movb	5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,2(%edi)
	movw	%dx,10(%edi)

	movb	2(%esi),%al
	movb	6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,4(%edi)
	movw	%dx,12(%edi)

	movb	3(%esi),%al
	movb	7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,6(%edi)
	movw	%dx,14(%edi)

	movb	8(%esi),%al
	movb	8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,16(%edi)
	movw	%dx,24(%edi)

	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,18(%edi)
	movw	%dx,26(%edi)

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,20(%edi)
	movw	%dx,28(%edi)

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,22(%edi)
	movw	%dx,30(%edi)

// Let's play to copy & paste

	movb	16(%esi),%al
	movb	16+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32(%edi)
	movw	%dx,32+8(%edi)

	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+2(%edi)
	movw	%dx,32+10(%edi)

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+4(%edi)
	movw	%dx,32+12(%edi)

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+6(%edi)
	movw	%dx,32+14(%edi)

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+16(%edi)
	movw	%dx,32+24(%edi)

	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+18(%edi)
	movw	%dx,32+26(%edi)

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+20(%edi)
	movw	%dx,32+28(%edi)

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+22(%edi)
	movw	%dx,32+30(%edi)
	
	addl	$32,%esi		// Next Tile Line
	addl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_00:
	cmp	$0xDEADBEEF,%esi
endc_00:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Mapped_16_FlipY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_01:
	movl	%edx,endc_01-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
	movb	(%esi),%al
	movb	4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62(%edi)
	movw	%dx,62-8(%edi)

	movb	1(%esi),%al
	movb	5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-2(%edi)
	movw	%dx,62-10(%edi)

	movb	2(%esi),%al
	movb	6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-4(%edi)
	movw	%dx,62-12(%edi)

	movb	3(%esi),%al
	movb	7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-6(%edi)
	movw	%dx,62-14(%edi)

	movb	8(%esi),%al
	movb	8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-16(%edi)
	movw	%dx,62-24(%edi)

	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-18(%edi)
	movw	%dx,62-26(%edi)

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-20(%edi)
	movw	%dx,62-28(%edi)

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-22(%edi)
	movw	%dx,62-30(%edi)

	// The copy /paste thing...

	movb	16(%esi),%al
	movb	16+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30(%edi)
	movw	%dx,30-8(%edi)

	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-2(%edi)
	movw	%dx,30-10(%edi)

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-4(%edi)
	movw	%dx,30-12(%edi)

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-6(%edi)
	movw	%dx,30-14(%edi)

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-16(%edi)
	movw	%dx,30-24(%edi)

	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-18(%edi)
	movw	%dx,30-26(%edi)

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-20(%edi)
	movw	%dx,30-28(%edi)

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-22(%edi)
	movw	%dx,30-30(%edi)
	
	addl	$32,%esi		// Next Tile Line
	addl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_01:
	cmp	$0xDEADBEEF,%esi
endc_01:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Mapped_16_FlipX)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_02:
	movl	%edx,endc_02-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
	movb	(%esi),%al
	movb	4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,(%edi)
	movw	%dx,8(%edi)

	movb	1(%esi),%al
	movb	5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,2(%edi)
	movw	%dx,10(%edi)

	movb	2(%esi),%al
	movb	6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,4(%edi)
	movw	%dx,12(%edi)

	movb	3(%esi),%al
	movb	7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,6(%edi)
	movw	%dx,14(%edi)

	movb	8(%esi),%al
	movb	8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,16(%edi)
	movw	%dx,24(%edi)

	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,18(%edi)
	movw	%dx,26(%edi)

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,20(%edi)
	movw	%dx,28(%edi)

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,22(%edi)
	movw	%dx,30(%edi)

// Let's play to copy & paste (from the non flipping version)

	movb	16(%esi),%al
	movb	16+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32(%edi)
	movw	%dx,32+8(%edi)

	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+2(%edi)
	movw	%dx,32+10(%edi)

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+4(%edi)
	movw	%dx,32+12(%edi)

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+6(%edi)
	movw	%dx,32+14(%edi)

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+16(%edi)
	movw	%dx,32+24(%edi)

	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+18(%edi)
	movw	%dx,32+26(%edi)

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+20(%edi)
	movw	%dx,32+28(%edi)

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,32+22(%edi)
	movw	%dx,32+30(%edi)	

	addl	$32,%esi		// Next Tile Line
	subl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_02:
	cmp	$0xDEADBEEF,%esi
endc_02:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Mapped_16_FlipXY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_03:
	movl	%edx,endc_03-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:

// Copy this whole thing from the FlipX function... Cool...

	movb	(%esi),%al
	movb	4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62(%edi)
	movw	%dx,62-8(%edi)

	movb	1(%esi),%al
	movb	5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-2(%edi)
	movw	%dx,62-10(%edi)

	movb	2(%esi),%al
	movb	6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-4(%edi)
	movw	%dx,62-12(%edi)

	movb	3(%esi),%al
	movb	7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-6(%edi)
	movw	%dx,62-14(%edi)

	movb	8(%esi),%al
	movb	8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-16(%edi)
	movw	%dx,62-24(%edi)

	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-18(%edi)
	movw	%dx,62-26(%edi)

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-20(%edi)
	movw	%dx,62-28(%edi)

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,62-22(%edi)
	movw	%dx,62-30(%edi)

	// The copy /paste thing...

	movb	16(%esi),%al
	movb	16+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30(%edi)
	movw	%dx,30-8(%edi)

	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-2(%edi)
	movw	%dx,30-10(%edi)

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-4(%edi)
	movw	%dx,30-12(%edi)

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-6(%edi)
	movw	%dx,30-14(%edi)

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-16(%edi)
	movw	%dx,30-24(%edi)

	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-18(%edi)
	movw	%dx,30-26(%edi)

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-20(%edi)
	movw	%dx,30-28(%edi)

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl
	movw	(%ebp,%eax,2),%cx
	movw	(%ebp,%ebx,2),%dx

	movw	%cx,30-22(%edi)
	movw	%dx,30-30(%edi)

	addl	$32,%esi		// Next Tile Line
	subl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_03:
	cmp	$0xDEADBEEF,%esi
endc_03:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Trans_Mapped_16)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_04:
	movl	%edx,endc_04-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
	movb	(%esi),%al
	movb	4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,8(%edi)
7:
	movb	1(%esi),%al
	movb	5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,10(%edi)
7:

	movb	2(%esi),%al
	movb	6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,12(%edi)
7:

	movb	3(%esi),%al
	movb	7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,14(%edi)
7:

	movb	8(%esi),%al
	movb	8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,24(%edi)
7:
	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,26(%edi)
7:

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,28(%edi)
7:

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30(%edi)
7:

// Copy... paste...
	
	movb	16(%esi),%al
	movb	16+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+8(%edi)
7:
	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+10(%edi)
7:

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+12(%edi)
7:

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+14(%edi)
7:

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+24(%edi)
7:
	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+26(%edi)
7:

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+28(%edi)
7:

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+30(%edi)
7:
		
	addl	$32,%esi		// Next Tile Line
	addl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_04:
	cmp	$0xDEADBEEF,%esi
endc_04:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Trans_Mapped_16_FlipY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_05:
	movl	%edx,endc_05-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
	movb	(%esi),%al
	movb	4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-8(%edi)
7:
	movb	1(%esi),%al
	movb	5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-10(%edi)
7:

	movb	2(%esi),%al
	movb	6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-12(%edi)
7:

	movb	3(%esi),%al
	movb	7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-14(%edi)
7:

	movb	8(%esi),%al
	movb	8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-24(%edi)
7:
	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-26(%edi)
7:

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-28(%edi)
7:

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-30(%edi)
7:

// Copy...

	movb	16(%esi),%al
	movb	16+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-8(%edi)
7:
	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-10(%edi)
7:

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-12(%edi)
7:

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-14(%edi)
7:

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-24(%edi)
7:
	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-26(%edi)
7:

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-28(%edi)
7:

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-30(%edi)
7:
	
	addl	$32,%esi		// Next Tile Line
	addl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_05:
	cmp	$0xDEADBEEF,%esi
endc_05:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Trans_Mapped_16_FlipX)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_06:
	movl	%edx,endc_06-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
	movb	(%esi),%al
	movb	4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,8(%edi)
7:
	movb	1(%esi),%al
	movb	5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,10(%edi)
7:

	movb	2(%esi),%al
	movb	6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,12(%edi)
7:

	movb	3(%esi),%al
	movb	7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,14(%edi)
7:

	movb	8(%esi),%al
	movb	8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,24(%edi)
7:
	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,26(%edi)
7:

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,28(%edi)
7:

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30(%edi)
7:

// Copy... paste... (from the non flipping version)
	
	movb	16(%esi),%al
	movb	16+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+8(%edi)
7:
	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+10(%edi)
7:

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+12(%edi)
7:

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+14(%edi)
7:

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+24(%edi)
7:
	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+26(%edi)
7:

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+28(%edi)
7:

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,32+22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,32+30(%edi)
7:
	
	addl	$32,%esi		// Next Tile Line
	subl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_06:
	cmp	$0xDEADBEEF,%esi
endc_06:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

FUNC(Draw32x32_Trans_Mapped_16_FlipXY)

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	movl	20(%esp),%esi			// source
	movl	28(%esp),%eax			// y
	movl	%esi,%edx
	sall	$2,%eax
	addl	$32*32,%edx			// tile end
	movl	0xDEADBEEF(%eax),%edi
blin_07:
	movl	%edx,endc_07-4
	movl	32(%esp),%ebp			// cmap
	addl	24(%esp),%edi			// x
	addl	24(%esp),%edi			// x

	xorl	%edx,%edx
	xorl	%ecx,%ecx
	xorl	%ebx,%ebx
	xorl	%eax,%eax
9:
// copy all this from the FlipY version...
	
	movb	(%esi),%al
	movb	4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-8(%edi)
7:
	movb	1(%esi),%al
	movb	5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-10(%edi)
7:

	movb	2(%esi),%al
	movb	6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-12(%edi)
7:

	movb	3(%esi),%al
	movb	7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-14(%edi)
7:

	movb	8(%esi),%al
	movb	8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-24(%edi)
7:
	movb	8+1(%esi),%al
	movb	8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-26(%edi)
7:

	movb	8+2(%esi),%al
	movb	8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-28(%edi)
7:

	movb	8+3(%esi),%al
	movb	8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,62-22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,62-30(%edi)
7:

// Copy...

	movb	16(%esi),%al
	movb	16+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-8(%edi)
7:
	movb	16+1(%esi),%al
	movb	16+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-2(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-10(%edi)
7:

	movb	16+2(%esi),%al
	movb	16+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-4(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-12(%edi)
7:

	movb	16+3(%esi),%al
	movb	16+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-6(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-14(%edi)
7:

	movb	16+8(%esi),%al
	movb	16+8+4(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-16(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-24(%edi)
7:
	movb	16+8+1(%esi),%al
	movb	16+8+5(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-18(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-26(%edi)
7:

	movb	16+8+2(%esi),%al
	movb	16+8+6(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-20(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-28(%edi)
7:

	movb	16+8+3(%esi),%al
	movb	16+8+7(%esi),%bl

	testb	%al,%al
	jz	7f
	movw	(%ebp,%eax,2),%cx
	movw	%cx,30-22(%edi)
7:	testb	%bl,%bl
	jz	7f
	movw	(%ebp,%ebx,2),%dx
	movw	%dx,30-30(%edi)
7:
	addl	$32,%esi		// Next Tile Line
	subl	$0xDEADBEEF,%edi	// Next Screen Line
bitw_07:
	cmp	$0xDEADBEEF,%esi
endc_07:
	jne	9b
	
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret

// The Draw32x32_16 functions are really uninteresting...
// If someone else wants to add them, he is welcome to !	

FUNC(init_spr32x32asm_16)


	movl	GLOBL(GameBitmap),%eax
	movl	(%eax),%eax		// Width
	addl	%eax,%eax
	movl	%eax,bitw_00-4
	movl	%eax,bitw_01-4
	movl	%eax,bitw_02-4
	movl	%eax,bitw_03-4
	movl	%eax,bitw_04-4
	movl	%eax,bitw_05-4
	movl	%eax,bitw_06-4
	movl	%eax,bitw_07-4

	movl	GLOBL(GameBitmap),%eax
	addl	$64,%eax		// Line 0
	movl	%eax,blin_00-4
	movl	%eax,blin_01-4
	movl	%eax,blin_04-4
	movl	%eax,blin_05-4
	addl	$31*4,%eax		// Line 31
	movl	%eax,blin_02-4
	movl	%eax,blin_03-4
	movl	%eax,blin_06-4
	movl	%eax,blin_07-4

	ret
