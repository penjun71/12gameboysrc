/******************************************************************************/
/*                                                                            */
/*                         RAINE 32x32 TILE DRAWING                           */
/*                                                                            */
/******************************************************************************/

/*

Mapped
Transparent Mapped
Direct-Mapped
Transparent Direct-Mapped

*/

void init_spr32x32asm(void);

// 32x32 mapped sprites

void Draw32x32_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 32x32 mapped transparent sprites

void Draw32x32_Trans_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

void init_spr32x32asm_16(void);

// 32x32 mapped sprites

void Draw32x32_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 32x32 mapped transparent sprites

void Draw32x32_Trans_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

void init_spr32x32asm_32(void);

// 32x32 mapped sprites

void Draw32x32_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 32x32 mapped transparent sprites

void Draw32x32_Trans_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw32x32_Trans_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// 32x32 sprites

void Draw32x32(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw32x32_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw32x32_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap);
void Draw32x32_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap);

// 32x32 transparent sprites
// Gave up here. Useless normaly.
