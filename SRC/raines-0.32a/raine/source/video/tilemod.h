/******************************************************************************/
/*                                                                            */
/*                               TILE MODIFIERS                               */
/*                                                                            */
/******************************************************************************/

#ifndef _tilemod_h_
#define _tilemod_h_

#include "deftypes.h"
#include "games.h"

/* So far, this type is only used in the about dialog !!! */
enum tile_type_num
{
   TILE_TYPE_UNKNOWN = 0,
   TILE_TYPE_8x8,
   TILE_TYPE_16x16,
   TILE_TYPE_32x32,
   TILE_TYPE_MAX,
};

extern char *tile_type[TILE_TYPE_MAX];

typedef struct TILE_INFO
{
   UINT32 count;
   UINT32 type;
   UINT32 width;
   UINT32 height;
   UINT8 *data;
   UINT8 *mask;
   UINT32 rotate;
   UINT32 flip;
} TILE_INFO;

struct TILE_INFO tile_list[0x10];

UINT32 tile_list_count;

extern UINT8 tile_8x8_map[8 * 8];
extern UINT8 tile_16x16_map[16 * 16];

/*

Rotation by 90 degrees

*/

void Rotate8x8(UINT8 *source, UINT32 count);
void Rotate8x8_4bpp(UINT8 *source, UINT32 count);
void Rotate16x16(UINT8 *source, UINT32 count);

/*

Flip on X Axis

*/

void Flip8x8_X(UINT8 *source, UINT32 count);
void Flip8x8_4bpp_X(UINT8 *source, UINT32 count);
void Flip16x16_X(UINT8 *source, UINT32 count);

/*

Flip on Y Axis

*/

void Flip8x8_Y(UINT8 *source, UINT32 count);
void Flip16x16_Y(UINT8 *source, UINT32 count);

/*

Count Colour bits (for F3-System 64 colour tile check)

*/

void CountColours(UINT8 *source, UINT32 count);

/*

Make Tile Solid/Transparent map

*/

UINT8 *MakeSolidTileMap16x16(UINT8 *source, UINT32 count);
UINT8 *MakeSolidTileMap64x64(UINT8 *source, UINT32 count);
UINT8 *MakeSolidTileMap32x32(UINT8 *source, UINT32 count);

UINT8 *make_solid_mask_8x8(UINT8 *source, UINT32 count);
UINT8 *make_solid_mask_16x8(UINT8 *source, UINT32 count);
UINT8 *make_solid_mask_16x16(UINT8 *source, UINT32 count);
UINT8 *make_solid_mask_32x32(UINT8 *source, UINT32 count);
UINT8 *make_solid_mask_64x64(UINT8 *source, UINT32 count);

UINT8 *make_solid_mask_8x8_4bpp(UINT8 *source, UINT32 count);
UINT8 *make_solid_mask_16x16_4bpp(UINT8 *source, UINT32 count);
UINT8 *make_solid_mask_64x64_4bpp(UINT8 *source, UINT32 count);

UINT8 *make_solid_mask_pad_8x8(UINT8 *source, UINT32 count, UINT32 pad_size);
UINT8 *make_solid_mask_pad_16x16(UINT8 *source, UINT32 count, UINT32 pad_size);

/*

generate used colour count map

*/

UINT8 *make_colour_count_16x16(UINT8 *source, UINT32 count);

UINT8 check_tile_solid(UINT8 *src, UINT32 size);
void check_tile_rotation(void);

UINT8 *decode_gfx(const UINT8 *src, const UINT32 src_size, const GFX_LAYOUT *gfx_layout);

/*

convert real bpp -> internal bpp

 8 ->  8
15 -> 16
16 -> 16
24 -> 32
32 -> 32

*/

UINT32 internal_bpp(UINT32 source);

#endif //_tilemod_h_
