/******************************************************************************/
/*                                                                            */
/*                            RAINE TILE DRAWING                              */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

#include "spr8x8.h"
#include "spr16x8.h"
#include "spr16x16.h"
#include "spr32x32.h"

#include "spp8x8.h"

#include "str6x8.h"

typedef void text_func(UINT8 *STR, int x, int y, UINT32 cmap);
extern text_func *textout_fast;

typedef void draw_mapped_zoom_xy_func(UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
typedef void draw_mapped_func(UINT8 *SPR, int x, int y, UINT8 *cmap);
// 8xh functions
typedef void drawh_mapped_func(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
// line scroll functions. I need the address of the buffer containing the
// offsets. That means a new ldraw�_mapped_func, and another version of the
// macros to build the tables... big sigh !
typedef void ldraw_mapped_func(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy);
typedef void draw_func(UINT8 *SPR, int x, int y, UINT8 cmap);

/***************************************/
/* These Flip functions tables are built automatically by MAKE_ROT_JUMP_16 *
 * You DON'T have to declare anything in newspr.c except this. */

extern draw_func *Draw8x8_Flip_Rot[4];

#define Draw8x8_flip_Rot(a, b, c, d, e) (*Draw8x8_Flip_Rot[e])(a, b, c, d)

#define Draw8x8_Rot(a, b, c, d)        (*Draw8x8_Flip_Rot[0])(a, b, c, d)
#define Draw8x8_FlipY_Rot(a, b, c, d)  (*Draw8x8_Flip_Rot[1])(a, b, c, d)
#define Draw8x8_FlipX_Rot(a, b, c, d)  (*Draw8x8_Flip_Rot[2])(a, b, c, d)
#define Draw8x8_FlipXY_Rot(a, b, c, d) (*Draw8x8_Flip_Rot[3])(a, b, c, d)

extern draw_func *Draw8x8_Trans_Flip_Rot[4];

#define Draw8x8_Trans_flip_Rot(a, b, c, d, e) (*Draw8x8_Trans_Flip_Rot[e])(a, b, c, d)

#define Draw8x8_Trans_Rot(a, b, c, d)        (*Draw8x8_Trans_Flip_Rot[0])(a, b, c, d)
#define Draw8x8_Trans_FlipY_Rot(a, b, c, d)  (*Draw8x8_Trans_Flip_Rot[1])(a, b, c, d)
#define Draw8x8_Trans_FlipX_Rot(a, b, c, d)  (*Draw8x8_Trans_Flip_Rot[2])(a, b, c, d)
#define Draw8x8_Trans_FlipXY_Rot(a, b, c, d) (*Draw8x8_Trans_Flip_Rot[3])(a, b, c, d)


extern draw_mapped_func *Draw8x8_Mapped_Flip_Rot[4];

#define Draw8x8_Mapped_flip_Rot(a, b, c, d, e) (*Draw8x8_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw8x8_Mapped_Rot(a, b, c, d)        (*Draw8x8_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw8x8_Mapped_FlipY_Rot(a, b, c, d)  (*Draw8x8_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw8x8_Mapped_FlipX_Rot(a, b, c, d)  (*Draw8x8_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw8x8_Mapped_FlipXY_Rot(a, b, c, d) (*Draw8x8_Mapped_Flip_Rot[3])(a, b, c, d)

extern draw_mapped_func *Draw8x8_Trans_Mapped_Flip_Rot[4];

#define Draw8x8_Trans_Mapped_flip_Rot(a, b, c, d, e) (*Draw8x8_Trans_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw8x8_Trans_Mapped_Rot(a, b, c, d)        (*Draw8x8_Trans_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw8x8_Trans_Mapped_FlipY_Rot(a, b, c, d)  (*Draw8x8_Trans_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw8x8_Trans_Mapped_FlipX_Rot(a, b, c, d)  (*Draw8x8_Trans_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw8x8_Trans_Mapped_FlipXY_Rot(a, b, c, d) (*Draw8x8_Trans_Mapped_Flip_Rot[3])(a, b, c, d)

extern draw_mapped_func *Draw8x8_Trans_Packed_Mapped_Flip_Rot[4];

#define Draw8x8_Trans_Packed_Mapped_flip_Rot(a, b, c, d, e) (*Draw8x8_Trans_Packed_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw8x8_Trans_Packed_Mapped_Rot(a, b, c, d)        (*Draw8x8_Trans_Packed_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw8x8_Trans_Packed_Mapped_FlipY_Rot(a, b, c, d)  (*Draw8x8_Trans_Packed_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw8x8_Trans_Packed_Mapped_FlipX_Rot(a, b, c, d)  (*Draw8x8_Trans_Packed_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw8x8_Trans_Packed_Mapped_FlipXY_Rot(a, b, c, d) (*Draw8x8_Trans_Packed_Mapped_Flip_Rot[3])(a, b, c, d)

extern draw_mapped_func *Draw8x8_Trans_Packed_Mapped_SwapWord_Flip_Rot[4];

#define Draw8x8_Trans_Packed_Mapped_SwapWord_flip_Rot(a, b, c, d, e) (*Draw8x8_Trans_Packed_Mapped_SwapWord_Flip_Rot[e])(a, b, c, d)

#define Draw8x8_Trans_Packed_Mapped_SwapWord_Rot(a, b, c, d)        (*Draw8x8_Trans_Packed_Mapped_SwapWord_Flip_Rot[0])(a, b, c, d)
#define Draw8x8_Trans_Packed_Mapped_SwapWord_FlipY_Rot(a, b, c, d)  (*Draw8x8_Trans_Packed_Mapped_SwapWord_Flip_Rot[1])(a, b, c, d)
#define Draw8x8_Trans_Packed_Mapped_SwapWord_FlipX_Rot(a, b, c, d)  (*Draw8x8_Trans_Packed_Mapped_SwapWord_Flip_Rot[2])(a, b, c, d)
#define Draw8x8_Trans_Packed_Mapped_SwapWord_FlipXY_Rot(a, b, c, d) (*Draw8x8_Trans_Packed_Mapped_SwapWord_Flip_Rot[3])(a, b, c, d)


extern draw_mapped_func *Draw16x16_Mapped_Flip_Rot[4];

#define Draw16x16_Mapped_flip_Rot(a, b, c, d, e) (*Draw16x16_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw16x16_Mapped_Rot(a, b, c, d)        (*Draw16x16_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw16x16_Mapped_FlipY_Rot(a, b, c, d)  (*Draw16x16_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw16x16_Mapped_FlipX_Rot(a, b, c, d)  (*Draw16x16_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw16x16_Mapped_FlipXY_Rot(a, b, c, d) (*Draw16x16_Mapped_Flip_Rot[3])(a, b, c, d)

extern draw_mapped_func *Draw16x16_Trans_Mapped_Flip_Rot[4];

#define Draw16x16_Trans_Mapped_flip_Rot(a, b, c, d, e) (*Draw16x16_Trans_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw16x16_Trans_Mapped_Rot(a, b, c, d)        (*Draw16x16_Trans_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw16x16_Trans_Mapped_FlipY_Rot(a, b, c, d)  (*Draw16x16_Trans_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw16x16_Trans_Mapped_FlipX_Rot(a, b, c, d)  (*Draw16x16_Trans_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw16x16_Trans_Mapped_FlipXY_Rot(a, b, c, d) (*Draw16x16_Trans_Mapped_Flip_Rot[3])(a, b, c, d)

extern draw_mapped_func *Draw32x32_Mapped_Flip_Rot[4];

#define Draw32x32_Mapped_flip_Rot(a, b, c, d, e) (*Draw32x32_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw32x32_Mapped_Rot(a, b, c, d)        (*Draw32x32_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw32x32_Mapped_FlipY_Rot(a, b, c, d)  (*Draw32x32_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw32x32_Mapped_FlipX_Rot(a, b, c, d)  (*Draw32x32_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw32x32_Mapped_FlipXY_Rot(a, b, c, d) (*Draw32x32_Mapped_Flip_Rot[3])(a, b, c, d)

extern draw_mapped_func *Draw32x32_Trans_Mapped_Flip_Rot[4];

#define Draw32x32_Trans_Mapped_flip_Rot(a, b, c, d, e) (*Draw32x32_Trans_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw32x32_Trans_Mapped_Rot(a, b, c, d)        (*Draw32x32_Trans_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw32x32_Trans_Mapped_FlipY_Rot(a, b, c, d)  (*Draw32x32_Trans_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw32x32_Trans_Mapped_FlipX_Rot(a, b, c, d)  (*Draw32x32_Trans_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw32x32_Trans_Mapped_FlipXY_Rot(a, b, c, d) (*Draw32x32_Trans_Mapped_Flip_Rot[3])(a, b, c, d)

// Line scrolls (almost identical)

extern ldraw_mapped_func *ldraw16x16_Mapped_Flip_Rot[4];

#define ldraw16x16_Mapped_flip_Rot(a, b, c, d, e,f) (*ldraw16x16_Mapped_Flip_Rot[f])(a, b, c, d, e)

#define ldraw16x16_Mapped_Rot(a, b, c, d, e)        (*ldraw16x16_Mapped_Flip_Rot[0])(a, b, c, d, e)
#define ldraw16x16_Mapped_FlipY_Rot(a, b, c, d, e)  (*ldraw16x16_Mapped_Flip_Rot[1])(a, b, c, d, e)
#define ldraw16x16_Mapped_FlipX_Rot(a, b, c, d, e)  (*ldraw16x16_Mapped_Flip_Rot[2])(a, b, c, d, e)
#define ldraw16x16_Mapped_FlipXY_Rot(a, b, c, d, e) (*ldraw16x16_Mapped_Flip_Rot[3])(a, b, c, d, e)

extern ldraw_mapped_func *ldraw16x16_Trans_Mapped_Flip_Rot[4];

#define ldraw16x16_Trans_Mapped_flip_Rot(a, b, c, d, e, f) (*ldraw16x16_Trans_Mapped_Flip_Rot[f])(a, b, c, d, e)

#define ldraw16x16_Trans_Mapped_Rot(a, b, c, d, e)        (*ldraw16x16_Trans_Mapped_Flip_Rot[0])(a, b, c, d, e)
#define ldraw16x16_Trans_Mapped_FlipY_Rot(a, b, c, d, e)  (*ldraw16x16_Trans_Mapped_Flip_Rot[1])(a, b, c, d, e)
#define ldraw16x16_Trans_Mapped_FlipX_Rot(a, b, c, d, e)  (*ldraw16x16_Trans_Mapped_Flip_Rot[2])(a, b, c, d, e)
#define ldraw16x16_Trans_Mapped_FlipXY_Rot(a, b, c, d, e) (*ldraw16x16_Trans_Mapped_Flip_Rot[3])(a, b, c, d, e)



extern draw_mapped_zoom_xy_func *Draw16x16_Mapped_ZoomXY_Flip_Rot[4];

#define Draw16x16_Mapped_ZoomXY_flip_Rot(a, b, c, d, e, f, g) (*Draw16x16_Mapped_ZoomXY_Flip_Rot[g])(a, b, c, d, e, f)

#define Draw16x16_Mapped_ZoomXY_Rot(a, b, c, d, e, f)        (*Draw16x16_Mapped_ZoomXY_Flip_Rot[0])(a, b, c, d, e, f)
#define Draw16x16_Mapped_ZoomXY_FlipY_Rot(a, b, c, d, e, f)  (*Draw16x16_Mapped_ZoomXY_Flip_Rot[1])(a, b, c, d, e, f)
#define Draw16x16_Mapped_ZoomXY_FlipX_Rot(a, b, c, d, e, f)  (*Draw16x16_Mapped_ZoomXY_Flip_Rot[2])(a, b, c, d, e, f)
#define Draw16x16_Mapped_ZoomXY_FlipXY_Rot(a, b, c, d, e, f) (*Draw16x16_Mapped_ZoomXY_Flip_Rot[3])(a, b, c, d, e, f)

extern draw_mapped_zoom_xy_func *Draw16x16_Trans_Mapped_ZoomXY_Flip_Rot[4];

#define Draw16x16_Trans_Mapped_ZoomXY_flip_Rot(a, b, c, d, e, f, g) (*Draw16x16_Trans_Mapped_ZoomXY_Flip_Rot[g])(a, b, c, d, e, f)

#define Draw16x16_Trans_Mapped_ZoomXY_Rot(a, b, c, d, e, f)        (*Draw16x16_Trans_Mapped_ZoomXY_Flip_Rot[0])(a, b, c, d, e, f)
#define Draw16x16_Trans_Mapped_ZoomXY_FlipY_Rot(a, b, c, d, e, f)  (*Draw16x16_Trans_Mapped_ZoomXY_Flip_Rot[1])(a, b, c, d, e, f)
#define Draw16x16_Trans_Mapped_ZoomXY_FlipX_Rot(a, b, c, d, e, f)  (*Draw16x16_Trans_Mapped_ZoomXY_Flip_Rot[2])(a, b, c, d, e, f)
#define Draw16x16_Trans_Mapped_ZoomXY_FlipXY_Rot(a, b, c, d, e, f) (*Draw16x16_Trans_Mapped_ZoomXY_Flip_Rot[3])(a, b, c, d, e, f)

// 64x64 Mapped Sprites

void Draw64x64_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);
//16 bpp version
void Draw64x64_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);
//32 bpp version
void Draw64x64_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);

// Rot definition

extern draw_mapped_func *Draw64x64_Mapped_Flip_Rot[4];

#define Draw64x64_Mapped_flip_Rot(a, b, c, d, e) (*Draw64x64_Mapped_Flip_Rot[e])(a, b, c, d)

#define Draw64x64_Mapped_Rot(a, b, c, d)        (*Draw64x64_Mapped_Flip_Rot[0])(a, b, c, d)
#define Draw64x64_Mapped_FlipY_Rot(a, b, c, d)  (*Draw64x64_Mapped_Flip_Rot[1])(a, b, c, d)
#define Draw64x64_Mapped_FlipX_Rot(a, b, c, d)  (*Draw64x64_Mapped_Flip_Rot[2])(a, b, c, d)
#define Draw64x64_Mapped_FlipXY_Rot(a, b, c, d) (*Draw64x64_Mapped_Flip_Rot[3])(a, b, c, d)

// 64x64 Transparent Mapped Sprites

void Draw64x64_Trans_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Trans_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Trans_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap);
void Draw64x64_Trans_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap);
// no 16bpp version (yet)
// no 32bpp version (yet)

// Specially for F3 System (pixel layer)

void Draw8xH_Trans_Packed_Mapped_Column(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_FlipY(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_FlipX(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_FlipXY(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
//16 bpp version
void Draw8xH_Trans_Packed_Mapped_Column_16(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_16_FlipY(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_16_FlipX(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_16_FlipXY(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
//32 bpp version
void Draw8xH_Trans_Packed_Mapped_Column_32(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_32_FlipY(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_32_FlipX(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);
void Draw8xH_Trans_Packed_Mapped_Column_32_FlipXY(UINT8 *SPR, int x, int y, int height, UINT8 *cmap);

// Rot definition

extern drawh_mapped_func *Draw8xH_Trans_Packed_Mapped_Column_Flip_Rot[4];

#define Draw8xH_Trans_Packed_Mapped_Column_flip_Rot(a, b, c, d, e,f) (*Draw8xH_Trans_Packed_Mapped_Column_Flip_Rot[f])(a, b, c, d,e)

#define Draw8xH_Trans_Packed_Mapped_Column_Rot(a, b, c, d, e)        (*Draw8xH_Trans_Packed_Mapped_Column_Flip_Rot[0])(a, b, c, d, e)
#define Draw8xH_Trans_Packed_Mapped_Column_FlipY_Rot(a, b, c, d, e)  (*Draw8xH_Trans_Packed_Mapped_Column_Flip_Rot[1])(a, b, c, d, e)
#define Draw8xH_Trans_Packed_Mapped_Column_FlipX_Rot(a, b, c, d, e)  (*Draw8xH_Trans_Packed_Mapped_Column_Flip_Rot[2])(a, b, c, d, e)
#define Draw8xH_Trans_Packed_Mapped_Column_FlipXY_Rot(a, b, c, d, e) (*Draw8xH_Trans_Packed_Mapped_Column_Flip_Rot[3])(a, b, c, d, e)

void init_newspr2asm();
void init_newspr2asm_16();
void init_newspr2asm_32();

// Draw Flipping Jumptables

extern draw_mapped_func *Draw16x16_Mapped_JumpList[];
extern draw_mapped_func *Draw16x16_Mapped_JumpListR[];

extern draw_mapped_func *Draw16x16_Trans_Mapped_JumpList[];
extern draw_mapped_func *Draw16x16_Trans_Mapped_JumpListR[];

void init_video_core(void);
