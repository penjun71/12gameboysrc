/******************************************************************************/
/*                                                                            */
/*                   SOME CUSTOM ASM VIDEO SUPPORT ROUTINES                   */
/*                                                                            */
/******************************************************************************/

#include "raine.h"

UINT32 SourceSeg;
UINT32 DestSeg;
UINT32 SourceStart;
UINT32 DestStart;
UINT32 BlitWidth;
UINT32 BlitHeight;
UINT32 SourceAdd;
UINT32 DestAdd;

void fast_blit_movsl(void);
void fast_blit_movl(void);
void fast_blit_movq(void);

// Update only changed palette entries, and vsync if enabled
// even if it wasn't needed

void fast_palette_update(PALETTE p, PALETTE p2, int vsync);

// Update only changed palette entries, and only vsync if
// a changed entry exists

void fast_palette_update_vsync_check(PALETTE p, PALETTE p2);
