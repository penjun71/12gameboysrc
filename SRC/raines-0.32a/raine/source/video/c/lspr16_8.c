/* Raine line scroll. Made for cave driver : 16x16 sprites, 16bpp,
   and offsets of the lines every 4 bytes */

#include "deftypes.h"
#include "raine.h"

void ldraw16x16_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;
   // No bound checking : temptating, but almost sure to fail soon or later !
   for(yy=0; yy<16; yy++){
     //dx = x+ReadWord(dy+yy*4);
     line = ((UINT8 *)(GameBitmap->line[y+yy]))+ x+ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line++ = ((UINT8 *)cmap)[ *SPR++ ];
      }
   }
}

void ldraw16x16_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy]))+ x+ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line++ = ((UINT8 *)cmap)[ *SPR++ ];
      }
   }
}

void ldraw16x16_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + 7+ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line-- = ((UINT8 *)cmap)[ *SPR++ ];
      }
   }
}

void ldraw16x16_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + 7+ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line-- = ((UINT8 *)cmap)[ *SPR++ ];
      }
   }
}

// 16x16 mapped transparent sprites

void ldraw16x16_Trans_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
     //dx = x+ReadWord(dy+yy*4);
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x+ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = ((UINT8 *)cmap)[ *SPR ];
      }
   }

}

void ldraw16x16_Trans_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = ((UINT8 *)cmap)[ *SPR ];
      }
   }
}

void ldraw16x16_Trans_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = ((UINT8 *)cmap)[ *SPR ];
      }
   }
}

void ldraw16x16_Trans_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = ((UINT8 *)cmap)[ *SPR ];
      }
   }
}

// 16x16 sprites

void ldraw16x16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line++ = *SPR++ | cmap;
      }
   }
}

void ldraw16x16_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line++ = *SPR++ | cmap;
      }
   }
}

void ldraw16x16_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + 7 + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line-- = *SPR++ | cmap;
      }
   }
}

void ldraw16x16_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + 7 + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++){
         *line-- = *SPR++ | cmap;
      }
   }
}

// 16x16 transparent sprites

void ldraw16x16_Trans(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void ldraw16x16_Trans_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void ldraw16x16_Trans_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void ldraw16x16_Trans_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT8 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+yy*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}
