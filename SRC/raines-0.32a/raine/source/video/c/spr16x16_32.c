/******************************************************************************/
/*                                                                            */
/*                         RAINE 16x16 TILE DRAWING                           */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"
#include "raine.h"

/*

Mapped
Transparent Mapped
Direct-Mapped
Transparent Direct-Mapped

*/

void init_spr16x16asm_32(void)
{
}

// 16x16 mapped sprites

void Draw16x16_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++){
         *line++ = cmap[ *SPR++ ];
      }
   }
}

void Draw16x16_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++){
         *line++ = cmap[ *SPR++ ];
      }
   }
}

void Draw16x16_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x + 7;
      for(xx=0; xx<16; xx++){
         *line-- = cmap[ *SPR++ ];
      }
   }
}

void Draw16x16_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x + 7;
      for(xx=0; xx<16; xx++){
         *line-- = cmap[ *SPR++ ];
      }
   }
}

// 16x16 mapped transparent sprites

void Draw16x16_Trans_Mapped_32(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

void Draw16x16_Trans_Mapped_32_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

void Draw16x16_Trans_Mapped_32_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

void Draw16x16_Trans_Mapped_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

// 16x16 sprites

void Draw16x16_32(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++){
         *line++ = *SPR++ | cmap;
      }
   }
}

void Draw16x16_32_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++){
         *line++ = *SPR++ | cmap;
      }
   }
}

void Draw16x16_32_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x + 7;
      for(xx=0; xx<16; xx++){
         *line-- = *SPR++ | cmap;
      }
   }
}

void Draw16x16_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x + 7;
      for(xx=0; xx<16; xx++){
         *line-- = *SPR++ | cmap;
      }
   }
}

// 16x16 transparent sprites

void Draw16x16_Trans_32(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void Draw16x16_Trans_32_FlipX(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void Draw16x16_Trans_32_FlipY(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void Draw16x16_Trans_32_FlipXY(UINT8 *SPR, int x, int y, UINT8 cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}
