/******************************************************************************/
/*                                                                            */
/*                 RAINE 8x8 TILE DRAWING (4BPP PACKED DATA)                  */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"
#include "raine.h"


/*

Transparent Packed Mapped

*/

// 8x8 4bpp Packed Mapped Sprites

void init_spp8x8asm(void)
{
}


// 8x8 4bpp Packed Mapped Transparent Sprites
// [10325476]

void Draw8x8_Trans_Packed_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}

void Draw8x8_Trans_Packed_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}

void Draw8x8_Trans_Packed_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}

void Draw8x8_Trans_Packed_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}


// 8x8 4bpp Packed Mapped Transparent Sprites (word swapped)
// [54761032]

void Draw8x8_Trans_Packed_Mapped_SwapWord(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}

void Draw8x8_Trans_Packed_Mapped_SwapWord_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}

void Draw8x8_Trans_Packed_Mapped_SwapWord_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}

void Draw8x8_Trans_Packed_Mapped_SwapWord_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
}
