/* Raine line scroll. Made for cave driver : 16x16 sprites, 16bpp,
   and offsets of the lines every 4 bytes */

#include "deftypes.h"
#include "raine.h"

void ldraw16x16_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;
   // No bound checking : temptating, but almost sure to fail soon or later !
   for(yy=0; yy<16; yy++){
     //dx = x+ReadWord(dy+yy*4);
     line = ((UINT16 *)(GameBitmap->line[y+yy]))+ x+ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line++ = ((UINT16 *)cmap)[ *SPR++ ];
      }
   }
}

void cdraw16x16_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
  // This is necessary in cave games because of rotation (270)
  UINT16 *line;
   int xx,yy,offs;
   for(yy=0; yy<16; yy++){
     //dx = x+ReadWord(dy+yy*4); 
     for(xx=0; xx<16; xx++){
       offs = y+yy+(ReadWord(dy+(x+xx-32)*4)&0x1ff);
       if (offs > 320){
	 //fprintf(stderr,"overflow\n");
	 continue;
       }
       //fprintf(stderr,"dy %d %d %d\n",offs,x,xx);
       line = ((UINT16 *)(GameBitmap->line[offs]))+ x+xx;
       *line = ((UINT16 *)cmap)[ *SPR++ ];
     }
   }
}

void ldraw16x16_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy]))+ x+ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line++ = ((UINT16 *)cmap)[ *SPR++ ];
      }
   }
}

void ldraw16x16_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + 7+ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line-- = ((UINT16 *)cmap)[ *SPR++ ];
      }
   }
}

void ldraw16x16_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + 7+ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line-- = ((UINT16 *)cmap)[ *SPR++ ];
      }
   }
}

// 16x16 mapped transparent sprites

void ldraw16x16_Trans_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
     //dx = x+ReadWord(dy+(y+yy-32)*4);
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x+ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = ((UINT16 *)cmap)[ *SPR ];
      }
   }

}

void cdraw16x16_Trans_Mapped_16(UINT8 *SPR, int x, int y, UINT8 *cmap,UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
     //dx = x+ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++, SPR++){
	if(*SPR){
	  line = ((UINT16 *)(GameBitmap->line[y+yy])) + x+ReadWord(dy+(x+xx-32)*4)+xx;
	  *line = ((UINT16 *)cmap)[ *SPR ];
	}
      }
   }

}

void ldraw16x16_Trans_Mapped_16_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = ((UINT16 *)cmap)[ *SPR ];
      }
   }
}

void ldraw16x16_Trans_Mapped_16_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = ((UINT16 *)cmap)[ *SPR ];
      }
   }
}

void ldraw16x16_Trans_Mapped_16_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = ((UINT16 *)cmap)[ *SPR ];
      }
   }
}

// 16x16 sprites

void ldraw16x16_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line++ = *SPR++ | cmap;
      }
   }
}

void ldraw16x16_FlipX_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line++ = *SPR++ | cmap;
      }
   }
}

void ldraw16x16_FlipY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + 7 + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line-- = *SPR++ | cmap;
      }
   }
}

void ldraw16x16_FlipXY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + 7 + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++){
         *line-- = *SPR++ | cmap;
      }
   }
}

// 16x16 transparent sprites

void ldraw16x16_Trans_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void ldraw16x16_Trans_FlipX_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void ldraw16x16_Trans_FlipY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=0; yy<16; yy++){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}

void ldraw16x16_Trans_FlipXY_16(UINT8 *SPR, int x, int y, UINT8 cmap, UINT8 *dy)
{
   UINT16 *line;
   int xx,yy;

   for(yy=15; yy>=0; yy--){
      line = ((UINT16 *)(GameBitmap->line[y+yy])) + x + ReadWord(dy+(y+yy-32)*4);
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = *SPR | cmap;
      }
   }
}
