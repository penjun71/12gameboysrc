/******************************************************************************/
/*                                                                            */
/*                         RAINE 16x8 TILE DRAWING                            */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"
#include "raine.h"

/*

Mapped
Transparent Mapped

*/

void init_spr16x8asm(void)
{
}

// 16x8 mapped sprites

void Draw16x8_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<8; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++){
         *line++ = cmap[ *SPR++ ];
      }
   }
}

void Draw16x8_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=7; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++){
         *line++ = cmap[ *SPR++ ];
      }
   }
}

void Draw16x8_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<8; yy++){
      line = GameBitmap->line[y+yy] + x + 7;
      for(xx=0; xx<16; xx++){
         *line-- = cmap[ *SPR++ ];
      }
   }
}

void Draw16x8_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=7; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x + 7;
      for(xx=0; xx<16; xx++){
         *line-- = cmap[ *SPR++ ];
      }
   }
}

// 16x8 mapped transparent sprites

void Draw16x8_Trans_Mapped(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<8; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

void Draw16x8_Trans_Mapped_FlipX(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=7; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=0; xx<16; xx++, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

void Draw16x8_Trans_Mapped_FlipY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=0; yy<8; yy++){
      line = GameBitmap->line[y+yy] + x;
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

void Draw16x8_Trans_Mapped_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap)
{
   UINT8 *line;
   int xx,yy;

   for(yy=7; yy>=0; yy--){
      line = GameBitmap->line[y+yy] + x;
      for(xx=15; xx>=0; xx--, SPR++){
         if(*SPR)
            line[xx] = cmap[ *SPR ];
      }
   }
}

