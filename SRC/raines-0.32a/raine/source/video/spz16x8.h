/******************************************************************************/
/*                                                                            */
/*                          RAINE 16x8 TILE ZOOMING                           */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

// These ones can zoom from x/y = [0..16] (ie. max size is normal: 16x8)

void init_16x8_zoom(void);
UINT8 *make_16x8_zoom_ofs_type1(void);
UINT8 *make_16x8_zoom_ofs_type1z(void);
UINT8 *make_16x8_zoom_ofs_type1zz(void);

void Draw16x8_Trans_Mapped_ZoomXY       (UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
void Draw16x8_Trans_Mapped_ZoomXY_FlipY (UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
void Draw16x8_Trans_Mapped_ZoomXY_FlipX (UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
void Draw16x8_Trans_Mapped_ZoomXY_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);

void Draw16x8_Mapped_ZoomXY       (UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
void Draw16x8_Mapped_ZoomXY_FlipY (UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
void Draw16x8_Mapped_ZoomXY_FlipX (UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
void Draw16x8_Mapped_ZoomXY_FlipXY(UINT8 *SPR, int x, int y, UINT8 *cmap, int zoom_x, int zoom_y);
