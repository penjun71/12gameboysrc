/******************************************************************************/
/*                                                                            */
/*                        RAINE FILE ACCESS/ZIP SUPPORT                       */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"
#include "unzip.h"

int load_file(char *filename, UINT8 *dest, UINT32 size);
int save_file(char *filename, UINT8 *source, UINT32 size);

// zip files
int unz_locate_file_crc32(unzFile file, UINT32 crc32);
int unz_locate_file_name(unzFile file, char *name);
int size_file(char *filename);

#ifdef RAINE_DEBUG

void save_debug(UINT8 *name, UINT8 *src, UINT32 size, UINT32 mode);

#endif
