#ifdef RAINE_DOS

#define CODE_SEG				\
.text					;	\

#define FUNC(name)				\
.p2align 2				;	\
.globl _##name				;	\
_##name:				;	\

#define GLOBL(name)		_##name		\

#endif

#ifdef RAINE_UNIX

#define CODE_SEG				\
.data					;	\

#define FUNC(name)				\
.p2align 2				;	\
.globl name				;	\
##name:					;	\

#define GLOBL(name)		name		\

#endif

#ifdef RAINE_WIN32

#define CODE_SEG				\
.data					;	\

#define FUNC(name)				\
.p2align 2				;	\
.globl _##name				;	\
_##name:				;	\

#define GLOBL(name)		_##name		\

#endif


