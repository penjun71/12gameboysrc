/******************************************************************************/
/*                                                                            */
/*                       RAINE CONTROL / INPUT / LED                          */
/*                                                                            */
/******************************************************************************/

#include "deftypes.h"

/******************************************************************************/

#define BIT_ACTIVE_0 (0)
#define BIT_ACTIVE_1 (1)

enum {
   RJOY_NULL = 0,

   RJOY_1_UP,
   RJOY_1_DOWN,
   RJOY_1_LEFT,
   RJOY_1_RIGHT,

   RJOY_1_BUTTON1,
   RJOY_1_BUTTON2,
   RJOY_1_BUTTON3,
   RJOY_1_BUTTON4,
   RJOY_1_BUTTON5,
   RJOY_1_BUTTON6,
   RJOY_1_BUTTON7,
   RJOY_1_BUTTON8,

   RJOY_2_UP,
   RJOY_2_DOWN,
   RJOY_2_LEFT,
   RJOY_2_RIGHT,

   RJOY_2_BUTTON1,
   RJOY_2_BUTTON2,
   RJOY_2_BUTTON3,
   RJOY_2_BUTTON4,
   RJOY_2_BUTTON5,
   RJOY_2_BUTTON6,
   RJOY_2_BUTTON7,
   RJOY_2_BUTTON8,

   RJOY_3_UP,
   RJOY_3_DOWN,
   RJOY_3_LEFT,
   RJOY_3_RIGHT,

   RJOY_3_BUTTON1,
   RJOY_3_BUTTON2,
   RJOY_3_BUTTON3,
   RJOY_3_BUTTON4,
   RJOY_3_BUTTON5,
   RJOY_3_BUTTON6,
   RJOY_3_BUTTON7,
   RJOY_3_BUTTON8,

   RJOY_4_UP,
   RJOY_4_DOWN,
   RJOY_4_LEFT,
   RJOY_4_RIGHT,

   RJOY_4_BUTTON1,
   RJOY_4_BUTTON2,
   RJOY_4_BUTTON3,
   RJOY_4_BUTTON4,
   RJOY_4_BUTTON5,
   RJOY_4_BUTTON6,
   RJOY_4_BUTTON7,
   RJOY_4_BUTTON8,

   RJOY_1_BUTTON9,
   RJOY_1_BUTTON10,
   RJOY_1_BUTTON11,
   RJOY_1_BUTTON12,

   RJOY_2_BUTTON9,
   RJOY_2_BUTTON10,
   RJOY_2_BUTTON11,
   RJOY_2_BUTTON12,

   RJOY_3_BUTTON9,
   RJOY_3_BUTTON10,
   RJOY_3_BUTTON11,
   RJOY_3_BUTTON12,

   RJOY_4_BUTTON9,
   RJOY_4_BUTTON10,
   RJOY_4_BUTTON11,
   RJOY_4_BUTTON12,

   RJOY_COUNT,
};

/******************************************************************************/
/*                                                                            */
/*                           DEFAULT KEY SETTINGS                             */
/*                                                                            */
/******************************************************************************/

enum {
   KB_DEF_COIN1 = 0x100,	// Coin A
   KB_DEF_COIN2,		// Coin B
   KB_DEF_COIN3,		// Coin C
   KB_DEF_COIN4,		// Coin D

   KB_DEF_TILT,			// Tilt
   KB_DEF_SERVICE,		// Service
   KB_DEF_TEST,			// Test

   KB_DEF_P1_START,		// P1 Start

   KB_DEF_P1_UP,		// P1 Joystick
   KB_DEF_P1_DOWN,
   KB_DEF_P1_LEFT,
   KB_DEF_P1_RIGHT,

   KB_DEF_P1_B1,		// 8 Buttons
   KB_DEF_P1_B2,
   KB_DEF_P1_B3,
   KB_DEF_P1_B4,
   KB_DEF_P1_B5,
   KB_DEF_P1_B6,
   KB_DEF_P1_B7,
   KB_DEF_P1_B8,

   KB_DEF_P2_START,		// P2 Start
   KB_DEF_P2_UP,		// P2 Joystick
   KB_DEF_P2_DOWN,
   KB_DEF_P2_LEFT,
   KB_DEF_P2_RIGHT,

   KB_DEF_P2_B1,		// 8 Buttons
   KB_DEF_P2_B2,
   KB_DEF_P2_B3,
   KB_DEF_P2_B4,
   KB_DEF_P2_B5,
   KB_DEF_P2_B6,
   KB_DEF_P2_B7,
   KB_DEF_P2_B8,

   KB_DEF_P3_START,		// P3 Start

   KB_DEF_P3_UP,		// P3 Joystick
   KB_DEF_P3_DOWN,
   KB_DEF_P3_LEFT,
   KB_DEF_P3_RIGHT,

   KB_DEF_P3_B1,		// 8 Buttons
   KB_DEF_P3_B2,
   KB_DEF_P3_B3,
   KB_DEF_P3_B4,
   KB_DEF_P3_B5,
   KB_DEF_P3_B6,
   KB_DEF_P3_B7,
   KB_DEF_P3_B8,

   KB_DEF_P4_START,		// P4 Start

   KB_DEF_P4_UP,		// P4 Joystick
   KB_DEF_P4_DOWN,
   KB_DEF_P4_LEFT,
   KB_DEF_P4_RIGHT,

   KB_DEF_P4_B1,		// 8 Buttons
   KB_DEF_P4_B2,
   KB_DEF_P4_B3,
   KB_DEF_P4_B4,
   KB_DEF_P4_B5,
   KB_DEF_P4_B6,
   KB_DEF_P4_B7,
   KB_DEF_P4_B8,

   KB_DEF_FLIPPER_1_L,		// Pinball Controls
   KB_DEF_FLIPPER_1_R,
   KB_DEF_FLIPPER_2_L,
   KB_DEF_FLIPPER_2_R,
   KB_DEF_TILT_L,
   KB_DEF_TILT_R,
   KB_DEF_B1_L,
   KB_DEF_B1_R,

   KB_DEF_END,
};

#define KB_DEF_COUNT	(KB_DEF_END-0x100)

/******************************************************************************/
/*                                                                            */
/*                       DEFAULT EMULATOR KEY SETTINGS                        */
/*                                                                            */
/******************************************************************************/

enum {
   KB_EMU_SCREENSHOT = 0,
   KB_EMU_INC_FRAMESKIP,
   KB_EMU_DEC_FRAMESKIP,
   KB_EMU_INC_CPU_FRAMESKIP,
   KB_EMU_DEC_CPU_FRAMESKIP,
   KB_EMU_SAVE_GAME,
   KB_EMU_SWITCH_SAVE_SLOT,
   KB_EMU_LOAD_GAME,
   KB_EMU_SWITCH_FPS,
   KB_EMU_PAUSE_GAME,
   KB_EMU_STOP_EMULATION,
   KB_EMU_OPEN_GUI,
   KB_EMU_SCREEN_UP,
   KB_EMU_SCREEN_DOWN,
   KB_EMU_SCREEN_LEFT,
   KB_EMU_SCREEN_RIGHT,
   KB_EMU_SWITCH_MIXER,
   
   KB_EMU_DEF_COUNT,
};

typedef struct DEF_INPUT
{
   UINT8 scancode;
   UINT8 joycode;
   char *name;
} DEF_INPUT;

extern struct DEF_INPUT def_input_list[KB_DEF_COUNT];
extern struct DEF_INPUT def_input_list_emu[KB_EMU_DEF_COUNT];

/******************************************************************************/

int JoystickType;		// Type of Joystick chosen (see raine.cfg)

int use_custom_keys;
int joy_use_custom_keys;

typedef struct INPUT
{
   UINT8 Key;			// keyboard code for this input
   UINT8 Joy;			// joystick code for this input
   UINT8 *InputName;		// input name string
   UINT32 Address;		// address in RAM[] for this input
   UINT8 Bit;			// bit mask for this input
   UINT8 high_bit;		// input is acitve high or active low
   UINT16 default_key;		// default button for this input
   UINT8 auto_rate;		// autofire frame rate
   UINT32 active_time;		// frames since the input became active (used for coin time)
} INPUT;

struct INPUT InputList[64];	// Max 64 control inputs in a game

int InputCount;			// Number of Inputs in InputList

void init_inputs(void);

UINT8 input_buffer[0x100];

void reset_game_keys(void);
void joy_reset_game_keys(void);

void load_game_keys(char *section);
void save_game_keys(char *section);
void load_game_joys(char *section);
void save_game_joys(char *section);

void load_default_keys(char *section);
void save_default_keys(char *section);
void load_default_joys(char *section);
void save_default_joys(char *section);

void load_emulator_keys(char *section);
void save_emulator_keys(char *section);
void load_emulator_joys(char *section);
void save_emulator_joys(char *section);

// ProcessInputs():
// Goes through the input list setting/clearing the mapped RAM[] bits

// rjoy[] is an array, which should act just like allegro's key[] array.
// only difference is that it must be updated by the user by calling
// update_rjoy_list().

UINT8 rjoy[RJOY_COUNT];

void update_rjoy_list(void);

void update_inputs(void);

int use_leds;

void switch_led(int num, int on);
void update_leds(void);
void init_leds(void);
void force_update_leds(void);

extern char MSG_COIN1[];
extern char MSG_COIN2[];
extern char MSG_COIN3[];
extern char MSG_COIN4[];

extern char MSG_TILT[];
extern char MSG_SERVICE[];
extern char MSG_TEST[];
extern char MSG_UNKNOWN[];
extern char MSG_YES[];
extern char MSG_NO[];
extern char MSG_FREE_PLAY[];
extern char MSG_UNUSED[];
extern char MSG_COINAGE[];

extern char MSG_P1_START[];

extern char MSG_P1_UP[];
extern char MSG_P1_DOWN[];
extern char MSG_P1_LEFT[];
extern char MSG_P1_RIGHT[];

extern char MSG_P1_B1[];
extern char MSG_P1_B2[];
extern char MSG_P1_B3[];
extern char MSG_P1_B4[];
extern char MSG_P1_B5[];
extern char MSG_P1_B6[];
extern char MSG_P1_B7[];
extern char MSG_P1_B8[];

extern char MSG_P2_START[];

extern char MSG_P2_UP[];
extern char MSG_P2_DOWN[];
extern char MSG_P2_LEFT[];
extern char MSG_P2_RIGHT[];

extern char MSG_P2_B1[];
extern char MSG_P2_B2[];
extern char MSG_P2_B3[];
extern char MSG_P2_B4[];
extern char MSG_P2_B5[];
extern char MSG_P2_B6[];
extern char MSG_P2_B7[];
extern char MSG_P2_B8[];

extern char MSG_P3_START[];

extern char MSG_P3_UP[];
extern char MSG_P3_DOWN[];
extern char MSG_P3_LEFT[];
extern char MSG_P3_RIGHT[];

extern char MSG_P3_B1[];
extern char MSG_P3_B2[];
extern char MSG_P3_B3[];
extern char MSG_P3_B4[];
extern char MSG_P3_B5[];
extern char MSG_P3_B6[];
extern char MSG_P3_B7[];
extern char MSG_P3_B8[];

extern char MSG_P4_START[];

extern char MSG_P4_UP[];
extern char MSG_P4_DOWN[];
extern char MSG_P4_LEFT[];
extern char MSG_P4_RIGHT[];

extern char MSG_P4_B1[];
extern char MSG_P4_B2[];
extern char MSG_P4_B3[];
extern char MSG_P4_B4[];
extern char MSG_P4_B5[];
extern char MSG_P4_B6[];
extern char MSG_P4_B7[];
extern char MSG_P4_B8[];

extern char MSG_FLIPPER_1_L[];
extern char MSG_FLIPPER_1_R[];
extern char MSG_FLIPPER_2_L[];
extern char MSG_FLIPPER_2_R[];
extern char MSG_TILT_L[];
extern char MSG_TILT_R[];
extern char MSG_B1_L[];
extern char MSG_B1_R[];

extern char MSG_DSWA_BIT1[];
extern char MSG_DSWA_BIT2[];
extern char MSG_DSWA_BIT3[];
extern char MSG_DSWA_BIT4[];
extern char MSG_DSWA_BIT5[];
extern char MSG_DSWA_BIT6[];
extern char MSG_DSWA_BIT7[];
extern char MSG_DSWA_BIT8[];

extern char MSG_DSWB_BIT1[];
extern char MSG_DSWB_BIT2[];
extern char MSG_DSWB_BIT3[];
extern char MSG_DSWB_BIT4[];
extern char MSG_DSWB_BIT5[];
extern char MSG_DSWB_BIT6[];
extern char MSG_DSWB_BIT7[];
extern char MSG_DSWB_BIT8[];

extern char MSG_DSWC_BIT1[];
extern char MSG_DSWC_BIT2[];
extern char MSG_DSWC_BIT3[];
extern char MSG_DSWC_BIT4[];
extern char MSG_DSWC_BIT5[];
extern char MSG_DSWC_BIT6[];
extern char MSG_DSWC_BIT7[];
extern char MSG_DSWC_BIT8[];

extern char MSG_COIN_SLOTS[];

extern char MSG_1COIN_1PLAY[];
extern char MSG_1COIN_2PLAY[];
extern char MSG_1COIN_3PLAY[];
extern char MSG_1COIN_4PLAY[];
extern char MSG_1COIN_5PLAY[];
extern char MSG_1COIN_6PLAY[];
extern char MSG_1COIN_7PLAY[];
extern char MSG_1COIN_8PLAY[];
extern char MSG_1COIN_9PLAY[];

extern char MSG_2COIN_1PLAY[];
extern char MSG_2COIN_2PLAY[];
extern char MSG_2COIN_3PLAY[];
extern char MSG_2COIN_4PLAY[];
extern char MSG_2COIN_5PLAY[];
extern char MSG_2COIN_6PLAY[];
extern char MSG_2COIN_7PLAY[];
extern char MSG_2COIN_8PLAY[];

extern char MSG_3COIN_1PLAY[];
extern char MSG_3COIN_2PLAY[];
extern char MSG_3COIN_3PLAY[];
extern char MSG_3COIN_4PLAY[];
extern char MSG_3COIN_5PLAY[];
extern char MSG_3COIN_6PLAY[];
extern char MSG_3COIN_7PLAY[];
extern char MSG_3COIN_8PLAY[];

extern char MSG_4COIN_1PLAY[];
extern char MSG_4COIN_2PLAY[];
extern char MSG_4COIN_3PLAY[];
extern char MSG_4COIN_4PLAY[];
extern char MSG_4COIN_5PLAY[];
extern char MSG_4COIN_6PLAY[];
extern char MSG_4COIN_7PLAY[];
extern char MSG_4COIN_8PLAY[];

extern char MSG_5COIN_1PLAY[];
extern char MSG_5COIN_2PLAY[];
extern char MSG_5COIN_3PLAY[];
extern char MSG_5COIN_4PLAY[];

extern char MSG_6COIN_1PLAY[];
extern char MSG_6COIN_2PLAY[];
extern char MSG_6COIN_3PLAY[];
extern char MSG_6COIN_4PLAY[];

extern char MSG_7COIN_1PLAY[];
extern char MSG_7COIN_2PLAY[];
extern char MSG_7COIN_3PLAY[];
extern char MSG_7COIN_4PLAY[];

extern char MSG_8COIN_1PLAY[];
extern char MSG_8COIN_2PLAY[];
extern char MSG_8COIN_3PLAY[];
extern char MSG_8COIN_4PLAY[];

extern char MSG_9COIN_1PLAY[];

extern char MSG_OFF[];
extern char MSG_ON[];

extern char MSG_SCREEN[];
extern char MSG_NORMAL[];
extern char MSG_INVERT[];

extern char MSG_TEST_MODE[];

extern char MSG_DEMO_SOUND[];

extern char MSG_CONTINUE_PLAY[];
extern char MSG_EXTRA_LIFE[];
extern char MSG_LIVES[];

extern char MSG_CHEAT[];

extern char MSG_DIFFICULTY[];
extern char MSG_EASY[];
extern char MSG_HARD[];
extern char MSG_HARDEST[];

extern char MSG_CABINET[];
extern char MSG_UPRIGHT[];
extern char MSG_TABLE[];
