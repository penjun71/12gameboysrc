################################################################################
##                                                                            ##
##                                  RAINE                                     ##
##                                                                            ##
################################################################################

##
## Requirements:
##
## - cmp: djgpp (gcc 2.95.2)
## - cmp: nasm 0.98
## - lib: allegro 3.9.34 + Triple Buffering hacks
## - lib: zlib 1.1.3
##
## Changes:
## 
## $Log: makefile,v $
## Revision 1.20  2001/05/28 14:01:22  emanne
##
## new spr32x32_16 and spr32x32_32.s files
##
## Revision 1.19  2001/05/27 18:02:21  emanne
##
## new spr32x32_8.s file...
##
## Revision 1.18  2001/04/21 22:39:57  emanne
##
## patch for kicknrun
##
## Revision 1.17  2001/03/11 23:19:21  logiqx
## exedat should just be somewhere in PATH now
##
## Revision 1.16  2001/03/04 15:29:35  emanne
##
## spz16x16_16 & spz16x16_32
##
## Revision 1.15  2001/03/04 12:25:08  emanne
##
## newspr2_32.s
##
## Revision 1.14  2001/03/02 16:22:34  emanne
##
## remove useless libm for linux, and add asm version of newspr2
##
## Revision 1.13  2001/02/27 22:32:27  emanne
##
## spp8x8_16 and spp8x8_32
##
## Revision 1.12  2001/02/25 19:45:33  emanne
##
## linescroll with different bpps
##
## Revision 1.11  2001/02/24 10:31:35  emanne
##
## new line-scroll functinos, highly experimental, read comments !
##
## Revision 1.10  2001/02/14 23:05:11  antiriad
## cps1.c
##
## Revision 1.9  2001/02/11 17:34:21  antiriad
## fixed 'make compress'
##
## Revision 1.8  2001/02/11 15:34:36  emanne
##
## fix the error when building static target in linux
##
## Revision 1.7  2001/02/11 15:24:14  emanne
##
## no debug options...
##
## Revision 1.6  2001/02/11 09:19:37  emanne
##
## A better way to build dependancies, and replaced "Compiling Raine" by
## "Linking Raine".
##
## Revision 1.5  2001/02/07 22:43:24  logiqx
## Rmove redundant rule and fix clean message
##
## Revision 1.4  2001/02/07 18:28:29  emanne
##
## remove the stupid CR added by dos, and update to the latest requirements...
##
##
## 7 february 2001 : (Logiqx)
## Unified DOS and Linux makefiles
##
## 7 december 2000 :
## added dependencies (make dep)... very slow to build though.
## But dependencies do not need to be built everyday !!!
##
## 11 november 2000 :
## Unified with dos version !
## Applied all the patches by perl and not directly to avoid the executable
## bit.
## - s68000 is now generated and compiled by nasm, instead of being precompiled,
##   so there is no 900kb object in the source, but a full recompile is going to
##   take a bit longer.
## - mz80 is generated too. eagle is compiled in here now too.
## - m6502 is generated, although it's currently not in use. it was going to be
##   for the seta x1 games, but they looked really lame, plus i hate 6502 and
##   they used a variant with extra opcodes, so i've forgotten them for now.
## - added m68705 dir. also moved mcu.h from games dir and renamed it m68705.h.
##

# error logging
# CC =	redir -ea errorlog.txt gcc

# profiling
# CC =	gcc -pg

# DOS/Linux differences

ifdef DJDIR
   RAINE_EXE = Raine.exe

   ASM = @nasmw
   AFLAGS = -f coff

   ALDIR = $(DJDIR)/allegro

   DEFINE = -D__RAINE__ \
	   -DRAINE_DOS \
	   -DSIGNED_SAMPLES \
	   -DMEMORY_DEBUG \

   LIBS = -lz -lalleg
else
   RAINE_EXE = raine

   ASM = @nasm
   AFLAGS = -f elf

   ALDIR = $(DJDIR)/../allegro

   DEFINE = -D__RAINE__ \
	   -DRAINE_UNIX \
	   -DSIGNED_SAMPLES \
#	   -DMAME_SOUND \
#	   -DMEMORY_DEBUG \

   LIBS = -L/usr/X11R6/lib -lz -lalleg -lalleg_unsharable -lXxf86dga -lXxf86vm -lXext -lX11 -lesd -lvga # -lefence \

LIBS_DEBUG = -L/usr/X11R6/lib -lz -lalld -lalld_unsharable -lm -lXxf86dga -lXxf86vm -lXext -lX11 -lesd -lvga \

   LIBS_STATIC = -L/usr/X11R6/lib -lz /usr/local/lib/liballeg.a -lalleg_unsharable -lm -lXxf86dga -lXxf86vm -lXext -lX11 -lesd -lvga
endif

MD =    mkdir
RM =    @rm -f
CC =	@gcc

OBJDIR= object

OBJDIRS=$(OBJDIR) \
	$(OBJDIR)/gui \
	$(OBJDIR)/net \
	$(OBJDIR)/mame \
	$(OBJDIR)/sound \
	$(OBJDIR)/68000 \
	$(OBJDIR)/68020 \
	$(OBJDIR)/z80 \
	$(OBJDIR)/6502 \
	$(OBJDIR)/m68705 \
	$(OBJDIR)/video \
	$(OBJDIR)/video/c \
	$(OBJDIR)/video/i386 \
	$(OBJDIR)/games

INCDIR=	-I. \
	-Isource \
	-Isource/68000 \
	-Isource/68020 \
	-Isource/z80 \
	-Isource/6502 \
	-Isource/m68705 \
	-Isource/sound \
	-Isource/games \
	-Isource/video \
	-Isource/gui \
	-Isource/mame \

CFLAGS=	$(INCDIR) \
	$(DEFINE) \
	-mcpu=pentium \
	-march=pentium \
	-Wall \
	-O6 \
	-fomit-frame-pointer \
	-fschedule-insns2 \
	-frerun-cse-after-loop \
	-funroll-all-loops \
	-fexpensive-optimizations \
	-ffast-math \
	-finline-functions \
#	-pedantic \

SFLAGS=	$(INCDIR) \
	$(DEFINE) \
	-Wall \
	-mcpu=pentium \
	-march=pentium \
	-xassembler-with-cpp \

# ASM 68020 core

ASM020=	$(OBJDIR)/68020/newcpu.o \
	$(OBJDIR)/68020/readcpu.o \
	$(OBJDIR)/68020/cpustbl.o \
	$(OBJDIR)/68020/cpudefs.o \
	$(OBJDIR)/68020/a020core.o \

# STARSCREAM 68000 core

SC000=	$(OBJDIR)/68000/s68000.oa \
	$(OBJDIR)/68000/starhelp.o \

# MZ80 core

MZ80=	$(OBJDIR)/z80/mz80.oa \
	$(OBJDIR)/z80/mz80help.o \

# network core

NET=	$(OBJDIR)/net/d_system.o \
	$(OBJDIR)/net/d_net.o \
	$(OBJDIR)/net/d_netfil.o

# M6502 core

M6502=	$(OBJDIR)/6502/m6502.oa \
	$(OBJDIR)/6502/m6502hlp.o \

# M68705 core

M68705=	$(OBJDIR)/m68705/m68705.o \

# virtua i960 core

I960=	$(OBJDIR)/i960/i960cpu.o \

# Video core

VIDEO=	$(OBJDIR)/video/tilemod.o \
	$(OBJDIR)/video/palette.o \
	$(OBJDIR)/video/blitasm.o \
	$(OBJDIR)/video/newspr.o \
	$(OBJDIR)/video/spr64.o \
	$(OBJDIR)/video/i386/newspr2.o \
	$(OBJDIR)/video/i386/newspr2_16.o \
	$(OBJDIR)/video/i386/newspr2_32.o \
	$(OBJDIR)/video/i386/spp8x8.o \
	$(OBJDIR)/video/i386/spp8x8_16.o \
	$(OBJDIR)/video/i386/spp8x8_32.o \
	$(OBJDIR)/video/i386/spr16x8.o \
	$(OBJDIR)/video/i386/spr8x8_8.o \
	$(OBJDIR)/video/i386/spr8x8_16.o \
	$(OBJDIR)/video/i386/spr8x8_32.o \
	$(OBJDIR)/video/i386/str6x8_8.o \
	$(OBJDIR)/video/i386/str6x8_16.o \
	$(OBJDIR)/video/i386/str6x8_32.o \
	                                 \
	$(OBJDIR)/video/i386/spr16x16_8.o \
	$(OBJDIR)/video/i386/spr16x16_16.o \
	$(OBJDIR)/video/i386/spr16x16_32.o \
	                                   \
	$(OBJDIR)/video/i386/spr32x32_8.o \
	$(OBJDIR)/video/i386/spr32x32_16.o \
	$(OBJDIR)/video/i386/spr32x32_32.o \
	                                  \
	$(OBJDIR)/video/i386/blit_x2_8.o \
	$(OBJDIR)/video/i386/blit_x2_16.o \
	$(OBJDIR)/video/i386/blit_x2_24.o \
	$(OBJDIR)/video/i386/blit_x2_32.o \
	$(OBJDIR)/video/spz16x16.o \
	$(OBJDIR)/video/spz16x16_16.o \
	$(OBJDIR)/video/spz16x16_32.o \
	$(OBJDIR)/video/spz16x8.o \
	$(OBJDIR)/video/arcmon.o \
	$(OBJDIR)/video/arcmode.o \
	$(OBJDIR)/video/eagle.oa \
	$(OBJDIR)/video/c/lspr16_8.o \
	$(OBJDIR)/video/c/lspr16_16.o \
	$(OBJDIR)/video/c/lspr16_32.o \

# Sound core

SOUND=	$(OBJDIR)/sound/2610intf.o \
	$(OBJDIR)/sound/2151intf.o \
	$(OBJDIR)/sound/adpcm.o \
	$(OBJDIR)/sound/ymdeltat.o \
	$(OBJDIR)/sound/2203intf.o \
	$(OBJDIR)/sound/3812intf.o \
	$(OBJDIR)/sound/fm.o \
	$(OBJDIR)/sound/ym2151.o \
	$(OBJDIR)/sound/ay8910.o \
	$(OBJDIR)/sound/fmopl.o \
	$(OBJDIR)/sound/ymz280b.o \
	$(OBJDIR)/sound/m6585.o \
	$(OBJDIR)/sound/msm5205.o \
	$(OBJDIR)/sound/smp16bit.o \
	$(OBJDIR)/sound/es5506.o \
	$(OBJDIR)/sound/sasound.o \

# Release Games

GAMES=	$(OBJDIR)/games/arabianm.o \
	$(OBJDIR)/games/arkretrn.o \
	$(OBJDIR)/games/armedf.o \
	$(OBJDIR)/games/ashura.o \
	$(OBJDIR)/games/asuka.o \
	$(OBJDIR)/games/biomtoy.o \
	$(OBJDIR)/games/bloodbro.o \
	$(OBJDIR)/games/bonzeadv.o \
	$(OBJDIR)/games/bubblem.o \
	$(OBJDIR)/games/bublbobl.o $(OBJDIR)/games/bubl_mcu.o \
	$(OBJDIR)/games/bubsymph.o \
	$(OBJDIR)/games/cabal.o \
	$(OBJDIR)/games/cadash.o \
	$(OBJDIR)/games/chasehq.o \
	$(OBJDIR)/games/ctribebl.o \
	$(OBJDIR)/games/cameltry.o \
	$(OBJDIR)/games/cleofort.o \
	$(OBJDIR)/games/crimec.o \
	$(OBJDIR)/games/darius.o \
	$(OBJDIR)/games/darius2d.o \
	$(OBJDIR)/games/dariusg.o \
	$(OBJDIR)/games/dinorex.o \
	$(OBJDIR)/games/dondokod.o \
	$(OBJDIR)/games/doubled3.o \
	$(OBJDIR)/games/driftout.o \
	$(OBJDIR)/games/eaction2.o \
	$(OBJDIR)/games/earthjkr.o \
	$(OBJDIR)/games/fchamp.o \
	$(OBJDIR)/games/exzisus.o \
	$(OBJDIR)/games/finalb.o \
	$(OBJDIR)/games/galpanic.o \
	$(OBJDIR)/games/gcpball.o \
	$(OBJDIR)/games/gekridan.o \
	$(OBJDIR)/games/growl.o \
	$(OBJDIR)/games/gunbird.o \
	$(OBJDIR)/games/hitice.o \
	$(OBJDIR)/games/kaiserkn.o \
	$(OBJDIR)/games/liquidk.o \
	$(OBJDIR)/games/masterw.o \
	$(OBJDIR)/games/megab.o \
	$(OBJDIR)/games/ninjak.o \
	$(OBJDIR)/games/ninjaw.o \
	$(OBJDIR)/games/othunder.o \
	$(OBJDIR)/games/opwolf.o \
	$(OBJDIR)/games/pbobble2.o \
	$(OBJDIR)/games/pbobble3.o \
	$(OBJDIR)/games/popnpop.o \
	$(OBJDIR)/games/puchicar.o \
	$(OBJDIR)/games/pulirula.o \
	$(OBJDIR)/games/rainbow.o \
	$(OBJDIR)/games/rambo3.o \
	$(OBJDIR)/games/rastan.o \
	$(OBJDIR)/games/rastan2.o \
	$(OBJDIR)/games/robokid.o \
	$(OBJDIR)/games/silentd.o \
	$(OBJDIR)/games/silkworm.o \
	$(OBJDIR)/games/snowbros.o \
	$(OBJDIR)/games/solfigtr.o \
	$(OBJDIR)/games/spacegun.o \
	$(OBJDIR)/games/spcinv95.o \
	$(OBJDIR)/games/ssi.o \
	$(OBJDIR)/games/syvalion.o \
	$(OBJDIR)/games/terrac.o \
	$(OBJDIR)/games/terraf.o \
	$(OBJDIR)/games/tetrist.o \
	$(OBJDIR)/games/thundfox.o \
	$(OBJDIR)/games/toki.o \
	$(OBJDIR)/games/twinqix.o \
	$(OBJDIR)/games/viofight.o \
	$(OBJDIR)/games/warriorb.o \
	$(OBJDIR)/games/wrestlef.o \
	$(OBJDIR)/games/wwfsstar.o \
	$(OBJDIR)/games/zerozone.o \
	$(OBJDIR)/games/ksystem.o $(OBJDIR)/games/kiki_mcu.o $(OBJDIR)/games/kick_mcu.o \
	$(OBJDIR)/games/gunfront.o \
	$(OBJDIR)/games/gunlock.o \
	$(OBJDIR)/games/gridseek.o \
	$(OBJDIR)/games/cps1.o \
	$(OBJDIR)/games/cps1drv.o \

# System drivers

SYSDRV=	$(OBJDIR)/games/games.o \
	$(OBJDIR)/games/f3system.o \
	$(OBJDIR)/games/konamigx.o \
	$(OBJDIR)/games/lsystem.o \
	$(OBJDIR)/games/megasys1.o \
	$(OBJDIR)/games/nichisnd.o \
	$(OBJDIR)/games/quizgame.o \
	$(OBJDIR)/games/nmk.o \
	$(OBJDIR)/games/taitosnd.o \
	$(OBJDIR)/games/tchnosnd.o \
	$(OBJDIR)/games/tecmosys.o \
	$(OBJDIR)/games/bsystem.o \
	$(OBJDIR)/games/xsystem1.o \
	$(OBJDIR)/games/xsystem2.o \
	$(OBJDIR)/games/toaplan1.o \
	$(OBJDIR)/games/toaplan2.o \
	$(OBJDIR)/games/upl.o \
	$(OBJDIR)/games/setax1.o \
	$(OBJDIR)/games/tc002obj.o \
	$(OBJDIR)/games/tc003vcu.o \
	$(OBJDIR)/games/tc004vcu.o \
	$(OBJDIR)/games/tc005rot.o \
	$(OBJDIR)/games/tc006vcu.o \
	$(OBJDIR)/games/tc100scn.o \
	$(OBJDIR)/games/tc101scn.o \
	$(OBJDIR)/games/tc110pcr.o \
	$(OBJDIR)/games/tc140syt.o \
	$(OBJDIR)/games/tc150rod.o \
	$(OBJDIR)/games/tc180vcu.o \
	$(OBJDIR)/games/tc200obj.o \
	$(OBJDIR)/games/tc220ioc.o \
	$(OBJDIR)/games/default.o \

# Beta only games

BGAMES=	$(OBJDIR)/games/chasehq2.o \
	$(OBJDIR)/games/koshien.o \
	$(OBJDIR)/games/lightbr.o \
	$(OBJDIR)/games/cupfinal.o \
	$(OBJDIR)/games/intcup94.o \
	$(OBJDIR)/games/opwolf3.o \
	$(OBJDIR)/games/trstars.o \
	$(OBJDIR)/games/topland.o \
	$(OBJDIR)/games/topspeed.o \
	$(OBJDIR)/games/metalb.o \
	$(OBJDIR)/games/ainferno.o \
	$(OBJDIR)/games/aquajack.o \
	$(OBJDIR)/games/bshark.o \
	$(OBJDIR)/games/chaknpop.o \
	$(OBJDIR)/games/contcirc.o \
	$(OBJDIR)/games/dleague.o \
	$(OBJDIR)/games/recordbr.o \
	$(OBJDIR)/games/ringrage.o \
	$(OBJDIR)/games/undrfire.o \
	$(OBJDIR)/games/ridefght.o \
	$(OBJDIR)/games/deadconx.o \
	$(OBJDIR)/games/pbobble4.o \
	$(OBJDIR)/games/volfied.o \
	$(OBJDIR)/games/megasy32.o \
	$(OBJDIR)/games/ktiger2.o \
	$(OBJDIR)/games/f3demo.o \
	$(OBJDIR)/games/flstory.o \
	$(OBJDIR)/games/wgp.o \
	$(OBJDIR)/games/superchs.o \
	$(OBJDIR)/games/hthero95.o \
	$(OBJDIR)/games/heavyunt.o \
	$(OBJDIR)/games/landmakr.o \
	$(OBJDIR)/games/dangarb.o \
	$(OBJDIR)/games/mjnquest.o \
	$(OBJDIR)/games/cave.o \
	$(OBJDIR)/games/psyforce.o \
#	$(OBJDIR)/games/seta.o \

# Interface

GUI=	$(OBJDIR)/gui/gui.o \
	$(OBJDIR)/gui/rgui.o \
	$(OBJDIR)/gui/rguiproc.o \
	$(OBJDIR)/gui/rfsel.o \
	$(OBJDIR)/gui/dlg_about.o \
	$(OBJDIR)/gui/dlg_cheat.o \
	$(OBJDIR)/gui/dlg_dsw.o \
	$(OBJDIR)/gui/dlg_sound.o \
	$(OBJDIR)/gui/dlg_key_game.o \
	$(OBJDIR)/gui/dlg_joy_game.o \

# Core

CORE=	$(OBJDIR)/raine.o \
	$(OBJDIR)/hiscore.o \
	$(OBJDIR)/ingame.o \
	$(OBJDIR)/savegame.o \
	$(OBJDIR)/arpro.o \
	$(OBJDIR)/decode.o \
	$(OBJDIR)/control.o \
	$(OBJDIR)/debug.o \
	$(OBJDIR)/config.o \
	$(OBJDIR)/confile.o \
	$(OBJDIR)/unzip.o \
	$(OBJDIR)/files.o \
	$(OBJDIR)/newmem.o \
	$(OBJDIR)/dsw.o \
	$(OBJDIR)/cpumain.o \
	$(OBJDIR)/emumain.o \
	$(OBJDIR)/cat93c46.o \
	$(OBJDIR)/loadroms.o \

# Mame Support

MAME=	$(OBJDIR)/mame/memory.o \
	$(OBJDIR)/mame/eeprom.o \

OBJS=	$(SC000) \
	$(ASM020) \
	$(MZ80) \
	$(M68705) \
	$(GAMES) \
	$(BGAMES) \
	$(SYSDRV) \
	$(VIDEO) \
	$(SOUND) \
	$(GUI) \
	$(CORE) \
	$(MAME) \
#	$(NET) \
#	$(M6502) \

all:	maketree raine

raine:	$(OBJS)
	@echo Linking Raine...
	$(CC) -o $(RAINE_EXE) $(OBJS) $(LIBS)

rained:	$(OBJS)
	@echo Linking Raine...
	$(CC) -o $(RAINE_EXE) $(OBJS) $(LIBS_DEBUG)

static:	$(OBJS)
	@echo Linking Raine static...
	$(CC) -s -o $(RAINE_EXE) $(OBJS) $(LIBS_STATIC)

ASM020: $(ASM020)

VIDEO: $(VIDEO)

ifdef DJDIR
   grabber: raine.dat
	   @echo Editing datafile...
	   $(ALDIR)/tools/grabber raine.dat

   compress: $(RAINE_EXE)
	   @echo Appending datafile...
	   exedat -a -c $(RAINE_EXE) raine.dat
	   upx -9 $(RAINE_EXE)
else
   grabber: dejap

   dejap: raine.dat
	   @echo Editing datafile...
	   $(ALDIR)/tools/grabber raine.dat

   compress: pack

   pack: $(RAINE_EXE)
	   @echo Appending datafile...
	   $(ALDIR)/tools/exedat -a -c -007 sardu $(RAINE_EXE) raine.dat
endif

# compile object from standard c

$(OBJDIR)/%.o: source/%.c
	@echo Compiling $<...
	$(CC) $(CFLAGS) -c $< -o $@

# compile object from at&t asm

$(OBJDIR)/%.o: source/%.s
	@echo Assembling $<...
	$(CC) --save-temps $(SFLAGS) -c $< -o $@

# compile at&t asm from standard c

$(OBJDIR)/%.s: source/%.c
	@echo Compiling $<...
	$(CC) $(CFLAGS) -S -c $< -o $@

# compile object from intel asm

ifndef DJDIR
   $(OBJDIR)/video/eagle.oa: source/video/eagle.asm
	@echo Patching $<...
	perl source/video/patch_eagle $< >$(OBJDIR)/video/eagle.asm
	@echo Assembling $<...
	$(ASM) -o $@ $(AFLAGS) $(OBJDIR)/video/eagle.asm
endif

$(OBJDIR)/%.oa: source/%.asm
	@echo Assembling $<...
	$(ASM) -o $@ $(AFLAGS) $<

# generate s68000.asm

$(OBJDIR)/68000/s68000.oa: $(OBJDIR)/68000/s68000.asm
	@echo Assembling $<...
	$(ASM) -o $@ $(AFLAGS) $<

$(OBJDIR)/68000/s68000.asm: $(OBJDIR)/68000/star.o
	$(CC) -s -o $(OBJDIR)/68000/star.exe $(OBJDIR)/68000/star.o
	@$(OBJDIR)/68000/star.exe -hog $@
ifndef DJDIR
	@echo Patching $@...
	perl source/68000/patch_68000 $(OBJDIR)/68000/s68000.asm > $(OBJDIR)/s2.asm
	mv -f $(OBJDIR)/s2.asm $(OBJDIR)/68000/s68000.asm
endif

# generate mz80.asm

$(OBJDIR)/z80/mz80.oa: $(OBJDIR)/z80/mz80.asm
	@echo Assembling $<...
	$(ASM) -o $@ $(AFLAGS) $<

$(OBJDIR)/z80/mz80.asm: $(OBJDIR)/z80/makez80.o
	$(CC) -s -o $(OBJDIR)/z80/makez80.exe $(OBJDIR)/z80/makez80.o
ifdef DJDIR
	@$(OBJDIR)/z80/makez80.exe -s -cs -x86 $@
else
	$(OBJDIR)/z80/makez80.exe -l -s -cs -x86 $@
	perl source/z80/patch_z80 $(OBJDIR)/z80/mz80.asm > $(OBJDIR)/z80.asm
	mv -f $(OBJDIR)/z80.asm $(OBJDIR)/z80/mz80.asm
endif

# generate m6502.asm

$(OBJDIR)/6502/m6502.oa: $(OBJDIR)/6502/m6502.asm
	@echo Assembling $<...
	$(ASM) -o $@ $(AFLAGS) $<

$(OBJDIR)/6502/m6502.asm: $(OBJDIR)/6502/make6502.o
	@echo Building M6502 $(OBJDIR)...
	$(CC) -s -o $(OBJDIR)/6502/make6502.exe $(OBJDIR)/6502/make6502.o
	$(OBJDIR)/6502/make6502.exe -s -6510 $@

# Notice : the following fix is specific to the frame pointer optimisation
# of gcc 2.81 and higher (< 3.00)

# kiki kai kai gcc bug

$(OBJDIR)/games/kiki_mcu.o: source/games/kiki_mcu.c
	@echo Compiling $<...
	$(CC) $(INCDIR) $(DEFINE) -mpentium -march=pentium -O6 -c $< -o $@

# Same for kick and run...

object/games/kick_mcu.o: source/games/kick_mcu.c
	@echo Compiling $<...
	$(CC) $(INCDIR) $(DEFINE) -mpentium -march=pentium -O6 -c $< -o $@

# dependencies

ifndef DJDIR

dep:
	./makedep $(OBJS) > make.dep
endif

include make.dep

# create directories

dirs:
	@echo make dirs is no longer necessary, just type make

$(sort $(OBJDIRS)):
	$(MD) $@

maketree: $(sort $(OBJDIRS))

# remove all object files

clean:
	@echo Deleting object tree...
	$(RM) -r $(OBJDIR)
	@echo Deleting $(RAINE_EXE)...
	$(RM) $(RAINE_EXE)

vclean:
	@echo make vclean is no longer necessary, just type make clean
