/**************************************
***      ADPCM decode routines      ***
***           Header File           ***
**************************************/

#ifndef AdpcmH
#define AdpcmH

typedef struct {
	int	stream;
	char	*start;
	char	*end;
	char	*ptr;
	int	shift;
	int	flag;
	int	signal;
	int	delta;
	int	ch_val;
	int	vol;
	int	pan;
	} ADPCM_CH;

extern int		adpcm_status;
extern int		adpcm_statusmask;
extern ADPCM_CH	adpcm_ch[7];

extern void		adpcm_init(void);
extern int		adpcm_ch_init(ADPCM_CH *);
extern void		adpcm_ch_play(ADPCM_CH *, char *, int, int, int, int);
extern void		adpcm_ch_pause(ADPCM_CH *);
extern void		adpcm_ch_resume(ADPCM_CH *);
extern void		adpcm_ch_destroy(ADPCM_CH *);
extern void		adpcm_ch_decodea(ADPCM_CH *);
extern void		adpcm_ch_decodeb(ADPCM_CH *);
extern void		adpcma_write(int Register, int Value);
extern void		adpcmb_write(int Register, int Value);
extern void		sound_mute(void);

#endif
