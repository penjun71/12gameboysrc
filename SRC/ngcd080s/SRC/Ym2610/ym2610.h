/**************************************
***    YM2610 emulation routines    ***
***           Header File           ***
**************************************/

#ifndef YM2610H
#define YM2610H

extern int	sound_init(void);
extern void	sound_shutdown(void);
extern char	audio_error[128];
extern int	audio_freq;
extern int	address_register0;
extern int	address_register1;
extern int	port0state;
extern int	port1state;
extern int	port0shift;
extern int	port1shift;
extern int	audio_freq;
extern int	sound_vol;
extern int	sound_device;

extern int	YM2610Init(void);
extern void	YM2610Shutdown(void);
extern void	YM2610Write(int Register, int Value);
extern int	YM2610Read(int Register);

#endif
