#include <stdio.h>
#include <allegro.h>
#include "../config/config.h"
#include "../gui/gui.h"
#include "../input/macro.h"

extern int neogeo_ipl_done;

int	cheats_enable = 0;

typedef struct {
	char	cheat_name[32];
	int		cheat_code;
	int		cheat_type;
	int		cheat_enable;
} CHEAT_ENTRY;

CHEAT_ENTRY	cheat_list[16];

DIALOG cheat_dialog[] =
{
   /* (dialog proc)   (x)  (y)  (w)  (h)    (fg)    (bg)  (key)  (flags)   (d1)  (d2)            (dp) */
 {d_neo_border_proc,   0,   0, 239, 239, 0x0000, 0x0000,      0,      0,     0,    0,                NULL},
 {d_neo_fbox_proc,     2,   2, 235,   8, 0x0000, 0x10E3,      0,      0,     0,    0,                NULL},
 {d_text_proc,        10,   3,   0,   0, 0xFFFF, 0x10E3,      0,      0,     0,    0,  "Cheats Selection"},
 {d_text_proc,        10,  13,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,       "Game Name :"},
 {d_text_proc,		  80,  13,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,                  ""},
 
 {d_neo_gbox_proc,    10,  26, 220, 189, 0xC71C, 0x328A,      0,      0,     0,    0,  "Available cheats"},
 {d_neo_check_proc,   15,  32,  10,  10, 0xFFFF, 0x9596,    '1',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15,  44,  10,  10, 0xFFFF, 0x9596,    '2',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15,  56,  10,  10, 0xFFFF, 0x9596,    '3',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15,  68,  10,  10, 0xFFFF, 0x9596,    '4',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15,  80,  10,  10, 0xFFFF, 0x9596,    '5',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15,  92,  10,  10, 0xFFFF, 0x9596,    '6',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 104,  10,  10, 0xFFFF, 0x9596,    '7',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 116,  10,  10, 0xFFFF, 0x9596,    '8',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 128,  10,  10, 0xFFFF, 0x9596,    '9',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 140,  10,  10, 0xFFFF, 0x9596,    'a',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 152,  10,  10, 0xFFFF, 0x9596,    'b',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 164,  10,  10, 0xFFFF, 0x9596,    'c',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 176,  10,  10, 0xFFFF, 0x9596,    'd',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 188,  10,  10, 0xFFFF, 0x9596,    'e',      0,     0,    0,                  ""},
 {d_neo_check_proc,   15, 200,  10,  10, 0xFFFF, 0x9596,    'f',      0,     0,    0,                  ""},

 {d_neo_button_proc,  87, 219, 65,  15, 0x0000, 0x0000,     'o', D_EXIT,     0,    0,               "&OK"},

 {NULL,                0,   0,   0,   0, 0x0000, 0x0000,      0,      0,     0,    0,                NULL}
};

void cheat_select(void)
{
	int			ret, i, code;
	MACROFILE	cfg;
	
	if (!neogeo_ipl_done)
	{
		neo_alert1("Error", NULL, "This function is useless without a game loaded.", NULL, "&Abort", 'a');
		return;
	}
	macro_fopen(&cfg, "CHEATS.CFG");
	
	if ((!macro_find_section(&cfg, config_game_name))||(!macro_find_subsection(&cfg, "codes")))
	{
		macro_fclose(&cfg);
		neo_alert1(config_game_name, NULL, "No entry for this game in CHEATS.CFG.", NULL, "&Abort", 'a');
		return;
	}
	
	cheat_dialog[4].dp = config_game_name;
	
	i = 0;
	
	while(macro_enum_variables(&cfg))
	{
		cheat_split((char *)cfg.macro_variable, cheat_list[i].cheat_name, &cheat_list[i].cheat_code, &cheat_list[i].cheat_type);
		i++;
	}
	
	for(i=0;i<15;i++)
	{
		cheat_dialog[i + 6].flags = 0;

		if (cheat_list[i].cheat_code == 0)
			cheat_dialog[i + 6].flags = D_HIDDEN;
			
		if (cheat_list[i].cheat_enable)
			cheat_dialog[i + 6].flags = D_SELECTED;
			
		cheat_dialog[i + 6].dp = cheat_list[i].cheat_name;
	}
	
	centre_dialog(cheat_dialog);

	ret = do_dialog(cheat_dialog, 0);

	for(i=0;i<15;i++)
	{
		if (cheat_dialog[i + 6].flags&D_SELECTED)
		{
			cheat_list[i].cheat_enable = 1;
			cheats_enable = 1;
		}
		else
			cheat_list[i].cheat_enable = 0;
	}

	macro_fclose(&cfg);
}
