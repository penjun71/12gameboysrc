
#ifndef CheatsH
#define CheatsH

extern int	cheats_enable;

typedef struct {
	char	cheat_name[32];
	int		cheat_code;
	int		cheat_type;
	int		cheat_enable;
} CHEAT_ENTRY;

extern CHEAT_ENTRY	cheat_list[16];

extern void cheat_select(void);

#endif
