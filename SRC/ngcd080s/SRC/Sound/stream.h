/*******************************
*** Sound Streaming Routines ***
***       Header  File       ***
*******************************/

#ifndef STREAM_H
#define STREAM_H

extern void	stream_init(void);
extern int	stream_create(int);		 /* Nominal freq, returns handle (-1 = error) */
extern void	stream_destroy(int);		 /* Handle */
extern void	stream_adjust(int, int, int);	 /* Handle, volume, pan  */
extern int	stream_get_buffer_size(int);	 /* Handle */
extern void	stream_fill_buffer(int, char *); /* Handle, buffer to copy */
extern void	stream_kill_all(void);
extern void	stream_pause(int);
extern void	stream_resume(int);

#endif
