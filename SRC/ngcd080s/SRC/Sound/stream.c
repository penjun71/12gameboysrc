/*******************************
*** Sound Streaming Routines ***
*******************************/

//-- Include Files ----------------------------------------------------------
#include <mem.h>
#include <time.h>
#include <audio.h>
#include "../ym2610/ym2610.h"

//-- Defines ----------------------------------------------------------------
#define	MAX_VOICES	7
#define NUM_BUFFERS	3

//-- Structures -------------------------------------------------------------
#include <stdio.h>
typedef struct {
	HAC		voice;
	LPAUDIOWAVE	wave;
	int		buffer_size;
	int		current_buffer;
	int		in_use;
	} STREAM;

static	STREAM		audio_stream[MAX_VOICES];

//-- Exported Fuctions ------------------------------------------------------
void	stream_init(void);
int		stream_create(int);					/* Nominal freq, returns handle (-1 = error) */
void	stream_destroy(int);				/* Handle */
void	stream_adjust(int, int, int);		/* Handle, volume, pan  */
int		stream_get_buffer_size(int);		/* Handle */
void	stream_fill_buffer(int, char *);	/* Handle, buffer to copy */
void	stream_kill_all(void);
void	stream_pause(int);
void	stream_resume(int);

//---------------------------------------------------------------------------
void	stream_init(void)
{
	int	i;
	
	for(i=0;i<MAX_VOICES;i++) {
		audio_stream[i].current_buffer = 1;
		audio_stream[i].in_use = 0;	
	}	
}

//---------------------------------------------------------------------------
int	stream_create(int sound_freq)
{
	int	i;
	
	for(i=0;i<MAX_VOICES;i++) {
		if (audio_stream[i].in_use == 0) {
			audio_stream[i].buffer_size = sound_freq / 60;
			if ((audio_stream[i].wave = (LPAUDIOWAVE)malloc(sizeof(AUDIOWAVE))) != NULL) {
				audio_stream[i].wave->wFormat = AUDIO_FORMAT_16BITS | AUDIO_FORMAT_MONO | AUDIO_FORMAT_LOOP;
				audio_stream[i].wave->nSampleRate = sound_freq;
				audio_stream[i].wave->dwLength = audio_stream[i].buffer_size*NUM_BUFFERS*2;
				audio_stream[i].wave->dwLoopEnd = audio_stream[i].wave->dwLength;
				audio_stream[i].wave->dwLoopStart = 0;

				if (ACreateAudioData(audio_stream[i].wave) != AUDIO_ERROR_NONE) {
					free(audio_stream[i].wave);
				} else {
					memset(audio_stream[i].wave->lpData, 0x00, audio_stream[i].wave->dwLength);
					AWriteAudioData(audio_stream[i].wave, 0L, audio_stream[i].wave->dwLength);
					if (ACreateAudioVoice(&audio_stream[i].voice) != AUDIO_ERROR_NONE) {
						ADestroyAudioData(audio_stream[i].wave);
						free(audio_stream[i].wave);
					} else {
						ASetVoiceFrequency(audio_stream[i].voice, audio_freq);
						ASetVoiceVolume(audio_stream[i].voice, 255);
						ASetVoicePanning(audio_stream[i].voice, 128);
						audio_stream[i].current_buffer = 1;
						audio_stream[i].in_use = 1;
						APlayVoice(audio_stream[i].voice, audio_stream[i].wave);
						return i;
					}
				}
			}
		}
	}
	return -1;
}

//---------------------------------------------------------------------------
void	stream_destroy(int i)
{
	if (audio_stream[i].in_use != 0) {
		AStopVoice(audio_stream[i].voice);
		ADestroyAudioVoice(audio_stream[i].voice);
		ADestroyAudioData(audio_stream[i].wave);
		free(audio_stream[i].wave);
		audio_stream[i].in_use = 0;
	}
}

//---------------------------------------------------------------------------
void	stream_adjust(int i, int vol, int pan)
{
	ASetVoiceVolume(audio_stream[i].voice, vol);
	ASetVoicePanning(audio_stream[i].voice, pan);
}

//---------------------------------------------------------------------------
int	stream_get_buffer_size(int i)
{
	return audio_stream[i].buffer_size;
}

//---------------------------------------------------------------------------
void	stream_fill_buffer(int i, char *Buffer)
{
	int	pos, left, right;
	uclock_t a, b;
	
	left = audio_stream[i].current_buffer * audio_stream[i].buffer_size;
	right = left + audio_stream[i].buffer_size;

	do {
		AUpdateAudioEx(audio_freq/60);
		AGetVoicePosition(audio_stream[i].voice, (LPLONG)&pos);
	} while((pos>=left)&&(pos<=right));

	memcpy(((char *)(audio_stream[i].wave->lpData))+(left*2), Buffer, audio_stream[i].buffer_size*2);
	AWriteAudioData(audio_stream[i].wave, left*2, audio_stream[i].buffer_size*2);
	audio_stream[i].current_buffer = (audio_stream[i].current_buffer + 1)%NUM_BUFFERS;
}

//---------------------------------------------------------------------------
void	stream_kill_all(void)
{
	int	i;
	
	for(i=0;i<MAX_VOICES;i++) {
		if (audio_stream[i].in_use != 0) {
			AStopVoice(audio_stream[i].voice);
			ADestroyAudioVoice(audio_stream[i].voice);
			ADestroyAudioData(audio_stream[i].wave);
			free(audio_stream[i].wave);
			audio_stream[i].in_use = 0;
		}
	}
}

//---------------------------------------------------------------------------
void	stream_pause(int i)
{
	AStopVoice(audio_stream[i].voice);
}

//---------------------------------------------------------------------------
void	stream_resume(int i)
{
	AStartVoice(audio_stream[i].voice);
}
