/**************************************
****    MACRO.H  -  Macro Keys     ****
****         Header File           ****
**************************************/

#ifndef	MacroH
#define MacroH

//-- Structures --------------------------------------------------------------
typedef struct {
	unsigned int	macro_section_pos;
	unsigned char	macro_section_name[80];
	unsigned int	macro_subsection_pos;
	unsigned char	macro_subsection_name[80];
	unsigned int	macro_variable_pos;
	unsigned char	macro_variable[80];
	FILE		*file_pointer;
	} MACROFILE;

typedef struct {
	unsigned char	name[80];
	unsigned char	macro[80];
} MACRO;

//-- Global Variables --------------------------------------------------------
extern MACRO	macro_list_p1[16];
extern MACRO	macro_list_p2[16];

//-- Exported Functions ------------------------------------------------------
extern void	strtrim(unsigned char *, unsigned char *);
extern void	macro_init(void);
extern int	macro_fopen(MACROFILE *, const char *);
extern int	macro_fclose(MACROFILE *);
extern int	macro_enum_sections(MACROFILE *);
extern int	macro_enum_subsections(MACROFILE *);
extern int	macro_enum_variables(MACROFILE *);
extern void	macro_invalidate_section(MACROFILE *);
extern void	macro_invalidate_subsection(MACROFILE *);
extern void	macro_invalidate_variable(MACROFILE *);
extern int	macro_split(char *, char *, int *, char *);
extern int	cheat_split(char *, char *, int *, int *);
extern int	macro_find_section(MACROFILE *, unsigned char *);
extern int	macro_find_subsection(MACROFILE *, unsigned char *);
extern int	macro_load(MACROFILE *, unsigned char *, MACRO *);
extern void	macro_avail(void);
extern void	macro_print_list(MACROFILE *);

#endif

