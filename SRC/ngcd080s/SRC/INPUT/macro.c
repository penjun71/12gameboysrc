/**************************************
****    MACRO.C  -  Macro Keys     ****
**************************************/

//-- Include Files -----------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "input.h"

//-- Structures --------------------------------------------------------------
typedef struct {
	unsigned int	macro_section_pos;
	unsigned char	macro_section_name[80];
	unsigned int	macro_subsection_pos;
	unsigned char	macro_subsection_name[80];
	unsigned int	macro_variable_pos;
	unsigned char	macro_variable[80];
	FILE		*file_pointer;
} MACROFILE;

typedef struct {
	unsigned char	name[80];
	unsigned char	macro[80];
} MACRO;

//-- Global Variables --------------------------------------------------------
MACRO	macro_list_p1[16];
MACRO	macro_list_p2[16];

//-- Exported Functions ------------------------------------------------------
void	strtrim(unsigned char *, unsigned char *);
void	macro_init(void);
int		macro_fopen(MACROFILE *, const char *);
int		macro_fclose(MACROFILE *);
int		macro_enum_sections(MACROFILE *);
int		macro_enum_subsections(MACROFILE *);
int		macro_enum_variables(MACROFILE *);
void	macro_invalidate_section(MACROFILE *);
void	macro_invalidate_subsection(MACROFILE *);
void	macro_invalidate_variable(MACROFILE *);
int		macro_split(char *, char *, char *);
int		macro_find_section(MACROFILE *, unsigned char *);
int		macro_find_subsection(MACROFILE *, unsigned char *);
int		macro_load(MACROFILE *, unsigned char *, MACRO *);
int		cheat_split(char *, char *, int *, int *);
void	sprintm(unsigned char *, unsigned char *);
int		sgetm(unsigned char *, unsigned char *);

//----------------------------------------------------------------------------
void	strtrim(unsigned char *s1, unsigned char *s2)
{
	unsigned int	i, j, k;
	
	i = 0;
	while( (isspace(s2[i]))&&(s2[i] != 0) )
		i++;
	
	j = 0;
	for(k=0;k<strlen((char *)s2);k++)
	{
		if (!isspace(s2[k]))
			j = k;
	}
	
	strncpy((char *)s1, (char *)&s2[i], j - i + 1);
	s1[j - i + 1] = 0;
}		

//----------------------------------------------------------------------------
void	macro_init(void)
{
	int	i;
	
	for(i=0;i<16;i++)
	{
		macro_list_p1[i].macro[0] = 0;
		macro_list_p2[i].macro[0] = 0;
	}
}
//----------------------------------------------------------------------------
int	macro_fopen(MACROFILE *file, const char *path)
{
	file->file_pointer = fopen(path, "rt");
	file->macro_section_pos = -1;
	file->macro_subsection_pos = -1;
	file->macro_variable_pos = -1;
	
	return (file->file_pointer != NULL);
}

//----------------------------------------------------------------------------
int	macro_fclose(MACROFILE *file)
{
	if (fclose(file->file_pointer) == 0)
	{
		file->file_pointer = NULL;
		file->macro_section_pos = -1;
		file->macro_subsection_pos = -1;
		file->macro_variable_pos = -1;
	
		return	1;
	}
	
	return 0;
}

//----------------------------------------------------------------------------
int	macro_enum_sections(MACROFILE *file)
{
	char	line[80];
	int	i;
	
	if (file->macro_section_pos != -1)
		fseek(file->file_pointer, file->macro_section_pos, SEEK_SET);
	else
		rewind(file->file_pointer);
	
	do {
		if (fgets(line, 80, file->file_pointer) == NULL) {
			file->macro_section_pos = -1;		
			return 0;			// EOF, no more sections
		}
		
		strtrim((unsigned char *)line, (unsigned char *)line);

		if (line[0]=='[')
			for(i=0;i<80;i++)
				if (line[i]==']') {
					file->macro_section_pos = ftell(file->file_pointer);
					memset(file->macro_section_name, 0, 80);
					strncpy((char *)file->macro_section_name, line+1, i-1);
					strtrim(file->macro_section_name, 
						file->macro_section_name);
					return 1;	// Success
				}
	} while(!feof(file->file_pointer));

	file->macro_section_pos = -1;
	return 0;
}

//----------------------------------------------------------------------------
int	macro_enum_subsections(MACROFILE *file)
{
	char	line[80];
	int	i;
	
	if (file->macro_section_pos == -1) {
		file->macro_subsection_pos = -1;
		return 0;
	}
	
	if (file->macro_subsection_pos == -1)
		fseek(file->file_pointer, file->macro_section_pos, SEEK_SET);
	else
		fseek(file->file_pointer, file->macro_subsection_pos, SEEK_SET);

	do {
		if (fgets(line, 80, file->file_pointer) == NULL) {
			file->macro_subsection_pos = -1;
			return 0;			// EOF, no more subsections
		}
		
		strtrim((unsigned char *)line, (unsigned char *)line);

		if (line[0]=='[')
			for(i=0;i<80;i++)
				if (line[i]==']') {
					file->macro_subsection_pos = -1;
					return 0;	// Next section, no more subsections
				}

		if (line[0]=='<')
			for(i=0;i<80;i++)
				if (line[i]=='>') {
					file->macro_subsection_pos = ftell(file->file_pointer);
					memset(file->macro_subsection_name, 0, 80);
					strncpy((char *)file->macro_subsection_name, line+1, i-1);
					strtrim(file->macro_subsection_name, 
						file->macro_subsection_name);
					return 1;	// Success
				}
	} while(!feof(file->file_pointer));

	file->macro_subsection_pos = -1;
	return 0;
}

//----------------------------------------------------------------------------		
int	macro_enum_variables(MACROFILE *file)
{
	char	line[80];
	int	i;
	
	if (file->macro_subsection_pos == -1) {
		file->macro_variable_pos = -1;
		return 0;
	}
	
	if (file->macro_variable_pos == -1)
		fseek(file->file_pointer, file->macro_subsection_pos, SEEK_SET);
	else
		fseek(file->file_pointer, file->macro_variable_pos, SEEK_SET);

	do {
		if (fgets(line, 80, file->file_pointer) == NULL) {
			file->macro_variable_pos = -1;
			return 0;			// EOF, no more variables
		}
		
		strtrim((unsigned char *)line, (unsigned char *)line);

		if (strchr(line, '=') != NULL) {
			unsigned char	*c;
			file->macro_variable_pos = ftell(file->file_pointer);
			strtrim(file->macro_variable, (unsigned char *)line);

			return 1;
		}
			
		if (line[0]=='[')
			for(i=0;i<80;i++)
				if (line[i]==']') {
					file->macro_variable_pos = -1;
					return 0;	// Next section, no more variables
				}

		if (line[0]=='<')
			for(i=0;i<80;i++)
				if (line[i]=='>') {
					file->macro_variable_pos = -1;
					return 0;	// Next subsection, no more variables
				}
	} while(!feof(file->file_pointer));

	file->macro_variable_pos = -1;
	return 0;
}

//----------------------------------------------------------------------------
void	macro_invalidate_section(MACROFILE *file)
{
	file->macro_section_pos = -1;
	file->macro_subsection_pos = -1;
	file->macro_variable_pos = -1;
}

//----------------------------------------------------------------------------
void	macro_invalidate_subsection(MACROFILE *file)
{
	file->macro_subsection_pos = -1;
	file->macro_variable_pos = -1;
}

//----------------------------------------------------------------------------
void	macro_invalidate_variable(MACROFILE *file)
{
	file->macro_variable_pos = -1;
}

//----------------------------------------------------------------------------
int	macro_split(char *line, char *name, char *macro)
{
	char **ptr = &line;
  	char *element;

	element = strsep(ptr, "=,");
	if (element==NULL)
		return 0;
	strtrim((unsigned char *)name, (unsigned char *)element);
	
	element = strsep(ptr, "=,");
	if (element==NULL)
		return 0;
	strtrim((unsigned char *)macro, (unsigned char *)element);

	return 1;
}

//----------------------------------------------------------------------------
int	cheat_split(char *line, char *name, int *code, int *type)
{
	char **ptr = &line;
  	char *element;

	element = strsep(ptr, "=,");
	if (element==NULL)
		return 0;
	strtrim((unsigned char *)name, (unsigned char *)element);
	
	element = strsep(ptr, "=,");
	if (element==NULL)
		return 0;

	strtrim((unsigned char *)element, (unsigned char *)element);

	if (toupper(element[0]) == 'B')
		*type = 0;
	else
		*type = 1;

	element++;

	sscanf(element, "%08x", code);
	
	return 1;
}

//----------------------------------------------------------------------------
int	macro_find_section(MACROFILE *file, unsigned char *section_name)
{
	macro_invalidate_section(file);
	
	while( macro_enum_sections(file) )
		if ( strcmp((char *)file->macro_section_name, (char *)section_name)
			== 0 )
			return	1;

	return 0;
}

//----------------------------------------------------------------------------
int	macro_find_subsection(MACROFILE *file, unsigned char *subsection_name)
{
	macro_invalidate_subsection(file);
	
	while( macro_enum_subsections(file) )
		if ( strcmp((char *)file->macro_subsection_name,
			(char *)subsection_name) == 0 )
			return 1;
	
	return 0;
}

//----------------------------------------------------------------------------
int	macro_load(MACROFILE *file, unsigned char *subsection_name, MACRO *mac)
{
	char		name[80];
	char		macro[80];
	int			i;
	char		temp[80];

	if (macro_find_subsection(file, subsection_name) == 0)
		return 0;
	
	for(i=0;i<16;i++)
	{
		mac[i].macro[0] = 0;
		mac[i].name[0] = 0;
	}
	
	i = 0;
	while( macro_enum_variables(file)&&(i < 16) )
	{
		macro_split((char *)file->macro_variable, name, macro);
		
		strcpy((char *)mac[i].name, (char *)name);
		sgetm((unsigned char *)macro, (unsigned char *)temp);
		memcpy(mac[i].macro, temp, 80);
		
		i++;
	}
	
	return 1;
}

//----------------------------------------------------------------------------
void	sprintm(unsigned char *buffer, unsigned char *manip)
{
	while( (*manip) != 0xFF ) {
		switch( (*manip)&0x0f )
		{
		case 0:
			(*buffer++) = '5';
			break;
		case 1:
			(*buffer++) = '8';
			break;
		case 2:
			(*buffer++) = '2';
			break;
		case 4:
			(*buffer++) = '4';
			break;
		case 5:
			(*buffer++) = '7';
			break;
		case 6:
			(*buffer++) = '1';
			break;
		case 8:
			(*buffer++) = '6';
			break;
		case 9:
			(*buffer++) = '9';
			break;
		case 10:
			(*buffer++) = '3';
			break;
		}
	
		if ( ((*manip)&0xf0)!=0 ) {
			(*buffer++) = '+';
			if ((*manip)&0x10)
				(*buffer++) = 'A';
			if ((*manip)&0x20)
				(*buffer++) = 'B';
			if ((*manip)&0x40)
				(*buffer++) = 'C';
			if ((*manip)&0x80)
				(*buffer++) = 'D';
		}
		manip++;
	}
	*buffer = 0;
}

//----------------------------------------------------------------------------
int	sgetm(unsigned char *buffer, unsigned char *manip)
{
	while( (*buffer) != 0 ) {
		*manip = 0;
		switch( *buffer ) {
		case	'1':
			(*manip) =  6;
			break;
		case	'4':
			(*manip) =  4;
			break;
		case	'7':
			(*manip) =  5;
			break;
		case	'8':
			(*manip) =  1;
			break;
		case	'9':
			(*manip) =  9;
			break;
		case	'6':
			(*manip) =  8;
			break;
		case	'3':
			(*manip) =  10;
			break;
		case	'2':
			(*manip) =  2;
			break;
		case	'5':
			(*manip) = 0;
			break;
		case	'+':
			manip--;
			buffer++;
			goto do_button_decoding;
			break;
		case	'A':
		case	'B':
		case	'C':
		case	'D':
do_button_decoding:
			while( (*buffer>='A')&&(*buffer<='D') ) {
				switch( *buffer ) {
				case	'A' :
					*manip |= 0x10;
					break;
				case	'B' :
					*manip |= 0x20;
					break;
				case	'C' :
					*manip |= 0x40;
					break;
				case	'D' :
					*manip |= 0x80;
					break;
				}
				buffer++;
			}
			buffer--;
			break;
		default :
			return 0;
		}
		buffer++;
		manip++;
	}
	
	*manip = 0xFF;
	return 1;
}			

