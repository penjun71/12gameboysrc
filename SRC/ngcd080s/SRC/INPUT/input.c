/**************************************
****   INPUT.C  -  Input devices   ****
**************************************/

//-- Include Files -----------------------------------------------------------
#include <stdio.h>
#include <allegro.h>
#include "../config/config.h"
#include "../gui/gui.h"
#include "macro.h"

//-- Structures --------------------------------------------------------------
typedef struct {
	unsigned char	key_name[16];
	unsigned int	key_scancode;
} KEYDEF;

typedef struct {
	unsigned char	joy_name[28];
	unsigned int	joy_id;
} JOYDEF;

extern void not_implemented(void);
extern int	neogeo_ipl_done;

//-- Private Variables -------------------------------------------------------

#define JOY_MAX 18

JOYDEF	joydefs[JOY_MAX] = {
	{"No Joystick",              JOY_TYPE_NONE},
	{"4 Buttons",                JOY_TYPE_4BUTTON},
	{"6 Buttons",                JOY_TYPE_6BUTTON},
	{"8 Buttons",                JOY_TYPE_8BUTTON},
	{"CH FlightStick Pro",       JOY_TYPE_FSPRO},
	{"Wingman Extreme",          JOY_TYPE_WINGEX},
	{"MS Sidewinder",            JOY_TYPE_SIDEWINDER},
	{"Gravis Gamepad Pro",       JOY_TYPE_GAMEPAD_PRO},
	{"SNES Pad LPT1",            JOY_TYPE_SNESPAD_LPT1},
	{"SNES Pad LPT2",            JOY_TYPE_SNESPAD_LPT2},
	{"SNES Pad LPT3",            JOY_TYPE_SNESPAD_LPT3},
	{"PSX Pad LPT1",			 JOY_TYPE_PSXPAD_LPT1},
	{"PSX Pad LPT2",			 JOY_TYPE_PSXPAD_LPT2},
	{"PSX Pad LPT3",			 JOY_TYPE_PSXPAD_LPT3},
	{"N64 Pad LPT1",			 JOY_TYPE_N64PAD_LPT1},
	{"N64 Pad LPT2",			 JOY_TYPE_N64PAD_LPT2},
	{"N64 Pad LPT3",			 JOY_TYPE_N64PAD_LPT3},
	{"WingMan Warrior",			 JOY_TYPE_WINGWARRIOR}
	};
	
KEYDEF	keydefs[KEY_MAX] = {
	{"KEY_A",					KEY_A},
	{"KEY_B",					KEY_B},
 	{"KEY_C",					KEY_C},
	{"KEY_D",					KEY_D},
	{"KEY_E",					KEY_E},
	{"KEY_F",					KEY_F},
	{"KEY_G",					KEY_G},
	{"KEY_H",					KEY_H},
	{"KEY_I",					KEY_I},
	{"KEY_J",					KEY_J},
	{"KEY_K",					KEY_K},
	{"KEY_L", 					KEY_L},
	{"KEY_M",					KEY_M},
	{"KEY_N",					KEY_N},
	{"KEY_O",					KEY_O},
	{"KEY_P",					KEY_P},
	{"KEY_Q",					KEY_Q},
	{"KEY_R",					KEY_R},
	{"KEY_S",					KEY_S},
	{"KEY_T",					KEY_T},
	{"KEY_U",					KEY_U},
	{"KEY_V",					KEY_V},
	{"KEY_W",					KEY_W},
	{"KEY_X",					KEY_X},
	{"KEY_Y",					KEY_Y},
	{"KEY_Z",					KEY_Z},
	{"KEY_0",					KEY_0},
	{"KEY_1",					KEY_1},
	{"KEY_2",					KEY_2},
	{"KEY_3", 					KEY_3},
	{"KEY_4",					KEY_4},
	{"KEY_5",					KEY_5},
	{"KEY_6",					KEY_6},
	{"KEY_7",					KEY_7},
	{"KEY_8",					KEY_8},
	{"KEY_9",					KEY_9},
	{"KEY_0_PAD",	    	KEY_0_PAD},
	{"KEY_1_PAD",			KEY_1_PAD},
	{"KEY_2_PAD",			KEY_2_PAD},
	{"KEY_3_PAD",			KEY_3_PAD},
	{"KEY_4_PAD",			KEY_4_PAD},
	{"KEY_5_PAD",			KEY_5_PAD},
	{"KEY_6_PAD",			KEY_6_PAD},
	{"KEY_7_PAD",			KEY_7_PAD},
	{"KEY_8_PAD",			KEY_8_PAD},
	{"KEY_9_PAD",			KEY_9_PAD},
	{"KEY_F1",				   KEY_F1},
	{"KEY_F2",				   KEY_F2},
	{"KEY_F3",				   KEY_F3},
	{"KEY_F4",				   KEY_F4},
	{"KEY_F5",				   KEY_F5},
	{"KEY_F6",				   KEY_F6},
	{"KEY_F7",				   KEY_F7},
	{"KEY_F8",				   KEY_F8},
	{"KEY_F9",				   KEY_F9},
	{"KEY_F10",				  KEY_F10},
	{"KEY_F11",				  KEY_F11},
	{"KEY_F12",				  KEY_F12},
	{"KEY_ESC",				  KEY_ESC},
	{"KEY_TILDE",			KEY_TILDE},
	{"KEY_MINUS",			KEY_MINUS},
	{"KEY_EQUALS",		   KEY_EQUALS},
	{"KEY_BACKSPACE",	KEY_BACKSPACE},
	{"KEY_TAB",				  KEY_TAB},
	{"KEY_OPENBRACE",	KEY_OPENBRACE},
	{"KEY_CLOSEBRACE", KEY_CLOSEBRACE},
	{"KEY_ENTER",			KEY_ENTER},
	{"KEY_COLON",			KEY_COLON},
	{"KEY_QUOTE",			KEY_QUOTE},
	{"KEY_BACKSLASH",	KEY_BACKSLASH},
	{"KEY_BACKSLASH2", KEY_BACKSLASH2},
	{"KEY_COMMA",			KEY_COMMA},
	{"KEY_STOP",			 KEY_STOP},
	{"KEY_SLASH",			KEY_SLASH},
	{"KEY_SPACE",			KEY_SPACE},
	{"KEY_INSERT",		   KEY_INSERT},
	{"KEY_DEL",				  KEY_DEL},
	{"KEY_HOME",			 KEY_HOME},
	{"KEY_END",				  KEY_END},
	{"KEY_PGUP",			 KEY_PGUP},
	{"KEY_PGDN",			 KEY_PGDN},
	{"KEY_LEFT",			 KEY_LEFT},
	{"KEY_RIGHT",			KEY_RIGHT},
	{"KEY_UP",				   KEY_UP},
	{"KEY_DOWN",			 KEY_DOWN},
	{"KEY_SLASH_PAD",	KEY_SLASH_PAD},
	{"KEY_ASTERISK",	 KEY_ASTERISK},
	{"KEY_MINUS_PAD",	KEY_MINUS_PAD},
	{"KEY_PLUS_PAD",     KEY_PLUS_PAD},
	{"KEY_DEL_PAD",		  KEY_DEL_PAD},
	{"KEY_ENTER_PAD",   KEY_ENTER_PAD},
	{"KEY_PRTSCR",		   KEY_PRTSCR},
	{"KEY_PAUSE",			KEY_PAUSE},
	{"KEY_LSHIFT",		   KEY_LSHIFT},
	{"KEY_RSHIFT",		   KEY_RSHIFT},
	{"KEY_LCONTROL",	 KEY_LCONTROL},
	{"KEY_RCONTROL",	 KEY_RCONTROL},
	{"KEY_ALT",				  KEY_ALT},
	{"KEY_ALTGR",			KEY_ALTGR},
	{"KEY_LWIN",			 KEY_LWIN},
	{"KEY_RWIN",			 KEY_RWIN},
	{"KEY_MENU",			 KEY_MENU},
	{"KEY_SCRLOCK",		  KEY_SCRLOCK},
	{"KEY_NUMLOCK",		  KEY_NUMLOCK},
	{"KEY_CAPSLOCK",	 KEY_CAPSLOCK}
};

//---------------------------------------------------------------------------
char	*key_getter(int index, int *list_size)
{
	if (index < 0) {
		*list_size = KEY_MAX;
		return NULL;
	}
	
	return (keydefs[index].key_name + 4);
}

//---------------------------------------------------------------------------
char	*joy_getter(int index, int *list_size)
{
	if (index < 0) {
		*list_size = JOY_MAX;
		return NULL;
	}
	
	return (joydefs[index].joy_name);
}

//---------------------------------------------------------------------------
char	*act1_getter(int index, int *list_size)
{
	static char *act1_str[] = {
		"Up",
		"Down",
		"Left",
		"Right",
		"A",
		"B",
		"C",
		"D",
		"Start",
		"Select",
		"Macro01",		
		"Macro02",		
		"Macro03",		
		"Macro04",		
		"Macro05",		
		"Macro06",		
		"Macro07",		
		"Macro08",		
		"Macro09",		
		"Macro10",		
		"Macro11",		
		"Macro12",		
		"Macro13",		
		"Macro14",		
		"Macro15",		
		"Macro16"
	};
	
   if (index < 0) {
      *list_size = 26;
      return NULL;
   }

   return act1_str[index]; 
}	

//---------------------------------------------------------------------------
char	*act2_getter(int index, int *list_size)
{
	static char *act2_str[] = {
		"A",
		"B",
		"C",
		"D",
		"Start",
		"Select",
		"Macro01",		
		"Macro02",		
		"Macro03",		
		"Macro04",		
		"Macro05",		
		"Macro06",		
		"Macro07",		
		"Macro08",		
		"Macro09",		
		"Macro10",		
		"Macro11",		
		"Macro12",		
		"Macro13",		
		"Macro14",		
		"Macro15",		
		"Macro16"
	};
	
   if (index < 0) {
      *list_size = 22;
      return NULL;
   }

   return act2_str[index]; 
}

//---------------------------------------------------------------------------
char	*btn_getter(int index, int *list_size)
{
	static char *btn_str = "But. 00";
	
	if (index < 0) {
		*list_size = 22;
		return NULL;
	}
	
	sprintf(btn_str, "But. %02d", index);
	return btn_str;
}

//---------------------------------------------------------------------------
char	*p1_getter(int index, int *list_size)
{
	if (index < 0)
	{
		*list_size = 16;
		return NULL;
	}
	
	return macro_list_p1[index].name;
}

//---------------------------------------------------------------------------
char	*p2_getter(int index, int *list_size)
{
	if (index < 0)
	{
		*list_size = 16;
		return NULL;
	}
	
	return macro_list_p2[index].name;
}

char	name_list[50][32];
int		name_list_elems = 0;

//---------------------------------------------------------------------------
char	*name_getter(int index, int *list_size)
{
	if (index < 0)
	{
		*list_size = name_list_elems;
		return NULL;
	}
	
	return name_list[index];
}

DIALOG kbd_setup_dlg[] =
{
   /* (dialog proc)   (x)  (y)  (w)  (h)    (fg)    (bg)  (key)  (flags)   (d1)  (d2)            (dp) */
 {d_neo_border_proc,   0,   0, 224, 225, 0x0000, 0x0000,      0,      0,     0,    0,                NULL},
 {d_neo_fbox_proc,     2,   2, 220,   8, 0x0000, 0x10E3,      0,      0,     0,    0,                NULL},
 {d_text_proc,        10,   3,   0,   0, 0xFFFF, 0x10E3,      0,      0,     0,    0,    "Keyboard Setup"},
 {d_neo_gbox_proc,     5,  20, 214,  70, 0xC71C, 0x328A,      0,      0,     0,    0,        "Player One"},
 {d_neo_list_proc,    10,  26,  83,  59, 0xC71C, 0x328A,      0,      0,     0,    0,          key_getter},
 {d_neo_button_proc,  98,  38,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "<<Get"},
 {d_neo_button_proc,  98,  58,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "Set>>"},
 {d_neo_list_proc,   143,  26,  71,  59, 0xC71C, 0x328A,      0,      0,     0,    0,         act1_getter},

 {d_neo_button_proc,  79, 100,  65,  15, 0x0000, 0x0000,    'e', D_EXIT,     0,    0,         "&Exchange"},

 {d_neo_gbox_proc,     5, 125, 214,  70, 0xC71C, 0x328A,      0,      0,     0,    0,        "Player Two"},
 {d_neo_list_proc,    10, 131,  83,  59, 0xC71C, 0x328A,      0,      0,     0,    0,          key_getter},
 {d_neo_button_proc,  98, 143,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "<<Get"},
 {d_neo_button_proc,  98, 163,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "Set>>"},
 {d_neo_list_proc,   143, 131,  71,  59, 0xC71C, 0x328A,      0,      0,     0,    0,         act1_getter},
 
 {d_neo_button_proc,  42, 203, 65,  15, 0x0000, 0x0000,     'o', D_EXIT,     0,    0,               "&OK"},
 {d_neo_button_proc, 117, 203, 65,  15, 0x0000, 0x0000,     'm', D_EXIT,     0,    0,        "&Macrokeys"},

 {NULL,                0,   0,   0,   0, 0x0000, 0x0000,      0,      0,     0,    0,                NULL}
};

DIALOG joy_setup_dlg[] =
{
   /* (dialog proc)   (x)  (y)  (w)  (h)    (fg)    (bg)  (key)  (flags)   (d1)  (d2)            (dp) */
 {d_neo_border_proc,   0,   0, 319, 200, 0x0000, 0x0000,      0,      0,     0,    0,                NULL},
 {d_neo_fbox_proc,     2,   2, 315,   8, 0x0000, 0x10E3,      0,      0,     0,    0,                NULL},
 {d_text_proc,        10,   3,   0,   0, 0xFFFF, 0x10E3,      0,      0,     0,    0,    "Joystick Setup"},
 {d_neo_gbox_proc,     5,  20, 190,  70, 0xC71C, 0x328A,      0,      0,     0,    0,        "Player One"},
 {d_neo_list_proc,    10,  26,  65,  59, 0xC71C, 0x9596,      0,      0,     0,    0,          btn_getter},
 {d_neo_button_proc,  80,  38,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "<<Get"},
 {d_neo_button_proc,  80,  58,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "Set>>"},
 {d_neo_list_proc,   125,  26,  65,  59, 0xC71C, 0x9596,      0,      0,     0,    0,         act2_getter},

 {d_neo_gbox_proc,     5, 100, 190,  70, 0xC71C, 0x328A,      0,      0,     0,    0,        "Player Two"},
 {d_neo_list_proc,    10, 106,  65,  59, 0xC71C, 0x9596,      0,      0,     0,    0,          btn_getter},
 {d_neo_button_proc,  80, 118,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "<<Get"},
 {d_neo_button_proc,  80, 138,  40,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,             "Set>>"},
 {d_neo_list_proc,   125, 106,  65,  59, 0xC71C, 0x9596,      0,      0,     0,    0,         act2_getter},

 {d_neo_gbox_proc,   200,  20, 115, 150, 0xC71C, 0x328A,      0,      0,     0,    0,     "Joystick Type"},
 {d_neo_list_proc,   205,  26, 105, 119, 0xC71C, 0x9596,      0,      0,     0,    0,          joy_getter},
 {d_neo_button_proc, 205, 150, 105,  15, 0x0000, 0x0000,    'c', D_EXIT,     0,    0,           "&Change"},
 
 {d_neo_button_proc,  55, 180, 100,  15, 0x0000, 0x0000,    'o', D_EXIT,     0,    0,               "&OK"},
 {d_neo_button_proc, 165, 180, 100,  15, 0x0000, 0x0000,    'm', D_EXIT,     0,    0, "Select &Macrokeys"},

 {NULL,                0,   0,   0,   0, 0x0000, 0x0000,      0,      0,     0,    0,                NULL}
};

DIALOG macros_select[] =
{
   /* (dialog proc)   (x)  (y)  (w)  (h)    (fg)    (bg)  (key)  (flags)   (d1)  (d2)            (dp) */
 {d_neo_border_proc,   0,   0, 319, 239, 0x0000, 0x0000,      0,      0,     0,    0,                NULL},
 {d_neo_fbox_proc,     2,   2, 315,   8, 0x0000, 0x10E3,      0,      0,     0,    0,                NULL},
 {d_text_proc,        10,   3,   0,   0, 0xFFFF, 0x10E3,      0,      0,     0,    0,"Macrokeys Selection"},
 {d_text_proc,        10,  13,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,       "Game Name :"},
 {d_text_proc,		  80,  13,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,    config_game_name},
 
 {d_neo_gbox_proc,    10,  28, 145,  95, 0xC71C, 0x328A,      0,      0,     0,    0,          "Player 1"},
 {d_neo_gbox_proc,   164,  28, 145,  95, 0xC71C, 0x328A,      0,      0,     0,    0,          "Player 2"},
 {d_neo_list_proc,    15,  33, 135,  85, 0xFFFF, 0x9596,      0,      0,     0,    0,           p1_getter},
 {d_neo_list_proc,   169,  33, 135,  85, 0xFFFF, 0x9596,      0,      0,     0,    0,           p2_getter},

 {d_neo_button_proc,  49, 127,  65,  15, 0x0000, 0x0000,    '1', D_EXIT,     0,    0,         "Load (&1)"},
 {d_neo_button_proc, 203, 127,  65,  15, 0x0000, 0x0000,    '2', D_EXIT,     0,    0,         "Load (&2)"},

 {d_neo_gbox_proc,    10, 150, 299,  60, 0xC71C, 0x328A,      0,      0,     0,    0,"Available Macro Sets"},
 {d_neo_list_proc,    15, 155, 289,  50, 0xFFFF, 0x9596,      0,      0,     0,    0,         name_getter}, 

 {d_neo_button_proc, 127, 217, 65,  15, 0x0000, 0x0000,     'o', D_EXIT,     0,    0,               "&OK"},

 {NULL,                0,   0,  0,   0, 0x0000, 0x0000,      0,       0,     0,    0,                NULL}
};

static int	macro_number_p1 = 0;
static int	macro_pos_p1 = 0;
static int	macro_counter_p1 = 0;
static int	macro_playing_p1 = 0;

static int	macro_number_p2 = 0;
static int	macro_pos_p2 = 0;
static int	macro_counter_p2 = 0;
static int	macro_playing_p2 = 0;

//-- Global Variables --------------------------------------------------------

void	keyboard_setup(void);
void	macrokeys_select(void);
				
//----------------------------------------------------------------------------
unsigned char	read_player1(void)
{
	unsigned char	k = 0xFF;
	int				i = 0;

	if ( !macro_playing_p1 )
	{
		if (key[kbd_p1.up]||joy[0].stick[0].axis[1].d1)
			k &= 0xFE;

		if (key[kbd_p1.down]||joy[0].stick[0].axis[1].d2)
			k &= 0xFD;

		if (key[kbd_p1.left]||joy[0].stick[0].axis[0].d1)
			k &= 0xFB;

		if (key[kbd_p1.right]||joy[0].stick[0].axis[0].d2)
			k &= 0xF7;

		if (key[kbd_p1.a]||joy[0].button[joy_p1.a].b)
			k &= 0xEF;

		if (key[kbd_p1.b]||joy[0].button[joy_p1.b].b)
			k &= 0xDF;

		if (key[kbd_p1.c]||joy[0].button[joy_p1.c].b)
			k &= 0xBF;

		if (key[kbd_p1.d]||joy[0].button[joy_p1.d].b)
			k &= 0x7F;
	
		if (k==0xFF)
		{
			while((macro_list_p1[i].macro[0] != 0)&&(i<16))
			{
				if (key[((int *)&kbd_p1.macro_01)[i]] != 0)
				{
					macro_playing_p1 = 1;
					macro_number_p1 = i;
					macro_pos_p1 = 0;
					macro_counter_p1 = 5;
				}
			
				i++;
			}
		}
	}
	else
	{
		if (macro_list_p1[macro_number_p1].macro[macro_pos_p1] == 0xFF)
			macro_playing_p1 = 0;
		else
		{
			k = ~macro_list_p1[macro_number_p1].macro[macro_pos_p1];
			if (macro_counter_p1 != 0)
				macro_counter_p1--;
			else
			{
				macro_counter_p1 = 5;
				macro_pos_p1++;
			}
		}
	}

	return	k;
}

//----------------------------------------------------------------------------
unsigned char	read_player2(void)
{
	unsigned char	k = 0xFF;
	int				i = 0;

	if ( !macro_playing_p2 )
	{
		if (key[kbd_p2.up]||joy[1].stick[0].axis[1].d1)
			k &= 0xFE;

		if (key[kbd_p2.down]||joy[1].stick[0].axis[1].d2)
			k &= 0xFD;

		if (key[kbd_p2.left]||joy[1].stick[0].axis[0].d1)
			k &= 0xFB;

		if (key[kbd_p2.right]||joy[1].stick[0].axis[0].d2)
			k &= 0xF7;

		if (key[kbd_p2.a]||joy[1].button[joy_p2.a].b)
			k &= 0xEF;

		if (key[kbd_p2.b]||joy[1].button[joy_p2.b].b)
			k &= 0xDF;

		if (key[kbd_p2.c]||joy[1].button[joy_p2.c].b)
			k &= 0xBF;

		if (key[kbd_p2.d]||joy[1].button[joy_p2.d].b)
			k &= 0x7F;
	
		if (k==0xFF)
		{
			while((macro_list_p2[i].macro[0] != 0)&&(i<16))
			{
				if (key[((int *)&kbd_p2.macro_01)[i]] != 0)
				{
					macro_playing_p2 = 1;
					macro_number_p2 = i;
					macro_pos_p2 = 0;
					macro_counter_p2 = 5;
				}
			
				i++;
			}
		}
	}
	else
	{
		if (macro_list_p2[macro_number_p2].macro[macro_pos_p2] == 0xFF)
			macro_playing_p2 = 0;
		else
		{
			k = ~macro_list_p2[macro_number_p2].macro[macro_pos_p2];
			if (macro_counter_p2 != 0)
				macro_counter_p2--;
			else
			{
				macro_counter_p2 = 5;
				macro_pos_p2++;
			}
		}
	}

	return	k;
}

//----------------------------------------------------------------------------
char	read_pl12_startsel(void)
{
	char	k = 0x0F;
	
	if (key[kbd_p1.start]||joy[0].button[joy_p1.start].b)
		k &= 0xFE;
		
	if (key[kbd_p1.select]||joy[0].button[joy_p1.select].b)
		k &= 0xFD;

	if (key[kbd_p2.start]||joy[1].button[joy_p2.start].b)
		k &= 0xFB;
		
	if (key[kbd_p2.select]||joy[1].button[joy_p2.select].b)
		k &= 0xF7;

	return k;
}

//----------------------------------------------------------------------------
unsigned int	input_identify(unsigned char *key_name)
{
	unsigned int	i;
	
	for(i=0;i<KEY_MAX;i++)
		if (strcmp((char *)key_name, (char *)keydefs[i].key_name) == 0)
			return keydefs[i].key_scancode;
	
	return 0;
}

//----------------------------------------------------------------------------
unsigned char	*input_whois(unsigned int key_scancode)
{
	unsigned int	i;
	
	for(i=0;i<KEY_MAX;i++)
		if (keydefs[i].key_scancode == key_scancode)
			return keydefs[i].key_name;
	
	return	NULL;
}

//---------------------------------------------------------------------------
void	keyboard_setup(void)
{
	int	ret;
	
	
	centre_dialog(kbd_setup_dlg);
	
	do
	{
		vsync();
		ret = do_dialog(kbd_setup_dlg, 0);
		
		if (ret==5)
			kbd_setup_dlg[4].d1 = val_to_index(config_get_var_kbd(&kbd_p1, kbd_setup_dlg[7].d1));
			
		if (ret==6)
			config_set_var_kbd(&kbd_p1, kbd_setup_dlg[7].d1, keydefs[kbd_setup_dlg[4].d1].key_scancode);

		if (ret==8)
		{
			CONFIG	temp;
			
			memcpy((void *)&temp, (void *)&kbd_p1, sizeof(CONFIG));
			memcpy((void *)&kbd_p1, (void *)&kbd_p2, sizeof(CONFIG));
			memcpy((void *)&kbd_p2, (void *)&temp, sizeof(CONFIG));
			
			neo_alert1("Notification", NULL, "Exchanged P1 <-> P2 configurations.", NULL, "&Ok", 'o');
		}
	
		if (ret==11)
			kbd_setup_dlg[10].d1 = val_to_index(config_get_var_kbd(&kbd_p2, kbd_setup_dlg[13].d1));

		if (ret==12)
			config_set_var_kbd(&kbd_p2, kbd_setup_dlg[13].d1, keydefs[kbd_setup_dlg[10].d1].key_scancode);
			
		if (ret==15)
			macrokeys_select();
		
		
	} while(ret != 14);

	config_save_kbd_conf(&kbd_p1, "keyboard1");
	config_save_kbd_conf(&kbd_p2, "keyboard2");

}

//---------------------------------------------------------------------------
void joystick_setup(void)
{
	int	ret, i;
	
	centre_dialog(joy_setup_dlg);

	ret = get_config_id("joystick", "joytype", JOY_TYPE_NONE);

	for(i=0;i<JOY_MAX;i++)
	{
		if (ret == joydefs[i].joy_id)
			break;
	}

	joy_setup_dlg[14].d1 = i;
	
	do {
		vsync();	
		ret = do_dialog(joy_setup_dlg, 0);
		
		if (ret==5)
			joy_setup_dlg[4].d1 = config_get_var_joy(&joy_p1, joy_setup_dlg[7].d1);
		
		if (ret==6)
			config_set_var_joy(&joy_p1, joy_setup_dlg[7].d1, joy_setup_dlg[4].d1);

		if (ret==10)
			joy_setup_dlg[9].d1 = config_get_var_joy(&joy_p2, joy_setup_dlg[12].d1);
		
		if (ret==11)
			config_set_var_joy(&joy_p2, joy_setup_dlg[12].d1, joy_setup_dlg[9].d1);
		
		if (ret==15)
		{
			int	val;
			char	msg1[80];
			char	msg2[80];
			
			remove_joystick();
			val = install_joystick(joydefs[joy_setup_dlg[14].d1].joy_id);
			
			if (val != 0)
				neo_alert1("ERROR", NULL, "Joystick initialization failed! (is it plugged?)", NULL, "&Abort", 'a');
			else
			{
				for (val=0; val<num_joysticks; val++) {
					if (joy[val].flags & JOYFLAG_CALIBRATE)
						sprintf(msg1, "Please calibrate joystick #%d", val);
					
					while (joy[val].flags & JOYFLAG_CALIBRATE) {
						sprintf(msg2, "%s, and press OK.", calibrate_joystick_name(val));

						rest(10);
						neo_alert1(msg1, NULL, msg2, NULL, "&Ok", 'o');
	 
		 				calibrate_joystick(val);
					
						rest(10);
					}
				}
			}
			
			neo_alert1("OK", NULL, "Joystick setup complete !", NULL, "&Ok", 'o');
			save_joystick_data(NULL);
		}
		
		if (ret==17)
			macrokeys_select();
		
	} while(ret != 16);

	config_save_joy_conf(&joy_p1, "joystick1");
	config_save_joy_conf(&joy_p2, "joystick2");

}

//---------------------------------------------------------------------------
void macrokeys_select(void)
{
	int			ret, i;
	MACROFILE	cfg;
	
	if (!neogeo_ipl_done)
	{
		neo_alert1("Error", NULL, "This function is useless without a game loaded.", NULL, "&Abort", 'a');
		return;
	}

	name_list_elems = 0;
	
	macro_fopen(&cfg, "MACRO.CFG");
	
	if (!macro_find_section(&cfg, config_game_name))
	{
		macro_fclose(&cfg);
		neo_alert1(config_game_name, NULL, "No entry for this game in MACRO.CFG.", NULL, "&Abort", 'a');
		return;
	}
	
	while(macro_enum_subsections(&cfg))
	{
		strcpy(name_list[name_list_elems], cfg.macro_subsection_name);
		name_list_elems++;
	}
	
	if (name_list_elems == 0)
	{
		macro_fclose(&cfg);
		neo_alert1(config_game_name, NULL, "No macrokey set associated with this game.", NULL, "&Abort", 'a');
		return;
	}
	
	centre_dialog(macros_select);

	do{
		vsync();	
		ret = do_dialog(macros_select, 0);
		
		if (ret==9)
			macro_load( &cfg, name_list[macros_select[12].d1], macro_list_p1);
			
		if (ret==10)
			macro_load( &cfg, name_list[macros_select[12].d1], macro_list_p2);

	} while(ret != 13);

	macro_fclose(&cfg);
}
