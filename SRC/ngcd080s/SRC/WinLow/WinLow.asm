; ************************************
; **** Windows Low Level Routines ****
; ************************************

	BITS	32
	
	GLOBAL	_winlow_is_windows_present
	GLOBAL	_winlow_disable_windows
	GLOBAL	_winlow_enable_windows
	GLOBAL	_winlow_get_vm_id
	GLOBAL	_winlow_enable_switch_notification
	GLOBAL	_winlow_disable_switch_notification
	GLOBAL	_custom_interrupt_handler
	
	EXTERN	_background_switched
	
	%include ".\src\helper.asm"
	
	SECTION	.text

_winlow_is_windows_present:
	mov	ax, 1600h
	int	2Fh
	and	eax, 7Fh
	ret

_winlow_disable_windows:
	mov	ax, 1681h
	int	2Fh
	ret

_winlow_enable_windows:
	mov	ax, 1682h
	int	2Fh
	ret

_winlow_get_vm_id:
	push	ebx
	mov		ax, 1683h
	int		2Fh
	mov		ax, bx
	pop		ebx
	ret

_winlow_enable_switch_notification:
	mov	ax, 4000h
	int	2Fh
	ret

_winlow_disable_switch_notification:
	mov	ax, 4007h
	int	2Fh
	ret

_custom_interrupt_handler:

;	cmp	ax, 4001h
;	jnz	not_bk
	
;	mov	[_background_switched], dword 1
	
;not_bk:
	ret
