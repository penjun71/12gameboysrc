#include <go32.h>
#include <dpmi.h>
#include <string.h>

extern	void custom_interrupt_handler(void);

extern __dpmi_regs	_regs;

void	winlow_set_vm_title(char *new_title)
{
	dosmemput(new_title, strlen(new_title)+1, __tb);
	memset(&_regs, 0, sizeof(_regs));
	_regs.x.es = _regs.x.ds = _regs.x.cs = (__tb>>4);
	_regs.x.di = 0;
	_regs.x.dx = 1;
	_regs.x.ax = 0x168E;
	__dpmi_int(0x2f, &_regs);
}

void	winlow_set_app_title(char *new_title)
{
	dosmemput(new_title, strlen(new_title)+1, __tb);
	memset(&_regs, 0, sizeof(_regs));
	_regs.x.es = _regs.x.ds = _regs.x.cs = (__tb>>4);
	_regs.x.di = 0;
	_regs.x.dx = 0;
	_regs.x.ax = 0x168E;
	__dpmi_int(0x2f, &_regs);
}

void	winlow_get_vm_title(char *buffer, int buf_len)
{
	memset(&_regs, 0, sizeof(_regs));
	_regs.x.es = _regs.x.ds = _regs.x.cs = (__tb>>4);
	_regs.x.di = 0;
	_regs.x.dx = 3;
	_regs.x.cx = buf_len;
	_regs.x.ax = 0x168E;
	__dpmi_int(0x2f, &_regs);
	dosmemget(__tb, buf_len, buffer);
}

void	winlow_get_app_title(char *buffer, int buf_len)
{
	memset(&_regs, 0, sizeof(_regs));
	_regs.x.es = _regs.x.ds = _regs.x.cs = (__tb>>4);
	_regs.x.di = 0;
	_regs.x.dx = 2;
	_regs.x.cx = buf_len;
	_regs.x.ax = 0x168E;
	__dpmi_int(0x2f, &_regs);
	dosmemget(__tb, buf_len, buffer);
}
