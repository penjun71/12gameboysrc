#ifndef WinLowH
#define WinLowH

extern int	winlow_is_windows_present(void);
extern void	winlow_disable_windows(void);
extern void winlow_enable_windows(void);
extern int	winlow_get_vm_id(void);
extern void	winlow_enable_switch_notification(void);
extern void	winlow_disable_switch_notification(void);

extern void	winlow_set_vm_title(char *);
extern void	winlow_set_app_title(char *);
extern void	winlow_get_vm_title(char *buffer, int buf_len);
extern void	winlow_get_app_title(char *buffer, int buf_len);

#endif
