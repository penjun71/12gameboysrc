/**************************************
****  CONFIG.C  - Config Handling  ****
**************************************/

//-- Include Files -----------------------------------------------------------
#include <allegro.h>
#include <ctype.h>
#include "../cdrom/cdrom.h"
#include "../cdaudio/cdaudio.h"
#include "../memory/memory.h"
#include "../input/input.h"

#define KEY_NONE	0

typedef struct {
	int	up;
	int down;
	int left;
	int right;
	int	a;
	int	b;
	int	c;
	int	d;
	int	start;
	int	select;
	int	macro_01;
	int	macro_02;
	int	macro_03;
	int	macro_04;
	int	macro_05;
	int	macro_06;
	int	macro_07;
	int	macro_08;
	int	macro_09;
	int	macro_10;
	int	macro_11;
	int	macro_12;
	int	macro_13;
	int	macro_14;
	int	macro_15;
	int	macro_16;
} CONFIG;

//-- Global Variables --------------------------------------------------------
int				config_vsync;
int				config_scanlines;
unsigned char	config_game_name[80];

CONFIG			kbd_p1;
CONFIG	 		kbd_p2;
CONFIG			joy_p1;
CONFIG			joy_p2;

//-- Imported Variables ------------------------------------------------------
extern char	*neogeo_prg_memory;

//-- Exported Functions ------------------------------------------------------
void	config_read(void);
void	config_read_name(void);
void	config_read_kbd_conf(CONFIG *, char *);
void	config_save_kbd_conf(CONFIG *, char *);
void	config_read_joy_conf(CONFIG *, char *);
void	config_save_joy_conf(CONFIG *, char *);

//----------------------------------------------------------------------------
void	config_read(void)
{
	int				temp;
	unsigned char	tmp[80];
	
	config_vsync = get_config_int(NULL, "vsync", 0);
	config_scanlines = get_config_int(NULL, "scanlines", 0);
	temp = get_config_int(NULL, "nationality", 0);
	if (temp>2)
		temp = 2;
	cpu_writemem24(0x10FD83, temp);
	cdrom_current_drive = get_config_int(NULL, "cdrom", -1);
	cdda_current_drive = get_config_int(NULL, "cdaudio", -1);
	
	config_read_kbd_conf(&kbd_p1, "keyboard1");
	config_read_kbd_conf(&kbd_p2, "keyboard2");
	config_read_joy_conf(&joy_p1, "joystick1");
	config_read_joy_conf(&joy_p2, "joystick2");
}

//----------------------------------------------------------------------------
void	config_read_name(void)
{
	unsigned char	*Ptr;
	int				temp;

	Ptr = neogeo_prg_memory + cpu_readmem24_dword(0x11A);
	swab(Ptr, config_game_name, 80);
	
	for(temp=0;temp<80;temp++)
		if (!isprint(config_game_name[temp]))
			config_game_name[temp]=0;
}

void	config_read_kbd_conf(CONFIG *conf, char *section)
{
	conf->up = input_identify(get_config_string(section, "up", KEY_NONE));
	conf->down = input_identify(get_config_string(section, "down", KEY_NONE));
	conf->left = input_identify(get_config_string(section, "left", KEY_NONE));
	conf->right = input_identify(get_config_string(section, "right", "KEY_NONE"));
	conf->a = input_identify(get_config_string(section, "a", "KEY_NONE"));
	conf->b = input_identify(get_config_string(section, "b", "KEY_NONE"));
	conf->c = input_identify(get_config_string(section, "c", "KEY_NONE"));
	conf->d = input_identify(get_config_string(section, "d", "KEY_NONE"));
	conf->start = input_identify(get_config_string(section, "start", "KEY_NONE"));
	conf->select = input_identify(get_config_string(section, "select", "KEY_NONE"));
	conf->macro_01 = input_identify(get_config_string(section, "macro01", "KEY_NONE"));
	conf->macro_02 = input_identify(get_config_string(section, "macro02", "KEY_NONE"));
	conf->macro_03 = input_identify(get_config_string(section, "macro03", "KEY_NONE"));
	conf->macro_04 = input_identify(get_config_string(section, "macro04", "KEY_NONE"));
	conf->macro_05 = input_identify(get_config_string(section, "macro05", "KEY_NONE"));
	conf->macro_06 = input_identify(get_config_string(section, "macro06", "KEY_NONE"));
	conf->macro_07 = input_identify(get_config_string(section, "macro07", "KEY_NONE"));
	conf->macro_08 = input_identify(get_config_string(section, "macro08", "KEY_NONE"));
	conf->macro_09 = input_identify(get_config_string(section, "macro09", "KEY_NONE"));
	conf->macro_10 = input_identify(get_config_string(section, "macro10", "KEY_NONE"));
	conf->macro_11 = input_identify(get_config_string(section, "macro11", "KEY_NONE"));
	conf->macro_12 = input_identify(get_config_string(section, "macro12", "KEY_NONE"));
	conf->macro_13 = input_identify(get_config_string(section, "macro13", "KEY_NONE"));
	conf->macro_14 = input_identify(get_config_string(section, "macro14", "KEY_NONE"));
	conf->macro_15 = input_identify(get_config_string(section, "macro15", "KEY_NONE"));
	conf->macro_16 = input_identify(get_config_string(section, "macro16", "KEY_NONE"));
}

void	config_save_kbd_conf(CONFIG *conf, char *section)
{
	set_config_string(section, "up", input_whois(conf->up));
	set_config_string(section, "down", input_whois(conf->down));
	set_config_string(section, "left", input_whois(conf->left));
	set_config_string(section, "right", input_whois(conf->right));
	set_config_string(section, "a", input_whois(conf->a));
	set_config_string(section, "b", input_whois(conf->b));
	set_config_string(section, "c", input_whois(conf->c));
	set_config_string(section, "d", input_whois(conf->d));
	set_config_string(section, "start", input_whois(conf->start));
	set_config_string(section, "select", input_whois(conf->select));
	set_config_string(section, "macro01", input_whois(conf->macro_01));
	set_config_string(section, "macro02", input_whois(conf->macro_02));
	set_config_string(section, "macro03", input_whois(conf->macro_03));
	set_config_string(section, "macro04", input_whois(conf->macro_04));
	set_config_string(section, "macro05", input_whois(conf->macro_05));
	set_config_string(section, "macro06", input_whois(conf->macro_06));
	set_config_string(section, "macro07", input_whois(conf->macro_07));
	set_config_string(section, "macro08", input_whois(conf->macro_08));
	set_config_string(section, "macro09", input_whois(conf->macro_09));
	set_config_string(section, "macro10", input_whois(conf->macro_10));
	set_config_string(section, "macro11", input_whois(conf->macro_11));
	set_config_string(section, "macro12", input_whois(conf->macro_12));
	set_config_string(section, "macro13", input_whois(conf->macro_13));
	set_config_string(section, "macro14", input_whois(conf->macro_14));
	set_config_string(section, "macro15", input_whois(conf->macro_15));
	set_config_string(section, "macro16", input_whois(conf->macro_16));
}

void	config_read_joy_conf(CONFIG *conf, char *section)
{
	conf->a = get_config_int(section, "a", 0);
	conf->b = get_config_int(section, "b", 1);
	conf->c = get_config_int(section, "c", 2);
	conf->d = get_config_int(section, "d", 3);
	conf->start = get_config_int(section, "start", 4);
	conf->select = get_config_int(section, "select", 5);
	conf->macro_01 = get_config_int(section, "macro01", 6);
	conf->macro_02 = get_config_int(section, "macro02", 7);
	conf->macro_03 = get_config_int(section, "macro03", 8);
	conf->macro_04 = get_config_int(section, "macro04", 9);
	conf->macro_05 = get_config_int(section, "macro05", 10);
	conf->macro_06 = get_config_int(section, "macro06", 11);
	conf->macro_07 = get_config_int(section, "macro07", 12);
	conf->macro_08 = get_config_int(section, "macro08", 13);
	conf->macro_09 = get_config_int(section, "macro09", 14);
	conf->macro_10 = get_config_int(section, "macro10", 15);
	conf->macro_11 = get_config_int(section, "macro11", 16);
	conf->macro_12 = get_config_int(section, "macro12", 17);
	conf->macro_13 = get_config_int(section, "macro13", 18);
	conf->macro_14 = get_config_int(section, "macro14", 19);
	conf->macro_15 = get_config_int(section, "macro15", 20);
	conf->macro_16 = get_config_int(section, "macro16", 21);
}

void	config_save_joy_conf(CONFIG *conf, char *section)
{
	set_config_int(section, "a", conf->a);
	set_config_int(section, "b", conf->b);
	set_config_int(section, "c", conf->c);
	set_config_int(section, "d", conf->d);
	set_config_int(section, "start", conf->start);
	set_config_int(section, "select", conf->select);
	set_config_int(section, "macro01", conf->macro_01);
	set_config_int(section, "macro02", conf->macro_02);
	set_config_int(section, "macro03", conf->macro_03);
	set_config_int(section, "macro04", conf->macro_04);
	set_config_int(section, "macro05", conf->macro_05);
	set_config_int(section, "macro06", conf->macro_06);
	set_config_int(section, "macro07", conf->macro_07);
	set_config_int(section, "macro08", conf->macro_08);
	set_config_int(section, "macro09", conf->macro_09);
	set_config_int(section, "macro10", conf->macro_10);
	set_config_int(section, "macro11", conf->macro_11);
	set_config_int(section, "macro12", conf->macro_12);
	set_config_int(section, "macro13", conf->macro_13);
	set_config_int(section, "macro14", conf->macro_14);
	set_config_int(section, "macro15", conf->macro_15);
	set_config_int(section, "macro16", conf->macro_16);
}

void	config_set_var_kbd(CONFIG *conf, int var, int val)
{
	*( ((int *)conf) + var ) = val;
}

int		config_get_var_kbd(CONFIG *conf, int var)
{
	return *( ((int *)conf) + var );
}

void	config_set_var_joy(CONFIG *conf, int var, int val)
{
	*( ((int *)conf) + var + 4 ) = val;
}

int		config_get_var_joy(CONFIG *conf, int var)
{
	return *( ((int *)conf) + var + 4 );
}

int		val_to_index(int val)
{
	int	i;
	
	for(i=0;i<96;i++)
	{
		if (keydefs[i].key_scancode == val)
			return i;
	}
	
	return 0;
}
