/**************************************
****  CONFIG.H  - Config Handling  ****
****         Header File           ****
**************************************/

#ifndef	ConfigH
#define ConfigH

typedef struct {
	int	up;
	int down;
	int left;
	int right;
	int	a;
	int	b;
	int	c;
	int	d;
	int	start;
	int	select;
	int	macro_01;
	int	macro_02;
	int	macro_03;
	int	macro_04;
	int	macro_05;
	int	macro_06;
	int	macro_07;
	int	macro_08;
	int	macro_09;
	int	macro_10;
	int	macro_11;
	int	macro_12;
	int	macro_13;
	int	macro_14;
	int	macro_15;
	int	macro_16;
} CONFIG;

//-- Exported Variables ------------------------------------------------------
extern int				config_vsync;
extern int				config_scanlines;
extern CONFIG			kbd_p1;
extern CONFIG	 		kbd_p2;
extern CONFIG			joy_p1;
extern CONFIG			joy_p2;
extern unsigned char	config_game_name[80];

//-- Exported Functions ------------------------------------------------------
extern void		config_read(void);
extern void		config_read_name(void);
extern void		config_read_kbd_conf(CONFIG *, char *);
extern void		config_save_kbd_conf(CONFIG *, char *);
extern void		config_read_joy_conf(CONFIG *, char *);
extern void		config_save_joy_conf(CONFIG *, char *);
extern void		config_set_var_kbd(CONFIG *conf, int var, int val);
extern int		config_get_var_kbd(CONFIG *conf, int var);
extern void		config_set_var_joy(CONFIG *conf, int var, int val);
extern int		config_get_var_joy(CONFIG *conf, int var);
extern int		val_to_index(int val);

#endif