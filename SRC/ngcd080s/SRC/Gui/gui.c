
#include <stdlib.h>
#include <stdio.h>
#include <allegro.h>
#include "rsc.h"

char *	strncpy(char *_s1, const char *_s2, size_t _n);

/* typedef for the listbox callback functions */
typedef char *(*getfuncptr)(int, int *);

int		d_neo_radio_proc(int msg, DIALOG *d, int c);
int		d_neo_button_proc(int msg, DIALOG *d, int c);
int		d_neo_list_proc(int msg, DIALOG *d, int c);
int		d_neo_border_proc(int msg, DIALOG *d, int c);
int 	d_neo_check_proc(int msg, DIALOG *d, int c);
int 	d_neo_fbox_proc(int msg, DIALOG *d, int c);
int 	d_neo_gbox_proc(int msg, DIALOG *d, int c);
int 	d_neo_slider_proc(int msg, DIALOG *d, int c);
void	neo_draw_scrollable_frame(DIALOG *d, int listsize, int offset, int height, int fg_color, int bg);
int		neo_alert1(char *t, char *s1, char *s2, char *s3, char *b1, int r1);
int		neo_alert2(char *t, char *s1, char *s2, char *s3, char *b1, char *b2, int r1, int r2);
int		neo_alert3(char *t, char *s1, char *s2, char *s3, char *b1, char *b2, char *b3, int r1, int r2, int r3);
int		neo_alert_common1(char *t, char *s1, char *s2, char *s3);
void	neo_alert_common2(int maxlen, int butlen, int x);

// Changes 'x' into CTRL+'x'
#define C(x)      (x - 'a' + 1)

DIALOG neo_alert[] =
{
   /* (dialog proc)     (x)  (y)  (w)  (h)    (fg)    (bg)  (key)  (flags)   (d1)  (d2)            (dp) */
   { d_neo_border_proc,   0,   0, 200,  67, 0x0000, 0x0000,      0,      0,     0,    0,            NULL},
   { d_neo_fbox_proc,     2,   2, 196,   8, 0x0000, 0x10E3,      0,      0,     0,    0,            NULL},
   { d_text_proc,        10,   3,   0,   0, 0xFFFF, 0x10E3,      0,      0,     0,    0,            NULL},
   { d_text_proc,        10,  18,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,            NULL},
   { d_text_proc,        10,  26,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,            NULL},
   { d_text_proc,        10,  34,   0,   0, 0xFFFF, 0x9596,      0,      0,     0,    0,            NULL},
   { d_neo_button_proc,   8,  49,  58,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,            NULL},
   { d_neo_button_proc,  71,  49,  58,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,            NULL},
   { d_neo_button_proc, 134,  49,  58,  15, 0x0000, 0x0000,      0, D_EXIT,     0,    0,            NULL},
   { NULL,                0,   0,   0,   0, 0x0000, 0x0000,      0,      0,     0,    0,            NULL}
};

int d_neo_radio_proc(int msg, DIALOG *d, int c)
{
   int x, ret, fg;

   switch(msg) {

     case MSG_DRAW:
		 fg = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
		 text_mode(d->bg);
		 gui_textout(screen, d->dp, d->x+d->h+text_height(font), d->y+(d->h-(text_height(font)-gui_font_baseline))/2, (d->flags & D_DISABLED) ? gui_mg_color : 0xFFFF, FALSE);
	
		 x = d->x;
		 rectfill(screen, x+1, d->y+1, x+d->h-1, d->y+d->h-1, 0x9596);
		 
		 hline(screen, x, d->y, x+d->h, 0xC71C);
		 hline(screen, x, d->y+d->h, x+d->h, 0x328A);
		 vline(screen, x, d->y, d->y+d->h-1, 0xC71C);
		 vline(screen, x+d->h, d->y, d->y+d->h, 0x328A);
		 putpixel(screen, x, d->y, 0xFFFF);
		 putpixel(screen, x+d->h, d->y+d->h, 0x10E3);
		 
	     if (d->flags & D_SELECTED)
			rectfill(screen, x+2, d->y+2, x+d->h-2, d->y+d->h-2, 0xC71C);
	
		 return D_O_K;
	
     case MSG_KEY:
     case MSG_CLICK:
		 if (d->flags & D_SELECTED) {
		    return D_O_K;
		 }
	      break;

     case MSG_RADIO:
		 if ((c == d->d1) && (d->flags & D_SELECTED)) {
		    d->flags &= ~D_SELECTED;
		    scare_mouse();
		    SEND_MESSAGE(d, MSG_DRAW, 0);
		    unscare_mouse();
		 }
		 break;
   }

   ret = d_button_proc(msg, d, 0);

   if (((msg==MSG_KEY) || (msg==MSG_CLICK)) &&
       (d->flags & D_SELECTED) && (!(d->flags & D_EXIT))) {
      d->flags &= ~D_SELECTED;
      broadcast_dialog_message(MSG_RADIO, d->d1);
      d->flags |= D_SELECTED;
   }

   return ret;
}

int d_neo_button_proc(int msg, DIALOG *d, int c)
{
	if (msg == MSG_DRAW)
	{
		rectfill(screen, d->x, d->y, d->x+d->w, d->y+d->h, 0x9596);
		
		if (d->flags & D_SELECTED)
		{
			hline(screen,         d->x,      d->y, d->x+d->w, 0x328A);
			hline(screen,       d->x+1, d->y+d->h, d->x+d->w, 0xC71C);
			vline(screen,         d->x,      d->y, d->y+d->h, 0x328A);
			vline(screen,    d->x+d->w,    d->y+1, d->y+d->h, 0xC71C);
			putpixel(screen,      d->x,      d->y,            0x10E3);
			putpixel(screen, d->x+d->w, d->y+d->h,            0xFFFF);
		}
		else
		{
			hline(screen,         d->x,      d->y,   d->x+d->w, 0xC71C);
			hline(screen,         d->x, d->y+d->h,   d->x+d->w, 0x328A);
			vline(screen,         d->x,      d->y, d->y+d->h-1, 0xC71C);
			vline(screen,    d->x+d->w,      d->y,   d->y+d->h, 0x328A);
			putpixel(screen,      d->x,      d->y,              0xFFFF);
			putpixel(screen, d->x+d->w, d->y+d->h,              0x10E3);
		}
		
		text_mode(-1);
		gui_textout(screen, d->dp, d->x+d->w/2, d->y+d->h/2-text_height(font)/2, (d->flags & D_DISABLED) ? gui_mg_color : 0xFFFF, TRUE);
	}
	else
		return d_button_proc(msg, d, 0);


	return D_O_K;
}

int	d_neo_list_proc(int msg, DIALOG *d, int c)
{
	int height, listsize, i, len, bar, x, y, w;
	int fg, bg;
	char *sel = d->dp2;
	char s[1024];

	if (msg == MSG_DRAW)
	{
		(*(getfuncptr)d->dp)(-1, &listsize);
		height = (d->h-3) / text_height(font);
		bar = (listsize > height);
		w = (bar ? d->w-14 : d->w-2);
		
		/* draw box contents */
		for (i=0; i<height; i++) {
			if (d->d2+i < listsize) {
				if (d->d2+i == d->d1) {
					fg = 0xFFFF;
		    		bg = 0xC71C;
				} 
				else if ((sel) && (sel[d->d2+i])) { 
					fg = 0xFFFF;
		    		bg = 0xC71C;
				}
				else {
			    	fg = 0xFFFF;
		    		bg = 0x9596;
				}
				strncpy(s, (*(getfuncptr)d->dp)(i+d->d2, NULL), 1023);
				x = d->x + 2;
				y = d->y + 2 + i*text_height(font);
				text_mode(bg);
				rectfill(screen, x, y, x+7, y+text_height(font)-1, bg); 
				x += 8;
				len = strlen(s);
				while (text_length(font, s) >= d->w - (bar ? 22 : 10)) {
			   		len--;
		   			s[len] = 0;
				}
				textout(screen, font, s, x, y, fg); 
				x += text_length(font, s);
				if (x <= d->x+w) 
		   		rectfill(screen, x, y, d->x+w, y+text_height(font)-1, bg);
		 	}
		 	else
		 		rectfill(screen, d->x+2,  d->y+2+i*text_height(font), 
					d->x+w, d->y+1+(i+1)*text_height(font), d->bg);
			}
		
		if (d->y+2+i*text_height(font) <= d->y+d->h-2)
		  rectfill(screen, d->x+2, d->y+2+i*text_height(font), 
					       d->x+w, d->y+d->h-2, d->bg);
		
		/* draw frame, maybe with scrollbar */
		neo_draw_scrollable_frame(d, listsize, d->d2, height, 0xC71C, 0x9596);
	}
	else
		d_list_proc(msg, d, 0);
	
	return	D_O_K;
}

void neo_draw_scrollable_frame(DIALOG *d, int listsize, int offset, int height, int fg_color, int bg)
{
	int i, len;
	BITMAP *pattern;
	int xx, yy;

	/* draw frame */
	hline(screen,         d->x,      d->y,   d->x+d->w, 0xC71C);
	hline(screen,         d->x, d->y+d->h,   d->x+d->w, 0x328A);
	vline(screen,         d->x,      d->y, d->y+d->h-1, 0xC71C);
	vline(screen,    d->x+d->w,      d->y,   d->y+d->h, 0x328A);
	putpixel(screen,      d->x,      d->y,              0xFFFF);
	putpixel(screen, d->x+d->w, d->y+d->h,              0x10E3);
	rect(screen, d->x+1, d->y+1, d->x+d->w-1, d->y+d->h-1, 0x9596);

	if (listsize > height)
	{
		/* possibly draw scrollbar */
		vline(screen, d->x+d->w-12, d->y+1, d->y+d->h-1, 0xC71C);
		vline(screen, d->x+d->w-13, d->y+1, d->y+d->h-1, 0x328A);

		rect(screen, d->x+1, d->y+1, d->x+d->w-14, d->y+d->h-1, 0x9596);
		rect(screen, d->x+d->w-11, d->y+1, d->x+d->w-1, d->y+d->h-1, 0x9596);

		/* create and draw the scrollbar */
		pattern = create_bitmap(2, 2);
		i = ((d->h-4) * height + listsize/2) / listsize;
		xx = d->x+d->w-10;
		yy = d->y+2;

		putpixel(pattern, 0, 1, bg);
		putpixel(pattern, 1, 0, bg);
		putpixel(pattern, 0, 0, fg_color);
		putpixel(pattern, 1, 1, fg_color);

		if (offset > 0) {
			len = (((d->h-4) * offset) + listsize/2) / listsize;
			rectfill(screen, xx, yy, xx+8, yy+len-1, 0x9596);
			yy += len;
		}
		if (yy+i < d->y+d->h-2) {
			drawing_mode(DRAW_MODE_COPY_PATTERN, pattern, 0, 0);
			rectfill(screen, xx, yy, xx+8, yy+i, 0);
			solid_mode();
			yy += i;
			rectfill(screen, xx, yy, xx+8, d->y+d->h-2, bg);
		} else {
			drawing_mode(DRAW_MODE_COPY_PATTERN, pattern, 0, 0);
			rectfill(screen, xx, yy, xx+8, d->y+d->h-2, 0);
			solid_mode();
		}
		
		destroy_bitmap(pattern);
	}
}

int	d_neo_border_proc(int msg, DIALOG *d, int c)
{
	if (msg == MSG_DRAW)
	{
		rectfill(screen, d->x, d->y, d->x+d->w,   d->y+d->h, 0x9596);
		hline(screen,          d->x,      d->y,   d->x+d->w, 0xC71C);
		hline(screen,          d->x, d->y+d->h,   d->x+d->w, 0x328A);
		vline(screen,          d->x,      d->y, d->y+d->h-1, 0xC71C);
		vline(screen,     d->x+d->w,      d->y,   d->y+d->h, 0x328A);
		putpixel(screen,       d->x,      d->y,              0xFFFF);
		putpixel(screen,  d->x+d->w, d->y+d->h,              0x10E3);
	}

	return D_O_K;
}

int d_neo_check_proc(int msg, DIALOG *d, int c)
{
	if (msg == MSG_DRAW)
	{
		 rectfill(screen, d->x+1, d->y+1, d->x+d->h-1, d->y+d->h-1, 0x9596);
		 
		 text_mode(d->bg);
		 gui_textout(screen, d->dp, d->x+d->h+text_height(font), d->y+(d->h-(text_height(font)-gui_font_baseline))/2, (d->flags & D_DISABLED) ? gui_mg_color : 0xFFFF, FALSE);

		 hline(screen, d->x, d->y, d->x+d->h, 0xC71C);
		 hline(screen, d->x, d->y+d->h, d->x+d->h, 0x328A);
		 vline(screen, d->x, d->y, d->y+d->h-1, 0xC71C);
		 vline(screen, d->x+d->h, d->y, d->y+d->h, 0x328A);
		 putpixel(screen, d->x, d->y, 0xFFFF);
		 putpixel(screen, d->x+d->h, d->y+d->h, 0x10E3);

	     if (d->flags & D_SELECTED)
	     {
	     	line(screen, d->x+2, d->y+2, d->x+d->h-2, d->y+d->h-2, 0x328A);
	     	line(screen, d->x+2, d->y+d->h-2, d->x+d->h-2, d->y+2, 0x328A);
		 }
	}
	else
		return d_button_proc(msg, d, 0);


	return D_O_K;
}

int d_neo_fbox_proc(int msg, DIALOG *d, int c)
{
	if (msg == MSG_DRAW)
		rectfill(screen, d->x, d->y, d->x+d->w, d->y+d->h, d->bg);
		
	return D_O_K;
}

int d_neo_gbox_proc(int msg, DIALOG *d, int c)
{
	if (msg == MSG_DRAW)
	{
		rect(screen, d->x+1, d->y+1,d->x+d->w, d->y+d->h, d->fg);
		rect(screen, d->x, d->y, d->x+d->w-1, d->y+d->h-1, d->bg);
		putpixel(screen, d->x, d->y+d->h, d->fg);
		putpixel(screen, d->x+d->w, d->y, d->fg);
		
		if (d->dp != NULL)
		{
			text_mode(0x9596);
			textout(screen, font, d->dp, d->x+6, d->y-(text_height(font)/2), 0xFFFF);
		}	
	}
	
	return D_O_K;
}

int d_neo_slider_proc(int msg, DIALOG *d, int c)
{
   BITMAP *slhan = NULL;
   int oldpos, newpos;
   int sfg;                /* slider foreground color */
   int vert = TRUE;        /* flag: is slider vertical? */
   int hh = 7;             /* handle height (width for horizontal sliders) */
   int hmar;               /* handle margin */
   int slp;                /* slider position */
   int mp;                 /* mouse position */
   int irange;
   int slx, sly, slh, slw;
   int msx, msy;
   int retval = D_O_K;
   int upkey, downkey;
   int pgupkey, pgdnkey;
   int homekey, endkey;
   int delta;
   fixed slratio, slmax, slpos;
   int (*proc)(void *cbpointer, int d2value);

   /* check for slider direction */
   if (d->h < d->w)
      vert = FALSE;

   /* set up the metrics for the control */
   if (d->dp != NULL) {
      slhan = (BITMAP *)d->dp;
      if (vert)
	 hh = slhan->h;
      else
	 hh = slhan->w;
   }

   hmar = hh/2;
   irange = (vert) ? d->h : d->w;
   slmax = itofix(irange-hh);
   slratio = slmax / (d->d1);
   slpos = slratio * d->d2;
   slp = fixtoi(slpos);

   switch (msg) {

      case MSG_DRAW:
	 sfg = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;

	 if (vert) {
	 	rectfill(screen, d->x, d->y, d->x+d->w, d->y+d->h, d->bg);
	    vline(screen, d->x+d->w/2-1, d->y, d->y+d->h-1, 0x328A);
	    vline(screen, d->x+d->w/2+1, d->y, d->y+d->h-1, 0xC71C);
	 	putpixel(screen, d->x+d->w/2, d->y, 0x328A);
	 	putpixel(screen, d->x+d->w/2, d->y+d->h-1, 0xC71C);
	    
	    /*
	    rectfill(screen, d->x, d->y, d->x+d->w/2-2, d->y+d->h, d->bg);
	    rectfill(screen, d->x+d->w/2-1, d->y, d->x+d->w/2+1, d->y+d->h, sfg);
	    rectfill(screen, d->x+d->w/2+2, d->y, d->x+d->w, d->y+d->h, d->bg);
	    */
	 }
	 else {
	 	rectfill(screen, d->x, d->y, d->x+d->w, d->y+d->h, d->bg);
	 	hline(screen, d->x, d->y+d->h/2-1, d->x+d->w-1, 0x328A);
	 	hline(screen, d->x, d->y+d->h/2+1, d->x+d->w-1, 0xC71C);
	 	putpixel(screen, d->x, d->y+d->h/2, 0x328A);
	 	putpixel(screen, d->x+d->w-1, d->y+d->h/2, 0xC71C);
	 	
	 	/*
	    rectfill(screen, d->x, d->y, d->x+d->w, d->y+d->h/2-2, d->bg);
	    rectfill(screen, d->x, d->y+d->h/2-1, d->x+d->w, d->y+d->h/2+1, sfg);
	    rectfill(screen, d->x, d->y+d->h/2+2, d->x+d->w, d->y+d->h, d->bg);
	    */
	 }

	 /* okay, background and slot are drawn, now draw the handle */
	 if (slhan) {
	    if (vert) {
	       slx = d->x+(d->w/2)-(slhan->w/2);
	       sly = d->y+d->h-(hh+slp);
	    } 
	    else {
	       slx = d->x+slp;
	       sly = d->y+(d->h/2)-(slhan->h/2);
	    }
	    draw_sprite(screen, slhan, slx, sly);
	 } 
	 else {
	    /* draw default handle */
	    if (vert) {
	       slx = d->x;
	       sly = d->y+d->h-(hh+slp);
	       slw = d->w;
	       slh = hh;
	    } else {
	       slx = d->x+slp;
	       sly = d->y;
	       slw = hh;
	       slh = d->h;
	    }

	    /* draw body */
	    rectfill(screen, slx+2, sly, slx+(slw-2), sly+slh, sfg);
	    vline(screen, slx+1, sly+1, sly+slh-1, sfg);
	    vline(screen, slx+slw-1, sly+1, sly+slh-1, sfg);
	    vline(screen, slx, sly+2, sly+slh-2, sfg);
	    vline(screen, slx+slw, sly+2, sly+slh-2, sfg);
	    vline(screen, slx+1, sly+2, sly+slh-2, d->bg);
	    hline(screen, slx+2, sly+1, slx+slw-2, d->bg);
	    putpixel(screen, slx+2, sly+2, d->bg);
	 }

	 break;

      case MSG_WANTFOCUS:
      case MSG_LOSTFOCUS:
	 return D_WANTFOCUS;

      case MSG_KEY:
	 if (!(d->flags & D_GOTFOCUS))
	    return D_WANTFOCUS;
	 else
	    return D_O_K;

      case MSG_CHAR:
	 /* handle movement keys to move slider */
	 c >>= 8;

	 if (vert) {
	    upkey = KEY_UP;
	    downkey = KEY_DOWN;
	    pgupkey = KEY_PGUP;
	    pgdnkey = KEY_PGDN;
	    homekey = KEY_END;
	    endkey = KEY_HOME;
	 } 
	 else {
	    upkey = KEY_RIGHT;
	    downkey = KEY_LEFT;
	    pgupkey = KEY_PGDN;
	    pgdnkey = KEY_PGUP;
	    homekey = KEY_HOME;
	    endkey = KEY_END;
	 }

	 if (c == upkey)
	    delta = 1;
	 else if (c == downkey)
	    delta = -1;
	 else if (c == pgdnkey)
	    delta = -d->d1 / 16;
	 else if (c == pgupkey)
	    delta = d->d1 / 16;
	 else if (c == homekey)
	    delta = -d->d2;
	 else if (c == endkey)
	    delta = d->d1 - d->d2;
	 else
	    delta = 0;

	 if (delta) {
	    oldpos = slp;

	    while (1) {
	       d->d2 = d->d2+delta;
	       slpos = slratio*d->d2;
	       slp = fixtoi(slpos);
	       if ((slp != oldpos) || (d->d2 <= 0) || (d->d2 >= d->d1))
		  break;
	    }

	    if (d->d2 < 0)
	       d->d2 = 0;
	    if (d->d2 > d->d1)
	       d->d2 = d->d1;

	    retval = D_USED_CHAR;

	    /* call callback function here */
	    if (d->dp2) {
	       proc = d->dp2;
	       retval |= (*proc)(d->dp3, d->d2);
	    }

	    scare_mouse();
	    SEND_MESSAGE(d, MSG_DRAW, 0);
	    unscare_mouse();
	 }
	 break;

      case MSG_CLICK:
	 /* track the mouse until it is released */
	 mp = slp;

	 while (gui_mouse_b()) {
	    msx = gui_mouse_x();
	    msy = gui_mouse_y();
	    oldpos = d->d2;
	    if (vert)
	       mp = (d->y+d->h-hmar)-msy;
	    else
	       mp = msx-(d->x+hmar);
	    if (mp < 0)
	       mp = 0;
	    if (mp > irange-hh)
	       mp = irange-hh;
	    slpos = itofix(mp);
	    slmax = fdiv(slpos, slratio);
	    newpos = fixtoi(slmax);
	    if (newpos != oldpos) {
	       d->d2 = newpos;

	       /* call callback function here */
	       if (d->dp2 != NULL) {
		  proc = d->dp2;
		  retval |= (*proc)(d->dp3, d->d2);
	       }

	       if (d->d2 != oldpos) {
		  scare_mouse();
		  SEND_MESSAGE(d, MSG_DRAW, 0);
		  unscare_mouse();
	       }
	    }

	    /* let other objects continue to animate */
	    broadcast_dialog_message(MSG_IDLE, 0);
	 }
	 break;
   }

   return retval;
}

int	neo_alert3(char *t, char *s1, char *s2, char *s3, char *b1, char *b2, char *b3, int r1, int r2, int r3)
{
	int	maxlen, len, butlen;
	
	if (s1 == NULL)
		s1 = "";

	if (s2 == NULL)
		s2 = "";

	if (s3 == NULL)
		s3 = "";
	
	maxlen = neo_alert_common1(t, s1, s2, s3);
	
	butlen = text_length(font, b1) + 10;
	
	len = text_length(font, b2) + 10;
	if (len > butlen)
		butlen = len;
	
	len = text_length(font, b3) + 10;
	if (len > butlen)
		butlen = len;
	
	len = (3*butlen) + 30;
	if (len > maxlen)
		maxlen = len;
	
	neo_alert[6].dp = b1;
	neo_alert[7].dp = b2;
	neo_alert[8].dp = b3;

	neo_alert[6].key = r1;
	neo_alert[7].key = r2;
	neo_alert[8].key = r3;

	neo_alert[6].flags = D_EXIT;
	neo_alert[7].flags = D_EXIT;
	neo_alert[8].flags = D_EXIT;

	len = (maxlen - ( (3*butlen) + 10) ) / 2;
	
	neo_alert_common2(maxlen, butlen, len);
	
	centre_dialog(neo_alert);
	return (popup_dialog(neo_alert, -1) - 6);
}

int	neo_alert2(char *t, char *s1, char *s2, char *s3, char *b1, char *b2, int r1, int r2)
{
	int	maxlen, len, butlen;
	
	if (s1 == NULL)
		s1 = "";

	if (s2 == NULL)
		s2 = "";

	if (s3 == NULL)
		s3 = "";

	maxlen = neo_alert_common1(t, s1, s2, s3);
	
	butlen = text_length(font, b1) + 10;
	
	len = text_length(font, b2) + 10;
	if (len > butlen)
		butlen = len;
	
	len = (2*butlen) + 25;
	if (len > maxlen)
		maxlen = len;
	
	neo_alert[6].dp = b1;
	neo_alert[7].dp = b2;
	neo_alert[8].dp = NULL;

	neo_alert[6].key = r1;
	neo_alert[7].key = r2;
	neo_alert[8].key = 0;

	neo_alert[6].flags = D_EXIT;
	neo_alert[7].flags = D_EXIT;
	neo_alert[8].flags = D_HIDDEN;

	len = (maxlen - ( (2*butlen) + 5) ) / 2;
	
	neo_alert_common2(maxlen, butlen, len);
	
	centre_dialog(neo_alert);
	return (popup_dialog(neo_alert, -1) - 6);
}

int	neo_alert1(char *t, char *s1, char *s2, char *s3, char *b1, int r1)
{
	int	maxlen, len, butlen;
	
	if (s1 == NULL)
		s1 = "";

	if (s2 == NULL)
		s2 = "";

	if (s3 == NULL)
		s3 = "";

	maxlen = neo_alert_common1(t, s1, s2, s3);
	butlen = text_length(font, b1) + 10;
	
	len = butlen + 20;
	if (len > maxlen)
		maxlen = len;
	
	neo_alert[6].dp = b1;
	neo_alert[7].dp = NULL;
	neo_alert[8].dp = NULL;

	neo_alert[6].key = r1;
	neo_alert[7].key = 0;
	neo_alert[8].key = 0;

	neo_alert[6].flags = D_EXIT;
	neo_alert[7].flags = D_HIDDEN;
	neo_alert[8].flags = D_HIDDEN;

	len = ((maxlen - butlen) / 2);
	
	neo_alert_common2(maxlen, butlen, len);
	
	centre_dialog(neo_alert);
	return (popup_dialog(neo_alert, -1) - 6);
}

int neo_alert_common1(char *t, char *s1, char *s2, char *s3)
{
	int	maxlen, len;
	
	maxlen = text_length(font, t) + 20;
	
	len = text_length(font, s1) + 20;
	if (len > maxlen)
		maxlen = len;
	
	if (s2 != NULL)
	{
		len = text_length(font, s2) + 20;
		if (len > maxlen)
			maxlen = len;
	}
	
	if (s3 != NULL)
	{
		len = text_length(font, s3) + 20;
		if (len > maxlen)
			maxlen = len;
	}
	
	neo_alert[2].dp = t;
	neo_alert[3].dp = s1;
	neo_alert[4].dp = s2;
	neo_alert[5].dp = s3;
	
	return	maxlen;
}

void neo_alert_common2(int maxlen, int butlen, int x)
{	
	neo_alert[0].x = neo_alert[0].y = 0;
	neo_alert[0].w = maxlen;

	neo_alert[1].x = neo_alert[1].y = 2;
	neo_alert[1].w = maxlen - 4;
	
	neo_alert[2].x = 10;
	neo_alert[2].y = 3;

	neo_alert[3].x = 10;
	neo_alert[3].y = 18;

	neo_alert[4].x = 10;
	neo_alert[4].y = 26;

	neo_alert[5].x = 10;
	neo_alert[5].y = 34;

	neo_alert[6].x = x;
	neo_alert[6].y = 49;
	neo_alert[6].w = butlen;
	x += (butlen + 5);
	
	neo_alert[7].x = neo_alert[7].flags == D_HIDDEN ? 0 : x;
	neo_alert[7].y = 49;
	neo_alert[7].w = butlen;
	x += (butlen + 5);

	neo_alert[8].x = neo_alert[8].flags == D_HIDDEN ? 0 : x;
	neo_alert[8].y = 49;
	neo_alert[8].w = butlen;
}

int d_neo_dip_sw(int msg, DIALOG *d, int c)
{
	int	i, x, y;

	switch (msg) {
	
	case MSG_DRAW:
		rectfill(screen, d->x, d->y, d->x+112, d->y+14, 0);
		rect(screen, d->x+1, d->y+1, d->x+111, d->y+13, 0x9596);

		hline(screen, d->x, d->y, d->x+112, 0xC71C);
		hline(screen, d->x+2, d->y+2, d->x+110, 0x328A);

		hline(screen, d->x, d->y+14, d->x+112, 0x328A);
		hline(screen, d->x+2, d->y+12, d->x+110, 0xC71C);
		
		vline(screen, d->x, d->y, d->y+13, 0xC71C);
		vline(screen, d->x+2, d->y+2, d->y+11, 0x328A);

		vline(screen, d->x+112, d->y, d->y+14, 0x328A);
		vline(screen, d->x+110, d->y+2, d->y+12, 0xC71C);
		
		putpixel(screen, d->x, d->y, 0xFFFF);
		putpixel(screen, d->x+112, d->y+14, 0x10E3);

		x = d->x+5;
		y = d->y+4;
		
		text_mode(0);
		for(i=7;i>=0;i--)
		{
			gui_textout(screen, ((d->d1>>i)&1) ? "1" : "0", x, y, 0xFFFF, FALSE);
			x += 14;
		}
				
		break;

	case MSG_CLICK:
		x = gui_mouse_x() - d->x;
		y = gui_mouse_y();
		
		if ((x>3)&&(x<110))
			d->d1 ^= 128>>((x-3)/14);
					
		scare_mouse();
		SEND_MESSAGE(d, MSG_DRAW, 0);
		unscare_mouse();
		
		while (gui_mouse_b())
	    	broadcast_dialog_message(MSG_IDLE, 0);
	 break;
	}

	return D_O_K;	
}	