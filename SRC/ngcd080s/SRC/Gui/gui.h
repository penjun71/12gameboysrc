extern int	d_neo_radio_proc(int msg, DIALOG *d, int c);
extern int	d_neo_button_proc(int msg, DIALOG *d, int c);
extern int	d_neo_list_proc(int msg, DIALOG *d, int c);
extern int	d_neo_border_proc(int msg, DIALOG *d, int c);
extern int 	d_neo_check_proc(int msg, DIALOG *d, int c);
extern int 	d_neo_fbox_proc(int msg, DIALOG *d, int c);
extern int 	d_neo_gbox_proc(int msg, DIALOG *d, int c);
extern int 	d_neo_slider_proc(int msg, DIALOG *d, int c);
extern int 	d_neo_dip_sw(int msg, DIALOG *d, int c);
extern void	neo_draw_scrollable_frame(DIALOG *d, int listsize, int offset, int height, int fg_color, int bg);
extern int	neo_alert1(char *t, char *s1, char *s2, char *s3, char *b1, int r1);
extern int	neo_alert2(char *t, char *s1, char *s2, char *s3, char *b1, char *b2, int r1, int r2);
extern int	neo_alert3(char *t, char *s1, char *s2, char *s3, char *b1, char *b2, char *b3, int r1, int r2, int r3);
extern int	neo_alert_common1(char *t, char *s1, char *s2, char *s3);
extern void	neo_alert_common2(int maxlen, int butlen, int x);

#define CENTRE_ITEM(a, b, c) (a)[(b)].x = (a)[0].x + ((a)[0].w>>1) - ((c)>>1)

