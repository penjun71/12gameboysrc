; *******************************************
; **** Fixed Text Layer Drawing Routines ****
; *******************************************

	%include ".\src\helper.asm"	

	BITS	32
	
	GLOBAL	_video_draw_fix
	
	EXTERN	_video_vidram
	EXTERN	_video_fix_usage
	EXTERN	_video_line_ptr
	EXTERN	_video_paletteram_pc
	EXTERN	_neogeo_fix_memory
	
	%macro	PUT_FIX 0
	mov	ebp, edx
	shr	edx, 4
	and	ebp, 0Fh
	jz	%%no_fix
	mov	bp, [eax + ebp * 2]
	mov	[edi], bp
%%no_fix
	add	edi, 2
	%endmacro

	SECTION	.text

	AlignFunc
	
_video_draw_fix:
	pushad
	
	mov	ecx, 28
	mov	edi, [_video_line_ptr]
	add	edi, 16
	mov	esi, [_video_vidram]
	add	esi, 0xE044

	AlignFunc
		
draw_fix_loop1:
	push	ecx
	mov	ecx, 38

	AlignFunc

draw_fix_loop2:
	mov	ax, [esi]
	add	esi, 0x40
	mov	bx, ax
	and	eax, 0F000h
	and	ebx, 0FFFh
	mov	edx, ebx
	add	edx, _video_fix_usage
	test	byte [edx], 0FFh
	jnz	thing_to_do
	add	edi, 16
	jmp	nothing_to_do
thing_to_do:
	shl	ebx, 5
	add	ebx, [_neogeo_fix_memory]
	shr	eax, 7
	add	eax, [_video_paletteram_pc]

	%rep	8
	mov	edx, [ebx]

	PUT_FIX
	PUT_FIX
	PUT_FIX
	PUT_FIX
	PUT_FIX
	PUT_FIX
	PUT_FIX
	PUT_FIX

	add	ebx, 4
	add	edi, 624
	%endrep

	sub	edi, 5104
nothing_to_do:
	dec	ecx
	jz	suite
	jmp	draw_fix_loop2

suite:
	pop	ecx
	add	edi, 4512
	sub	esi, 2430 ;2558
	dec	ecx
	jz	termine
	jmp	draw_fix_loop1
termine:
	popad
	ret

	AlignFunc
	
	
