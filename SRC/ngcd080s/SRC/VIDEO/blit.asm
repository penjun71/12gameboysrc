; ***************************************
; ****      Blitting Routines        ****
; ***************************************

	%include ".\src\helper.asm"	
	
	BITS	32
	
	GLOBAL	_neogeo_fast_clear
	GLOBAL	_neogeo_fast_copy
	GLOBAL	_neogeo_fast_copy_scanlines
	
	SECTION	.text
	
_neogeo_fast_clear:
	BEGIN
	PUSHL	eax
	PUSHL	edi
	PUSHL	ecx

	mov	edi, [esp + ARGS]
	mov	eax, [esp + ARGS + 4]

	mov	cx, ax
	shl	eax, 16
	mov	ax, cx
	cld

	%rep	224
	mov	ecx, 168
	rep	stosd
	%endrep

	pop	ecx
	pop	edi
	pop	eax
	ret

	%define	BMP_SEG		[edx + 60]
	%define BMP_WBANK	[edx + 32]
	%define BMP_RBANK	[edx + 36]
	
	%define B_SOURCE	[esp + ARGS + 0]
	%define B_DEST		[esp + ARGS + 4]
	%define B_SOURCE_X	[esp + ARGS + 8]
	%define B_SOURCE_Y	[esp + ARGS + 12]
	%define B_DEST_X	[esp + ARGS + 16]
	%define B_DEST_Y	[esp + ARGS + 20]
	%define B_WIDTH		[esp + ARGS + 24]
	%define B_HEIGHT	[esp + ARGS + 28]

	AlignFunc
	
_neogeo_fast_copy:
	BEGIN
	PUSHL	es
	PUSHL	edi
	PUSHL	esi
	PUSHL	ebx
	
	mov	edx, B_DEST
	mov	es, BMP_SEG
	mov	bx, ds
	cld
	shr	dword B_WIDTH, 1

	AlignFunc
	
blit_loop:
	mov	edx, B_DEST
	mov	eax, B_DEST_Y
	call	BMP_WBANK
	mov	edi, B_DEST_X
	lea	edi, [eax + edi * 2]
	mov	edx, B_SOURCE
	mov	eax, B_SOURCE_Y
	call	BMP_RBANK
	mov	esi, B_SOURCE_X
	lea	esi, [eax + esi * 2]
	mov	ecx, B_WIDTH
	mov	ds, BMP_SEG
	rep	movsd
	mov	ds, bx
	inc	dword B_SOURCE_Y
	inc	dword B_DEST_Y
	dec	dword B_HEIGHT
	jg	blit_loop
	
	pop	ebx
	pop	esi
	pop	edi
	pop	es

	ret

	AlignFunc
	
_neogeo_fast_copy_scanlines:
	BEGIN
	PUSHL	es
	PUSHL	edi
	PUSHL	esi
	PUSHL	ebx
	
	mov	edx, B_DEST
	mov	es, BMP_SEG
	mov	bx, ds
	cld
	shr	dword B_WIDTH, 1

	AlignFunc
	
blit_loop2:
	mov	edx, B_DEST
	mov	eax, B_DEST_Y
	call	BMP_WBANK
	mov	edi, B_DEST_X
	lea	edi, [eax + edi * 2]
	mov	edx, B_SOURCE
	mov	eax, B_SOURCE_Y
	call	BMP_RBANK
	mov	esi, B_SOURCE_X
	lea	esi, [eax + esi * 2]
	mov	ecx, B_WIDTH
	mov	ds, BMP_SEG
	rep	movsd
	mov	ds, bx
	inc	dword B_SOURCE_Y
	add	dword B_DEST_Y, 2
	dec	dword B_HEIGHT
	jg	blit_loop2
	
	pop	ebx
	pop	esi
	pop	edi
	pop	es
	ret

	AlignFunc
	
