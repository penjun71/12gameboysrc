; ***************************************
; **** Helper macros for asm sources ****
; ***************************************

	%macro BEGIN 		0
	%define ARGS 4
	%endmacro
	
	%macro PUSHL 		1
	push %1
	%assign ARGS ARGS + 4
	%endmacro
	
	%macro PUSHW 		1
	push %1
	%assign ARGS ARGS + 2
	%endmacro
	
	%macro AlignFunc 	0
	ALIGN	8
	%endmacro
	
	%macro AlignData 	0
	ALIGN	4
	%endmacro
	
	%macro	BeginMemMap	0
	%define	ZoneCount 0
MemoryMap:
	%endmacro
	
	%macro	DeclareZone	3
	dd	%1, %2, %3
	%assign ZoneCount ZoneCount+1
	%endmacro
	
	%macro	EndMemMap	0
	dd	0, 0, 0, 0
	%endmacro

	%macro	ILLEGAL		0
	db	0Fh, 0FFh
	%endmacro