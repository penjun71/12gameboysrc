/************************
*** Z80 CPU Interface ***
***    Header File    ***
************************/

#ifndef	Z80INTRF_H
#define Z80INRTF_H

#include "../z80/mz80.h"

extern void z80_init(void);
extern int sound_code;
extern UINT8 subcpu_memspace[65536];
extern int z80_cycles;

#endif

