/************************
*** Z80 CPU Interface ***
************************/

//-- Include Files ----------------------------------------------------------
#include	"../ym2610/ym2610.h"
#include 	<stdio.h>
#include 	<stdlib.h>
#include	"mz80.h"

//-- Exported Functions -----------------------------------------------------
void	PortWrite(UINT16 PortNo, UINT8 Value, struct z80PortWrite *ptr);
UINT16	PortRead(UINT16 PortNo, struct z80PortRead *ptr);
UINT16 PortRead2(UINT16 PortNo, struct z80PortRead *ptr);

//-- Structures -------------------------------------------------------------
struct	z80PortRead ReadPorts[] =
{
	{0x0000,		0xffff,		PortRead},
	{(UINT16)-1,	(UINT16)-1,	NULL}
};

struct	z80PortWrite WritePorts[] =
{
	{0x0000,		0xffff,		PortWrite},
	{(UINT16)-1,	(UINT16)-1,	NULL}
};

struct MemoryReadByte	MemRead[] =
{
	{(UINT16)-1,	(UINT16)-1,	NULL}
};

struct MemoryWriteByte	MemWrite[] =
{
	{(UINT16)-1,	(UINT16)-1,	NULL}
};

//-- Variables --------------------------------------------------------------
CONTEXTMZ80	subcpu_context;
UINT8		subcpu_memspace[65536];
int			sound_code = 0;
int			pending_command = 0;
int			result_code = 0;
int			z80_cycles = Z80_VBL_CYCLES;

//---------------------------------------------------------------------------
void z80_init(void)
{
	subcpu_context.z80Base = subcpu_memspace;
	
	subcpu_context.z80IoRead  = ReadPorts;
	subcpu_context.z80IoWrite = WritePorts;
	
	subcpu_context.z80MemRead = MemRead;
	subcpu_context.z80MemWrite = MemWrite;
	
	mz80SetContext((void *)&subcpu_context);
	
	mz80reset();

	// Let Z80 do its initialization
	mz80exec(100000);
}

//---------------------------------------------------------------------------
void PortWrite(UINT16 PortNo, UINT8	Value, struct z80PortWrite *ptr)
{
	FILE	*fp;

/*	fp = fopen("C:\\TMP\\Z80.LOG", "at");
	fprintf(fp, "Write: %02X %02X\n", PortNo, Value);
	fclose(fp);*/

	switch( PortNo )
	{
	case	0x4:
		YM2610Write(0, Value);
		break;
	
	case	0x5:
		YM2610Write(1, Value);
		break;
	
	case	0x6:
		YM2610Write(2, Value);
		break;
	
	case	0x7:
		YM2610Write(3, Value);
		break;
	
	case	0xC:
		result_code = Value;
		break;
	}
}

//---------------------------------------------------------------------------
UINT16 PortRead(UINT16 PortNo, struct z80PortRead *ptr)
{
	int	Value;
	FILE	*fp;
	
	Value = PortRead2(PortNo, ptr);
	
/*	fp = fopen("C:\\TMP\\Z80.LOG", "at");
	fprintf(fp, "Read: %02X %02X\n", PortNo, Value);
	fclose(fp);*/
	
	return Value;
}

UINT16 PortRead2(UINT16 PortNo, struct z80PortRead *ptr)
{
	switch( PortNo )
	{
	case	0x0:
		pending_command = 0;
		return sound_code;
		break;
	
	case	0x4:
		return rand()&0x83;//YM2610Read(0);
		break;
	
	case	0x5:
		return YM2610Read(1);
		break;
	
	case	0x6:
		return YM2610Read(2);
		break;
	};
	
	return 0;
}
