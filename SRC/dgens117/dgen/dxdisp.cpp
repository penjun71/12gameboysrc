#include <stdio.h>
#include <windows.h>
#include <ddraw.h>
#include "d.h"

// MD DirectX Display stuff

dxdisp::dxdisp(HWND iwnd,int ifullscreen)
{
  dd=NULL; prim=buf=NULL; clipper=NULL;
  wnd=iwnd; bm.data=NULL; fullscreen=ifullscreen;
  directdraw_init();
}

dxdisp::~dxdisp()
{
  directdraw_exit();
  wnd=0; bm.data=NULL;
}

// Main MD Screen Window

// This gets called until DirectDraw calls are completed
// clipper isn't vital
#define DIRECTDRAW_READY (dd&&prim&&buf)
int dxdisp::directdraw_init()
{
  int ret=0;
  dprintf ("directdraw_init()\n");

  if (dd==NULL)
  {
    ret = DirectDrawCreate( NULL, &dd, NULL );
    if (ret!=DD_OK)
    {
      dprintf ("DirectDrawCreate returned %x\n",ret);
      return 1;
    }
  }
  if (dd!=NULL)
  {
    if (fullscreen)
    {
      ret=dd->SetCooperativeLevel(wnd, DDSCL_ALLOWREBOOT | DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWMODEX);
      if (ret!=DD_OK)
      {
        dprintf ("SetCooperativeLevel(EXCLUSIVE) returned %x\n",ret);
        fullscreen=0;
      }
    }

    if (!fullscreen)
    {
      ret=dd->SetCooperativeLevel(wnd, DDSCL_NORMAL);
      if (ret!=DD_OK)
      {
        dprintf ("SetCooperativeLevel returned %x\n",ret);
        return 1;
      }
    }

    if (fullscreen)
    {
      ret = dd->SetDisplayMode(320,240,16);
      if (ret!=DD_OK)
        dprintf ("SetDisplayMode() return value was %x (%+-d)\n",
          ret,ret-MAKE_DDHRESULT(0));
    }

    if (prim==NULL) prim=create_primary_surface(dd);
    if (prim==NULL) return 1;

    if (buf==NULL)  buf=create_surface(dd,320+16,224+16);
    if (buf==NULL)  return 1;

    if (!fullscreen)
      if (prim!=NULL)
        if (clipper==NULL) clipper=attach_clipper(dd,wnd,prim);
  }
  return 0;
}

int dxdisp::directdraw_exit()
{
  dprintf ("directdraw_exit()\n");
  // Release DirectDraw when we are destroyed
  if (clipper  !=NULL) clipper->Release(); clipper=NULL;
  if (buf      !=NULL) buf    ->Release(); buf    =NULL;
  if (prim     !=NULL) prim   ->Release(); prim   =NULL;
  if (dd       !=NULL) dd     ->Release(); dd     =NULL;
  return 0;
}

int dxdisp::paint()
{
  if (!DIRECTDRAW_READY)
  {
    if (directdraw_init()!=0) directdraw_exit();
      // maybe have another go later when problem has cleared
  }

  if (!DIRECTDRAW_READY) return 1;

  if (fullscreen)
  {
    DDBLTFX bltfx;
    RECT destrect={0,0,320,240};
    // clear screen
    memset(&bltfx, 0,sizeof(bltfx)); // Sets dwFillColor to 0 as well
    bltfx.dwSize = sizeof(bltfx);
    prim->Blt(&destrect,NULL,NULL,/*DDBLT_WAIT|*/DDBLT_COLORFILL,&bltfx);
  }

  update();

  return 0;
}

int dxdisp::update()
{
  RECT src,dest; int ret=0;
  if (!DIRECTDRAW_READY)
  {
    if (directdraw_init()!=0) directdraw_exit();
      // maybe have another go later when problem has cleared
  }

  if (!DIRECTDRAW_READY) return 1;

  src.left=8; src.top=8;
  src.right=8+320; src.bottom=8+224;

  if (fullscreen)
  {
    int s;
    src.left=8; src.top=8;
    src.right=8+320; src.bottom=8+224;
    ret=prim->BltFast(0,8,buf,&src,0);
  }
  else
  {
    GetWindowRect(wnd,&dest);
    dest.top+=22; dest.left+=4;
    dest.right=dest.left+320*2;
    dest.bottom=dest.top+224*2;
    ret=prim->Blt(&dest,buf,&src,/*DDBLT_WAIT*/0,NULL);
  }

  return 0;
}

struct bmap dxdisp::lock()
{
  if (!DIRECTDRAW_READY)
  {
    if (directdraw_init()!=0)  directdraw_exit();
      // maybe have another go later when problem has cleared
  }
  if (!DIRECTDRAW_READY) { bm.data=NULL; return bm; }
  if (lock_surface_to_bm(&bm,buf)!=0) { bm.data=NULL; return bm; }
  return bm;
}

int dxdisp::unlock()
{
  if (bm.data) buf->Unlock(bm.data); bm.data=NULL;
  return 0;
}

