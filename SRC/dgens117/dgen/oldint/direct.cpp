// DGen Old DirectX Interface v1.14+
#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <windowsx.h>
#include <commdlg.h>
#include <mmsystem.h>
#include "dxdisp.h"
#include <dsound.h>
#include "../md/md.h"

#include <math.h>

static char temp[0x8000];
HWND hWnd=0; // Main window handle.
static FILE *debug_log=NULL;
extern "C" int dprintf(char *format, ...)
{
  if (debug_log!=NULL)
  {
    va_list arg;
    va_start(arg,format);

    vfprintf (debug_log,format,arg);
    va_end(arg);
  }
  return 0;
}

#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

HINSTANCE main_inst=0;
static md megad;
static int ret;
static int crap_tv=0,tog_fs_req=0,md_res_req=0;
static char rom_path[0x800]="";
static char state_path[0x800]="";

static char rom_name[0x800]="";
static char save_name[0x800]=""; // Can be State or SRAM
static char config_name[0x800]="";
static char curr_path[0x800]="";

static int auto_saveload=0,sram_saveload=1,no_sound=0;
FILE *sound_log=NULL;

static unsigned char md_pal[1024]={0,0,0,0,255,255,255,0};
static dxdisp *mddisp=0;

static int fullscreen=0,width=320,height=240,depth=16;
static int force_depth=0;
static int we_are_active=1;
//static PALETTEENTRY ape[256]={0};
//static IDirectDrawPalette *ddpal=0;
static float umul=1.2,vmul=1.2,brightness=0.1,contrast=0.8;
static int virtual_fps=60,any_size=0;

static signed short *fmbuf[2]={NULL};
static int snd_size=367*1,snd_rate=22050*1,snd_segs=4,stereo16=1;
static float snd_distort=1.00;
static IDirectSound *lpds=NULL;
static IDirectSoundBuffer *lpBufPrim,*lpBuf;
int sound_is_okay=0;
extern "C" int fm_chan_on,dave_fb_val;
static int dgen_display_on=1;

static int medley=0,medley_beep=0,medley_keys[60*10*2]={0},medley_record=-1;
static int medley_countdown=0,medley_press_mark=0,medley_silence=-2*60;
static int medley_nothing_after=0,medley_had_fire=0;

// Note: if medley>0
//            medley_record>=0 RECORDING keypresses
//            medley_record<0 playing songs
// otherwise, medley is off

static int pad_code[32]=
{
  0xc8,0xd0,0xcb,0xcd,
  0x1e,0x1f,0x20,0x1c,
  0x10,0x11,0x12,0x13,
  0x00,0x00,0x00,0x00,
  0x10001,0x10002,0x10003,0x10004,
  0x10010,0x10012,0x10013,0x10011,
  0x10014,0x10015,0x10016,0x10017,
  0x00,0x00,0x00,0x00
};

static int sound_init()
{
  if (no_sound) { sound_is_okay=0; return 1; }
  if (hWnd==0) return 1;

  snd_size=snd_rate/virtual_fps; if (snd_size<16) snd_size=16;


  ret=DirectSoundCreate(NULL, &lpds, NULL);
  if (ret!=DS_OK)
	{
    sound_is_okay=0; return 1; // turn off sound
	}

//  ret=lpds->SetCooperativeLevel(hWnd, DSSCL_PRIORITY);
  ret=lpds->SetCooperativeLevel(hWnd, DSSCL_EXCLUSIVE);
  if (ret!=DS_OK)
  {
    sprintf (temp,"returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    MessageBox(hWnd,temp,"lpds->SetCooperativeLevel()",MB_OK);
    if (ret!=DS_OK) goto snd_fail;
  }

	PCMWAVEFORMAT	pcmwf;
	WAVEFORMATEX	wf;

	memset(&pcmwf, 0, sizeof(PCMWAVEFORMAT)); 
	memset(&wf,0,sizeof(WAVEFORMATEX));
  pcmwf.wBitsPerSample = stereo16? 16:8; 
	pcmwf.wf.wFormatTag = WAVE_FORMAT_PCM; 
  pcmwf.wf.nChannels = stereo16? 2:1 ; 
  pcmwf.wf.nSamplesPerSec = snd_rate;
	pcmwf.wf.nBlockAlign = pcmwf.wf.nChannels * (pcmwf.wBitsPerSample/8); 
	pcmwf.wf.nAvgBytesPerSec = pcmwf.wf.nSamplesPerSec * pcmwf.wf.nBlockAlign; 
	//
	wf.wFormatTag = WAVE_FORMAT_PCM;
	wf.nChannels = pcmwf.wf.nChannels;
	wf.nSamplesPerSec = pcmwf.wf.nSamplesPerSec;
	wf.wBitsPerSample = pcmwf.wBitsPerSample;
	wf.nBlockAlign = pcmwf.wf.nBlockAlign;
	wf.nAvgBytesPerSec = pcmwf.wf.nAvgBytesPerSec;
	wf.cbSize = sizeof(WAVEFORMATEX);

	//--PrimaryBuffer make
	DSBUFFERDESC dsbdesc;
  memset(&dsbdesc, 0, sizeof(DSBUFFERDESC));
  dsbdesc.dwSize = sizeof(DSBUFFERDESC);
  dsbdesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
  ret=lpds->CreateSoundBuffer(&dsbdesc,&lpBufPrim,NULL);
  if (ret!=DS_OK)
  {
    sprintf (temp,"returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    MessageBox(hWnd,temp,"create primary buffer",MB_OK);
    if (ret!=DS_OK) goto snd_fail;
  }
  ret=lpBufPrim->SetFormat(&wf);
  if (ret!=DS_OK)
  {
    sprintf (temp,"returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    MessageBox(hWnd,temp,"primary buffer set format",MB_OK);
    if (ret!=DS_OK) goto snd_fail;
  }
  ret=lpBufPrim->Play(0,0,DSBPLAY_LOOPING);
  if (ret!=DS_OK)
  {
    sprintf (temp,"returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    MessageBox(hWnd,temp,"primary buffer play",MB_OK);
    if (ret!=DS_OK) goto snd_fail;
  }

	//--SecondaryBuffer Make
  memset(&dsbdesc, 0, sizeof(DSBUFFERDESC));
  dsbdesc.dwSize = sizeof(DSBUFFERDESC);
  dsbdesc.dwFlags = DSBCAPS_CTRLDEFAULT  | DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_STICKYFOCUS;
  dsbdesc.dwBufferBytes = snd_size*snd_segs*(stereo16?4:1); 
  dsbdesc.lpwfxFormat = (LPWAVEFORMATEX)&pcmwf;

  ret=lpds->CreateSoundBuffer(&dsbdesc,&lpBuf,NULL);
  if (ret!=DS_OK)
  {
    sprintf (temp,"returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    MessageBox(hWnd,temp,"create secondary sound buffer",MB_OK);
    if (ret!=DS_OK) goto snd_fail;
  }

/*
	LPVOID ptr1, ptr2;
	DWORD  size1, size2;
  ret=lpBuf->Lock(0,snd_size,&ptr1, &size1, &ptr2, &size2, 0);
  if (ret == DS_OK)
  {
    lpBuf->Unlock(ptr1, size1, ptr2, size2);
  }
*/

  ret=lpBuf->Play(0,0,DSBPLAY_LOOPING);
  if (ret!=DS_OK)
  {
    sprintf (temp,"returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    MessageBox(hWnd,temp,"play secondary sound buffer",MB_OK);
    sound_is_okay=0;
  }

  fmbuf[0]=(signed short *)malloc(snd_size*sizeof(signed short));
  if (fmbuf[0]==NULL) goto snd_fail;
  fmbuf[1]=(signed short *)malloc(snd_size*sizeof(signed short));
  if (fmbuf[1]==NULL) goto snd_fail;

  memset(fmbuf[0],0,snd_size*sizeof(signed short));
  memset(fmbuf[1],0,snd_size*sizeof(signed short));

// Dave fiddling - mjazz
  // meant to be 53693100L/8 ? (int)(8000000L*94/100) sounds right
  ret=YM2612Init(3,8000000L*94/100,(int)((double)snd_rate/snd_distort),NULL,NULL);
  if (ret!=0)
  {
    sprintf (temp,"YM2612Init returned %d\n",ret);
    MessageBox(hWnd,temp,"YM2612Init",MB_OK);
    if (ret!=0) goto snd_fail;
  }

  // Meant to be 53693100L/11?
  ret=SN76496_init(0,3750000L*94/100,(int)((double)snd_rate/snd_distort),16);
  if (ret!=0)
  {
    sprintf (temp,"SN76496_init returned %d\n",ret);
    MessageBox(hWnd,temp,"SN76496_init",MB_OK);
//    if (ret!=0) goto snd_fail;
  }

  sound_is_okay=1;
  megad.flush_fm_to_mame();


  return 0;
snd_fail:
  return 1;
}

static int fill_snd_seg(int fillseg)
{
  LPVOID ptr1, ptr2;
	DWORD  size1, size2;

  if (fillseg<0) return 1;
  if (fillseg>=snd_segs) return 1;

  ret=lpBuf->Lock(fillseg*snd_size*(stereo16?4:1),snd_size*(stereo16?4:1),&ptr1,&size1,&ptr2,&size2,0);
/*
  if (ret!=DS_OK || ptr2 || size2 || size1!=snd_size*(stereo16?4:1))
  {
    sprintf (temp,"returned %.8x (+%d) WANTED seg %d ptr1=%p,%x ptr2=%p,%x\n",ret,ret-MAKE_DSHRESULT(0),
      fillseg,ptr1,size1,ptr2,size2);
    MessageBox(hWnd,temp,"lpBuf->Lock",MB_OK);
    //if (ret!=DS_OK)
    sound_is_okay=0;
  }
*/
  if (ret == DS_OK)
  {
    signed short *newsnd16; unsigned char *newsnd8;
    newsnd16=(signed short *)ptr1;
    newsnd8=(unsigned char *)ptr1;
    if (ptr1!=NULL)
    {
      signed short *sound_buf[2];
      int j;
      int fade=0x100;
      int something=0;

      if (medley>0 && medley_record<0)
      {
        if (medley_press_mark>0)
        {
          if (medley_countdown<medley_press_mark) fade=0;
          else if (medley_countdown<medley_press_mark+0x100)
            fade=medley_countdown-medley_press_mark;
            // fade out
        }
      }

      sound_buf[0]=fmbuf[0];
      sound_buf[1]=fmbuf[1];
      for (j=0;j<snd_size;j++)
      {
        int disp;
        if (stereo16)
        {
          newsnd16[j*2+0]=sound_buf[0][j];
          newsnd16[j*2+1]=sound_buf[1][j];
          if (fade!=0x100)
          {
            newsnd16[j*2+0]=((int)newsnd16[j*2+0]*fade)>>8;
            newsnd16[j*2+1]=((int)newsnd16[j*2+1]*fade)>>8;
          }
          newsnd16[j*2+0]+=((medley_beep>0)? (j&8?-0x2000:+0x2000):0);

          if (j>0)
          {
            if (newsnd16[j*2+0]!=newsnd16[(j-1)*2+0]) something=1;
            if (newsnd16[j*2+1]!=newsnd16[(j-1)*2+1]) something=1;
          }
        }
        else
        {
          disp=0;
          disp+=((int)sound_buf[0][j]);
          disp+=((int)sound_buf[1][j]);
          disp>>=9;
          if (disp>+0x02 || disp<-0x02) something=1;

          disp=((int)disp*fade)>>8;

          disp+=((medley_beep>0)? (j&8?-0x20:+0x20):0);
          newsnd8[j]=disp+0x80;

          if (j>0)
          {
            if (newsnd8[j]!=newsnd8[j-1]) something=1;
          }
        }
      }

      if (something) medley_silence=0; else medley_silence++;

      if (medley>0 && medley_record<0)
      {
        if (medley_silence>30 && medley_countdown>60*10 )
        { medley_countdown=60*10; medley_silence=-2*60; }
      }

      if (sound_log!=NULL)
        fwrite(ptr1,1,snd_size*(stereo16?4:1),sound_log);

    }
    lpBuf->Unlock(ptr1, size1, ptr2, size2);
    if (medley_beep>0) medley_beep--;
  }

  return 0;
}


static int sound_exit()
{
  if (sound_is_okay)
  {
    //-- DirectSound Cleanup
    if(lpBuf)   lpBuf->Release(); lpBuf=NULL;
    if(lpBufPrim) lpBufPrim->Release(); lpBufPrim=NULL;
    if(lpds)    lpds->Release(); lpds=NULL;
    YM2612Shutdown();

    if (fmbuf[0]!=NULL) free(fmbuf[0]); fmbuf[0]=NULL;
    if (fmbuf[1]!=NULL) free(fmbuf[1]); fmbuf[1]=NULL;
  }

  sound_is_okay=0;
  return 0;
}

static char *gst_name(char *fn)
{
  char *pt,*st;
  int len;

  temp[0x200]=0;
  pt=fn+strlen(fn)-1;
  while (pt>fn)
  {
    if (*pt=='\\') {pt++; break; }
    pt--;
  }
  st=pt;
  len=0;
  while (*pt!=0)
  {
    if (*pt=='.') break; 
    pt++; len++;
  }
  memcpy(temp+0x200,st,len);
  temp[0x200+len]=0;

  return temp+0x200;
}

static inline int depth_to_bytes(int depth)
{
       if (depth<=8)  return 1;
  else if (depth<=16) return 2;
  else if (depth<=24) return 3;
  else                return 4;
}

static int mess_with_bm(struct bmap *bm)
{
  // Remember messy border - handy
  int x,y; unsigned char *line;

  if (crap_tv)
  {
    line=bm->data+8*bm->pitch+8*depth_to_bytes(bm->bpp);
#ifdef ASM_CTV
    for (y=0;y<224;y++)
    {
      // asm in ctv.asm
      if (bm->bpp==16) blur_bitmap_16(line,320-1);
      else if (bm->bpp==15) blur_bitmap_15(line,320-1);

//      if (y==medley_silence) memset(line,0xff,100);
//      if (y==medley_countdown/30) memset(line,0x80,100);

      line+=bm->pitch;
    }
#else
    if (bm->bpp==16)
    for (y=0;y<224;y++)
    {
      unsigned char *pix;
  
      pix=line;
      for (x=0;x<320-1;x++)
      {
        int tmp;
        int dot,dot2;
        dot= pix[0]|(pix[1]<<8);
        dot2=pix[2]|(pix[3]<<8);
  
        // Split g out from middle
        dot= ((dot &0x07e0)<<16) | (dot &0xf81f);
        dot2=((dot2&0x07e0)<<16) | (dot2&0xf81f);
  
        dot=(dot+dot2)>>1;
  
        // Put g back in middle
        dot= ((dot >>16) &0x07e0) | (dot &0xf81f);
  
        pix[0]=dot&255;
        pix[1]=(dot>>8)&255;
  
        pix+=2;
      }
      line+=bm->pitch;
    }
#endif
  }
  return 0;
}

static int mess_with_pal(unsigned char pal[256])
{
  int i;
  if (pal==NULL) return 1;
  for (i=0;i<64;i++)
  {
    int ir,ig,ib;
    float y,u,v,r,g,b;
    ir=pal[i*4+0]; ig=pal[i*4+1]; ib=pal[i*4+2];

    r=((float)ir)/255.0;
    g=((float)ig)/255.0;
    b=((float)ib)/255.0;

    y=0.30*r+0.59*g+0.11*b;
    u=0.49*(b-y);
    v=0.88*(r-y);

    ir=ig=ib=0;

    y=(y*contrast)+brightness;
    u*=umul;
    v*=vmul;

    r=v/0.88+y;
    b=u/0.49+y;
    g=(y - 0.30*r - 0.11*b)/0.59;

    ir=(int)(r*255.0);
    ig=(int)(g*255.0);
    ib=(int)(b*255.0);

    if (ir<0) ir=0; if (ig<0) ig=0; if (ib<0) ib=0;
    if (ir>255) ir=255; if (ig>255) ig=255; if (ib>255) ib=255;

    pal[i*4+0]=ir; pal[i*4+1]=ig; pal[i*4+2]=ib;
  }
  return 0;
}

/*
static int put_in_ddraw_pal(unsigned char pal[1024])
{
  int i;
  if (ddpal==0) return 1;
  for (i=0;i<256;i++)
  {
    ape[i].peRed  =pal[i*4+0];
    ape[i].peGreen=pal[i*4+1];
    ape[i].peBlue =pal[i*4+2];
  }
  ddpal->SetEntries(0,0,256,ape);
  return 0;
}

static make_ddraw_pal(IDirectDraw *pdd)
{
  int i;
  if (pdd->CreatePalette(DDPCAPS_8BIT, ape, &ddpal, NULL)!=DD_OK)
    return 1;
  return 0;
}
*/

long int  PASCAL  MainWndProc(HWND, UINT, WPARAM, LPARAM);
BOOL InitApplication(HINSTANCE);

/***  Global Variables  ***/
char szName[256];
//HINSTANCE hInst;  // current instance

static int get_megadrive_pads()
{
  int p;
  for (p=0;p<2;p++)
    megad.pad[p]=0x000f303f; // 0x000f303f; = untouched pad

  // Now calculate the sound for the next seg
  if (we_are_active)
  {
    input_update();
    for (p=0;p<2;p++)
    {
      int bse=0,k;
      static const int msk[12]={~1,~2,~4,~8,~0x1000,~0x10,~0x20,~0x2000,
        ~0x40000,~0x20000,~0x10000,~0x80000,};
      if (p==1) bse=16;
      // Putting extra buttons in the top f
      // X=1011 Y=1101 Z=1110 Mode=0111

      for (k=0;k<12;k++)
        if (input_check(pad_code[bse+k])) megad.pad[p]&=msk[k];
    }
  }
  if (medley>0)
  {
    if (medley_record>=0)
    {
      // recording keypress for next music
      if (medley_record>=60*10)
      {
        medley_record=-1;
        medley_beep=2;
        // Start timer
        medley_countdown=medley+60*9; // a sec to press it
      }
      else
      {
        int i;
        for (i=0;i<2;i++)
        {
          if (megad.pad[i]!=0xf303f)
          {
            if (medley_press_mark<=0)
              medley_press_mark=60*10-(medley_record+0);
              // but not medley_had_fire yet - maybe the start

            medley_nothing_after=60*10-(medley_record+1);
          }

          if ((megad.pad[i]&0xf3030)!=0xf3030)
          {
            if (medley_press_mark<=0 || !medley_had_fire)
              // mark where they start the music on the countdown
                medley_press_mark=60*10-(medley_record+0);
            medley_had_fire=1; // definitely the start
          }
        }
        medley_keys[medley_record*2+0]=megad.pad[0];
        medley_keys[medley_record*2+1]=megad.pad[1];
        medley_record++;
      }
    }
    else
    {
      if (medley_countdown<medley_nothing_after)
      {
        // Start timer again
        medley_countdown=medley+60*9; // a sec to press it
      }
      else if (medley_countdown<60*10)
      {
        // Start keying in next music
        int fram;
        fram=(60*10)-1-medley_countdown;
        megad.pad[0]&=medley_keys[fram*2+0];
        megad.pad[1]&=medley_keys[fram*2+1];
      }
      medley_countdown--;
    }
  }
  return 0;
}

static unsigned int frame_count=0;
static unsigned int start_time=0,end_time=0;

static int run_megadrive_without_sound()
{
  static DWORD lasttime=0,thistime;
  static int firsttime=1;
  static int extramsgone=0;
  int msgone,frgone,f;

  thistime=timeGetTime();
  msgone=thistime-lasttime;
  lasttime=thistime;

  if (firsttime) { msgone=0; firsttime=0; }
  msgone+=extramsgone;
  frgone=msgone*virtual_fps/1000;
  extramsgone=msgone-(frgone*1000/virtual_fps);

  if (frgone<0) frgone=0;
  else if (frgone>12) frgone=12;

  for (f=0;f<frgone;f++)
  {
    get_megadrive_pads();
    if (f<frgone-1)
    {
      get_megadrive_pads();
      megad.one_frame(NULL,NULL,NULL);
    }
    else if (mddisp)
    {
      struct bmap sbm;
      sbm=mddisp->lock();
      if (sbm.data!=NULL)
      {
        get_megadrive_pads();
        megad.one_frame(&sbm,NULL,NULL);
        mess_with_bm(&sbm);
        mddisp->unlock();
        mddisp->update(any_size);
        frame_count++;
      }
    }
  }
  return 0;
}


static int run_megadrive_with_sound()
{
  DWORD posPlay,posWrite;
  int read_ptr=0;
  static int write_ptr=0;
  int pic_and_sound=0;
  if (lpds==NULL) return 1;
  if (lpBuf==NULL) return 1;
  if (lpBufPrim==NULL) return 1;

  {
    extern int medley_marker;
    medley_marker=(medley>0);
  }

// Sound buffer loop round a number of segs of size snd_size
// SSSSSSSSS SSSSSSSSS SSSSSSSSS SSSSSSSSS 
//            Playing   Writing

  pic_and_sound=0;
  if (sound_is_okay)
  {
    lpBuf->GetCurrentPosition(&posPlay,&posWrite);
  
    read_ptr=posPlay/(snd_size*(stereo16?4:1));
    while (write_ptr!=read_ptr)
    {
      fill_snd_seg(write_ptr);


  
      write_ptr=(write_ptr+1)%snd_segs;
  
      if (write_ptr!=read_ptr)
      {
        {
          struct sndinfo si;
          si.l=fmbuf[0]; si.r=fmbuf[1];
          si.len=snd_size;
          // Just the sound - don't need the picture yet
          get_megadrive_pads();
          megad.one_frame(NULL,NULL,&si);
        }
      }
      else
        pic_and_sound=1;
    }
  }

  if (pic_and_sound && !dgen_display_on)
  {
    struct sndinfo si;
    si.l=fmbuf[0]; si.r=fmbuf[1];
    si.len=snd_size;
    get_megadrive_pads();
    megad.one_frame(NULL,NULL,&si);
  }

  if (pic_and_sound && dgen_display_on && mddisp)
  {
    // Right - now we need the picture and the sound too
    // Turn the buf surface into something we can write into
    struct bmap sbm;
    sbm=mddisp->lock();
    if (sbm.data!=NULL)
    {
      struct sndinfo si;
      si.l=fmbuf[0]; si.r=fmbuf[1];
      si.len=snd_size;
      get_megadrive_pads();
      megad.one_frame(&sbm,NULL,&si);
      mess_with_bm(&sbm);
      mddisp->unlock();
      mddisp->update(any_size);

      frame_count++;
    }
  }

  return 0;
}

static int run_megadrive()
{
  if (sound_is_okay) return run_megadrive_with_sound();
  return run_megadrive_without_sound();
}

static int do_save_load(int save,int sram,int alert,char *name)
{
  FILE *hand=NULL; int sl;
  if (megad.romfilename[0]==0) return 1;

  // sram==0 Normal RAM, sram==1 Save RAM

  if (sram==1 && !megad.has_save_ram())
  {
    if (alert)
    {
      sprintf (temp+0x4000,"Couldn't %s SRAM since %s doesn't have SRAM.\n",
        save?"save":"load",gst_name(megad.romfilename));
      MessageBox(hWnd,temp+0x4000,
      save?"DGen SRAM Save Warning":"DGen SRAM Load Warning",
      MB_OK);
    }
    return 1;
  }

  if (name==NULL)
  {
    sprintf (temp,"%s\\%.8s.%s",state_path,gst_name(megad.romfilename),
      sram==0?"gsx":"srm");
    name=temp;
  }


  hand=fopen(name,save?"wb":"rb");
  if (hand!=NULL)
  {
    if (sram==0)
    {
      if (save) megad.export_gst(hand);
      else      megad.import_gst(hand);
    }
    if (sram==1)
    {
      if (save) megad.put_save_ram(hand);
      else      megad.get_save_ram(hand);
    }
    fclose(hand);

    if (alert)
    {
      sprintf (temp+0x4000,"Succeeded in %s '%s'\n",save?"save to":"load from",name);
      MessageBox(hWnd,temp+0x4000,
      save?"DGen Saved Okay":"DGen Loaded Okay",
      MB_OK);
    }
  }
  else if (alert)
  {
    sprintf (temp+0x4000,"Couldn't %s '%s'\n%s",
      save?"save to":"load from",name,
      save?"Try making a directory there.\n":""
      );
    MessageBox(hWnd,temp+0x4000,"DGen Save Warning",MB_OK);
  }

  medley=0;
  return 0;
}

static int maybe_auto_save_load(int save)
{
  if (auto_saveload) do_save_load(save,0,0,NULL);
  if (sram_saveload) do_save_load(save,1,0,NULL);
  return 0;
}

static int common_codes[3][32]=
{
  {
    0xc8,0xd0,0xcb,0xcd,
    0x1e,0x1f,0x20,0x1c,
    0x10,0x11,0x12,0x13,
    0x00,0x00,0x00,0x00,
    0x10001,0x10002,0x10003,0x10004,
    0x10010,0x10012,0x10013,0x10011,
    0x10014,0x10015,0x10016,0x10017,
    0x00,0x00,0x00,0x00
  },
  {
    0x10001,0x10002,0x10003,0x10004,
    0x10010,0x10012,0x10013,0x10011,
    0x10014,0x10015,0x10016,0x10017,
    0x00,0x00,0x00,0x00,
    0xc8,0xd0,0xcb,0xcd,
    0x1e,0x1f,0x20,0x1c,
    0x10,0x11,0x12,0x13,
    0x00,0x00,0x00,0x00
  },
  {
    0x10001,0x10002,0x10003,0x10004,
    0x10010,0x10012,0x10013,0x10011,
    0x10014,0x10015,0x10016,0x10017,
    0x00,0x00,0x00,0x00,
    0x10101,0x10102,0x10103,0x10104,
    0x10110,0x10112,0x10113,0x10111,
    0x10114,0x10115,0x10116,0x10117,
    0x00,0x00,0x00,0x00
  }
};
static int common_keys(int type)
{
  if (type>=0 && type<3)
    memcpy(pad_code,common_codes[type],32*sizeof(int));

  return 0;
}

extern int split_screen;
static int update_menu()
{
  HMENU mnu;
  int i;
  mnu=GetSubMenu(GetMenu(hWnd), 1);

  CheckMenuItem(mnu, 0x0104,megad.pause?MF_CHECKED:MF_UNCHECKED);

  CheckMenuItem(mnu, 0x0112,(megad.country_ver&0xff0)==0x80?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0113,(megad.country_ver&0xff0)==0x00?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0114,(megad.country_ver&0xff0)==0xc0?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0115,(megad.country_ver&0xff0)==0x40?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0116,(megad.country_ver&0xff0)==0xff0?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0117,auto_saveload?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0118,sram_saveload?MF_CHECKED:MF_UNCHECKED);

  CheckMenuItem(mnu, 0x0170,virtual_fps==50?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0171,virtual_fps==60?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0172,virtual_fps==70?MF_CHECKED:MF_UNCHECKED);
  for (i=0;i<2;i++)
    CheckMenuItem(mnu, 0x0120+i,(megad.country_ver&0x0f)==i?MF_CHECKED:MF_UNCHECKED);

  CheckMenuItem(mnu, 0x0130,megad.cpu_emu==0?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0131,megad.cpu_emu==1?MF_CHECKED:MF_UNCHECKED);

  mnu=GetMenu(hWnd);
  CheckMenuItem(mnu, 0x0150,fullscreen?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0151,split_screen?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0152,crap_tv?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0153,any_size?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0154,dgen_display_on?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0155,megad.render_method==0?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0156,megad.render_method==1?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0157,megad.render_method==2?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0158,megad.render_method==3?MF_CHECKED:MF_UNCHECKED);

  for (i=0;i<3;i++)
  {
    int match; match=(memcmp(pad_code,common_codes[i],32*sizeof(int))==0);
    CheckMenuItem(mnu, 0x0142+i,match?MF_CHECKED:MF_UNCHECKED);
  }

  for (i=0;i<0x10;i++)
    CheckMenuItem(mnu, 0x0330+i,(megad.layer_sel&(1<<i))?MF_CHECKED:MF_UNCHECKED);

  mnu=GetSubMenu(GetMenu(hWnd), 5);
  CheckMenuItem(mnu, 0x0163, no_sound?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x01b0, megad.mjazz?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0161,!stereo16?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0162, stereo16?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0168,snd_rate==8000?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0169,snd_rate==11025?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x016a,snd_rate==22050?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x016b,snd_rate==44100?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0204,snd_segs==4?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0208,snd_segs==8?MF_CHECKED:MF_UNCHECKED);
  CheckMenuItem(mnu, 0x0220,snd_segs==32?MF_CHECKED:MF_UNCHECKED);

  CheckMenuItem(mnu, 0x0180,megad.snd_mute&1?MF_UNCHECKED:MF_CHECKED);
  CheckMenuItem(mnu, 0x0181,megad.snd_mute&2?MF_UNCHECKED:MF_CHECKED);
  CheckMenuItem(mnu, 0x0182,megad.snd_mute&4?MF_UNCHECKED:MF_CHECKED);

  for (i=0;i<6;i++)
    CheckMenuItem(mnu, 0x0190+i,medley==i*30*60?MF_CHECKED:MF_UNCHECKED);

  for (i=0;i<6;i++)
    CheckMenuItem(mnu, 0x01a0+i,(fm_chan_on&(1<<i))?MF_CHECKED:MF_UNCHECKED);

  //  HMENU hm=LoadMenu(main_inst,"DGENMENU");
  //  EnableMenuItem(,0x100,MF_CHANGE | MF_CHECKED);
  //EnableMenuItem(GetSubMenu(GetMenu(hWnd), 0), 0x101, MF_CHECKED);
  //CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), 0x0111, MF_CHECKED);
  return 0;
}

// parses the config file
static int load_config(char *name)
{
  int done=0;
  FILE *hand;
  int i;
  int no_config_file=0;

  hand=fopen(name,"rt");
  if (hand==NULL)
  {
    no_config_file=1;
  }
  else
  {
    no_config_file=0;
    done=0;
    while (!done)
    {
      static char buf[256]="";
      if (fgets(buf,255,hand)==NULL) done=1;
      else
      {
        char *check_for=NULL;
        int slen=strlen(buf);

        // Knock off linefeeds, because they will mess up strings
        if (buf[slen-1]==10) {buf[slen-1]=0; slen--;}

#define IT_IS (strncmp(buf,check_for,strlen(check_for))==0)
#define VALUE (buf+strlen(check_for))
        // Graphics
        check_for="fullscreen";
        if (IT_IS) fullscreen=atoi(VALUE);
        check_for="width";
        if (IT_IS) width=atoi(VALUE);
        check_for="height";
        if (IT_IS) height=atoi(VALUE);
        check_for="depth";
        if (IT_IS) depth=atoi(VALUE);
        check_for="force_depth";
        if (IT_IS) force_depth=atoi(VALUE);
        check_for="brightness";
        if (IT_IS) brightness=atof(VALUE);
        check_for="contrast";
        if (IT_IS) contrast=atof(VALUE);
        check_for="umul";
        if (IT_IS) umul=atof(VALUE);
        check_for="vmul";
        if (IT_IS) vmul=atof(VALUE);
        check_for="crap_tv";
        if (IT_IS) crap_tv=atoi(VALUE);

        // Sound
        check_for="no_sound";
        if (IT_IS) no_sound=atoi(VALUE);
        check_for="snd_rate";
        if (IT_IS) snd_rate=atoi(VALUE);
        check_for="snd_segs";
        if (IT_IS) snd_segs=atoi(VALUE);
        check_for="snd_distort";
        if (IT_IS) snd_distort=atof(VALUE);
        check_for="stereo16";
        if (IT_IS) stereo16=atoi(VALUE);

        // Input
        check_for="pad_code[";
        if (IT_IS)
        {
          char *next_bit;
          int p=strtol(VALUE,&next_bit,10);
          if (next_bit!=NULL)
          {
            if (next_bit[0]==']')
            {
              if (next_bit[1]==' ')
              {
                int val=strtol(next_bit+2,NULL,0);
                pad_code[p&31]=val;
              }
            }
          }
        }

        // Paths
        check_for="rom_path";
        if (IT_IS) if (*VALUE)
            strncpy(rom_path,VALUE+1,0x7ff);
        check_for="state_path";
        if (IT_IS) if (*VALUE)
        strncpy(state_path,VALUE+1,0x7ff);

        // Misc
        check_for="virtual_fps";
        if (IT_IS) virtual_fps=atoi(VALUE);
        check_for="any_size";
        if (IT_IS) any_size=atoi(VALUE);
        check_for="split_screen";
        if (IT_IS) split_screen=atoi(VALUE);
        check_for="country_ver";
        if (IT_IS) megad.country_ver=strtol(VALUE,NULL,0);
        check_for="cpu_emu";
        if (IT_IS) megad.change_cpu_emu(atoi(VALUE));
        check_for="auto_saveload";
        if (IT_IS) auto_saveload=atoi(VALUE);
        check_for="sram_saveload";
        if (IT_IS) sram_saveload=atoi(VALUE);
      }
    }
  
    fclose(hand);
  }

  return no_config_file;
}

static int unload_config(char *name)
{
  int i;
  FILE *hand=fopen(name,"wt");

  if (hand==NULL) return 1;

  fprintf(hand,"// Dave's DGEN v%s (DirectX) config file\n\n",VER);
  fprintf(hand,"// Note, 0=off, 1=on\n");

  fprintf(hand,"\n// Graphics\n");
  fprintf(hand,"fullscreen %d\n",fullscreen);
  fprintf(hand,"width %d\n",width);
  fprintf(hand,"height %d\n",height);
  fprintf(hand,"depth %d\n",depth);
  fprintf(hand,"force_depth %d\n",force_depth);
  fprintf(hand,"brightness %.2f\n",brightness);
  fprintf(hand,"contrast %.2f\n",contrast);
  fprintf(hand,"umul %.2f\n",umul);
  fprintf(hand,"vmul %.2f\n",vmul);
  fprintf(hand,"crap_tv %d\n",crap_tv);

  fprintf(hand,"\n\n// Sound (halve rate or increase segs if you have trouble)\n");
  fprintf(hand,"no_sound %d\n",no_sound);
  fprintf(hand,"snd_rate %d\n",snd_rate);
  fprintf(hand,"snd_segs %d\n",snd_segs);
  fprintf(hand,"snd_distort %.2f\n",snd_distort);
  fprintf(hand,"stereo16 %d\n",stereo16);

  fprintf(hand,"\n\n// Input\n");
  for (i=0;i<32;i++)
    fprintf(hand,"pad_code[%.2d] 0x%.5x\n",i,pad_code[i]);

  fprintf(hand,"\n\n// Paths\n");
  fprintf(hand,"rom_path %s\n",rom_path);
  fprintf(hand,"state_path %s\n",state_path);

  fprintf(hand,"\n\n// Misc.\n");
  fprintf(hand,"virtual_fps %d\n",virtual_fps);
  fprintf(hand,"any_size %d\n",any_size);
  fprintf(hand,"split_screen %d\n",split_screen);
  fprintf(hand,"country_ver 0x%.2x\n",megad.country_ver);
  fprintf(hand,"cpu_emu %d\n",megad.cpu_emu);
  fprintf(hand,"auto_saveload %d\n",auto_saveload);
  fprintf(hand,"sram_saveload %d\n",sram_saveload);

  fprintf(hand,"\n");
  fclose(hand);
  return 0;
}

BOOL get_rom_name()
{
	OPENFILENAME	open;
  char  Filter[]=
    "Genesis Roms (*.bin;*.smd;*.zip)\0"
    "*.bin;*.smd;*.zip\0"
    "Zipped Roms (*.zip)\0"
    "*.zip\0"
    "All Files\0"
    "*.*\0";
	memset(&open, 0, sizeof(OPENFILENAME));

	open.lStructSize=sizeof(OPENFILENAME);
  open.lpstrInitialDir=rom_path;
	open.lpstrFilter=&Filter[0];
  rom_name[0]=0;
  open.lpstrFile=rom_name;
  open.nMaxFile=0x7ff;
  open.lpstrTitle="Load Rom Image";
  open.Flags=OFN_HIDEREADONLY;
  if (GetOpenFileName(&open)==0) return 1;

  int i; i=strlen(rom_name);
  for (;i>=0;i--) if (rom_name[i]=='\\') break;
  if (i>0x7ff) i=0x7ff;
  memcpy(rom_path,rom_name,i);
  rom_path[i]=0;

  return 0;
}

BOOL get_save_name(int save,int sram)
{
	OPENFILENAME	open;
  char      gsx_filt[]="*.gs*\0*.gs*\0All Files\0*.*\0";
  char      srm_filt[]="*.sr*\0*.sr*\0All Files\0*.*\0";
  char *Filter;
  if (sram) Filter=srm_filt; else Filter=gsx_filt;

  memset(&open, 0, sizeof(OPENFILENAME));

	open.lStructSize=sizeof(OPENFILENAME);
  open.lpstrInitialDir=state_path;
	open.lpstrFilter=&Filter[0];

  if (megad.romfilename[0]==0)
    save_name[0]=0;
  else
    sprintf (save_name,"%.8s.%s%c",gst_name(megad.romfilename),
      sram?"sr":"gs", save?(sram?'m':'x'):'*');
  open.lpstrFile=save_name;

  open.nMaxFile=0x7ff;
  if (sram)
  {
    open.lpstrTitle=save?"Save State to...":"Load State from...";
  }
  else
  {
    open.lpstrTitle=save?"Save SRAM to...":"Load SRAM from...";
  }

  open.Flags=OFN_HIDEREADONLY;
  if (save)
  {
    if (GetSaveFileName(&open)==0) return 1;
  }
  else
  {
    if (GetOpenFileName(&open)==0) return 1;
  }

  int i; i=strlen(save_name);
  for (;i>=0;i--) if (save_name[i]=='\\') break;
  if (i>0x7ff) i=0x7ff;
  memcpy(state_path,save_name,i);
  state_path[i]=0;

  return 0;
}

static int manual_defining_keys=0;
static int upto=0,waiting_to_release=-1;
HWND rwin;
static HFONT hnewsfont;   // handle of new fixed font

long int  PASCAL  man_def_keys_proc(HWND rwin, UINT message,WPARAM wParam, LPARAM lParam)
{
  static char kname[12][32]={"Up","Down","Left","Right","A","B","C","Start","X","Y","Z","Mode"};
  switch (message)
  {

    case WM_PAINT:
    {
      PAINTSTRUCT ps;
      HDC  hdc = BeginPaint(rwin, &ps);

      // Establish fixed font in display context.
      SelectObject(hdc, hnewsfont);

      SetTextAlign (hdc, TA_BASELINE /* | TA_CENTER */);

      sprintf (temp,"Manual Define keys for pad %c",manual_defining_keys==1?'A':'B');
      TextOut(hdc, 16, 20, temp, strlen(temp));

      if (upto<=-1)
      {
        sprintf (temp,"(Waiting for 0x%.5x to be released) <===",waiting_to_release);
        TextOut(hdc, 16, 50-1*16, temp, strlen(temp));
      }

      if (upto>=12)
      {
        sprintf (temp,"(Done - Press any key to exit.) <===");
        TextOut(hdc, 16, 50+13*16, temp, strlen(temp));
      }

      int i;
      for (i=0;i<12;i++)
      {
        sprintf (temp,"0x%.5x = %s %s",
          pad_code[(manual_defining_keys==1)?(0+i):(16+i)],kname[i],
            (upto==i)?"<=== (press a button / escape to skip)":"");
        TextOut(hdc, 16, 50+i*16, temp, strlen(temp));
      }
      EndPaint(rwin,&ps);
    }
    break;

    case WM_QUIT:
    case WM_DESTROY:  // message: window being destroyed
      manual_defining_keys=0;
    break;

    default:      // Passes it on if unproccessed
      return (DefWindowProc(rwin, message, wParam, lParam));
  }
  return 0L;
}

static int manual_define_keys()
{
  HDC hDC;
  static LOGFONT   cursfont;           // font structure
  static HFONT     holdsfont;   // handle of original font

  upto=-1;
  // Create a window
  rwin = CreateWindow(
   "manual_define",                 // See RegisterClass() call.
   "Manual Define Keys",
	 WS_OVERLAPPEDWINDOW,        // Window style.
   160,120,160+300,120+190,
	 NULL,                  // Overlapped windows have no parent.
	 NULL,                  // Use the window class menu.
   main_inst,              // This instance owns this window.
	 NULL                   // Pointer not needed.
  );

  if (!rwin) return 1;
  // Get the display context.
  hDC = GetDC(rwin);

  // Build fixed screen font.
  cursfont.lfHeight         =  14;
  cursfont.lfWidth          =  9;
  cursfont.lfEscapement     =  0;
  cursfont.lfOrientation    =  0;
  cursfont.lfWeight         =  FW_NORMAL;
  cursfont.lfItalic         =  FALSE;
  cursfont.lfUnderline      =  FALSE;
  cursfont.lfStrikeOut      =  FALSE;
  cursfont.lfCharSet        =  ANSI_CHARSET;
  cursfont.lfOutPrecision   =  OUT_DEFAULT_PRECIS;
  cursfont.lfClipPrecision  =  CLIP_DEFAULT_PRECIS;
  cursfont.lfQuality        =  DEFAULT_QUALITY;
  cursfont.lfPitchAndFamily =  FIXED_PITCH | FF_DONTCARE;
  strcpy((char *)cursfont.lfFaceName, "System");

  hnewsfont = CreateFontIndirect((LPLOGFONT) &cursfont);

  // Install the font in the current display context.
//  holdsfont = SelectObject(hDC, hnewsfont);

  // get text metrics for paint
//  GetTextMetrics(hDC, &tm);
//  xChar = tm.tmAveCharWidth;
//  yChar = tm.tmHeight + tm.tmExternalLeading;
//  yCharnl = tm.tmHeight;

  // Release the display context.
  ReleaseDC(rwin, hDC);

  // Make the window visible; update its client area;

  ShowWindow(rwin, 1); // Show the window
  InvalidateRect(rwin, NULL, TRUE);
  UpdateWindow(rwin);     // Sends WM_PAINT message

  return 0;
}

static int manual_define_update()
{
  int code;

  input_update();

  if (upto>=12)
  {
    if (input_check(-1)!=-1)
      DestroyWindow(rwin);
    return 0;
  }

  if (input_check(0x01) || input_check(0x201)) code=0x01; // just in case
  else code=input_check(-1);

  if (upto<=-1)
  {
    if (code==0x01 || code==-1)
    {
      upto++;
    }
    else
    {
      waiting_to_release=code;
    }
    InvalidateRect(rwin, NULL, TRUE);
    UpdateWindow(rwin);

    return 0;
  }

  if (code!=-1)
  {
    if (code!=0x01)
    {
      pad_code[ ((manual_defining_keys==1)?(0+upto):(16+upto))&31 ]=code;
      update_menu(); // common_code match may have changed
    }
    upto++;

    InvalidateRect(rwin, NULL, TRUE);
    UpdateWindow(rwin);

    int done=0;
    while(!done)
    {
      input_update();
      if (input_check(code)==0) done=1;
      if (code!=0x01) if (input_check(0x01)) done=1;
      if (code!=0x201) if (input_check(0x201)) done=1;
    }
  }

  return 0;
}

/**********************************************************************/
BOOL InitApplication(HINSTANCE hInstance)
{
  WNDCLASS  wc;

  // Fill in window class structure with parameters that describe the
  // main window.

  wc.style = CS_HREDRAW | CS_VREDRAW; // Class style(s).
  wc.lpfnWndProc = MainWndProc; // Function to retrieve messages for
									 // windows of this class.
  wc.cbClsExtra = 0;  // No per-class extra data.
  wc.cbWndExtra = 0;  // No per-window extra data.
  wc.hInstance = hInstance; // Application that owns the class.
  wc.hIcon = LoadIcon(hInstance,"DGENICON"); /*LoadIcon(NULL, 0x3000 IDI_APPLICATION ); */
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
  wc.lpszMenuName = "DGENMENU";  // Name of menu resource in .RC file.
  wc.lpszClassName = "DGENMAIN"; // Name used in call to CreateWindow.

  /* Register the window class and return success/failure code. */

  RegisterClass(&wc);

  // This is the manual_define window

  wc.style = CS_HREDRAW | CS_VREDRAW; // Class style(s).
  wc.lpfnWndProc = man_def_keys_proc; // Function to retrieve messages for
									 // windows of this class.
  wc.cbClsExtra = 0;  // No per-class extra data.
  wc.cbWndExtra = 0;  // No per-window extra data.
  wc.hInstance = hInstance; // Application that owns the class.
  wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
  wc.lpszMenuName = NULL;  // Name of menu resource in .RC file.
  wc.lpszClassName = "manual_define"; // Name used in call to CreateWindow.

  RegisterClass(&wc);
  return 0;
}

static int direct_draw_init()
{
  dprintf ("*direct_draw_init();\n");

  hWnd = CreateWindow(
   "DGENMAIN",                 // See RegisterClass() call.
   "DGen v" VER,  // Text for window title bar.
   fullscreen?WS_POPUP:WS_OVERLAPPEDWINDOW,        // Window style.
//   CW_USEDEFAULT,         // Default horizontal position.
//   CW_USEDEFAULT,          // Default vertical position.
//   CW_USEDEFAULT,            // Default width.
//   CW_USEDEFAULT,        // Default height.
160,80,
(320*1+8),(224*1+42+4),
	 NULL,                  // Overlapped windows have no parent.
	 NULL,                  // Use the window class menu.
   main_inst,              // This instance owns this window.
	 NULL                   // Pointer not needed.
  );

  if (!hWnd) exit(1);

  ShowWindow(hWnd, 1); // Show the window
  UpdateWindow(hWnd);     // Sends WM_PAINT message

  mddisp=new dxdisp(hWnd,fullscreen); // DXDISP.CPP inits directdraw

  InvalidateRect(hWnd,NULL,TRUE);
  UpdateWindow(hWnd);     // Sends WM_PAINT message

  update_menu();

  return 0;
}

static int direct_draw_exit()
{
  dprintf ("*direct_draw_exit();\n");
  if (mddisp) delete mddisp; mddisp=0; // DXDISP.CPP exits directdraw
  if (hWnd) DestroyWindow(hWnd); hWnd=0;
  return 0;
}

static int really_quit=1;

static int toggle_fullscreen()
{
  fullscreen=!fullscreen;
  sound_exit();
  really_quit=0;
  direct_draw_exit();
  direct_draw_init();

  really_quit=1;
  sound_init();
  return 0;
}


BOOL CALLBACK genie_proc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
  if (message == WM_INITDIALOG) return(TRUE);
  if (message == WM_COMMAND)
  {
    int id; id=(wParam&0xffff);
    if (id==0x1002) // Patch
    {
      static char box[0x800]=""; int ret;
      ret=GetDlgItemText(hDlg,0x1001,box,0x800);
      megad.patch(box); // call joe's Genie patch code
      return(TRUE);
    }
    else if (id==0x1003) megad.fix_rom_checksum();
    else if (id==0x1004) EndDialog(hDlg, TRUE);
  }

  if (message==WM_CLOSE) EndDialog(hDlg, TRUE);
  return(FALSE);
}

/****************************************************************************
  FUNCTION: MainWndProc(HWND, UINT, WPARAM, LPARAM)
****************************************************************************/
long int  PASCAL  MainWndProc(HWND hWnd, UINT message,
						WPARAM wParam, LPARAM lParam)
{
  switch (message)
  {
    case WM_COMMAND:
    {
      int sele=GET_WM_COMMAND_ID(wParam, lParam);
      if (sele==0x0100) // Load
      {
        if (get_rom_name()==0)
        {
          maybe_auto_save_load(1);
          megad.load(rom_name,1);
          maybe_auto_save_load(0);
          medley=0;
        }
        sound_exit();
        sound_init();
      }
      else if (sele==0x0105) // Genie Dialog
      {
        DLGPROC  lpproc; // pointer to thunk for dialog box
        lpproc = (DLGPROC)MakeProcInstance((FARPROC)genie_proc, main_inst);
        DialogBox(main_inst,MAKEINTRESOURCE(0x1010),hWnd,lpproc);
        FreeProcInstance((FARPROC)lpproc);
      }
      else if (sele==0x0106) // Reload (e.g for genie codes
      {
        if (get_rom_name()==0) megad.load(rom_name,0);
      }
      else if (sele==0x0101)
      {
        maybe_auto_save_load(1);
        megad.unplug();
        sound_exit();
        sound_init();
        medley=0;
      }
      else if (sele==0x0102) md_res_req=1;
      else if (sele==0x0103)
      {
        DestroyWindow(hWnd);
      }
      else if (sele==0x0104) megad.pause=!megad.pause;
      else if (sele==0x0110) // Load State
      {
        if (get_save_name(0,0)==0) do_save_load(0,0,1,save_name);
      }
      else if (sele==0x0111) // Save State
      {
        if (get_save_name(1,0)==0) do_save_load(1,0,1,save_name);
      }
      else if (sele==0x0310) // Load SRAM
      {
        if (get_save_name(0,1)==0) do_save_load(0,1,1,save_name);
      }
      else if (sele==0x0311) // Save SRAM
      {
        if (get_save_name(1,1)==0) do_save_load(1,1,1,save_name);
      }
      else if (sele==0x0117) auto_saveload=!auto_saveload;
      else if (sele==0x0118) sram_saveload=!sram_saveload;
      else if (sele==0x0112) megad.country_ver=(megad.country_ver&0x0f)+0x80;
      else if (sele==0x0113) megad.country_ver=(megad.country_ver&0x0f)+0x00;
      else if (sele==0x0114) megad.country_ver=(megad.country_ver&0x0f)+0xc0;
      else if (sele==0x0115) megad.country_ver=(megad.country_ver&0x0f)+0x40;
      else if (sele==0x0116) megad.country_ver=(megad.country_ver&0x0f)+0xff0;
      else if ((sele&0xff0)==0x0120)
      {
        megad.country_ver=(megad.country_ver&0xff0)+sele&0x0f;
      }
      else if (sele==0x0130) megad.change_cpu_emu(0);
      else if (sele==0x0131) megad.change_cpu_emu(1);
      else if (sele==0x0140)
      { manual_defining_keys=1; manual_define_keys(); }
      else if (sele==0x0141)
      { manual_defining_keys=2; manual_define_keys(); }
      else if (sele>=0x0142 && sele<0x0150) common_keys(sele-0x0142);
      else if (sele==0x0150) tog_fs_req=1;
      else if (sele==0x0151) split_screen=!split_screen;
      else if (sele==0x0152) crap_tv=!crap_tv;
      else if (sele==0x0153) any_size=!any_size;
      else if (sele==0x0154) dgen_display_on=!dgen_display_on;
      else if (sele==0x0155) megad.render_method=0;
      else if (sele==0x0156) megad.render_method=1;
      else if (sele==0x0157) megad.render_method=2;
      else if (sele==0x0158) megad.render_method=3;
      else if ((sele&0xff0)==0x0330)
      {
        megad.layer_sel^=1<<(sele&15);
      }
      else if (sele==0x01b0)
      {
        megad.mjazz=!megad.mjazz;
        megad.flush_fm_to_mame();
      }
      else if ((sele&0xfe0)==0x0160)
      {
             if (sele==0x0160) ; // reset
        else if (sele==0x0161) stereo16=0;
        else if (sele==0x0162) stereo16=1;
        else if (sele==0x0163) no_sound=!no_sound;
        else if (sele==0x0168) snd_rate=8000;
        else if (sele==0x0169) snd_rate=11025;
        else if (sele==0x016a) snd_rate=22050;
        else if (sele==0x016b) snd_rate=44100;
        else if (sele==0x0170) virtual_fps=50;
        else if (sele==0x0171) virtual_fps=60;
        else if (sele==0x0172) virtual_fps=70;

        sound_exit();
        sound_init();
      }
      else if ((sele&0xf00)==0x200)
      {
        snd_segs=sele&0xff;
        sound_exit();
        sound_init();
      }
      else if (sele==0x0180) megad.snd_mute^=1;
      else if (sele==0x0181) megad.snd_mute^=2;
      else if (sele==0x0182) megad.snd_mute^=4;
      else if ((sele&0xff0)==0x190)
      {
        if (medley<=0)
        {
          medley_record=0; // start recording keypresses
          medley_press_mark=0; medley_had_fire=0;
          medley_nothing_after=0;
          medley_silence=-2*60;
        }
        medley_beep=10;
        medley=(sele&0xf)*30*60;
      }
      else if ((sele&0xff0)==0x1a0) fm_chan_on^=1<<(sele&0xf);
      update_menu();
    }
    case WM_MENUSELECT:
    {
      // Mute sound when user hits a menu
      int tot_size=snd_segs*snd_size*(stereo16?4:1);
      LPVOID ptr1, ptr2; DWORD  size1, size2;
      if (lpBuf!=NULL)
      {
        int ret=lpBuf->Lock(0,tot_size,&ptr1,&size1,&ptr2,&size2,0);
        if (ret == DS_OK && ptr1!=NULL && size1>=tot_size )
        {
          memset(ptr1,stereo16?0:0x80,tot_size);
          lpBuf->Unlock(ptr1, size1, ptr2, size2);
        }
      }
    }
    break;
    case WM_ACTIVATEAPP:
      // wParam true if we are switched to, false if user
      // switches to another program
      we_are_active = wParam;
    break;
    case WM_PAINT: case WM_NCPAINT:
    {
      PAINTSTRUCT ps;
      HDC  hdc = BeginPaint(hWnd, &ps);
      // function calls to repaint main window go here
      if (mddisp)
      {
        mddisp->cls();
        dprintf ("mess %.4x calling ",message);
        mddisp->update(any_size);
      }
      //RECT rect;
      //GetClientRect (hWnd, &rect);
      //SetTextAlign (hdc, TA_BASELINE | TA_CENTER);
      //TextOut(hdc, rect.right / 2, rect.bottom / 2, "DGen", 4);

      EndPaint(hWnd,&ps);
    }
    break;


    case WM_DESTROY:  // message: window being destroyed
      if (really_quit) PostQuitMessage(0);
    break;

    // Passes it on if unproccessed
  }
  // passes it on anyway...
  return (DefWindowProc(hWnd, message, wParam, lParam));
}





int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
			  LPSTR lpCmdLine, int nCmdShow)
{
  MSG msg;      // message
  char *rom_name=NULL;
  main_inst=hInstance;

  if (!hPrevInstance) // Other instances of app running?
  if (InitApplication(hInstance)!=0)  // Initialize shared things
	 return (FALSE); // Exits if unable to initialize

  /* Perform initializations that apply to a specific instance */

//  sound_log=fopen("sound.log","wb");
//  debug_log=fopen("zzdebug.txt","wt");

  GetCurrentDirectory(0x7f0,curr_path);
  strcpy(config_name,curr_path);
  strcat(config_name,"\\dgen.ini");
  load_config(config_name);

  if (state_path[0]==0) strcpy(state_path,curr_path);
  if (rom_path[0]==0)   strcpy(rom_path,curr_path);

  // Wait for a bit (let screen mode change) before creating directdraw
  // may fix +590 bugs?

  direct_draw_init();
  sound_init();


  DirectInputInit(hInstance); // In kinp.cpp

  if (fullscreen)
  {
/*
    if (depth==8)
    {
      ret=make_ddraw_pal(lpdd);
      if (ret!=0)
      {
        sprintf (temp,"Return value was %x (%+-d)\n",ret,ret-MAKE_DDHRESULT(0));
        MessageBox(hWnd,temp,"make_ddraw_pal(lpdd) results",MB_OK);
      }
      ret = lpDDSPrimary->SetPalette(ddpal);
      // this sets the palette for the primary surface
    }
*/
  }

  rom_name=NULL;

  if (lpCmdLine!=NULL && lpCmdLine[0])
  {
    rom_name=lpCmdLine;
  }

  if (rom_name!=NULL)
  {
    megad.load(rom_name,1);
    maybe_auto_save_load(0);
  }



  start_time=timeGetTime();
  int done=0;
  while (!done)
  {
    if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (msg.message==WM_SYSKEYDOWN)
        if (msg.wParam==0xd) tog_fs_req=1;

      if (msg.message == WM_QUIT) done=1;
      else if (msg.message == WM_DESTROY) done=1;
      else
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
    else
    {
      run_megadrive();

      if (we_are_active)
      {
        static int last=0,ths;
        ths=KEY_DOWN(0x70)||KEY_DOWN(0x71)||KEY_DOWN(0x72)||KEY_DOWN(0x76)||
          KEY_DOWN(0x74)||KEY_DOWN(0x76)||KEY_DOWN('R')
          ||KEY_DOWN('P');
        if ((!last) && ths)
        {
          if (KEY_DOWN(0x70)) crap_tv=!crap_tv;
          if (KEY_DOWN(0x71)) split_screen=!split_screen;
          if (KEY_DOWN(0x72)) megad.render_method=(megad.render_method+1)%4;
          if (KEY_DOWN(0x74)) do_save_load(1,0,0,NULL);
          if (KEY_DOWN(0x76)) do_save_load(0,0,0,NULL);
          if (KEY_DOWN(0x12)&&KEY_DOWN('R')) md_res_req=1;
          if (KEY_DOWN(0x12)&&KEY_DOWN('P')) megad.pause=!megad.pause;
          update_menu();
        }
        last=ths;


        if (KEY_DOWN(0x73))
        { int i; for (i=0;i<20;i++) megad.one_frame(NULL,NULL,NULL); }

        float vchange=0.05;
        if (KEY_DOWN(0x10)) vchange=-vchange;
        if (KEY_DOWN('V')) vmul+=vchange;
        if (KEY_DOWN('U')) umul+=vchange;
        if (KEY_DOWN('C')) contrast+=vchange;
        if (KEY_DOWN('B')) brightness+=vchange;

        if (manual_defining_keys) manual_define_update();
        if (tog_fs_req) toggle_fullscreen(); tog_fs_req=0;
        if (md_res_req)
        {
          megad.reset();
          sound_exit();
          sound_init();
          medley=0;
          update_menu();
        }
        md_res_req=0;
      }
    }
  }

  end_time=timeGetTime();
  maybe_auto_save_load(1);
  megad.unplug();

  sound_exit();
  direct_draw_exit();

  {
    int timetaken;
    timetaken=end_time-start_time;
    if (timetaken)
    {
      dprintf ("Frames per second was %.1f\n",
          (float)frame_count/((float)timetaken/1000.0));
      //MessageBox(hWnd,temp,"DGen Performance",MB_OK);
    }
  }

  unload_config(config_name);
  if (sound_log!=NULL) fclose(sound_log); sound_log=NULL;
  if (debug_log!=NULL) fclose(debug_log); debug_log=NULL;

  return 0;
}

