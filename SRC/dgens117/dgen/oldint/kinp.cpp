// DGen v1.11+
// DirectInput routines thanks to Steve Snake
#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <ddraw.h>
#define   DIRECTINPUT_VERSION 0x0500
#include <dinput.h>
#include "../md/md.h"

//------------------------------------------------------------------------------------------------
// Stuff for DGen - Written by Steve Snake sometime in 1998/1999.
//------------------------------------------------------------------------------------------------
// (Dave: fiddled with it a bit...)

//------------------------------------------------------------------------------------------------
// Direct Input Init.
//------------------------------------------------------------------------------------------------

IDirectInput *lpDI=NULL;
IDirectInputDevice *lpDIKeyboard=NULL,*lpDIMouse=NULL;
char temp[0x2000]="";
extern HWND hWnd;

#define MAX_DI_JOYSTICKS 32
static int JoystickCount=0;
static int JoystickX[MAX_DI_JOYSTICKS]={0};
static int JoystickY[MAX_DI_JOYSTICKS]={0};
static int JoystickB[MAX_DI_JOYSTICKS]={0};
static IDirectInputDevice2 *joy_idi[MAX_DI_JOYSTICKS]={NULL};

//------------------------------------------------------------------------------------------------
// Joystick Enumeration Callback.
//------------------------------------------------------------------------------------------------

BOOL CALLBACK
DIEnumJoyCallback(LPCDIDEVICEINSTANCE lpDIIJoy, LPVOID pvRef)
{
	BOOL					bRes;
	DWORD					ButtonCount;

  joy_idi[JoystickCount]=0;

  int ret;
  GUID guid=lpDIIJoy->guidInstance;

	LPDIRECTINPUTDEVICE	lpDIJoystick;
  ret=lpDI->CreateDevice(guid, &lpDIJoystick, NULL);
  dprintf ("Createdevice returned %d\n",ret);
  if(ret!=DI_OK)
    return(DIENUM_CONTINUE);
	
  ret=lpDIJoystick->QueryInterface(IID_IDirectInputDevice2, (void **)&joy_idi[JoystickCount]);
	lpDIJoystick->Release();

  dprintf ("QueryInterface returned %.8x\n",ret);

  if(ret!=DI_OK)
	{
    joy_idi[JoystickCount]=NULL;
    return(DIENUM_CONTINUE);
	}

  ret=(joy_idi[JoystickCount])->SetDataFormat(&c_dfDIJoystick);
  dprintf ("SetDataFormat returned %d\n",ret);
  if(ret!=DI_OK)
	{
    (joy_idi[JoystickCount])->Release();
    joy_idi[JoystickCount]=0;
    return(DIENUM_CONTINUE);
  }

	DIPROPRANGE			diPrg;
	DIPROPDWORD			diPdw;

	diPrg.diph.dwSize		=	sizeof(diPrg);
	diPrg.diph.dwHeaderSize	=	sizeof(diPrg.diph); 
	diPrg.diph.dwObj		=	0;
	diPrg.diph.dwHow		=	DIPH_DEVICE;
	diPrg.lMin				=	-1000; 
	diPrg.lMax				=	+1000;
   
  ret=(joy_idi[JoystickCount])->SetProperty(DIPROP_RANGE, &diPrg.diph);
  dprintf ("SetProperty(DIPROP_RANGE...); returned %d\n",ret);

  if(JoystickCount++==MAX_DI_JOYSTICKS)  return(DIENUM_STOP);
	return(DIENUM_CONTINUE);
}


int DirectInputInit(HINSTANCE hInstance)
{
	HRESULT	hRes;

  dprintf ("DirectInputInit()...\n");

  if(DirectInputCreate(hInstance, DIRECTINPUT_VERSION, &lpDI, NULL)!=DI_OK)
	{
    dprintf ("DirectX 5.0 or higher required.\n");
		return(FALSE);
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Keyboard.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  hRes=lpDI->CreateDevice(GUID_SysKeyboard, &lpDIKeyboard, NULL);
	if(hRes!=DI_OK)
	{
    dprintf ("Cannot Locate Keyboard Device.\n");
		return(FALSE);
	}

  lpDIKeyboard->SetDataFormat(&c_dfDIKeyboard);

//  hRes=lpDIKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
  hRes=lpDIKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
  if(hRes!=DI_OK)
	{
    lpDIKeyboard->Release();
    dprintf ("Cannot Set Keyboard Cooperative Level.\n");
		return(FALSE);
	}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Joystick(s)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  JoystickCount=0;
  hRes=lpDI->EnumDevices(DIDEVTYPE_JOYSTICK, DIEnumJoyCallback, NULL, DIEDFL_ATTACHEDONLY);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Mouse
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  hRes=lpDI->CreateDevice(GUID_SysMouse, &lpDIMouse, NULL);
	if(hRes!=DI_OK)
	{
    dprintf ("Cannot Locate Pointing Device.");
		return(FALSE);
	}

  lpDIMouse->SetDataFormat(&c_dfDIMouse);

  hRes=lpDIMouse->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND);
	if(hRes!=DI_OK)
	{
    lpDIMouse->Release();
    dprintf ("Cannot Set Mouse Cooperative Level.");
		return(FALSE);
	}

  int ret=lpDIKeyboard->Acquire();
  dprintf ("kb acquire returned %.8x\n",ret);

  return(TRUE);
}

static DIJOYSTATE diJoyState[MAX_DI_JOYSTICKS]={{0}};
static unsigned char keys[256]={0};
static unsigned char bukeys[256]={0};

int input_update()
{
  int j,ret=0;
  int di_failed=1;

  if (lpDIKeyboard)
  {
    ret=lpDIKeyboard->GetDeviceState(256, keys);
    if (ret==DIERR_INPUTLOST)
    {
      ret=lpDIKeyboard->Acquire();
      ret=lpDIKeyboard->GetDeviceState(256, keys);
    }

    if (ret==DI_OK) di_failed=0;
  }

// Our backup 0x200+ codes if DirectInput isn't working
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
  if (di_failed)
    for (j=0;j<256;j++) bukeys[j]=KEY_DOWN(j);


  for (j=0;j<JoystickCount;j++)
  {
    if (joy_idi[j])
    {
      ret=joy_idi[j]->Acquire();
      if (ret==DI_OK)
      {
        joy_idi[j]->Poll();
        joy_idi[j]->GetDeviceState(sizeof(diJoyState[j]), &diJoyState[j]);
      }
      joy_idi[j]->Unacquire();
    }
  }

  return 0;
}

// Good for redefining keys (pass -1,returns code) OR checking keys
// (pass code, returns 0/1)
int input_check(int code)
{
  int i,j,s=0; // s is true if the control we are looking at is pressed

  // Wait for something to be pressed and then return a code for it
  // Must call input_acquire first

  for (i=0;i<256;i++)
  {
    s=keys[i];
    if (code==-1) { if (s) return 0x0000+i; }
    else if (code==0x0000+i) return s;
  }

  for (i=0;i<256;i++)
  {
    s=bukeys[i];
    if (code==-1) { if (s) return 0x0200+i; }
    else if (code==0x0200+i) return s;
  }

  for (j=0;j<JoystickCount;j++)
  {
    int codebase;
    codebase=0x10000+j*0x100;
    if (joy_idi[j])
    {
      int b;
      int s;

      s=(diJoyState[j].lY<-500);
      if (code==-1) { if (s) return codebase+0x01; }
      else if (code==codebase+0x01) return s;

      s=(diJoyState[j].lY>+500);
      if (code==-1) { if (s) return codebase+0x02; }
      else if (code==codebase+0x02) return s;

      s=(diJoyState[j].lX<-500);
      if (code==-1) { if (s) return codebase+0x03; }
      else if (code==codebase+0x03) return s;

      s=(diJoyState[j].lX>+500);
      if (code==-1) { if (s) return codebase+0x04; }
      else if (code==codebase+0x04) return s;

      for (b=0;b<32;b++)
      {
        s=(diJoyState[j].rgbButtons[b]);
        if (code==-1) { if (s) return codebase+0x10+b; }
        else if (code==codebase+0x10+b) return s;
      }
    }
  }

  if (code==-1) return -1; // Nothing pressed
  return 0; // Unknown code passed
}



//
/*
//------------------------------------------------------------------------------------------------
// Read Keyboard State.
//------------------------------------------------------------------------------------------------

void
DirectInputReadKeyboard(void)
{
	HRESULT	hRes;

	if(KGen.KeyboardAcquired==FALSE)
	{
		KGen.KeyboardAcquired=TRUE;
    lpDIKeyboard->Acquire();
	}

  hRes=lpDIKeyboard->GetDeviceState(256, &KGen.KeyStatus[0]);
	if(hRes==DIERR_INPUTLOST)
	{
		SetMyWindowFocus();
    lpDIKeyboard->Acquire();
    hRes=lpDIKeyboard->GetDeviceState(256, &KGen.KeyStatus[0]);
		if(hRes!=DI_OK)
		{
			DXMsg("Cannot Read Keyboard State.", "DirectInput Acc Error #4:");
		}
	}
}


//------------------------------------------------------------------------------------------------
// Read Mouse State.
//------------------------------------------------------------------------------------------------

void
DirectInputReadMouse(void)
{
	HRESULT			hRes;
	DIMOUSESTATE	DIMouseState;

	if(KGen.MouseAcquired==FALSE)
	{
    lpDIMouse->Acquire();
		KGen.MouseAcquired=TRUE;
		KGen.MouseX=160;
		KGen.MouseY=120;
	}

  hRes=lpDIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &DIMouseState);
	if(hRes==DIERR_INPUTLOST)
	{
		SetMyWindowFocus();
    lpDIMouse->Acquire();
    hRes=lpDIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &DIMouseState);
	}
	if(hRes!=DI_OK)
	{
		DXMsg("Cannot Read Mouse State.", "DirectInput Acc Error #6:", hRes);
	}
	else
	{
		KGen.MouseX+=DIMouseState.lX;
		KGen.MouseY+=DIMouseState.lY;

		if((abs(DIMouseState.lX)>=3) || (abs(DIMouseState.lY)>=3))
			KGen.HideMouse=0;

		KGen.DMouseButton=KGen.MouseButton^0x80;
		KGen.MouseButton=DIMouseState.rgbButtons[0];
		KGen.DMouseButton&=KGen.MouseButton;

		if(KGen.MouseX<0)	KGen.MouseX=0;
		if(KGen.MouseX>319)	KGen.MouseX=319;
		if(KGen.MouseY<8)	KGen.MouseY=8;
		if(KGen.MouseY>220)	KGen.MouseY=220;
	}
}




//------------------------------------------------------------------------------------------------
// This file contains all the code to emulate a Genesis Joypad.
// It maps the Keyboard and PC Joypads to the Emulated Genesis values.
//------------------------------------------------------------------------------------------------

void
JoypadHandler(void)
{
	DWORD	i, j;
	BYTE	ButIdx[]=
	{
		0x00, 0x40, 0x10, 0x50, 0x20, 0x60, 0x30, 0x70,
		0x80, 0xc0, 0x90, 0xd0, 0xa0, 0xe0, 0xb0, 0xf0
	};

	Joypads[0]=Joypads[1]=0;

// Handle Keyboard controls.

	if(KGen.PlayerMode==1)
	{
		for(i=0; i<8; i++)
		{
			if((!KGen.JoySelect[0]) && (KGen.KeyStatus[KGen.Player1Keys[i]]))	Joypads[0]|=(1<<i);
			if((!KGen.JoySelect[1]) && (KGen.KeyStatus[KGen.Player2Keys[i]]))	Joypads[1]|=(1<<i);
		}
	}
	else
	{
		for(i=0; i<8; i++)
		{
			if((!KGen.JoySelect[0]) && (KGen.KeyStatus[KGen.Player1SKeys[i]]))	Joypads[0]|=(1<<i);
		}
	}

// Handle Joystick controls.

	for(i=0; i<2; i++)
	{
		if(KGen.JoySelect[i])
		{
			if(KGen.JoystickY[i]<0)	Joypads[i]|=1;
			if(KGen.JoystickY[i]>0)	Joypads[i]|=2;
			if(KGen.JoystickX[i]<0)	Joypads[i]|=4;
			if(KGen.JoystickX[i]>0)	Joypads[i]|=8;

			Joypads[i]|=((KGen.JoystickB[i]<<4)&0xf0);

// Handle extra Key controls if Joystick with < 4 buttons.

			if(KGen.JoystickButs[KGen.JoySelect[i]-1]<4)
			{
				for(j=4; j<8; j++)
				{
					if((i==0) && (KGen.KeyStatus[KGen.Player1EKeys[j]]) && (KGen.Player1EKeys[j]))	Joypads[0]|=(1<<j);
					if((i==1) && (KGen.KeyStatus[KGen.Player2EKeys[j]]) && (KGen.Player2EKeys[j]))	Joypads[1]|=(1<<j);
				}
			}
		}	
	}

	Joypads[0]=((Joypads[0]&0x0f) | (ButIdx[Joypads[0]>>4]));
	Joypads[1]=((Joypads[1]&0x0f) | (ButIdx[Joypads[1]>>4]));

	Joypads[0]^=0xff;
	Joypads[1]^=0xff;
}

*/

