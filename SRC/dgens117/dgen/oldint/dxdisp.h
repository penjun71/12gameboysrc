// dxdisp.h
#define   DIRECTDRAW_VERSION 0x0300
#include <ddraw.h>

#ifndef STRUCT_BMAP
struct bmap { unsigned char *data; int w,h; int pitch; int bpp; };
#define STRUCT_BMAP
#endif

// MD DirectX Display stuff
class dxdisp
{
  // DirectX Display (e.g. for a MD display)
  int okay;
  HWND wnd; int fullscreen;
  struct bmap bm;
  IDirectDraw *dd;
  IDirectDrawSurface *prim,*buf;
  IDirectDrawClipper *clipper;
  int directdraw_init();
  int directdraw_exit();
public:
  dxdisp(HWND iwnd,int ifullscreen);
  ~dxdisp();
  int cls(); int update(int any_size);
  int exit();
  struct bmap lock();
  int unlock();
};

extern "C" int dprintf(char *format, ...);
