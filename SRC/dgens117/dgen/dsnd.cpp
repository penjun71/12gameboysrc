#include <stdio.h>
#include <windows.h>
#include <dsound.h>
#include "d.h"

int snd_size=367*1,snd_rate=22050*1,snd_segs=4,stereo16=1;
static IDirectSound *lpds=NULL;
static IDirectSoundBuffer *lpBufPrim,*lpBuf;
#define DIRECTSOUND_OKAY (lpds&&lpBufPrim&&lpBuf)

int directsound_init(HWND hWnd)
{
  int ret=0;
  if (hWnd==0) return 1; if (snd_segs<=0) snd_segs=1;
  dprintf ("directsound_init()\n");

  snd_size=snd_rate/60; if (snd_size<16) snd_size=16;

  ret=DirectSoundCreate(NULL, &lpds, NULL);
  if (ret!=DS_OK) return 1;

  ret=lpds->SetCooperativeLevel(hWnd, DSSCL_EXCLUSIVE);
  if (ret!=DS_OK)
  {
    dprintf ("DSound SetCooperativeLevel() returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    if (ret!=DS_OK) return 1;
  }

	PCMWAVEFORMAT	pcmwf;
	WAVEFORMATEX	wf;

	memset(&pcmwf, 0, sizeof(PCMWAVEFORMAT)); 
	memset(&wf,0,sizeof(WAVEFORMATEX));
  pcmwf.wBitsPerSample = stereo16? 16:8; 
	pcmwf.wf.wFormatTag = WAVE_FORMAT_PCM; 
  pcmwf.wf.nChannels = stereo16? 2:1 ; 
  pcmwf.wf.nSamplesPerSec = snd_rate;
	pcmwf.wf.nBlockAlign = pcmwf.wf.nChannels * (pcmwf.wBitsPerSample/8); 
	pcmwf.wf.nAvgBytesPerSec = pcmwf.wf.nSamplesPerSec * pcmwf.wf.nBlockAlign; 

	wf.wFormatTag = WAVE_FORMAT_PCM;
	wf.nChannels = pcmwf.wf.nChannels;
	wf.nSamplesPerSec = pcmwf.wf.nSamplesPerSec;
	wf.wBitsPerSample = pcmwf.wBitsPerSample;
	wf.nBlockAlign = pcmwf.wf.nBlockAlign;
	wf.nAvgBytesPerSec = pcmwf.wf.nAvgBytesPerSec;
	wf.cbSize = sizeof(WAVEFORMATEX);

	//--PrimaryBuffer make
	DSBUFFERDESC dsbdesc;
  memset(&dsbdesc, 0, sizeof(DSBUFFERDESC));
  dsbdesc.dwSize = sizeof(DSBUFFERDESC);
  dsbdesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
  ret=lpds->CreateSoundBuffer(&dsbdesc,&lpBufPrim,NULL);
  if (ret!=DS_OK)
  {
    dprintf ("DSound create primary buffer returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    if (ret!=DS_OK) return 1;
  }
  ret=lpBufPrim->SetFormat(&wf);
  if (ret!=DS_OK)
  {
    dprintf ("DSound primary buffer set format returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    if (ret!=DS_OK) return 1;
  }
  ret=lpBufPrim->Play(0,0,DSBPLAY_LOOPING);
  if (ret!=DS_OK)
  {
    dprintf ("DSound primary buffer play returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    if (ret!=DS_OK) return 1;
  }

	//--SecondaryBuffer Make
  memset(&dsbdesc, 0, sizeof(DSBUFFERDESC));
  dsbdesc.dwSize = sizeof(DSBUFFERDESC);
  dsbdesc.dwFlags = DSBCAPS_CTRLDEFAULT  | DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_STICKYFOCUS;
  dsbdesc.dwBufferBytes = snd_size*snd_segs*(stereo16?4:1); 
  dsbdesc.lpwfxFormat = (LPWAVEFORMATEX)&pcmwf;

  ret=lpds->CreateSoundBuffer(&dsbdesc,&lpBuf,NULL);
  if (ret!=DS_OK)
  {
    dprintf ("DSound create secondary sound buffer returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    if (ret!=DS_OK) return 1;
  }

  ret=lpBuf->Play(0,0,DSBPLAY_LOOPING);
  if (ret!=DS_OK)
  {
    dprintf ("DSound play secondary sound buffer returned %.8x (+%d)\n",ret,ret-MAKE_DSHRESULT(0));
    if (ret!=DS_OK) return 1;
  }


  return 0;
}

int directsound_silence()
{
  int tot_size,ret;
  LPVOID ptr1, ptr2; DWORD  size1, size2;
  tot_size=snd_segs*snd_size*(stereo16?4:1);
  if (lpBuf==NULL) return 1;

  ret=lpBuf->Lock(0,tot_size,&ptr1,&size1,&ptr2,&size2,0);
  if (ret == DS_OK && ptr1!=NULL && size1>=tot_size )
  {
    memset(ptr1,stereo16?0:0x80,tot_size);
    lpBuf->Unlock(ptr1, size1, ptr2, size2);
  }
  return 0;
}

static int write_ptr=0;
int directsound_need()
{
  DWORD posPlay,posWrite;
  int read_ptr=0,next_ptr=0;
  if (!DIRECTSOUND_OKAY) return 0;

  lpBuf->GetCurrentPosition(&posPlay,&posWrite);

  read_ptr=posPlay/(snd_size*(stereo16?4:1));

  next_ptr=write_ptr+1; next_ptr%=snd_segs;

  if (next_ptr==read_ptr)  return 2; // sound and picture
  if (write_ptr!=read_ptr) return 1; // just sound
  return 0;
}

int directsound_give(signed short *newsnd[2])
{
  LPVOID ptr1, ptr2;
	DWORD  size1, size2;
  int ret=0;
  if (!DIRECTSOUND_OKAY) return 1;

// Sound buffer loop round a number of segs of size snd_size
// SSSSSSSSS SSSSSSSSS SSSSSSSSS SSSSSSSSS 
//            Playing   Writing

  ret=lpBuf->Lock(write_ptr*snd_size*(stereo16?4:1),snd_size*(stereo16?4:1),&ptr1,&size1,&ptr2,&size2,0);
  if (ret != DS_OK) dprintf ("DSound: lpBuf->Lock() returned %x\n",ret);
  if (ret != DS_OK) return 1;

  if (ptr1==NULL) dprintf ("ptr1 was NULL\n");

  if (ptr1!=NULL)
  {
    signed short *convsnd16; unsigned char *convsnd8;
    int j;
    convsnd16=(signed short *)ptr1;
    convsnd8=(unsigned char *)ptr1;
    if (newsnd!=NULL)
    {
      for (j=0;j<snd_size;j++)
      {
        int disp;
        if (stereo16)
        {
          convsnd16[j*2+0]=newsnd[0][j];
          convsnd16[j*2+1]=newsnd[1][j];
        }
        else
        {
          disp=0;
          disp+=((int)newsnd[0][j]);
          disp+=((int)newsnd[1][j]);
          disp>>=9;
          convsnd8[j]=disp+0x80;
        }
      }
    }
    lpBuf->Unlock(ptr1, size1, ptr2, size2);
  }

  write_ptr++; write_ptr%=snd_segs;

  return 0;
}

int directsound_exit()
{
  dprintf ("directsound_exit()\n");

  //-- DirectSound Cleanup
  if(lpBuf)   lpBuf->Release(); lpBuf=NULL;
  if(lpBufPrim) lpBufPrim->Release(); lpBufPrim=NULL;
  if(lpds)    lpds->Release(); lpds=NULL;

  return 0;
}
