#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "d.h"

// Text console

static HWND debug_win=0;
static char temp[0x1000]="";
static FILE *errfile=stderr;

static wconsole *dcon=0;

LRESULT CALLBACK text_win_proc
  (HWND wnd,UINT cmd,WPARAM wparam,LPARAM lparam)
{
  if (cmd==WM_DESTROY)
  {
    PostQuitMessage(0);
    return 0;
  }

  if (cmd>=WM_USER && cmd<WM_USER+65536)
  {
    net_recv(cmd-WM_USER);
    return 0;
  }

  if (dcon) return dcon->proc(cmd,wparam,lparam);

  // Let windows handle everything else
  return DefWindowProc(wnd,cmd,wparam,lparam);
}                                     

static int text_init(HINSTANCE hinst)
{
  static WNDCLASS wclass;
  memset(&wclass,0,sizeof(wclass));
  wclass.style = 0; //CS_HREDRAW | CS_VREDRAW;
  wclass.lpszClassName="text";
  wclass.hInstance=hinst;
  wclass.lpfnWndProc=text_win_proc;
  wclass.hbrBackground=GetStockObject(BLACK_BRUSH);
  wclass.hCursor = LoadCursor(NULL, IDC_ARROW);
//  wclass.lpszMenuName = "NETPMENU";  // Name of menu resource in .RC file.
  RegisterClass(&wclass);
  return 0;
}


static char *check_for(char *cmd,char *var)
{
  int ln;
  ln=strlen(var);
  if (strncmp(cmd,var,ln)==0) return cmd+ln;
  return NULL;
}

extern char dest_name[0x1000];
extern int dest_port;

int dcon_proc(char *cmd,console *con)
{
  char *v; int parse=0;
#define DVAR(x)if (v=check_for(cmd,#x"=")) { x=atoi(v); parse=1; }\
  if (v=check_for(cmd,"print "))\
  if (check_for(v,#x)) { dprintf (#x" is %d\n",x); parse=1; }

#define SVAR(x)if (v=check_for(cmd,#x"=")) { strcpy(x,v); parse=1; }\
  if (v=check_for(cmd,"print "))\
  if (check_for(v,#x)) { dprintf (#x" is %s\n",x); parse=1; }

  DVAR(dest_port)
  SVAR(dest_name)

  if (v=check_for(cmd,"net_init")) { net_init(); parse=1; }
  if (v=check_for(cmd,"net_exit")) { net_exit(); parse=1; }

  if (check_for(cmd,"exit")) { if (debug_win) DestroyWindow(debug_win); }
  if (v=check_for(cmd,"server"))
    { net_server(debug_win,atoi(v)); parse=1; }

  if (v=check_for(cmd,"client"))
  {
    parse=1;
    if (v[0]) net_client(v+1); else net_client(NULL);
  }

  if (!parse)// con->puts("Command unrecognised\n",2);
    net_client(cmd);
  return 0;
}

int WINAPI WinMain
  (HINSTANCE hinst,HINSTANCE prev_inst,LPSTR cmd_line,int show_code)
{
  int ret=0,done=0;
  MSG msg;
  errfile=stderr;
  static FILE *debug_log=NULL;

  debug_log=fopen("zzdebug.txt","wt");
  if (debug_log!=NULL) errfile=debug_log;

  text_init(hinst); // Registers the main WNDCLASS

  debug_win=CreateWindow("text","Text",
//                             WS_OVERLAPPEDWINDOW     |
//                             WS_THICKFRAME     |
                             WS_SYSMENU        |
                             WS_MINIMIZEBOX       |
//                             WS_VSCROLL       |
    0,
   CW_USEDEFAULT,         // Default horizontal position.
   CW_USEDEFAULT,          // Default vertical position.
    //80,60,
    50*8 +4+4,17*15 +4+22+2,
    0,0,hinst,0);

  ShowWindow(debug_win,SW_NORMAL); //show_code);
//  UpdateWindow(debug_win); // Sends WM_PAINT

  dcon=new wconsole(debug_win);
  dcon->cmd_proc=dcon_proc;
  dcon->puts("TEXT DCon test v0.01\n",3);
  net_init();

  while (!done)
  {
    GetMessage(&msg, NULL, 0, 0);
    {
      if (msg.message == WM_QUIT) done=1;
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }

  net_exit();
  if (dcon) delete dcon;

  if (debug_log!=NULL) fclose(debug_log); debug_log=NULL;
  errfile=stderr;

  return msg.wParam; // Returns the value from PostQuitMessage
}

extern "C" int dprintf(char *format, ...)
{
  static int reentry=0;
  if (reentry) return 1; reentry=1;
  va_list arg;
  va_start(arg,format);
  vsprintf (temp,format,arg);
  if (dcon) dcon->puts(temp,0);
  va_end(arg);
  reentry=0; return 0;
}

