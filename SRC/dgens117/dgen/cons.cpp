#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "d.h"

int console::reset_prompt()
{
  strcpy(enter,"D> "); curs=3; search_back=h;// enter line cursor
  changed=1;
  return 0;
}

console::console(int iw,int ih,int iew)
{
  int x,y; char *tc;
  prinx=w=h=ew=0; cmd_proc=0; text=NULL; enter=NULL; changed=1;

  // First char in each line is attr
  text=(char *)malloc((iw+1)*ih);
  if (text==NULL) return;
  tc=text;
  w=iw; h=ih;
  for (y=0;y<h;y++) { *(tc++)=0; for (x=0;x<w;x++) *(tc++)=0; }

  enter=(char *)malloc(iew);
  if (enter==NULL) return;
  ew=iew;
  reset_prompt();
}

console::~console()
{
  if (text!=NULL) free(text); text=NULL;
  if (enter!=NULL) free(enter); enter=NULL; 
  w=h=ew=0;
}                                 

int console::puts(char *txt,char attr)
{
  char *nw; int done=0;
  nw=text+(h-1)*(w+1); *(nw++)=attr; nw+=prinx;

  while (!done)
  {
    int newline=0;
    if (prinx>w-2) newline=1;
    if (*txt=='\n') { txt++; newline=1; }

    if (newline)
    {
      *nw=0; prinx++;
      memmove(text,text+w+1,(h-1)*(w+1)); // scroll text up
      nw=text+(h-1)*(w+1); *(nw++)=attr; prinx=0; *nw=0;
    }
    else
    { *nw++=*txt++; prinx++; }

    if (*txt==0) done=1;
  }

  changed=1;
  return 0;
}


int console::del()
{
  if (curs>=ew-1) return 1;
  int right=strlen(enter+curs)+1;
  if (right<=1) return 1;
  if (right > ew-2-curs ) right = ew-2-curs;
  memmove(enter+curs,enter+curs+1,right);
  return 0;
}

int console::check_search(int d)
{
  int max; char *line;
  if (d<0 && search_back+d >=0) search_back+=d;
  if (d>0 && search_back+d <=h) search_back+=d;

  while (search_back>=0 && search_back<h)
  {
    line=text+search_back*(w+1)+1;
    if (memcmp(line,"D> ",3)==0)
    {
      max=w; if (max>ew) max=ew;
      memcpy(enter,line,max);
      curs=strlen(enter);
      return 0;
    }
    search_back+=d;
  }
  if (search_back>=h) { reset_prompt(); return 0; }
  return 1;
}

int console::specialtype(int ch)
{
  if (enter==NULL) return 1;
       if (ch==0x24) curs=3;
  else if (ch==0x23) curs=strlen(enter);
  else if (ch==0x25 && curs>3) curs--;
  else if (ch==0x27 && curs<strlen(enter)) curs++; 
  else if (ch==0x2e) del();
  else if (ch==0x26) check_search(-1);
  else if (ch==0x28) check_search(+1); 
  else return 0;

  changed=1;
  return 0;
}

int console::type(int ch)
{
  if (enter==NULL) return 1;
  changed=1;

  if (ch==0x08 && curs>3)
  { curs--; del(); return 0; }

  if (ch=='\r')
  {
    curs=strlen(enter); if (curs>ew-2) curs=ew-2;
    enter[curs+0]='\n'; enter[curs+1]=0;
    puts(enter,1);

    enter[curs+0]=0;
    if (cmd_proc) cmd_proc(enter+3,this);

    reset_prompt();
    return 0;
  }

  if (ch>=' ' && curs>=0 && curs<ew-2)
  {
    int right=strlen(enter+curs);
    if (right > ew-2-curs-1 ) right = ew-2-curs-1;
    memmove(enter+curs+1,enter+curs,right);
    enter[curs+1+right]=0;
    enter[curs]=ch; curs++;
  }
  return 0;
}

static HFONT make_nicefont()
{
  HFONT font;
  LOGFONT lf;
  memset(&lf,0,sizeof(lf));

  lf.lfWidth=0;

  lf.lfHeight=14;
  lf.lfPitchAndFamily=FIXED_PITCH|FF_DONTCARE;
  strcpy((char *)lf.lfFaceName, "System");

  font = CreateFontIndirect((LPLOGFONT) &lf);
  return font;
}


wconsole::wconsole(HWND iwnd,int iw,int ih,int iew)
: console(iw,ih,iew)
{
  wnd=iwnd;
  font=make_nicefont();
}

wconsole::~wconsole()
{
  wnd=0; if (font) DeleteObject(font); font=0;
}

int wconsole::proc(UINT cmd,WPARAM wparam,LPARAM lparam)
{
  if (cmd==WM_CREATE)
  {
//    SetScrollRange(wnd,SB_VERT,0,10,TRUE);
  }
  if (cmd==WM_PASTE)
  {
    char *clip;
    if (OpenClipboard(wnd)==0) return 0;
    clip=(char *)GetClipboardData(CF_TEXT);
    if (clip!=NULL) while (*clip) type(*clip++);
    CloseClipboard();
  }
  if (cmd==WM_CHAR)
  {
    if (wparam=='V'-'@') { SendMessage(wnd,WM_PASTE,0,0); return 0; }
    type(wparam);
  }
  if (cmd==WM_KEYDOWN)
  {
    specialtype(wparam);
  }

  if (cmd==WM_NCPAINT) // not sure about this
    wm_paint();
  if (cmd==WM_PAINT)
    wm_paint();

  // Let windows handle everything else
  return DefWindowProc(wnd,cmd,wparam,lparam);
}

wconsole::wm_paint()
{
  PAINTSTRUCT ps; int y;
  HDC hdc;
  hdc=BeginPaint(wnd,&ps);
  if (hdc==0) return 1;
  if (font) SelectObject(hdc,font);

  for (y=0;y<h;y++)
  {
    char *line;
    COLORREF cr,cb;
    cb=RGB(0x00,0x00,0x00);
    cr=RGB(0xd0,0xd0,0xd0);

    line=text+y*(w+1);
    if      (line[0]==1) cr=RGB(0xb0,0xd0,0xff);
    else if (line[0]==2) cr=RGB(0xff,0x50,0x00);
    else if (line[0]==3)
      { cb=RGB(0x00,0x00,0xf0); cr=RGB(0xff,0xff,0xff); }

    SelectObject(hdc,GetStockObject(BLACK_BRUSH));
    SelectObject(hdc,GetStockObject(BLACK_PEN));
    if (y==search_back)
    {
      SelectObject(hdc,GetStockObject(WHITE_PEN));
      cb=RGB(0xf0,0x00,0x00);
    }
    Rectangle(hdc,0,y*15,w*8,(y+1)*15);
    SetBkColor(hdc,cb); SetTextColor(hdc,cr);
    TextOut(hdc,0,y*15,line+1,strlen(line+1));
  }

  SelectObject(hdc,GetStockObject(BLACK_BRUSH));
  SelectObject(hdc,GetStockObject(BLACK_PEN));
  Rectangle(hdc,0,h*15,w*8,(h+1)*15);

  SetBkColor  (hdc,RGB(0x00,0x00,0x00));
  SetTextColor(hdc,RGB(0xb0,0xb0,0xd0));
  TextOut(hdc,0,h*15,enter,strlen(enter));

  // cursor
  SetBkColor  (hdc,RGB(0xff,0xff,0xff));
  SetTextColor(hdc,RGB(0xff,0x00,0x00));
  {
    char cc=' ';
    if (curs>=0 && curs<strlen(enter) ) cc=enter[curs];
    TextOut(hdc,curs*8,h*15,&cc,1);
  }

  EndPaint(wnd,&ps);
  return 0;
}

// Best to call this in the idle part of the PeekMessage loop
int wconsole::update()
{
  if (changed)
  {
    InvalidateRect(wnd,NULL,0);
    UpdateWindow(wnd);
  }
  changed=0;
  return 0;
}
