// d.h - header for everything

#ifdef _INC_WINDOWS
// Net stuff
int net_init();
int net_exit();
int net_server(HWND wnd,int bind_port);
int net_recv(int soc);
int net_client(char *mess);
#endif

#ifndef STRUCT_BMAP
struct bmap { unsigned char *data; int w,h; int pitch; int bpp; };
#define STRUCT_BMAP
#endif

// MD DirectX Display stuff
#ifdef _INC_WINDOWS
#ifdef __DDRAW_INCLUDED__
class dxdisp
{
  // DirectX Display (e.g. for a MD display)
  int okay;
  HWND wnd; int fullscreen;
  struct bmap bm;
  IDirectDraw *dd;
  IDirectDrawSurface *prim,*buf;
  IDirectDrawClipper *clipper;
  int directdraw_init();
  int directdraw_exit();
public:
  dxdisp(HWND iwnd,int ifullscreen);
  ~dxdisp();
  int paint(); int update();
  int exit();
  struct bmap lock();
  int unlock();
};
#endif
#endif

// Graphics stuff

#ifdef __DDRAW_INCLUDED__
extern IDirectDrawSurface *create_surface
  (IDirectDraw *lpdd,int width,int height);

extern IDirectDrawSurface *create_primary_surface(IDirectDraw *lpdd);

extern int lock_surface_to_bm(struct bmap *sbm,IDirectDrawSurface *lpsurf);

extern IDirectDrawClipper *attach_clipper
  (IDirectDraw *lpdd,HWND hWnd,IDirectDrawSurface *lpprim);

#endif

// Steve's DirectInput stuff KINP.CPP
#ifdef _INC_WINDOWS
int directinput_init(HINSTANCE hInstance,HWND hWnd);
int directinput_exit();
int directinput_update();
int directinput_check(int code,unsigned int jignore);
#endif

// In MDINP.CPP
int get_megad_pad(int pad[2],int ignore);

// In DSND.CPP
#ifdef _INC_WINDOWS
int directsound_init(HWND hWnd);
int directsound_exit();
int directsound_need();
int directsound_give(signed short *newsnd[2]);
int directsound_silence();
#endif

// Text console stuff

class console
{
private:
  int del(); int prinx;
  int check_search(int d);
  int reset_prompt();
protected:
  int w,h,ew; int search_back;
  char *text,*enter;
  int curs;
public:
  console(int iw,int ih,int iew);
  ~console();
  int type(int ch);
  int specialtype(int ch);
  int puts(char *txt,char attr);
  int (*cmd_proc) (char *cmd,console *con);
  int changed;
};

#ifdef _INC_WINDOWS
class wconsole : public console
{
  HFONT font;
public:
  HWND wnd;
  wconsole::wconsole(HWND iwnd,int iw=50,int ih=16,int iew=256);
  ~wconsole();
  int wm_paint();
  int update();
  int proc(UINT cmd,WPARAM wparam,LPARAM lparam);
};
#endif


extern "C" int dprintf(char *format, ...);
int udp_stuff();
