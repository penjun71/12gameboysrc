{
	int psx, psy; // playfield size
	int x, scan, width, xStart;
	static int playInfo[4]={0x20,0x40,0x40,0x80};
	unsigned int tile;
	byte *tileAddr;
	word *pix;
	byte *scrollList;
	byte *tileList, *tileCurrentLine;
	int tileOffsetY, tileOffsetX, tileOffsetXMask;
	int xScroll, yScroll;

#if PLANE == 0
  // Plane 0 is only where the window isn't
  // This should make Herzog Zwei split screen work perfectly, and clean
  // up those little glitches on Sonic 3's level select.
  if(reg[18] & 0x80) // Window goes down, plane 0 goes up! :)
    {
      if((scanline>>3) >= (reg[18]&0x1f)) return;
    } else { // Window goes up, plane 0 goes down
      if((scanline>>3) < (reg[18]&0x1f)) return;
    }
#endif


	psx = playInfo[  reg[0x10]       & 3 ] << 1;
	psy = playInfo[ (reg[0x10] >> 4) & 3 ];
	
#if PLANE == 0
	yScroll    = Get16( vsram   );                       
	scrollList = vram + ((reg[0xd]<<10) & 0xFC00); 
	tileList   = vram +  (reg[0x2]<<10);           
#elif PLANE == 1 
	yScroll    = Get16( vsram+2 ); 
	scrollList = vram + ((reg[0xd]<<10) & 0xFC00) + 2; 
	tileList   = vram +  (reg[0x4]<<13);
#else
	#error Plane can only be 0 or 1
#endif

	if( reg[0xc] & 2 ) 
	{
		yScroll >>= 1; // interlace mode
	}
	
	yScroll += scanline;
	
	if( reg[0xc] & 1 ) 
	{
		width=40; // wide mode   (320 pixels across)
		xStart=-8;
	}
	else
	{
		width=32; // narrow mode (256 pixels across)
		xStart=24;
	}
	
	switch( reg[0xb] & 3 )
	{
		default:
			break;
			
		case 2: // offset per cel
			scrollList += (scanline & ~7) << 2;
			break;
		
		case 3: // offset per scan
			scrollList += scanline << 2;
			break;
	}
	
	xScroll         = Get16( scrollList );

	tileOffsetXMask = psx-1;

	tileOffsetY     = (yScroll>>3) & (psy-1);
	tileOffsetX     = ((-(xScroll>>3)-1)<<1) & tileOffsetXMask;

	tileCurrentLine = tileList + psx*tileOffsetY;
	scan = yScroll & 7;
	
	pix = dstPix + xStart + (xScroll & 7);
	
	for( x=-1; x<width; x++ )
	{		
		tileAddr = tileCurrentLine + tileOffsetX;
		tile = Get16(tileAddr);

#if (FRONT == 0) && (PLANE == 1)
		DrawMDSolid( scan, pix, tile );
#elif FRONT == 1
		if( tile >> 15 ) DrawMDTile( scan, pix, tile );
#else
		if( !(tile >> 15) ) DrawMDTile( scan, pix, tile );
#endif
				
		pix += 8;
		tileOffsetX = (tileOffsetX + 2) & tileOffsetXMask;
	}
}
