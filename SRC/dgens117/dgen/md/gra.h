// DGen - header file for gra.asm

extern "C"
{
int clear_to_col_8 (unsigned char *dest,int len,unsigned int col);
int clear_to_col_16(unsigned char *dest,int len,unsigned int col);
int clear_to_col_24(unsigned char *dest,int len,unsigned int col);
int clear_to_col_32(unsigned char *dest,int len,unsigned int col);

int draw_tile_8 (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_8_hf (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_8_vf (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_8_hf_vf (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_16(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_16_hf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_16_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_16_hf_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_24(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_24_hf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_24_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_24_hf_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_32(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_32_hf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_32_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_32_hf_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_i_8 (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_8_hf (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_8_vf (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_8_hf_vf (unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_i_16(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_16_hf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_16_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_16_hf_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_i_24(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_24_hf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_24_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_24_hf_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);

int draw_tile_i_32(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_32_hf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_32_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
int draw_tile_i_32_hf_vf(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette);
}

//typedef 
//(extern "C" int *)(unsigned char *dest,int pitch,unsigned char *tilep,unsigned int *palette)
//tile_function;

typedef int (*tilefn) (unsigned char *,int,unsigned char *,unsigned int *);
static tilefn tf_tab[32]=
{
  draw_tile_8,draw_tile_8_hf,draw_tile_8_vf,draw_tile_8_hf_vf,
  draw_tile_16,draw_tile_16_hf,draw_tile_16_vf,draw_tile_16_hf_vf,
  draw_tile_24,draw_tile_24_hf,draw_tile_24_vf,draw_tile_24_hf_vf,
  draw_tile_32,draw_tile_32_hf,draw_tile_32_vf,draw_tile_32_hf_vf,
  draw_tile_i_8,draw_tile_i_8_hf,draw_tile_i_8_vf,draw_tile_i_8_hf_vf,
  draw_tile_i_16,draw_tile_i_16_hf,draw_tile_i_16_vf,draw_tile_i_16_hf_vf,
  draw_tile_i_24,draw_tile_i_24_hf,draw_tile_i_24_vf,draw_tile_i_24_hf_vf,
  draw_tile_i_32,draw_tile_i_32_hf,draw_tile_i_32_vf,draw_tile_i_32_hf_vf
};

