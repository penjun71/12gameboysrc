// DGen

#define VER "1.17"

#define COMPILE_WITH_STAR
#define COMPILE_WITH_MUSA
#define ASM_TILES
#define ASM_CTV
// change makefile too

#ifdef COMPILE_WITH_STAR
#ifndef __STARCPU_H__
#include "starcpu.h"
#endif
#endif


#ifdef COMPILE_WITH_MUSA
#ifndef M68K__HEADER
extern "C"
{
#include "m68k.h"
}
#endif
#endif

#ifndef	_MZ80_H_
#include "mz80.h"
#endif

//#define BUILD_YM2612
extern "C" {
#include "fm.h"
}

extern "C" int dprintf(char *format, ...);


extern "C" {
int SN76496_init(int chip,int clock,int sample_rate,int sample_bits);
void SN76496Write(int chip,int data);
void SN76496Update_16(int chip,void *buffer,int length);
}

#ifdef _INC_WINDOWS
int DirectInputInit(HINSTANCE hInstance);
#endif
int input_check(int code);
int input_update();

extern "C" int blur_bitmap_15(unsigned char *dest,int len);
extern "C" int blur_bitmap_16(unsigned char *dest,int len);
extern "C" char *prof_name;
// When you profile something, it's a very good idea
// to make sure it doesn't return before you call PROFE!
#define PROFS(x)  { char *oname=prof_name; prof_name=x;
#define PROFE       prof_name=oname; }

#ifndef STRUCT_BMAP
struct bmap { unsigned char *data; int w,h; int pitch; int bpp; };
#define STRUCT_BMAP
#endif

// Info on VDP
// passed to render the screen (possibly line by line)
struct dgen_sinfo { unsigned char *vram,*cram,*vsram,*vdp_reg; };
struct sndinfo { signed short *l,*r; int len; };

// GRAPH.CPP:
int draw_md_graphics
  (struct bmap *bm,unsigned char passpal[256],struct dgen_sinfo *si,
    int ymin,int ymax,int layer_sel);
// rastergf.cpp
class md;
void DrawMDScanline( struct bmap *bits, struct dgen_sinfo *screen, int scanline,md *megaDrive);

int get_md_palette(unsigned char pal[256],unsigned char *cram);

class md_vdp
{
public:
  // Next three lines are the state of the VDP
  // They have to be public so we can save and load states
  // and draw the screen.
  unsigned char *mem,*vram,*cram,*vsram;
  unsigned char reg[0x20];
  int rw_mode,rw_addr,rw_dma;
private:

  int poke_vram (int addr,unsigned char d);
  int poke_cram (int addr,unsigned char d);
  int poke_vsram(int addr,unsigned char d);
  int dma_len();
  int dma_addr();
  unsigned char dma_mem_read(int addr);
  int putword(unsigned short d);
  int putbyte(unsigned char d);
  int ok;
public:
  md_vdp(); ~md_vdp();
  md *belongs;
// These are called by MEM.CPP
  int command(unsigned int cmd);
  unsigned short readword();
  unsigned char readbyte();
  int writeword(unsigned short d);
  int writebyte(unsigned char d);

  int okay() {return ok;}
  unsigned char *dirt; // Bitfield: what has changed VRAM/CRAM/VSRAM/Reg
  int get_screen_info(struct dgen_sinfo *si); // For Graph
  int reset();
  int make_dirty();

// Joe's RAS.CPP
  // Used by draw_scanline to render the different display components
  void draw_tile1(int which, int line, unsigned char *where);
  void draw_tile1_solid(int which, int line, unsigned char *where);
  void draw_tile2(int which, int line, unsigned char *where);
  void draw_tile2_solid(int which, int line, unsigned char *where);
// FIXME: do 3 bytes/pixel
  void draw_tile3(int which, int line, unsigned char *where);
  void draw_tile3_solid(int which, int line, unsigned char *where);
  void draw_tile4(int which, int line, unsigned char *where);
  void draw_tile4_solid(int which, int line, unsigned char *where);
  void draw_window(int line, int front);
  void draw_sprites(int line, int front);
  void draw_plane_back0(int line);
  void draw_plane_back1(int line);
  void draw_plane_front0(int line);
  void draw_plane_front1(int line);
  // Working variables for the above
  unsigned char sprite_order[0x101], *sprite_base;
  unsigned sprite_count, Bpp, Bpp_times8;
  unsigned char *dest;

  void draw_scanline(struct bmap *bits, int line);

// end of Joe's RAS.CPP

// LINE.CPP:
private:
  int draw_sprites(unsigned char *bml,int bpp,int line);
  unsigned int *highpal;
  int tile_line(unsigned char *at,int bpp,int line,int tile);
public:
  int draw_md_line(struct bmap *bm,int line,int mark);
};

class md
{
private:
  int romlen;
  int ok; 
  unsigned char *mem,*rom,*ram,*z80ram;
// Joe's bit for save ram:
  unsigned char *saveram;
  unsigned save_start, save_len;

  int last_render_method,last_bpp;

public:
  md_vdp vdp;
private:
  struct mz80context z80;
#ifdef COMPILE_WITH_STAR
  struct S68000CONTEXT cpu;
  struct STARSCREAM_PROGRAMREGION *fetch;
  struct STARSCREAM_DATAREGION *readbyte,*readword,*writebyte,*writeword;
  int memory_map();
#endif
  int star_mz80_on();
  int star_mz80_off();

  int z80_bank68k; // 9 bits
  int z80_online;
  int hint_countdown;
  int z80_extra_cycles;

  // Note order is (0) Vblank end -------- Vblank Start -- (HIGH)
  // So int6 happens in the middle of the count

  int coo_waiting; // after 04 write which is 0x4xxx or 0x8xxx
  unsigned int coo_cmd; // The complete command
  int aoo3_toggle,aoo5_toggle,aoo3_six,aoo5_six;
  int aoo3_six_timeout,aoo5_six_timeout;
  int run_to_odo(int odo_to);

  int fm_sel[2],fm_tover[2],ras_fm_ticker[4];
  signed short fm_reg[2][0x100]; // ALL of them (-1 = not defined yet)
  int dac_data,dac_enable;

  int odo,odo_line_start,odo_line_len,ras;
  int calculate_hcoord();
  unsigned char calculate_coo8();
  unsigned char calculate_coo9();
  int may_want_to_get_pic(struct bmap *bm,unsigned char retpal[256],int mark);
  int may_want_to_get_sound(struct sndinfo *sndi,int sndfrom,int sndto);
  int z80_int_pending;
  int fm_timer_callback();
  int mysn_write(int v);
  int myfm_read(int a);
public:
  int myfm_write(int a,int v,int md);
  int mjazz;

  int pause;
  int snd_mute; // &1=fm &2=psg &4=dac
  int country_ver,layer_sel;

  int flush_fm_to_mame();
  int one_frame(
    struct bmap *bm,unsigned char retpal[256],
    struct sndinfo *sndi);

  // one_frame_hints call is now obsolete, use one_frame() instead

  int pad[2];
// c000004 bit 1 write fifo empty, bit 0 write fifo full (???)
// c000005 vint happened, (sprover, coll, oddinint)
// invblank, inhblank, dma busy, pal
  unsigned char coo4,coo5;
  int okay() {return ok;}
  md();
  ~md();
  int plug_in_without_reset(unsigned char *cart,int len);
  int plug_in(unsigned char *cart,int len);
  int unplug();
  int load(char *name,int do_reset);

  int reset();

  unsigned misc_readbyte(unsigned a);
  void misc_writebyte(unsigned a,unsigned d);
  unsigned misc_readword(unsigned a);
  void misc_writeword(unsigned a,unsigned d);

  int z80_init();
  unsigned char z80_read(unsigned int a);
  void z80_write(unsigned int a,unsigned char v);
  unsigned short z80_port_read(unsigned short a);
  void z80_port_write(unsigned short a,unsigned char v);

  int cpu_emu;// OK to read it but call change_cpu_emu to change it
  int change_cpu_emu(int to);
  mz80context&   z80_context() {return z80;}
  int load(FILE *hand,char *desc);
  int save(FILE *hand,char *desc);
  int import_gst(FILE *hand);
  int export_gst(FILE *hand);

  char romfilename[256]; // If known

  // Joe's code in md-joe.cpp and decode.c
  void patch(const char *list);
  void get_save_ram(FILE *from);
  void put_save_ram(FILE *into);
  int has_save_ram();

  int fix_rom_checksum();
  int render_method;

  int get_screen_info(struct dgen_sinfo *si)
  { return vdp.get_screen_info(si); }
  int z80dump();
};

