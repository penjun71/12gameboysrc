#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "d.h"
#include "md/md.h"

char rom_name[0x800]="";
char rom_path[0x800]="";
char state_name[0x800]="";
char state_path[0x800]="";
static char config_name[0x800]="";

int get_rom_name()
{
	OPENFILENAME	open;
  char  Filter[]=
    "Genesis Roms (*.bin;*.smd;*.zip)\0"
    "*.bin;*.smd;*.zip\0"
    "Zipped Roms (*.zip)\0"
    "*.zip\0"
    "All Files\0"
    "*.*\0";
	memset(&open, 0, sizeof(OPENFILENAME));

	open.lStructSize=sizeof(OPENFILENAME);
  open.lpstrInitialDir=rom_path;
	open.lpstrFilter=&Filter[0];
  rom_name[0]=0;
  open.lpstrFile=rom_name;
  open.nMaxFile=0x7ff;
  open.lpstrTitle="DGen - Load Rom Image";
  open.Flags=OFN_HIDEREADONLY;
  if (GetOpenFileName(&open)==0) return 1;

  int i; i=strlen(rom_name);
  for (;i>=0;i--) if (rom_name[i]=='\\') break;
  if (i>0x7ff) i=0x7ff;
  memcpy(rom_path,rom_name,i);
  rom_path[i]=0;

  return 0;
}

int get_state_name(int save)
{
	OPENFILENAME	open;
  char      Filter[]="*.gs?\0*.gs?\0All Files\0*.*\0";
	memset(&open, 0, sizeof(OPENFILENAME));

	open.lStructSize=sizeof(OPENFILENAME);
  open.lpstrInitialDir=state_path;
	open.lpstrFilter=&Filter[0];

//  if (megad.romfilename[0]==0)
    state_name[0]=0;
//  else
//    sprintf (state_name,"%.8s.gs%c",gst_name(megad.romfilename),save?'x':'?');
  open.lpstrFile=state_name;

  open.nMaxFile=0x7ff;
  open.lpstrTitle=save?"DGen - Save State to...":"DGen - Load State from...";
  open.Flags=OFN_HIDEREADONLY;
  if (save)
  {
    if (GetSaveFileName(&open)==0) return 1;
  }
  else
  {
    if (GetOpenFileName(&open)==0) return 1;
  }

  int i; i=strlen(state_name);
  for (;i>=0;i--) if (state_name[i]=='\\') break;
  if (i>0x7ff) i=0x7ff;
  memcpy(state_path,state_name,i);
  state_path[i]=0;

  return 0;
}

extern int pad_code[32]; // In MDINP.CPP

// parses the config file
int load_config(md *megad)
{
  int done=0;
  FILE *hand;
  int i;

  GetCurrentDirectory(0x7f0,config_name);

  if (state_path[0]==0) strcpy(state_path,config_name);
  if (rom_path[0]==0)   strcpy(rom_path  ,config_name);
  strcat(config_name,"\\dgen.ini");

  hand=fopen(config_name,"rt");
  if (hand==NULL)
    return 1;

  dprintf ("Getting config from '%s'\n",config_name);
  done=0;
  while (!done)
  {
    static char buf[256]="";
    if (fgets(buf,255,hand)==NULL) done=1;
    else
    {
      char *check_for=NULL;
      int slen=strlen(buf);

      // Knock off linefeeds, because they will mess up strings
      if (buf[slen-1]==10) {buf[slen-1]=0; slen--;}

#define IT_IS (strncmp(buf,check_for,strlen(check_for))==0)
#define VALUE (buf+strlen(check_for))

      // Input
      check_for="pad_code[";
      if (IT_IS)
      {
        char *next_bit;
        int p=strtol(VALUE,&next_bit,10);
        if (next_bit!=NULL)
        {
          if (next_bit[0]==']')
          {
            if (next_bit[1]==' ')
            {
              int val=strtol(next_bit+2,NULL,0);
              pad_code[p&31]=val;
            }
          }
        }
      }


      // Paths
      check_for="rom_path";
      if (IT_IS) if (*VALUE)
          strncpy(rom_path,VALUE+1,0x7ff);
      check_for="state_path";
      if (IT_IS) if (*VALUE)
      strncpy(state_path,VALUE+1,0x7ff);

      // Misc
      check_for="country_ver";
      if (IT_IS) megad->country_ver=strtol(VALUE,NULL,0);
      check_for="cpu_emu";
      if (IT_IS) megad->change_cpu_emu(atoi(VALUE));
    }
  }

  fclose(hand);
  return 0;
}

int unload_config(md *megad)
{
  int i;
  FILE *hand=fopen(config_name,"wt");

  if (hand==NULL) return 1;

  fprintf(hand,"// Dave's DGEN v%s (DirectX) config file\n\n",VER);
  fprintf(hand,"// Note, 0=off, 1=on\n");

  fprintf(hand,"\n// Graphics\n");

  fprintf(hand,"\n\n// Sound\n");

  fprintf(hand,"\n\n// Input\n");
  for (i=0;i<32;i++)
    fprintf(hand,"pad_code[%.2d] 0x%.5x\n",i,pad_code[i]);

  fprintf(hand,"\n\n// Paths\n");
  fprintf(hand,"rom_path %s\n",rom_path);
  fprintf(hand,"state_path %s\n",state_path);

  fprintf(hand,"\n\n// Misc.\n");
  fprintf(hand,"country_ver 0x%.2x\n",megad->country_ver);
  fprintf(hand,"cpu_emu %d\n",megad->cpu_emu);

  fprintf(hand,"\n");
  fclose(hand);

  dprintf ("Saved config to '%s'\n",config_name);
  return 0;
}
