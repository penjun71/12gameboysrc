// DGen v1.14+
// DirectInput routines thanks to Steve Snake
#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#define   DIRECTINPUT_VERSION 0x0500
#include <dinput.h>
#include "d.h"

//------------------------------------------------------------------------------------------------
// Stuff for DGen - Written by Steve Snake sometime in 1998/1999.
//------------------------------------------------------------------------------------------------
// (Dave: fiddled with it a bit.)

//------------------------------------------------------------------------------------------------
// Direct Input Init.
//------------------------------------------------------------------------------------------------

IDirectInput *lpDI=NULL;
IDirectInputDevice *lpDIKeyboard=NULL; //,*lpDIMouse=NULL;

#define MAX_DI_JOYSTICKS 32
static int JoystickCount=0;
static IDirectInputDevice2 *joy_idi[MAX_DI_JOYSTICKS]={NULL};

//------------------------------------------------------------------------------------------------
// Joystick Enumeration Callback.
//------------------------------------------------------------------------------------------------

BOOL CALLBACK
DIEnumJoyCallback(LPCDIDEVICEINSTANCE lpDIIJoy, LPVOID pvRef)
{
	BOOL					bRes;
	DWORD					ButtonCount;

  joy_idi[JoystickCount]=0;

  int ret;
  GUID guid=lpDIIJoy->guidInstance;

	LPDIRECTINPUTDEVICE	lpDIJoystick;
  ret=lpDI->CreateDevice(guid, &lpDIJoystick, NULL);
  if (ret!=DI_OK) dprintf ("DIEnum: Createdevice returned %d\n",ret);
  if(ret!=DI_OK)
    return(DIENUM_CONTINUE);
	
  ret=lpDIJoystick->QueryInterface(IID_IDirectInputDevice2, (void **)&joy_idi[JoystickCount]);
	lpDIJoystick->Release();

  if (ret!=DI_OK) dprintf ("DIEnum: QueryInterface returned %.8x\n",ret);
  if(ret!=DI_OK)
	{
    joy_idi[JoystickCount]=NULL;
    return(DIENUM_CONTINUE);
	}

  ret=(joy_idi[JoystickCount])->SetDataFormat(&c_dfDIJoystick);
  if (ret!=DI_OK) dprintf ("DIEnum: SetDataFormat returned %d\n",ret);
  if(ret!=DI_OK)
	{
    (joy_idi[JoystickCount])->Release();
    joy_idi[JoystickCount]=0;
    return(DIENUM_CONTINUE);
  }

	DIPROPRANGE			diPrg;
	DIPROPDWORD			diPdw;

	diPrg.diph.dwSize		=	sizeof(diPrg);
	diPrg.diph.dwHeaderSize	=	sizeof(diPrg.diph); 
	diPrg.diph.dwObj		=	0;
	diPrg.diph.dwHow		=	DIPH_DEVICE;
	diPrg.lMin				=	-1000; 
	diPrg.lMax				=	+1000;
   
  ret=(joy_idi[JoystickCount])->SetProperty(DIPROP_RANGE, &diPrg.diph);
  if (ret!=DI_OK) dprintf ("DIEnum: SetProperty(DIPROP_RANGE...); returned %d\n",ret);

  if(JoystickCount++==MAX_DI_JOYSTICKS)  return(DIENUM_STOP);
	return(DIENUM_CONTINUE);
}


int directinput_init(HINSTANCE hInstance,HWND hWnd)
{
	HRESULT	hRes;

  dprintf("directinput_init()\n");

  if(DirectInputCreate(hInstance, DIRECTINPUT_VERSION, &lpDI, NULL)!=DI_OK)
	{
    dprintf("DIInit: DirectX 5.0 or higher required.\n");
		return(FALSE);
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Keyboard.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  hRes=lpDI->CreateDevice(GUID_SysKeyboard, &lpDIKeyboard, NULL);
	if(hRes!=DI_OK)
	{
    dprintf("DIInit: Cannot Locate Keyboard Device.\n");
		return(FALSE);
	}

  lpDIKeyboard->SetDataFormat(&c_dfDIKeyboard);

//  hRes=lpDIKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
  hRes=lpDIKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
  if(hRes!=DI_OK)
	{
    lpDIKeyboard->Release();
    dprintf("DIInit: Cannot Set Keyboard Cooperative Level.\n");
		return(FALSE);
	}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Joystick(s)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  JoystickCount=0;
  hRes=lpDI->EnumDevices(DIDEVTYPE_JOYSTICK, DIEnumJoyCallback, NULL, DIEDFL_ATTACHEDONLY);

/* don't use mouse yet
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Mouse
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  hRes=lpDI->CreateDevice(GUID_SysMouse, &lpDIMouse, NULL);
	if(hRes!=DI_OK)
	{
    dprintf("DIInit: Cannot Locate Pointing Device.");
		return(FALSE);
	}

  lpDIMouse->SetDataFormat(&c_dfDIMouse);

  hRes=lpDIMouse->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND);
	if(hRes!=DI_OK)
	{
    lpDIMouse->Release();
    dprintf("DIInit: Cannot Set Mouse Cooperative Level.");
		return(FALSE);
	}

*/

  int ret=lpDIKeyboard->Acquire();
  if (ret!=DI_OK) dprintf ("DI init: kb acquire returned %.8x\n",ret);

  return(TRUE);
}

int directinput_exit()
{
  dprintf("directinput_exit()\n");
  if (lpDI!=NULL) lpDI->Release(); lpDI=NULL;
  if (lpDIKeyboard!=NULL) lpDIKeyboard->Release(); lpDIKeyboard=NULL;
  return 0;
}

static DIJOYSTATE diJoyState[MAX_DI_JOYSTICKS]={{0}};
static unsigned char keys[256]={0};
static unsigned char bukeys[256]={0};

int directinput_update()
{
  int j,ret=0;
  int di_failed=1;

  if (lpDIKeyboard)
  {
    ret=lpDIKeyboard->GetDeviceState(256, keys);
    if (ret==DIERR_INPUTLOST)
    {
      ret=lpDIKeyboard->Acquire();
      ret=lpDIKeyboard->GetDeviceState(256, keys);
    }

    if (ret==DI_OK) di_failed=0;
  }

// Our backup 0x200+ codes if DirectInput isn't working
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
  if (di_failed)
    for (j=0;j<256;j++) bukeys[j]=KEY_DOWN(j);


  for (j=0;j<JoystickCount;j++)
  {
    if (joy_idi[j])
    {
      ret=joy_idi[j]->Acquire();
      if (ret==DI_OK)
      {
        joy_idi[j]->Poll();
        joy_idi[j]->GetDeviceState(sizeof(diJoyState[j]), &diJoyState[j]);
      }
      joy_idi[j]->Unacquire();
    }
  }

  return 0;
}

// Good for redefining keys (pass -1,returns code) OR checking keys
// (pass code, returns 0/1)
int directinput_check(int code,unsigned int jignore)
{
  int i,j,s=0; // s is true if the control we are looking at is pressed

  // Wait for something to be pressed and then return a code for it
  // Must call input_acquire first

  for (i=0;i<256;i++)
  {
    s=keys[i];
    if (code==-1) { if (s) return 0x0000+i; }
    else if (code==0x0000+i) return s;
  }

  for (i=0;i<256;i++)
  {
    s=bukeys[i];
    if (code==-1) { if (s) return 0x0200+i; }
    else if (code==0x0200+i) return s;
  }

  for (j=0;j<JoystickCount;j++)
  {
    int codebase;
    if (jignore&(1U<<j)) continue; // ignore this joystick (maybe stuck?)

    codebase=0x10000+j*0x100;
    if (joy_idi[j])
    {
      int b;
      int s;

      s=(diJoyState[j].lY<-500);
      if (code==-1) { if (s) return codebase+0x01; }
      else if (code==codebase+0x01) return s;

      s=(diJoyState[j].lY>+500);
      if (code==-1) { if (s) return codebase+0x02; }
      else if (code==codebase+0x02) return s;

      s=(diJoyState[j].lX<-500);
      if (code==-1) { if (s) return codebase+0x03; }
      else if (code==codebase+0x03) return s;

      s=(diJoyState[j].lX>+500);
      if (code==-1) { if (s) return codebase+0x04; }
      else if (code==codebase+0x04) return s;

      for (b=0;b<32;b++)
      {
        s=(diJoyState[j].rgbButtons[b]);
        if (code==-1) { if (s) return codebase+0x10+b; }
        else if (code==codebase+0x10+b) return s;
      }
    }
  }

  if (code==-1) return -1; // Nothing pressed
  return 0; // Unknown code passed
}



/* Don't use mouse yet

//------------------------------------------------------------------------------------------------
// Read Mouse State.
//------------------------------------------------------------------------------------------------

void
DirectInputReadMouse(void)
{
	HRESULT			hRes;
	DIMOUSESTATE	DIMouseState;

	if(KGen.MouseAcquired==FALSE)
	{
    lpDIMouse->Acquire();
		KGen.MouseAcquired=TRUE;
		KGen.MouseX=160;
		KGen.MouseY=120;
	}

  hRes=lpDIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &DIMouseState);
	if(hRes==DIERR_INPUTLOST)
	{
		SetMyWindowFocus();
    lpDIMouse->Acquire();
    hRes=lpDIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &DIMouseState);
	}
	if(hRes!=DI_OK)
	{
		DXMsg("Cannot Read Mouse State.", "DirectInput Acc Error #6:", hRes);
	}
	else
	{
		KGen.MouseX+=DIMouseState.lX;
		KGen.MouseY+=DIMouseState.lY;

		if((abs(DIMouseState.lX)>=3) || (abs(DIMouseState.lY)>=3))
			KGen.HideMouse=0;

		KGen.DMouseButton=KGen.MouseButton^0x80;
		KGen.MouseButton=DIMouseState.rgbButtons[0];
		KGen.DMouseButton&=KGen.MouseButton;

		if(KGen.MouseX<0)	KGen.MouseX=0;
		if(KGen.MouseX>319)	KGen.MouseX=319;
		if(KGen.MouseY<8)	KGen.MouseY=8;
		if(KGen.MouseY>220)	KGen.MouseY=220;
	}
}


*/

