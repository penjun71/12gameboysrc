#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <ddraw.h>
#include "d.h"
#include "../md/md.h"

// Main MD Screen Window

static char temp[0x1000]="";

extern FILE *errfile;
extern int udp_stuff();
extern int process_cl_args();

static IDirectDraw *dd=NULL;
static IDirectDrawSurface *prim=NULL,*buf=NULL;
static IDirectDrawClipper *clipper=NULL;


// This gets called until DirectDraw calls are completed
// clipper isn't vital
#define DIRECTDRAW_READY (dd&&prim&&buf)
static int direct_draw_init(HWND wnd)
{
  int ret=0;
  dprintf ("direct_draw_init()\n");

  if (dd==NULL)
  {
    ret = DirectDrawCreate( NULL, &dd, NULL );
    if (ret!=DD_OK)
    {
      dprintf ("DirectDrawCreate returned %x\n",ret);
      return 1;
    }
  }
  if (dd!=NULL)
  {
    ret=dd->SetCooperativeLevel(wnd, DDSCL_NORMAL);
    if (ret!=DD_OK)
    {
      dprintf ("SetCooperativeLevel returned %x\n",ret);
      return 1;
    }

    if (prim==NULL) prim=create_primary_surface(dd);
    if (prim==NULL) return 1;

    if (buf==NULL)  buf=create_surface(dd,320+16,224+16);
    if (buf==NULL)  return 1;

    if (prim!=NULL)
      if (clipper==NULL) clipper=attach_clipper(dd,wnd,prim);
  }
  return 0;
}

static int direct_draw_exit()
{
  dprintf ("direct_draw_exit()\n");
  // Release DirectDraw when we are destroyed
  if (clipper  !=NULL) clipper->Release(); clipper=NULL;
  if (buf      !=NULL) buf    ->Release(); buf    =NULL;
  if (prim     !=NULL) prim   ->Release(); prim   =NULL;
  if (dd       !=NULL) dd     ->Release(); dd     =NULL;
  return 0;
}


static int md_win_paint(HWND wnd)
{
  RECT src,dest; int ret=0;

// 320 and 224 should really be determined by a Get DESC call
  src.left=8; src.top=8;
  src.right=8+320; src.bottom=8+224;

  GetWindowRect(wnd,&dest);
  dest.top+=22; dest.left+=4;
  dest.right=dest.left+320;
  dest.bottom=dest.top+224;

  // Okay - finished with bm structure now

  ret=prim->Blt(&dest,buf,&src,DDBLT_WAIT,NULL);
  if (ret==DD_OK) ;                  // great!
  else if (ret==DDERR_SURFACEBUSY) ; // not too bad
  else
  {
    // Otherwise we had better notify the user:

    dprintf ("prim->Blt returned %x (%+-d)\n",
      ret,ret-MAKE_DDHRESULT(0));
    if (ret==DDERR_SURFACELOST)      // oops
      direct_draw_exit();// Try reinitting DDraw
  }

  if (ret!=DD_OK) return 1;
  return 0;
}

int md_win_one_frame(HWND wnd,md *megad)
{
  PROFS("DirectX overhead")

  struct bmap bm={0};
  extern IDirectDrawSurface *buf;

  if (buf==0) return 1;
  if (lock_surface_to_bm(&bm,buf)!=0) return 1;
  if (bm.data==NULL) return 1;


  megad->one_frame(&bm,NULL,NULL);
  buf->Unlock(bm.data); bm.data=NULL;
  md_win_paint(wnd);

  PROFE
  return 0;
}

LRESULT CALLBACK md_win_proc
  (HWND wnd,UINT cmd,WPARAM wparam,LPARAM lparam)
{
  int ret=0;

  if (cmd==WM_PAINT)
  {
    if (DIRECTDRAW_READY)
      md_win_paint(wnd);
    else
    {
      if (direct_draw_init(wnd)!=0)
        direct_draw_exit();
        // maybe have another go later when problem has cleared
    }
//    return 0;
  }

  if (cmd==WM_KEYDOWN || cmd==WM_KEYUP )
  {
    extern md *megad; int mask=0;
         if (wparam==0x26) mask=0x01;
    else if (wparam==0x28) mask=0x02;
    else if (wparam==0x25) mask=0x04;
    else if (wparam==0x27) mask=0x08;
    else if (wparam==0x0d) mask=0x2000;
    else if (wparam==0x41) mask=0x1000;
    else if (wparam==0x53) mask=0x10;
    else if (wparam==0x44) mask=0x20;
    if (cmd==WM_KEYDOWN) megad->pad[0]&=~mask;
    if (cmd==WM_KEYUP  ) megad->pad[0]|= mask;
  }

  if (cmd==WM_DESTROY)
  {
    direct_draw_exit();
    PostQuitMessage(0);
//    return 0;
  }

  return DefWindowProc(wnd,cmd,wparam,lparam);
}                                     

int md_win_init(HINSTANCE hinst)
{
  static WNDCLASS wclass;
  memset(&wclass,0,sizeof(wclass));
  wclass.style = 0; //CS_HREDRAW | CS_VREDRAW;
  wclass.lpszClassName="md_win";
  wclass.hInstance=hinst;
  wclass.lpfnWndProc=md_win_proc;
  wclass.hbrBackground=GetStockObject(BLACK_BRUSH);
  wclass.hCursor = LoadCursor(NULL, IDC_ARROW);
//  wclass.lpszMenuName = "NETPMENU";  // Name of menu resource in .RC file.
  RegisterClass(&wclass);
  return 0;
}

