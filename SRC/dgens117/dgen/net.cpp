//#define UNIX_LAND
#define STRCASECMP stricmp
#define WIN_LAND

#ifndef STRCASECMP
#define STRCASECMP strcasecmp
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef UNIX_LAND
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
typedef int SOCKET;
#else

#ifdef WIN_LAND
#include <windows.h>
#include <ddraw.h>
#endif

#include <winsock.h>
#endif

#include "d.h"

extern char *next_cl_arg();

// errfile should always be a valid file handle, or the program will crash
char dest_name[0x1000]="localhost";
int dest_port=19890;
static char send_string[0x1000]="One",recv_buf[0x1000]="";

int net_init()
{
  int ret;
#ifndef UNIX_LAND
  {
    WSADATA wsd;
    memset(&wsd,0,sizeof(wsd));
    ret=WSAStartup(0x0101,&wsd);
    dprintf("WSAStartup(0x0101,&wsd) returned %d\n",ret);
    dprintf("wsd.wVersion==%.4x\n",wsd.wVersion);
    dprintf("wsd.wHighVersion==%.4x\n",wsd.wHighVersion);
    dprintf("wsd.iMaxUdpDg==%d\n",wsd.iMaxUdpDg);
  }
#endif
  return 0;
}

int net_exit()
{
  int ret=0;
#ifndef UNIX_LAND
  ret=WSACleanup();
  dprintf("WSACleanup() returned %d\n",ret);
#endif
  return 0;
}

int net_server(HWND wnd,int bind_port)
{
  int ret=0;
  SOCKET s;
  struct sockaddr_in bind_addr;
  int from_addr_len=0;
  if (bind_port<=0) bind_port=19890;

  s=socket(AF_INET,SOCK_DGRAM,0/*protocol*/);
  dprintf("Socket number is %d\n",s);
  if (s<0) return 1;

  bind_addr.sin_family=AF_INET;
  bind_addr.sin_port = htons((unsigned short)bind_port);
  bind_addr.sin_addr.s_addr = INADDR_ANY;
  ret=bind(s,(struct sockaddr *)&bind_addr,sizeof(bind_addr));
  dprintf("bind() returned %d\n",ret);
  if (ret!=0) return 1;

  dprintf("Did bind to port %d\n",bind_port);

  ret=WSAAsyncSelect(s,wnd,WM_USER+(int)s,FD_READ);
  dprintf("WSAAsyncSelect(s,wnd,WM_USER+%d,FD_READ) returned %d\n",(int)s,ret);
  return 0;
}

int net_recv(int soc)
{
  int ret=0;
  struct sockaddr_in from_addr;
  int from_addr_len=0;
  SOCKET s;
  s=(SOCKET)soc;
  unsigned int dotted;

  memset(&from_addr,0,sizeof(from_addr));
  from_addr_len=sizeof(from_addr);

  ret=recvfrom(s,recv_buf,0x1000,0,(struct sockaddr *)&from_addr,&from_addr_len);
  if (ret>=0) recv_buf[ret]=0;

//  dprintf("recvfrom() returned %d\n",ret);
  dotted=from_addr.sin_addr.s_addr; dotted=ntohl(dotted);
  dprintf("%d.%d.%d.%d: %s\n",
    (dotted>>24)&255,
    (dotted>>16)&255,
    (dotted>> 8)&255,
    (dotted    )&255,recv_buf);
  return 0;
}

int net_client(char *mess)
{
  int ret=0;
  SOCKET s;
  struct sockaddr_in to_addr;
  struct hostent *he=NULL;
  struct in_addr *ina=NULL;

  if (mess==NULL) mess=send_string;

  s=socket(AF_INET,SOCK_DGRAM,0/*protocol*/);
  if (s<0) dprintf("Made Socket number %d\n",s);
  if (s<0) return 1;

  // Do DNS lookup
  //dprintf("Doing DNS lookup on %s...\n",dest_name);

  he=gethostbyname(dest_name);
  if (he==NULL) dprintf("gethostbyname(%s) failed\n",dest_name);
  else
  {
    unsigned int dotted=0;
    ina=(struct in_addr *)he->h_addr_list[0];
    if (ina!=NULL)
    {
      dotted=ina->s_addr; dotted=ntohl(dotted);
      //dprintf("ip of %s is %d.%d.%d.%d\n",
      //  dest_name,
      //  (dotted>>24)&255,
      //  (dotted>>16)&255,
      //  (dotted>> 8)&255,
      //  (dotted    )&255);
    }
  }
  memset(&to_addr,0,sizeof(to_addr));

  to_addr.sin_family=AF_INET;
  to_addr.sin_port = htons((unsigned short)dest_port);
  if (ina!=NULL) to_addr.sin_addr = *ina;
  ret=sendto(s,mess,strlen(mess),0,(struct sockaddr *)&to_addr,sizeof(to_addr));
  //dprintf("Did send [%s] to %s port %d\n",mess,dest_name,dest_port);
  //dprintf("sendto() returned %d\n",ret);

//  {
//    int t=1;
//    ret=setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(const char *)&t,sizeof(t));
//    dprintf("setsockopt returned %d\n",ret);
//  }

  closesocket(s);
  return 0;
}

