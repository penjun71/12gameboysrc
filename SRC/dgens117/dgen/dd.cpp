#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <ddraw.h>
#include "d.h"

// Dave's DirectDraw routines
IDirectDrawClipper *attach_clipper
  (IDirectDraw *lpdd,HWND hWnd,IDirectDrawSurface *lpprim)
{
  IDirectDrawClipper *clipper=NULL; int ret=0;
  ret=lpdd->CreateClipper(0,&clipper,NULL);
  if (ret!=DD_OK)
  {
    dprintf ("createclipper failed: Ret was %x (%+-d)\n",
      ret,ret-MAKE_DDHRESULT(0));
  }
  else
  {
    ret=clipper->SetHWnd(0,hWnd);
    if (ret!=DD_OK)
    {
      dprintf ("SethWnd failed: Ret was %x (%+-d)\n",
        ret,ret-MAKE_DDHRESULT(0));
    }
    else
    {
      ret=lpprim->SetClipper(clipper);
      if (ret!=DD_OK)
      {
        dprintf ("SetClipper failed: Ret was %x (%+-d)\n",
          ret,ret-MAKE_DDHRESULT(0));
      }
    }
  }
  return 0;
}

IDirectDrawSurface *create_surface (IDirectDraw *lpdd,int width,int height)
{
  IDirectDrawSurface *lpsurf=NULL; int ret=0;
  // Seems to default to Window's bpp anyway!
  DDSURFACEDESC ddsd;
  //Clear all fields in structure
  memset( &ddsd, 0, sizeof(ddsd) );
  //Set flags in structure
  ddsd.dwSize = sizeof( ddsd );
  ddsd.dwFlags = DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT;
  ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY;

  ddsd.dwWidth  = width;
  ddsd.dwHeight = height;
  ret=lpdd->CreateSurface(&ddsd,&lpsurf,NULL);
  if (ret!=DD_OK)
  {
    dprintf (
      "Error creating buffer Surface: Return value was %x (%+-d)\n",
      ret,ret-MAKE_DDHRESULT(0));
    return NULL;
  }
  return lpsurf;
}

IDirectDrawSurface *create_primary_surface(IDirectDraw *lpdd)
{
  int ret=0; IDirectDrawSurface *lpsurf=NULL;
  if (lpdd==NULL) return 0;
  //  (Creating Primary Surface)
  DDSURFACEDESC ddsd;
  //Clear all fields in structure
  memset( &ddsd, 0, sizeof(ddsd) );
  //Set flags in structure
  ddsd.dwSize = sizeof( ddsd );
  ddsd.dwFlags = DDSD_CAPS;
  ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
  //Create Surface
  ret = lpdd->CreateSurface(&ddsd, &lpsurf, NULL );
  if (ret!=DD_OK)
  {
    dprintf (
      "Error creating primary Surface: Return value was %x (%+-d)\n",
      ret,ret-MAKE_DDHRESULT(0));
    return NULL;
  }
  return lpsurf;
}

// Remember, after you are done using the DirectDrawSurface,
// you must do this:
// bufsurf->Unlock(bm.data); bm.data=NULL;

int lock_surface_to_bm(struct bmap *sbm,IDirectDrawSurface *lpsurf)
{
  // Turn the buf surface into something we can write into
  DDSURFACEDESC sd; int ret=0;

  memset(&sd,0,sizeof( sd ));
  sd.dwSize = sizeof( sd );
  ret=lpsurf->GetSurfaceDesc(&sd);
  if (ret!=DD_OK)
  {
    dprintf ("GetSurfaceDesc returned %x (%+-d)\n"
      ,ret,ret-MAKE_DDHRESULT(0));
    return 1;
  }

  sbm->w=sd.dwWidth;
  sbm->h=sd.dwHeight;

  DDPIXELFORMAT pf;
  memset(&pf,0,sizeof(pf));
  pf.dwSize = sizeof(pf);
  ret=lpsurf->GetPixelFormat(&pf);
  if (ret!=DD_OK)
  {
    dprintf ("ret is %x (%+-d)\n",ret,ret-MAKE_DDHRESULT(0));
    return 1;
  }

  sbm->bpp=pf.dwRGBBitCount;
  if (sbm->bpp==16)
    if (pf.dwGBitMask==0x03e0) sbm->bpp=15;

  ret=lpsurf->Lock(NULL,&sd,DDLOCK_SURFACEMEMORYPTR,NULL);
  if (ret!=DD_OK)
  {
    dprintf ("lpsurf->Lock returned %.8x (%.8x) sd is %p\n",
      ret,MAKE_DDHRESULT(0),sd.lpSurface);
    return 1;
  }
  sbm->pitch=sd.lPitch;
  sbm->data=(unsigned char *)sd.lpSurface;
  return 0;
}
